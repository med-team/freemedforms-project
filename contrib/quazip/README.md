# Build process
 
You have to include the QuaZip lib and also the zlib. 
You can use the .pro and .pri of the FreeMedForms project to build the lib QuaZip as a static library.
Also update the dontbuildquazip CONFIG tag in buildspecs/optionalfeatures.pri

For further explanation, please refer to https://freemedforms.com