/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include <QtPlugin>
#include <QApplication>
#include <QDir>
#include <QTimer>

#include <QDebug>

#include <extensionsystem/pluginmanager.h>
#include <extensionsystem/pluginspec.h>
#include <extensionsystem/iplugin.h>

#include <utils/log.h>
#include <utils/global.h>

#include <iostream>

#include <QLocale>
#include <QDebug>

enum { WarnAllPluginSpecs=false };

typedef QList<ExtensionSystem::PluginSpec *> PluginSpecSet;
static const char* COREPLUGINSNAME = "Core";
static const char* USERPLUGINSNAME = "UserManager";

static const QString HELP_MESSAGE =
        QString("FreeMedForms %1 (%2%3 compilation)\n"
                "Usage: freemedforms [--clear-user-databases] [--create-virtuals] [--config=...]\n"
                "                    [--version,-version,-v] [--help,-help,-h]\n\n"
                "FreeMedForms, free and open source Electronic Medical Record.\n\n"
                "Options:\n"
                "  -h, -help, --help:         show this message\n"
                "  -v, -version, --version:   show the version\n"
                "  --config=/path/to/config.ini, --config=../../relative/path/config.ini\n"
                "                             Define the configuration file to use.\n"
                "                             In the case the specified file is found, in debug\n"
                "                             mode, the application resources will be located\n"
                "                             next to it (pixmaps, forms, user databases...)\n"
                "  --clear-user-databases:    only available with debug compilations,\n"
                "                             removes all user databases and recreate them. All\n"
                "                             data will be lost.\n"
                "  --create-virtuals:         create virtual data to ease tests only available\n"
                "                             with debug compilation.\n"
                "  --wine                     obsolete, ensure compatibility with wine\n"
                )
        .arg(PACKAGE_VERSION)
#ifdef LINUX_INTEGRATED
#  ifdef DEBUG
        .arg("Debug (Linux Integrated)")
#  else
        .arg("Release (Linux Integrated)")
#  endif
#else  // NOT LINUX_INTEGRATED
#  ifdef DEBUG
        .arg("Debug")
#  else
        .arg("Release")
#  endif
#endif  // LINUX_INTEGRATED

#ifdef DEBUG_WITHOUT_INSTALL
        .arg("-debug_without_install")
#else
        .arg("")
#endif

    ;

int main(int argc, char *argv[])
{
     QApplication app(argc, argv);

     app.setApplicationName(BINARY_NAME);
     app.setOrganizationName(BINARY_NAME);
     app.setApplicationVersion(PACKAGE_VERSION);

     QStringList args = qApp->arguments();
     if (args.contains("--version") ||
         args.contains("-version") ||
         args.contains("-v")) {
         std::cout << qPrintable(VERSION_MESSAGE);
         return 0;
     }

     if (args.contains("--help") ||
         args.contains("-help") ||
         args.contains("-h")) {
         std::cout << qPrintable(HELP_MESSAGE);
         return 0;
     }

     ExtensionSystem::PluginManager pluginManager;
     pluginManager.setFileExtension(QString("pluginspec"));

     QStringList libpath;
#ifdef LIBRARY_BASENAME
     libpath << Utils::applicationPluginsPath(QString(BINARY_NAME), QString(LIBRARY_BASENAME));
#else
     libpath << Utils::applicationPluginsPath(QString(BINARY_NAME), "");
#endif
     if (libpath.isEmpty())
         return 123;
     pluginManager.setPluginPaths(libpath);

     Utils::Log::logCompilationConfiguration();

     QStringList arguments = app.arguments(); // adapted arguments list is passed to plugin manager later
     QMap<QString, QString> foundAppOptions;
     if (arguments.size() > 1) {
         QMap<QString, bool> appOptions;
         QString errorMessage;
         if (!pluginManager.parseOptions(arguments, appOptions, &foundAppOptions, &errorMessage)) {
         }
     }

     const PluginSpecSet plugins = pluginManager.plugins();
     ExtensionSystem::PluginSpec *coreplugin = 0;

     if (WarnAllPluginSpecs) {
        foreach (ExtensionSystem::PluginSpec *spec, plugins) {
            qWarning() << "PluginSpecs :::"<< spec->filePath() << spec->name() << spec->version();
        }
    }

    foreach (ExtensionSystem::PluginSpec *spec, plugins) {
        if (spec->name() == QString(COREPLUGINSNAME)) {
            coreplugin = spec;
            break;
        }
    }

    ExtensionSystem::PluginSpec *userplugin = 0;
    foreach (ExtensionSystem::PluginSpec *spec, plugins) {
        if (spec->name() == QString(USERPLUGINSNAME)) {
            userplugin = spec;
            break;
        }
    }

    if (!coreplugin) {
        const QString reason = QCoreApplication::translate("Application", "Couldn't find 'Core.pluginspec' in %1").arg(qApp->libraryPaths().join("; "));
        qWarning() << reason;
        return 1;
    }
    if (!userplugin) {
        const QString reason = QCoreApplication::translate("Application", "Couldn't find 'UserManager.pluginspec' in %1").arg(qApp->libraryPaths().join("; "));
        qWarning() << reason;
        return 1;
    }


    if (coreplugin->hasError()) {
        qWarning() << coreplugin->errorString();
        return 1;
    }

    if (userplugin->hasError()) {
        qWarning() << userplugin->errorString();
        return 1;
    }




    pluginManager.loadPlugins();
    if (WarnAllPluginSpecs) {
        foreach (ExtensionSystem::PluginSpec *spec, plugins) {
            qWarning() << "PluginSpecs :::"<< spec->name() << "hasError:" << spec->hasError() << spec->errorString();
        }
    }

    if (coreplugin->hasError()) {
        qWarning() << coreplugin->errorString();
        return 1;
    }
    if (userplugin->hasError()) {
        qWarning() << userplugin->errorString();
        return 1;
    }



    QObject::connect(&app, SIGNAL(aboutToQuit()), &pluginManager, SLOT(shutdown()));

#ifdef WITH_TESTS
    if (pluginManager.runningTests())
        QTimer::singleShot(100, &pluginManager, SLOT(startTests()));
#endif

    int r = app.exec();
    return r;
}

