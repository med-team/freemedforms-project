#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

OTHER_FILES += \
    wizard.xml \
    myplugin_dependencies.pri \
    MyPlugin.pluginspec \
    myplugin.pro \
    myplugin.pri \
    myplugin_exporter.h.tpl \
    mypluginconstants.h.tpl \
    myplugin.h.tpl \
    myplugin.cpp.tpl
