/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer:                                                        *
 *       Eric MAEKER, <eric.maeker@gmail.com>                              *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "%PluginName:l%plugin.%CppHeaderSuffix%"

#include <utils/log.h>
#include <utils/randomizer.h>

#include <QTest>

using namespace %PluginName%;
using namespace Internal;

void %PluginName%Plugin::initTestCase()
{
}

void %PluginName%Plugin::test_your_plugin_unit_test()
{
}

void %PluginName%Plugin::cleanupTestCase()
{
}
