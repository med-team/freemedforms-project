#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

include(%PluginName:l%_dependencies.pri)
CONFIG( debug, debug|release ) {
    unix:LIBS    *= -l%PluginName%_debug
    win32:LIBS   *= -l%PluginName%_d
} else {
    LIBS  *= -l%PluginName%
}
