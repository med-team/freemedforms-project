<plugin name="%PluginName%" version="0.0.1" compatVersion="0.0.1">
    <vendor>%VendorName%</vendor>
    <copyright>(C) 2008-2017 by Eric MAEKER, MD</copyright>
    <license>See application's license</license>
    <description>%Description%</description>
    <url>%URL%</url>
    <dependencyList>
        <dependency name="Core" version="0.0.1"/>
    </dependencyList>
</plugin>
