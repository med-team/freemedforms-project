/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : %AuthorName%                                *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
@if "%Doxygen%" == "true"
/*!
@if "%Internal%" == "true"
 * \class %PluginNamespace:c%::Internal::%ClassName:c%
@else
 * \class %PluginNamespace:c%::%ClassName:c%
@endif
 * \brief short description of class
 *
 * Long description of class
 * \sa %PluginNamespace:c%::
 */
@endif

#include "%ClassName:l%.h"
#include "constants.h"

#include <formmanagerplugin/iformitem.h>
#include <formmanagerplugin/iformitemspec.h>

#include <utils/log.h>
#include <utils/global.h>

@if "%Translations%" == "true"
#include <translationutils/constants.h>

@endif
#include <QDebug>

using namespace %PluginNamespace:c%;
@if "%Internal%" == "true"
using namespace Internal;
@endif
@if "%Translations%" == "true"
using namespace Trans::ConstantTranslations;
@endif

@if "%Doxygen%" == "true"
@if "%Internal%" == "true"
/*! Constructor of the %PluginNamespace%::Internal::%ClassName:c% class */
@else
/*! Constructor of the %PluginNamespace%::%ClassName:c% class */
@endif
@endif
%ClassName:c%::%ClassName:c%(Form::FormItem *formItem, QWidget *parent)
  : Form::IFormWidget(formItem, parent)
{
    setObjectName("%ClassName:c%");










}

@if "%Doxygen%" == "true"
@if "%Internal%" == "true"
/*! Destructor of the %PluginNamespace%::Internal::%ClassName:c% class */
@else
/*! Destructor of the %PluginNamespace%::%ClassName:c% class */
@endif
@endif
%ClassName:c%::~%ClassName:c%()
{
}

void %ClassName:c%::addWidgetToContainer(Form::IFormWidget *widget)
{
    Q_UNUSED(widget);
}

bool %ClassName:c%::isContainer() const
{
    return false;
}

QString %ClassName:c%::printableHtml(bool withValues) const
{
    return QString::null;
}

void %ClassName:c%::retranslate()
{
    if (m_Label)
        m_Label->setText(m_FormItem->spec()->label());
}

%ClassName:c%Data::%ClassName:c%Data(Form::FormItem *item, %ClassName:c% *formWidget) :
    m_FormItem(item),
    m_Form(formWidget)
{
}

%ClassName:c%Data::~%ClassName:c%Data()
{
}

void %ClassName:c%Data::clear()
{
}

bool %ClassName:c%Data::isModified() const
{
    return false;
}

void %ClassName:c%Data::setModified(bool modified)
{
}

void %ClassName:c%Data::setReadOnly(bool readOnly)
{}

bool %ClassName:c%Data::isReadOnly() const
{}

bool %ClassName:c%Data::setData(const int ref, const QVariant &data, const int role)
{
    Q_UNUSED(ref);
    Q_UNUSED(data);
    Q_UNUSED(role);
    return true;
}

QVariant %ClassName:c%Data::data(const int ref, const int role) const
{
    return QVariant();
}

void %ClassName:c%Data::setStorableData(const QVariant &data)
{
    if (m_Form) {
    }
}

QVariant %ClassName:c%Data::storableData() const
{
    if (m_Form) {
    }
    return QVariant();
}

void %ClassName:c%Data::onValueChanged()
{
    Constants::executeOnValueChangedScript(m_FormItem);
    Q_EMIT dataChanged(0);
}
