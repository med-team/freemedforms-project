/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : %AuthorName%                                *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
@if "%Internal%" == "true"
#ifndef %PluginNamespace:u%_INTERNAL_%ClassName:u%_H
#define %PluginNamespace:u%_INTERNAL_%ClassName:u%_H
@else
#ifndef %PluginNamespace:u%_%ClassName:u%_H
#define %PluginNamespace:u%_%ClassName:u%_H
@endif

#include <formmanagerplugin/iformwidgetfactory.h>
#include <formmanagerplugin/iformitemdata.h>

/**
 * \file ./qtcreator_wizards/formitemwidget/formitemwidget.h
 * \author %AuthorName%
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace %PluginNamespace:c% {
namespace Internal {
@if "%Internal%" != "true"
} // namespace Internal
@endif

class %ClassName:c% : public Form::IFormWidget
{
    Q_OBJECT
public:
    explicit %ClassName:c%(Form::FormItem *formItem, QWidget *parent = 0);
    ~%ClassName:c%();

    void addWidgetToContainer(Form::IFormWidget *widget);
    bool isContainer() const;

    // Printing
    QString printableHtml(bool withValues = true) const;

public Q_SLOTS:
    void retranslate();

};

class %ClassName:c%Data : public Form::IFormItemData
{
    Q_OBJECT
public:
    %ClassName:c%Data(Form::FormItem *item, %ClassName:c% *formWidget);
    ~%ClassName:c%Data();
    void clear();

    Form::FormItem *parentItem() const {return m_FormItem;}
    bool isModified() const;
    void setModified(bool modified);

    void setReadOnly(bool readOnly);
    bool isReadOnly() const;

    // Use setData/Data for episode data
    bool setData(const int ref, const QVariant &data, const int role = Qt::EditRole);
    QVariant data(const int ref, const int role = Qt::DisplayRole) const;

    // Used storable data for forms
    void setStorableData(const QVariant &modified);
    QVariant storableData() const;

public Q_SLOTS:
    void onValueChanged();

private:
    Form::FormItem *m_FormItem;
    %ClassName:c% *m_Form;
    bool m_Modified;
    // m_OriginalData;
};


@if "%Internal%" == "true"
} // namespace Internal
@endif
} // namespace %PluginNamespace:c%

@if "%Internal%" == "true"
#endif // %PluginNamespace:u%_INTERNAL_%ClassName:u%_H
@else
#endif  // %PluginNamespace:u%_%ClassName:u%_H
@endif
