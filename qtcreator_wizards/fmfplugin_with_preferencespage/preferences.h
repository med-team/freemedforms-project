/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       %Author% <%AuthorEmail%>                             *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef %PluginName:u%_INTERNAL_%PluginName:u%PREFERENCES_H
#define %PluginName:u%_INTERNAL_%PluginName:u%PREFERENCES_H

#include <coreplugin/ioptionspage.h>

#include <QWidget>
#include <QPointer>

namespace Core {
class ISettings;
}

namespace %PluginName:c% {
namespace Internal {
namespace Ui {
class %PluginName:c%PreferencesWidget;
}

class %PluginName:c%PreferencesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit %PluginName:c%PreferencesWidget(QWidget *parent = 0);
    ~%PluginName:c%PreferencesWidget();

    void setDataToUi();
    QString searchKeywords() const;

    static void writeDefaultSettings(Core::ISettings *s);

public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);

private:
    void retranslateUi();
    void changeEvent(QEvent *e);

private:
    Ui::%PluginName:c%PreferencesWidget *ui;
};


class %PluginName:c%PreferencesPage : public Core::IOptionsPage
{
public:
    %PluginName:c%PreferencesPage(QObject *parent = 0);
    ~%PluginName:c%PreferencesPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const {return displayName();}
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    QString helpPage() {return QString();}

    static void writeDefaultSettings(Core::ISettings *s) {%PluginName:c%PreferencesWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);

private:
    QPointer<Internal::%PluginName:c%PreferencesWidget> m_Widget;
};


} // namespace Internal
} // namespace %PluginName:c%
#endif // %PluginName:u%_INTERNAL_%PluginName:u%PREFERENCES_H
