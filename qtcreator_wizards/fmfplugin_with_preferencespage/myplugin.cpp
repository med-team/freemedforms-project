/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: %Author% <%AuthorEmail%>                  *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "%PluginName:l%plugin.%CppHeaderSuffix%"
#include "%PluginName:l%constants.%CppHeaderSuffix%"

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/itheme.h>
#include <coreplugin/iuser.h>
#include <coreplugin/translators.h>

#include <utils/log.h>
#include <extensionsystem/pluginmanager.h>

#include <QtPlugin>
#include <QDebug>

using namespace %PluginName%::Internal;

static inline Core::IUser *user()  { return Core::ICore::instance()->user(); }
static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline void messageSplash(const QString &s) {theme()->messageSplashScreen(s); }

%PluginName%Plugin::%PluginName%Plugin()
{
    setObjectName("%PluginName%Plugin");
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating %PluginName%";

    Core::ICore::instance()->translators()->addNewTranslator("plugin_%PluginName:l%");


    m_prefPage = new %PluginName:c%PreferencesPage(this);
    addObject(m_prefPage);

    connect(Core::ICore::instance(), SIGNAL(coreAboutToClose()), this, SLOT(coreAboutToClose()));
}

%PluginName%Plugin::~%PluginName%Plugin()
{
}

bool %PluginName%Plugin::initialize(const QStringList &arguments, QString *errorString)
{
    Q_UNUSED(arguments)
    Q_UNUSED(errorString)

    if (Utils::Log::debugPluginsCreation()) {
        qWarning() << "creating %PluginName%";
    }






    return true;
}

void %PluginName%Plugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation()) {
        qWarning() << "%PluginName%::extensionsInitialized";
    }



    messageSplash(tr("Initializing %PluginName%..."));



    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));
    connect(Core::ICore::instance(), SIGNAL(coreOpened()), this, SLOT(postCoreInitialization()));
}

void %PluginName%Plugin::postCoreInitialization()
{
}

ExtensionSystem::IPlugin::ShutdownFlag %PluginName%Plugin::aboutToShutdown()
{
    return SynchronousShutdown;
}


void %PluginName%Plugin::coreAboutToClose()
{
}





Q_EXPORT_PLUGIN(%PluginName%Plugin)
