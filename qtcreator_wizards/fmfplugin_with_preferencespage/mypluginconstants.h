/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: %Author% <%AuthorEmail%>                  *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef %PluginName:u%CONSTANTS_%CppHeaderSuffix:u%
#define %PluginName:u%CONSTANTS_%CppHeaderSuffix:u%

namespace %PluginName% {
namespace Constants {

    const char * const ACTION_ID = "%PluginName%.Action";
    const char * const MENU_ID = "%PluginName%.Menu";

} // namespace %PluginName%
} // namespace Constants

#endif // %PluginName:u%CONSTANTS_%CppHeaderSuffix:u%
