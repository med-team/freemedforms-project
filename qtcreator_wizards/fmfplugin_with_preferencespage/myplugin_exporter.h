/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: %Author% <%AuthorEmail%>                  *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef %PluginName:u%_EXPORTER_%CppHeaderSuffix:u%
#define %PluginName:u%_EXPORTER_%CppHeaderSuffix:u%

#include <QtCore/QtGlobal>

#if defined(%PluginName:u%_LIBRARY)
#  define %PluginName:u%SHARED_EXPORT Q_DECL_EXPORT
#else
#  define %PluginName:u%SHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // %PluginName:u%_EXPORTER_H
