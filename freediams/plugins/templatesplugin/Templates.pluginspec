<plugin name="Templates" version="0.0.1" compatVersion="0.0.1">
    <vendor>FreeDiams</vendor>
    <copyright>(C) 2008-2017 by Eric MAEKER, MD</copyright>
    <license>See application's license</license>
    <category>Templates manager</category>
    <description>This plugin manages user's templates.</description>
    <url>http://www.freemedforms.com/</url>
    <dependencyList>
        <dependency name="Core" version="0.0.1"/>
    </dependencyList>
</plugin>
