<plugin name="MainWindow" version="0.0.1" compatVersion="0.0.1">
    <vendor>FreeDiams</vendor>
    <copyright>(C) 2008-2017 by Eric MAEKER, MD</copyright>
    <license>See application's license</license>
    <category>FreeDiams</category>
    <description></description>
    <url>http://www.freemedforms.com</url>
    <dependencyList>
        <dependency name="Core" version="0.0.1"/>
        <dependency name="Templates" version="0.0.1"/>
        <dependency name="DrugsBase" version="0.0.1"/>
        <dependency name="Drugs" version="0.0.1"/>
        <dependency name="Printer" version="0.0.1"/>
    </dependencyList>
</plugin>
