/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#include "mainwindowplugin.h"
#include "mainwindow.h"

#include <coreplugin/icore.h>
#include <coreplugin/translators.h>

#include <utils/log.h>

#include <QtPlugin>

#include <QDebug>

using namespace MainWin;
using namespace Internal;

MainWinPlugin::MainWinPlugin() :
        m_MainWindow(0)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating FREEDIAMS::MainWinPlugin";
    m_MainWindow = new MainWindow();
    Core::ICore::instance()->setMainWindow(m_MainWindow);
}

MainWinPlugin::~MainWinPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "MainWinPlugin::~MainWinPlugin()";
}

bool MainWinPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "MainWinPlugin::initialize";
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);

    Core::ICore::instance()->translators()->addNewTranslator("plugin_fdmainwindow");

    m_MainWindow->initialize(QStringList(),0);
    return true;
}

void MainWinPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "MainWinPlugin::extensionsInitialized";

    m_MainWindow->extensionsInitialized();
}

ExtensionSystem::IPlugin::ShutdownFlag MainWinPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    if (m_MainWindow->isVisible())
        m_MainWindow->hide();

    if (m_MainWindow) {
        delete m_MainWindow;
        m_MainWindow = 0;
    }
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(MainWinPlugin)
