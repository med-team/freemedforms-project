/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       Guillaume DENRY <guillaume.denry@gmail.com>                       *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FDMAINWIN_PLUGIN_H
#define FDMAINWIN_PLUGIN_H

#include <extensionsystem/iplugin.h>

#include <QtCore/QObject>

/**
 * \file ./freediams/plugins/mainwindowplugin/mainwindowplugin.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace MainWin {
class MainWindow;
namespace Internal {

class MainWinPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.freemedforms.FreeDiams.MainWindowPlugin" FILE "MainWindow.json")

public:
    MainWinPlugin();
    ~MainWinPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

private:
    MainWindow *m_MainWindow;
};

} // namespace Internal
}  // End MainWin

#endif  // End FDMAINWIN_PLUGIN_H
