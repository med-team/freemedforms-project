#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

TEMPLATE = lib
TARGET = MainWindow

DEFINES += FD_MAINWIN_LIBRARY
INCLUDEPATH += ../
DEPENDPATH += ../

include(../../../plugins/fmf_plugins.pri)
include(mainwindowplugin_dependencies.pri)


HEADERS = mainwindowplugin.h \
    mainwindow_exporter.h \
    mainwindow.h \
    medintux.h

SOURCES = mainwindowplugin.cpp \
    mainwindow.cpp \
    medintux.cpp

FORMS = mainwindow.ui

# include translations
TRANSLATION_NAME = fdmainwindow
include($${SOURCES_ROOT_PATH}/buildspecs/translations.pri)

OTHER_FILES = MainWindow.pluginspec

