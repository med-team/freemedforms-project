/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#ifndef FREEDIAMS_MEDINTUX_H
#define FREEDIAMS_MEDINTUX_H

namespace MainWin {
namespace Internal {

void configureMedinTux();

}  // End Internal
}  // End MainWin


#endif // FREEDIAMS_MEDINTUX_H
