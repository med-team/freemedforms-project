/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FREEDIAMS_PATIENT_H
#define FREEDIAMS_PATIENT_H

#include <coreplugin/core_exporter.h>
#include <coreplugin/ipatient.h>

#include <QVariant>
#include <QModelIndex>

/**
 * \file ./freediams/plugins/coreplugin/patient.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/
namespace Core {
namespace Internal {
class PatientPrivate;
}

class CORE_EXPORT Patient : public IPatient
{
    Q_OBJECT
public:
    Patient(QObject *parent = 0);
    ~Patient();

    void clear();
    bool has(const int ref) const;

    QModelIndex currentPatientIndex() const {return index(0,0);}

    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant data(int column) const;

    /** \todo remove this and use setData instead **/
    bool setValue(int ref, const QVariant &value);

    QString toXml() const;
    bool fromXml(const QString &xml);

    Core::IPatientBar *patientBar() const {return 0;}
    void hidePatientBar() {}
    void showPatientBar() {}
    bool isPatientBarVisible() const {return false;}

private:
    Internal::PatientPrivate *d;
};

}  // End Core

#endif // FREEDIAMS_PATIENT_H
