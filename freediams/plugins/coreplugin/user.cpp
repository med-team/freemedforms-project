/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#include "user.h"

#include <coreplugin/isettings.h>
#include <coreplugin/icore.h>
#include <coreplugin/constants_tokensandsettings.h>

#include <drugsbaseplugin/constants.h>

static inline Core::ISettings *settings() {return Core::ICore::instance()->settings();}

namespace Core {
namespace Constants {

    const char * const FREEDIAMS_DEFAULT_USER_UUID = "freediams.default.user";

}
}

using namespace Core;
using namespace Internal;

User::User(QObject *parent) :
        Core::IUser(parent)
{
}

User::~User()
{}

void User::clear()
{
}

bool User::has(const int ref) const
{
    if (ref >= 0 && ref < NumberOfColumns)
        return true;
    return false;
}


QVariant User::value(const int ref) const
{
    if (!has(ref))
        return QVariant();

    switch (ref) {
    case Id : return -1;
    case Uuid : return Constants::FREEDIAMS_DEFAULT_USER_UUID;;
    case Validity: return true;




    case PrescriptionHeader: return settings()->value(DrugsDB::Constants::S_USERHEADER);
    case PrescriptionFooter: return settings()->value(DrugsDB::Constants::S_USERFOOTER);
    case PrescriptionWatermark: return settings()->value(DrugsDB::Constants::S_WATERMARK_HTML);
    case PrescriptionWatermarkPresence: return settings()->value(DrugsDB::Constants::S_WATERMARKPRESENCE);
    case PrescriptionWatermarkAlignement: return settings()->value(DrugsDB::Constants::S_WATERMARKALIGNEMENT);

    case DrugsRights: return IUser::AllRights;
    case MedicalRights: return IUser::AllRights;

    case IsModified: settings()->sync(); return false;
    default: return QVariant();
    }

    return QVariant();
}

bool User::setValue(const int ref, const QVariant &value)
{
    Q_UNUSED(ref);
    Q_UNUSED(value);
    return true;
}

QString User::toXml() const
{
    return QString();
}

bool User::fromXml(const QString &xml)
{
    Q_UNUSED(xml);
    return true;
}


