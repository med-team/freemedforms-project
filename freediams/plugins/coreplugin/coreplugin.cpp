/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "coreplugin.h"
#include "coreimpl.h"

#include <extensionsystem/pluginmanager.h>
#include <utils/log.h>
#include <translationutils/constants.h>

#include <coreplugin/appaboutpage.h>
#include <coreplugin/dialogs/commonaboutpages.h>
#include <coreplugin/dialogs/commondebugpages.h>
#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/translators.h>
#include <coreplugin/dialogs/applicationgeneralpreferences.h>
#include <coreplugin/dialogs/networkpreferences.h>

#include <QtCore/QtPlugin>
#include <QDebug>

using namespace Core::Internal;

CorePlugin::CorePlugin() :
    m_CoreImpl(new CoreImpl(this)),
    prefPage(0),
    proxyPage(0)
{
}

CorePlugin::~CorePlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool CorePlugin::initialize(const QStringList &arguments, QString *errorMessage)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "CorePlugin::initialize";
    return m_CoreImpl->initialize(arguments,errorMessage);
}

void CorePlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "CorePlugin::extensionsInitialized";
    m_CoreImpl->extensionsInitialized();
    addAutoReleasedObject(new AppAboutPage(this));
    addAutoReleasedObject(new TeamAboutPage(this));
    addAutoReleasedObject(new LicenseAboutPage(this));
    addAutoReleasedObject(new BuildAboutPage(this));
    addAutoReleasedObject(new CommandLineAboutPage(this));
    addAutoReleasedObject(new LogErrorDebugPage(this));
    addAutoReleasedObject(new LogMessageDebugPage(this));
    addAutoReleasedObject(new SettingDebugPage(this));
    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));

    prefPage = new ApplicationGeneralPreferencesPage(this);
    prefPage->checkSettingsValidity();
    addObject(prefPage);
    proxyPage = new ProxyPreferencesPage(this);
    proxyPage->checkSettingsValidity();
    addObject(proxyPage);

    Core::ICore::instance()->translators()->addNewTranslator(Trans::Constants::CONSTANTS_TRANSLATOR_NAME);
    Core::ICore::instance()->translators()->addNewTranslator("lib_utils");
    Core::ICore::instance()->translators()->addNewTranslator("lib_medicalutils");
    Core::ICore::instance()->translators()->addNewTranslator("plugin_fdcore");
}

void CorePlugin::remoteArgument(const QString &arg)
{
    qWarning() << arg;
    Q_UNUSED(arg);
}

ExtensionSystem::IPlugin::ShutdownFlag CorePlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;

    if (prefPage) {
        removeObject(prefPage);
        delete prefPage; prefPage=0;
    }
    if (proxyPage) {
        removeObject(proxyPage);
        delete proxyPage; proxyPage=0;
    }
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(CorePlugin)
