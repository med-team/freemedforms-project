/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef APPABOUTPAGE_H
#define APPABOUTPAGE_H

#include <coreplugin/iaboutpage.h>

namespace Core {
namespace Internal {

class AppAboutPage : public IAboutPage
{
    Q_OBJECT
public:
    AppAboutPage(QObject *parent);
    ~AppAboutPage();

    QString id() const       { return objectName();   }
    QString displayName() const     { return tr("General"); }
    QString category() const { return tr("Application"); }
    int sortIndex() const    { return 1;}

    QWidget *createPage(QWidget *parent = 0);
};

class CommandLineAboutPage : public IAboutPage
{
    Q_OBJECT
public:
    CommandLineAboutPage(QObject *parent) : IAboutPage(parent) {}
    ~CommandLineAboutPage() {}

    QString id() const       { return objectName();   }
    QString displayName() const     { return tr("Command line"); }
    QString category() const { return tr("Application"); }
    int sortIndex() const    { return 50;}

    QWidget *createPage(QWidget *parent = 0);
};

} // End Internal
} // End Core

#endif // APPABOUTPAGE_H
