<plugin name="Core" version="0.0.1" compatVersion="0.0.1">
    <vendor>FreeDiams</vendor>
    <copyright>(C) 2008-2017 by Eric MAEKER, MD</copyright>
    <license>See application's license</license>
    <category>FreeDiams</category>
    <description>The main Core plugin for FreeMedForms.</description>
    <url>http://www.freemedforms.com</url>
</plugin>
