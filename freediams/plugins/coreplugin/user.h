/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#ifndef USER_H
#define USER_H

#include <coreplugin/core_exporter.h>
#include <coreplugin/iuser.h>

namespace Core {
namespace Internal {

class User : public Core::IUser
{
    Q_OBJECT
public:
    User(QObject *parent);
    ~User();

    void clear();
    bool has(const int ref) const;
    virtual bool hasCurrentUser() const {return true;}

    QString fullNameOfUser(const QVariant &uid) {Q_UNUSED(uid); return QString();}

    QVariant value(const int ref) const;
    bool setValue(const int ref, const QVariant &value);

    bool saveChanges() {return false;}

    QString toXml() const ;
    bool fromXml(const QString &xml);

};

}
}

#endif // USER_H
