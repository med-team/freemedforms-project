/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FREEDIAMS_COMMANDLINEPARSER_H
#define FREEDIAMS_COMMANDLINEPARSER_H

#include <coreplugin/icommandline.h>

#include <QString>
#include <QVariant>

namespace Core {
class Patient;
namespace Internal {
class CommandLinePrivate;
}

class CommandLine : public Core::ICommandLine
{
public:
    enum Param {
        CL_MedinTux = 0,
        CL_EMR_Name,
        CL_EMR_Uid,
        CL_SelectionOnly,
        CL_DrugsDatabaseUid,
        CL_ExchangeOutFile,
        CL_ExchangeOutFileFormat,
        CL_ExchangeInFile,
        CL_PatientName,
        CL_PatientOtherNames,
        CL_PatientFirstname,
        CL_PatientUid,
        CL_PatientGender,
        CL_DateOfBirth,
        CL_WeightInGrams,
        CL_HeightInCentimeters,
        CL_CrCl,
        CL_CrCl_Unit,
        CL_Creatinine,
        CL_Creatinine_Unit,
        CL_DrugsAllergies,
        CL_InnAllergies,
        CL_AtcAllergies,
        CL_DrugsIntolerances,
        CL_InnIntolerances,
        CL_AtcIntolerances,
        CL_ICD10Diseases,
        CL_Chrono,
        CL_TransmitDosage,
        CL_ConfigFile,
        CL_RunningUnderWine,
        CL_BlockPatientDatas,
        CL_MaxParam
    };

    CommandLine();
    ~CommandLine();

    QVariant value(int param, const QVariant &def = QVariant()) const;
    QString paramName(int param) const;

    void feedPatientDatas(Core::Patient *patient);

private:
    Internal::CommandLinePrivate *d;
};

}

#endif // COMMANDLINEPARSER_H
