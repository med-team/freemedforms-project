<plugin name="PadTools" version="0.0.1" compatVersion="0.0.1">
    <vendor>FreeDiams</vendor>
    <copyright>(C) 2008-2017 by Eric MAEKER, MD</copyright>
    <license>See application's license</license>
    <category>Token manager.</category>
    <description>Improved tokenmanager for FreeDiams.</description>
    <url>http://www.freemedforms.com</url>
    <dependencyList>
        <dependency name="Core" version="0.0.1"/>
    </dependencyList>
</plugin>
