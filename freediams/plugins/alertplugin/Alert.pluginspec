<plugin name="Alert" version="0.0.1" compatVersion="0.0.1">
    <vendor>FreeDiams</vendor>
    <copyright>(C) 2008-2017 by Eric MAEKER, MD</copyright>
    <license>See application's license</license>
    <description>Alert management for the FreeMedForms suite.</description>
    <category>Alerts</category>
    <url>http://www.freemedforms.com/</url>
    <dependencyList>
       <dependency name="Core" version="0.0.1"/>
    </dependencyList>
</plugin>
