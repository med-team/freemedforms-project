# FreeMedForms
# Copyright (C) 2008-2017 by Eric MAEKER
# https://freemedforms.com


### What is FreeMedForms?

FreeMedForms is a free and open source software community building healthcare
applications:

- Electronic Medical Record / Electronic Health Record: FreeMedForms EMR
- Pharmaceutical drug prescription assistant: FreeDiams
- ICD10 coding assistant: FreeICD
- French specific coding assistant: FreeDRC (in partnership with the SMFG)

All official stable binaries are freely available from this page:
[Download FreeMedForms applications]

FreeMedForms and derivatives are coded in C++ / Qt5.

Supported OS:
- Mac OS
- GNU/Linux: Debian, Ubuntu
- Windows from 7 SP1 to latest

### Where can I find informations about FreeMedForms?

- [https://freemedforms.com]
- Stable source and issues are located on [GitHub]
- Unstable source and full code documentation are only available for approved developers
- [Build instructions]

### Building FreeMedForms from the source

You can build FreeMedForms from source. Ask the association [asso.freemedforms.com] to get the code documentation and build process documentation.

#### Requirements:

- You need to install the [Qt] lib/headers
- You need to install the [QuaZip] lib/headers or code in the contrib path
- You need to install the [OpenCV] lib/headers or code in the contrib path

You should be warn that the free DataPack server is only own by the non-profit french association: [asso.freemedforms.com]. The datapacks downloaded from the server are only compatible with the official stable binaries available from here: [Download FreeMedForms applications]. So applications build from source are not compatible with the free datapack server. You have to provide your own datapack server and update the URL in the code.

### How to contribute to FreeMedForms?

All contributors are welcome.

Information from the official website: [https://freemedforms.com]

Feel free to ask any question
- Mailing list (English or French): <freemedforms@googlegroups.com>


### Can I make a donation?

Yes! Go to our [Donation page].
Your money will go to the french non-profit organization [asso.freemedforms.com].

Donating will speed up the development process

[Download FreeMedForms applications]: https://freemedforms.com/en/downloads
[https://freemedforms.com]: https://freemedforms.com/
[GitHub]: https://github.com/EricMaeker/freemedforms
[Build instructions]: https://freemedforms.com/en/code_doc
[Participez]: https://freemedforms.com/fr/contribute
[Contribute]: https://freemedforms.com/en/contribute
[Donation page]: https://freemedforms.com/en/donation
[asso.freemedforms.com]: https://freemedforms.com/en/asso/start
[QuaZip]: http://quazip.sourceforge.net/
[OpenCV]: http://opencv.org/
[Qt]: https://www.qt.io/
