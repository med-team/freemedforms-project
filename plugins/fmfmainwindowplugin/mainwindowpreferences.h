/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef MAINWINDOWPREFERENCES_H
#define MAINWINDOWPREFERENCES_H

#include <coreplugin/ioptionspage.h>

#include <fmfmainwindowplugin/virtualdatabasepreferences.h>

#include <QPointer>
#include <QObject>

/**
 * \file ./plugins/fmfmainwindowplugin/mainwindowpreferences.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;
}

namespace MainWin {
namespace Internal {

class VirtualPatientBasePage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    VirtualPatientBasePage(QObject *parent = 0);
    ~VirtualPatientBasePage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults() {VirtualDatabasePreferences::writeDefaultSettings();}
    void checkSettingsValidity() {}
    void apply() {}
    void finish();

    QString helpPage() {return QString();}

    static void writeDefaultSettings(Core::ISettings *s) {VirtualDatabasePreferences::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);
private:
    QPointer<VirtualDatabasePreferences> m_Widget;
};


}  // End Internal
}  // End MainWin

#endif // MAINWINDOWPREFERENCES_H
