/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef VIRTUALDATABASEPREFERENCES_H
#define VIRTUALDATABASEPREFERENCES_H

#include <QWidget>

#include "ui_virtualbasepage.h"

/**
 * \file ./plugins/fmfmainwindowplugin/virtualdatabasepreferences.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;
}

namespace MainWin {
namespace Internal {

class VirtualDatabasePreferences : public QWidget, private Ui::VirtualDatabasePreferences
{
    Q_OBJECT
    Q_DISABLE_COPY(VirtualDatabasePreferences)

public:
    VirtualDatabasePreferences(QWidget *parent);

    static void writeDefaultSettings(Core::ISettings *s = 0);

public Q_SLOTS:
    void saveToSettings(Core::ISettings *) {}
    void on_populateDb_clicked();
    void on_populateEpisodes_clicked();
    void on_populateUsers_clicked();

protected:
    virtual void changeEvent(QEvent *e);
};

}  // End namespace MainWin
}  // End namespace Internal

#endif // VIRTUALDATABASEPREFERENCES_H
