/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "mainwindowplugin.h"
#include "mainwindow.h"
#include "mainwindowpreferences.h"

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/translators.h>

#include <utils/log.h>

#include <QtCore/QtPlugin>

#include <QDebug>

using namespace MainWin;
using namespace Internal;

static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline void messageSplash(const QString &s) {theme()->messageSplashScreen(s); }

MainWinPlugin::MainWinPlugin() :
    m_MainWindow(0),
    prefPage(0),
    virtualBasePage(0)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating FREEMEDFORMS::MainWinPlugin";

    Core::ICore::instance()->translators()->addNewTranslator("plugin_fmfmainwindow");

    m_MainWindow = new MainWindow;
    Core::ICore::instance()->setMainWindow(m_MainWindow);
    m_MainWindow->init();

    virtualBasePage = new Internal::VirtualPatientBasePage();
    addObject(virtualBasePage);
}

MainWinPlugin::~MainWinPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool MainWinPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "FREEMEDFORMS::MainWinPlugin::initialize";

    Q_UNUSED(arguments);
    Q_UNUSED(errorString);

    messageSplash(tr("Initializing main window plugin..."));

    m_MainWindow->initialize(arguments, errorString);
    return true;
}

void MainWinPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "FREEMEDFORMS::MainWinPlugin::extensionsInitialized";

    messageSplash(tr("Initializing main window plugin..."));
    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));

    virtualBasePage->checkSettingsValidity();

    m_MainWindow->extensionsInitialized();
}

ExtensionSystem::IPlugin::ShutdownFlag MainWinPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    if (virtualBasePage) {
        removeObject(virtualBasePage);
        delete virtualBasePage; virtualBasePage=0;
    }

    if (m_MainWindow->isVisible())
        m_MainWindow->hide();

    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(MainWinPlugin)
