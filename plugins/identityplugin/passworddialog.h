/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IDENTITY_PASSWORDDIALOG_H
#define IDENTITY_PASSWORDDIALOG_H

#include <QDialog>

/**
 * \file ./plugins/identityplugin/passworddialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Identity {
namespace Internal {
namespace Ui {
class  PasswordDialog;
}  // namespace ui

class PasswordDialog : public QDialog
{
    Q_OBJECT
    Q_DISABLE_COPY(PasswordDialog)

public:
    explicit PasswordDialog(QWidget *parent = 0);

    void setOldCryptedPassword(const QString &crypted);

    bool canGetNewPassword() const;
    QString cryptedPassword() const;
    QString uncryptedPassword() const;

//    bool applyChanges(UserModel *model, int userRow) const;

private Q_SLOTS:
    void checkControlPassword(const QString &text);
    void checkNewPassword(const QString &text);

private:
    void done(int result);

private:
    Internal::Ui::PasswordDialog *m_ui;
    bool m_AllIsGood;
    QString m_OldCryptedPass;
    QString m_CryptedNewPass;
    QDialog *m_Parent;
};
}  // namespace Internal
}  // namespace UserPlugin

#endif // IDENTITY_PASSWORDDIALOG_H
