/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Eric Maeker <eric.maeker@gmail.com>                  *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "identityplugin.h"
#include "identityconstants.h"

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/itheme.h>
#include <coreplugin/iuser.h>
#include <coreplugin/translators.h>

#include <utils/log.h>
#include <extensionsystem/pluginmanager.h>

#include <QtPlugin>
#include <QDebug>

using namespace Identity::Internal;

static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline void messageSplash(const QString &s) {theme()->messageSplashScreen(s); }

IdentityPlugin::IdentityPlugin() :
    ExtensionSystem::IPlugin()
{
    setObjectName("IdentityPlugin");
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating Identity";

    Core::ICore::instance()->translators()->addNewTranslator("plugin_identity");



    connect(Core::ICore::instance(), SIGNAL(coreOpened()), this, SLOT(postCoreInitialization()));
    connect(Core::ICore::instance(), SIGNAL(coreAboutToClose()), this, SLOT(coreAboutToClose()));
}

IdentityPlugin::~IdentityPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool IdentityPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    Q_UNUSED(arguments)
    Q_UNUSED(errorString)
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "IdentityPlugin::initialize";






    return true;
}

void IdentityPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation()) {
        qWarning() << "IdentityPlugin::extensionsInitialized";
    }



    messageSplash(tr("Initializing Identity..."));



    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));
    connect(Core::ICore::instance(), SIGNAL(coreOpened()), this, SLOT(postCoreInitialization()));
}

void IdentityPlugin::postCoreInitialization()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}



void IdentityPlugin::coreAboutToClose()
{
}

Q_EXPORT_PLUGIN(IdentityPlugin)
