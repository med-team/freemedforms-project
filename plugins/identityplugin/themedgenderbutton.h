/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer:  Christian A. Reiter <christian.a.reiter@gmail.com>   *
 *  Contributors:                                                          *
 *       Eric Maeker                                                       *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IDENTITY_THEMEDGENDERBUTTON_H
#define IDENTITY_THEMEDGENDERBUTTON_H

#include <QToolButton>

/**
 * \file ./plugins/identityplugin/themedgenderbutton.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Identity {
namespace Internal {
class ThemedGenderButton : public QToolButton
{
    Q_OBJECT
    Q_PROPERTY(QPixmap pixmap READ pixmap WRITE setPixmap NOTIFY pixmapChanged USER true)

public:
    explicit ThemedGenderButton(QWidget* parent = 0);

    QPixmap pixmap() const;
    void setDefaultAction(QAction *action);
    QAction* defaultAction() const;
    QAction* deletePhotoAction() const;

public Q_SLOTS:
    void setPixmap(const QPixmap &pixmap);
    void clearPixmap();
    void setGenderImage(int genderIndex);

Q_SIGNALS:
    void pixmapChanged(const QPixmap &pix);

private:
    QPixmap m_pixmap;
    QAction* m_deletePhotoAction;
    QAction *m_separator;
    QAction *m_defaultAction;
    bool m_isDefaultGender;
};

} // namespace Internal
} // namespace Identity

#endif // IDENTITY_THEMEDGENDERBUTTON_H
