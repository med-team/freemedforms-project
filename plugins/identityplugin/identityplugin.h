/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Eric Maeker <eric.maeker@gmail.com>                  *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IDENTITY_H
#define IDENTITY_H

#include "identity_exporter.h"
#include "identitypreferences.h"

#include <extensionsystem/iplugin.h>

/**
 * \file ./plugins/identityplugin/identityplugin.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Identity {
namespace Internal {

class IdentityPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.freemedforms.FreeMedForms.IdentityPlugin" FILE "Identity.json")

public:
    IdentityPlugin();
    ~IdentityPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    //    ShutdownFlag aboutToShutdown();

private Q_SLOTS:
    void postCoreInitialization();
    void coreAboutToClose();
    //    void triggerAction();

private:
//    IdentityPreferencesPage *m_prefPage;
};

} // namespace Internal
} // namespace Identity

#endif // IDENTITY_H

