/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Christian A. Reiter <christian.a.reiter@gmail.com>    *
 *  Contributors:                                                          *
 *       Eric Maeker                                                       *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/

/*!
 * \class Identity::Internal::ThemedGenderButton
 * \brief This class provides a QPushButton with a displayed Pixmap on it.
 * \internal
 *
 * \property Pixmap The normal Qt QPushButton only can store a QIcon and has
 * no pixmap property. This
 * can be unconvenient when using the button as display widget in a MVC pattern as
 * widget - a QDataWidgetMapper doesn't know how to handle a QPushbutton.
 * Here comes the Identity::Internal::ThemedGenderButton. \n
 * It still displays a QIcon, but stores a QPixmap underneath.
 * It also provides a \e pixmap property for easy interacting with a QDataWidgetMapper.
 *
 * Theme connection: \n
 * This button is automatically connected to the application central theme and
 * correctly manages the Gender Pixmap.
 */

#include "themedgenderbutton.h"
#include <coreplugin/itheme.h>
#include <coreplugin/icore.h>
#include <coreplugin/constants.h>
#include <coreplugin/itheme.h>

#include <QMenu>

#include <QDebug>

using namespace Identity;
using namespace Internal;

static inline Core::ITheme *theme() {return Core::ICore::instance()->theme();}

/*!
 * Default constructor of the ThemedGenderButton class.
 * \param parent parent of the button, passed through to QToolButton.
 * Just calls the QPushButton constructor and initializes the internal Pixmap.
 */
ThemedGenderButton::ThemedGenderButton(QWidget *parent) :
    QToolButton(parent),
    m_pixmap(QPixmap()),
    m_deletePhotoAction(0),
    m_separator(0),
    m_defaultAction(0),
    m_isDefaultGender(false)
{
    setContextMenuPolicy(Qt::ActionsContextMenu);

    m_deletePhotoAction = new QAction(Core::ICore::instance()->theme()->icon(Core::Constants::ICONREMOVE),
                                      tr("Delete photo"), this);
    connect(m_deletePhotoAction, SIGNAL(triggered()),this, SLOT(clearPixmap()));
    addAction(m_deletePhotoAction);
    m_deletePhotoAction->setEnabled(false);

    m_separator = new QAction(this);
    m_separator->setSeparator(true);
    addAction(m_separator);
}

void ThemedGenderButton::setPixmap(const QPixmap &pixmap)
{

    setIcon(QIcon(pixmap));
    m_isDefaultGender = false;
    m_pixmap = pixmap;
    m_deletePhotoAction->setEnabled(!pixmap.isNull());
    Q_EMIT pixmapChanged(pixmap);
}

QPixmap ThemedGenderButton::pixmap() const
{
    if (m_isDefaultGender)
        return QPixmap();
    else
        return m_pixmap;
}

void ThemedGenderButton::setDefaultAction(QAction *action)
{
    if (action == m_deletePhotoAction)
        return;
    if (action == m_separator)  // eh, nearly impossible, are we paranoid?
        return;

    if (actions().count() == 3) {
        m_defaultAction = actions().first();
        return;
    }

    if (actions().contains(action))
        m_defaultAction = action;
}

QAction *ThemedGenderButton::defaultAction() const
{
    return m_defaultAction;
}

QAction *ThemedGenderButton::deletePhotoAction() const
{
    return m_deletePhotoAction;
}

void ThemedGenderButton::clearPixmap()
{
    setPixmap(QPixmap());
    m_deletePhotoAction->setEnabled(false);
}


void ThemedGenderButton::setGenderImage(int genderIndex)
{
    if (!m_pixmap.isNull() && !m_isDefaultGender)
        return;
    QPixmap genderPix;
    genderPix = theme()->defaultGenderPixmap(genderIndex);
    setPixmap(genderPix);
    m_isDefaultGender = true;
    m_deletePhotoAction->setEnabled(false);
}
