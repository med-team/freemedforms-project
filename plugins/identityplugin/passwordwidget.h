/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IDENTITY_INTERNAL_PASSWORDWIDGET_H
#define IDENTITY_INTERNAL_PASSWORDWIDGET_H

#include <utils/widgets/detailswidget.h>

QT_BEGIN_NAMESPACE
class QLineEdit;
QT_END_NAMESPACE

/**
 * \file ./plugins/identityplugin/passwordwidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Identity {
namespace Internal {
class PasswordWidgetPrivate;

class PasswordWidget : public Utils::DetailsWidget
{
    Q_OBJECT
    Q_PROPERTY(QString uncryptedPassword READ uncryptedPassword WRITE setUncryptedPassword RESET resetUncryptedPassword NOTIFY uncryptedPasswordChanged)
    Q_PROPERTY(QString cryptedPassword READ cryptedPassword WRITE setCryptedPassword RESET resetCryptedPassword NOTIFY cryptedPasswordChanged)

public:
    explicit PasswordWidget(QWidget *parent = 0);
    ~PasswordWidget();
    bool initialize();
    bool isPasswordValidAndConfirmed();
    void setReadOnly(bool readonly);
    QLineEdit *loginEditor() const;

Q_SIGNALS:
    void uncryptedPasswordChanged(const QString &password);
    void cryptedPasswordChanged(const QString &password);

public Q_SLOTS:
    void clear();

    void resetUncryptedPassword();
    QString uncryptedPassword() const;
    void setUncryptedPassword(const QString &password);

    void resetCryptedPassword();
    QString cryptedPassword() const;
    void setCryptedPassword(const QString &password);

private Q_SLOTS:
    void onChangeOrSetPasswordClicked();
    void onLoginChanged(const QString &login);

private:
    Internal::PasswordWidgetPrivate *d;
};

} // namespace Internal
} // namespace Identity

#endif // IDENTITY_INTERNAL_PASSWORDWIDGET_H

