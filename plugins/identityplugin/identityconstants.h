/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Eric Maeker <eric.maeker@gmail.com>                  *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IDENTITYCONSTANTS_H
#define IDENTITYCONSTANTS_H

namespace Identity {
namespace Constants {

const char * const ACTION_ID = "Identity.Action";
const char * const MENU_ID = "Identity.Menu";

const char* const PASSWORD_SUMMARY_NOLOGIN_NOPASSWORD = QT_TRANSLATE_NOOP("UserPlugin", "Please choose login and password");
const char* const PASSWORD_SUMMARY_LOGIN_NOPASSWORD   = QT_TRANSLATE_NOOP("UserPlugin", "Login set, but password not");
const char* const PASSWORD_SUMMARY_LOGIN_PASSWORD     = QT_TRANSLATE_NOOP("UserPlugin", "Login and password are set");

const char* const UNCRYPTED_PASSWORD_AVAILABLE        = QT_TRANSLATE_NOOP("UserPlugin", "Uncrypted password available");
const char* const CRYPTED_PASSWORD_AVAILABLE          = QT_TRANSLATE_NOOP("UserPlugin", "Crypted password set");

} // namespace Identity
} // namespace Constants

#endif // IDENTITYCONSTANTS_H

