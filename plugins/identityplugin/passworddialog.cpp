/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "passworddialog.h"

#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/constants_icons.h>

#include <utils/log.h>
#include <utils/global.h>
#include <utils/passwordandlogin.h>
#include <utils/widgets/lineeditechoswitcher.h>
#include <translationutils/constanttranslations.h>

#include "ui_passworddialog.h"

#include <QDebug>

using namespace Identity;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Core::ITheme *theme() {return Core::ICore::instance()->theme();}

PasswordDialog::PasswordDialog(QWidget *parent) :
    QDialog(parent),
    m_ui(new Internal::Ui::PasswordDialog()),
    m_AllIsGood(false)
{
    m_ui->setupUi(this);
    m_ui->newPass->setIcon(theme()->icon(Core::Constants::ICONEYES));
    m_ui->newControl->setIcon(theme()->icon(Core::Constants::ICONEYES));
    m_ui->oldPass->setIcon(theme()->icon(Core::Constants::ICONEYES));
    m_ui->newPass->toogleEchoMode();
    m_ui->newControl->toogleEchoMode();
    m_ui->oldPass->toogleEchoMode();

    connect(m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    m_ui->oldPass->setFocus();

    m_ui->label->setText(tkTr(Trans::Constants::SET_PASSWORD));
    setWindowTitle(tkTr(Trans::Constants::SET_PASSWORD));

    m_ui->oldLabel->hide();
    m_ui->oldPass->hide();
    m_ui->newControl->hide();
    m_ui->labelControlPassword->hide();

    connect(m_ui->newControl, SIGNAL(textChanged(QString)), this, SLOT(checkControlPassword(QString)));
    connect(m_ui->newPass, SIGNAL(textChanged(QString)), this, SLOT(checkNewPassword(QString)));
    checkNewPassword("");
}

void PasswordDialog::checkControlPassword(const QString &text)
{
    if (text == m_ui->newPass->text()) {
        m_ui->labelControlPassword->setStyleSheet("color:black");
    } else {
        m_ui->labelControlPassword->setStyleSheet("color:red");
    }
}

void PasswordDialog::checkNewPassword(const QString &text)
{
    if (text.size() >= 5) {
        m_ui->labelNewPassword->setStyleSheet("color:black");
        m_ui->labelNewPassword->setToolTip("");
        m_ui->newPass->setToolTip("");
    } else {
        m_ui->labelNewPassword->setStyleSheet("color:red");
        m_ui->labelNewPassword->setToolTip(tr("Password must have at least 5 chars."));
        m_ui->newPass->setToolTip(tr("Password must have at least 5 chars."));
    }
    checkControlPassword(m_ui->newControl->text());
}

void PasswordDialog::setOldCryptedPassword(const QString &crypted)
{
    m_OldCryptedPass = crypted;
    m_ui->label->setText(tkTr(Trans::Constants::CHANGE_PASSWORD));
    setWindowTitle(tkTr(Trans::Constants::CHANGE_PASSWORD));
    m_ui->oldLabel->show();
    m_ui->labelNewPassword->show();
    m_ui->labelControlPassword->show();
    m_ui->oldPass->show();
    m_ui->newPass->show();
    m_ui->newControl->show();
}

bool PasswordDialog::canGetNewPassword() const
{
    return m_AllIsGood;
}

QString PasswordDialog::cryptedPassword() const
{
    if (m_AllIsGood)
        return m_CryptedNewPass;
    return QString::null;
}

QString PasswordDialog::uncryptedPassword() const
{
    if (m_AllIsGood)
        return m_ui->newPass->text();
    return QString::null;
}



void PasswordDialog::done(int result)
{
    if (result == Rejected) {
        m_ui->newPass->text().clear();
        QDialog::done(Rejected);
        return;
    }

    if (m_ui->newPass->text().size() < 5)
        return;

    Utils::PasswordCrypter crypter;
    if (m_OldCryptedPass.isEmpty()) {
        m_AllIsGood = true;
        m_CryptedNewPass = crypter.cryptPassword(m_ui->newPass->text());
        QDialog::done(result);
    } else {
        bool oldPassCorrect = crypter.checkPassword(m_ui->oldPass->text(), m_OldCryptedPass);
        if (oldPassCorrect &&
                (m_ui->newPass->text() == m_ui->newControl->text())) {
            m_AllIsGood = true;
            m_CryptedNewPass = crypter.cryptPassword(m_ui->newPass->text());
            QDialog::done(result);
        } else {
            m_AllIsGood = false;
            QString info;
            if (oldPassCorrect)
                info = tr("The old password is not correct. Please retry with the correct password.");
            else
                info = tr("Wrong password confirmation.");
            Utils::warningMessageBox(tr("Password can not be change."),
                                     info, "", windowTitle());
            QDialog::done(Rejected);
        }
    }
}
