/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Eric Maeker <eric.maeker@gmail.com>                  *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IDENTITY_EXPORTER_H
#define IDENTITY_EXPORTER_H

#include <QtCore/QtGlobal>

#if defined(IDENTITY_LIBRARY)
#  define IDENTITYSHARED_EXPORT Q_DECL_EXPORT
#else
#  define IDENTITYSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // IDENTITY_EXPORTER_H

