<plugin name="Core" version="0.0.1" compatVersion="0.0.1">
    <vendor>FreeMedForms</vendor>
    <copyright>(C) 2008-2017 by Eric MAEKER, MD</copyright>
    <category>FreeMedForms</category>
    <license>See application's license</license>
    <description>The core of FreeMedForms.</description>
    <url>http://www.freemedforms.com/</url>
</plugin>
