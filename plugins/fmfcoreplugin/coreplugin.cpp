/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "coreplugin.h"
#include "coreimpl.h"
#include "sqlitedatabasepathpreferences.h"

#include <utils/log.h>
#include <utils/databaseconnector.h>
#include <extensionsystem/pluginmanager.h>

#include <fmfcoreplugin/appaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/isettings.h>
#include <coreplugin/dialogs/commonaboutpages.h>
#include <coreplugin/dialogs/commondebugpages.h>
#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/translators.h>
#include <coreplugin/dialogs/applicationgeneralpreferences.h>
#include <coreplugin/dialogs/networkpreferences.h>

#include <QtPlugin>

using namespace Core;
using namespace Internal;

static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline Core::ISettings *settings() { return Core::ICore::instance()->settings(); }
static inline void messageSplash(const QString &s) {theme()->messageSplashScreen(s); }

CorePlugin::CorePlugin() :
    m_CoreImpl(0),
    prefPage(0),
    proxyPage(0)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating CorePlugin";
    m_CoreImpl =  new CoreImpl(this);

    prefPage = new ApplicationGeneralPreferencesPage(this);
    addObject(prefPage);
    proxyPage = new ProxyPreferencesPage(this);
    addObject(proxyPage);
}

CorePlugin::~CorePlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool CorePlugin::initialize(const QStringList &arguments, QString *errorMessage)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "CorePlugin::initialize";

    messageSplash(tr("Initializing core plugin..."));

    return m_CoreImpl->initialize(arguments,errorMessage);
}

void CorePlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "CorePlugin::extensionsInitialized";

    messageSplash(tr("Initializing core plugin..."));

    this->addAutoReleasedObject(new AppAboutPage(this));
    this->addAutoReleasedObject(new TeamAboutPage(this));
    this->addAutoReleasedObject(new LicenseAboutPage(this));
    this->addAutoReleasedObject(new BuildAboutPage(this));

    this->addAutoReleasedObject(new LogErrorDebugPage(this));
    this->addAutoReleasedObject(new LogMessageDebugPage(this));
    this->addAutoReleasedObject(new SettingDebugPage(this));

    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));

    prefPage->checkSettingsValidity();
    proxyPage->checkSettingsValidity();
    m_CoreImpl->settings()->sync();

    m_CoreImpl->extensionsInitialized();

    if (settings()->databaseConnector().driver() == Utils::Database::SQLite)
        this->addAutoReleasedObject(new SqliteDatabasePathPage(this));
}

void CorePlugin::remoteArgument(const QString& arg)
{
    Q_UNUSED(arg);
}

ExtensionSystem::IPlugin::ShutdownFlag CorePlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    if (prefPage) {
        removeObject(prefPage);
        delete prefPage; prefPage=0;
    }
    if (proxyPage) {
        removeObject(proxyPage);
        delete proxyPage; proxyPage=0;
    }
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(CorePlugin)
