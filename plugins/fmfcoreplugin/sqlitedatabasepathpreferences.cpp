/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "sqlitedatabasepathpreferences.h"
#include "ui_sqlitedatabasepathpreferences.h"

#include <utils/log.h>
#include <utils/global.h>
#include <utils/databaseconnector.h>
#include <translationutils/constants.h>
#include <translationutils/trans_msgerror.h>

#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/constants_menus.h>

using namespace Core;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Core::ISettings *settings() { return Core::ICore::instance()->settings(); }

SqliteDatabasePathPage::SqliteDatabasePathPage(QObject *parent) :
    IOptionsPage(parent),
    m_Widget(0)
{
    setObjectName("SqliteDatabasePathPage");
}

SqliteDatabasePathPage::~SqliteDatabasePathPage()
{
    if (m_Widget)
        delete m_Widget;
    m_Widget = 0;
}

QString SqliteDatabasePathPage::id() const
{
    return objectName();
}

QString SqliteDatabasePathPage::displayName() const
{
    return tr("Database path");
}

QString SqliteDatabasePathPage::category() const
{
    return tkTr(Trans::Constants::GENERAL_PREFERENCES).remove("&");
}

QString SqliteDatabasePathPage::title() const
{
    return tr("Database path");
}

int SqliteDatabasePathPage::sortIndex() const
{
    return Core::Constants::OPTIONINDEX_MAIN + 100;
}

void SqliteDatabasePathPage::finish()
{
    delete m_Widget;
}

QWidget *SqliteDatabasePathPage::createPage(QWidget *parent)
{
    if (m_Widget)
        delete m_Widget;
    m_Widget = new SqliteDatabasePathWidget(parent);
    return m_Widget;
}

SqliteDatabasePathWidget::SqliteDatabasePathWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SqliteDatabasePathWidget)
{
    ui->setupUi(this);
    ui->path->setPromptDialogTitle(tr("Select a path"));
    ui->path->setExpectedKind(Utils::PathChooser::Directory);
    ui->path->setPath(settings()->databaseConnector().absPathToSqliteReadWriteDatabase());
    connect(ui->moveDatabase, SIGNAL(pressed()), this, SLOT(onMoveDatabaseRequested()));
}

SqliteDatabasePathWidget::~SqliteDatabasePathWidget()
{
    delete ui;
}

void SqliteDatabasePathWidget::saveToSettings(Core::ISettings *)
{
}

void SqliteDatabasePathWidget::onMoveDatabaseRequested()
{
    if (!ui->path->validatePath(ui->path->path()))
        return;

    Utils::DatabaseConnector c = settings()->databaseConnector();
    QString source = c.absPathToSqliteReadWriteDatabase();
    QString dest = ui->path->path();
    Utils::copyDir(source, dest);
    Utils::removeDirRecursively(source);
    c.setAbsPathToReadWriteSqliteDatabase(dest);
    settings()->setDatabaseConnector(c);
    Utils::warningMessageBox(tr("Database moved"),
                             tr("All your database are now moved to:\n"
                                "%1\n\n"
                                "<b>You must restart the application.</b>")
                             .arg(dest));
}


