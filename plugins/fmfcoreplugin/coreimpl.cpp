/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "coreimpl.h"
#include "appconfigwizard.h"
#include "commandlineparser.h"

#include <coreplugin/settings_p.h>
#include <coreplugin/isettings.h>
#include <coreplugin/imainwindow.h>
#include <coreplugin/theme.h>
#include <coreplugin/iuser.h>
#include <coreplugin/translators.h>
#include <coreplugin/actionmanager/actionmanager_p.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/contextmanager/contextmanager.h>
#include <coreplugin/contextmanager/contextmanager_p.h>
#include <coreplugin/filemanager.h>
#include <coreplugin/modemanager/modemanager.h>
#include <coreplugin/constants_icons.h>
#include <coreplugin/ipadtools.h>
#include <coreplugin/applicationautolock.h>

#include <utils/log.h>
#include <utils/global.h>
#include <utils/updatechecker.h>
#include <utils/versionnumber.h>
#include <translationutils/constanttranslations.h>

#include <QDir>
#include <QCoreApplication>
#include <QLibraryInfo>
#include <QTranslator>

namespace Core {
namespace Internal {
static CoreImpl *m_instance = 0;
} // namespace Internal
} // namespace Core


using namespace Core;
using namespace Core::Internal;
using namespace Trans::ConstantTranslations;

ICore* ICore::instance()
{
    return m_instance;
}

CoreImpl::CoreImpl(QObject *parent) :
    ICore(parent),
    m_MainWindow(0),
    m_ActionManager(0),
    m_ContextManager(0),
    m_ModeManager(0),
    m_Patient(0),
    m_User(0),
    m_Script(0),
    m_PadTools(0),
    m_AutoLock(0)
{
    setObjectName("Core");
    m_Settings = new SettingsPrivate(this);
    m_Settings->setPath(ISettings::UpdateUrl, Utils::Constants::FREEMEDFORMS_UPDATE_URL);

    m_Theme = new ThemePrivate(this);
    m_Theme->setThemeRootPath(m_Settings->path(ISettings::ThemeRootPath));
    m_CommandLine = new CommandLine(this);

    QTime chrono;
    chrono.start();
    bool logChrono = m_CommandLine->value(ICommandLine::Chrono).toBool();
    if (logChrono)
        Utils::Log::logTimeElapsed(chrono, "Core", "command line parsing");

    m_Theme->createSplashScreen(Constants::FREEMEDFORMS_SPLASHSCREEN);
    m_Settings->setPath(ISettings::SplashScreen, Constants::FREEMEDFORMS_SPLASHSCREEN);

    m_Theme->messageSplashScreen(tkTr(Trans::Constants::INITIALIZING_TRANSLATIONS));
    m_Translators = new Translators(this);
    m_Translators->setPathToTranslations(m_Settings->path(ISettings::TranslationsPath));
    m_Translators->addNewTranslator("qt");
    m_Translators->addNewTranslator(Trans::Constants::CONSTANTS_TRANSLATOR_NAME);
    m_Translators->addNewTranslator("lib_utils");
    m_Translators->addNewTranslator("lib_medical");
    m_Translators->addNewTranslator("lib_calendar");
    m_Translators->addNewTranslator("plugin_fmfcore");

    if (logChrono)
        Utils::Log::logTimeElapsed(chrono, "Core", "translators");

    m_Theme->messageSplashScreen(tkTr(Trans::Constants::STARTING_APPLICATION_AT_1).arg(QDateTime::currentDateTime().toString()));
    LOG(tkTr(Trans::Constants::STARTING_APPLICATION_AT_1).arg( QDateTime::currentDateTime().toString()));

    foreach(const QString &l, QCoreApplication::libraryPaths()) {
        LOG(tkTr(Trans::Constants::USING_LIBRARY_1).arg(l));
    }

    m_Theme->messageSplashScreen(tkTr(Trans::Constants::LOADING_SETTINGS));

#ifdef Q_OS_WIN
    if (m_CommandLine->value(Core::ICommandLine::RunningUnderWine).toBool()) {
        LOG("Running under Wine environnement." );
        QFont::insertSubstitution("MS Shell Dlg", "Tahoma" );
        QFont::insertSubstitution("MS Shell Dlg 2", "Tahoma" );
    }
#endif

    m_FileManager = new FileManager(this);
    m_UpdateChecker = new Utils::UpdateChecker(this);
    if (logChrono)
        Utils::Log::logTimeElapsed(chrono, "Core", "managers");

    if (Utils::isRunningOnMac())
        QApplication::setAttribute(Qt::AA_DontShowIconsInMenus);

    m_Theme->messageSplashScreen(tr("Core intialization finished..."));

    LOG(tr("Core intialization finished..."));
    if (logChrono)
        Utils::Log::logTimeElapsed(chrono, "Core", "end of core intialization");

    m_instance = this;
}

CoreImpl::~CoreImpl()
{
    Q_EMIT coreAboutToClose();
    if (m_MainWindow)
        delete m_MainWindow;
    if (m_ModeManager)
        delete m_ModeManager;
}

IMainWindow *CoreImpl::mainWindow() const  { return m_MainWindow; }
void CoreImpl::setMainWindow(IMainWindow *win)
{
    Q_ASSERT(m_MainWindow==0);
    Q_ASSERT(m_ActionManager==0);
    Q_ASSERT(m_ContextManager==0);
    Q_ASSERT(m_ModeManager==0);
    m_MainWindow = win;
    m_ModeManager = new ModeManager(m_MainWindow);
    m_ContextManager = new ContextManagerPrivate(m_MainWindow);
    m_ActionManager = new ActionManagerPrivate(m_MainWindow);
#ifdef WITH_UI_AUTO_LOCKING
    m_AutoLock = new ApplicationAutoLock(this);
    m_AutoLock->initialize();
    m_AutoLock->setTimeBeforeLocking(2000);
    m_AutoLock->startListening();
    connect(m_AutoLock, SIGNAL(lockRequired()), m_MainWindow, SLOT(lockApplication()));
#endif
}

bool CoreImpl::applicationConfigurationDialog() const
{
    AppConfigWizard wizard;
    if (m_Theme->splashScreen())
        m_Theme->splashScreen()->finish(&wizard);
    if (wizard.exec()==QDialog::Rejected)
        return false;
    return true;
}

ActionManager *CoreImpl::actionManager() const { return m_ActionManager; }
ContextManager *CoreImpl::contextManager() const { return m_ContextManager; }
ITheme *CoreImpl::theme() const { return m_Theme; }
Translators *CoreImpl::translators() const { return m_Translators; }
ISettings *CoreImpl::settings() const{ return m_Settings; }
FileManager *CoreImpl::fileManager() const { return m_FileManager; }
Utils::UpdateChecker *CoreImpl::updateChecker() const { return m_UpdateChecker; }
ICommandLine *CoreImpl::commandLine() const { return m_CommandLine; }
ModeManager *CoreImpl::modeManager() const {return m_ModeManager;}

bool CoreImpl::initialize(const QStringList &arguments, QString *errorString)
{
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);

    QString appr = m_Settings->licenseApprovedApplicationNumber();
    if (appr.isEmpty())
        appr="0.0.0";
    Utils::VersionNumber approved(appr);
    Utils::VersionNumber app(qApp->applicationVersion());

    if (Utils::isAlpha() && (approved < app)) {
        if (!QFile(settings()->fileName()).remove())
            LOG_ERROR("Unable to clean user settings... " + settings()->fileName());

        QString error;
        if (!settings()->path(Core::ISettings::ReadWriteDatabasesPath).contains("global_resources")) {
            if (!Utils::removeDirRecursively(settings()->path(Core::ISettings::ReadWriteDatabasesPath), &error))
                LOG_FOR("Settings", "Unable to clean user databases... " + error);
        } else {
            LOG_FOR("Settings", "Unable to clean user databases... The global_resources can not be deleted");
        }

        settings()->sync();
        settings()->setFirstTimeRunning(true);
    }

    if (m_Settings->firstTimeRunning()) {
        if (!applicationConfigurationDialog())
            return false;
        m_Settings->noMoreFirstTimeRunning();
        m_Settings->setLicenseApprovedApplicationNumber(qApp->applicationVersion());
        Utils::DatabaseConnector connector = settings()->databaseConnector();
        connector.setClearLog("_");
        connector.setClearPass("_");
        settings()->setDatabaseConnector(connector);


        m_Theme->createSplashScreen(Constants::FREEMEDFORMS_SPLASHSCREEN);
    }

    return true;
}

void CoreImpl::extensionsInitialized()
{
    LOG("Core opened");
    if (!m_User)
        return;
    if (m_User->uuid().isEmpty())
        return;
    Q_EMIT coreOpened();
}
