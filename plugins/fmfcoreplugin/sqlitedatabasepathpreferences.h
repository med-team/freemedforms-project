/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FREEMEDFORMSCORE_PLUGIN_SQLITEDATABASEPATHPREFERENCES_H
#define FREEMEDFORMSCORE_PLUGIN_SQLITEDATABASEPATHPREFERENCES_H

#include <coreplugin/ioptionspage.h>
#include <QPointer>
#include <QObject>

/**
 * \file ./plugins/fmfcoreplugin/sqlitedatabasepathpreferences.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;
namespace Internal {
namespace Ui {
class SqliteDatabasePathWidget;
}

class SqliteDatabasePathWidget : public QWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(SqliteDatabasePathWidget)

public:
    explicit SqliteDatabasePathWidget(QWidget *parent = 0);
    ~SqliteDatabasePathWidget();

    static void writeDefaultSettings(Core::ISettings * = 0) {}

public Q_SLOTS:
    void saveToSettings(Core::ISettings *);

private Q_SLOTS:
    void onMoveDatabaseRequested();

private:
    Ui::SqliteDatabasePathWidget *ui;
};

class SqliteDatabasePathPage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    SqliteDatabasePathPage(QObject *parent = 0);
    ~SqliteDatabasePathPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults() {SqliteDatabasePathWidget::writeDefaultSettings();}
    void checkSettingsValidity() {}
    void apply() {}
    void finish();

    QString helpPage() {return QString();}

    static void writeDefaultSettings(Core::ISettings *s) {SqliteDatabasePathWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);
private:
    QPointer<SqliteDatabasePathWidget> m_Widget;
};
}  // End Internal
}  // End Core

#endif // FREEMEDFORMSCORE_PLUGIN_SQLITEDATABASEPATHPREFERENCES_H
