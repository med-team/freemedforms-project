/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "drugscentralwidget.h"

#include <drugsbaseplugin/drugbasecore.h>
#include <drugsbaseplugin/drugsbase.h>
#include <drugsbaseplugin/protocolsbase.h>
#include <drugsbaseplugin/drugsmodel.h>
#include <drugsbaseplugin/globaldrugsmodel.h>
#include <drugsbaseplugin/drugsio.h>
#include <drugsbaseplugin/drugsdatabaseselector.h>
#include <drugsbaseplugin/druginteractioninformationquery.h>
#include <drugsbaseplugin/druginteractionresult.h>
#include <drugsbaseplugin/idruginteractionalert.h>
#include <drugsbaseplugin/prescriptionprinter.h>
#include <drugsbaseplugin/idrug.h>

#include <drugsplugin/drugswidget/drugselector.h>
#include <drugsplugin/drugswidget/prescriptionviewer.h>
#include <drugsplugin/drugswidget/dynamicalert.h>
#include <drugsplugin/dosagedialog/dosagecreatordialog.h>
#include <drugsplugin/dosagedialog/dosagedialog.h>
#include <drugsplugin/drugswidgetmanager.h>
#include <drugsplugin/constants.h>

#include <utils/log.h>
#include <utils/global.h>
#include <utils/widgets/databaseinformationdialog.h>
#include <translationutils/constanttranslations.h>
#include <extensionsystem/pluginmanager.h>

#include <coreplugin/constants.h>
#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/itheme.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/contextmanager/contextmanager.h>
#include <coreplugin/iuser.h>

#include <templatesplugin/templatescreationdialog.h>

#include "ui_drugscentralwidget.h"
#include <QTreeWidget>
#include <QProgressDialog>

using namespace DrugsWidget;
using namespace Trans::ConstantTranslations;

static inline ExtensionSystem::PluginManager *pluginManager() {return ExtensionSystem::PluginManager::instance();}
static inline Core::ISettings *settings()  { return Core::ICore::instance()->settings(); }
static inline Core::ContextManager *contextManager() {return Core::ICore::instance()->contextManager();}
static inline Core::IUser *user() {return Core::ICore::instance()->user();}
static inline DrugsDB::DrugsBase &drugsBase() {return DrugsDB::DrugBaseCore::instance().drugsBase();}
static inline DrugsDB::ProtocolsBase &protocolsBase() {return DrugsDB::DrugBaseCore::instance().protocolsBase();}
static inline DrugsDB::DrugsIO &drugsIo() {return DrugsDB::DrugBaseCore::instance().drugsIo();}
static inline DrugsDB::PrescriptionPrinter &prescriptionPrinter() {return DrugsDB::DrugBaseCore::instance().prescriptionPrinter();}

DrugsCentralWidget::DrugsCentralWidget(QWidget *parent) :
    QWidget(parent), m_CurrentDrugModel(0), m_ui(0)
{
    DrugsWidgetManager::instance();
}

DrugsCentralWidget::~DrugsCentralWidget()
{
    if (m_ui)
        delete m_ui;
    m_ui = 0;
}

bool DrugsCentralWidget::initialize(bool hideSelector)
{
    m_ui = new DrugsWidget::Internal::Ui::DrugsCentralWidget();
    m_ui->setupUi(this);

    m_Context = new Internal::DrugsContext(this);
    m_Context->setContext(Core::Context(Constants::C_DRUGS_PLUGINS));
    contextManager()->addContextObject(m_Context);

    m_CurrentDrugModel = new DrugsDB::DrugsModel(this);
    m_ui->m_PrescriptionView->initialize();
    m_ui->m_PrescriptionView->setModel(m_CurrentDrugModel);

    m_ui->m_DrugSelector->initialize();
    m_ui->m_DrugSelector->setFocus();
    if (hideSelector)
        m_ui->m_DrugSelector->hide();

    DrugsWidgetManager::instance()->setCurrentView(this);

    changeFontTo(QFont(settings()->value(Constants::S_VIEWFONT).toString(), settings()->value(Constants::S_VIEWFONTSIZE).toInt()));

    return true;
}


void DrugsCentralWidget::clear()
{
    m_ui->m_DrugSelector->clear();
    m_ui->m_PrescriptionView->clear();
}

void DrugsCentralWidget::setReadOnly(bool readOnly)
{
    m_ui->m_DrugSelector->setEnabled(!readOnly);
    m_ui->m_PrescriptionView->setEnabled(!readOnly);
}

bool DrugsCentralWidget::isReadOnly() const
{
    return (!m_ui->m_DrugSelector->isEnabled());
}

QListView *DrugsCentralWidget::prescriptionListView()
{
    if (!m_ui)
        return 0;
    return m_ui->m_PrescriptionView->listview();
}

PrescriptionViewer *DrugsCentralWidget::prescriptionView()
{
    if (!m_ui)
        return 0;
    return m_ui->m_PrescriptionView;
}

Internal::DrugSelector *DrugsCentralWidget::drugSelector()
{
    if (!m_ui)
        return 0;
    return m_ui->m_DrugSelector;
}

DrugsDB::DrugsModel *DrugsCentralWidget::currentDrugsModel() const
{
    return m_CurrentDrugModel;
}

void DrugsCentralWidget::setCurrentSearchMethod(int method)
{
    if (!m_ui)
        return;
    m_ui->m_DrugSelector->setSearchMethod(method);
}

void DrugsCentralWidget::createConnections()
{
    connect(m_ui->m_DrugSelector, SIGNAL(drugSelected(QVariant)),
            this, SLOT(onSelectorDrugSelected(QVariant)));
    connect(prescriptionListView(), SIGNAL(activated(const QModelIndex &)),
            m_ui->m_PrescriptionView, SLOT(showDosageDialog(const QModelIndex&)));
}

void DrugsCentralWidget::disconnect()
{
    prescriptionListView()->disconnect( prescriptionListView(), SIGNAL(activated(const QModelIndex &)),
             m_ui->m_PrescriptionView, SLOT(showDosageDialog(const QModelIndex&)));
    m_ui->m_DrugSelector->disconnect(m_ui->m_DrugSelector, SIGNAL(drugSelected(QVariant)),
                                     this, SLOT(onSelectorDrugSelected(QVariant)));
}

void DrugsCentralWidget::focusInEvent(QFocusEvent *event)
{
    Q_UNUSED(event);
    m_ui->m_DrugSelector->setFocus();
}


void DrugsCentralWidget::onSelectorDrugSelected(const QVariant &drugId)
{

    m_CurrentDrugModel->addDrug(drugId);

    DrugsDB::DrugInteractionInformationQuery query;
    query.processTime = DrugsDB::DrugInteractionInformationQuery::BeforePrescription;
    query.result = m_CurrentDrugModel->drugInteractionResult();
    query.relatedDrug = m_CurrentDrugModel->getDrug(drugId);

    DynamicAlert::DialogResult result = DynamicAlert::executeDynamicAlert(query, this);
    if (result==DynamicAlert::DynamicAlertAccepted) {
        m_CurrentDrugModel->removeLastInsertedDrug();
        return;
    }

    if (!m_CurrentDrugModel->isSelectionOnlyMode()) {
        Internal::DosageCreatorDialog dlg(this, m_CurrentDrugModel->dosageModel(drugId));
        if (dlg.exec()==QDialog::Rejected) {
            m_CurrentDrugModel->removeLastInsertedDrug();
        } else {
            m_CurrentDrugModel->setModified(true);
        }
        m_ui->m_PrescriptionView->listview()->update();
    }
}

void DrugsCentralWidget::changeFontTo(const QFont &font)
{
    m_ui->m_DrugSelector->setFont(font);
    m_ui->m_PrescriptionView->listview()->setFont(font);
}

bool DrugsCentralWidget::printPrescription()
{
    return prescriptionPrinter().print(m_CurrentDrugModel);
}

void DrugsCentralWidget::printPreview()
{
    prescriptionPrinter().printPreview(m_CurrentDrugModel);
}

bool DrugsCentralWidget::createTemplate()
{
    if (m_CurrentDrugModel->rowCount() == 0)
        return false;
    QString content = drugsIo().prescriptionToXml(m_CurrentDrugModel, "");
    Templates::TemplatesCreationDialog dlg(this);
    dlg.setTemplateContent(content);
    dlg.setTemplateSummary(prescriptionPrinter().prescriptionToHtml(m_CurrentDrugModel));
    dlg.setTemplateMimeTypes(drugsIo().prescriptionMimeTypes());
    dlg.setUserUuid(user()->uuid());
    dlg.exec();
    return true;
}

void DrugsCentralWidget::showDrugsDatabaseInformation()
{
    const DrugsDB::DatabaseInfos *info = drugsBase().actualDatabaseInformation();
    if (!info)
        return;

    QProgressDialog progress(this);
    progress.setLabelText(tr("Preparing database and drug engines report"));
    progress.setRange(0, 0);
    progress.setValue(0);
    progress.show();

    drugsBase().setConnectionName(DrugsDB::Constants::DB_DRUGS_NAME);
    Utils::DatabaseInformationDialog dlg(this);
    dlg.setTitle(tkTr(Trans::Constants::DRUGS_DATABASE_INFORMATION));
    dlg.setDatabase(drugsBase());
    info->toTreeWidget(dlg.getHeaderTreeWidget());

    QList<DrugsDB::IDrugEngine*> engines = pluginManager()->getObjects<DrugsDB::IDrugEngine>();
    QFont bold;
    bold.setBold(true);
    QFont mono("monospace");
    mono.setStyleHint(QFont::Monospace);
    foreach(DrugsDB::IDrugEngine *engine, engines) {
        QTreeWidgetItem *item = new QTreeWidgetItem(dlg.getHeaderTreeWidget(), QStringList() << tr("Drug engine report: %1").arg(engine->name()));
        item->setFont(0, bold);
        item->setFirstColumnSpanned(true);
        QString reportText = engine->engineDataReport();
        QTreeWidgetItem *report = new QTreeWidgetItem(item, QStringList() << reportText);
        report->setFont(0, mono);
        report->setFirstColumnSpanned(true);
        if (!reportText.isEmpty())
            item->setExpanded(true);
    }

    progress.close();
    Utils::resizeAndCenter(&dlg);
    dlg.exec();
}

void DrugsCentralWidget::showDosagesDatabaseInformation()
{
    Utils::DatabaseInformationDialog dlg(this);
    dlg.setTitle(tkTr(Trans::Constants::DRUGS_DATABASE_INFORMATION));
    dlg.setDatabase(protocolsBase());
    Utils::resizeAndCenter(&dlg);
    dlg.exec();
}
