/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "prescriptionviewer.h"

#include <drugsplugin/constants.h>
#include <drugsplugin/drugswidget/druginfo.h>
#include <drugsplugin/dosagedialog/dosagecreatordialog.h>
#include <drugsplugin/dosagedialog/dosagedialog.h>
#include <drugsplugin/drugswidgetmanager.h>
#include <drugsplugin/drugswidget/textualprescriptiondialog.h>
#include <drugsplugin/drugswidget/interactionsynthesisdialog.h>

#include <drugsbaseplugin/drugbasecore.h>
#include <drugsbaseplugin/drugsmodel.h>
#include <drugsbaseplugin/drugsio.h>

#include <coreplugin/constants_menus.h>
#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/isettings.h>
#include <coreplugin/imainwindow.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/dialogs/settingsdialog.h>
#include <coreplugin/dialogs/simpletextdialog.h>

#include <utils/global.h>
#include <translationutils/constanttranslations.h>

#include <QFileDialog>

#if QT_VERSION < 0x050000                                                       
#include <QPrinter>                                                             
#include <QPrintDialog>                                                         
#else                                                                           
#include <QtPrintSupport/QPrinter>                                              
#include <QtPrintSupport/QPrintDialog>                                          
#endif 

#include <QTextDocument>
#include <QTreeWidget>
#include <QClipboard>
#include <QMimeData>

#include <QDebug>

using namespace DrugsWidget;
using namespace DrugsWidget::Constants;
using namespace Trans::ConstantTranslations;

static inline DrugsDB::DrugsIO &drugsIo() {return DrugsDB::DrugBaseCore::instance().drugsIo();}
static inline Core::ActionManager *actionManager() {return Core::ICore::instance()->actionManager();}
static inline Core::ISettings *settings() {return Core::ICore::instance()->settings();}

PrescriptionViewer::PrescriptionViewer(QWidget *parent) :
    QWidget(parent),
    m_ToolBar(0),
    m_DrugsModel(0)
{
    setObjectName("PrescriptionViewer");
    setupUi(this);
}

void PrescriptionViewer::initialize()
{
    createActionsAndToolbar();

    verticalLayout->insertWidget(0, m_ToolBar);

    listView->setObjectName("PrescriptionListView");
    setListViewPadding(5);
    listView->setAcceptDrops(true);
    listView->setDragDropMode(QAbstractItemView::DropOnly);
    listView->setDropIndicatorShown(true);
    listView->setContextMenuPolicy(Qt::CustomContextMenu);
}

void PrescriptionViewer::setModel(DrugsDB::DrugsModel *model)
{
    Q_ASSERT_X(model, "PrescriptionViewer::setModel", "Model must be set correctly");
    m_DrugsModel = model;
    listView->setModel(model);
    listView->setModelColumn(DrugsDB::Constants::Drug::FullPrescription);
}

void PrescriptionViewer::setListViewPadding(const int pad)
{
    listView->setStyleSheet(QString("QListView#PrescriptionListView:item { padding: %1px; }").arg(pad));
}

void PrescriptionViewer::createActionsAndToolbar()
{
    Core::Command *cmd = 0;
    m_ToolBar = new QToolBar(this);
    int iconSize = settings()->value(Constants::S_TOOLBARICONSIZE).toInt();
    if (iconSize < 8)
        iconSize = 16;
    m_ToolBar->setIconSize(QSize(iconSize, iconSize));
    QStringList actionsToAdd;

#ifdef FREEMEDFORMS
    actionsToAdd
            << Constants::A_TOGGLE_DRUGSELECTOR
            << Core::Constants::A_FILE_OPEN
            << Constants::A_SAVE_PRESCRIPTION
            << Core::Constants::A_TEMPLATE_CREATE
            << Core::Constants::A_FILE_PRINTPREVIEW
            << DrugsWidget::Constants::A_PRINT_PRESCRIPTION;
#else
    actionsToAdd
            << Core::Constants::A_FILE_OPEN
            << Core::Constants::A_FILE_SAVE
            << Core::Constants::A_FILE_SAVEAS
            << Core::Constants::A_TEMPLATE_CREATE
            << Core::Constants::A_FILE_PRINTPREVIEW
            << DrugsWidget::Constants::A_PRINT_PRESCRIPTION;
#endif
    actionsToAdd << "--";
    actionsToAdd
            << DrugsWidget::Constants::A_CLEAR_PRESCRIPTION
            << Core::Constants::A_LIST_REMOVE
            << Core::Constants::A_LIST_MOVEDOWN
            << Core::Constants::A_LIST_MOVEUP
            << Core::Constants::A_LIST_SORT
            ;
    actionsToAdd << "--";
    actionsToAdd
            << DrugsWidget::Constants::A_TOGGLE_TESTINGDRUGS
            << DrugsWidget::Constants::A_VIEW_INTERACTIONS
            << DrugsWidget::Constants::A_CHANGE_DURATION
            << DrugsWidget::Constants::A_TOGGLEDRUGPRECAUTIONS;

    foreach(const QString &s, actionsToAdd) {
        if (s=="--") {
            m_ToolBar->addSeparator();
            continue;
        }
        cmd = actionManager()->command(Core::Id(s));
        if (cmd)
            m_ToolBar->addAction(cmd->action());
    }
    m_ToolBar->setFocusPolicy(Qt::ClickFocus);
}

void PrescriptionViewer::on_listView_customContextMenuRequested(const QPoint &)
{
    Q_ASSERT(m_DrugsModel);
    if (!m_DrugsModel)
        return;
    if (!m_DrugsModel->rowCount())
        return;

    QMenu *pop = new QMenu(this);
    QStringList actionsToAdd;
    actionsToAdd
            << DrugsWidget::Constants::A_COPYPRESCRIPTIONITEM
            << DrugsWidget::Constants::A_OPENDOSAGEDIALOG
            << DrugsWidget::Constants::A_OPENDOSAGEPREFERENCES
            << DrugsWidget::Constants::A_RESETPRESCRIPTIONSENTENCE_TODEFAULT
            << DrugsWidget::Constants::A_CHANGE_DURATION;

    Core::Command *cmd = 0;
    foreach(const QString &s, actionsToAdd) {
        cmd = actionManager()->command(Core::Id(s));
        pop->addAction(cmd->action());
    }
    pop->exec(QCursor::pos());
    delete pop;
    pop = 0;
}

bool PrescriptionViewer::savePrescription()
{
    Q_ASSERT(m_DrugsModel);
    if (!m_DrugsModel)
        return false;
    QHash<QString, QString> extra;
    return drugsIo().savePrescription(m_DrugsModel, extra);
}

bool PrescriptionViewer::saveAsPrescription()
{
    Q_ASSERT(m_DrugsModel);
    if (!m_DrugsModel)
        return false;
    QHash<QString, QString> extra;
    return drugsIo().savePrescription(m_DrugsModel, extra);
}

void PrescriptionViewer::clear()
{
    Q_ASSERT(m_DrugsModel);
    if (!m_DrugsModel)
        return;
    m_DrugsModel->clearDrugsList();
    m_DrugsModel->setModified(false);
}

void PrescriptionViewer::removeTriggered()
{
    Q_ASSERT(m_DrugsModel);
    if (!m_DrugsModel)
        return;
    if (!listView->selectionModel())
        return;
    if (!listView->selectionModel()->hasSelection())
        return;
    const QModelIndexList &list = listView->selectionModel()->selectedRows(0);
    foreach(const QModelIndex &index, list) {
        static_cast<DrugsDB::DrugsModel*>(listView->model())->removeRows(index.row(), 1);
    }
}

void PrescriptionViewer::moveUp()
{
    Q_ASSERT(m_DrugsModel);
    if (!m_DrugsModel)
        return;
    int row = listView->currentIndex().row();
    m_DrugsModel->moveUp(listView->currentIndex());
    listView->setCurrentIndex(m_DrugsModel->index(row-1,0));
}

void PrescriptionViewer::moveDown()
{
    Q_ASSERT(m_DrugsModel);
    if (!m_DrugsModel)
        return;
    int row = listView->currentIndex().row();
    m_DrugsModel->moveDown(listView->currentIndex());
    listView->setCurrentIndex(m_DrugsModel->index(row+1,0));
}

void PrescriptionViewer::sortDrugs()
{
    Q_ASSERT(m_DrugsModel);
    if (!m_DrugsModel)
        return;
    m_DrugsModel->sort(0);
}

void PrescriptionViewer::showDrugInfo(const QModelIndex &item)
{
    Internal::DrugInfo di(item.row(), this);
    di.exec();
}

void PrescriptionViewer::showDosageDialog(const QModelIndex &item)
{
    Q_ASSERT(m_DrugsModel);
    if (!m_DrugsModel)
        return;
    if (m_DrugsModel->isSelectionOnlyMode())
        return;

    int row;
    if (!item.isValid())
        row = listView->currentIndex().row();
    else
        row = item.row();
    if (row < 0)
        return;

    const QVariant &drugUid = m_DrugsModel->index(row, DrugsDB::Constants::Drug::DrugId).data();
    bool isTextual = m_DrugsModel->index(row, DrugsDB::Constants::Prescription::IsTextualOnly).data().toBool();
    if (isTextual) {
        TextualPrescriptionDialog dlg(this);
        dlg.setDrugLabel(m_DrugsModel->index(row,DrugsDB::Constants::Drug::Denomination).data().toString());
        dlg.setDrugNote(m_DrugsModel->index(row,DrugsDB::Constants::Prescription::Note).data().toString());
        dlg.setALD(m_DrugsModel->index(row,DrugsDB::Constants::Prescription::IsALD).data().toBool());
        int r = dlg.exec();
        if (r==QDialog::Accepted) {
            m_DrugsModel->setData(m_DrugsModel->index(row, DrugsDB::Constants::Drug::Denomination), dlg.drugLabel());
            m_DrugsModel->setData(m_DrugsModel->index(row, DrugsDB::Constants::Prescription::Note), dlg.drugNote());
            m_DrugsModel->setData(m_DrugsModel->index(row, DrugsDB::Constants::Prescription::IsALD), dlg.isALD());
        }
    } else if (drugUid.toInt()!=-1 && !drugUid.isNull()) {
        Internal::DosageDialog dlg(this);
        dlg.changeRow(drugUid, row);
        dlg.exec();
    }
    listView->setViewMode(QListView::ListMode);
}

void PrescriptionViewer::viewInteractions()
{
    Q_ASSERT(m_DrugsModel);
    if (!m_DrugsModel)
        return;
    InteractionSynthesisDialog dlg(m_DrugsModel, this);
    Utils::resizeAndCenter(&dlg, Core::ICore::instance()->mainWindow());
    dlg.exec();

}

void PrescriptionViewer::changeDuration()
{
    QPoint pos;
    QString senderTag;
    if (sender()) {
        senderTag = "%ù";
        pos = QCursor::pos();
    } else {
        QAction *a = actionManager()->command(DrugsWidget::Constants::A_CHANGE_DURATION)->action();
        pos = mapToGlobal(m_ToolBar->actionGeometry(a).center());
        senderTag.clear();
    }

    QMenu *root = new QMenu(this);
    QStringList subs = QStringList()
                       << Trans::Constants::DAY_S
                       << Trans::Constants::WEEK_S
                       << Trans::Constants::MONTH_S
                       << Trans::Constants::QUARTER_S;
    QList<int> quantity = QList<int>() << 31 << 15 << 12 << 4;
    int i = 0;
    foreach(const QString &s, subs) {
        QMenu *submenu = new QMenu(tkTr(s.toUtf8()), root);
        root->addMenu(submenu);
        int j = quantity[i];
        for(int z=0; z<j;++z) {
            QAction *a = submenu->addAction(QString::number(z+1));
            a->setObjectName(tkTr(s.toUtf8())+":"+QString::number(z+1)+senderTag);
            connect(a,SIGNAL(triggered()), this, SLOT(changeDurationTo()));
        }
        ++i;
    }

    root->popup(pos);
}

/*! \brief Changes all drugs duration according to the triggered action. \sa PrescriptionViewer::changeDuration(). */
void PrescriptionViewer::changeDurationTo()
{
    Q_ASSERT(m_DrugsModel);
    if (!m_DrugsModel)
        return;
    QAction *a = qobject_cast<QAction*>(sender());
    if (!a)
        return;

    QString name = a->objectName().remove("%ù");
    QString scheme = name.left(name.indexOf(":"));
    int duration = name.mid(name.indexOf(":")+1).toInt();

    int i = 0;
    int nb = 0;
    if (a->objectName().contains("%ù")) {
        i = listView->currentIndex().row();
        nb = i + 1;
    } else {
        i = 0;
        nb = m_DrugsModel->rowCount();
    }

    for(int j = i ; j<nb ; ++j) {
        QModelIndex idx = m_DrugsModel->index(j, DrugsDB::Constants::Prescription::DurationScheme);
        m_DrugsModel->setData(idx, scheme);
        idx = m_DrugsModel->index(j, DrugsDB::Constants::Prescription::DurationFrom);
        m_DrugsModel->setData(idx, duration);
        idx = m_DrugsModel->index(j, DrugsDB::Constants::Prescription::DurationUsesFromTo);
        m_DrugsModel->setData(idx, false);
    }
}

void PrescriptionViewer::openProtocolPreferencesDialog()
{
    Core::SettingsDialog dlg(this, tkTr(Trans::Constants::DRUGS), "DrugsPrintOptionsPage");
    dlg.exec();
}

void PrescriptionViewer::copyPrescriptionItem()
{
    Q_ASSERT(m_DrugsModel);
    if (!m_DrugsModel)
        return;
    if (!listView->selectionModel())
        return;
    if (!listView->selectionModel()->hasSelection())
        return;
    QModelIndexList list = listView->selectionModel()->selectedRows();
    qSort(list);
    QString html;
    for(int i=0; i < list.count() ; ++i) {
        int row = list.at(i).row();
        QModelIndex idx = m_DrugsModel->index(row, DrugsDB::Constants::Prescription::ToHtml);
        html += idx.data().toString();
    }
    QMimeData *data = new QMimeData;
    data->setHtml(html);
    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setMimeData(data);
}

QListView *PrescriptionViewer::listview()
{
    return listView;
}

void PrescriptionViewer::changeEvent(QEvent *e)
{
    switch (e->type()) {
    case QEvent::LanguageChange:
        retranslateUi(this);
        break;
    default:
        break;
    }
}
