/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TEXTUALPRESCRIPTIONDIALOG_H
#define TEXTUALPRESCRIPTIONDIALOG_H

#include <drugsplugin/drugs_exporter.h>

#include <QDialog>
#include <QString>

namespace DrugsWidget {
namespace Ui {
    class TextualPrescriptionDialog;
}

class DRUGS_EXPORT TextualPrescriptionDialog : public QDialog
{
    Q_OBJECT
public:
    TextualPrescriptionDialog(QWidget *parent = 0);
    ~TextualPrescriptionDialog();

    void done(int result);

    QString drugLabel() const;
    QString drugNote() const;
    bool isALD() const;

    void setDrugLabel(const QString &label);
    void setDrugNote(const QString &note);
    void setALD(const bool ald);

private:
    Ui::TextualPrescriptionDialog *m_ui;
};

} // End namespace DrugsWidget

#endif // TEXTUALPRESCRIPTIONDIALOG_H
