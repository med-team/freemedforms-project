/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef INTERACTIONSYNTHESISDIALOG_H
#define INTERACTIONSYNTHESISDIALOG_H

#include <QDialog>
QT_BEGIN_NAMESPACE
class QTableWidgetItem;
class QModelIndex;
QT_END_NAMESPACE

/**
 * \file ./plugins/drugsplugin/drugswidget/interactionsynthesisdialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsDB {
class DrugsModel;
}

namespace DrugsWidget {
namespace Internal {
class InteractionSynthesisDialogPrivate;
}

namespace Ui {
    class InteractionSynthesisDialog;
}

class InteractionSynthesisDialog : public QDialog
{
    Q_OBJECT

public:
    explicit InteractionSynthesisDialog(DrugsDB::DrugsModel *drugModel, QWidget *parent = 0);
    ~InteractionSynthesisDialog();

protected Q_SLOTS:
//    void levelActivated(QAction *a);
    void interactionActivated(const QModelIndex &current, const QModelIndex &previous);
//    void interactorsActivated(QTableWidgetItem *item);
    void on_getBiblio_clicked();
    void showEbm(const QModelIndex &index);
    void print(QAction *action);
    void drugReportRequested();

protected:
    void changeEvent(QEvent *e);

private:
    Internal::InteractionSynthesisDialogPrivate *d;
};


}  // End namespace DrugsWidget

#endif // INTERACTIONSYNTHESISDIALOG_H
