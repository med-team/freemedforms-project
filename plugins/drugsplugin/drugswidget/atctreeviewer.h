/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ATCTREEVIEWER_H
#define ATCTREEVIEWER_H

#include <drugsplugin/drugs_exporter.h>

#include <QTreeView>

namespace DrugsWidget {

class DRUGS_EXPORT AtcTreeViewer : public QTreeView
{
public:
    AtcTreeViewer(QWidget *parent = 0);
    ~ AtcTreeViewer();

};

}  // End namespace

#endif // ATCTREEVIEWER_H
