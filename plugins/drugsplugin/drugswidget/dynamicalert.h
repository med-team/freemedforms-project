/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DYNAMICALERT_H
#define DYNAMICALERT_H

#include <drugsplugin/drugs_exporter.h>

#include <QDialog>

/**
 * \file ./plugins/drugsplugin/drugswidget/dynamicalert.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
 */

namespace DrugsDB {
struct DrugInteractionInformationQuery;
}

namespace DrugsWidget {

namespace Ui {
    class DynamicAlert;
}

class DRUGS_EXPORT DynamicAlert : public QDialog
{
    Q_OBJECT

    explicit DynamicAlert(const DrugsDB::DrugInteractionInformationQuery &query, QWidget *parent = 0);
public:
    enum DialogResult {
        NoDynamicAlert = 0,
        DynamicAlertOverridden,
        DynamicAlertAccepted
    };
    ~DynamicAlert();

    static DialogResult executeDynamicAlert(const DrugsDB::DrugInteractionInformationQuery &query, QWidget *parent = 0);

protected Q_SLOTS:
    void showInteractionSynthesisDialog();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::DynamicAlert *ui;
};

}  // End namespace DrugsWidget
#endif // DYNAMICALERT_H
