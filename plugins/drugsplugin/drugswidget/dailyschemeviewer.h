/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DAILYSCHEMEVIEWER_H
#define DAILYSCHEMEVIEWER_H

#include <QWidget>

/**
 * \file ./plugins/drugsplugin/drugswidget/dailyschemeviewer.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/


namespace Utils {
class SpinBoxDelegate;
}

namespace DrugsDB {
class DailySchemeModel;
}

namespace DrugsWidget {
namespace Internal {
class DailySchemeViewerPrivate;

class DailySchemeViewer : public QWidget
{
    Q_OBJECT
    /**
      \todo Add properties for the mapper ?
      \todo Add a undo/redo possibility (when changing the view mode)
    */

public:
    DailySchemeViewer(QWidget *parent = 0);
    ~DailySchemeViewer();

    void setModel(DrugsDB::DailySchemeModel *model);
    DrugsDB::DailySchemeModel *model() const;

    void setScoredTablet(bool isScored);
    void setDailyMaximum(double maximum);

protected:
    void changeEvent(QEvent *e);

private:
    DailySchemeViewerPrivate *d;
};


}  // end namespace Internal
}  // End namespace DrugsWidget


#endif // DAILYSCHEMEVIEWER_H
