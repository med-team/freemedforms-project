#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

HEADERS += $${PWD}/druginfo.h \
    $${PWD}/druginfo_p.h \
    $${PWD}/mfDrugSelector.h \
    $${PWD}/mfPrescriptionViewer.h \
    $${PWD}/mfInteractionDialog.h \
    $${PWD}/mfDrugsCentralWidget.h

SOURCES += $${PWD}/druginfo.cpp \
    $${PWD}/mfDrugSelector.cpp \
    $${PWD}/mfPrescriptionViewer.cpp \
    $${PWD}/mfInteractionDialog.cpp \
    $${PWD}/mfDrugsCentralWidget.cpp

FORMS += $${PWD}/druginfo.ui \
    $${PWD}/mfDrugSelector.ui \
    $${PWD}/mfPrescriptionViewer.ui \
    $${PWD}/mfInteractionDialog.ui \
    $${PWD}/mfDrugsCentralWidget.ui
