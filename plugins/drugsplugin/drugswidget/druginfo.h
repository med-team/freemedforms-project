/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGINFO_H
#define DRUGINFO_H

#include <QDialog>
#include <QObject>

/**
 * \file ./plugins/drugsplugin/drugswidget/druginfo.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/
namespace DrugsWidget {
namespace Internal {
class DrugInfoPrivate;

class DrugInfo : public QDialog
{
    Q_OBJECT
public:
    DrugInfo(const QVariant &drugUid, QWidget * parent = 0 );
    ~DrugInfo() {}

    void setDrug(const QVariant &drugUid);

protected Q_SLOTS:
    void accept();
    void reject();
    void done(int r) {QDialog::done(r);}

private:
    Internal::DrugInfoPrivate *d;
};

}  // End Internal
}  // End DrugsWidget

#endif  //  DRUGINFO
