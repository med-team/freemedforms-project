/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef MFDRUGINFO_P_H
#define MFDRUGINFO_P_H

#include <utils/messagesender.h>

#include <QDialog>
#include <QObject>

// include Ui
#include "ui_druginfo.h"


/**
 * \file ./plugins/drugsplugin/drugswidget/druginfo_p.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsDB {
class IDrugInteraction;
}


namespace DrugsWidget {
namespace Internal {
class DrugsData;
class DrugInteraction;

class DrugInfoPrivate : public QObject, public Ui::DrugInfo
{
     Q_OBJECT
public:
     DrugInfoPrivate(QDialog *parent);
     ~DrugInfoPrivate() {}

     bool checkSent();

public Q_SLOTS:
     void on_butSendINN_clicked();
     void on_butIAMSend_clicked();
     void on_listWidgetInteractions_itemSelectionChanged();

public:
     QDialog *m_Parent;
     QVariant m_DrugUid;
     Utils::MessageSender m_Sender;
     QList<DrugsDB::IDrugInteraction *> m_InteractionsList;         // should not be deleted
     bool m_INNSent, m_InteractSent;
};

}  // End Internal
}  // End DrugsWidget

#endif  // MFDRUGINFO_P_H

