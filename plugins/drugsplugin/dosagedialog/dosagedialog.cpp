/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "dosagedialog.h"

#include <drugsbaseplugin/dosagemodel.h>
#include <drugsbaseplugin/drugsmodel.h>
#include <drugsbaseplugin/constants.h>

#include <drugsplugin/constants.h>
#include <drugsplugin/drugswidget/druginfo.h>
#include <drugsplugin/drugswidgetmanager.h>

#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/itheme.h>

#include <QHeaderView>
#include <QRadioButton>
#include <QCheckBox>
#include <QTableWidget>
#include <QSpinBox>

using namespace DrugsWidget::Constants;
using namespace DrugsWidget::Internal;

static inline DrugsDB::DrugsModel *drugModel() { return DrugsWidget::DrugsWidgetManager::instance()->currentDrugsModel(); }

namespace DrugsWidget {
namespace Internal {

class DosageDialogPrivate
{
public:
    DosageDialogPrivate() : m_DosageModel(0), m_UserFormButtonPopup(0) {}

    DosageModel*m_DosageModel;
    QString m_ActualDosageUuid;
    QVariant m_DrugUid;
    int m_DrugRow;
    QMenu *m_UserFormButtonPopup;
};

} // namespace Internal
} // namespace DrugsWidget






DosageDialog::DosageDialog(QWidget *parent)
    : QDialog(parent),
    d(0)
{
    setObjectName("DosageDialog");
    d = new DosageDialogPrivate();
    setupUi(this);
    innButton->setIcon(Core::ICore::instance()->theme()->icon(DrugsDB::Constants::I_SEARCHINN));
    setWindowTitle(tr("Drug Dosage") + " - " + qApp->applicationName());

    connect(drugModel(), SIGNAL(dataChanged(QModelIndex,QModelIndex)),
             this, SLOT(updatePosologicSentence(QModelIndex,QModelIndex)));
}

DosageDialog::~DosageDialog()
{
    if (d) delete d; d=0;
}


void DosageDialog::changeRow(const QVariant &drugUid, const int drugRow)
{
    using namespace DrugsDB::Constants;
    Q_ASSERT(drugModel()->containsDrug(drugUid));
    d->m_DrugUid = drugUid;
    d->m_DrugRow = drugRow;
    dosageViewer->useDrugsModel(d->m_DrugUid, drugRow);
    innButton->setChecked(drugModel()->drugData(d->m_DrugUid, Prescription::IsINNPrescription).toBool());

    QString name = drugModel()->drugData(d->m_DrugUid, Drug::Denomination).toString();
    if (drugModel()->drugData(d->m_DrugUid, Prescription::IsINNPrescription).toBool())
        drugNameButton->setText(drugModel()->drugData(d->m_DrugUid, Drug::InnCompositionString).toString());
    else
        drugNameButton->setText(name);
    QString toolTip = drugModel()->drugData(d->m_DrugUid, Interaction::ToolTip).toString();
    iconInteractionLabel->setToolTip(toolTip);
    iconInteractionLabel->setPixmap(drugModel()->drugData(d->m_DrugUid, Interaction::Icon).value<QIcon>().pixmap(16,16));
    toolTip = drugModel()->drugData(d->m_DrugUid, Drug::CompositionString).toString();
    drugNameButton->setToolTip(toolTip);
    innButton->setEnabled(drugModel()->drugData(d->m_DrugUid, Drug::AllInnsKnown).toBool());
}


void DosageDialog::done(int r)
{
    drugNameButton->setFocus();

    disconnect(drugModel(), SIGNAL(dataChanged(QModelIndex,QModelIndex)),
             this, SLOT(updatePosologicSentence(QModelIndex,QModelIndex)));

    dosageViewer->commitToModel();

    if (r == QDialog::Accepted) {
        dosageViewer->done(r);
    }
    QDialog::done(r);
}

void DosageDialog::on_drugNameButton_clicked()
{
    DrugInfo dialog(d->m_DrugUid, this);
    dialog.exec();
}

void DosageDialog::on_innButton_clicked()
{
    using namespace DrugsDB::Constants;
    drugModel()->setDrugData(d->m_DrugUid, Prescription::IsINNPrescription, innButton->isChecked());
    if (innButton->isChecked())
        drugNameButton->setText(drugModel()->drugData(d->m_DrugUid, Drug::InnCompositionString).toString());
    else {
        QString name = drugModel()->drugData(d->m_DrugUid, Drug::Denomination).toString();
        drugNameButton->setText(name);
    }
}

void DosageDialog::updatePosologicSentence(const QModelIndex &, const QModelIndex &)
{
    resultTextBrowser->setPlainText(drugModel()->data(drugModel()->index(d->m_DrugRow, DrugsDB::Constants::Drug::FullPrescription)).toString());
}
