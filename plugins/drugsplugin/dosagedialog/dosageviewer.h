/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DOSAGEVIEWER_H
#define DOSAGEVIEWER_H

#include <QWidget>

/** \todo aggregate the ui in the private part */
#include "ui_dosageviewer.h"

/**
 * \file ./plugins/drugsplugin/dosagedialog/dosageviewer.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsDB {
namespace Internal {
class DosageModel;
}
}

namespace DrugsWidget {
namespace Internal {
class DosageViewerPrivate;
class DosageModel;

class DosageViewer : public QWidget, public Ui::DosageViewer
{
    Q_OBJECT
    Q_DISABLE_COPY(DosageViewer);

public:
    explicit DosageViewer(QWidget *parent);
    ~DosageViewer();

    void setDosageModel(DrugsDB::Internal::DosageModel *model);
    void useDrugsModel(const QVariant &drugId, const int drugRow);

public Q_SLOTS:
    void done(int r);
    void commitToModel();
    void changeCurrentRow(const int dosageRow);
    void changeCurrentRow(const QModelIndex &current, const QModelIndex &previous);

Q_SIGNALS:
    void protocolDataChanged();

private:
    void resizeEvent(QResizeEvent * event);

private Q_SLOTS:
    void on_fromToIntakesCheck_stateChanged(int state);
    void on_fromToDurationCheck_stateChanged(int state);
    void on_intakesFromSpin_valueChanged(double d);
    void on_durationFromSpin_valueChanged(int d);
    void on_userformsButton_clicked();
    void on_dosageForAllInnCheck_stateChanged(int state);
    void on_aldCheck_stateChanged(int state);
    void on_monographButton_clicked();
    void on_tabWidget_currentChanged(int);
    void onDailySchemeModelDataChanged(const QModelIndex &index);

private:
    DosageViewerPrivate *d;
};

}  // End Internal
}  // End DrugsWidget

#endif // DOSAGEVIEWER_H
