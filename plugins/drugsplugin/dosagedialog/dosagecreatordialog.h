/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DOSAGECREATORDIALOG_H
#define DOSAGECREATORDIALOG_H

#include <QtGlobal>
QT_BEGIN_NAMESPACE
class QDataWidgetMapper;
QT_END_NAMESPACE

// include Ui
#include "ui_dosagecreatordialog.h"

/**
 * \file ./plugins/drugsplugin/dosagedialog/dosagecreatordialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsDB {
namespace Internal {
class DosageModel;
}  // End Internal
}  // End DrugsDB

namespace DrugsWidget {
namespace Internal {
class DosageModel;
class DosageCreatorDialogPrivate;

class DosageCreatorDialog : public QDialog, public Ui::DosageCreatorDialog
{
    Q_OBJECT
    Q_DISABLE_COPY(DosageCreatorDialog);

public:
    explicit DosageCreatorDialog(QWidget *parent, DrugsDB::Internal::DosageModel *dosageModel);
    ~DosageCreatorDialog();

private:
    void keyPressEvent(QKeyEvent *e);

private Q_SLOTS:
    void done(int r);
    void updateSettings();
    void onProtocolDataChanged();
    void saveRequested();
    void prescribeRequested();
    void saveAndPrescribeRequested();
    void helpRequested();
    void drugsInformationRequested();
    void addTestOnlyRequested();
    void showInteractionSynthesisRequested();

private:
    DosageCreatorDialogPrivate *d;
};

}  // End Internal
}  // End DrugsWidget


#endif // DOSAGECREATORDIALOG_H
