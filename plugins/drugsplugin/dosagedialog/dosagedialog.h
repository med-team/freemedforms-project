/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DOSAGEDIALOG_H
#define DOSAGEDIALOG_H

// include Ui
#include "ui_dosagedialog.h"

/**
 * \file ./plugins/drugsplugin/dosagedialog/dosagedialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsWidget {
namespace Internal {
class DosageDialogPrivate;


/**
 * \brief Dialog for dosage prescription based on a standard dosage set.
 * Before all, this dialog is a wrapper on the DrugsDB::DrugsModel (not the DrugsDB::DosageModel). The mfDrugsModel is a kind of proxy
 * that manages drugs (view only) / dosages (via DrugsDB::DosageModel) / interactions (view only).
 * If you want to create a new dosage, you must create a new row onto the model BEFORE.
 * If you want to edit or modify a dosage, you must inform the dialog of the row and the CIS of the drug.
 \ingroup freediams drugswidget
*/
class DosageDialog : public QDialog, public Ui::DosageDialog
{
    Q_OBJECT
    Q_DISABLE_COPY(DosageDialog);

public:
    explicit DosageDialog(QWidget *parent);
    ~DosageDialog();

    void changeRow(const QVariant &drugUid, const int dosageRow);


private Q_SLOTS:
    void done(int r);
    void on_drugNameButton_clicked();
    void on_innButton_clicked();
    void updatePosologicSentence(const QModelIndex &, const QModelIndex &);

private:
    DosageDialogPrivate *d;
};

} // namespace Internal
} // namespace DrugsWidget

#endif // DOSAGEDIALOG_H
