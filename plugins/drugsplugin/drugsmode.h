/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGSMODE_H
#define DRUGSMODE_H

#include <coreplugin/modemanager/imode.h>

/**
 * \file ./plugins/drugsplugin/drugsmode.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Form {
class FormPlaceHolder;
}

namespace DrugsWidget {
namespace Internal {

class DrugsMode : public Core::IMode
{
    Q_OBJECT

public:
    explicit DrugsMode(QObject *parent = 0);
    ~DrugsMode();

public Q_SLOTS:
    void onPatientFormsLoaded();

private:
    bool inPool;
    Form::FormPlaceHolder *m_Holder;
};


}  // End namespace Internal
}  // End namespace DrugsWidget

#endif // DRUGSMODE_H
