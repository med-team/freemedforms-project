/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGSWIDGETPLUGIN_H
#define DRUGSWIDGETPLUGIN_H

#include <extensionsystem/iplugin.h>

/**
 * \file ./plugins/drugsplugin/drugsplugin.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsWidget {
namespace Internal {
class DrugGeneralOptionsPage;
class DrugsSelectorOptionsPage;
class DrugPosologicSentencePage;
class DrugsUserOptionsPage;
class DrugsExtraOptionsPage;
class ProtocolPreferencesPage;
class DrugsDatabaseSelectorPage;
class DrugEnginesPreferencesPage;

class DrugsPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.freemedforms.FreeMedForms.DrugsPlugin" FILE "Drugs.json")

public:
    DrugsPlugin();
    ~DrugsPlugin();

    bool initialize(const QStringList &arguments, QString *errorMessage = 0);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

private Q_SLOTS:
    void postCoreOpened();

private:
    DrugGeneralOptionsPage *viewPage;
    DrugsSelectorOptionsPage *selectorPage;
    DrugPosologicSentencePage *posologicPage;
    DrugsUserOptionsPage *userPage;
    DrugsExtraOptionsPage *extraPage;
    DrugsDatabaseSelectorPage *databaseSelectorPage;
    ProtocolPreferencesPage *protocolPage;
    DrugEnginesPreferencesPage *enginePage;
};

} // namespace Internal
} // namespace DrugsWidget

#endif // DRUGSWIDGETPLUGIN_H
