#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

include( ../coreplugin/coreplugin.pri )
include( ../drugsbaseplugin/drugsbaseplugin.pri )
include( ../templatesplugin/templatesplugin.pri )
include( ../texteditorplugin/texteditorplugin.pri )
include( ../printerplugin/printerplugin.pri )
include( ../listviewplugin/listviewplugin.pri )
include( $${SOURCES_LIBS_PATH}/utils.pri)
include( $${SOURCES_LIBS_PATH}/translationutils.pri)
include( $${SOURCES_LIBS_PATH}/medicalutils.pri)

# FreeMedForms only inclusion
isEmpty(FREEDIAMS):include( ../formmanagerplugin/formmanagerplugin.pri )
