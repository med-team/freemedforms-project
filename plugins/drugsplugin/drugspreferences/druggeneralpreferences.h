/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGS_DRUGGENERAL_PREFERENCES
#define DRUGS_DRUGGENERAL_PREFERENCES

#include <coreplugin/ioptionspage.h>

#include <QPointer>

#include "ui_druggeneralpreferencespage.h"

/**
 * \file ./plugins/drugsplugin/drugspreferences/druggeneralpreferences.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;
}

namespace DrugsDB {
class IDrug;
}

namespace DrugsWidget {
namespace Internal {

class DrugGeneralPreferencesWidget : public QWidget, private Ui::DrugGeneralPreferencesWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(DrugGeneralPreferencesWidget)

public:
    explicit DrugGeneralPreferencesWidget(QWidget *parent = 0);
    void setDataToUi();

    static void writeDefaultSettings( Core::ISettings *s );

public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);

protected:
    virtual void changeEvent(QEvent *e);
};

class DrugGeneralOptionsPage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    DrugGeneralOptionsPage(QObject *parent = 0);
    ~DrugGeneralOptionsPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    QString helpPage();

    static void writeDefaultSettings(Core::ISettings *s) {Internal::DrugGeneralPreferencesWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);
private:
    QPointer<Internal::DrugGeneralPreferencesWidget> m_Widget;
};

}  // End namespace Internal
}  // End namespace Drugs

#endif // DRUGS_DRUGGENERAL_PREFERENCES
