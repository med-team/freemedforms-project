/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGSWIDGET_INTERNAL_DRUGENGINESPREFERENCES_H
#define DRUGSWIDGET_INTERNAL_DRUGENGINESPREFERENCES_H

#include <coreplugin/ioptionspage.h>

#include <QPointer>
#include <QWidget>

/**
 * \file ./plugins/drugsplugin/drugspreferences/drugenginespreferences.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;
}

namespace DrugsWidget {
namespace Internal {

namespace Ui {
    class DrugEnginesPreferences;
}

class DrugEnginesPreferences : public QWidget
{
    Q_OBJECT

public:
    explicit DrugEnginesPreferences(QWidget *parent = 0);
    ~DrugEnginesPreferences();

    void setDataToUi();
    void saveToSettings(Core::ISettings *sets = 0);

    static void writeDefaultSettings(Core::ISettings *s);

private:
    Ui::DrugEnginesPreferences *ui;
};

class DrugEnginesPreferencesPage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    DrugEnginesPreferencesPage(QObject *parent = 0);
    ~DrugEnginesPreferencesPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    QString helpPage();

    static void writeDefaultSettings(Core::ISettings *s)  {Internal::DrugEnginesPreferences::writeDefaultSettings(s);}
    QWidget *createPage(QWidget *parent = 0);
private:
    QPointer<Internal::DrugEnginesPreferences> m_Widget;
};

} // namespace Internal
} // namespace DrugsWidget


#endif // DRUGSWIDGET_INTERNAL_DRUGENGINESPREFERENCES_H
