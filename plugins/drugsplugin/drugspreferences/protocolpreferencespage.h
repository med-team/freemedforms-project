/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PROTOCOLPREFERENCESPAGE_H
#define PROTOCOLPREFERENCESPAGE_H

#include <coreplugin/ioptionspage.h>

#include <QPointer>
#include <QWidget>
#include <QStringListModel>

/**
 * \file ./plugins/drugsplugin/drugspreferences/protocolpreferencespage.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsDB {
class DatabaseInfos;
}

namespace Core {
class ISettings;
}

namespace DrugsWidget {
namespace Internal {
namespace Ui {
class ProtocolPreferencesWidget;
}

class ProtocolPreferencesWidget : public QWidget
{
    Q_OBJECT
public:
    ProtocolPreferencesWidget(QWidget *parent = 0);
    ~ProtocolPreferencesWidget();

    void setDataToUi();
    static void writeDefaultSettings(Core::ISettings *s = 0);

public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);

protected:
    void changeEvent(QEvent *e);

private:
    Ui::ProtocolPreferencesWidget *ui;
    QHash<QString, QString> m_ButtonChoices;
};

class ProtocolPreferencesPage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    ProtocolPreferencesPage(QObject *parent = 0);
    ~ProtocolPreferencesPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    QString helpPage();

    static void writeDefaultSettings(Core::ISettings *s) {Internal::ProtocolPreferencesWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);

private:
    QPointer<Internal::ProtocolPreferencesWidget> m_Widget;
};

}  // End namespace Internal
}  // End namespace DrugsWidget

#endif // PROTOCOLPREFERENCESPAGE_H
