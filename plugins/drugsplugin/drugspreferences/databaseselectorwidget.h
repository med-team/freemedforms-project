/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATABASESELECTORWIDGET_H
#define DATABASESELECTORWIDGET_H

#include <coreplugin/ioptionspage.h>

#include <QPointer>
#include <QWidget>
#include <QStringListModel>

/**
 * \file ./plugins/drugsplugin/drugspreferences/databaseselectorwidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/


namespace DrugsDB {
class DatabaseInfos;
}

namespace Core {
class ISettings;
}

namespace DrugsWidget {
namespace Internal {
class DatabaseSelectorWidgetPrivate;
namespace Ui {
class DatabaseSelectorWidget;
}

class DatabaseSelectorWidget : public QWidget
{
    Q_OBJECT
public:
    DatabaseSelectorWidget(QWidget *parent = 0);
    ~DatabaseSelectorWidget();

    void setDataToUi();
    static void writeDefaultSettings(Core::ISettings *s = 0);

public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);

protected:
    void changeEvent(QEvent *e);

private Q_SLOTS:
    void updateDatabaseInfos(int row);

private:
    Ui::DatabaseSelectorWidget *ui;
    DatabaseSelectorWidgetPrivate *d;
};

class DrugsDatabaseSelectorPage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    DrugsDatabaseSelectorPage(QObject *parent = 0);
    ~DrugsDatabaseSelectorPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    QString helpPage();

    static void writeDefaultSettings(Core::ISettings *s) {Internal::DatabaseSelectorWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);

private:
    QPointer<Internal::DatabaseSelectorWidget> m_Widget;
};

}  // End namespace Internal
}  // End namespace DrugsWidget


#endif // DATABASESELECTORWIDGET_H
