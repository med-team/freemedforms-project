/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "datapackpreference.h"

#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/itheme.h>
#include <coreplugin/constants_icons.h>
#include <coreplugin/constants_tokensandsettings.h>
#include <coreplugin/constants_menus.h>

#include <utils/log.h>
#include <translationutils/constants.h>
#include <translationutils/trans_current.h>

#include <datapackutils/datapackcore.h>
#include <datapackutils/ipackmanager.h>
#include <datapackutils/pack.h>
#include <datapackutils/widgets/serverpackeditor.h>

#include <QHBoxLayout>

#include "ui_datapackpreference.h"

using namespace DataPackPlugin;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Core::ISettings *settings()  { return Core::ICore::instance()->settings(); }
static inline DataPack::DataPackCore &dataPackCore() { return DataPack::DataPackCore::instance(); }
static inline DataPack::IPackManager *packManager() { return dataPackCore().packManager(); }

DataPackPreferencePage::DataPackPreferencePage(QObject *parent) :
        IOptionsPage(parent), m_Widget(0)
{
    setObjectName("DataPackPreferencePage");
}

DataPackPreferencePage::~DataPackPreferencePage()
{
    if (m_Widget) {
        delete m_Widget;
        m_Widget = 0;
    }
}

QString DataPackPreferencePage::id() const { return objectName(); }
QString DataPackPreferencePage::displayName() const { return tkTr(Trans::Constants::DATAPACK_MANAGER); }
QString DataPackPreferencePage::category() const { return tkTr(Trans::Constants::DATAPACK); }
QString DataPackPreferencePage::title() const {return tkTr(Trans::Constants::DATAPACK_MANAGER); }
int DataPackPreferencePage::sortIndex() const
{
    return Core::Constants::OPTIONINDEX_MAIN + 20;
}

void DataPackPreferencePage::resetToDefaults()
{
    m_Widget->writeDefaultSettings(settings());
    m_Widget->setDataToUi();
}

void DataPackPreferencePage::apply()
{
    if (!m_Widget) {
        return;
    }
    m_Widget->saveToSettings(settings());
}
void DataPackPreferencePage::checkSettingsValidity()
{
    QHash<QString, QVariant> defaultvalues;

    foreach(const QString &k, defaultvalues.keys()) {
        if (settings()->value(k).isNull())
            settings()->setValue(k, defaultvalues.value(k));
    }
    settings()->sync();
}

void DataPackPreferencePage::finish() { delete m_Widget; }
QString DataPackPreferencePage::helpPage()
{
    return QString();
}

QWidget *DataPackPreferencePage::createPage(QWidget *parent)
{
    if (m_Widget)
        delete m_Widget;
    m_Widget = new DataPackPreferenceWidget(parent);
    return m_Widget;
}

DataPackPreferenceWidget::DataPackPreferenceWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DataPackPreference)
{
    ui->setupUi(this);
    QHBoxLayout *lay = new QHBoxLayout(this);
    lay->setMargin(0);
    setLayout(lay);
    DataPack::ServerPackEditor *packEditor = new DataPack::ServerPackEditor(this);
    lay->addWidget(packEditor);
    setDataToUi();
    connect(packManager(), SIGNAL(packInstalled(DataPack::Pack)), this, SLOT(packInstalled(DataPack::Pack)));
}

DataPackPreferenceWidget::~DataPackPreferenceWidget()
{
    delete ui;
}

void DataPackPreferenceWidget::setDataToUi()
{
}

void DataPackPreferenceWidget::writeDefaultSettings(Core::ISettings *s)
{
    Q_UNUSED(s);
}

void DataPackPreferenceWidget::saveToSettings(Core::ISettings *s)
{
    Core::ISettings *set = s;
    if (!set) {
        set = settings();
    }
}

void DataPackPreferenceWidget::packInstalled(const DataPack::Pack &pack)
{
    WARN_FUNC << pack;
}

void DataPackPreferenceWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
