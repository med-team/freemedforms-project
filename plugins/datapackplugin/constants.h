/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATAPACK_PLUGIN_CONSTANTS_H
#define DATAPACK_PLUGIN_CONSTANTS_H

namespace DataPackPlugin {
namespace Constants {

const char * const  DATAPACKCONSTANTS_TR_CONTEXT = "DataPackPlug";

const char * const TOGGLEPACKMANAGER_TEXT = QT_TRANSLATE_NOOP("DataPackPlug", "Data pack manager");
const char * const I_TOGGLEPACKMANAGER = "package.png";
const char * const A_TOGGLE_PACKMANAGER = "aTooglePackManager";

}
}

#endif // CONSTANTS_H
