#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

DEFINES += DATAPACK_PLUGIN_LIBRARY

include(../fmf_plugins.pri)
include(datapackplugin_dependencies.pri)

HEADERS = $${PWD}/datapackplugin.h $${PWD}/datapack_exporter.h \
    $${PWD}/constants.h \
    $${PWD}/datapackpreference.h

SOURCES = $${PWD}/datapackplugin.cpp \
    $${PWD}/datapackpreference.cpp

FORMS += \
    $${PWD}/datapackpreference.ui

# include translations
TRANSLATION_NAME = datapack
include($${SOURCES_ROOT_PATH}/buildspecs/translations.pri)



