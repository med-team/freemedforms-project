/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ZIPCODES_ZIPCORE_H
#define ZIPCODES_ZIPCORE_H

#include <zipcodesplugin/zipcodes_exporter.h>
#include <QObject>
#include <QSqlDatabase>

/**
 * \file ./plugins/zipcodesplugin/zipcore.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DataPack {
class Pack;
}

namespace ZipCodes {
class ZipCountryCompleters;
namespace Internal {
class ZipCodesPlugin;
class ZipCorePrivate;
} // namespace Internal

class ZIPCODES_EXPORT ZipCore : public QObject
{
    Q_OBJECT
    friend class ZipCodes::Internal::ZipCodesPlugin;
    friend class ZipCodes::ZipCountryCompleters;

protected:
    explicit ZipCore(QObject *parent = 0);
    bool initialize();

public:
    static ZipCore &instance();
    ~ZipCore();

    bool isDatabaseAvailable() const;
    QSqlDatabase &database() const;

Q_SIGNALS:
    void databaseRefreshed();

public Q_SLOTS:

private Q_SLOTS:
    void packChanged(const DataPack::Pack &pack);

private:
    Internal::ZipCorePrivate *d;
    static ZipCore *_instance;
};

} // namespace ZipCodes

#endif  // ZIPCODES_ZIPCORE_H

