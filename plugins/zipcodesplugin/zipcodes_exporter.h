/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ZIPCODES_EXPORTER_H
#define ZIPCODES_EXPORTER_H

#include <qglobal.h>

#if defined(ZIPCODES_LIBRARY)
#define ZIPCODES_EXPORT Q_DECL_EXPORT
#else
#define ZIPCODES_EXPORT Q_DECL_IMPORT
#endif

#endif
