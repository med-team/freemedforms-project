/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric MAEKER, MD <eric.maeker@gmail.com>             *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ZIPCODESCOMPLETERS_H
#define ZIPCODESCOMPLETERS_H

#include <zipcodesplugin/zipcodes_exporter.h>
#include <utils/widgets/countrycombobox.h>
#include <utils/widgets/qbuttonlineedit.h>

#include <QObject>
#include <QSqlQueryModel>
#include <QValidator>

QT_BEGIN_NAMESPACE
class QLineEdit;
class QCompleter;
class QModelIndex;
class QAbstractItemView;
class QComboBox;
class QToolButton;
QT_END_NAMESPACE

/**
 * \file ./plugins/zipcodesplugin/zipcodescompleters.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace ZipCodes {
namespace Internal {
class ZipStateProvinceModel;

class ZipCountryModel : public QSqlQueryModel
{
    Q_OBJECT
public:
    enum ColumnRepresentation {
        Id = 0,
        Zip,
        City,
        ExtraCode,
        Country,
        ZipCity,
        CityZip,
        Province,
        NumberOfColumn
    };

    ZipCountryModel(QObject *parent);

    int columnCount(const QModelIndex &) const {return NumberOfColumn;}

    QVariant data(const QModelIndex &index, int role) const;

    bool isCountryAvailable(const QLocale::Country country) const;
    bool exists(const QString &countryIso, const QString &city, const QString &zip, const QString &province) const;

public Q_SLOTS:
    void setCountryFilter(const QString &iso);
    void setCityFilter(const QString &city);
    void setZipFilter(const QString &zip);

private:
    QString currentFilter() const;
    void refreshQuery();

private:
    QString _sqlQuery, _countryIso, _zip, _province, _city;
};

class ZipStateProvinceModel : public QSqlQueryModel
{
    Q_OBJECT
public:
    enum ColumnRepresentation {
        Province = 0
    };

    ZipStateProvinceModel(QObject *parent);
    int columnCount(const QModelIndex &) const {return 1;}
    QVariant data(const QModelIndex &index, int role) const;

public Q_SLOTS:
    void setCountryFilter(const QString &iso);
    void setCityFilter(const QString &city);
    void setZipFilter(const QString &zip);

private:
    QString currentFilter() const;
    void refreshQuery();

private:
    QString _sqlQuery, _countryIso, _zip, _province, _city;
};
}  // End namespace Internal

class ZIPCODES_EXPORT ZipCountryCompleters : public QObject
{
    Q_OBJECT
public:
    explicit ZipCountryCompleters(QObject *parent = 0);
    ~ZipCountryCompleters();

    void setCountryComboBox(Utils::CountryComboBox *box);
    void setStateProvinceComboBox(QComboBox *box);
    void setCityLineEdit(Utils::QButtonLineEdit *country);
    void setZipLineEdit(Utils::QButtonLineEdit *zip);
    void checkData();

private Q_SLOTS:
    void onCompleterIndexActivated(const QModelIndex &index);
    void setCountryFilter(const QLocale::Country country);
    void zipTextChanged();
    void cityTextChanged();
    void setStateProvinceFilter(int);
    void onDatabaseRefreshed();

private:
    void createModel();

private:
    Utils::QButtonLineEdit *m_cityEdit, *m_zipEdit;
    Utils::CountryComboBox *m_countryCombo;
    QComboBox *m_provinceCombo;
    Internal::ZipCountryModel *m_ZipModel, *m_CityModel;
    Internal::ZipStateProvinceModel *m_ProvinceModel;
    QToolButton *m_ZipButton, *m_CityButton;
    bool m_DbAvailable;
};

}  // End namespace ZipCodes

#endif // ZIPCODESCOMPLETERS_H
