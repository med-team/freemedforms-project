#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

DEFINES += ZIPCODES_LIBRARY

include($${PWD}/../fmf_plugins.pri)
include($${PWD}/zipcodesplugin_dependencies.pri )

HEADERS = $${PWD}/zipcodesplugin.h \
    $${PWD}/zipcodes_exporter.h \
    $${PWD}/zipcodescompleters.h \
    $${PWD}/zipcodeswidget.h \
    $${PWD}/zipcore.h

SOURCES = $${PWD}/zipcodesplugin.cpp \
    $${PWD}/zipcodescompleters.cpp \
    $${PWD}/zipcodeswidget.cpp \
    $${PWD}/zipcore.cpp

# include translations
TRANSLATION_NAME = zipcodes
include($${SOURCES_ROOT_PATH}/buildspecs/translations.pri)
