/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
/*!
 * \class ZipCodes::ZipCore
 * Contains central ZipcCodes plugin instances and data.
 */

#include "zipcore.h"

#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>

#include <datapackutils/datapackcore.h>
#include <datapackutils/ipackmanager.h>
#include <datapackutils/pack.h>

#include <utils/log.h>
#include <translationutils/constants.h>
#include <translationutils/trans_database.h>

#include <QFileInfo>
#include <QSqlDatabase>
#include <QDir>

#include <QDebug>

using namespace ZipCodes;
using namespace Trans::ConstantTranslations;

static inline Core::ISettings *settings() {return Core::ICore::instance()->settings();}
static inline DataPack::DataPackCore &dataPackCore() { return DataPack::DataPackCore::instance(); }
static inline DataPack::IPackManager *packManager() { return dataPackCore().packManager(); }

namespace ZipCodes {
namespace Internal {
class ZipCorePrivate
{
public:
    ZipCorePrivate(ZipCore *parent) :
        _initialized(false),
        _dbAvailable(false),
        q(parent)
    {
    }

    ~ZipCorePrivate()
    {
    }

    QString databasePath()
    {
        QString dbRelPath = "/zipcodes/zipcodes.db";
        QString tmp;
        tmp = settings()->dataPackInstallPath() + dbRelPath;
        if (QFileInfo(tmp).exists())
            return settings()->dataPackInstallPath();
        return settings()->dataPackApplicationInstalledPath();
    }

    QString databaseFileName()
    {
        return databasePath() + QDir::separator() + "zipcodes" + QDir::separator() + "zipcodes.db";
    }

    void checkDatabase()
    {
        _dbAvailable = false;
        if (!QFileInfo(databaseFileName()).exists())
            return;
        _db = QSqlDatabase();
        if (QSqlDatabase::connectionNames().contains("ZIPS")) {
            _db = QSqlDatabase::database("ZIPS");
            _dbAvailable = true;
        } else {
            LOG_FOR(q, QString("Trying to open ZipCode database from %1").arg(databaseFileName()));
            _db = QSqlDatabase::addDatabase("QSQLITE", "ZIPS");
            _db.setDatabaseName(databaseFileName());
            _dbAvailable = true;
        }
        if (_dbAvailable) {
            if (!_db.open()) {
                LOG_ERROR_FOR(q, "Unable to open Zip database");
                QSqlDatabase::removeDatabase("ZIPS");
                _db = QSqlDatabase();
                _dbAvailable = false;
            } else {
                LOG_FOR(q, tkTr(Trans::Constants::CONNECTED_TO_DATABASE_1_DRIVER_2).arg("zipcodes").arg("sqlite"));
            }
        }
    }

public:
    QSqlDatabase _db;
    bool _initialized, _dbAvailable;

private:
    ZipCore *q;
};
} // namespace Internal
} // end namespace ZipCodes

ZipCore *ZipCore::_instance = 0;


ZipCodes::ZipCore &ZipCodes::ZipCore::instance() // static
{
    Q_ASSERT(_instance);
    return *_instance;
}

/*! Constructor of the ZipCodes::ZipCore class */
ZipCore::ZipCore(QObject *parent) :
    QObject(parent),
    d(new Internal::ZipCorePrivate(this))
{
    _instance = this;
    setObjectName("ZipCore");
}

/*! Destructor of the ZipCodes::ZipCore class */
ZipCore::~ZipCore()
{
    _instance = 0;
    if (d)
        delete d;
    d = 0;
}

/*! Initializes the object with the default values. Return true if initialization was completed. */
bool ZipCore::initialize()
{
    if (d->_initialized)
        return true;
    d->checkDatabase();
    connect(packManager(), SIGNAL(packInstalled(DataPack::Pack)), this, SLOT(packChanged(DataPack::Pack)));
    connect(packManager(), SIGNAL(packRemoved(DataPack::Pack)), this, SLOT(packChanged(DataPack::Pack)));
    d->_initialized = true;
    return true;
}

QSqlDatabase &ZipCore::database() const
{
    return d->_db;
}

bool ZipCore::isDatabaseAvailable() const
{
    return d->_dbAvailable;
}

void ZipCore::packChanged(const DataPack::Pack &pack)
{
    if (pack.dataType() == DataPack::Pack::ZipCodes) {
        QSqlDatabase::removeDatabase("ZIPS");
        d->checkDatabase();
        Q_EMIT databaseRefreshed();
    }
}

