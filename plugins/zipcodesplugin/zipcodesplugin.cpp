/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "zipcodesplugin.h"
#include "zipcore.h"

#include <utils/log.h>

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/translators.h>

#include <QtPlugin>
#include <QDebug>

using namespace ZipCodes;
using namespace Internal;

ZipCodesPlugin::ZipCodesPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating ZipCodesPlugin";

    _core = new ZipCore(this);
}

ZipCodesPlugin::~ZipCodesPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool ZipCodesPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "ZipCodesPlugin::initialize";
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);


    Core::ICore::instance()->translators()->addNewTranslator("plugin_zipcodes");

    _core->initialize();

    return true;
}

void ZipCodesPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "ZipCodesPlugin::extensionsInitialized";



    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));
}

ExtensionSystem::IPlugin::ShutdownFlag ZipCodesPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(ZipCodesPlugin)
