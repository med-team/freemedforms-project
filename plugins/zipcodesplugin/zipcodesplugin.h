/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ZIPCODES_PLUGIN_H
#define ZIPCODES_PLUGIN_H

#include <extensionsystem/iplugin.h>

#include <QtCore/QObject>

/**
 * \file ./plugins/zipcodesplugin/zipcodesplugin.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace ZipCodes {
class ZipCore;
namespace Internal {

class ZipCodesPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.freemedforms.FreeMedForms.ZipCodesPlugin" FILE "ZipCodes.json")

public:
    ZipCodesPlugin();
    ~ZipCodesPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

private:
    ZipCore *_core;
};

} // namespace Internal
} // namespace ZipCodes

#endif  // End ZIPCODES_PLUGIN_H
