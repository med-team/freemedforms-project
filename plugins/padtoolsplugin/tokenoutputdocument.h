/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>                  *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/
#ifndef PADTOOLS_TOKENOUTPUTDOCUMENT_H
#define PADTOOLS_TOKENOUTPUTDOCUMENT_H

#include <texteditorplugin/texteditor.h>

/**
 * \file ./plugins/padtoolsplugin/tokenoutputdocument.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace PadTools {
namespace Internal {
class PadDocument;
class PadItem;
class PadFragment;
class TokenHighlighterEditorPrivate;
class TokenOutputDocumentPrivate;

class TokenHighlighterEditor : public Editor::TextEditor
{
    Q_OBJECT

public:
    TokenHighlighterEditor(QWidget *parent = 0);
    ~TokenHighlighterEditor();

    virtual void setPadDocument(PadDocument *pad);
    PadDocument *padDocument() const;

protected Q_SLOTS:
    void onPadCleared();
    void onDocumentAnalyzeReset();
    void cursorPositionChanged();
    void connectPadDocument();
    void disconnectPadDocument();
    void connectOutputDocumentChanges();
    void disconnectOutputDocumentChanges();
    void contentChanged(const int pos, const int rm, const int ins);
    void onPadFragmentAboutToRemoved(PadFragment *fragment);

public Q_SLOTS:
    void hightlight(PadItem *item);

protected:
    bool isPadItem(int textEditorPos);
    bool isPadCore(int textEditorPos);
    virtual bool event(QEvent *event);
    virtual bool eventFilter(QObject *o, QEvent *e);

Q_SIGNALS:
    void highlighting(PadItem *item);

private:
    TokenHighlighterEditorPrivate *d_th;
};

class TokenOutputDocument : public TokenHighlighterEditor
{
    Q_OBJECT
    friend class TokenOutputDocumentPrivate;

public:
    TokenOutputDocument(QWidget *parent = 0);
    ~TokenOutputDocument();

private Q_SLOTS:
    void contextMenu(const QPoint &pos);
    void editTokenUnderCursor();

protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dragLeaveEvent(QDragLeaveEvent *event);
    void dropEvent(QDropEvent *event);
    bool eventFilter(QObject *o, QEvent *e);

private:
    Internal::TokenOutputDocumentPrivate *d;
};

}  // namespace Internal
}  // namespace PadTools

#endif // PADTOOLS_TOKENOUTPUTDOCUMENT_H

