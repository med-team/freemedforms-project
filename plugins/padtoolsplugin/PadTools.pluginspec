<plugin name="PadTools" version="0.0.1" compatVersion="0.0.1">
    <vendor>FreeMedForms</vendor>
    <copyright>(C) 2008-2017 Eric Maeker, MD</copyright>
    <license>See application's license</license>

    <!-- TODO: Eric, what is this plugin about? I couldn't determine the exact purpose -->
    <description>Manages pads.</description>
    <url>http://www.ericmaeker.fr/FreeMedForms/</url>
    <dependencyList>
        <dependency name="Core" version="0.0.1"/>
        <dependency name="TextEditor" version="0.0.1"/>
    </dependencyList>
</plugin>
