/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>,                *
 *                    Guillaume Denry <guillaume.denry@gmail.com>          *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/





#include "pad_document.h"
#include "pad_item.h"
#include "pad_string.h"
#include "pad_analyzer.h"
#include "tokenpool.h"
#include "padtoolscore.h"

#include <utils/log.h>
#include <utils/global.h>

#include <QString>
#include <QTextCursor>
#include <QTextDocument>
#include <QTextDocumentFragment>

#include <QDebug>

using namespace PadTools;
using namespace Internal;

static inline PadTools::Internal::PadToolsCore &padCore() {return PadTools::Internal::PadToolsCore::instance();}

PadPositionTranslator::PadPositionTranslator()
{
}

void PadPositionTranslator::clear()
{
    _translations.clear();
}

void PadPositionTranslator::addOutputTranslation(const int outputPos, const int length)
{
    _translations.insertMulti(outputPos, length);
}

void PadPositionTranslator::addRawTranslation(const int rawPos, const int length)
{
    int output = rawToOutput(rawPos);
    addOutputTranslation(output, -length);
}

int PadPositionTranslator::deltaForSourcePosition(const int outputPos)
{
    int delta = 0;
    foreach(int begin, _translations.uniqueKeys()) {
        if (begin > outputPos)
            break;

        foreach(int size, _translations.values(begin)) {
            int end = begin + size;
            if ((outputPos >= begin) && (outputPos <= end)) {
                delta += outputPos - begin;
                continue;
            }
            if (begin <= outputPos)
                delta += size;
        }
    }
    return delta;
}

int PadPositionTranslator::rawToOutput(const int rawPos)
{
    int pos = rawPos;
    foreach(int begin, _translations.uniqueKeys()) {
        if (pos > begin) {
            foreach(int size, _translations.values(begin)) {
                if (pos + size < begin)
                    pos = begin;
                else
                    pos += size;
            }
        }
    }
    return pos>0 ? pos : 0;
}

int PadPositionTranslator::outputToRaw(const int outputPos)
{
    int pos = outputPos - deltaForSourcePosition(outputPos);
    return pos>0 ? pos : 0;
}

void PadPositionTranslator::debug()
{
    qWarning() << "Translations" << _translations;
}


PadDocument::PadDocument(QTextDocument *source, QObject *parent) :
    QObject(parent),
    _docSource(source),
    _docOutput(new QTextDocument(this)),
    _tokenPool(padCore().tokenPool()),
    _timer(0),
    _contentType(ContentAutoType)
{
}


PadDocument::PadDocument(QObject *parent) :
    QObject(parent),
    _docSource(0),
    _docOutput(new QTextDocument(this)),
    _tokenPool(padCore().tokenPool()),
    _timer(0),
    _contentType(ContentAutoType)
{
}

PadDocument::~PadDocument()
{
}

void PadDocument::clear()
{
    Q_EMIT aboutToClear();
    qDeleteAll(_fragments);
    _fragments.clear();
    _items.clear();
    if (_docOutput)
        _docOutput->clear();
    _posTrans.clear();
    Q_EMIT cleared();
}

void PadDocument::setSource(QTextDocument *source)
{
    Q_ASSERT(source);
    clear();
    _docSource = source;
}

void PadDocument::setOutput(QTextDocument *output)
{
    Q_ASSERT(output);
    _docOutput = output;
}


void PadDocument::setTokenPool(Core::ITokenPool *pool)
{
    _tokenPool = pool;
}

QString PadDocument::fragmentRawSource(PadFragment *fragment) const
{
    if (!fragment)
        return QString::null;
    if (_docSource) {
        return _docSource->toPlainText().mid(fragment->start(), fragment->end() - fragment->start());
    }
    return QString::null;
}

QString PadDocument::fragmentHtmlOutput(PadFragment *fragment) const
{
    if (!fragment)
        return QString::null;
    if (_docOutput) {
        QTextCursor cursor(_docOutput);
        cursor.setPosition(fragment->outputStart());
        cursor.setPosition(fragment->outputEnd(), QTextCursor::KeepAnchor);
        return cursor.selection().toHtml();
    }
    return QString::null;
}

void PadDocument::addChild(PadFragment *fragment)
{
    PadItem *item = dynamic_cast<PadItem *>(fragment);
    if (item)
        _items << item;
    PadFragment::addChild(fragment);
}

void PadDocument::removeChild(PadFragment *fragment)
{
    PadItem *item = dynamic_cast<PadItem *>(fragment);
    if (item)
        _items.removeAll(item);
    PadFragment::removeChild(fragment);
}

void PadDocument::removeAndDeleteFragment(PadFragment *fragment)
{
    if (_docSource) {
        QTextCursor cursor(_docSource);
        cursor.setPosition(fragment->start());
        cursor.setPosition(fragment->end(), QTextCursor::KeepAnchor);
        cursor.removeSelectedText();
    }
    if (_docOutput) {
        QTextCursor cursor(_docOutput);
        cursor.setPosition(fragment->outputStart());
        cursor.setPosition(fragment->outputEnd(), QTextCursor::KeepAnchor);
        cursor.removeSelectedText();
    }
    PadItem *item = dynamic_cast<PadItem*>(fragment);
    if (item)
        _items.removeAll(item);
    Q_EMIT padFragmentAboutToRemoved(item);
    PadFragment::removeAndDeleteFragment(fragment);
}

void PadDocument::sortChildren()
{
    PadFragment::sortChildren();
    qSort(_items);
}

void PadDocument::beginRawSourceAnalyze()
{
    Q_EMIT rawSourceAnalyzeStarted();
}

void PadDocument::endRawSourceAnalyze()
{
    Q_EMIT rawSourceAnalyseFinished();
}

void PadDocument::debug(int indent) const
{
	QString str(indent, ' ');
	str += "[pad]";
	qDebug("%s", qPrintable(str));
	foreach (PadFragment *fragment, _fragments){
        fragment->debug(indent + 2);
	}
}

PadItem *PadDocument::padItemForOutputPosition(int p) const
{
    PadFragment *fragment = padFragmentForOutputPosition(p);
    PadFragment *parent = fragment;
    PadItem *item = dynamic_cast<PadItem*>(fragment);
    while (parent && !item) {
        parent = parent->parent();
        item = dynamic_cast<PadItem*>(parent);
    }
    return item;
}

PadItem *PadDocument::padItemForSourcePosition(int p) const
{
    PadFragment *fragment = padFragmentForSourcePosition(p);
    PadItem *item = dynamic_cast<PadItem*>(fragment);
    PadFragment *parent = fragment;
    while (parent && !item) {
        parent = parent->parent();
        item = dynamic_cast<PadItem*>(parent);
    }
    return item;
}

PadFragment *PadDocument::padFragmentForSourcePosition(int rawPos) const
{
    if (_fragments.isEmpty()) {
        if (IN_RANGE_STRICTLY(rawPos, _start, _end))
            return (PadFragment *)this;
    } else {
        foreach(PadFragment *fragment, _fragments) {
            if (fragment->start() < rawPos && rawPos < fragment->end())
                return fragment->padFragmentForSourcePosition(rawPos);
        }
    }
    return 0;
}

PadFragment *PadDocument::padFragmentForOutputPosition(int outputPos) const
{
    if (_fragments.isEmpty()) {
        if (IN_RANGE_STRICTLY(outputPos, _outputStart, _outputEnd)) // _outputStart < outputPos && outputPos < _outputEnd)
            return (PadFragment *)this;
    } else {
        foreach(PadFragment *fragment, _fragments) {
            if (IN_RANGE_STRICTLY(outputPos, fragment->outputStart(), fragment->outputEnd()))
                return fragment->padFragmentForOutputPosition(outputPos);
        }
    }
    return 0;
}


QTextCursor PadDocument::rawSourceCursorForOutputPosition(int outputPos)
{
    Q_ASSERT(_docSource);
    Q_ASSERT(_docOutput);

    QTextCursor cursor(_docSource);
    cursor.setPosition(positionTranslator().outputToRaw(outputPos));
    return cursor;
}

void PadDocument::outputPosChanged(const int oldPos, const int newPos)
{
    foreach(PadItem *item, _items)
        item->outputPosChanged(oldPos, newPos);

    foreach(PadFragment *f, _fragmentsToDelete) {
        if (f->parent())
            f->parent()->removeChild(f);
    }
    qDeleteAll(_fragmentsToDelete);
    _fragmentsToDelete.clear();
}

static void syncOutputRange(PadFragment *f)
{
    f->setOutputStart(f->start());
    f->setOutputEnd(f->end());
    foreach(PadFragment *frag, f->children())
        syncOutputRange(frag);
}

void PadDocument::run(QMap<QString,QVariant> &tokens)
{
    Q_ASSERT(_docSource);
    if (!_docSource)
        return;
    Q_EMIT beginTokenReplacement();

    if (!_docOutput) {
        _docOutput = new QTextDocument(this);
    }
    _docOutput->clear();
    _docOutput->setHtml(_docSource->toHtml());

    foreach (PadFragment *fragment, _fragments)
        syncOutputRange(fragment);

    foreach (PadFragment *fragment, _fragments)
        fragment->run(tokens, this);


    Q_EMIT endTokenReplacement();
}

void PadDocument::toOutput(Core::ITokenPool *pool, TokenReplacementMethod method)
{
    Q_ASSERT(_docSource);
    if (!_docSource)
        return;
    Q_EMIT beginTokenReplacement();

    if (!_docOutput) {
        _docOutput = new QTextDocument(this);
    }
    _docOutput->clear();
    _docOutput->setHtml(_docSource->toHtml());

    foreach (PadFragment *fragment, _fragments)
        syncOutputRange(fragment);

    foreach (PadFragment *fragment, _fragments)
        fragment->toOutput(pool, this, method);


    Q_EMIT endTokenReplacement();
}

static void syncRawRange(PadFragment *f)
{
    f->setStart(f->outputStart());
    f->setEnd(f->outputEnd());
    foreach(PadFragment *frag, f->children())
        syncRawRange(frag);
}

void PadDocument::toRaw(PadDocument *doc)
{
    Q_ASSERT(!doc);
    if (doc)
        return;
    Q_ASSERT(_docOutput);
    if (!_docOutput)
        return;
    Q_ASSERT(_docSource);
    if (!_docSource)
        return;

    _docSource->clear();
    _posTrans.clear();
    _docSource->setHtml(_docOutput->toHtml());

    foreach(PadFragment *fragment, _fragments)
        syncRawRange(fragment);

    foreach(PadFragment *fragment, _fragments)
        fragment->toRaw(this);
}

void PadDocument::softReset()
{
    QTime c;
    c.start();

    qDeleteAll(_fragments);
    _fragments.clear();
    _items.clear();
    _docOutput->clear();

    PadAnalyzer a;
    a.analyze(_docSource, this);
    if (_tokenPool)
        toOutput(_tokenPool);

    Utils::Log::logTimeElapsed(c, "PadTools::PadDocument", "reset");
}

void PadDocument::reset()
{
    clear();
    softReset();
    return;
}
