/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric Maeker                                                       *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PADTOOLS_INTERNAL_PADTOOLSCONTEXTUALWIDGETMANAGER_H
#define PADTOOLS_INTERNAL_PADTOOLSCONTEXTUALWIDGETMANAGER_H

#include <coreplugin/contextmanager/icontext.h>
#include <QObject>
#include <QPointer>

/**
 * \file ./plugins/padtoolsplugin/padtoolscontextualwidgetmanager.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class IContext;
}

namespace PadTools {
namespace Internal {
class PadWriter;
class PadToolsContextualWidget;

class PadToolsActionHandler : public QObject
{
    Q_OBJECT
public:
    PadToolsActionHandler(QObject *parent = 0);
    virtual ~PadToolsActionHandler() {}

    void setCurrentView(PadWriter *view);

private Q_SLOTS:
    void onViewOutputRequested();
    void onShowSourceRequested();

private Q_SLOTS:
    void updateActions();

protected:
    QAction *aShowSource;
    QAction *aViewOutput;

    QPointer<PadWriter> m_CurrentView;
};

class PadToolsContextualWidgetManager : public PadToolsActionHandler
{
    Q_OBJECT

public:
    explicit PadToolsContextualWidgetManager(QObject *parent = 0);
    ~PadToolsContextualWidgetManager();

    PadWriter *currentView() const;

private Q_SLOTS:
    void updateContext(Core::IContext *object, const Core::Context &additionalContexts);
};

} // namespace Internal
} // namespace PadTools

#endif // PADTOOLS_INTERNAL_PADTOOLSCONTEXTUALWIDGETMANAGER_H

