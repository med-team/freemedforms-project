/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>,                *
 *                    Guillaume Denry <guillaume.denry@gmail.com>          *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/


/** \fn virtual void PadTools::PadFragment::setParent(PadFragment *parent)
 * Define the parent of the fragment
 */

/** \fn virtual PadFragment *PadTools::PadFragment::parent() const
 * Return the parent of the fragment
 */

/** \fn virtual void PadTools::PadFragment::debug(int indent = 0) const = 0;
 * Debug to console
 */

/** \fn virtual int PadTools::PadFragment::id() const
 * Returns the id of the fragment
 */

/** \fn virtual void PadTools::PadFragment::setId(int id)
 * Defines the id of the fragment
 */

/** \fn int PadTools::PadFragment::start() const { return _start; }
 * Returns the start position in the raw source string/document
 */

/** \fn void PadTools::PadFragment::setStart(int start)
 * Defines the start position in the raw source string/document
 */

/** \fn int PadTools::PadFragment::end() const
 * Returns the end position in the raw source string/document
 */

/** \fn void PadTools::PadFragment::setEnd(int end)
 * Defines the end position in the raw source string/document
 */

/** \fn int PadTools::PadFragment::rawLength() const
 * Return the length of the fragment in the raw source string/document
 */

/** \fn int PadTools::PadFragment::outputLength() const
 * Return the length of the fragment in the output string/document
 */

/** \fn void PadTools::PadFragment::move(int nbChars);
 * Moves the fragment of \e nbChars (if \e nbChars is negative, go backward).
 * Modifies fragment begin and end tags.
 */

/** \fn void PadTools::PadFragment::moveEnd(int nbOfChars);
 * Moves the end of the fragment of \e nbOfChars. The length is modified.
 */

/** \fn void PadTools::PadFragment::setOutputStart(const int pos)
 * Defines the position of the fragment in the token output document.
 */

/** \fn void PadTools::PadFragment::setOutputEnd(const int pos)
 * Defines the position of the fragment in the token output document.
 */

/** \fn int PadTools::PadFragment::outputStart() const
 * Returns the start position in the output QTextDocument.
 * This is processed by the toOutput() member.
 */

/** \fn int PadTools::PadFragment::outputEnd() const
 * Returns the end position in the output QTextDocument.
 * This is processed by the toOutput() member.
 */

/** \fn void PadTools::PadFragment::resetOutputRange()
 * Removes (set to -1) all output limits to the object and its children.
 */

/** \fn bool PadTools::PadFragment::containsRawPosition(const int pos) const
 * Returns true if the fragment contains raw source \e position (position in the raw document).
 */

/** \fn bool PadTools::PadFragment::containsOutputPosition(const int pos) const
 * Returns true idf the fragment contains the output \e position
 */

/** \fn void PadTools::PadFragment::setToolTip(const QString &tooltip)
 * Define the tooltip of the fragment
 */

/** \fn const QString &PadTools::PadFragment::toolTip() const
 * Returns the tooltip of the fragment
 */

/** \fn void PadTools::PadFragment::setUserData(const QString &key, const QVariant &value)
 * Stores some user data with a specific \e key related to a \e value.
 */

/** \fn QVariant PadTools::PadFragment::userData(const QString &key) const
 * Returns the user date.
 */

/** \fn virtual void PadTools::PadFragment::run(QMap<QString,QVariant> &tokens, PadDocument *document) = 0;
 * \obsolete
 * Run this fragment over some tokens inside the output QTextDocument (which is initially a clone of the source).
 */

/** \fn virtual void PadTools::PadFragment::toOutput(Core::ITokenPool *pool, PadDocument *document, TokenReplacementMethod method) = 0;
 * Replaces the tokens from the \e pool in a PadDocument. Use the
 * \e method to define the way tokens are replaced.
 * \sa PadTools::PadFragment::TokenReplacementMethod
 */

/** \fn virtual void PadTools::PadFragment::toRaw(PadDocument *doc) = 0;
 * Transform a PadDocument to a pure raw source.
 */

/** \fn virtual QList<PadFragment*> PadTools::PadFragment::children() const
 * Returns the list of PadTools::PadFragment children
 */

#include "pad_fragment.h"

#include <utils/global.h>

#include <QTextDocument>
#include <QTextCursor>
#include <QTextDocumentFragment>
#include <QTextBlock>
#include <QClipboard>
#include <QApplication>

#include <QDebug>

using namespace PadTools;
using namespace Internal;

enum { WarnOutputPosChangedDebugging = false };

QList<PadFragment *> PadTools::Internal::PadFragment::_fragmentsToDelete;

PadFragment::PadFragment(PadFragment *parent) :
    _start(-1), _end(-1),
    _outputStart(-1), _outputEnd(-1),
    _parent(parent),
    _id(-1)
{
}

PadFragment::~PadFragment()
{
    qDeleteAll(_fragments);
    _fragments.clear();
    _parent = 0;
}

void PadFragment::clear()
{
    qDeleteAll(_fragments);
    _fragments.clear();
    _parent = 0;
    _start = -1;
    _end = -1;
    _outputStart = -1;
    _outputEnd = -1;
    _id = -1;
}

void PadFragment::addChild(PadFragment *fragment)
{
    fragment->setParent(this);
    _fragments << fragment;
}

void PadFragment::removeChild(PadFragment *fragment)
{
    _fragments.removeAll(fragment);
}

void PadFragment::removeAndDeleteFragment(PadFragment *fragment)
{
    if (_fragments.contains(fragment)) {
        _fragments.removeAll(fragment);
        delete fragment;
        fragment = 0;
    }
}

void PadFragment::sortChildren()
{
    qSort(_fragments.begin(), _fragments.end(), PadFragment::lessThan);
    foreach(PadFragment *f, _fragments)
        f->sortChildren();
}


bool PadFragment::containsRawPosition(const int pos) const
{
    return IN_RANGE(pos, _start, _end);
}


bool PadFragment::containsOutputPosition(const int pos) const
{
    return IN_RANGE(pos, _outputStart, _outputEnd);
}


bool PadFragment::isBeforeOutputPosition(const int pos) const
{
    return pos > _outputEnd;
}


bool PadFragment::isAfterOutputPosition(const int pos) const
{
    return pos < _outputStart;
}

PadFragment *PadFragment::padFragmentForSourcePosition(int pos) const
{
    if (!containsRawPosition(pos))
        return 0;
    if (_fragments.isEmpty())
        return (PadFragment*)(this);
    PadFragment *child = 0;
    foreach(PadFragment *frag, _fragments) {
        PadFragment *test = frag->padFragmentForSourcePosition(pos);
        if (test)
            child = test;
    }
    return child;
}

PadFragment *PadFragment::padFragmentForOutputPosition(int pos) const
{


    if (!containsOutputPosition(pos))
        return 0;
    if (_fragments.isEmpty())
        return (PadFragment*)(this);
    PadFragment *child = (PadFragment *)this;
    foreach(PadFragment *frag, _fragments) {
        PadFragment *test = frag->padFragmentForOutputPosition(pos);
        if (test)
            child = test;
    }
    return child;
}

void PadFragment::outputPosChanged(const int oldPos, const int newPos)
{
    if (_outputStart == -1 && _outputEnd == -1)
        return;
    QString debug;
    int delta = newPos - oldPos;
    if (WarnOutputPosChangedDebugging)
        debug = QString("outputPosChanged : Fragment %1 (%2:%3)\n"
                        "    oldPos: %4; newPos: %5; delta: %6 \n")
                .arg(_id)
                .arg(_outputStart).arg(_outputEnd)
                .arg(oldPos).arg(newPos).arg(delta);

    if (containsOutputPosition(oldPos)) {
        if (WarnOutputPosChangedDebugging)
            debug += QString("    oldPos is inside token; moveEnd\n");
        if (delta < 0) {
            if (_outputStart > newPos)
                _outputStart = newPos;
            moveOutputEnd(delta);
        } else {
            moveOutputEnd(delta);
        }
        foreach(PadFragment *f, children()) {
            if (f!=this)
                f->outputPosChanged(oldPos, newPos);
        }
    } else {
        if (WarnOutputPosChangedDebugging)
            debug += QString("    move: %1\n").arg((_outputStart > oldPos));
        if (isAfterOutputPosition(oldPos)) {
            translateOutput(delta);
            foreach(PadFragment *f, children()) {
                if (f!=this)
                    f->outputPosChanged(oldPos, newPos);
            }
        } else {
            if (delta<0) {
                if (IN_RANGE(_outputStart, newPos, oldPos) && IN_RANGE(_outputEnd, newPos, oldPos)) {
                    if (WarnOutputPosChangedDebugging)
                        debug += QString("    fragment removed\n");
                    resetOutputRange();
                    _fragmentsToDelete << this;
                }
            }
        }
    }

    if (WarnOutputPosChangedDebugging) {
        int b = debug.indexOf("\n");
        debug.insert(b, QString(" -> (%1;%2)").arg(_outputStart).arg(_outputEnd));
        qWarning() << debug;
    }
}

void PadFragment::translateOutput(int nbChars)
{
    _outputStart+=nbChars;
    _outputEnd+=nbChars;
}

void PadFragment::moveOutputEnd(int nbOfChars)
{
    if (_outputEnd + nbOfChars < _outputStart) {
        _outputEnd = _outputStart;
    } else {
        _outputEnd += nbOfChars;
    }
}

bool PadFragment::lessThan(PadFragment *first, PadFragment *second)
{
    return first->_outputStart < second->_outputStart;
}
