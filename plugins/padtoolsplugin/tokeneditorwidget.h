/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>,                *
 *                    Guillaume Denry <guillaume.denry@gmail.com>          *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/
#ifndef PADTOOLS_TOKENEDITORWIDGET_H
#define PADTOOLS_TOKENEDITORWIDGET_H

#include <QWidget>

QT_BEGIN_NAMESPACE
class QModelIndex;
QT_END_NAMESPACE

/**
 * \file ./plugins/padtoolsplugin/tokeneditorwidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/


namespace PadTools {
namespace Internal {
class TokenModel;
class PadItem;

namespace Ui {
class TokenEditorWidget;
}

class TokenEditorWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TokenEditorWidget(QWidget *parent = 0);
    ~TokenEditorWidget();

    void setTokenModel(TokenModel *model);

public Q_SLOTS:
    void clear();

    void setCurrentIndex(const QModelIndex &index);
    void setTokenUid(const QString &uid);

    void setConditionnalBeforeHtml(const QString &html);
    void setConditionnalAfterHtml(const QString &html);

    void setConditionnalBeforePlainText(const QString &txt);
    void setConditionnalAfterPlainText(const QString &txt);

    QString toRawSourceHtml() const;
    void getOutput(QString &html, PadItem &item, int startingOutputPos) const;

private:
    Ui::TokenEditorWidget *ui;
    QString _tokenUid;
    TokenModel *_model;
};

} // namespace Internal
} // namespace PadTools

#endif // PADTOOLS_TOKENEDITORWIDGET_H
