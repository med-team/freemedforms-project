/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>,                *
 *                    Guillaume Denry <guillaume.denry@gmail.com>          *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/


#include "padtoolsplugin.h"
#include "padtoolscore.h"

#include <utils/log.h>

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/ipatient.h>
#include <coreplugin/iuser.h>
#include <coreplugin/imainwindow.h>
#include <coreplugin/translators.h>

#include <QtPlugin>
#include <QDebug>

using namespace PadTools;
using namespace Internal;

static inline Core::IUser *user() {return Core::ICore::instance()->user();}

PadToolsPlugin::PadToolsPlugin() :
    ExtensionSystem::IPlugin(),
    _core(0),
    _impl(0)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating PadToolsPlugin";
    Core::ICore::instance()->translators()->addNewTranslator("plugin_padtools");

    _core = new  PadToolsCore(this);
}

PadToolsPlugin::~PadToolsPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool PadToolsPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "PadToolsPlugin::initialize";
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);

    return true;
}

void PadToolsPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "PadToolsPlugin::extensionsInitialized";

#if defined(FREEMEDFORMS) || defined(FREEACCOUNT)
    if (!user())
        return;
#endif

    _core->initialize();
    Core::ICore::instance()->patient()->registerPatientTokens();
    Core::ICore::instance()->user()->registerUserTokens();

    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));
}

ExtensionSystem::IPlugin::ShutdownFlag PadToolsPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;

    Core::ICore::instance()->setPadTools(0);

    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(PadToolsPlugin)
