/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>,                *
 *                    Guillaume Denry <guillaume.denry@gmail.com>          *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/
#ifndef PADTOOLS_PAD_HIGHLIGHTER_H
#define PADTOOLS_PAD_HIGHLIGHTER_H

#include <QSyntaxHighlighter>

#include "pad_analyzer.h"

/**
 * \file ./plugins/padtoolsplugin/pad_highlighter.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace PadTools {
namespace Internal {

struct BlockData : public QTextBlockUserData
{
	enum TokenType {
		Token_OpenPad,
		Token_CoreDelimiter,
		Token_Core
	};
	QVector<TokenType> tokens;

	void eatClosePad();
	void eatCoreDelimiter();
};

class PadHighlighter : public QSyntaxHighlighter
{
	Q_OBJECT

public:
	PadHighlighter(QObject *parent) : QSyntaxHighlighter(parent) { init(); }
	PadHighlighter(QTextDocument *parent) : QSyntaxHighlighter(parent) { init(); }
//	PadHighlighter(QTextEdit *parent) : QSyntaxHighlighter(parent) { init(); }

protected:
    void highlightBlock(const QString &text);

private:
/*	enum BlockState {
		State_Normal = 0,
		State_Prefix,
		State_Postfix,
		State_Core
		};*/

	PadAnalyzer _padAnalyzer;
	QTextCharFormat _padDelimiterFormat;
	QTextCharFormat _coreDelimiterFormat;
	QTextCharFormat _prefixFormat;
	QTextCharFormat _postfixFormat;
	QTextCharFormat _coreTextFormat;

	void init();
};

} // namespace Internal
} // namespace PadTools

#endif  // PADTOOLS_PAD_HIGHLIGHTER
