/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>,                *
 *                    Guillaume Denry <guillaume.denry@gmail.com>          *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/
#ifndef PADTOOLS_TOKENEDITOR_H
#define PADTOOLS_TOKENEDITOR_H

#include <QDialog>
#include <QModelIndex>

/**
 * \file ./plugins/padtoolsplugin/tokeneditor.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace PadTools {
namespace Internal {
class TokenModel;
class PadItem;

namespace Ui {
class TokenEditor;
}

class TokenEditor : public QDialog
{
    Q_OBJECT

public:
    explicit TokenEditor(QWidget *parent = 0);
    ~TokenEditor();

    void setTokenModel(TokenModel *model);
    void setCurrentIndex(const QModelIndex &index);

    void setTokenUid(const QString &uid);

    void setConditionnalHtml(const QString &before, const QString &after);
    void setConditionnalPlainText(const QString &before, const QString &after);

    QString toRawSourceHtml() const;
    void getOutput(QString &html, PadItem &item, int startingOutputPos) const;

protected:
    void done(int result);

private:
    Ui::TokenEditor *ui;
};

} // namespace Internal
} // namespace PadTools

#endif // PADTOOLS_TOKENEDITOR_H
