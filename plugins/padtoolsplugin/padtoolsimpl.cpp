/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>,                *
 *                    Guillaume Denry <guillaume.denry@gmail.com>          *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/


#include "padtoolsimpl.h"
#include "tokenpool.h"
#include "pad_analyzer.h"
#include "pad_highlighter.h"
#include "padwriter.h"

#include <utils/log.h>

#include <QTime>
#include <QCryptographicHash>

#include <QDebug>

using namespace PadTools;
using namespace Internal;

PadToolsImpl::PadToolsImpl(QObject *parent) :
    Core::IPadTools(parent)
{
    _pool = new TokenPool(this);
}

PadToolsImpl::~PadToolsImpl()
{}

Core::ITokenPool *PadToolsImpl::tokenPool() const
{
    return _pool;
}


QString PadToolsImpl::processPlainText(const QString &plainText)
{

    PadAnalyzer analyzer;
    PadDocument *pad = analyzer.analyze(plainText);
    pad->setContentType(PadDocument::ContentIsPlainText);

    pad->toOutput(_pool);
    const QString &text = pad->outputDocument()->toPlainText();
    return text;
}

QString PadToolsImpl::processHtml(const QString &html)
{

    PadAnalyzer analyzer;
    QTextDocument *doc = new QTextDocument(this);
    doc->setPlainText(html);

    PadDocument *pad = analyzer.analyze(doc, 0);
    pad->setContentType(PadDocument::ContentIsPlainText);

    pad->toOutput(_pool);
    const QString &out = pad->outputDocument()->toPlainText();
    return out;
}

Core::IPadWriter *PadToolsImpl::createWriter(QWidget *parent)
{
    return new Internal::PadWriter(parent);
}

