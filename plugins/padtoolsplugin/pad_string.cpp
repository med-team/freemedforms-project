/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>,                *
 *                    Guillaume Denry <guillaume.denry@gmail.com>          *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/
#include "pad_string.h"
#include "constants.h"
#include "pad_document.h"

#include <QDebug>

using namespace PadTools;
using namespace Internal;

void PadString::debug(int indent) const
{
    QString i(indent, ' ');
    QString str;
    str += i+ QString("[padString:Source(%1;%2);Output(%3;%4)]\n")
            .arg(start()).arg(end())
            .arg(outputStart()).arg(outputEnd());
    str += i+ _string;
    qWarning() << str;
}



void PadString::run(QMap<QString,QVariant> &tokens, PadDocument *document)
{
    Q_UNUSED(tokens);
    setOutputStart(start() + document->positionTranslator().deltaForSourcePosition(start()));
    setOutputEnd(outputStart() + rawLength());
}

void PadString::toOutput(Core::ITokenPool *pool, PadDocument *document, TokenReplacementMethod)
{
    Q_UNUSED(pool);
    setOutputStart(start() + document->positionTranslator().deltaForSourcePosition(start()));
    setOutputEnd(outputStart() + rawLength());
}

void PadString::toRaw(PadDocument *document)
{
    Q_UNUSED(document);
    setStart(document->positionTranslator().outputToRaw(outputStart()));
    setEnd(document->positionTranslator().outputToRaw(outputEnd()));
}
