/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>                  *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/
#ifndef PADTOOLS_TOKENPOOL_H
#define PADTOOLS_TOKENPOOL_H

#include <coreplugin/ipadtools.h>

#include <QString>
#include <QVariant>

/**
 * \file ./plugins/padtoolsplugin/tokenpool.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace PadTools {
namespace Internal {
class TokenPoolPrivate;

class TokenPool : public Core::ITokenPool
{
    Q_OBJECT
public:
    TokenPool(QObject *parent = 0);
    ~TokenPool();

    void registerNamespace(const Core::TokenNamespace &ns);
    int rootNamespaceCount() const;
    const Core::TokenNamespace &rootNamespaceAt(int index) const;
    Core::TokenNamespace getTokenNamespace(const QString &name) const;

    void addToken(Core::IToken *token);
    void addTokens(const QVector<Core::IToken *> &tokens);
    Core::IToken *token(const QString &name);
    void removeToken(Core::IToken *token);

    QList<Core::IToken *> tokens() const;

    QVariant tokenTestingValue(const QString &name);
    QVariant tokenCurrentValue(const QString &name);

private:
    static TokenPool *_instance;
    Internal::TokenPoolPrivate *d;
};

} // namespace Internal
} // namespace PadTools

#endif // PADTOOLS_TOKENPOOL_H
