/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>,                *
 *                    Guillaume Denry <guillaume.denry@gmail.com>          *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/
#ifndef PADTOOLS_IMPL_H
#define PADTOOLS_IMPL_H

#include <coreplugin/ipadtools.h>

/**
 * \file ./plugins/padtoolsplugin/padtoolsimpl.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ITokenPool;
}

namespace PadTools {
namespace Internal {
class TokenPool;

class PadToolsImpl : public Core::IPadTools
{
    Q_OBJECT
public:
    PadToolsImpl(QObject *parent = 0);
    ~PadToolsImpl();

    Core::ITokenPool *tokenPool() const;

    QString processPlainText(const QString &plainText);
    QString processHtml(const QString &html);

    Core::IPadWriter *createWriter(QWidget *parent = 0);

private:
    TokenPool *_pool;
};

} // namespace Internal
} // namespace PadTools

#endif  // PADTOOLS_IMPL_H
