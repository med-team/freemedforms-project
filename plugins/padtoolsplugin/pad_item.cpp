/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>,                *
 *                    Guillaume Denry <guillaume.denry@gmail.com>          *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/


#include "pad_item.h"
#include "constants.h"
#include "pad_document.h"

#include <utils/log.h>
#include <utils/global.h>

#include <QString>
#include <QTextCursor>
#include <QTextDocumentFragment>

#include <QDebug>

using namespace PadTools;
using namespace Internal;

PadConditionnalSubItem::PadConditionnalSubItem(TokenCoreCondition cond, Place place, PadFragment *parent) :
    PadFragment(parent), _coreCond(cond), _place(place)
{}

void PadConditionnalSubItem::addDelimiter(const int posInRaw, const int size)
{
    PadDelimiter delim;
    delim.rawPos = posInRaw;
    delim.size = size;
    _delimiters << delim;
}

void PadConditionnalSubItem::debug(int indent) const
{
    QString str(indent, ' ');
    str += QString("[padSubItem:Source(%1;%2);Output(%3;%4)]")
            .arg(start()).arg(end())
            .arg(outputStart()).arg(outputEnd());
    qDebug("%s", qPrintable(str));
    foreach (PadFragment *fragment, _fragments) {
        fragment->debug(indent + 2);
    }
}

void PadConditionnalSubItem::run(QMap<QString,QVariant> &tokens, PadDocument *document)
{
    PadFragment *f = parent();
    PadItem *item = 0;
    while (f) {
        item = dynamic_cast<PadItem*>(f);
        if (item)
            break;
    }

    if (!item) {
        LOG_ERROR_FOR("PadConditionnalSubItem", "No PadItem parent");
        return;
    }

    const QString &value = tokens.value(item->getCore()->uid()).toString();

    bool removeMe = false;
    if (value.isEmpty()) {
        removeMe = (_coreCond == Defined);
    } else {
        removeMe = (_coreCond == Undefined);
    }

    setOutputStart(document->positionTranslator().rawToOutput(start()));
    if (removeMe) {
        QTextCursor cursor(document->outputDocument());
        cursor.setPosition(outputStart());
        cursor.setPosition(outputStart() + rawLength(), QTextCursor::KeepAnchor);
        cursor.removeSelectedText();
        setOutputEnd(outputStart());
        document->positionTranslator().addOutputTranslation(outputStart(), -rawLength());
        return;
    } else {
        foreach(const PadDelimiter &delim, _delimiters) {


            QTextCursor cursor(document->outputDocument());
            int pos = document->positionTranslator().rawToOutput(delim.rawPos);
            cursor.setPosition(pos);
            cursor.setPosition(pos + delim.size, QTextCursor::KeepAnchor);
            cursor.removeSelectedText();
            setOutputEnd(outputEnd() - delim.size);
            document->positionTranslator().addOutputTranslation(delim.rawPos, -delim.size);
        }

        foreach(PadFragment *frag, _fragments)
            frag->run(tokens, document);
    }
    setOutputEnd(document->positionTranslator().rawToOutput(end()));
}

void PadConditionnalSubItem::toOutput(Core::ITokenPool *pool, PadDocument *document, TokenReplacementMethod method)
{
    PadFragment *f = parent();
    PadItem *item = 0;
    while (f) {
        item = dynamic_cast<PadItem*>(f);
        if (item)
            break;
    }

    if (!item) {
        LOG_ERROR_FOR("PadConditionnalSubItem", "No PadItem parent");
        return;
    }
    if (!item->getCore()) {
        LOG_ERROR_FOR("PadConditionnalSubItem", "No PadCore parent");
        return;
    }

    QString value;
    switch (method) {
    case ReplaceWithTokenDisplayName: value = pool->token(item->getCore()->uid())->humanReadableName(); break;
    case ReplaceWithTokenTestingValue: value = pool->token(item->getCore()->uid())->testValue().toString(); break;
    case ReplaceWithTokenUuid: value = item->getCore()->uid();  break;
    default: // ReplaceWithTokenValue
        value = pool->token(item->getCore()->uid())->value().toString();
    }
    bool removeMe = false;
    if (value.isEmpty()) {
        removeMe = (_coreCond == Defined);
    } else {
        removeMe = (_coreCond == Undefined);
    }

    setOutputStart(document->positionTranslator().rawToOutput(start()));
    if (removeMe) {
        QTextCursor cursor(document->outputDocument());
        cursor.setPosition(outputStart());
        cursor.setPosition(outputStart() + rawLength(), QTextCursor::KeepAnchor);
        cursor.removeSelectedText();
        setOutputEnd(outputStart());
        document->positionTranslator().addOutputTranslation(outputStart(), -rawLength());
        return;
    } else {
        foreach(const PadDelimiter &delim, _delimiters) {


            QTextCursor cursor(document->outputDocument());
            int pos = document->positionTranslator().rawToOutput(delim.rawPos);
            cursor.setPosition(pos);
            cursor.setPosition(pos + delim.size, QTextCursor::KeepAnchor);
            cursor.removeSelectedText();
            setOutputEnd(outputEnd() - delim.size);
            document->positionTranslator().addOutputTranslation(delim.rawPos, -delim.size);
        }

        foreach(PadFragment *frag, _fragments)
            frag->toOutput(pool, document, method);
    }
    setOutputEnd(document->positionTranslator().rawToOutput(end()));
}

bool PadConditionnalSubItem::containsOutputPosition(const int pos) const
{
    return IN_RANGE_STRICTLY(pos, _outputStart, _outputEnd);
}

bool PadConditionnalSubItem::isBeforeOutputPosition(const int pos) const
{
    return pos >= _outputEnd;
}

bool PadConditionnalSubItem::isAfterOutputPosition(const int pos) const
{
    return pos <= _outputStart;
}


void PadConditionnalSubItem::toRaw(PadDocument *doc)
{
    Q_UNUSED(doc);
}

const QString &PadCore::uid() const
{
    return _uid;
}

void PadCore::setUid(const QString &uid)
{
    _uid = uid;
}

bool PadCore::containsOutputPosition(const int pos) const
{
    return IN_RANGE_STRICTLY(pos, _outputStart, _outputEnd);
}

bool PadCore::isBeforeOutputPosition(const int pos) const
{
    return pos >= _outputEnd;
}

bool PadCore::isAfterOutputPosition(const int pos) const
{
    return pos <= _outputStart;
}


void PadCore::debug(int indent) const
{
    QString str(indent, ' ');
    str += QString("[padCore:Source(%1;%2);Output(%3;%4)]: %5")
            .arg(start()).arg(end())
            .arg(outputStart()).arg(outputEnd())
            .arg(_uid);
    qDebug("%s", qPrintable(str));
}

void PadCore::run(QMap<QString,QVariant> &tokens, PadDocument *document)
{
    const QString &value = tokens[_uid].toString();
    if (value.isEmpty()) {
        LOG_ERROR_FOR("PadCore", "token run without value? Check PadItem.");
        return;
    }
    setOutputStart(document->positionTranslator().rawToOutput(start()));

    QTextCursor cursor(document->outputDocument());
    cursor.setPosition(outputStart());
    cursor.setPosition(outputStart() + rawLength(), QTextCursor::KeepAnchor);
    QTextCharFormat format = cursor.charFormat();
    cursor.removeSelectedText();
    if (Qt::mightBeRichText(value)) {
        cursor.insertHtml(value);
        setOutputEnd(cursor.selectionEnd());
        cursor.setPosition(outputStart());
        cursor.setPosition(outputEnd(), QTextCursor::KeepAnchor);
        cursor.mergeCharFormat(format);
    } else {
        cursor.insertText(value, format);
        setOutputEnd(outputStart() + value.size());
    }

    int delta = outputLength() - rawLength();
    document->positionTranslator().addOutputTranslation(outputStart(), delta);
}

void PadCore::toOutput(Core::ITokenPool *pool, PadDocument *document, TokenReplacementMethod method)
{
    Q_UNUSED(method);
    const QString &coreValue = tokenValue(pool, method);
    if (coreValue.isEmpty()) {
        LOG_ERROR_FOR("PadCore", "token run without value? Check PadItem.");
        return;
    }
    setOutputStart(document->positionTranslator().rawToOutput(start()));

    QTextCursor cursor(document->outputDocument());
    cursor.setPosition(outputStart());
    cursor.setPosition(outputStart() + rawLength(), QTextCursor::KeepAnchor);
    QTextCharFormat format = cursor.charFormat();
    cursor.removeSelectedText();

    if ((document->contentType() == PadDocument::ContentAutoType
            && Qt::mightBeRichText(coreValue))
            || document->contentType() == PadDocument::ContentIsHtml) {
        cursor.insertHtml(coreValue);
        setOutputEnd(cursor.selectionEnd());
        cursor.setPosition(outputStart());
        cursor.setPosition(outputEnd(), QTextCursor::KeepAnchor);
        cursor.mergeCharFormat(format);
    } else {
        cursor.insertText(coreValue, format);
        setOutputEnd(outputStart() + coreValue.size());
    }

    int delta = outputLength() - rawLength();
    document->positionTranslator().addOutputTranslation(outputStart(), delta);
}

void PadCore::toRaw(PadDocument *doc)
{
    QTextCursor raw(doc->rawSourceDocument());
    int oldLength = _end - _start;

    setStart(doc->positionTranslator().outputToRaw(_outputStart));
    setEnd(doc->positionTranslator().outputToRaw(_outputEnd));
    int s = QString(Constants::TOKEN_CORE_DELIMITER).size();

    raw.setPosition(_start);
    raw.setPosition(_end, QTextCursor::KeepAnchor);
    raw.removeSelectedText();
    raw.insertText(_uid);
    int newLength = _uid.size();
    doc->positionTranslator().addRawTranslation(_start, newLength-oldLength);
    doc->positionTranslator().addRawTranslation(_start, s);

    raw.setPosition(_start);
    raw.insertText(Constants::TOKEN_CORE_DELIMITER);

    setEnd(doc->positionTranslator().outputToRaw(_outputEnd));
    doc->positionTranslator().addRawTranslation(_start, s);
    raw.setPosition(_end);
    raw.insertText(Constants::TOKEN_CORE_DELIMITER);
    setEnd(_end + s);
}

QString PadCore::tokenValue(Core::ITokenPool *pool, TokenReplacementMethod method) const
{
    switch (method) {
    case ReplaceWithTokenDisplayName: return pool->token(uid())->humanReadableName();
    case ReplaceWithTokenTestingValue: return pool->token(uid())->testValue().toString();
    case ReplaceWithTokenUuid: return uid();
    default: // ReplaceWithTokenValue
        if (pool->token(uid()))
            return pool->token(uid())->value().toString();
        else
            qWarning() << "**** Missing token "<<uid();
    }
    return QString::null;
}


PadItem::~PadItem()
{
}

void PadItem::debug(int indent) const
{
	QString str(indent, ' ');
    str += QString("[padItem:Source(%1;%2);Output(%3;%4)]")
            .arg(start()).arg(end())
            .arg(outputStart()).arg(outputEnd());
	qDebug("%s", qPrintable(str));
    foreach (PadFragment *fragment, _fragments) {
        fragment->debug(indent + 2);
	}
}

QList<PadFragment*> PadItem::children() const
{
	QList<PadFragment*> fragments;
	PadItem *padItem;
	fragments.append(_fragments);
    foreach (PadFragment *fragment, _fragments) {
		padItem = dynamic_cast<PadItem*>(fragment);
		if (padItem)
            fragments.append(padItem->children());
	}
	return fragments;
}

void PadItem::addDelimiter(const int posInRaw, const int size)
{
    PadDelimiter delim;
    delim.rawPos = posInRaw;
    delim.size = size;
    _delimiters << delim;
}

bool PadItem::containsOutputPosition(const int pos) const
{
    return IN_RANGE_STRICTLY(pos, _outputStart, _outputEnd);
}

bool PadItem::isBeforeOutputPosition(const int pos) const
{
    return pos >= _outputEnd;
}

bool PadItem::isAfterOutputPosition(const int pos) const
{
    return pos <= _outputStart;
}

PadCore *PadItem::getCore() const
{
    PadCore *core;
    foreach (PadFragment *fragment, _fragments) {
        core = dynamic_cast<PadCore*>(fragment);
        if (core)
            return core;
    }
    return 0;
}

PadConditionnalSubItem *PadItem::subItem(const PadConditionnalSubItem::TokenCoreCondition cond, const PadConditionnalSubItem::Place place)
{
    PadConditionnalSubItem *sub;
    foreach (PadFragment *fragment, _fragments) {
        sub = dynamic_cast<PadConditionnalSubItem*>(fragment);
        if (sub) {
            if (sub->tokenCoreCondition() == cond && sub->place() == place)
                return sub;
        }
    }
    return 0;
}

void PadItem::run(QMap<QString,QVariant> &tokens, PadDocument *document)
{
    PadCore *core = getCore();
    QString coreValue;

    if (core) {
        coreValue = tokens.value(core->uid()).toString();

        if (coreValue.isEmpty()) {
            QTextCursor cursor(document->outputDocument());
            setOutputStart(document->positionTranslator().rawToOutput(start()));
            cursor.setPosition(outputStart());
            cursor.setPosition(outputStart() + rawLength(), QTextCursor::KeepAnchor);
            cursor.removeSelectedText();
            setOutputEnd(outputStart());
            document->positionTranslator().addOutputTranslation(outputStart(), -rawLength());
        } else {
            foreach(const PadDelimiter &delim, _delimiters) {
                if (delim.rawPos >= core->start())
                    continue;


                QTextCursor cursor(document->outputDocument());
                int pos = document->positionTranslator().rawToOutput(delim.rawPos);
                cursor.setPosition(pos);
                cursor.setPosition(pos + delim.size, QTextCursor::KeepAnchor);
                cursor.removeSelectedText();
                document->positionTranslator().addOutputTranslation(pos, -delim.size);
            }

            foreach(PadFragment *f, _fragments) {
                f->run(tokens, document);
            }

            foreach(const PadDelimiter &delim, _delimiters) {
                if (delim.rawPos < core->end())
                    continue;


                QTextCursor cursor(document->outputDocument());
                int pos = document->positionTranslator().rawToOutput(delim.rawPos);
                cursor.setPosition(pos);
                cursor.setPosition(pos + delim.size, QTextCursor::KeepAnchor);
                cursor.removeSelectedText();
                document->positionTranslator().addOutputTranslation(pos, -delim.size);
            }

            setOutputStart(document->positionTranslator().rawToOutput(start()));
            setOutputEnd(document->positionTranslator().rawToOutput(end()));
        }
    }
}

void PadItem::toOutput(Core::ITokenPool *pool, PadDocument *document, TokenReplacementMethod method)
{
    PadCore *core = getCore();
    if (!core) {
        LOG_ERROR_FOR("PadItem", "No Core.");
        return;
    }
    const QString &coreValue = core->tokenValue(pool, method);
    if (coreValue.isEmpty()) {
        QTextCursor cursor(document->outputDocument());
        setOutputStart(document->positionTranslator().rawToOutput(start()));
        cursor.setPosition(outputStart());
        cursor.setPosition(outputStart() + rawLength(), QTextCursor::KeepAnchor);
        cursor.removeSelectedText();
        setOutputEnd(outputStart());
        document->positionTranslator().addOutputTranslation(outputStart(), -rawLength());
    } else {
        foreach(const PadDelimiter &delim, _delimiters) {
            if (delim.rawPos >= core->start())
                continue;


            QTextCursor cursor(document->outputDocument());
            int pos = document->positionTranslator().rawToOutput(delim.rawPos);
            cursor.setPosition(pos);
            cursor.setPosition(pos + delim.size, QTextCursor::KeepAnchor);
            cursor.removeSelectedText();
            document->positionTranslator().addOutputTranslation(pos, -delim.size);
        }

        foreach(PadFragment *f, _fragments) {
            f->toOutput(pool, document, method);
        }

        foreach(const PadDelimiter &delim, _delimiters) {
            if (delim.rawPos < core->end())
                continue;


            QTextCursor cursor(document->outputDocument());
            int pos = document->positionTranslator().rawToOutput(delim.rawPos);
            cursor.setPosition(pos);
            cursor.setPosition(pos + delim.size, QTextCursor::KeepAnchor);
            cursor.removeSelectedText();
            document->positionTranslator().addOutputTranslation(pos, -delim.size);
        }

        setOutputStart(document->positionTranslator().rawToOutput(start()));
        setOutputEnd(document->positionTranslator().rawToOutput(end()));
    }
}

void PadItem::toRaw(PadDocument *doc)
{
    QTextCursor raw(doc->rawSourceDocument());
    setStart(doc->positionTranslator().outputToRaw(_outputStart));
    raw.setPosition(_start);
    raw.insertText(Constants::TOKEN_OPEN_DELIMITER);
    int s = QString(Constants::TOKEN_OPEN_DELIMITER).size();
    doc->positionTranslator().addRawTranslation(_start, s);


    PadCore *core = getCore();
    Q_ASSERT(core);
    if (!core)
        return;
    core->toRaw(doc);


    setEnd(doc->positionTranslator().outputToRaw(_outputEnd));
    raw.setPosition(_end);
    raw.insertText(Constants::TOKEN_CLOSE_DELIMITER);
    doc->positionTranslator().addRawTranslation(_end, s);
    setEnd(_end + s);

    foreach(PadFragment *f, _fragments) {
        if (f!=core)
            f->toRaw(doc);
    }
}
