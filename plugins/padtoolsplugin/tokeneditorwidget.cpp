/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>,                *
 *                    Guillaume Denry <guillaume.denry@gmail.com>          *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/


#include "tokeneditorwidget.h"
#include "constants.h"
#include "pad_item.h"

#include <translationutils/constants.h>
#include <translationutils/trans_current.h>

#include "ui_tokeneditorwidget.h"

#include <QDebug>

using namespace PadTools;
using namespace Internal;
using namespace Trans::ConstantTranslations;

TokenEditorWidget::TokenEditorWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TokenEditorWidget),
    _model(0)
{
    ui->setupUi(this);
    ui->tokenValueFormatting->setTypes(Editor::TextEditor::CharFormat);
    ui->tokenValueFormatting->toogleToolbar(false);
    ui->tokenValueFormatting->setReadOnly(true);

    ui->before->setTypes(Editor::TextEditor::Simple);
    ui->before->toogleToolbar(true);

    ui->after->setTypes(Editor::TextEditor::Simple);
    ui->after->toogleToolbar(true);

    layout()->setMargin(0);
    clear();

}

TokenEditorWidget::~TokenEditorWidget()
{
    delete ui;
}

void TokenEditorWidget::clear()
{
    ui->tokenGroup->setTitle(tkTr(Trans::Constants::TOKEN));
    ui->currentTokenValue->clear();
    ui->testTokenValue->clear();
    ui->tokenValueFormatting->clear();
    ui->before->clear();
    ui->after->clear();
}

void TokenEditorWidget::setTokenModel(TokenModel *model)
{
    _model = model;
}

void TokenEditorWidget::setCurrentIndex(const QModelIndex &index)
{
    clear();
    if (!index.isValid())
        return;

    _tokenUid = index.data().toString();
    ui->tokenGroup->setTitle(tkTr(Trans::Constants::TOKEN));
    ui->currentTokenValue->setText(tkTr(Trans::Constants::UNDEFINED));
    ui->testTokenValue->setText(tkTr(Trans::Constants::UNDEFINED));
    ui->tokenValueFormatting->setPlainText(_tokenUid);
}

void TokenEditorWidget::setTokenUid(const QString &uid)
{
    _tokenUid=uid;
    ui->tokenValueFormatting->clear();
    ui->tokenValueFormatting->setPlainText(_tokenUid);
}

void TokenEditorWidget::setConditionnalBeforeHtml(const QString &html)
{
    ui->before->setHtml(html);
}

void TokenEditorWidget::setConditionnalAfterHtml(const QString &html)
{
    ui->after->setHtml(html);
}

void TokenEditorWidget::setConditionnalBeforePlainText(const QString &txt)
{
    ui->before->setPlainText(txt);
}

void TokenEditorWidget::setConditionnalAfterPlainText(const QString &txt)
{
    ui->after->setPlainText(txt);
}

QString TokenEditorWidget::toRawSourceHtml() const
{
    QTextDocument doc;
    QTextCursor cursor(&doc);

    cursor.insertText(Constants::TOKEN_OPEN_DELIMITER);
    cursor.movePosition(QTextCursor::End);

    cursor.insertHtml(ui->before->toHtml());
    cursor.movePosition(QTextCursor::End);

    cursor.insertText(Constants::TOKEN_CORE_DELIMITER);
    cursor.movePosition(QTextCursor::End);

    cursor.insertText(_tokenUid);
    cursor.movePosition(QTextCursor::End);

    cursor.insertText(Constants::TOKEN_CORE_DELIMITER);
    cursor.movePosition(QTextCursor::End);

    cursor.insertHtml(ui->after->toHtml());
    cursor.movePosition(QTextCursor::End);

    cursor.insertText(Constants::TOKEN_CLOSE_DELIMITER);
    cursor.movePosition(QTextCursor::End);

    return doc.toHtml();
}

void TokenEditorWidget::getOutput(QString &html, PadItem &item, int startingOutputPos) const
{
    QTextDocument doc;
    QTextCursor cursor(&doc);
    html.clear();
    item.clear();
    item.setOutputStart(startingOutputPos);
    int previousPosition = 0;

    PadConditionnalSubItem *before = new PadConditionnalSubItem(PadConditionnalSubItem::Defined, PadConditionnalSubItem::Prepend, &item);
    before->setOutputStart(startingOutputPos);
    cursor.insertHtml(ui->before->toHtml());
    cursor.movePosition(QTextCursor::End);
    startingOutputPos += cursor.position() - previousPosition;
    before->setOutputEnd(startingOutputPos);

    previousPosition = cursor.position();
    PadCore *core = new PadCore;
    core->setUid(_tokenUid);
    core->setOutputStart(startingOutputPos);
    cursor.insertText(_tokenUid);
    cursor.movePosition(QTextCursor::End);
    startingOutputPos += cursor.position() - previousPosition;
    core->setOutputEnd(startingOutputPos);

    previousPosition = cursor.position();
    PadConditionnalSubItem *after = new PadConditionnalSubItem(PadConditionnalSubItem::Defined, PadConditionnalSubItem::Append, &item);
    after->setOutputStart(startingOutputPos);
    cursor.insertHtml(ui->after->toHtml());
    cursor.movePosition(QTextCursor::End);
    startingOutputPos += cursor.position() - previousPosition;
    after->setOutputEnd(startingOutputPos);

    item.addChild(before);
    item.addChild(core);
    item.addChild(after);
    item.setOutputEnd(startingOutputPos);
    html = doc.toHtml();
}

