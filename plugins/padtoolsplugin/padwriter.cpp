/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *      Eric Maeker                                                        *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/



#include "padwriter.h"
#include "constants.h"
#include "padtoolscore.h"
#include "pad_analyzer.h"

#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/constants_menus.h>
#include <coreplugin/constants_icons.h>
#include <coreplugin/ipadtools.h>
#include <coreplugin/itheme.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/contextmanager/contextmanager.h>

#include <utils/log.h>
#include <utils/global.h>
#include <utils/widgets/htmldelegate.h>

#include <QTimer>
#include <QToolBar>
#include <QToolButton>
#include <QSortFilterProxyModel>

#include "ui_padwriter.h"

#include <QDebug>

using namespace PadTools;
using namespace Internal;

static inline Core::ActionManager *actionManager() { return Core::ICore::instance()->actionManager(); }
static inline Core::ContextManager *contextManager() { return Core::ICore::instance()->contextManager(); }
static inline Core::ITheme *theme() { return Core::ICore::instance()->theme(); }
static inline Core::ISettings *settings() { return Core::ICore::instance()->settings(); }
static inline PadTools::Internal::PadToolsCore &padCore() {return PadTools::Internal::PadToolsCore::instance();}

namespace PadTools {
namespace Internal {
class TreeProxyModel : public QSortFilterProxyModel
{
public:
    TreeProxyModel(QObject *parent = 0)
        : QSortFilterProxyModel(parent)
    {
        setFilterCaseSensitivity(Qt::CaseInsensitive);
    }

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
    {
        if (filterRegExp().isEmpty())
            return true;

        QModelIndex currentParent(sourceModel()->index(sourceRow, 0, sourceParent));
        QModelIndex currentToFilter(sourceModel()->index(sourceRow, filterKeyColumn(), sourceParent));

        if (sourceModel()->hasChildren(currentParent)) {
            bool atLeastOneValidChild = false;
            int i = 0;
            while(!atLeastOneValidChild) {
                const QModelIndex child(currentParent.child(i, currentParent.column()));
                if (!child.isValid())
                    break;

                atLeastOneValidChild = filterAcceptsRow(i, currentParent);
                i++;
            }
            return atLeastOneValidChild;
        }

        return sourceModel()->data(currentToFilter).toString().contains(filterRegExp());
    }
};

class PadWriterPrivate
{
public:
    PadWriterPrivate(PadWriter *parent) :
        _context(0),
        ui(0),
        _filteredTokenModel(0),
        _padForEditor(0),
        _padForViewer(0),
        _toolBar(0),
        q(parent)
    {
    }

    void createUi()
    {
        ui = new Internal::Ui::PadWriter;
        ui->setupUi(q);
        ui->tokenTreeLayout->setMargin(0);
        ui->tokenTreeLayout->setSpacing(0);
        ui->listWidgetErrors->hide();
        ui->rawSource->hide();
        ui->outputTextEditor->setReadOnly(true);
    }

    void registerContext()
    {
        _context = new PadWriterContext(q);
        ui->dropTextEditor->addContext(_context->context());
        ui->rawSource->addContext(_context->context());
        contextManager()->addContextObject(_context);
    }

    void removeContext()
    {
        contextManager()->removeContextObject(_context);
    }

    void createActions()
    {
        QAction *a = aTest1 = new QAction(q);
        a->setText("Tokens and strings");
        a->setIcon(theme()->icon(Core::Constants::ICONHELP));

        a = aTest2 = new QAction(q);
        a->setText("Simple nested tokens & strings");
        a->setIcon(theme()->icon(Core::Constants::ICONHELP));

        a = aTest3 = new QAction(q);
        a->setText("Multinested tokens & strings");
        a->setIcon(theme()->icon(Core::Constants::ICONHELP));

        a = aTest4 = new QAction(q);
        a->setText("Tokens in table");
        a->setIcon(theme()->icon(Core::Constants::ICONHELP));

        a = aTest5 = new QAction(q);
        a->setText("Multinested tokens in table");
        a->setIcon(theme()->icon(Core::Constants::ICONHELP));

        a = aTest6 = new QAction(q);
        a->setText("Read prescription file");
        a->setIcon(theme()->icon(Core::Constants::ICONHELP));
    }

    void connectActionsAndUi()
    {
        QObject::connect(ui->dropTextEditor, SIGNAL(highlighting(PadItem*)), ui->outputTextEditor, SLOT(hightlight(PadItem*)));
        QObject::connect(ui->outputTextEditor, SIGNAL(highlighting(PadItem*)), ui->dropTextEditor, SLOT(hightlight(PadItem*)));
    }

    void createToolBar()
    {
        _toolBar = new QToolBar(q);
        _toolBar->setFocusPolicy(Qt::ClickFocus);
        if (!Utils::isReleaseCompilation()) {
            QToolButton *scenariTester = new QToolButton(q);
            scenariTester->setIcon(theme()->icon(Core::Constants::ICONHELP));
            scenariTester->setToolButtonStyle(Qt::ToolButtonIconOnly);
            scenariTester->setPopupMode(QToolButton::InstantPopup);
            scenariTester->addAction(aTest1);
            scenariTester->addAction(aTest2);
            scenariTester->addAction(aTest3);
            scenariTester->addAction(aTest4);
            scenariTester->addAction(aTest5);
            scenariTester->addAction(aTest6);
            scenariTester->setDefaultAction(aTest1);
            _toolBar->addWidget(scenariTester);
        }
        Core::Command *cmd;
        cmd = actionManager()->command(Constants::A_PADTOOLS_VIEWOUTPUT);
        _toolBar->addAction(cmd->action());

        cmd = actionManager()->command(Constants::A_PADTOOLS_SHOWSOURCE);
        _toolBar->addAction(cmd->action());

        ui->toolbarLayout->addWidget(_toolBar);
        aTest1->trigger();
    }

    void manageModelAndView()
    {
        _filteredTokenModel = new TreeProxyModel(q);
        _filteredTokenModel->setSourceModel(padCore().tokenModel());
        _filteredTokenModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
        _filteredTokenModel->setDynamicSortFilter(true);
        _filteredTokenModel->setFilterKeyColumn(Constants::TokenModel_UuidColumn);

        ui->treeView->setModel(_filteredTokenModel);
        ui->treeView->setItemDelegate(new Utils::HtmlDelegate(q));
        for(int i=0; i < _filteredTokenModel->columnCount(); ++i)
            ui->treeView->setColumnHidden(i, true);
        ui->treeView->setColumnHidden(Constants::TokenModel_HtmlLabelColumn, false);
        ui->treeView->setUniformRowHeights(false);
    #if QT_VERSION < 0x050000
        ui->treeView->header()->setResizeMode(Constants::TokenModel_HtmlLabelColumn, QHeaderView::Stretch);
    #else
        ui->treeView->header()->setSectionResizeMode(Constants::TokenModel_HtmlLabelColumn, QHeaderView::Stretch);
    #endif
        QObject::connect(_filteredTokenModel, SIGNAL(modelReset()), q, SLOT(expandTokenTreeView()));
    }

    void createPadDocument()
    {
        _padForEditor = new PadDocument();
        ui->dropTextEditor->setPadDocument(_padForEditor);
        _padForEditor->setSource(ui->rawSource->document());
        _padForEditor->setOutput(ui->dropTextEditor->document());

        _padForViewer = new PadDocument();
        ui->outputTextEditor->setPadDocument(_padForViewer);
        _padForViewer->setSource(ui->rawSource->document());
        _padForViewer->setOutput(ui->outputTextEditor->document());
    }

    void switchToRawSourceEdition()
    {
        ui->dropTextEditor->setVisible(false);
        ui->rawSource->setVisible(true);
    }

    void switchToDropTextEdition()
    {
        ui->dropTextEditor->setVisible(true);
        ui->rawSource->setVisible(false);
    }

public:
    PadWriterContext *_context;
    Ui::PadWriter *ui;
    TreeProxyModel *_filteredTokenModel;
    QAction *aTest1, *aTest2, *aTest3, *aTest4, *aTest5, *aTest6; // actions used to test different rawsource scenari
    PadDocument *_padForEditor, *_padForViewer;
    QToolBar *_toolBar;

private:
    PadWriter *q;
};
}  // Internal
}  // PadTools

PadWriterContext::PadWriterContext(PadWriter *w) :
    Core::IContext(w)
{
    setObjectName("PadWriterContext");
    setWidget(w);
    setContext(Core::Context(PadTools::Constants::C_PADTOOLS_PLUGINS));
}

PadWriter::PadWriter(QWidget *parent) :
    Core::IPadWriter(parent),
    d(new Internal::PadWriterPrivate(this))
{
    d->createUi();
    d->createActions();
    d->connectActionsAndUi();
    d->createToolBar();
    d->registerContext();

    d->manageModelAndView();
    d->createPadDocument();

    d->switchToDropTextEdition();
    setNamespaceFilter("");
    expandTokenTreeView();


}

PadWriter::~PadWriter()
{
    d->removeContext();
    if (d) {
        delete d;
        d = 0;
    }
}

void PadWriter::setPlainTextSource(const QString &plainText)
{
    d->ui->rawSource->setPlainText(plainText);
    analyzeRawSource();
}

void PadWriter::setHtmlSource(const QString &html)
{
    d->ui->rawSource->setHtml(html);
    analyzeRawSource();
}

void PadWriter::setNamespaceFilter(const QString &tokenNamespace)
{
    setNamespacesFilter(QStringList() << tokenNamespace);
}

void PadWriter::setNamespacesFilter(const QStringList &tokenNamespaces)
{
    QStringList corrected(tokenNamespaces);
    corrected.removeAll("");

    if (corrected.isEmpty()) {
        d->_filteredTokenModel->invalidate();
        return;
    }
    QString regexp = corrected.join("*|") + "*";
    regexp = regexp.remove("**").remove("||");
    QRegExp reg(regexp, Qt::CaseInsensitive);
    d->_filteredTokenModel->setFilterRegExp(reg);
}


QString PadWriter::outputToPlainText() const
{
    return d->ui->dropTextEditor->toPlainText();
}


QString PadWriter::outputToHtml() const
{
    return d->ui->dropTextEditor->toHtml();
}

QString PadWriter::rawSourceToPlainText() const
{
    return d->ui->rawSource->toPlainText();
}

QString PadWriter::rawSourceToHtml() const
{
    return d->ui->rawSource->toHtml();
}


void PadWriter::changeRawSourceScenario(QAction *a)
{
    QString source;
    if (a == d->aTest1) {
        source = "<p>"
                "<b><center>Simple token test</center></b></p><p>"
                "&nbsp;&nbsp;* To{{~test.A~) no before text}}ken D: {{\"...~test.D~...\"}}<br />"
                "&nbsp;&nbsp;* Token D without 'after conditional text':{{ ~test.D~}}<br />"
                "&nbsp;&nbsp;* Token D without 'before conditional text': {{~test.D~. }}<br />"
                "&nbsp;&nbsp;* Long token A: {{this text should appear in the output document, <u>including the core value</u> \"<b>~test.A~</b>\" (in bold) as defined in the <span style=' text-decoration: underline; color:#ff00ff;'>TokenModel</span>.}}<br />"
                "&nbsp;&nbsp;* HTML Token:<br />"
                "&nbsp;&nbsp;&nbsp;&nbsp;* Result should be \" <u><b>htmlToken</b></u> \"<br />"
                "&nbsp;&nbsp;&nbsp;&nbsp;* Result should be {{\" <u>~test.HTMLTOKEN~</u> \"}}<br />"
                "</p>"
                ;
    } else if (a == d->aTest2) {
        source = "<p>"
                "<b><center>Nested tokens test</center></b></p><p>"
                "&nbsp;&nbsp;* Testing tokens:<br />"
                "&nbsp;&nbsp;&nbsp;&nbsp;* {{\"Token B: (~test.B~) {{[[Token {{this text ~NULL~ should not appear in output}}C: ~test.C~]]}}.\"}}<br />"
                "&nbsp;&nbsp;* Result should be:<br />"
                "&nbsp;&nbsp;&nbsp;&nbsp;* \"Token B: (This is B) [[Token C: This is C]].\"<br />"
                ;
    } else if (a == d->aTest3) {
        source = "<p><b>{{(<span style='text-decoration: underline; color:#ff00ff;'>A:</span> ~test.A~)}}. Some strings.</b><br />"
                "{{(<span style='text-decoration: underline; color:#0000ff;'>D:</span> {{[C: ~test.C~]}} ~test.D~)}}<br/>"
                "{{(B: ~test.B~)}}<br />";
    } else if (a == d->aTest4) {
    source = "<p><b>Testing tokens inside a table</b><br />"
            "<table border=1>"
            "<tr>"
            "  <td>{{_<span style=' text-decoration: underline; color:#ff00ff;'>A_</span> ~test.A~ _A_}} 10 chars </td>"
            "</tr>"
            "<tr>"
            "  <td> 10 chars {{ _D_ ~test.D~ _D_}}</td>"
            "</tr>"
            "</table>"
            "</p>";
    } else if (a == d->aTest5) {
        source = "<p><b>Testing nested tokens inside a table</b><br />"
                "<table border=1>"
                "<tr>"
                "  <td>{{<span style=' text-decoration: underline; color:#ff00ff;'>_A_</span> ~test.A~ _A_}} 10 chars </td>"
                "</tr>"
                "<tr>"
                "  <td> 10 chars {{ _D_ ~test.D~ _D_}}</td>"
                "</tr>"
                "<tr>"
                "  <td>Two nested: {{ _D_ ~test.D~ _{{Nested C ~test.C~... }}D_}}</td>"
                "</tr>"
                "<tr>"
                "  <td>Multi-nested: {{ _D_ ~test.D~ _{{Nested C ~test.C~..{{//~test.A~//}}.. }}D_}}</td>"
                "</tr>"
                "</table>"
                "</p>";
    } else if (a == d->aTest6) {
        source = Utils::readTextFile(settings()->path(Core::ISettings::BundleResourcesPath) + "/textfiles/prescription/padtoolsstyle_fr.txt");
    }

    d->ui->rawSource->setHtml(source);
    analyzeRawSource();
}


void PadWriter::expandTokenTreeView()
{
    for(int i=0; i < d->_filteredTokenModel->rowCount(); ++i)
        d->ui->treeView->expand(d->_filteredTokenModel->index(i,0));
}


void PadWriter::analyzeRawSource()
{
    d->_padForEditor->clear();
    d->_padForViewer->clear();

    PadAnalyzer().analyze(d->ui->rawSource->document(), d->_padForEditor);
    d->_padForEditor->toOutput(padCore().tokenPool(), PadFragment::ReplaceWithTokenDisplayName);

    PadAnalyzer().analyze(d->ui->rawSource->document(), d->_padForViewer);
    d->_padForViewer->toOutput(padCore().tokenPool(), PadFragment::ReplaceWithTokenValue);
}


void PadWriter::outputToRaw()
{
    d->_padForEditor->toRaw();
    d->switchToRawSourceEdition();
}


void PadWriter::onViewOutputRequested()
{
    analyzeRawSource();
}


void PadWriter::onShowSourceRequested()
{
    outputToRaw();
}
