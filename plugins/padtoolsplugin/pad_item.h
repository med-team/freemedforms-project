/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>,                *
 *                    Guillaume Denry <guillaume.denry@gmail.com>          *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/
#ifndef PADTOOLS_PAD_ITEM_H
#define PADTOOLS_PAD_ITEM_H

#include <padtoolsplugin/pad_fragment.h>

#include <QList>
#include <QMap>
#include <QVariant>

/**
 * \file ./plugins/padtoolsplugin/pad_item.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/


namespace PadTools {
namespace Internal {
class PadDocument;

struct PadDelimiter {
    int rawPos, size;
};


class PadConditionnalSubItem : public PadFragment
{
public:
    enum TokenCoreCondition {
        Defined = 0,
        Undefined
    };
    enum Place {
        Prepend = 0,
        Append
    };

    PadConditionnalSubItem(TokenCoreCondition cond, Place place, PadFragment *parent = 0);
    virtual ~PadConditionnalSubItem() {}

    TokenCoreCondition tokenCoreCondition() const {return _coreCond;}
    Place place() const {return _place;}

    void addDelimiter(const int posInRaw, const int size);

    bool containsOutputPosition(const int pos) const;
    bool isBeforeOutputPosition(const int pos) const;
    bool isAfterOutputPosition(const int pos) const;

    void run(QMap<QString,QVariant> &tokens, PadDocument *document);
    void toOutput(Core::ITokenPool *pool, PadDocument *document, TokenReplacementMethod method);
    void toRaw(PadDocument *doc);

    void debug(int indent = 0) const;

private:
    TokenCoreCondition _coreCond;
    Place _place;
    QList<PadDelimiter> _delimiters;
};

class PadCore : public PadFragment
{
public:
    PadCore() : PadFragment() {}

    const QString &uid() const;
    void setUid(const QString &uid);

    bool containsOutputPosition(const int pos) const;
    bool isBeforeOutputPosition(const int pos) const;
    bool isAfterOutputPosition(const int pos) const;

    void debug(int indent = 0) const;

    void run(QMap<QString,QVariant> &tokens, PadDocument *document);
    void toOutput(Core::ITokenPool *pool, PadDocument *document, TokenReplacementMethod method);
    void toRaw(PadDocument *doc);

    QString tokenValue(Core::ITokenPool *pool, TokenReplacementMethod method) const;

private:
    QString _uid;
};

class PadItem : public PadFragment
{
public:
    enum PadStringType {
        NoType = 0,
        Core,
        DefinedCore_PrependText,
        DefinedCore_AppendText,
        UndefinedCore_PrependText,
        UndefinedCore_AppendText
    };

    PadItem() : PadFragment() {}
	virtual ~PadItem();

    void addDelimiter(const int posInRaw, const int size);

    bool containsOutputPosition(const int pos) const;
    bool isBeforeOutputPosition(const int pos) const;
    bool isAfterOutputPosition(const int pos) const;

    void debug(int indent = 0) const;

    void run(QMap<QString,QVariant> &tokens, PadDocument *document);
    void toOutput(Core::ITokenPool *pool, PadDocument *document, TokenReplacementMethod method);
    void toRaw(PadDocument *doc);

    QList<PadFragment*> children() const;
	PadCore *getCore() const;
    PadConditionnalSubItem *subItem(const PadConditionnalSubItem::TokenCoreCondition cond, const PadConditionnalSubItem::Place place);

private:
    QList<PadDelimiter> _delimiters;
};

} // namespace Internal
} // namespace PadTools

#endif  // PADTOOLS_PAD_ITEM
