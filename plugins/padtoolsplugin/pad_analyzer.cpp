/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>,                 *
 *                    Guillaume Denry <guillaume.denry@gmail.com>          *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/


#include "pad_analyzer.h"
#include "constants.h"

#include <utils/log.h>

#include <QTextCursor>
#include <QTime>

#include <QDebug>

enum { WarnLexemMatching = false, WarnIsDelimiterDebug = false };

using namespace PadTools;
using namespace Internal;

namespace PadTools {
namespace Internal {
class PadAnalyzerPrivate
{
public:
    enum LexemType {
        Lexem_Null = 0,
        Lexem_String,
        Lexem_PadOpenDelimiter,
        Lexem_PadCloseDelimiter,
        Lexem_CoreDelimiter
    };

    struct Lexem {
        LexemType type;		// type of the lexem
        QString value;		// value (can be empty) of the lexem
        QString rawValue;	// raw value of the lexem (never empty)
        int start;			// start index in the analyzed text
        int end;			// end index in the analyzed text
    };

    PadAnalyzerPrivate(PadAnalyzer *parent):
        _sourceDocument(0),
        _curPos(-1), // _curPos == -1 means no current analyze
        q(parent)
    {
        _lexemNull.type = Lexem_Null;
    }

    QChar getCharAt(int pos, QTextDocument *doc)
    {
        QTextCursor cursor(doc);
        cursor.setPosition(pos);
        if (cursor.atEnd())
            return QChar::Null;
        cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor);
        if (cursor.selectedText().size() > 0)
            return cursor.selectedText().at(0);
        return QChar::Null;
    }

    QString getStringAt(int pos, QTextDocument *doc, int size = 1)
    {
        QTextCursor cursor(doc);
        cursor.setPosition(pos);
        if (cursor.atEnd())
            return QString::null;
        cursor.setPosition(pos + size, QTextCursor::KeepAnchor);
        return cursor.selectedText();
    }

    PadDocument *startAnalyze(PadDocument *padDocument = 0);

    bool atEnd();
    PadItem *nextPadItem();
    PadCore *nextCore();

    bool isDelimiter(int pos, int *delimiterSize, LexemType *type);

    Lexem nextLexem();

    int nextId() {return ++_id;}
    void resetId() {_id=0;}


public:
    Lexem _lexemNull;
    QTextDocument *_sourceDocument;
    int _curPos; // contains the current position in the analyzed text
    uint _id;
    QList<Core::PadAnalyzerError> _lastErrors;

private:
    PadAnalyzer *q;
};

}
}

PadAnalyzer::PadAnalyzer(QObject *parent) :
    QObject(parent),
    d(new Internal::PadAnalyzerPrivate(this))
{
}

PadAnalyzer::~PadAnalyzer()
{
    if (d) {
        delete d;
        d = 0;
    }
}


PadDocument *PadAnalyzer::analyze(const QString &source)
{
    if (d->_sourceDocument && d->_sourceDocument->parent()==this) {
        delete d->_sourceDocument;
        d->_sourceDocument = 0;
    }
    d->_sourceDocument = new QTextDocument(this);
    d->_sourceDocument->setPlainText(source);
    return d->startAnalyze();
}


PadDocument *PadAnalyzer::analyze(QTextDocument *source, PadDocument *padDocument)
{
    if (d->_sourceDocument && d->_sourceDocument->parent()==this) {
        delete d->_sourceDocument;
        d->_sourceDocument = 0;
    }
    d->_sourceDocument = source;
    return d->startAnalyze(padDocument);
}

const QList<Core::PadAnalyzerError> PadAnalyzer::lastErrors() const
{
    return d->_lastErrors;
}


PadDocument *PadAnalyzerPrivate::startAnalyze(PadDocument *padDocument)
{
    Lexem lex;
    PadDocument *pad;
    padDocument ? pad = padDocument : pad = new PadDocument(q);
    pad->setSource(_sourceDocument);
    pad->beginRawSourceAnalyze();

	PadFragment *fragment;
    int pos = -1;
    QMap<QString, QVariant> errorTokens;

	_curPos = 0;
	_lastErrors.clear();
    resetId();

	while ((lex = nextLexem()).type != Lexem_Null) {
		fragment = 0;
		switch (lex.type) {
        case Lexem_String: // ignore this kind of fragment
			break;
		case Lexem_PadOpenDelimiter:
			fragment = nextPadItem();
			if (!fragment) { // bad pad item => turn it into a string fragment
                fragment = new PadString();//text.mid(pos, _curPos - pos));
				fragment->setStart(pos);
				fragment->setEnd(_curPos - 1);
                fragment->setId(nextId());
            }
			break;
		case Lexem_PadCloseDelimiter:
            errorTokens.insert("char", QString(Constants::TOKEN_CLOSE_DELIMITER));// padCloseDelimiter));
			_lastErrors << Core::PadAnalyzerError(Core::PadAnalyzerError::Error_UnexpectedChar,
                                                  _curPos - 1,
                                                  errorTokens);

            pos = _curPos - QString(Constants::TOKEN_CLOSE_DELIMITER).size();
            fragment = new PadString();//text.mid(pos, _curPos - pos));
			fragment->setStart(pos);
			fragment->setEnd(_curPos - 1);
            fragment->setId(nextId());
            break;
		case Lexem_CoreDelimiter:
            errorTokens.insert("char", QString(Constants::TOKEN_CORE_DELIMITER));
			_lastErrors << Core::PadAnalyzerError(Core::PadAnalyzerError::Error_UnexpectedChar,
                                                  _curPos - 1,
												  errorTokens);
			pos = _curPos - 1;
            fragment = new PadString();//text.mid(pos, _curPos - pos));
            fragment->setStart(pos);
			fragment->setEnd(_curPos - 1);
            fragment->setId(nextId());
            break;
		default:
			break;
		}
        if (fragment)
            pad->addChild(fragment);
	}
    pad->endRawSourceAnalyze();
    return pad;
}

PadItem *PadAnalyzerPrivate::nextPadItem()
{
    PadConditionnalSubItem *fragment;
	Lexem lex;
    PadItem *padItem = new PadItem;
    int s = QString(Constants::TOKEN_OPEN_DELIMITER).size();
    padItem->addDelimiter(_curPos - s, s);
    padItem->setStart(_curPos - s);
    padItem->setId(nextId());
    int previousType = PadItem::DefinedCore_PrependText;

	while ((lex = nextLexem()).type != Lexem_Null) {
		fragment = 0;
        switch (lex.type) {
        case Lexem_String:
        {
            switch (previousType) {
            case PadItem::DefinedCore_PrependText :
            {
                fragment = new PadConditionnalSubItem(PadConditionnalSubItem::Defined, PadConditionnalSubItem::Prepend);
                fragment->setStart(lex.start);
                fragment->setEnd(lex.end);


                break;
            }
            case PadItem::DefinedCore_AppendText:
            {
                fragment = new PadConditionnalSubItem(PadConditionnalSubItem::Defined, PadConditionnalSubItem::Append);
                fragment->setStart(lex.start);
                fragment->setEnd(lex.end);


                break;
            }
            case PadItem::UndefinedCore_AppendText : break;
            case PadItem::UndefinedCore_PrependText: break;
            default: LOG_ERROR_FOR(q->objectName(), "No type for PadItem subItem");
            }
            fragment->setId(nextId());
            padItem->addChild(fragment);
            break;
        }
        case Lexem_PadOpenDelimiter:
        {
            PadItem *item = nextPadItem();
            if (!item) { // an error occured, stop all
                delete padItem;
                return 0;
            }
            padItem->addChild(item);
            break;
        }
        case Lexem_PadCloseDelimiter:
            s = QString(Constants::TOKEN_CLOSE_DELIMITER).size();
            padItem->addDelimiter(_curPos - s, s);
            padItem->setEnd(_curPos);
            return padItem;
		case Lexem_CoreDelimiter:
        {
            PadCore *core = nextCore();
            if (!core) { // an error occured, stop all
				delete padItem;
				return 0;
			}
            padItem->addChild(core);
            previousType = PadItem::DefinedCore_AppendText;
			break;
        }
		default:
			break;
		}
	}
	delete padItem;
	return 0;
}


PadCore *PadAnalyzerPrivate::nextCore()
{
	Lexem lex;
	QMap<QString,QVariant> errorTokens;
    PadCore *core = new PadCore;
    int size = QString(Constants::TOKEN_CORE_DELIMITER).size();
    core->setStart(_curPos - size);
    core->setId(nextId());

	lex = nextLexem();
	if (lex.type == Lexem_String) {
        core->setUid(lex.value);
		lex = nextLexem();
	}

	if (lex.type != Lexem_CoreDelimiter) {
        errorTokens.insert("char", QString(Constants::TOKEN_CORE_DELIMITER));
		_lastErrors << Core::PadAnalyzerError(Core::PadAnalyzerError::Error_CoreDelimiterExpected,
                                              _curPos - 1,
                                              errorTokens);
		delete core;
		return 0;
	}
    core->setEnd(_curPos);
    core->setUid(getStringAt(core->start() + size, _sourceDocument, core->rawLength() - (size*2)));
    return core;
}

bool PadAnalyzerPrivate::isDelimiter(int pos, int *delimiterSize, LexemType *type)
{
    Q_ASSERT(delimiterSize);
    *delimiterSize = 0;
    *type = Lexem_Null;
    if (pos<0)
        return false;

    QChar currentChar;
    currentChar = getCharAt(pos, _sourceDocument);

    if (WarnIsDelimiterDebug)
        qWarning() << "    isDelimiter(); curChar" << currentChar;

    if (currentChar == Constants::TOKEN_OPEN_DELIMITER[0]) {
        QString tmp;
        int size = QString(Constants::TOKEN_OPEN_DELIMITER).size();
        tmp = getStringAt(pos, _sourceDocument, size);

        if (WarnIsDelimiterDebug)
            qWarning() << "  isDelimiter():: OpenDelim found" << tmp << "delimSize" << size;

        if (tmp == Constants::TOKEN_OPEN_DELIMITER) {
            *delimiterSize = size;
            *type = Lexem_PadOpenDelimiter;
            return true;
        }
    }
    if (currentChar == Constants::TOKEN_CLOSE_DELIMITER[0]) {
        QString tmp;
        int size = QString(Constants::TOKEN_OPEN_DELIMITER).size();
        tmp = getStringAt(pos, _sourceDocument, size);

        if (WarnIsDelimiterDebug)
            qWarning() << "  isDelimiter():: CloseDelim found" << tmp << "delimSize" << size;

        if (tmp == Constants::TOKEN_CLOSE_DELIMITER) {
            *delimiterSize = size;
            *type = Lexem_PadCloseDelimiter;
            return true;
        }
    }
    if (currentChar == Constants::TOKEN_CORE_DELIMITER[0]) {
        QString tmp;
        int size = QString(Constants::TOKEN_CORE_DELIMITER).size();
        tmp = getStringAt(pos, _sourceDocument, size);
        if (tmp == Constants::TOKEN_CORE_DELIMITER) {
            *delimiterSize = size;
            *type = Lexem_CoreDelimiter;
            return true;
        }
    }
    return false;
}

PadAnalyzerPrivate::Lexem PadAnalyzerPrivate::nextLexem()
{
    if (WarnLexemMatching)
        qWarning() << "Next Lexem; curpos" << _curPos <<  "atEnd" << atEnd();

    if (atEnd())
		return _lexemNull;

	Lexem lexem;
	lexem.start = _curPos;
	lexem.end = _curPos;

    int size = 0;
    LexemType type;
    if (isDelimiter(_curPos, &size, &type)) {
        if (WarnLexemMatching)
            qWarning() << "    isDelimiter" << _curPos << size << type;

        _curPos += size;
        lexem.type = type;
        lexem.end = _curPos;
        switch (type) {
        case Lexem_PadOpenDelimiter: lexem.rawValue = QString(Constants::TOKEN_OPEN_DELIMITER); break;
        case Lexem_PadCloseDelimiter: lexem.rawValue = QString(Constants::TOKEN_CLOSE_DELIMITER); break;
        case Lexem_CoreDelimiter: lexem.rawValue = QString(Constants::TOKEN_CORE_DELIMITER); break;
        default: break;
        }
        if (WarnLexemMatching)
            qWarning() << "  found delim" << lexem.rawValue << lexem.start << lexem.end;
        return lexem;
    }

	lexem.type = Lexem_String;
    if (WarnLexemMatching)
        qWarning() << "  isString";

    while (!atEnd() && !isDelimiter(_curPos, &size, &type)) {
        ++_curPos;
    }
    lexem.end = _curPos;

    if (WarnLexemMatching)
        qWarning() << "  * value" << lexem.value << "_curPos" << _curPos << "lex.start" << lexem.start << "lex.end" << lexem.end;

    return lexem;
}

bool PadAnalyzerPrivate::atEnd()
{
    QTextCursor cursor(_sourceDocument);
    cursor.setPosition(_curPos);
    return cursor.atEnd();
}
