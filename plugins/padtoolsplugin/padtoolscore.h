/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PADTOOLS_INTERNAL_PADTOOLSCORE_H
#define PADTOOLS_INTERNAL_PADTOOLSCORE_H

#include <QObject>

QT_BEGIN_NAMESPACE
class QAbstractItemModel;
QT_END_NAMESPACE

/**
 * \file ./plugins/padtoolsplugin/padtoolscore.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ITokenPool;
}

namespace PadTools {
namespace Internal {
class PadToolsPlugin;
class PadToolsCorePrivate;

class PadToolsCore : public QObject
{
    Q_OBJECT
    friend class PadTools::Internal::PadToolsPlugin;

protected:
    explicit PadToolsCore(QObject *parent = 0);
    bool initialize();

public:
    static PadToolsCore &instance();
    ~PadToolsCore();

    QAbstractItemModel *tokenModel() const;
    Core::ITokenPool *tokenPool() const;

private Q_SLOTS:
    void postCoreInitalization();

private:
    Internal::PadToolsCorePrivate *d;
    static PadToolsCore *_instance;
};

} // namespace Internal
} // namespace PadTools

#endif // PADTOOLS_INTERNAL_PADTOOLSCORE_H

