/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric Maeker <>                             *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "padtoolscontextualwidgetmanager.h"
#include "constants.h"
#include "padwriter.h"

#ifdef FREEDIAMS
#   include <drugsplugin/constants.h>
#endif

#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/constants_menus.h>
#include <coreplugin/constants_icons.h>
#include <coreplugin/contextmanager/contextmanager.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/actioncontainer.h>

#include <utils/log.h>
#include <utils/global.h>
#include <translationutils/constants.h>
#include <translationutils/trans_menu.h>
#include <translationutils/trans_database.h>

#include <QAction>
#include <QDialog>
#include <QGridLayout>
#include <QTreeWidget>
#include <QHeaderView>

#include <QDebug>

using namespace PadTools;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Core::ContextManager *contextManager() { return Core::ICore::instance()->contextManager(); }
static inline Core::ActionManager *actionManager() {return Core::ICore::instance()->actionManager();}
static inline Core::ITheme *theme() { return Core::ICore::instance()->theme(); }


static inline QAction *createAction(QObject *parent, const QString &name, const QString &icon,
                                    const QString &actionName,
                                    const Core::Context &context,
                                    const QString &trans, const QString &transContext,
                                    Core::Command *cmd,
                                    Core::ActionContainer *menu,
                                    const QString &group,
                                    QKeySequence::StandardKey key = QKeySequence::UnknownKey,
                                    bool checkable = false)
{
    QAction *a = new QAction(parent);
    a->setObjectName(name);
    if (!icon.isEmpty())
        a->setIcon(theme()->icon(icon));
    if (checkable) {
        a->setCheckable(true);
        a->setChecked(false);
    }
    cmd = actionManager()->registerAction(a, Core::Id(actionName), context);
    if (!transContext.isEmpty())
        cmd->setTranslations(trans, trans, transContext);
    else
        cmd->setTranslations(trans, trans); // use the Trans::Constants tr context (automatic)
    if (key != QKeySequence::UnknownKey)
        cmd->setDefaultKeySequence(key);
    if (menu)
        menu->addAction(cmd, Core::Id(group));
    return a;
}

PadToolsContextualWidgetManager::PadToolsContextualWidgetManager(QObject *parent) :
    PadToolsActionHandler(parent)
{
    connect(Core::ICore::instance()->contextManager(), SIGNAL(contextChanged(Core::IContext*,Core::Context)),
            this, SLOT(updateContext(Core::IContext*,Core::Context)));
    setObjectName("PadToolsContextualWidgetManager");
}

PadToolsContextualWidgetManager::~PadToolsContextualWidgetManager()
{
}


void PadToolsContextualWidgetManager::updateContext(Core::IContext *object, const Core::Context &additionalContexts)
{
    Q_UNUSED(additionalContexts);

    PadTools::Internal::PadWriter *view = 0;
    do {
        if (!object) {
            if (!m_CurrentView)
                return;
            break;
        }
        QWidget *testParents = object->widget();
        while (!view && testParents) {
            view = qobject_cast<PadTools::Internal::PadWriter *>(testParents);
            if (!view)
                testParents = testParents->parentWidget();
        }
        if (!view) {
            if (!m_CurrentView)
                return;
            break;
        }

        if (view == m_CurrentView) {
            return;
        }

    } while (false);

    if (view)
        PadToolsActionHandler::setCurrentView(view);
}

PadWriter *PadToolsContextualWidgetManager::currentView() const
{
    return PadToolsActionHandler::m_CurrentView;
}

PadToolsActionHandler::PadToolsActionHandler(QObject *parent) :
    QObject(parent),
    aShowSource(0),
    aViewOutput(0),
    m_CurrentView(0)
{
    setObjectName("PadToolsActionHandler");

    Core::Command *cmd = 0;
    Core::Context ctx(PadTools::Constants::C_PADTOOLS_PLUGINS);

    Core::ActionContainer *menu = actionManager()->actionContainer(PadTools::Constants::M_PLUGIN_PADTOOLS);
    if (!menu) {
        menu = actionManager()->createMenu(PadTools::Constants::M_PLUGIN_PADTOOLS);
        menu->setTranslations(PadTools::Constants::PADTOOLS_TEXT);

#ifdef FREEDIAMS
        actionManager()->actionContainer(Core::Id(DrugsWidget::Constants::M_PLUGINS_DRUGS))->addMenu(menu, Core::Constants::G_PLUGINS_PADTOOLS);
#else
#   ifdef FREEMEDFORMS
        actionManager()->actionContainer(Core::Id(Core::Constants::M_PLUGINS))->addMenu(menu, Core::Constants::G_PLUGINS_PADTOOLS);
#   else
        actionManager()->actionContainer(Core::Id(Core::Constants::MENUBAR))->addMenu(menu, Core::Constants::G_PLUGINS_PADTOOLS);
#   endif
#endif
    }
    Q_ASSERT(menu);



    aShowSource = createAction(this, "aShowSource", Constants::ICON_PADSOURCE,
                               Constants::A_PADTOOLS_SHOWSOURCE,
                               ctx,
                               Constants::SHOW_SOURCE, Constants::PADWRITER_TRANS_CONTEXT,
                               cmd,
                               0, "",
                               QKeySequence::UnknownKey, false);
    connect(aShowSource, SIGNAL(triggered()), this, SLOT(onShowSourceRequested()));

    aViewOutput = createAction(this, "aViewOutput", Constants::ICON_PADTOKENS,
                                     Constants::A_PADTOOLS_VIEWOUTPUT,
                                     ctx,
                                     Constants::VIEW_OUTPUT, Constants::PADWRITER_TRANS_CONTEXT,
                                     cmd,
                                     0, "",
                                     QKeySequence::UnknownKey, false);
    connect(aViewOutput, SIGNAL(triggered()), this, SLOT(onViewOutputRequested()));

    contextManager()->updateContext();
    actionManager()->retranslateMenusAndActions();
}

void PadToolsActionHandler::setCurrentView(PadWriter *view)
{
    Q_ASSERT(view);
    if (!view) { // this should never be the case
        LOG_ERROR("setCurrentView: no view");
        return;
    }

    if (m_CurrentView) {
    }
    m_CurrentView = view;


    updateActions();
}

void PadToolsActionHandler::updateActions()
{
}

void PadToolsActionHandler::onViewOutputRequested()
{
    if (m_CurrentView)
        m_CurrentView->onViewOutputRequested();
}

void PadToolsActionHandler::onShowSourceRequested()
{
    if (m_CurrentView)
        m_CurrentView->onShowSourceRequested();
}


