/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PADTOOLS_TOKENTREEVIEW_H
#define PADTOOLS_TOKENTREEVIEW_H

#include <QTreeView>

/**
 * \file ./plugins/padtoolsplugin/tokentreeview.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace PadTools {
namespace Internal {

class TokenTreeView : public QTreeView
{
    Q_OBJECT
public:
    explicit TokenTreeView(QWidget *parent = 0);

protected:
    void startDrag(Qt::DropActions supportedActions);

private:
//    QItemViewPaintPairs  draggablePaintPairs(const QModelIndexList &indexes, QRect *r);
    QPixmap renderToPixmap(const QModelIndexList &indexes, QRect *r);

};

} // namespace Internal
} // namespace PadTools

#endif // PADTOOLS_TOKENTREEVIEW_H
