/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>                  *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/
#ifndef PADTOOLS_DRAGDROPTEXTEDIT_H
#define PADTOOLS_DRAGDROPTEXTEDIT_H

#include <texteditorplugin/texteditor.h>

/**
 * \file ./plugins/padtoolsplugin/dragdroptextedit.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace PadTools {
namespace Internal {
class DragDropTextEdit : public Editor::TextEditor
{
public:
    DragDropTextEdit(QWidget *parent = 0);
    ~DragDropTextEdit();

private:
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dragLeaveEvent(QDragLeaveEvent *event);
    void dropEvent(QDropEvent *event);

};

} // namespace Internal
} // namespace PadTools

#endif // PADTOOLS_DRAGDROPTEXTEDIT_H
