/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>,                *
 *                    Guillaume Denry <guillaume.denry@gmail.com>          *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/
#ifndef PADTOOLS_PAD_STRING_H
#define PADTOOLS_PAD_STRING_H

#include <QString>

#include <padtoolsplugin/pad_fragment.h>

/**
 * \file ./plugins/padtoolsplugin/pad_string.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace PadTools {
namespace Internal {
class PadDocument;

class PadString : public PadFragment
{
public:
    PadString(const QString &string = "") : PadFragment(), _string(string) {}

	const QString &string() const { return _string; }
	void setValue(const QString &string) { _string = string; }

	void debug(int indent = 0) const;

    void run(QMap<QString,QVariant> &tokens, PadDocument *document);
    void toOutput(Core::ITokenPool *pool, PadDocument *document, TokenReplacementMethod);
    void toRaw(PadDocument *document);

private:
	QString _string;
};
} // namespace Internal
} // namespace PadTools

#endif  // PADTOOLS_PAD_STRING
