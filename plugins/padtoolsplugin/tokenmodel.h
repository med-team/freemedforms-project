/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>,                *
 *                    Guillaume Denry <guillaume.denry@gmail.com>          *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/
#ifndef PADTOOLS_TOKENMODEL_H
#define PADTOOLS_TOKENMODEL_H

#include <coreplugin/ipadtools.h>

#include <QStandardItemModel>
#include <QMap>

/**
 * \file ./plugins/padtoolsplugin/tokenmodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class IToken;
}

namespace PadTools {
namespace Internal {
class TokenModelPrivate;

class TokenModel : public QStandardItemModel
{
    Q_OBJECT
public:
    // DataRepresentation. See PadTools::Constants::TokenModelDataRepresentation

    explicit TokenModel(QObject *parent = 0);
    bool initialize();

    static Core::ITokenPool *tokenPool();
    void addToken(Core::IToken *token);
    void addTokens(const QVector<Core::IToken *> &token);

    int columnCount(const QModelIndex &) const;

    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);

    Qt::ItemFlags flags(const QModelIndex &index) const;
    Qt::DropActions supportedDropActions() const;
    QStringList mimeTypes() const;
    QMimeData *mimeData(const QModelIndexList &indexes) const;

Q_SIGNALS:
    void tokenChanged(const QString &token, const QString &value);

private:
    Internal::TokenModelPrivate *d;
};

}  // namespace Internal
}  // namespace PadTools

#endif // PADTOOLS_TOKENMODEL_H
