/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:  Eric Maeker <eric.maeker@gmail.com>,                *
 *                    Guillaume Denry <guillaume.denry@gmail.com>          *
 *  Contributors:                                                          *
 *      NAME <MAIL@ADDRESS.COM>                                            *
 ***************************************************************************/
#ifndef PADTOOLS_PAD_ANALYZER_H
#define PADTOOLS_PAD_ANALYZER_H

#include <QString>
#include <QStack>

#include <coreplugin/ipadtools.h>

#include "pad_fragment.h"
#include "pad_string.h"
#include "pad_document.h"
#include "pad_item.h"

/**
 * \file ./plugins/padtoolsplugin/pad_analyzer.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace PadTools {
namespace Internal {
class PadDocument;
class PadAnalyzerPrivate;

class PadAnalyzer : public QObject
{
    Q_OBJECT
public:
    PadAnalyzer(QObject *parent = 0);
    ~PadAnalyzer();

    PadDocument *analyze(const QString &source);
    PadDocument *analyze(QTextDocument *source, PadDocument *padDocument);

    const QList<Core::PadAnalyzerError> lastErrors() const;

private:
    Internal::PadAnalyzerPrivate *d;
};

} // namespace Internal
} // namespace PadTools

#endif  // PADTOOLS_PAD_ANALYZER
