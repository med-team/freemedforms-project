/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TEXTEDITORPLUGIN_TEXTEDITORDIALOG_H
#define TEXTEDITORPLUGIN_TEXTEDITORDIALOG_H

#include <texteditorplugin/texteditor_exporter.h>
#include <QDialog>

/**
 * \file ./plugins/texteditorplugin/texteditordialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Editor {
namespace Internal {
class TextEditorDialogPrivate;
}  // End Internal

class EDITOR_EXPORT TextEditorDialog : public QDialog
{
    Q_OBJECT
public:
    enum SaveOnCloseMethod {
        AlwaysSaveWhenClose = 0,
        AskUserToSaveWhenClose,
        NoAutomaticSavingOnClose
    };

    explicit TextEditorDialog(QWidget *parent = 0);
    ~TextEditorDialog();

    void setSaveOnCloseMethod(SaveOnCloseMethod method);

    void readFile(const QString &absPath);
    QString currentEditingFile() const;

private Q_SLOTS:
    void done(int r);

private:
    Internal::TextEditorDialogPrivate *d;
};

}  // End Editor

#endif // TEXTEDITORPLUGIN_TEXTEDITORDIALOG_H
