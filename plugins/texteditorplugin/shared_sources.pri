#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

DEFINES += EDITOR_LIBRARY

include($${PWD}/../fmf_plugins.pri)
include($${PWD}/texteditorplugin_dependencies.pri )

HEADERS +=  \
    $${PWD}/texteditor_exporter.h \
    $${PWD}/editoractionhandler.h \
    $${PWD}/editorcontext.h \
    $${PWD}/editormanager.h \
    $${PWD}/tabledialog.h \
    $${PWD}/tableeditor.h \
    $${PWD}/texteditor.h \
    $${PWD}/tablepropertieswidget.h \
    $${PWD}/texteditordialog.h

SOURCES += \
    $${PWD}/editoractionhandler.cpp \
    $${PWD}/editormanager.cpp \
    $${PWD}/tableeditor.cpp \
    $${PWD}/texteditor.cpp \
    $${PWD}/tablepropertieswidget.cpp \
    $${PWD}/texteditordialog.cpp

FORMS += $${PWD}/tabledialog.ui \
    $${PWD}/tablepropertieswidget.ui \
    $${PWD}/tablepropertiesdialog.ui \
    $${PWD}/texteditordialog.ui

# include translations
TRANSLATION_NAME = texteditor
include($${SOURCES_ROOT_PATH}/buildspecs/translations.pri)
