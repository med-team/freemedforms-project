/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TABLEEDITOR_H
#define TABLEEDITOR_H

#include <QWidget>
#include <QTextEdit>

/**
 * \file ./plugins/texteditorplugin/tableeditor.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Editor {

class TableEditor : public QWidget
{
    Q_OBJECT
public:
    TableEditor(QWidget *parent = 0);
    virtual ~TableEditor();

    virtual QTextEdit *textEdit() const = 0;

public Q_SLOTS:
    void addTable();
    void tableProperties();
    void tableAddRow();
    void tableAddCol();
    void tableRemoveRow();
    void tableRemoveCol();
    void tableMergeCells();
    void tableSplitCells();

};

}  // End Editor

#endif // TABLEEDITOR_H
