/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef EDITORMANAGER_H
#define EDITORMANAGER_H

#include <texteditorplugin/texteditor_exporter.h>
#include <texteditorplugin/editoractionhandler.h>

#include <QObject>

namespace Core {
class IContext;
}

namespace Editor {
class TextEditor;

namespace Internal {

class EditorManager : public EditorActionHandler
{
    Q_OBJECT
public:
    static EditorManager *instance(QObject *parent = 0);
    virtual ~EditorManager();

private Q_SLOTS:
    void updateContext(Core::IContext *object, const Core::Context &additionalContexts);

private:
    EditorManager(QObject *parent = 0);
    static EditorManager *m_Instance;
};

}  // End Internal
}  // End Editor

#endif // EDITORMANAGER_H
