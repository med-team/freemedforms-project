/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef EDITORCONTEXT_H
#define EDITORCONTEXT_H

// include toolkit headers
#include <texteditorplugin/texteditor_exporter.h>
#include <coreplugin/contextmanager/icontext.h>


namespace Editor {
class TextEditor;

namespace Internal {

class EditorContext : public Core::IContext
{
public:
    EditorContext(TextEditor *parent) : Core::IContext(parent)
    {
        setObjectName("EditorContext");
        setWidget(parent);
    }
};

}  // End Internal
}  // End Editor

#endif // EDITORCONTEXT_H
