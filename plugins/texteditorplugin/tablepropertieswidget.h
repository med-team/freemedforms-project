/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TABLEPROPERTIESWIDGET_H
#define TABLEPROPERTIESWIDGET_H

#include <QWidget>
#include <QDialog>
#include <QTextTableFormat>

/**
 * \file ./plugins/texteditorplugin/tablepropertieswidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Editor {
namespace Internal {
namespace Ui {
class TablePropertiesWidget;
class TablePropertiesDialog;
}

class TablePropertiesWidget : public QWidget
{
    Q_OBJECT
public:
    TablePropertiesWidget(QWidget *parent = 0);
    ~TablePropertiesWidget();

    void setFormat(const QTextTableFormat &format);
    QTextTableFormat format() const;

    int cellLeftMargin() const;
    int cellRightMargin() const;
    int cellTopMargin() const;
    int cellBottomMargin() const;
    int cellSpacing() const;
    int cellPadding() const;
    int borderWidth() const;


private:
    Ui::TablePropertiesWidget *m_ui;
    QTextTableFormat m_InitialFormat;
};


class TablePropertiesDialog : public QDialog
{
    Q_OBJECT
public:
    TablePropertiesDialog(QWidget *parent = 0);
    ~TablePropertiesDialog();

    void setFormat(const QTextTableFormat &format);
    QTextTableFormat format() const;

    bool applyToSelectedCells() const;
    bool applyToWholeTable() const;

    int cellLeftMargin() const;
    int cellRightMargin() const;
    int cellTopMargin() const;
    int cellBottomMargin() const;
    int cellPadding() const;
    int cellSpacing() const;
    int borderWidth() const;

private:
    bool applyToCell, applyToTable;
    TablePropertiesWidget *m_Widget;
    Ui::TablePropertiesDialog *m_ui;
};

}  // End Internal
}  // End Editor


#endif // TABLEPROPERTIESWIDGET_H
