/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "formmanagerplugin.h"
#include "formcore.h"
#include "formmanagermode.h"
#include "formmanager.h"
#include "formplaceholder.h"
#include "episodebase.h"
#include "episodemodel.h"
#include "formmanagerpreferencespage.h"
#include "firstrunformmanager.h"

#include <utils/log.h>

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/itheme.h>
#include <coreplugin/iuser.h>
#include <coreplugin/translators.h>

#include <QtCore/QtPlugin>
#include <QWidget>
#include <QDebug>

static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline Core::ISettings *settings()  { return Core::ICore::instance()->settings(); }
static inline Core::IUser *user()  { return Core::ICore::instance()->user(); }
static inline void messageSplash(const QString &s) {theme()->messageSplashScreen(s); }
static inline Form::FormManager &formManager() {return Form::FormCore::instance().formManager();}
static inline Form::Internal::EpisodeBase *episodeBase() {return Form::Internal::EpisodeBase::instance();}

using namespace Form;
using namespace Internal;

FormManagerPlugin::FormManagerPlugin() :
    ExtensionSystem::IPlugin(),
    _core(0),
    _mode(0),
    m_FirstRun(0),
    m_PrefPageSelector(0),
    m_PrefPage(0)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating FormManagerPlugin";

    Core::ICore::instance()->translators()->addNewTranslator("plugin_formmanager");

    m_FirstRun = new Internal::FirstRunFormManagerConfigPage(this);
    addObject(m_FirstRun);

    m_PrefPageSelector = new Internal::FormPreferencesFileSelectorPage(this);
    m_PrefPage = new Internal::FormPreferencesPage(this);
    addAutoReleasedObject(m_PrefPage);
    addAutoReleasedObject(m_PrefPageSelector);

    _core = new Form::FormCore(this);
    connect(Core::ICore::instance(), SIGNAL(coreOpened()), this, SLOT(postCoreInitialization()));
}

FormManagerPlugin::~FormManagerPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool FormManagerPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "FormManagerPlugin::initialize";
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);

    messageSplash(tr("Initializing form manager plugin..."));

    return _core->initialize();
}

void FormManagerPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "FormManagerPlugin::extensionsInitialized";

    if (!user())
        return;
    if (user()->uuid().isEmpty())
        return;

    messageSplash(tr("Initializing form manager plugin..."));


    episodeBase()->initialize();
    formManager().checkFormUpdates();

    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));

    _mode = new FormManagerMode(this);
}

void FormManagerPlugin::postCoreInitialization()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << Q_FUNC_INFO;


    const QString &uid = settings()->defaultForm();
    if (!uid.isEmpty()) {
        episodeBase()->setGenericPatientFormFile(uid);
        formManager().readPmhxCategories(uid);
        formManager().loadPatientFile();
        settings()->setDefaultForm("");
    } else {
        formManager().readPmhxCategories("");
        formManager().loadPatientFile();
    }
}

ExtensionSystem::IPlugin::ShutdownFlag FormManagerPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    if (m_FirstRun) {
        removeObject(m_FirstRun);
        delete m_FirstRun;
        m_FirstRun = 0;
    }
    delete _mode;
    delete _core;
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(FormManagerPlugin)
