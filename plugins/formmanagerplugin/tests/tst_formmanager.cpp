/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, <eric.maeker@gmail.com>,                             *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "../formmanagerplugin.h"
#include "../formcore.h"
#include "../formmanager.h"
#include "../iformio.h"

#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/constants_tokensandsettings.h>

#include <utils/log.h>
#include <utils/global.h>
#include <utils/randomizer.h>
#include <extensionsystem/pluginmanager.h>

#include <QDir>
#include <QFileInfo>
#include <QPixmap>
#include <QTest>
#include <QSignalSpy>


using namespace Form;
using namespace Internal;

static inline ExtensionSystem::PluginManager *pluginManager() { return ExtensionSystem::PluginManager::instance(); }
static inline Core::ISettings *settings()  { return Core::ICore::instance()->settings(); }
static inline Form::FormCore &formCore() {return Form::FormCore::instance();}
static inline Form::FormManager &formManager() {return Form::FormCore::instance().formManager();}

static inline Form::IFormIO *_xmlIo()
{
    Form::IFormIO *xmlIo = 0;
    QList<Form::IFormIO *> list = pluginManager()->getObjects<Form::IFormIO>();
    foreach(Form::IFormIO *io, list) {
        if (io->name() == "XmlFormIO")
            xmlIo = io;
    }
    return xmlIo;
}

void FormManagerPlugin::test_FormManager_initialization()
{
    Utils::Log::muteObjectConsoleWarnings("XmlFormIO");
    Utils::Log::muteObjectConsoleWarnings("XmlIOBase");

    QCOMPARE(formCore().formManager().isInitialized(), true);


}

void FormManagerPlugin::test_FormIOQuery()
{
    Form::FormIOQuery query;

    QVERIFY(query.typeOfForms() == Form::FormIOQuery::CompleteForms);
    QVERIFY(query.excludeGenderSpecific() == false);
    QVERIFY(query.isoLanguagesAndCountry().isEmpty());
    QVERIFY(query.specialties().isEmpty());
    QVERIFY(query.authors().isEmpty());
    QVERIFY(query.formUuid().isEmpty());
    QVERIFY(query.forceFileReading() == false);
    QVERIFY(query.getAllAvailableForms() == false);
    QVERIFY(query.getAllAvailableFormDescriptions() == false);

    query.setTypeOfForms(Form::FormIOQuery::CompleteForms);
    QVERIFY(query.typeOfForms() == Form::FormIOQuery::CompleteForms);
    query.setTypeOfForms(Form::FormIOQuery::SubForms);
    QVERIFY(query.typeOfForms() == Form::FormIOQuery::SubForms);
    query.setTypeOfForms(Form::FormIOQuery::CompleteForms | Form::FormIOQuery::SubForms);
    QVERIFY(query.typeOfForms() == (Form::FormIOQuery::CompleteForms | Form::FormIOQuery::SubForms));

    query.setExcludeGenderSpecific(true);
    QVERIFY(query.excludeGenderSpecific() == true);
    query.setExcludeGenderSpecific(false);
    QVERIFY(query.excludeGenderSpecific() == false);

    QStringList test;
    test << "fr_FR" << "en_US";
    query.setIsoLanguageAndCountry(test);
    QVERIFY(query.isoLanguagesAndCountry() == test);

    test.clear();
    test << "Spe1" << "Spe2";
    query.setSpecialties(test);
    QVERIFY(query.specialties() == test);

    test.clear();
    test << "Author1" << "Author2";
    query.setAuthors(test);
    QVERIFY(query.authors() == test);

    query.setFormUuid("FormUid");
    QVERIFY(query.formUuid() == "FormUid");

    query.setForceFileReading(false);
    QVERIFY(query.forceFileReading() == false);
    query.setForceFileReading(true);
    QVERIFY(query.forceFileReading() == true);

    query.setGetAllAvailableForms(false);
    QVERIFY(query.getAllAvailableForms() == false);
    query.setGetAllAvailableForms(true);
    QVERIFY(query.getAllAvailableForms() == true);

    query.setGetAllAvailableFormDescriptions(false);
    QVERIFY(query.getAllAvailableFormDescriptions() == false);
    query.setGetAllAvailableFormDescriptions(true);
    QVERIFY(query.getAllAvailableFormDescriptions() == true);
}

void FormManagerPlugin::test_FormIO_queryFromDatabase()
{

    QList<Form::IFormIO *> list = pluginManager()->getObjects<Form::IFormIO>();
    QVERIFY(list.count() >= 1);

    Form::IFormIO *xmlIo = _xmlIo();
    QVERIFY(xmlIo != 0);

    QDir completeFormsDir(settings()->path(Core::ISettings::CompleteFormsPath));
    QVERIFY(completeFormsDir.exists() == true);
    QDir subFormsDir(settings()->path(Core::ISettings::SubFormsPath));
    QVERIFY(subFormsDir.exists() == true);

    int countComplete = Utils::getFiles(completeFormsDir, "central.xml").count();
    QVERIFY(countComplete >= 1);
    int countSub = Utils::getFiles(subFormsDir, "central.xml").count();
    QVERIFY(countSub >= 1);

    Form::FormIOQuery query;
    query.setTypeOfForms(Form::FormIOQuery::CompleteForms);
    QList<Form::FormIODescription*> completeDescriptions = xmlIo->getFormFileDescriptions(query);
    query.setTypeOfForms(Form::FormIOQuery::SubForms);
    QList<Form::FormIODescription*> subDescriptions = xmlIo->getFormFileDescriptions(query);
    QVERIFY(completeDescriptions.count() >= countComplete);
    QVERIFY(subDescriptions.count() >= countSub);
}

void FormManagerPlugin::test_FormIO_queryFromLocal()
{
    Form::IFormIO *xmlIo = _xmlIo();
    QVERIFY(xmlIo != 0);

    QDir completeFormsDir(settings()->path(Core::ISettings::CompleteFormsPath));
    QVERIFY(completeFormsDir.exists() == true);
    QDir subFormsDir(settings()->path(Core::ISettings::SubFormsPath));
    QVERIFY(subFormsDir.exists() == true);
    int countComplete = Utils::getFiles(completeFormsDir, "central.xml").count();
    QVERIFY(countComplete >= 1);
    int countSub = Utils::getFiles(subFormsDir, "central.xml").count();
    QVERIFY(countSub >= 1);

    QDir completeUserFormsDir(settings()->path(Core::ISettings::UserCompleteFormsPath));
    QVERIFY(completeUserFormsDir.exists() == true);
    QDir subUserFormsDir(settings()->path(Core::ISettings::UserSubFormsPath));
    QVERIFY(subUserFormsDir.exists() == true);
    countComplete += Utils::getFiles(completeUserFormsDir, "central.xml").count();
    countSub += Utils::getFiles(subUserFormsDir, "central.xml").count();

    QDir completeDatapackFormsDir(settings()->path(Core::ISettings::DataPackCompleteFormsInstallPath));
    QVERIFY(completeDatapackFormsDir.exists() == true);
    QDir subDatapackFormsDir(settings()->path(Core::ISettings::DataPackSubFormsInstallPath));
    QVERIFY(subDatapackFormsDir.exists() == true);
    countComplete += Utils::getFiles(completeDatapackFormsDir, "central.xml").count();
    countSub += Utils::getFiles(subDatapackFormsDir, "central.xml").count();

    Form::FormIOQuery query;
    query.setForceFileReading(true);
    query.setTypeOfForms(Form::FormIOQuery::CompleteForms);
    QList<Form::FormIODescription*> localCompleteDescriptions = xmlIo->getFormFileDescriptions(query);
    query.setTypeOfForms(Form::FormIOQuery::SubForms);
    QList<Form::FormIODescription*> localSubDescriptions = xmlIo->getFormFileDescriptions(query);


    QVERIFY(localCompleteDescriptions.count() == countComplete);
    QVERIFY(localSubDescriptions.count() == countSub);

    query.setForceFileReading(false);
    query.setTypeOfForms(Form::FormIOQuery::CompleteForms);
    QList<Form::FormIODescription*> dbCompleteDescriptions = xmlIo->getFormFileDescriptions(query);
    query.setTypeOfForms(Form::FormIOQuery::SubForms);
    QList<Form::FormIODescription*> dbSubDescriptions = xmlIo->getFormFileDescriptions(query);
    int found = 0;
    foreach(Form::FormIODescription *desc, localCompleteDescriptions) {
        QString local = desc->data(Form::FormIODescription::UuidOrAbsPath).toString();
        foreach(Form::FormIODescription *dbDesc, dbCompleteDescriptions) {
            QString db = dbDesc->data(Form::FormIODescription::UuidOrAbsPath).toString();
            if (db == local) {
                ++found;
                break;
            }
        }
    }
    QVERIFY(found == dbCompleteDescriptions.count());

    found = 0;
    foreach(Form::FormIODescription *desc, localSubDescriptions) {
        QString local = desc->data(Form::FormIODescription::UuidOrAbsPath).toString();
        foreach(Form::FormIODescription *dbDesc, dbSubDescriptions) {
            QString db = dbDesc->data(Form::FormIODescription::UuidOrAbsPath).toString();
            if (db == local) {
                ++found;
                break;
            }
        }
    }
    QVERIFY(found == dbSubDescriptions.count());
}

void FormManagerPlugin::test_FormIO_screenshots()
{
    Form::IFormIO *xmlIo = _xmlIo();

    QHash<QString, int> uidCount;
    QFileInfoList files = Utils::getFiles(QDir(settings()->path(Core::ISettings::CompleteFormsPath)), "*.png");
    foreach(const QFileInfo &file, files) {
        QString fileName = file.absoluteFilePath();
        fileName.replace(settings()->path(Core::ISettings::CompleteFormsPath), Core::Constants::TAG_APPLICATION_COMPLETEFORMS_PATH);


        int splitAt = fileName.indexOf("/", fileName.indexOf("/") + 1);
        QString uid = fileName.left(splitAt);
        QString shot = fileName.mid(fileName.indexOf("shots/", splitAt) + 6);
        QVERIFY(xmlIo->screenShot(uid, shot).size() == QPixmap(file.absoluteFilePath()).size());

        uidCount.insert(uid, uidCount.value(uid)+1);
    }

    foreach(const QString &uid, uidCount.keys()) {
        const QList<QPixmap> &pix = xmlIo->screenShots(uid);
        QVERIFY(pix.count() == uidCount.value(uid));
    }
}

void FormManagerPlugin::test_FormIO_userForms()
{
    Form::IFormIO *xmlIo = _xmlIo();
    Form::FormIOQuery query;
    query.setTypeOfForms(Form::FormIOQuery::CompleteForms);
    QList<Form::FormIODescription*> completeDescriptions = xmlIo->getFormFileDescriptions(query);
    query.setTypeOfForms(Form::FormIOQuery::SubForms);
    QList<Form::FormIODescription*> subDescriptions = xmlIo->getFormFileDescriptions(query);
    int previousCompleteCount = completeDescriptions.count();

    QDir completeFormsDir(settings()->path(Core::ISettings::CompleteFormsPath));
    QVERIFY(completeFormsDir.exists() == true);
    QDir subFormsDir(settings()->path(Core::ISettings::SubFormsPath));
    QVERIFY(subFormsDir.exists() == true);

    QFileInfoList completeFormsFiles = Utils::getFiles(completeFormsDir, "central.xml");
    int countComplete = completeFormsFiles.count();
    QVERIFY(countComplete >= 1);
    int countSub = Utils::getFiles(subFormsDir, "central.xml").count();
    QVERIFY(countSub >= 1);

    Utils::Randomizer randomizer;
    int random = randomizer.randomInt(0, completeFormsFiles.count() - 1);
    QString fileName = completeFormsFiles.at(random).absoluteFilePath();
    QString formPath = fileName.left(fileName.indexOf("/", fileName.indexOf("/completeforms/") + 16));
    QString destPath = settings()->path(Core::ISettings::UserCompleteFormsPath) + "/new_one-" + Utils::createUid();
    QVERIFY(Utils::copyDir(formPath, destPath));

    QString centralFile = destPath + "/central.xml";
    QString content = Utils::readTextFile(centralFile);
    content.replace(QRegExp("<description*</description>", Qt::CaseSensitive, QRegExp::Wildcard),
                    QString("<description lang=\"xx\">Unit-test created (%1)</description>")
                    .arg(QLocale().toString(QDateTime::currentDateTime(), QLocale::LongFormat)));
    QVERIFY(Utils::saveStringToFile(content, centralFile, Utils::Overwrite, Utils::DontWarnUser) == true);


    QVERIFY(xmlIo->saveForm(centralFile));


    query.setTypeOfForms(Form::FormIOQuery::CompleteForms);
    completeDescriptions = xmlIo->getFormFileDescriptions(query);
    bool returned = false;
    QString formUid = destPath;
    formUid.replace(settings()->path(Core::ISettings::UserCompleteFormsPath),
                    Core::Constants::TAG_APPLICATION_USER_COMPLETEFORMS_PATH);
    foreach(Form::FormIODescription *desc, completeDescriptions) {
        qDebug() <<  desc->data(Form::FormIODescription::UuidOrAbsPath) << formUid;
        if (desc->data(Form::FormIODescription::UuidOrAbsPath) == formUid) {
            returned = true;
            break;
        }
    }
    QVERIFY(returned == true);
    query.setTypeOfForms(Form::FormIOQuery::SubForms);
    subDescriptions = xmlIo->getFormFileDescriptions(query);
    QVERIFY(completeDescriptions.count() == (previousCompleteCount+1));


    Utils::Log::unmuteObjectConsoleWarnings("XmlFormIO");
    Utils::Log::unmuteObjectConsoleWarnings("XmlIOBase");
}

