/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IFORMITEMVALUES_H
#define IFORMITEMVALUES_H

#include <formmanagerplugin/formmanager_exporter.h>

#include <coreplugin/idebugpage.h>
#include <translationutils/constants.h>

#include <QObject>
#include <QString>
#include <QDateTime>
#include <QWidget>
#include <QVariant>
#include <QPointer>
#include <QHash>

QT_BEGIN_NAMESPACE
class QTreeWidget;
class QTreeWidgetItem;
QT_END_NAMESPACE

/**
 * \file ./plugins/formmanagerplugin/iformitemvalues.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Form {

namespace Internal {
class FormItemValuesPrivate;
}

class FORM_EXPORT FormItemValues // : public QObject
{
public:
    enum {
        Value_Uuid = 0,
        Value_Possible,
        Value_Script,
        Value_Numerical,
        Value_Default,
        Value_Printing,
        Value_Dependency
    };
//    FormItemValues(QObject *parent);
    FormItemValues();
    ~FormItemValues();

    void setValue(int type, const int id, const QVariant &val, const QString &language = QString::null);
    void setDefaultValue(const QVariant &val, const QString &lang = QString::null);
    QVariant defaultValue(const QString &lang = QString::null) const;

    bool isOptional() const;
    void setOptional(bool state);

    QStringList values(const int typeOfValues) const;

    // filename
    void setFileName(const QString &fileName) {m_FileName=fileName;}
    QString fileName() const {return m_FileName;}

    void toTreeWidget(QTreeWidgetItem *tree) const;

private:
    Internal::FormItemValuesPrivate *d;
    QString m_FileName;
};

} // end Form

#endif // IFORMITEMVALUES_H
