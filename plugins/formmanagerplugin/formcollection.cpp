/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
/*!
 * \class Form::FormCollection
 * \brief Holds a book of forms (a specific mode, the central form, duplicates or not...).
 * The form books are created and managed by the Form::FormManager object. Books contain
 * all forms available for a specific mode. This object is responsible of the life of
 * the registered Form::FormMain pointers. Every Form::FormMain pointers are deleted here.
 * \sa Form::FormManager
 */

#include "formcollection.h"
#include "iformitem.h"

#include <translationutils/constants.h>

#include <QDebug>

using namespace Form;
using namespace Internal;
using namespace Trans::ConstantTranslations;

namespace Form {
namespace Internal {
class FormCollectionPrivate
{
public:
    FormCollectionPrivate(FormCollection */*parent*/) :
        _isDuplicates(false),
        _type(FormCollection::CompleteForm)  // , q(parent)
    {
    }

    ~FormCollectionPrivate()
    {
    }

public:
    bool _isDuplicates;
    QString _modeUid, _formUid;
    QList<Form::FormMain *> _emptyRootForms;
    FormCollection::CollectionType _type;

private:
};
}  // namespace Internal
} // end namespace Form

/*! Constructor of the Form::FormCollection class */
FormCollection::FormCollection() : //(QObject *parent) :
    d(new FormCollectionPrivate(this))
{
}

/*! Destructor of the Form::FormCollection class */
FormCollection::~FormCollection()
{
    qDeleteAll(d->_emptyRootForms);
    d->_emptyRootForms.clear();
    if (d)
        delete d;
    d = 0;
}

/*! Initializes the object with the default values. Return true if initialization was completed. */
bool FormCollection::initialize()
{
    return true;
}

void FormCollection::setDuplicates(bool isDuplicates)
{
    d->_isDuplicates = isDuplicates;
}

void FormCollection::setType(CollectionType type)
{
    d->_type = type;
}

bool FormCollection::isNull() const
{
    return d->_modeUid.isEmpty() && d->_formUid.isEmpty() && d->_emptyRootForms.isEmpty();
}

bool FormCollection::isDuplicates() const
{
    return d->_isDuplicates;
}

QString FormCollection::formUid() const
{
    return d->_formUid;
}

QString FormCollection::modeUid() const
{
    return d->_modeUid;
}

FormCollection::CollectionType FormCollection::type() const
{
    return d->_type;
}

void FormCollection::setEmptyRootForms(const QList<Form::FormMain *> &emptyRootForms)
{
    d->_emptyRootForms = emptyRootForms;
    if (emptyRootForms.count()) {
        d->_formUid = emptyRootForms.at(0)->uuid();
        d->_modeUid = emptyRootForms.at(0)->modeUniqueName();
    }
}

void FormCollection::addEmptyRootForm(Form::FormMain *emptyRootForm)
{
    d->_emptyRootForms << emptyRootForm;
}

const QList<FormMain *> &FormCollection::emptyRootForms() const
{
    return d->_emptyRootForms;
}

bool FormCollection::containsFormUid(const QString &formUid) const
{
    return (form(formUid)!=0);
}

bool FormCollection::containsIdentityForm() const
{
    return (identityForm()!=0);
}

Form::FormMain *FormCollection::identityForm() const
{
    for(int i=0; i < d->_emptyRootForms.count(); ++i) {
        FormMain *root = d->_emptyRootForms.at(i);
        if (root->spec()->value(FormItemSpec::Spec_IsIdentityForm).toBool())
            return root;
        foreach(FormMain *form, root->flattenedFormMainChildren()) {
            if (form->spec()->value(FormItemSpec::Spec_IsIdentityForm).toBool())
                return form;
        }
    }
    return 0;
}


Form::FormMain *FormCollection::form(const QString &formUid) const
{
    foreach(Form::FormMain *form, d->_emptyRootForms) {
        if (form->uuid() == formUid)
            return form;
        foreach(Form::FormMain *test, form->flattenedFormMainChildren()) {
            if (test->uuid() == formUid)
                return test;
        }
    }
    return 0;
}
