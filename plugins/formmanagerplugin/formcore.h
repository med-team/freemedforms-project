/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers :                                                     *
 *       Eric MAEKER <eric.maeker@gmail.com>                               *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FORM_FORMCORE_H
#define FORM_FORMCORE_H

#include <formmanagerplugin/formmanager_exporter.h>
#include <QObject>

/**
 * \file ./plugins/formmanagerplugin/formcore.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Form {
class FormManager;
class EpisodeManager;
class PatientFormItemDataWrapper;

namespace Internal {
class FormManagerPlugin;
class FormCorePrivate;
}  // namespace Internal

class FORM_EXPORT FormCore : public QObject
{
    Q_OBJECT
    friend class Form::Internal::FormManagerPlugin;

protected:
    explicit FormCore(QObject *parent = 0);
    bool initialize();

public:
    static FormCore &instance();
    ~FormCore();
    bool isInitialized() const;

    Form::FormManager &formManager() const;
    Form::EpisodeManager &episodeManager() const;
    Form::PatientFormItemDataWrapper &patientFormItemDataWrapper() const;

Q_SIGNALS:

public Q_SLOTS:
    void activatePatientFileCentralMode();

private:
    Internal::FormCorePrivate *d;
    static FormCore *_instance;
};

} // namespace Form

#endif  // FORM_FORMCORE_H

