/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "iformwidgetfactory.h"
#include "iformitem.h"







































#include <QLocale>
#include <QEvent>
#include <QBoxLayout>

using namespace Form;

IFormWidget::IFormWidget(Form::FormItem *formItem, QWidget *parent) :
    QWidget(parent),
    m_Label(0),
    m_FormItem(formItem),
    m_focusedWidget(0),
    _lastTabWidget(0)
{
    Q_ASSERT(formItem);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_FormItem->setFormWidget(this);
    m_OldTrans = QLocale().name().left(2);
}

IFormWidget::~IFormWidget()
{
}

void IFormWidget::createLabel(const QString &text, Qt::Alignment horizAlign)
{
    m_Label = new QLabel();
    QLabel *l = m_Label;
    l->setFrameStyle(IFormWidget::ItemLabelFrame);
    l->setText(text);
    l->setAlignment(horizAlign);
    l->setWordWrap(true);
}

QBoxLayout *IFormWidget::getBoxLayout(const int labelOption, const QString &text, QWidget *parent)
{
    QBoxLayout *hb;
    m_Label = new QLabel(this);
    m_Label->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    if (labelOption == OnTop) {
        hb = new QBoxLayout(QBoxLayout::TopToBottom, parent);
        createLabel(text, Qt::AlignTop | Qt::AlignLeft);
        hb->setSpacing(5);
    } else {
        hb = new QHBoxLayout(parent);
        if (labelOption != NoLabel) {
            createLabel(text, Qt::AlignTop | Qt::AlignLeft);
        }
        hb->setSpacing(5);
        hb->setMargin(5);
    }
    return hb;
}

QHBoxLayout *IFormWidget::getHBoxLayout(const int labelOption, const QString &text, QWidget *parent)
{
    QHBoxLayout *hb = new QHBoxLayout(parent);
    if (labelOption != NoLabel) {
        createLabel(text);
    }
    if (m_Label) {
        hb->insertWidget(0, m_Label);
    }
    hb->setSpacing(5);
    hb->setMargin(5);
    return hb;
}

void IFormWidget::changeEvent(QEvent *event)
{
    QString loc = QLocale().name().left(2);
    if ((event->type() == QEvent::LanguageChange) &&
        (m_OldTrans != loc) &&
            m_FormItem) {
        m_OldTrans = loc;
        retranslate();
        event->accept();
        return;
    }
    QWidget::changeEvent(event);
}

