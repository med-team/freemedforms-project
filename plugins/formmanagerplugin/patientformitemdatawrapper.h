/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FORM_PATIENTFORMITEMDATAWRAPPER_H
#define FORM_PATIENTFORMITEMDATAWRAPPER_H

#include <formmanagerplugin/formmanager_exporter.h>
#include <QObject>
#include <QModelIndex>

/**
 * \file ./plugins/formmanagerplugin/patientformitemdatawrapper.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Form {
class FormCore;

namespace Internal {
class PatientFormItemDataWrapperPrivate;
} // namespace Internal

class FORM_EXPORT PatientFormItemDataWrapper : public QObject
{
    Q_OBJECT
    friend class Form::FormCore;

protected:
    explicit PatientFormItemDataWrapper(QObject *parent = 0);
    bool initialize();
    
public:
    ~PatientFormItemDataWrapper();
    bool isInitialized() const;

    bool isDataAvailable(int ref) const;
    QVariant data(int ref, int role = -1) const;

private Q_SLOTS:
    void onCurrentPatientChanged();
//    void onCurrentPatientFormsLoaded();
    void editingModelEpisodeChanged(const QModelIndex &index);
    void editingModelRowsInserted(const QModelIndex &parent, int first, int last);
    void editingModelRowsRemoved(const QModelIndex &parent, int first, int last);

private:
    Internal::PatientFormItemDataWrapperPrivate *d;
};

} // namespace Form

#endif  // FORM_PATIENTFORMITEMDATAWRAPPER_H

