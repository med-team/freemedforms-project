/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FORM_FORMTREEMODEL_H
#define FORM_FORMTREEMODEL_H

#include <formmanagerplugin/formmanager_exporter.h>

#include <QStandardItemModel>

/**
 * \file ./plugins/formmanagerplugin/formtreemodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Form {
class FormMain;
class SubFormInsertionPoint;
class FormCollection;

namespace Internal {
class FormTreeModelPrivate;
class FormManagerPrivate;
}

class FORM_EXPORT FormTreeModel : public QStandardItemModel
{
    Q_OBJECT
    friend class Form::Internal::FormManagerPrivate;

protected:
    explicit FormTreeModel(const FormCollection &collection, QObject *parent = 0);

public:
    enum DataRepresentation {
        Label = 0,
        Uuid,
        EmptyColumn1,
        EmptyColumn2,
        MaxData
    };

//    explicit FormTreeModel(Form::FormMain *emptyRootForm, QObject *parent = 0);
    ~FormTreeModel();

    void initialize();
    void refreshFormTree();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    Qt::ItemFlags flags(const QModelIndex &index) const;

    // Form information
    const QString &modeUid() const;
    bool isUniqueEpisode(const QModelIndex &index) const;
    bool isNoEpisode(const QModelIndex &index) const;
    bool isMultiEpisode(const QModelIndex &index) const {return (!isUniqueEpisode(index) && !isNoEpisode(index));}
    Form::FormMain *formForIndex(const QModelIndex &index) const;

    // Subform management
    void clearSubForms();
    bool addSubForm(const Form::SubFormInsertionPoint &insertionPoint);
    bool isIncludedRootSubForm(const QModelIndex &index) const;

public Q_SLOTS:
    bool clearFormContents();
    bool updateFormCount(const QModelIndex &index);

private Q_SLOTS:
    bool updateFormCount();
    void onPatientFormsLoaded();

private:
    Internal::FormTreeModelPrivate *d;
};

} // namespace Form

#endif // FORM_FORMTREEMODEL_H
