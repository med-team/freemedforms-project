/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric MAEKER, <eric.maeker@gmail.com>                *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "episodemodel.h"
#include "episodebase.h"
#include "constants_db.h"
#include "constants_settings.h"
#include "episodedata.h"

#include <coreplugin/icore.h>
#include <coreplugin/ipatient.h>
#include <coreplugin/itheme.h>
#include <coreplugin/iuser.h>
#include <coreplugin/constants_menus.h>
#include <coreplugin/constants_tokensandsettings.h>
#include <coreplugin/constants_icons.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/icore.h>
#include <coreplugin/icorelistener.h>
#include <coreplugin/isettings.h>
#include <coreplugin/itheme.h>

#include <formmanagerplugin/formmanager.h>
#include <formmanagerplugin/iformitem.h>
#include <formmanagerplugin/iformitemspec.h>
#include <formmanagerplugin/iformitemdata.h>

#include <utils/log.h>
#include <utils/global.h>
#include <translationutils/constanttranslations.h>
#include <extensionsystem/pluginmanager.h>

#include <QSqlTableModel>

enum { base64MimeDatas = true  };

#ifdef DEBUG
enum {
    WarnDragAndDrop = false,
    WarnReparentItem = false,
    WarnDatabaseSaving = false,
    WarnFormAndEpisodeRetreiving = false,
    WarnLogChronos = false
   };
#else
enum {
    WarnDragAndDrop = false,
    WarnReparentItem = false,
    WarnDatabaseSaving = false,
    WarnFormAndEpisodeRetreiving = false,
    WarnLogChronos = false
   };
#endif



using namespace Form;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Form::Internal::EpisodeBase *episodeBase() {return Form::Internal::EpisodeBase::instance();}

static inline Core::IUser *user() {return Core::ICore::instance()->user();}
static inline Core::IPatient *patient() {return Core::ICore::instance()->patient();}

static inline Core::ISettings *settings()  { return Core::ICore::instance()->settings(); }
static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline Core::ActionManager *actionManager() { return Core::ICore::instance()->actionManager(); }
static inline ExtensionSystem::PluginManager *pluginManager() { return ExtensionSystem::PluginManager::instance(); }

namespace {
    class EpisodeModelTreeItem
    {
    public:
        EpisodeModelTreeItem(EpisodeModelTreeItem *parent = 0) :
                m_Parent(parent),
                m_IsEpisode(false),
                m_IsModified(false)
        {
        }
        ~EpisodeModelTreeItem() { qDeleteAll(m_Children); }

        EpisodeModelTreeItem *child(int number) { return m_Children.value(number); }
        int childCount() const { return m_Children.count(); }
        int columnCount() const { return EpisodeModel::MaxData; }
        EpisodeModelTreeItem *parent() { return m_Parent; }
        void setParent(EpisodeModelTreeItem *parent) { m_Parent = parent; }
        bool appendChild(EpisodeModelTreeItem *child)
        {
            if (!m_Children.contains(child))
                m_Children.append(child);
            return true;
        }
        bool insertChild(const int row, EpisodeModelTreeItem *child)
        {
            if (row > m_Children.count())
                return false;
            m_Children.insert(row, child);
            return true;
        }
        int childNumber() const
        {
            if (m_Parent)
                return m_Parent->m_Children.indexOf(const_cast<EpisodeModelTreeItem*>(this));
            return 0;
        }
        void sortChildren()
        {
            qSort(m_Children.begin(), m_Children.end(), EpisodeModelTreeItem::lessThan);
        }

        int childCategoryCount() const
        {
            int n = 0;
            foreach(EpisodeModelTreeItem *c, this->m_Children) {
                if (!c->isEpisode())
                    ++n;
            }
            return n;
        }

        EpisodeModelTreeItem *categoryChild(int number)
        {
            QList<EpisodeModelTreeItem *> cat;
            foreach(EpisodeModelTreeItem *c, this->m_Children) {
                if (!c->isEpisode())
                    cat << c;
            }
            return cat.value(number);
        }

        int categoryChildNumber() const
        {
            if (m_Parent) {
                QList<EpisodeModelTreeItem *> cat;
                foreach(EpisodeModelTreeItem *c, m_Parent->m_Children) {
                    if (!c->isEpisode())
                        cat << c;
                }
                return cat.indexOf(const_cast<EpisodeModelTreeItem*>(this));
            }
            return 0;
        }

        void setIsEpisode(bool isEpisode) {m_IsEpisode = isEpisode; }
        bool isEpisode() const {return m_IsEpisode;}

        void setModified(bool state)
        {
            m_IsModified = state;
            if (!state)
                m_DirtyRows.clear();
        }
        bool isModified() const {return m_IsModified;}

        bool removeChild(EpisodeModelTreeItem *child)
        {
            if (m_Children.contains(child)) {
                m_Children.removeAll(child);
                return true;
            }
            return false;
        }

        bool removeEpisodes()
        {
            foreach(EpisodeModelTreeItem *item, m_Children) {
                if (item->isEpisode()) {
                    m_Children.removeAll(item);
                    delete item;
                    item = 0;
                }
            }
            return true;
        }

        QVariant data(const int column) const
        {
            Q_UNUSED(column);
            return QVariant();
        }

        bool setData(int column, const QVariant &value)
        {
            Q_UNUSED(column);
            Q_UNUSED(value);
            return true;
        }

        QVector<int> dirtyRows() const
        {
            return m_DirtyRows;
        }

        static bool lessThan(EpisodeModelTreeItem *item1, EpisodeModelTreeItem *item2)
        {
            bool sameType = (((item1->isEpisode()) && (item2->isEpisode())) || ((!item1->isEpisode()) && (!item2->isEpisode())));
            if (sameType)
                return item1->data(EpisodeModel::Label).toString() < item2->data(EpisodeModel::Label).toString();
            return item2->isEpisode();
        }

    private:
        EpisodeModelTreeItem *m_Parent;
        QList<EpisodeModelTreeItem*> m_Children;
        QVector<int> m_DirtyRows;
        bool m_IsEpisode, m_IsModified;
        QHash<int, QVariant> m_Datas;
    };
}

namespace Form {
namespace Internal {

    EpisodeModelCoreListener::EpisodeModelCoreListener(Form::EpisodeModel *parent) :
            Core::ICoreListener(parent)
    {
        Q_ASSERT(parent);
        m_EpisodeModel = parent;
    }
    EpisodeModelCoreListener::~EpisodeModelCoreListener() {}

    bool EpisodeModelCoreListener::coreAboutToClose()
    {
        m_EpisodeModel->submit();
        return true;
    }

    EpisodeModelPatientListener::EpisodeModelPatientListener(Form::EpisodeModel *parent) :
            Core::IPatientListener(parent)
    {
        Q_ASSERT(parent);
        m_EpisodeModel = parent;
    }
    EpisodeModelPatientListener::~EpisodeModelPatientListener() {}

    bool EpisodeModelPatientListener::currentPatientAboutToChange()
    {
        qWarning() << Q_FUNC_INFO;
        m_EpisodeModel->submit();
        return true;
    }




class EpisodeModelPrivate
{
public:
    EpisodeModelPrivate(EpisodeModel *parent) :
        m_RootItem(0),
        m_ShowLastEpisodes(0),
        m_FormTreeCreated(false),
        m_ReadOnly(false),
        m_ActualEpisode(0),
        m_CoreListener(0),
        m_PatientListener(0),
        m_AddLastEpisodeIndex(true),
        m_RecomputeLastEpisodeSynthesis(true),
        q(parent)
    {
    }

    ~EpisodeModelPrivate ()
    {
        qDeleteAll(m_Episodes);
        m_Episodes.clear();
    }

    bool isEpisode(EpisodeModelTreeItem *item) { return (m_EpisodeItems.key(item, 0)!=0); }
    bool isForm(EpisodeModelTreeItem *item) { return (m_FormItems.key(item, 0)!=0); }

    void createFormTree()
    {
        if (m_FormTreeCreated)
            return;

        if (m_RootItem) {
            delete m_RootItem;
            m_RootItem = 0;
            m_ShowLastEpisodes = 0;
        }

        m_RootItem = new EpisodeModelTreeItem(0);
        m_FormUids.clear();

        if (WarnFormAndEpisodeRetreiving)
            LOG_FOR(q, "Getting Forms");

        if (m_AddLastEpisodeIndex) {
            m_ShowLastEpisodes = new EpisodeModelTreeItem(m_RootItem);
            m_RootItem->appendChild(m_ShowLastEpisodes);
        }

        foreach(Form::FormMain *form, m_RootForm->flattenFormMainChildren()) {
            EpisodeModelTreeItem *item = new EpisodeModelTreeItem(0);
            m_FormItems.insert(form, item);
            m_FormUids << form->uuid();
        }
        foreach(Form::FormMain *f, m_RootForm->flattenFormMainChildren()) {
            EpisodeModelTreeItem *it = m_FormItems.value(f);
            if (f->formParent() != m_RootForm) {
                it->setParent(m_FormItems.value(f->formParent()));
                it->parent()->appendChild(it);
            } else {
                it->setParent(m_RootItem);
                m_RootItem->appendChild(it);
            }
            it->setModified(false);
        }
        m_FormTreeCreated = true;
    }

    void deleteEpisodes(EpisodeModelTreeItem *item)
    {
        if (!item)
            return;
        if (isEpisode(item)) {
            item->parent()->removeChild(item);
            foreach(Form::Internal::EpisodeData  *dataChild, m_EpisodeItems.keys(item))
                m_EpisodeItems.remove(dataChild);
            delete item;
            return;
        }
        for(int i = 0; i < item->childCount(); ++i) {
            EpisodeModelTreeItem *child = item->child(i);
            foreach(Form::Internal::EpisodeData  *dataChild, m_EpisodeItems.keys(child))
                m_EpisodeItems.remove(dataChild);
        }
        item->removeEpisodes();
        int nb = item->childCount();
        for(int i = 0; i < nb; ++i) {
            deleteEpisodes(item->child(i));
        }
    }

    void refreshEpisodes()
    {
        QTime chrono;
        if (WarnLogChronos)
            chrono.start();

        if (!saveEpisode(m_ActualEpisode, m_ActualEpisode_FormUid))
            LOG_ERROR_FOR(q, "Unable to save actual episode");

        deleteEpisodes(m_RootItem);
        m_ActualEpisode = 0;
        m_ActualEpisode_FormUid = "";
        qDeleteAll(m_Episodes);
        m_Episodes.clear();
        m_EpisodeItems.clear();

        EpisodeBaseQuery query;
        query.setPatientUid(patient()->uuid());
        query.setValidEpisodes(true);
        query.setDeletedEpisodes(false);
        query.setFormUids(m_FormUids);
        m_Episodes = episodeBase()->getEpisodes(query);
        if (WarnFormAndEpisodeRetreiving)
            LOG_FOR(q, "Getting Episodes (refresh): " + QString::number(m_Episodes.count()));

        for(int i = 0; i < m_Episodes.count(); ++i) {
            EpisodeData *episode = m_Episodes.at(i);
            EpisodeModelTreeItem *formParent = 0;
            foreach(Form::FormMain *form, m_FormItems.keys()) {
                EpisodeModelTreeItem *parent = m_FormItems.value(form);
                if (episode->data(EpisodeData::FormUuid).toString() == form->uuid()) {
                    formParent = parent;
                    break;
                }
            }
            if (!formParent) {
                qWarning() << "no valid formUid" << episode->data(EpisodeData::FormUuid).toString();
                continue;
            }
            EpisodeModelTreeItem *item = new EpisodeModelTreeItem(formParent);
            item->setParent(formParent);
            formParent->appendChild(item);


            m_EpisodeItems.insert(episode, item);
        }
        if (WarnLogChronos)
            Utils::Log::logTimeElapsed(chrono, q->objectName(), "refreshEpisodes()");
    }

    EpisodeModelTreeItem *getItem(const QModelIndex &index) const
    {
        if (index.isValid()) {
            EpisodeModelTreeItem *item = static_cast<EpisodeModelTreeItem*>(index.internalPointer());
            if (item)
                return item;
        }
        return m_RootItem;
    }

    void getEpisodeContent(EpisodeData *episode)
    {
        if (episode->data(EpisodeData::Id).toInt()<0)
            return;
        if (episode->data(EpisodeData::IsXmlContentPopulated).toBool())
            return;
        episodeBase()->getEpisodeContent(episode);
    }

    QString createXmlEpisode(const QString &formUid)
    {
        FormMain *form = m_RootForm->formMainChild(formUid);
        if (!form)
            return QString::null;

        QHash<QString, FormItem *> items;
        foreach(FormItem *it, form->flattenedFormItemChildren()) {
            if (it->itemData()) {
                items.insert(it->uuid(), it);
            }
        }

            QHash<QString, QString> datas;
            foreach(FormItem *it, items) {
                datas.insert(it->uuid(), it->itemData()->storableData().toString());
            }
            return Utils::createXml(Form::Constants::XML_FORM_GENERAL_TAG, datas, 2, false);

    }

    bool saveEpisode(EpisodeModelTreeItem *item, const QString &formUid)
    {
        if (!item)
            return true;
        if (formUid.isEmpty()) {
            LOG_ERROR_FOR("EpisodeModel", "No formUid");
            return false;
        }

        EpisodeData *episode = m_EpisodeItems.key(item);
        FormMain *form = 0;
        foreach(FormMain *f, m_FormItems.keys()) {
            if (f->uuid()==formUid) {
                form = f;
                break;
            }
        }

        if (episode && form) {
            episode->setData(EpisodeData::XmlContent, createXmlEpisode(formUid));
            episode->setData(EpisodeData::IsXmlContentPopulated, true);
            episode->setData(EpisodeData::Label, form->itemData()->data(IFormItemData::ID_EpisodeLabel));
            episode->setData(EpisodeData::UserDate, form->itemData()->data(IFormItemData::ID_EpisodeDateTime));
            LOG_FOR("EpisodeModel", "Save episode: " + episode->data(EpisodeData::Label).toString());
            if (!settings()->value(Core::Constants::S_ALWAYS_SAVE_WITHOUT_PROMPTING, true).toBool()) {
                bool yes = Utils::yesNoMessageBox(QCoreApplication::translate("EpisodeModel", "Save episode?"),
                                                  QCoreApplication::translate("EpisodeModel", "The actual episode has been modified. Do you want to save changes in your database?\n"
                                                     "Answering 'No' will cause definitve data lose."),
                                                  "", QCoreApplication::translate("EpisodeModel", "Save episode"));
                if (!yes) {
                    return false;
                }
            }

            foreach(FormItem *it, form->flattenedFormItemChildren()) {
                if (!it->itemData())
                    continue;
                patient()->setValue(it->patientDataRepresentation(), it->itemData()->data(it->patientDataRepresentation(), IFormItemData::PatientModelRole));
            }

            return episodeBase()->saveEpisode(episode);
        }
        return false;
    }

    void feedFormWithEpisodeContent(Form::FormMain *form, EpisodeModelTreeItem *item, bool feedPatientModel = false)
    {
        EpisodeData *episode = m_EpisodeItems.key(item);
        feedFormWithEpisodeContent(form, episode, feedPatientModel);
    }

    void feedFormWithEpisodeContent(Form::FormMain *form, EpisodeData *episode, bool feedPatientModel = false)
    {
        QTime chrono;
        if (WarnLogChronos)
            chrono.start();

        getEpisodeContent(episode);
        const QString &xml = episode->data(EpisodeData::XmlContent).toString();
        if (xml.isEmpty()) {
            return;
        }

        QHash<QString, QString> datas;
        if (!Utils::readXml(xml, Form::Constants::XML_FORM_GENERAL_TAG, datas, false)) {
            LOG_ERROR_FOR(q, QString("Error while reading EpisodeContent"));
            return;
        }

        QHash<QString, FormItem *> items;
        foreach(FormItem *it, form->flattenedFormItemChildren()) {
            items.insert(it->uuid(), it);
        }

        form->clear();
        form->itemData()->setData(IFormItemData::ID_EpisodeDateTime, episode->data(EpisodeData::UserDate));
        form->itemData()->setData(IFormItemData::ID_EpisodeLabel, episode->data(EpisodeData::Label));
        const QString &username = user()->fullNameOfUser(episode->data(EpisodeData::UserCreatorUuid)); //value(Core::IUser::FullName).toString();
        if (username.isEmpty())
            form->itemData()->setData(IFormItemData::ID_UserName, "No user");
        else
            form->itemData()->setData(IFormItemData::ID_UserName, username);

        foreach(FormItem *it, items.values()) {
            if (!it) {
                qWarning() << "FormManager::activateForm :: ERROR: no item: " << items.key(it);
                continue;
            }
            if (!it->itemData())
                continue;

            it->itemData()->setStorableData(datas.value(it->uuid()));
            if (feedPatientModel) {
                if (it->patientDataRepresentation() >= 0) {
                    if (WarnLogChronos)
                        Utils::Log::logTimeElapsed(chrono, q->objectName(), "feedFormWithEpisodeContent: feed patient model: " + it->uuid());
                    patient()->setValue(it->patientDataRepresentation(), it->itemData()->data(it->patientDataRepresentation(), IFormItemData::PatientModelRole));
                }
            }
        }
        if (WarnLogChronos)
            Utils::Log::logTimeElapsed(chrono, q->objectName(), "feedFormWithEpisodeContent");

    }

    void getLastEpisodes(bool andFeedPatientModel = true)
    {
        qWarning() << "GetLastEpisode (feedPatientModel=" <<andFeedPatientModel << ")";
        if (patient()->uuid().isEmpty())
            return;

        QTime chrono;
        if (WarnLogChronos)
            chrono.start();

        foreach(Form::FormMain *form, m_FormItems.keys()) {

            EpisodeModelTreeItem *formItem = m_FormItems.value(form);
            if (!formItem->childCount()) {
                continue;  // No episodes
            }

            EpisodeData *lastOne = 0;
            for(int i=0; i < m_Episodes.count(); ++i) {
                EpisodeData *episode = m_Episodes.at(i);
                if (episode->data(EpisodeData::FormUuid).toString()==form->uuid()) {
                    if (!lastOne) {
                        lastOne = episode;
                        continue;
                    }
                    if (lastOne->data(EpisodeData::UserDate).toDateTime() < episode->data(EpisodeData::UserDate).toDateTime()) {
                        lastOne = episode;
                    }
                }
            }

            if (lastOne) {
                feedFormWithEpisodeContent(form, lastOne, andFeedPatientModel);
            }
        }
        if (WarnLogChronos)
            Utils::Log::logTimeElapsed(chrono, q->objectName(), "getLastEpisodes");
    }

public:
    FormMain *m_RootForm;
    EpisodeModelTreeItem *m_RootItem, *m_ShowLastEpisodes;
    QString m_UserUuid, m_LkIds, m_CurrentPatient, m_CurrentForm;
    bool m_FormTreeCreated, m_ReadOnly;
    QStringList m_FormUids;

    QMap<Form::FormMain *, EpisodeModelTreeItem *> m_FormItems;
    QMap<Form::Internal::EpisodeData *, EpisodeModelTreeItem *> m_EpisodeItems;
    QList<Form::Internal::EpisodeData *> m_Episodes;

    EpisodeModelTreeItem *m_ActualEpisode;
    QString m_ActualEpisode_FormUid;

    EpisodeModelCoreListener *m_CoreListener;
    EpisodeModelPatientListener *m_PatientListener;

    bool m_AddLastEpisodeIndex, m_RecomputeLastEpisodeSynthesis;


private:
    EpisodeModel *q;
};
}
}

EpisodeModel::EpisodeModel(FormMain *rootEmptyForm, QObject *parent) :
        QAbstractItemModel(parent), d(new Internal::EpisodeModelPrivate(this))
{
    Q_ASSERT(rootEmptyForm);
    setObjectName("EpisodeModel");
    d->m_RootForm = rootEmptyForm;

    d->m_CoreListener = new Internal::EpisodeModelCoreListener(this);
    pluginManager()->addObject(d->m_CoreListener);


    d->m_PatientListener = new Internal::EpisodeModelPatientListener(this);
    pluginManager()->addObject(d->m_PatientListener);


    init();
}

void EpisodeModel::init(bool addLastEpisodeIndex)
{
    d->m_UserUuid = user()->uuid();
    d->m_AddLastEpisodeIndex = addLastEpisodeIndex;
    d->m_CurrentPatient = patient()->uuid();
    d->createFormTree();

    onUserChanged();

    Core::Command * cmd = actionManager()->command(Core::Constants::A_FILE_SAVE);
    connect(cmd->action(), SIGNAL(triggered()), this, SLOT(submit()));

    onPatientChanged();

    connect(Core::ICore::instance(), SIGNAL(databaseServerChanged()), this, SLOT(onCoreDatabaseServerChanged()));
    connect(user(), SIGNAL(userChanged()), this, SLOT(onUserChanged()));
    connect(patient(), SIGNAL(currentPatientChanged()), this, SLOT(onPatientChanged()));
}

void EpisodeModel::refreshFormTree()
{
    d->m_FormTreeCreated = false;
    d->createFormTree();
    d->refreshEpisodes();
    d->getLastEpisodes(true);
    reset();
}

EpisodeModel::~EpisodeModel()
{
    if (d->m_CoreListener) {
        pluginManager()->removeObject(d->m_CoreListener);
    }
    if (d->m_PatientListener) {
        pluginManager()->removeObject(d->m_PatientListener);
    }
    if (d) {
        delete d;
        d=0;
    }
}

void EpisodeModel::onCoreDatabaseServerChanged()
{
    d->m_FormTreeCreated = false;
    d->createFormTree();
    d->refreshEpisodes();
    d->getLastEpisodes(true);
    reset();
}

void EpisodeModel::onUserChanged()
{
    d->m_UserUuid = user()->uuid();
}

void EpisodeModel::onPatientChanged()
{
    d->m_CurrentPatient = patient()->uuid();
    d->refreshEpisodes();

    d->getLastEpisodes(true);
    d->m_RecomputeLastEpisodeSynthesis = false;
    reset();
}

QModelIndex EpisodeModel::index(int row, int column, const QModelIndex &parent) const
 {
     if (parent.isValid() && parent.column() != 0)
         return QModelIndex();


     EpisodeModelTreeItem *parentItem = d->getItem(parent);
     EpisodeModelTreeItem *childItem = 0;
     childItem = parentItem->child(row);
     if (childItem) { // && childItem != d->m_RootItem) {
         return createIndex(row, column, childItem);
     }
     return QModelIndex();
 }

QModelIndex EpisodeModel::parent(const QModelIndex &index) const
 {
     if (!index.isValid())
         return QModelIndex();

     EpisodeModelTreeItem *childItem = d->getItem(index);
     EpisodeModelTreeItem *parentItem = childItem->parent();

     if (parentItem == d->m_RootItem)
         return QModelIndex();

     return createIndex(parentItem->childNumber(), 0, parentItem);
 }

int EpisodeModel::rowCount(const QModelIndex &parent) const
{
    EpisodeModelTreeItem *item = d->getItem(parent);
    if (item) {
        return item->childCount();
    }
    return 0;
}

int EpisodeModel::columnCount(const QModelIndex &) const
{
    return MaxData;
}

QVariant EpisodeModel::data(const QModelIndex &item, int role) const
{
    if (!item.isValid())
        return QVariant();

    if (item.column() == EmptyColumn1)
        return QVariant();
    if (item.column() == EmptyColumn2)
        return QVariant();

    EpisodeModelTreeItem *it = d->getItem(item);
    if (it==d->m_RootItem) {
        return QVariant();
    }

    if (it==d->m_ShowLastEpisodes) {
        switch (role) {
        case Qt::DisplayRole:
        case Qt::EditRole:
            if (item.column() == FormUuid)
                return Constants::PATIENTLASTEPISODES_UUID;
            if (item.column() == Label)
                return QApplication::translate(Constants::FORM_TR_CONTEXT, Constants::SHOWPATIENTLASTEPISODES_TEXT);
            break;
        case Qt::FontRole:
        {
            QFont f;
            f.fromString(settings()->value(Constants::S_EPISODEMODEL_FORM_FONT).toString());
            return f;
        }
        case Qt::DecorationRole:
            return theme()->icon(Core::Constants::ICONPATIENTSYNTHESIS);
        case Qt::ForegroundRole:
            return QColor(settings()->value(Constants::S_EPISODEMODEL_FORM_FOREGROUND).toString());
        }
        return QVariant();
    }

    EpisodeData *episode = d->m_EpisodeItems.key(it, 0);
    FormMain *form = d->m_FormItems.key(it, 0);

    switch (role)
    {
    case Qt::EditRole :
    case Qt::DisplayRole :
    {
        switch (item.column()) {
        case Label:
            if (episode) {
                QHash<QString, QString> tokens;
                tokens.insert(Constants::T_LABEL, episode->data(EpisodeData::Label).toString());
                tokens.insert(Constants::T_SMALLDATE, QLocale().toString(episode->data(EpisodeData::UserDate).toDate(), settings()->value(Constants::S_EPISODEMODEL_SHORTDATEFORMAT, tkTr(Trans::Constants::DATEFORMAT_FOR_MODEL)).toString()));
                tokens.insert(Constants::T_FULLDATE, QLocale().toString(episode->data(EpisodeData::UserDate).toDate(), settings()->value(Constants::S_EPISODEMODEL_SHORTDATEFORMAT, tkTr(Trans::Constants::DATEFORMAT_FOR_MODEL)).toString()));
                QString s = settings()->value(Constants::S_EPISODELABELCONTENT).toString();
                Utils::replaceTokens(s, tokens);
                return s;
            }
            if (form)
                return form->spec()->label();
            break;
        case IsValid:
            if (episode)
                return episode->data(EpisodeData::IsValid);
            if (form)
                return true;
            break;
        case  FormUuid:
            if (episode)
                return episode->data(EpisodeData::FormUuid);
            if (form)
                return form->uuid();
            break;
        case IsEpisode:
            return (episode!=0);
        case XmlContent:
            if (episode) {
                if (!episode->data(EpisodeData::IsXmlContentPopulated).toBool())
                    d->getEpisodeContent(episode);
                return episode->data(EpisodeData::XmlContent);
            }
            break;
        }
        break;
    }
    case Qt::ToolTipRole :
    {
        switch (item.column()) {
        case Label:
            if (episode)
                return QString("<p align=\"right\">%1&nbsp;-&nbsp;%2<br /><span style=\"color:gray;font-size:9pt\">%3</span></p>")
                        .arg(QLocale().toString(episode->data(EpisodeData::UserDate).toDate(), settings()->value(Constants::S_EPISODEMODEL_SHORTDATEFORMAT, tkTr(Trans::Constants::DATEFORMAT_FOR_MODEL)).toString()).replace(" ", "&nbsp;"))
                        .arg(episode->data(EpisodeData::Label).toString().replace(" ", "&nbsp;"))
                        .arg(user()->fullNameOfUser(episode->data(EpisodeData::UserCreatorUuid).toString()) + "<br/>" +
                             tr("Created: ") +  QLocale().toString(episode->data(EpisodeData::CreationDate).toDate(), settings()->value(Constants::S_EPISODEMODEL_LONGDATEFORMAT, tkTr(Trans::Constants::DATEFORMAT_FOR_MODEL)).toString()));
            if (form)
                return form->spec()->label();
            break;
        }
        break;
    }
    case Qt::ForegroundRole :
    {
        if (episode) {
            return QColor(settings()->value(Constants::S_EPISODEMODEL_EPISODE_FOREGROUND, "darkblue").toString());
        } else if (form) {
            if (it->parent() == d->m_RootItem) {
                if (settings()->value(Constants::S_USESPECIFICCOLORFORROOTS).toBool()) {
                    return QColor(settings()->value(Constants::S_FOREGROUNDCOLORFORROOTS, "darkblue").toString());
                }
            }
            return QColor(settings()->value(Constants::S_EPISODEMODEL_FORM_FOREGROUND, "#000").toString());
        }
        break;
    }
    case Qt::FontRole :
    {
        if (form) {
            QFont f;
            f.fromString(settings()->value(Constants::S_EPISODEMODEL_FORM_FONT).toString());
            return f;
        } else {
            QFont f;
            f.fromString(settings()->value(Constants::S_EPISODEMODEL_EPISODE_FONT).toString());
            return f;
        }
        return QFont();
    }
    case Qt::DecorationRole :
    {
        if (form) {
            QString icon = form->spec()->value(FormItemSpec::Spec_IconFileName).toString();
            icon.replace(Core::Constants::TAG_APPLICATION_THEME_PATH, settings()->path(Core::ISettings::SmallPixmapPath));
            if (QFileInfo(icon).isRelative())
                icon.append(qApp->applicationDirPath());
            return QIcon(icon);
        }
        break;
    }
    }
    return QVariant();
}

bool EpisodeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (d->m_ReadOnly)
        return false;

    if (!index.isValid())
        return false;

    EpisodeModelTreeItem *it = d->getItem(index);
    if (it==d->m_RootItem)
        return false;

    EpisodeData *episode = d->m_EpisodeItems.key(it, 0);

    if ((role==Qt::EditRole) || (role==Qt::DisplayRole)) {
        if (episode) {
            switch (index.column()) {
            case Label: episode->setData(EpisodeData::Label, value); break;
            case Date: episode->setData(EpisodeData::UserDate, value); break;
            case IsValid: episode->setData(EpisodeData::IsValid, value); break;
            case FormUuid: episode->setData(EpisodeData::FormUuid, value); break;
            case XmlContent: episode->setData(EpisodeData::XmlContent, value); episode->setData(EpisodeData::IsXmlContentPopulated, value); break;
            }
        }

        Q_EMIT dataChanged(index, index);
    }
    return true;
}

Qt::ItemFlags EpisodeModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index);
    return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

QVariant EpisodeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(section);
    Q_UNUSED(orientation);
    Q_UNUSED(role);
    return QVariant();
}

bool EpisodeModel::insertRows(int row, int count, const QModelIndex &parent)
{
    if (d->m_ReadOnly)
        return false;

    if (!parent.isValid())
        return false;

    EpisodeModelTreeItem *parentItem = d->getItem(parent);
    if (!parentItem)
        return false;

    FormMain *form = formForIndex(parent);
    if (!form)
        return false;
    const QString &formUid = form->uuid();

    beginInsertRows(parent, row, row + count);
    for(int i = 0; i < count; ++i) {
        Internal::EpisodeData *episode = new Internal::EpisodeData;
        episode->setData(Internal::EpisodeData::Label, tr("New episode"));
        episode->setData(Internal::EpisodeData::FormUuid, formUid);
        episode->setData(Internal::EpisodeData::UserCreatorUuid, user()->uuid());
        episode->setData(Internal::EpisodeData::PatientUuid, patient()->uuid());
        episode->setData(Internal::EpisodeData::CreationDate, QDateTime::currentDateTime());
        episode->setData(Internal::EpisodeData::UserDate, QDateTime::currentDateTime());
        episode->setData(Internal::EpisodeData::IsValid, true);

        EpisodeModelTreeItem *it = new EpisodeModelTreeItem(parentItem);
        parentItem->insertChild(row+i, it);

        d->m_EpisodeItems.insert(episode, it);
        d->m_Episodes.append(episode);
    }
    endInsertRows();
    return true;
}

bool EpisodeModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(row);
    Q_UNUSED(count);
    Q_UNUSED(parent);
    if (d->m_ReadOnly)
        return false;

    return true;
}

bool EpisodeModel::isEpisode(const QModelIndex &index) const
{
    if (!index.isValid())
        return false;

    EpisodeModelTreeItem *it = d->getItem(index);
    if (it==d->m_RootItem)
        return false;

    EpisodeData *episode = d->m_EpisodeItems.key(it, 0);

    return (episode);
}

bool EpisodeModel::isUniqueEpisode(const QModelIndex &index) const
{
    if (!index.isValid())
        return false;

    EpisodeModelTreeItem *it = d->getItem(index);
    if (it==d->m_RootItem)
        return false;

    FormMain *form = d->m_FormItems.key(it, 0);
    if (form)
        return form->episodePossibilities()==FormMain::UniqueEpisode;

    return false;
}

bool EpisodeModel::isNoEpisode(const QModelIndex &index)
{
    if (!index.isValid())
        return false;
    EpisodeModelTreeItem *it = d->getItem(index);
    if (it==d->m_RootItem)
        return false;

    FormMain *form = d->m_FormItems.key(it, 0);
    if (form)
        return form->episodePossibilities()==FormMain::NoEpisode;

    return false;
}

void EpisodeModel::setReadOnly(const bool state)
{
    d->m_ReadOnly = state;
}

bool EpisodeModel::isReadOnly() const
{
    return d->m_ReadOnly;
}

bool EpisodeModel::isDirty() const
{
    return false;
}

bool EpisodeModel::isLastEpisodeIndex(const QModelIndex &index) const
{
    if (!index.isValid())
        return false;
    EpisodeModelTreeItem *it = d->getItem(index);
    if (it==d->m_ShowLastEpisodes)
        return true;
    return false;
}

Form::FormMain *EpisodeModel::formForIndex(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;
    QModelIndex idx = index;
    while (idx.isValid()) {
        EpisodeModelTreeItem *it = d->getItem(idx);
        if (it==d->m_RootItem)
            return 0;
        FormMain *form = d->m_FormItems.key(it, 0);
        if (form)
            return form;
        idx = idx.parent();
    }
    return 0;
}

static QModelIndex formIndex(const QString &formUid, const QModelIndex &parent, const Form::EpisodeModel *model)
{
    if (model->isForm(parent)) {
        if (parent.data().toString()==formUid) {
            return model->index(parent.row(), 0, parent.parent());
        }
    }
    for(int i = 0; i < model->rowCount(parent); ++i) {
        QModelIndex item = model->index(i, EpisodeModel::FormUuid, parent);
        QModelIndex ret = formIndex(formUid, item, model);
        if (ret.isValid())
            return model->index(ret.row(), 0, ret.parent());
    }
    return QModelIndex();
}

QModelIndex EpisodeModel::indexForForm(const QString &formUid) const
{
    for(int i = 0; i < rowCount(); ++i) {
        QModelIndex ret = formIndex(formUid, index(i, EpisodeModel::FormUuid), this);
        if (ret.isValid()) {
            return ret;
        }
    }
    return QModelIndex();
}

bool EpisodeModel::submit()
{
    if (patient()->uuid().isEmpty())
        return false;

    if (d->m_ActualEpisode) {
        if (!d->saveEpisode(d->m_ActualEpisode, d->m_ActualEpisode_FormUid)) {
            LOG_ERROR("Unable to save actual episode before editing a new one");
        }
    }
    return true;
}

bool EpisodeModel::activateEpisode(const QModelIndex &index, const QString &formUid) //, const QString &xmlcontent)
{
    qWarning() << "activateEpisode" << formUid;
    if (!d->saveEpisode(d->m_ActualEpisode, d->m_ActualEpisode_FormUid)) {
        LOG_ERROR("Unable to save actual episode before editing a new one");
    }

    d->m_RecomputeLastEpisodeSynthesis = true;

    if (!index.isValid()) {
        d->m_ActualEpisode = 0;
        return false;
    }

    EpisodeModelTreeItem *it = d->getItem(index);
    if (it==d->m_RootItem)
        return false;

    EpisodeData *episode = d->m_EpisodeItems.key(it, 0);
    FormMain *form = formForIndex(index);
    if (!episode) {
        d->m_ActualEpisode = 0;
        return false;
    }
    d->m_ActualEpisode = it;
    d->m_ActualEpisode_FormUid = formUid;

    if (!form)
        return false;
    form->clear();
    form->itemData()->setData(IFormItemData::ID_EpisodeDateTime, episode->data(EpisodeData::UserDate));
    form->itemData()->setData(IFormItemData::ID_EpisodeLabel, episode->data(EpisodeData::Label));
    const QString &username = user()->fullNameOfUser(episode->data(EpisodeData::UserCreatorUuid)); //value(Core::IUser::FullName).toString();
    if (username.isEmpty())
        form->itemData()->setData(IFormItemData::ID_UserName, tr("No user"));
    else
        form->itemData()->setData(IFormItemData::ID_UserName, username);

    d->getEpisodeContent(episode);
    const QString &xml = episode->data(EpisodeData::XmlContent).toString();
    if (xml.isEmpty())
        return true;

    QHash<QString, QString> datas;
    if (!Utils::readXml(xml, Form::Constants::XML_FORM_GENERAL_TAG, datas, false)) {
        LOG_ERROR(QString("Error while reading EpisodeContent %2:%1").arg(__LINE__).arg(__FILE__));
        return false;
    }

    QHash<QString, FormItem *> items;
    foreach(FormItem *it, form->flattenedFormItemChildren()) {
        items.insert(it->uuid(), it);
    }


    foreach(const QString &s, datas.keys()) {
        FormItem *it = items.value(s, 0);
        if (!it) {
            qWarning() << "FormManager::activateForm :: ERROR: no item: " << s;
            continue;
        }
        if (it->itemData())
            it->itemData()->setStorableData(datas.value(s));
        else
            qWarning() << "FormManager::activateForm :: ERROR: no itemData: " << s;
    }
    return true;
}

bool EpisodeModel::saveEpisode(const QModelIndex &index, const QString &formUid)
{
    return d->saveEpisode(d->getItem(index), formUid);
}

QString EpisodeModel::lastEpisodesSynthesis() const
{
    QTime chrono;
    if (WarnLogChronos)
        chrono.start();

    if (d->m_RecomputeLastEpisodeSynthesis) {
        if (!d->saveEpisode(d->m_ActualEpisode, d->m_ActualEpisode_FormUid)) {
            LOG_ERROR("Unable to save actual episode before editing a new one");
        }
        d->m_ActualEpisode = 0;
        d->m_ActualEpisode_FormUid.clear();

        d->getLastEpisodes(false);
    }
    if (WarnLogChronos)
        Utils::Log::logTimeElapsed(chrono, objectName(), "Compute last episode Part 1");

    QString html;
    foreach(FormMain *f, d->m_RootForm->firstLevelFormMainChildren()) {
        if (!f) {
            LOG_ERROR("??");
            continue;
        }
        html += f->printableHtml();
    }

    if (WarnLogChronos)
        Utils::Log::logTimeElapsed(chrono, objectName(), "Compute last episode Part 2 (getting html code)");

    return html;
}

