/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FORM_INTERNAL_FORMDATAWIDGETMAPPER_H
#define FORM_INTERNAL_FORMDATAWIDGETMAPPER_H

#include <formmanagerplugin/formmanager_exporter.h>
#include <QWidget>
#include <QPixmap>
QT_BEGIN_NAMESPACE
class QModelIndex;
QT_END_NAMESPACE

/**
 * \file ./plugins/formmanagerplugin/formdatawidgetmapper.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Form {
class FormMain;
namespace Internal {
class FormDataWidgetMapperPrivate;
} // namespace Internal

class FORM_EXPORT FormDataWidgetMapper : public QWidget
{
    Q_OBJECT
public:
    explicit FormDataWidgetMapper(QWidget *parent = 0);
    ~FormDataWidgetMapper();
    bool initialize();
    void clear();
    bool isDirty() const;
    QModelIndex currentEditingEpisodeIndex() const;
    QString currentEpisodeLabel() const;
    QString currentFormName() const;

public Q_SLOTS:
    void setCurrentForm(const QString &formUid);
    void setCurrentForm(FormMain *form);
    void setLastEpisodeAsCurrent();
    void setCurrentEpisode(const QModelIndex &index);
    void setFormWidgetEnabled(bool enabled);

    QPixmap screenshot();

    bool submit();

private:
    Internal::FormDataWidgetMapperPrivate *d;
};

} // namespace Form

#endif // FORM_INTERNAL_FORMDATAWIDGETMAPPER_H
