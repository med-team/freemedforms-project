/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FORM_MANAGER_IPLUGIN_H
#define FORM_MANAGER_IPLUGIN_H

#include <extensionsystem/iplugin.h>

#include <QtCore/QObject>

/**
 * \file ./plugins/formmanagerplugin/formmanagerplugin.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Form {
class FormCore;
namespace Internal {
class FormManagerMode;
class FirstRunFormManagerConfigPage;
class FormPreferencesPage;
class FormPreferencesFileSelectorPage;

class FormManagerPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.freemedforms.FreeMedForms.FormManagerPlugin" FILE "FormManager.json")

public:
    FormManagerPlugin();
    ~FormManagerPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

private Q_SLOTS:
    void postCoreInitialization();

#ifdef WITH_TESTS
    void initTestCase();
    void test_formcore_initialization();
    void test_FormManager_initialization();
    void test_FormIOQuery();
    void test_FormIO_queryFromDatabase();
    void test_FormIO_queryFromLocal();
    void test_FormIO_screenshots();
    void test_FormIO_userForms();
    void cleanupTestCase();
#endif

private:
    FormCore *_core;
    FormManagerMode *_mode;
    Internal::FirstRunFormManagerConfigPage *m_FirstRun;
    Internal::FormPreferencesFileSelectorPage *m_PrefPageSelector;
    Internal::FormPreferencesPage *m_PrefPage;
};

}
}

#endif  // End FORM_MANAGER_IPLUGIN_H
