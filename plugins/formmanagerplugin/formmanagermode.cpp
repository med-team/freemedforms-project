/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "formmanagermode.h"
#include "formcore.h"
#include "formplaceholder.h"
#include "iformitem.h"
#include "iformio.h"
#include "formmanager.h"
#include "constants_db.h"

#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/imainwindow.h>
#include <coreplugin/constants_menus.h>
#include <coreplugin/constants_icons.h>
#include <coreplugin/modemanager/modemanager.h>
#include <coreplugin/actionmanager/actionmanager.h>

#include <extensionsystem/pluginmanager.h>

#include <QWidget>
#include <QGridLayout>
#include <QLabel>

using namespace Form;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline ExtensionSystem::PluginManager *pluginManager() { return ExtensionSystem::PluginManager::instance(); }
static inline Form::FormManager &formManager() {return Form::FormCore::instance().formManager();}
static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }


FormManagerMode::FormManagerMode(QObject *parent) :
    Core::IMode(parent),
    m_inPluginManager(false),
    m_actionInBar(false)
{
    setDisplayName(tkTr(Trans::Constants::PATIENT));
    setIcon(theme()->icon(Core::Constants::ICONPATIENTFILES, Core::ITheme::BigIcon));
    setPriority(Core::Constants::P_MODE_PATIENT_FILE);
    setId(Core::Constants::MODE_PATIENT_FILE);
    setPatientBarVisibility(true);
    setEnabledOnlyWithCurrentPatient(true);

    m_Holder = new FormPlaceHolder;
    m_Holder->setObjectName("EpisodesFormPlaceHolder");

    setWidget(m_Holder);
    onPatientFormsLoaded();
    connect(&formManager(), SIGNAL(patientFormsLoaded()), this, SLOT(onPatientFormsLoaded()));
}

FormManagerMode::~FormManagerMode()
{
    if (m_inPluginManager) {
        pluginManager()->removeObject(this);
    }
}

QString FormManagerMode::name() const
{
    return tkTr(Trans::Constants::PATIENT);
}


bool FormManagerMode::onPatientFormsLoaded()
{
    if (!m_inPluginManager) {
        pluginManager()->addObject(this);
        m_inPluginManager = true;
    }
    Form::FormTreeModel *model = formManager().formTreeModelForMode(Core::Constants::MODE_PATIENT_FILE);
    m_Holder->setFormTreeModel(model);
    return (model);
}
