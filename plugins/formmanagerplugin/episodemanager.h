/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric MAEKER
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FORM_INTERNAL_EPISODEMANAGER_H
#define FORM_INTERNAL_EPISODEMANAGER_H

#include <formmanagerplugin/formmanager_exporter.h>
#include <QObject>

/**
 * \file ./plugins/formmanagerplugin/episodemanager.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Form {
class EpisodeModel;
class FormMain;
class FormCore;

namespace Internal {
class EpisodeManagerPrivate;
} // namespace Internal

class FORM_EXPORT EpisodeManager : public QObject
{
    Q_OBJECT
    friend class Form::FormCore;

protected:
    explicit EpisodeManager(QObject *parent = 0);
    bool initialize();

public:
    ~EpisodeManager();
    bool isInitialized() const;

    EpisodeModel *episodeModel(Form::FormMain *form);
    EpisodeModel *episodeModel(const QString &formUid);

private:
    Internal::EpisodeManagerPrivate *d;
};

} // namespace Form

#endif // FORM_INTERNAL_EPISODEMANAGER_H

