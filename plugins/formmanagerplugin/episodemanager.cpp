/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
/*!
 * \class Form::Internal::EpisodeManager
 * Manages all editing Form::EpisodeModel. The models are created with the editing form collection
 * not the duplicate collection.
 * \sa Form::EpisodeModel, Form::FormManager
 */

#include "episodemanager.h"
#include "episodemodel.h"
#include "formcore.h"
#include "formmanager.h"

#include <translationutils/constants.h>

#include <QDebug>

using namespace Form;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Form::FormManager &formManager() {return Form::FormCore::instance().formManager();}

namespace Form {
namespace Internal {
class EpisodeManagerPrivate
{
public:
    EpisodeManagerPrivate(EpisodeManager *parent) :
        _initialized(false),
        q(parent)
    {
        Q_UNUSED(q);
    }

    ~EpisodeManagerPrivate()
    {
    }

public:
    bool _initialized;
    QHash<Form::FormMain *, EpisodeModel *> _episodeModels;

private:
    EpisodeManager *q;
};
}  // namespace Internal
} // end namespace Form

/*! Constructor of the Form::Internal::EpisodeManager class */
EpisodeManager::EpisodeManager(QObject *parent) :
    QObject(parent),
    d(new EpisodeManagerPrivate(this))
{
}

/*! Destructor of the Form::Internal::EpisodeManager class */
EpisodeManager::~EpisodeManager()
{
    if (d)
        delete d;
    d = 0;
}

/*! Initializes the object with the default values. Return true if initialization was completed. */
bool EpisodeManager::initialize()
{
    d->_initialized = true;
    return true;
}

bool EpisodeManager::isInitialized() const
{
    return d->_initialized;
}


EpisodeModel *EpisodeManager::episodeModel(Form::FormMain *form)
{
    if (!form)
        return 0;

    if (!d->_episodeModels.value(form, 0)) {
        EpisodeModel *model = new EpisodeModel(form, this);
        model->initialize();
        d->_episodeModels.insert(form, model);
        return model;
    }
    return d->_episodeModels.value(form);
}


EpisodeModel *EpisodeManager::episodeModel(const QString &formUid)
{
    return episodeModel(formManager().form(formUid));
}

