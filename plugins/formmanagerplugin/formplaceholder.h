/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FORMPLACEHOLDER_H
#define FORMPLACEHOLDER_H

#include <formmanagerplugin/formmanager_exporter.h>
#include <formmanagerplugin/formcontextualwidget.h>
#include <coreplugin/icorelistener.h>
#include <coreplugin/ipatientlistener.h>

#include <QWidget>
#include <QTreeView>

QT_BEGIN_NAMESPACE
class  QTreeWidgetItem;
class QStackedLayout;
class QModelIndex;
QT_END_NAMESPACE

/**
 * \file ./plugins/formmanagerplugin/formplaceholder.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Form {
class EpisodeModel;
class FormMain;
class FormPlaceHolder;
class FormTreeModel;
class FormManager;

namespace Internal {
class FormPlaceHolderPrivate;

class FormPlaceHolderCoreListener : public Core::ICoreListener
{
    Q_OBJECT
public:
    FormPlaceHolderCoreListener(FormPlaceHolder *parent);
    ~FormPlaceHolderCoreListener();
    bool coreAboutToClose();
    QString errorMessage() const {return _errorMsg;}

private:
    FormPlaceHolder *_formPlaceHolder;
    QString _errorMsg;
};

class FormPlaceHolderPatientListener : public Core::IPatientListener
{
    Q_OBJECT
public:
    FormPlaceHolderPatientListener(FormPlaceHolder *parent);
    ~FormPlaceHolderPatientListener();
    bool currentPatientAboutToChange();
    QString errorMessage() const {return _errorMsg;}

private:
    FormPlaceHolder *_formPlaceHolder;
    QString _errorMsg;
};

}  // End namespace Internal


class FORM_EXPORT FormPlaceHolder : public Internal::FormContextualWidget
{
    Q_OBJECT
    friend class BaseFormData;
    friend class Form::FormManager;
    friend class Form::Internal::FormPlaceHolderPrivate;
    friend class Form::Internal::FormPlaceHolderCoreListener;
    friend class Form::Internal::FormPlaceHolderPatientListener;

public:
    FormPlaceHolder(QWidget *parent = 0);
    ~FormPlaceHolder();

    bool enableAction(WidgetAction action) const;
    void setFormTreeModel(FormTreeModel *model);
    QString currentFormLabel() const;

public Q_SLOTS:
    bool clear();

protected Q_SLOTS:
    void currentSelectedFormChanged(const QModelIndex &current, const QModelIndex &previous);
    void setCurrentEditingFormItem(const QModelIndex &index);
    bool createEpisode();
    bool validateCurrentEpisode();
    bool renewEpisode();
    bool saveCurrentEpisode();
    bool removeCurrentEpisode();
    bool takeScreenshotOfCurrentEpisode();
    bool addForm();
    bool removeSubForm();
    bool printFormOrEpisode();
    void episodeChanged(const QModelIndex &current, const QModelIndex &previous);

protected:
    bool isDirty() const;

private Q_SLOTS:
    void onFormTreeModelReset();
    void saveSortOrderToSettings(int col, Qt::SortOrder sort);
    void onCurrentPatientChanged();
    void handlePressed(const QModelIndex &index);
    void handleClicked(const QModelIndex &index);
    void updateFormCount();

private:
    void changeEvent(QEvent *event);
    void hideEvent(QHideEvent *event);
    void showEvent(QShowEvent *event);

private:
    Internal::FormPlaceHolderPrivate *d;
};

}  // End namespace Form

#endif // FORMPLACEHOLDER_H
