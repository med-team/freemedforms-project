/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FIRSTRUNFORMMANAGER_H
#define FIRSTRUNFORMMANAGER_H

#include <coreplugin/ifirstconfigurationpage.h>

#include <QWizardPage>

/**
 * \file ./plugins/formmanagerplugin/firstrunformmanager.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Form {
class FormFilesSelectorWidget;

namespace Internal {

class FirstRunFormManagerWizardPage : public QWizardPage
{
    Q_OBJECT
public:
    FirstRunFormManagerWizardPage(QWidget *parent);

    void initializePage();
    bool validatePage();
    int nextId() const;

private:
    void changeEvent(QEvent *e);
    void retranslate();

private:
    FormFilesSelectorWidget *selector;
};

class FirstRunFormManagerConfigPage : public Core::IFirstConfigurationPage
{
public:
    FirstRunFormManagerConfigPage(QObject *parent) : IFirstConfigurationPage(parent) {}
    int id() const {return Core::IFirstConfigurationPage::PatientForm;}
    QWizardPage *createPage(QWidget *parent) {return new FirstRunFormManagerWizardPage(parent);}
};


}  // End namespace Internal
}  // End namespace Form

#endif // FIRSTRUNFORMMANAGER_H
