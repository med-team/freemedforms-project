/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker                                         *
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FORM_INTERNAL_FORMITEMTOKEN_H
#define FORM_INTERNAL_FORMITEMTOKEN_H

#include <coreplugin/ipadtools.h>
#include <formmanagerplugin/iformitem.h>

/**
 * \file ./plugins/formmanagerplugin/formitemtoken.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Form {
namespace Internal {
class FormItemTokenPrivate;

class FormItemToken : public Core::IToken
{
public:
    enum ValueType {
        FormItemLabel = 0,
        FormItemTooltip,
        FormItemPatientModelValue,
        FormItemPrintValue,
        FormItemDataValue
    };
    static bool canManageValueType(FormItem *item, ValueType type);

    explicit FormItemToken(Form::FormItem *item, ValueType type);
    ~FormItemToken();
    bool initialize();
    
    QString uid() const;
    QString humanReadableName() const;
    QString tooltip() const;
    QString helpText() const;
    QString shortHtmlDescription() const;

    virtual QVariant testValue() const;
    virtual QVariant value() const;

private:
    FormItemTokenPrivate *d;
};

} // namespace Internal
} // namespace Form

#endif // FORM_INTERNAL_FORMITEMTOKEN_H

