/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric MAEKER
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FORM_FORMCOLLECTION_H
#define FORM_FORMCOLLECTION_H

#include <formmanagerplugin/formmanager_exporter.h>
#include <QObject>

/**
 * \file ./plugins/formmanagerplugin/formcollection.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Form {
class FormManager;
class FormMain;
namespace Internal {
class FormCollectionPrivate;
class FormManagerPrivate;
}  // namespace Internal

class FORM_EXPORT FormCollection 
{
    friend class Form::FormManager;
    friend class Form::Internal::FormManagerPrivate;

public:
    enum CollectionType {
        CompleteForm,
        SubForm
    };

protected:
    explicit FormCollection();//(QObject *parent = 0);
    bool initialize();
    void setDuplicates(bool isDuplicates);
    void setType(CollectionType type);
    void setEmptyRootForms(const QList<Form::FormMain *> &emptyRootForms);
    void addEmptyRootForm(Form::FormMain * emptyRootForm);

public:
    ~FormCollection();

    bool isNull() const;
    bool isDuplicates() const;
    QString formUid() const;
    QString modeUid() const;
    CollectionType type() const;
    const QList<Form::FormMain *> &emptyRootForms() const;

    bool containsFormUid(const QString &formUid) const;
    bool containsIdentityForm() const;
    Form::FormMain *identityForm() const;
    Form::FormMain *form(const QString &formUid) const;

private:
    Internal::FormCollectionPrivate *d;
};

} // namespace Form

#endif  // FORM_FORMCOLLECTION_H

