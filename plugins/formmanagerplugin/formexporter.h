/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FORM_FORMEXPORTER_H
#define FORM_FORMEXPORTER_H

#include <coreplugin/ipatientdataexporter.h>
#include <QObject>
#include <QString>
#include <QStringList>

/**
 * \file ./plugins/formmanagerplugin/formexporter.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Form {
namespace Internal {
class FormExporterPrivate;

class FormExporter : public Core::IPatientDataExporter
{
    Q_OBJECT
    friend class Form::Internal::FormExporterPrivate;

public:
    explicit FormExporter(bool identityOnly, QObject *parent = 0);
    ~FormExporter();
    bool initialize();

    void setIdentityOnly(bool identityOnly);

    bool isBusy() const;
    
public Q_SLOTS:
    Core::PatientDataExtraction *startExportationJob(const Core::PatientDataExporterJob &job);

private:
    Internal::FormExporterPrivate *d;
};

} // namespace Internal
} // namespace Form

#endif  // FORM_FORMEXPORTER_H

