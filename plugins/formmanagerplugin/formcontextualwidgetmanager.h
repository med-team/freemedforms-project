/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER <eric.maeker@gmail.com>                             *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FORM_INTERNAL_FORMCONTEXTUALWIDGETMANAGER_H
#define FORM_INTERNAL_FORMCONTEXTUALWIDGETMANAGER_H

#include <coreplugin/contextmanager/icontext.h>
#include <formmanagerplugin/formcontextualwidget.h>

#include <QObject>
#include <QPointer>

/**
 * \file ./plugins/formmanagerplugin/formcontextualwidgetmanager.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class IContext;
}

namespace Form {
namespace Internal {

class FormActionHandler : public QObject
{
    Q_OBJECT
public:
    FormActionHandler(QObject *parent = 0);
    virtual ~FormActionHandler() {}

    void setCurrentView(FormContextualWidget *view);

private Q_SLOTS:
    void onClearRequested();
    void onSaveEpisodeRequested();
    void onCreateEpisodeRequested();
    void onValidateEpisodeRequested();
    void onRenewEpisodeRequested();
    void onRemoveEpisodeRequested();
    void onTakeScreenshotRequested();
    void onAddFormRequested();
    void onRemoveFormRequested();
    void onPrintFormRequested();

    void showDatabaseInformation();

private Q_SLOTS:
    void updateActions();
    void onActionEnabledStateUpdated(Form::Internal::FormContextualWidget::WidgetAction action);

protected:
    QAction *aClear;
    QAction *aShowDatabaseInformation;
    QAction *aCreateEpisode, *aValidateEpisode, *aRenewEpisode, *aRemoveEpisode, *aSaveEpisode;
    QAction *aTakeScreenshot;
    QAction *aAddForm;
    QAction *aRemoveSubForm;
    QAction *aPrintForm;

    QPointer<FormContextualWidget> m_CurrentView;
};

class FormContextualWidgetManager : public FormActionHandler
{
    Q_OBJECT

public:
    explicit FormContextualWidgetManager(QObject *parent = 0);
    ~FormContextualWidgetManager();

private Q_SLOTS:
    void updateContext(Core::IContext *object, const Core::Context &additionalContexts);

private:
    QPointer<Core::IContext> _contextObject;
};

} // namespace Internal
} // namespace Form

#endif // FORM_INTERNAL_FORMCONTEXTUALWIDGETMANAGER_H

