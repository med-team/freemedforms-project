/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FORMFILESSELECTORWIDGET_H
#define FORMFILESSELECTORWIDGET_H

#include <formmanagerplugin/formmanager_exporter.h>

#include <QWidget>
#include <QAbstractItemView>

QT_BEGIN_NAMESPACE
class QModelIndex;
QT_END_NAMESPACE


/**
 * \file ./plugins/formmanagerplugin/formfilesselectorwidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Form {
class FormIODescription;
namespace Internal {
class FormFilesSelectorWidgetPrivate;
}  // End namespace Internal

class FORM_EXPORT FormFilesSelectorWidget : public QWidget
{
    Q_OBJECT

public:
    enum FormType {
        AllForms = 0,
        CompleteForms,
        SubForms
//        Pages
    };
    enum SelectionType {
        Single = QAbstractItemView::SingleSelection,
        Multiple = QAbstractItemView::MultiSelection
    };

    FormFilesSelectorWidget(QWidget *parent = 0, const FormType type = AllForms, const SelectionType selType = Single);
    ~FormFilesSelectorWidget();

    void setFormType(FormType type);
    void setExcludeGenderSpecific(bool excludeGenderSpecific);
    void setExcludeFormByUid(const QStringList &formuids);
    const QStringList &excludedFormByUid() const;
    void setSelectionType(SelectionType type);
    void expandAllItems() const;
    void setIncludeLocalFiles(bool includeLocal);

    QList<Form::FormIODescription *> selectedForms() const;

    void highlightForm(const QString &uuidOrAbsPath);

private Q_SLOTS:
    void onDescriptionSelected(const QModelIndex &index, const QModelIndex &previous);
    void onFilterSelected();
    void showScreenShot();

protected:
    void changeEvent(QEvent *e);

private:
    Internal::FormFilesSelectorWidgetPrivate *d;
};

}  // End namespace Form

#endif // FORMFILESSELECTORWIDGET_H
