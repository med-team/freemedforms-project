/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER <eric.maeker@gmail.com>                               *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "formcontextualwidget.h"
#include "constants_db.h"

#include <coreplugin/contextmanager/contextmanager.h>
#include <coreplugin/icore.h>

using namespace Form;
using namespace Internal;

static inline Core::ContextManager *contextManager() { return Core::ICore::instance()->contextManager(); }

FormContextualWidget::FormContextualWidget(QWidget *parent) :
    QWidget(parent),
    m_Context(0)
{
    m_Context = new Internal::FormContext(this);
    m_Context->setContext(Core::Context(Constants::C_FORM_PLUGINS));

    contextManager()->addContextObject(m_Context);
}

FormContextualWidget::~FormContextualWidget()
{
    contextManager()->removeContextObject(m_Context);
}

Core::IContext *FormContextualWidget::context() const
{
    return m_Context;
}
