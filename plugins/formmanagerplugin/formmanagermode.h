/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FORMMANAGERMODE_H
#define FORMMANAGERMODE_H

#include <coreplugin/modemanager/imode.h>

#include <QObject>

/**
 * \file ./plugins/formmanagerplugin/formmanagermode.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/


namespace Form {
class FormPlaceHolder;
namespace Internal {

class FormManagerMode : public Core::IMode
{
    Q_OBJECT
public:
    FormManagerMode(QObject *parent);
    ~FormManagerMode();

    QString name() const;

private Q_SLOTS:
    bool onPatientFormsLoaded();

private:
//    bool eventFilter(QObject *obj, QEvent *event);

private:
    FormPlaceHolder *m_Holder;
    bool m_inPluginManager, m_actionInBar;
};

}  // End namespace Internal
}  // End namespace Form

#endif // FORMMANAGERMODE_H
