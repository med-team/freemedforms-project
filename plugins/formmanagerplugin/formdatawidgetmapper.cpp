﻿/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "formdatawidgetmapper.h"
#include <formmanagerplugin/formcore.h>
#include <formmanagerplugin/formmanager.h>
#include <formmanagerplugin/episodemanager.h>
#include <formmanagerplugin/iformitem.h>
#include <formmanagerplugin/iformitemdata.h>
#include <formmanagerplugin/iformwidgetfactory.h>
#include <formmanagerplugin/episodemodel.h>
#include <formmanagerplugin/constants_db.h>
#include <formmanagerplugin/constants_settings.h>

#include <coreplugin/icore.h>
#include <coreplugin/ipatient.h>
#include <coreplugin/iuser.h>

#include <utils/log.h>
#include <utils/global.h>

#include <QScrollArea>
#include <QStackedLayout>
#include <QTextBrowser>

using namespace Form;
using namespace Internal;

enum { WarnLogChronos=false, WarnDirty=true };

static inline Form::FormManager &formManager() {return Form::FormCore::instance().formManager();}
static inline Form::EpisodeManager &episodeManager() {return Form::FormCore::instance().episodeManager();}

namespace Form {
namespace Internal {
class FormDataWidgetMapperPrivate
{
public:
    FormDataWidgetMapperPrivate(FormDataWidgetMapper *parent) :
        _stack(0),
        _formMain(0),
        _episodeModel(0),
        q(parent)
    {
    }

    ~FormDataWidgetMapperPrivate()
    {}

    void clearStackLayout()
    {
        if (_stack) {
            if (_formMain) {
                QList<Form::FormMain *> forms;
                forms << _formMain;
                forms << _formMain->flattenedFormMainChildren();
                foreach(FormMain *form, forms) {
                    if (form->formWidget()) {
                        form->formWidget()->setParent(0);
                    }
                }
            }

            for(int i=0; i < _stack->count(); ++i) {
                delete _stack->widget(i);
            }
        }
    }

    void populateStack(Form::FormMain *rootForm)
    {
        if (!_stack) {
            _stack = new QStackedLayout(q);
            q->setLayout(_stack);
        }
        clearStackLayout();
        _formMain = rootForm;


        if (!rootForm)
            return;

        QList<Form::FormMain *> forms;
        forms << _formMain;
        forms << _formMain->flattenedFormMainChildren();

        foreach(FormMain *form, forms) {
            if (form->formWidget()) {
                QScrollArea *sa = new QScrollArea(_stack->parentWidget());
                sa->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
                sa->setWidgetResizable(true);
                QWidget *w = new QWidget(sa);
                sa->setWidget(w);
                w->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
                QVBoxLayout *vl = new QVBoxLayout(w);
                vl->setSpacing(0);
                vl->setMargin(0);
                vl->addWidget(form->formWidget());
                int id = _stack->addWidget(sa);
                _stackId_FormUuid.insert(id, form->uuid());
            }
        }
    }

    void useEpisodeModel(Form::FormMain *rootForm)
    {
        if (_episodeModel) {
            _episodeModel = 0;
        }
        _episodeModel = episodeManager().episodeModel(rootForm);
    }

    QString getCurrentXmlEpisode()
    {
        if (!_formMain)
            return QString::null;

        QHash<QString, QString> xmlData;
        foreach(FormItem *it, _formMain->flattenedFormItemChildren()) {
            if (it->itemData()) {
                xmlData.insert(it->uuid(), it->itemData()->storableData().toString());
            }
        }
        return Utils::createXml(Form::Constants::XML_FORM_GENERAL_TAG, xmlData, 2, false);
    }

    void setCurrentEpisode(const QModelIndex &index)
    {
        if (!_episodeModel) {
            if (_formMain)
                LOG_ERROR_FOR(q, "No episode model. FormUid: " + _formMain->uuid());
            else
                LOG_ERROR_FOR(q, "No episode model. FormUid: (0x0)");
            return;
        }
        _currentEpisode = index;

        if (!index.isValid()) {
            LOG_ERROR_FOR(q, "Invalid index when setting current episode. Episode not read.");
            return;
        }

        int stackIndex;
        stackIndex = _formMain ? _stackId_FormUuid.key(_formMain->uuid()) : 0;
        _stack->setCurrentIndex(stackIndex);

        _episodeModel->populateFormWithEpisodeContent(index, true);
    }

public:
    QStackedLayout *_stack;
    QHash<int, QString>_stackId_FormUuid;
    Form::FormMain *_formMain;
    EpisodeModel *_episodeModel;
    QPersistentModelIndex _currentEpisode;

private:
    FormDataWidgetMapper *q;
};
}  // namespace Internal
}  // namespace Form

FormDataWidgetMapper::FormDataWidgetMapper(QWidget *parent) :
    QWidget(parent),
    d(new FormDataWidgetMapperPrivate(this))
{
    setObjectName("FormDataWidgetMapper");
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
}

FormDataWidgetMapper::~FormDataWidgetMapper()
{
}

bool FormDataWidgetMapper::initialize()
{
    return true;
}

void FormDataWidgetMapper::clear()
{
    if (!d->_formMain)
        return;
    LOG("Clear");
    d->_formMain->clear();
    d->_currentEpisode = QModelIndex();
}


bool FormDataWidgetMapper::isDirty() const
{
    if (!d->_formMain)
        return false;
    if (!d->_currentEpisode.isValid())
        return false;

    if (d->_formMain->itemData() && d->_formMain->itemData()->isReadOnly()) {
        LOG(QString("isDirty (form) %1 isReadOnly").arg(d->_formMain->uuid()));
        return false;
    }

    if (d->_formMain->itemData() && d->_formMain->itemData()->isModified()) {
        if (WarnDirty)
            LOG(QString("isDirty (form) %1 %2").arg(d->_formMain->uuid()).arg(d->_formMain->itemData()->isModified()));
        return true;
    }
    foreach(FormItem *it, d->_formMain->flattenedFormItemChildren()) {
        if (it->itemData() && it->itemData()->isModified()) {
            if (WarnDirty)
                LOG(QString("isDirty (item) %1 %2").arg(it->uuid()).arg(+it->itemData()->isModified()));
            return true;
        }
    }
    if (WarnDirty)
        LOG(QString("isDirty false, Form: %1").arg(d->_formMain->uuid()));
    return false;
}

QModelIndex FormDataWidgetMapper::currentEditingEpisodeIndex() const
{
    return d->_currentEpisode;
}

QString FormDataWidgetMapper::currentEpisodeLabel() const
{
    if (d->_formMain && d->_formMain->itemData())
        return d->_formMain->itemData()->data(IFormItemData::ID_EpisodeLabel).toString();
    return QString::null;
}

QString FormDataWidgetMapper::currentFormName() const
{
    if (d->_formMain)
        return d->_formMain->spec()->label();
    return QString::null;
}

void FormDataWidgetMapper::setCurrentForm(const QString &formUid)
{
    setCurrentForm(formManager().form(formUid));
}

void FormDataWidgetMapper::setCurrentForm(Form::FormMain *form)
{
    d->clearStackLayout();
    if (!form)
        return;
    d->populateStack(form);
    d->useEpisodeModel(form);
    if (d->_formMain->itemData())
        d->_formMain->itemData()->setModified(false);
}

void FormDataWidgetMapper::setLastEpisodeAsCurrent()
{
    setCurrentEpisode(d->_episodeModel->index(0,0));
}

void FormDataWidgetMapper::setCurrentEpisode(const QModelIndex &index)
{
    d->setCurrentEpisode(index);
}

void FormDataWidgetMapper::setFormWidgetEnabled(bool enabled)
{
    d->_formMain->formWidget()->setEnabled(enabled);
}

QPixmap FormDataWidgetMapper::screenshot()
{
    QScrollArea *area = qobject_cast<QScrollArea*>(d->_stack->currentWidget());
    QWidget *widget = area->widget();
    return QPixmap::grabWidget(widget);
}

bool FormDataWidgetMapper::submit()
{
    const QString &xml = d->getCurrentXmlEpisode();
    QModelIndex xmlIndex = d->_episodeModel->index(d->_currentEpisode.row(), EpisodeModel::XmlContent);
    if (!d->_episodeModel->setData(xmlIndex, xml)) {
        LOG_ERROR("Unable to save the episode XML content");
        return false;
    }

    QModelIndex userName = d->_episodeModel->index(d->_currentEpisode.row(), EpisodeModel::UserCreatorName);
    QModelIndex userDate = d->_episodeModel->index(d->_currentEpisode.row(), EpisodeModel::UserDateTime);
    QModelIndex label = d->_episodeModel->index(d->_currentEpisode.row(), EpisodeModel::Label);
    QModelIndex prior = d->_episodeModel->index(d->_currentEpisode.row(), EpisodeModel::Priority);

    d->_episodeModel->setData(label, d->_formMain->itemData()->data(IFormItemData::ID_EpisodeLabel));
    d->_episodeModel->setData(userName, d->_formMain->itemData()->data(IFormItemData::ID_UserName));
    d->_episodeModel->setData(userDate, d->_formMain->itemData()->data(IFormItemData::ID_EpisodeDateTime));
    d->_episodeModel->setData(prior, d->_formMain->itemData()->data(IFormItemData::ID_Priority));

    bool ok = d->_episodeModel->submit();
    if (ok) {
        d->_episodeModel->populateFormWithEpisodeContent(d->_currentEpisode, true);
        d->_episodeModel->dataChanged(d->_episodeModel->index(d->_currentEpisode.row(), EpisodeModel::Priority),
                                      d->_episodeModel->index(d->_currentEpisode.row(), EpisodeModel::Label));
    }
    return ok;
}

