/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric MAEKER, <eric.maeker@gmail.com>                *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FORMMANAGER_FORMFILE_H
#define FORMMANAGER_FORMFILE_H

#include <formmanagerplugin/formmanager_exporter.h>

/**
 * \file ./plugins/formmanagerplugin/formfile.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

// NOT USED

namespace Form {

class FORM_EXPORT FormFile
{
public:
    enum DataRepresentation {
        Id = 0,
        Uuid,
        IsValid,
        CategoryId,
        Authors,
        DateCreation,
        nextfollow
    };

    FormFile();
    ~FormFile();

};

}  // end namespace Form


#endif // FORMMANAGER_FORMFILE_H
