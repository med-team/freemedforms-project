/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FORM_NEW_EPISODEMODEL_H
#define FORM_NEW_EPISODEMODEL_H

#include <formmanagerplugin/formmanager_exporter.h>

#include <QAbstractListModel>
QT_BEGIN_NAMESPACE
class QSqlRecord;
QT_END_NAMESPACE

/**
 * \file ./plugins/formmanagerplugin/episodemodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

// friend class only
namespace Patients {
namespace Internal {
class IdentityViewerWidget;
}
}

namespace Form {
class FormMain;
class EpisodeManager;
class PatientFormItemDataWrapper;

namespace Internal {
class EpisodeModelPrivate;
class PatientFormItemDataWrapperPrivate;
class FormExporterPrivate;
}  // namespace Internal

class FORM_EXPORT EpisodeModel : public QAbstractListModel
{
    Q_OBJECT
    friend class Form::EpisodeManager;
    friend class Form::PatientFormItemDataWrapper;
    friend class Form::Internal::PatientFormItemDataWrapperPrivate;
    friend class Form::Internal::FormExporterPrivate;
    friend class Patients::Internal::IdentityViewerWidget;

public:
    EpisodeModel(Form::FormMain *rootEmptyForm, QObject *parent = 0);
    bool initialize();

public:
    enum DataRepresentation {
        ValidationStateIcon = 0,
        PriorityIcon,
        UserDateTime, // Date & time of the episode, can be edited by the user
        Label,
        IsValid,
        CreationDateTime, // Date & time of creation of the episode (read-only)
        Priority,
        UserCreatorName,
        XmlContent,
        Icon,
        Uuid,       // Read-Only
        FormUuid,   // Read-Only
        FormLabel,  // Read-Only - decoration includes the form icon
        EmptyColumn1,
        EmptyColumn2,
        MaxData
    };

    enum Priority {
        High = 0,
        Medium,
        Low
    };

    virtual ~EpisodeModel();
    QString formUid() const;
    void setCurrentPatient(const QString &uuid);
    QString currentPatientUuid() const;
    void setUseFormContentCache(bool useCache);

    Qt::ItemFlags flags(const QModelIndex &index) const;
    void fetchMore(const QModelIndex &parent);
    bool canFetchMore(const QModelIndex &parent) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

    QVariant headerData(int section, Qt::Orientation orientation,
                                int role = Qt::DisplayRole) const;

    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());
    bool removeAllEpisodes();

    void setReadOnly(bool state);
    bool isReadOnly() const;
    bool isDirty() const;

    bool validateEpisode(const QModelIndex &index);
    bool isEpisodeValidated(const QModelIndex &index) const;

    bool removeEpisode(const QModelIndex &index);
    QModelIndex renewEpisode(const QModelIndex &episodeToRenew);

    void refreshFilter();

public Q_SLOTS:
    // FIXME: remove the 'feedPatientModel' as we created the Form::PatientFormItemDataWrapper
    bool populateFormWithEpisodeContent(const QModelIndex &episode, bool feedPatientModel = false);
    bool populateFormWithLatestValidEpisodeContent();
    bool submit();

Q_SIGNALS:
    void episodeAboutToChange(const QModelIndex &index);
    void episodeChanged(const QModelIndex &index);

private Q_SLOTS:
    void onUserChanged();
    void onPatientFormLoaded();
    void populateNewRowWithDefault(int row, QSqlRecord &record);

public Q_SLOTS:
    QString lastEpisodesSynthesis() const;

    void onCoreDatabaseServerChanged();

private:
    Internal::EpisodeModelPrivate *d;
};

}  // End namespace Form


#endif // End FORM_NEW_EPISODEMODEL_H
