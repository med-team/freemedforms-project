    /***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "formviewdelegate.h"
#include "formtreemodel.h"
#include "iformitem.h"
#include "iformitemspec.h"

#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/constants_icons.h>

#include <QApplication>
#include <QPainter>
#include <QModelIndex>

#include <QDebug>

static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }

namespace Form {
namespace Internal {

FormViewDelegate::FormViewDelegate(QObject *parent) :
    QStyledItemDelegate(parent),
    _formTreeModel(0)
{
}

void FormViewDelegate::setFormTreeModel(FormTreeModel *model)
{
    _formTreeModel = model;
}

QSize FormViewDelegate::sizeHint(const QStyleOptionViewItem &option,const QModelIndex &index) const
{
    Q_ASSERT(_formTreeModel);
    const bool topLevel = !index.parent().isValid();
    if (topLevel) {
        QSize itemSize(10, 10);
        Form::FormMain *form = _formTreeModel->formForIndex(index);
        if (form) {
            if (form->extraData().contains("rootitemextraheight")) {
                itemSize = QSize(10, form->extraData().value("rootitemextraheight").toInt());
            } else {
                if (form->rootFormParent()->extraData().contains("rootitemextraheight")) {
                    itemSize = QSize(10, form->rootFormParent()->extraData().value("rootitemextraheight").toInt());
                }
            }
        }
        return QStyledItemDelegate::sizeHint(option, index) + itemSize;
    }
    return QStyledItemDelegate::sizeHint(option, index);
}

void FormViewDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
           const QModelIndex &index) const
{
    if (option.state & QStyle::State_MouseOver) {
        if ((QApplication::mouseButtons() & Qt::LeftButton) == 0)
            pressedIndex = QModelIndex();
        QBrush brush = option.palette.alternateBase();
        if (index == pressedIndex)
            brush = option.palette.dark();
        painter->fillRect(option.rect, brush);
    }

    QStyledItemDelegate::paint(painter, option, index);

    if (index.column()==FormTreeModel::EmptyColumn1 &&
            (option.state & QStyle::State_MouseOver)) {
        QIcon icon;
        if (option.state & QStyle::State_Selected) {
            if (_formTreeModel->isUniqueEpisode(index))
                return;
            if (_formTreeModel->isNoEpisode(index))
                return;
            icon = theme()->icon(Core::Constants::ICONADDLIGHT);
        } else {
            if (_formTreeModel->isUniqueEpisode(index))
                return;
            if (_formTreeModel->isNoEpisode(index))
                return;
            icon = theme()->icon(Core::Constants::ICONADDDARK);
        }

        QRect iconRect(option.rect.right() - option.rect.height(),
                       option.rect.top(),
                       option.rect.height(),
                       option.rect.height());

        icon.paint(painter, iconRect, Qt::AlignRight | Qt::AlignVCenter);
    }
}

} // namespace Internal
} // namespace Form
