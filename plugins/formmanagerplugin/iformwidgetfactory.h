/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IFORMWIDGETFACTORY_H
#define IFORMWIDGETFACTORY_H

#include <formmanagerplugin/formmanager_exporter.h>
#include <formmanagerplugin/iformitem.h>

#include <QObject>
#include <QString>
#include <QStringList>
#include <QWidget>
#include <QFrame>
#include <QBoxLayout>
#include <QLabel>
#include <QPointer>

/**
 * \file ./plugins/formmanagerplugin/iformwidgetfactory.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Form {
class IFormWidget;

class FORM_EXPORT IFormWidgetFactory : public QObject
{
    Q_OBJECT
public:
    IFormWidgetFactory(QObject *parent = 0) : QObject(parent) {}
    virtual ~IFormWidgetFactory() {}

    virtual bool initialize(const QStringList &arguments, QString *errorString) = 0;
    virtual bool extensionInitialized() = 0;
    virtual QStringList providedWidgets() const = 0;
    virtual bool isContainer( const int idInStringList ) const = 0;
    inline bool isContainer(const QString &name) {return isContainer(providedWidgets().indexOf(name)); }
    virtual bool isInitialized() const = 0;
    virtual IFormWidget *createWidget(const QString &name, Form::FormItem *linkedObject, QWidget *parent = 0) = 0;
};


class FORM_EXPORT IFormWidget : public QWidget
{
    Q_OBJECT
public:
    enum QFrameStyleForLabels {
        FormLabelFrame = QFrame::Panel | QFrame::Sunken,
        ItemLabelFrame = QFrame::NoFrame,
        HelpTextFrame =  QFrame::Panel | QFrame::Sunken
    };

    enum LabelOptions {
        NoOptions = 0,
        NoLabel,
        OnTop,
        OnBottom,
        OnLeft,
        OnRight,
        OnTopCentered
    };

    enum OutputType {
        HtmlPrintOutput,
        HtmlExportOutput,
        PlainTextPrintOutput,
        PlainTextExportOutput
    };

    IFormWidget(Form::FormItem *linkedObject, QWidget *parent = 0);
    virtual ~IFormWidget();

    virtual void addWidgetToContainer(IFormWidget *) {}
    virtual bool isContainer() const {return false;}

    virtual void createLabel(const QString &text, Qt::Alignment horizAlign = Qt::AlignLeft);
    virtual QBoxLayout *getBoxLayout(const int labelOption, const QString &text, QWidget *parent);
    virtual QHBoxLayout *getHBoxLayout(const int labelOption, const QString &text, QWidget *parent);

    virtual void changeEvent(QEvent *event);

    virtual void setFormItem(Form::FormItem *link) { m_FormItem = link; }
    virtual Form::FormItem *formItem() { return m_FormItem; }

    QWidget *focusedWidget() const {return m_focusedWidget;}
    void setFocusedWidget(QWidget *widget) {m_focusedWidget = widget;}

    QWidget *lastTabWidget() const {if (!_lastTabWidget) return m_focusedWidget; return _lastTabWidget;}
    void setLastTabWidget(QWidget *widget) {_lastTabWidget = widget;}
    virtual void setTabOrder(bool consoleWarn = false) {Q_UNUSED(consoleWarn);}

    virtual QString printableHtml(bool withValues = true) const {Q_UNUSED(withValues); return QString();}
    // toString
//    virtual QString toString(OutputType type = HtmlPrintOutput, bool withValues = true) const {Q_UNUSED(type); Q_UNUSED(withValues); return QString();}

public Q_SLOTS:
    virtual void retranslate() {}

public:
    QLabel *m_Label;
    QPointer<Form::FormItem> m_FormItem;
    QString m_OldTrans;
    QWidget *m_focusedWidget, *_lastTabWidget;
};

} // namespace Form

#endif // IFORMWIDGETFACTORY_H
