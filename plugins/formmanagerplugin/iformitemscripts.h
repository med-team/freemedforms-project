/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IFORMITEMSCRIPTS_H
#define IFORMITEMSCRIPTS_H

#include <formmanagerplugin/formmanager_exporter.h>

#include <translationutils/constants.h>

#include <QObject>
#include <QString>
#include <QDateTime>
#include <QWidget>
#include <QVariant>
#include <QPointer>
#include <QHash>

QT_BEGIN_NAMESPACE
class QTreeWidget;
class QTreeWidgetItem;
QT_END_NAMESPACE

/**
 * \file ./plugins/formmanagerplugin/iformitemscripts.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Form {

namespace Internal{
class FormItemScriptsPrivate;
}

class FORM_EXPORT FormItemScripts
{
public:
    enum TypeOfScript {
        Script_OnLoad = 0,
        Script_PostLoad,
        Script_OnDemand,
        Script_OnValueChanged,
        Script_OnValueRequired,
        Script_OnDependentValueChanged,
        Script_OnClicked,
        Script_OnToggled

    };


    FormItemScripts(const QString &lang = Trans::Constants::ALL_LANGUAGE,
                    const QString &onLoad = QString::null,
                    const QString &postLoad = QString::null,
                    const QString &onDemand = QString::null,
                    const QString &onValChanged = QString::null,
                    const QString &onValRequired = QString::null,
                    const QString &onDependentValuesChanged = QString::null,
                    const QString &onToggled = QString::null);
    ~FormItemScripts();

    void setScript(const int type, const QString &script, const QString &lang = Trans::Constants::ALL_LANGUAGE);
    QString script(const int type, const QString &lang = Trans::Constants::ALL_LANGUAGE) const;

    void warn() const;

    QString onLoadScript() const {return script(Script_OnLoad);}
    QString postLoadScript() const {return script(Script_PostLoad);}
    QString onDemandScript() const {return script(Script_OnDemand);}
    QString onValueChangedScript() const {return script(Script_OnValueChanged);}
    QString onValueRequiredScript() const {return script(Script_OnValueRequired);}
    QString onDependentValueChangedScript() const {return script(Script_OnDependentValueChanged);}
    QString onClicked() const {return script(Script_OnClicked);}
    QString onToggledScript() const {return script(Script_OnToggled);}

    void toTreeWidget(QTreeWidgetItem *tree);
private:
    Internal::FormItemScriptsPrivate *d;
};


} // end Form

#endif // IFORMITEMSCRIPTS_H
