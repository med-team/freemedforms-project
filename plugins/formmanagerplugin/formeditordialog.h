/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FORMEDITORDIALOG_H
#define FORMEDITORDIALOG_H

#include <QDialog>
QT_BEGIN_NAMESPACE
class QSortFilterProxyModel;
QT_END_NAMESPACE

/**
 * \file ./plugins/formmanagerplugin/formeditordialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Form {
class FormTreeModel;
namespace Internal {
class FormViewDelegate;
}
namespace Ui {
class FormEditorDialog;
}

class FormEditorDialog : public QDialog
{
    Q_OBJECT
public:
    enum EditionMode {
        ViewOnly      = 0x000,
        AllowAddition = 0x001,
        AllowEdition  = 0x002,
        AllowRemoval  = 0x004,
        DefaultMode   = ViewOnly
    };
    Q_DECLARE_FLAGS(EditionModes, EditionMode)

    explicit FormEditorDialog(FormTreeModel *model, EditionModes mode = DefaultMode, QWidget *parent = 0);
    ~FormEditorDialog();

//    QString lastInsertedFormUid() const {return m_LastInsertedFormUid;}

protected:
    void changeEvent(QEvent *e);

private Q_SLOTS:
    void on_addForm_clicked();

private:
    Ui::FormEditorDialog *ui;
    Form::FormTreeModel *_formTreeModel;
    Internal::FormViewDelegate *_delegate;
//    QString m_LastInsertedFormUid;
};

}  // End namespace Form
Q_DECLARE_OPERATORS_FOR_FLAGS(Form::FormEditorDialog::EditionModes)

#endif // FORMEDITORDIALOG_H
