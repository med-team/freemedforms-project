/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FORMMANAGERPREFERENCESPAGE_H
#define FORMMANAGERPREFERENCESPAGE_H

#include <coreplugin/ioptionspage.h>

#include <QPointer>
#include <QObject>

/**
 * \file ./plugins/formmanagerplugin/formmanagerpreferencespage.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;
}

namespace Form {
class FormFilesSelectorWidget;

namespace Internal {
namespace Ui {
class FormPreferencesFileSelectorWidget;
}

class FormPreferencesFileSelectorWidget : public QWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(FormPreferencesFileSelectorWidget)

public:
    explicit FormPreferencesFileSelectorWidget(QWidget *parent = 0);
    ~FormPreferencesFileSelectorWidget();

    static void writeDefaultSettings(Core::ISettings *) {}

public Q_SLOTS:
    void saveFormToBase();
    void saveToSettings(Core::ISettings *);

//protected:
//    virtual void changeEvent(QEvent *e);

private:
    Ui::FormPreferencesFileSelectorWidget *ui;
};


class FormPreferencesFileSelectorPage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    FormPreferencesFileSelectorPage(QObject *parent = 0);
    ~FormPreferencesFileSelectorPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    QString helpPage() {return "parametrer.html";}

    static void writeDefaultSettings(Core::ISettings *s) {FormPreferencesFileSelectorWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);
private:
    QPointer<FormPreferencesFileSelectorWidget> m_Widget;
};


}  // End Internal
}  // End Form


namespace Form {
namespace Internal {
namespace Ui {
class FormPreferencesWidget;
}

class FormPreferencesWidget : public QWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(FormPreferencesWidget)

public:
    explicit FormPreferencesWidget(QWidget *parent = 0);
    ~FormPreferencesWidget();

    static void writeDefaultSettings(Core::ISettings *) {}
    void setupUiData();

public Q_SLOTS:
    void saveToSettings(Core::ISettings *);

//protected:
//    virtual void changeEvent(QEvent *e);

private:
    Ui::FormPreferencesWidget *ui;
};


class FormPreferencesPage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    FormPreferencesPage(QObject *parent = 0);
    ~FormPreferencesPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    QString helpPage() {return "parametrer.html";}

    static void writeDefaultSettings(Core::ISettings *s) {FormPreferencesWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);
private:
    QPointer<FormPreferencesWidget> m_Widget;
    QFont _defaultFormFont;
    QFont _defaultEpisodeFont;
};


}  // End Internal
}  // End Form

#endif // FORMMANAGERPREFERENCESPAGE_H
