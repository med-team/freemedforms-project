/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "druginteractionquery.h"
#include "idrug.h"

#include <QDebug>

using namespace DrugsDB;

DrugInteractionQuery::DrugInteractionQuery(const QVector<IDrug *> &testDrugs, QObject *parent) :
        QObject(parent), m_Drugs(testDrugs), m_TestDDI(true), m_TestPDI(true), m_StandardModel(0)
{
}

DrugInteractionQuery::DrugInteractionQuery(QObject *parent) :
        QObject(parent), m_TestDDI(true), m_TestPDI(true), m_StandardModel(0)
{
}

DrugInteractionQuery::~DrugInteractionQuery()
{
    if (m_StandardModel)
        delete m_StandardModel;
    m_StandardModel = 0;
}

void DrugInteractionQuery::clearDrugsList()
{
    m_Drugs.clear();
}

void DrugInteractionQuery::setDrugsList(const QVector<IDrug *> &list)
{
    m_Drugs = list;
}

void DrugInteractionQuery::addDrug(IDrug *drug)
{
    if (!m_Drugs.contains(drug))
        m_Drugs.append(drug);
}

void DrugInteractionQuery::removeDrug(IDrug *drug)
{
    int id = m_Drugs.indexOf(drug);
    if (id!=-1) {
        m_Drugs.remove(id);
    }
}

void DrugInteractionQuery::removeLastInsertedDrug()
{
    if (!m_Drugs.isEmpty())
        m_Drugs.remove(m_Drugs.indexOf(m_Drugs.last()));
}

bool DrugInteractionQuery::containsDrug(const IDrug *drug) const
{
    return m_Drugs.contains((IDrug*)drug);
}

QStandardItemModel *DrugInteractionQuery::toStandardModel() const
{
    if (!m_StandardModel) {
        m_StandardModel = new QStandardItemModel;
    }

    QFont bold;
    bold.setBold(true);
    QVector<IDrug *> insertedDrugs;
    for(int i=0; i < m_Drugs.count(); ++i) {
        IDrug *drug = m_Drugs.at(i);
        if (insertedDrugs.contains(drug))
            continue;
        insertedDrugs.append(drug);
        QStandardItem *root = new QStandardItem(drug->brandName());
        root->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
        root->setData(drug->drugId());
        m_StandardModel->appendRow(root);
    }
    return m_StandardModel;
}

QString DrugInteractionQuery::warnText() const
{
    QString tmp;
    for(int i=0; i < m_Drugs.count(); ++i) {
        tmp += "  * " + m_Drugs.at(i)->brandName() + "\n";
    }
    if (tmp.isEmpty())
        tmp = "  !! No drug\n";
    tmp = QString("DrugInteractionQuery: testing\n%1"
                  "  * TestDDI: %2\n"
                  "  * TestPDI: %3")
          .arg(tmp)
          .arg(m_TestDDI)
          .arg(m_TestPDI);
    return tmp;
}

QDebug operator<<(QDebug dbg, const DrugsDB::DrugInteractionQuery *c)
{
    if (!c) {
        dbg.nospace() << "DrugInteractionQuery(0x0)";
        return dbg.space();
    }
    return operator<<(dbg, *c);
}

QDebug operator<<(QDebug dbg, const DrugsDB::DrugInteractionQuery &c)
{
    dbg.nospace() << c.warnText();
    return dbg.space();
}


