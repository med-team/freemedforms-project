/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGSDB_INTERNAL_DRUGBASEESSENTIALS_H
#define DRUGSDB_INTERNAL_DRUGBASEESSENTIALS_H

#include <drugsbaseplugin/drugsbase_exporter.h>
#include <utils/database.h>

/**
 * \file ./plugins/drugsbaseplugin/drugbaseessentials.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/
namespace DrugsDB {
namespace Internal {

class DRUGSBASE_EXPORT DrugBaseEssentials : public Utils::Database
{
public:
    DrugBaseEssentials();
    virtual ~DrugBaseEssentials() {}
    void forceFullDatabaseRefreshing();
    bool initialize(const QString &pathToDb, bool createIfNotExists = false);

    bool checkDatabaseVersion() const;

    int getSourceId(const QString &drugsDbUid);
    bool isAtcAvailable() const;

private:
    bool createDatabase(const QString &connectionName , const QString &prefixedDbName,
                        const Utils::DatabaseConnector &connector,
                        CreationOption createOption
                       );

protected:
    bool m_dbcore_initialized;
    bool m_isDefaultDb;
};

}  // End namespace Internal
}  // End namespace DrugsDB

#endif // DRUGSDB_INTERNAL_DRUGBASEESSENTIALS_H
