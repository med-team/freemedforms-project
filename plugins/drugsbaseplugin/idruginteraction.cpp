/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "idruginteraction.h"
#include <drugsbaseplugin/idrugengine.h>

#include <QStringList>
#include <QDebug>

QDebug operator<<(QDebug dbg, const DrugsDB::IDrugInteraction &c)
{
    QStringList drugs;
    for(int i=0; i<c.drugs().count(); ++i) {
        drugs << c.drugs().at(i)->brandName();
    }
    QString type;
    if (c.isDrugDrugInteraction())
        type = "DDI";
    else if (c.isPotentiallyInappropriate())
        type = "PIM";
    dbg.nospace() << "IDrugInteraction("
                  << type << "; "
                  << c.type() << "\n"
                  << drugs.join(",") << "; "
                  << "engine: " << c.engine()->uid()<< "; "
                  << ")";
    return dbg.space();
}

QDebug operator<<(QDebug dbg, const DrugsDB::IDrugInteraction *c)
{
    if (c)
        dbg = operator<<(dbg, *c);
    else
        dbg.nospace() << "IDrugInteraction(0x0)";
    return dbg.space();
}

