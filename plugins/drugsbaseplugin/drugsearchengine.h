/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGSEARCHENGINE_H
#define DRUGSEARCHENGINE_H

#include <translationutils/constanttranslations.h>
#include <drugsbaseplugin/drugsbase_exporter.h>
#include <QString>

/**
 * \file ./plugins/drugsbaseplugin/drugsearchengine.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/


namespace DrugsDB {
class IDrug;
namespace Internal {
class DrugSearchEnginePrivate;

class DRUGSBASE_EXPORT DrugSearchEngine
{
    DrugSearchEngine();
public:
    static DrugSearchEngine *instance();
    ~DrugSearchEngine();

    void clear();

    void addNewEngine(const QString &label, const QString &url, const QString &lang = Trans::Constants::ALL_LANGUAGE);

    void setDrug(const IDrug *drug);

    QStringList processedLabels(const QString &lang = Trans::Constants::ALL_LANGUAGE) const;
    QStringList processedUrls(const QString &label, const QString &lang = Trans::Constants::ALL_LANGUAGE) const;

    int numberOfEngines() const;

private:
    DrugSearchEnginePrivate *d;
    static DrugSearchEngine *m_Instance;
};

}  // End namespace Internal
}  // End namespace DrugsDB

#endif // DRUGSEARCHENGINE_H
