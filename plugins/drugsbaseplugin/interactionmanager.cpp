/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "interactionmanager.h"

#include <drugsbaseplugin/idrugengine.h>
#include <drugsbaseplugin/idrug.h>
#include <drugsbaseplugin/drugsbase.h>
#include <drugsbaseplugin/idruginteraction.h>
#include <drugsbaseplugin/druginteractionquery.h>
#include <drugsbaseplugin/druginteractionresult.h>

#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>

#include <utils/log.h>
#include <translationutils/constanttranslations.h>
#include <extensionsystem/pluginmanager.h>

#include <QCoreApplication>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QFile>
#include <QDir>
#include <QMultiHash>
#include <QMap>
#include <QMultiMap>
#include <QList>
#include <QSet>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QLabel>
#include <QStandardItemModel>

enum { WarnComputations = false };

using namespace DrugsDB;
using namespace Trans::ConstantTranslations;

static inline ExtensionSystem::PluginManager *pluginManager() { return ExtensionSystem::PluginManager::instance(); }

namespace DrugsDB {
namespace Internal {
class InteractionManagerPrivate
{
public:
    InteractionManagerPrivate(InteractionManager *parent) :
        m_LogChrono(false),
        q(parent)
    {
        Q_UNUSED(q);
    }

    ~InteractionManagerPrivate()
    {
    }

public:
    QVector<IDrugEngine*> m_Engines;
    bool m_LogChrono;

private:
    InteractionManager *q;
};

}  // End Internal
}  // End Drugs

InteractionManager::InteractionManager(QObject *parent) :
        QObject(parent), d(0)
{
    static int handler = 0;
    ++handler;
    d = new Internal::InteractionManagerPrivate(this);
    setObjectName("InteractionManager" + QString::number(handler));

    d->m_Engines = pluginManager()->getObjects<IDrugEngine>().toVector();
    connect(pluginManager(), SIGNAL(objectAdded(QObject*)), this, SLOT(onNewObjectAddedToPluginManagerPool(QObject*)));
}

InteractionManager::~InteractionManager()
{
    if (d) delete d;
    d=0;
}

DrugInteractionResult *InteractionManager::checkInteractions(const DrugInteractionQuery &query, QObject *parent)
{
    if (query.drugsList().isEmpty())
        return new DrugInteractionResult(parent);

    QTime t;
    t.start();
    int nbInteractions = 0;
    DrugInteractionResult *result = new DrugInteractionResult(parent);
    result->setTestedDrugs(query.drugsList());

    for(int i = 0; i < d->m_Engines.count(); ++i) {
        IDrugEngine *engine = d->m_Engines.at(i);

        if (WarnComputations) {
            qWarning() << "DrugEngine" << engine->name()
                       << "Active" << engine->isActive()
                       << "CanCompute" <<  engine->canComputeInteractions()
                       << "nbDrugs" << query.drugsList().count();
        }

        if (!engine->isActive() || !engine->canComputeInteractions())
            continue;

        nbInteractions += engine->calculateInteractions(query.drugsList());
        if (WarnComputations) {
            qWarning() << "Found" << nbInteractions;
        }

        if (engine->isCalculatingDrugDrugInteractions())
            result->setDDITested(true);
        if (engine->isCalculatingPatientDrugInteractions())
            result->setPDITested(true);

        result->addInteractions(engine->getAllInteractionsFound());
        result->addInteractionAlerts(engine->getAllAlerts(result));

        if (d->m_LogChrono)
            Utils::Log::logTimeElapsed(t, engine->name(), QString("calculateInteractions(): Engine %1").arg(engine->name()));
    }

    return result;
}





QString InteractionManager::drugInteractionSynthesisToHtml(const IDrug *drug, const QVector<IDrugInteraction *> &list, bool fullInfos)
{
    QVector<IDrugInteraction *> interactions;
    for(int i=0; i < list.count(); ++i) {
        IDrugInteraction *interaction = list.at(i);

        if (interaction->drugs().contains((IDrug*)drug)) {
            interactions << interaction;
        }
    }

    return synthesisToHtml(interactions, fullInfos);
}

QString InteractionManager::synthesisToHtml(const QVector<IDrugInteraction *> &list, bool fullInfos) // static
{
    Q_UNUSED(fullInfos);
    QString display;

    QVector<IDrugEngine*> engines;
    for(int i=0; i < list.count(); ++i) {
        if (!engines.contains(list.at(i)->engine()))
            engines << list.at(i)->engine();
    }

    for(int j=0; j<engines.count(); ++j) {
        IDrugEngine *eng = engines.at(j);
        QList<IDrug *> concernedDrugs;
        for(int i=0; i < list.count(); ++i) {
            IDrugInteraction *interaction = list.at(i);

            if (interaction->engine()!=eng)
                continue;

            foreach(IDrug *drg, interaction->drugs()) {
                if (!concernedDrugs.contains(drg))
                    concernedDrugs.append(drg);
            }
        }

        display.append(QString("<p><center>%1</center></p><p>").arg(eng->name()));
        for(int i = 0; i < concernedDrugs.count(); ++i) {
            IDrug *drg = concernedDrugs.at(i);
            display.append(QString("%1&nbsp;.&nbsp;%2<br />")
                           .arg(i)
                           .arg(drg->brandName()));
        }
        display.append("</p><p>");

        if (concernedDrugs.count() > 0) {
            for(int i=0; i<list.count(); ++i)
                display.append(list.at(i)->toHtml(true));
        } else {
            display = tkTr(Trans::Constants::NO_1_FOUND).arg(tkTr(Trans::Constants::INTERACTION));
        }
        display.append("</p>");
    }
    return display;
}







void InteractionManager::onNewObjectAddedToPluginManagerPool(QObject *object)
{
    IDrugEngine *engine = qobject_cast<IDrugEngine*>(object);
    if (!engine)
        return;
    d->m_Engines.append(engine);
}
