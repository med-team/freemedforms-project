/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGSBASE_PLUGIN_H
#define DRUGSBASE_PLUGIN_H

#include <extensionsystem/iplugin.h>

#include <QtCore/QObject>

/**
 * \file ./plugins/drugsbaseplugin/drugsbaseplugin.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsDB {
namespace Internal {

class DrugsBasePlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.freemedforms.FreeMedForms.DrugsBasePlugin" FILE "DrugsBase.json")

public:
    DrugsBasePlugin();
    ~DrugsBasePlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

#ifdef WITH_TESTS
private Q_SLOTS:
    void test_drugsbase_init();
    void test_drugsmodel_add_drug_pointer();
    void test_drugsio_xmloutput();
    void test_prescriptionTokens();
#endif
};

}  // namespace Internal
}  // namespace DrugsDB

#endif  // End DRUGSBASE_PLUGIN_H
