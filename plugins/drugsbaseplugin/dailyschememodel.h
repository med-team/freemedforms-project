/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DAILYSCHEMEMODEL_H
#define DAILYSCHEMEMODEL_H

#include <drugsbaseplugin/drugsbase_exporter.h>

#include <translationutils/constanttranslations.h>

#include <QString>
#include <QAbstractTableModel>

/**
 * \file ./plugins/drugsbaseplugin/dailyschememodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsDB {
namespace Internal {
//class DrugsData;
//class DrugInteraction;
//class DrugInfo;
class DailySchemeModelPrivate;
} // end namespace Internal


class DRUGSBASE_EXPORT DailySchemeModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    enum Column {
        DayReference = 0,
        Value,
        MaxParam
    };

    enum Method {
        Repeat = 0,
        Distribute
    };

    DailySchemeModel(QObject * parent = 0, const QString &serializedContent = QString::null);
    ~DailySchemeModel();

    void clear();

    void setMethod(Method method);
    Method method() const;

    QString serializedContent() const;
    void setSerializedContent(const QString &content);

    int columnCount(const QModelIndex &) const {return MaxParam;}

    int rowCount(const QModelIndex & = QModelIndex()) const {return Trans::Constants::Time::DS_MaxParam;}

    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    void setScored(bool isScored);
    void setMaximumDay(double max);
    double sum() const;

    QString humanReadableRepeatedDailyScheme() const;
    QString humanReadableDistributedDailyScheme() const;

Q_SIGNALS:
    void methodChanged();

private:
    Internal::DailySchemeModelPrivate *d;
};


} // end namespace DrugsDB

#endif // DAILYSCHEMEMODEL_H
