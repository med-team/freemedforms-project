/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "interactionsbase.h"

#include <drugsbaseplugin/drugsdata.h>
#include <drugsbaseplugin/drugsinteraction.h>
#include <drugsbaseplugin/drugsbase.h>
#include "drugsdatabaseselector.h"

#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>

#include <utils/log.h>
#include <utils/global.h>
#include <utils/database.h>

#include <medicalutils/ebmdata.h>

#include <translationutils/constanttranslations.h>

#include <QCoreApplication>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QMap>
#include <QMultiMap>
#include <QMultiHash>
#include <QSet>
#include <QFile>
#include <QDir>
#include <QCache>


using namespace DrugsDB;
using namespace Constants;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Core::ISettings *settings()  { return Core::ICore::instance()->settings(); }

namespace DrugsDB {
namespace Internal {

    const char * const SEPARATOR = "|||";

    struct AtcLabel {
        QString lang, label, code;
        int id;
    };


class InteractionsBasePrivate
{
public:
    InteractionsBasePrivate(InteractionsBase *p) :
            m_Parent(p), m_DB(0), m_LogChrono(false), m_initialized(false)
    {
        m_AtcLabelCache.setMaxCost(200);
        m_AtcCodeCache.setMaxCost(1000);
    }

    ~InteractionsBasePrivate()
    {
        if (m_DB) {
            delete m_DB;
            m_DB=0;
        }
    }


    void retrieveLinkTables()
    {
         if ((!m_AtcToMol.isEmpty()) && (!m_ClassToAtcs.isEmpty()))
             return;

         {
             QSqlDatabase drugs = QSqlDatabase::database(Constants::DB_DRUGS_NAME);
             if (!drugs.open())
                 Utils::Log::addError("InteractionBase", "Drugs database not opened", __FILE__, __LINE__);
             QSqlQuery query("SELECT * FROM `LK_MOL_ATC`", drugs);
             if (query.isActive()) {
                 while (query.next()) {
                     m_AtcToMol.insertMulti(query.value(1).toInt(), query.value(0).toInt());
                 }
             } else {
                 Utils::Log::addQueryError("InteractionBase", query, __FILE__, __LINE__);
             }
             query.finish();
         }

         QSqlDatabase DB = QSqlDatabase::database(Constants::DB_IAM_NAME);
         if (!DB.isOpen())
              DB.open();
         QString req = m_DB->select(Table_ATC_CLASS_TREE, QList<int>() << TREE_ID_CLASS << TREE_ID_ATC);

         {
             QSqlQuery query(req , DB);
             if (query.isActive()) {
                 while (query.next())
                     m_ClassToAtcs.insertMulti(query.value(0).toInt(), query.value(1).toInt());
             } else {
                 Utils::Log::addQueryError("DrugsBase", query, __FILE__, __LINE__);
             }
         }

         InteractionsBase::m_InteractionsDatabaseAvailable = m_ClassToAtcs.count() && m_AtcToMol.count();
     }


    bool checkDrugInteraction( DrugsData *drug, const QList<DrugsData *> & drugs );
    QList<DrugsInteraction *> getInteractionsFromDatabase( const int & _id1, const int & _id2 );
    QList<DrugsInteraction *> getAllInteractionsFound();

public:
    InteractionsBase *m_Parent;
    Utils::Database *m_DB;
    QMap<int, int> m_InteractionsIDs;        /*!<  All possible interactions based on ATC IDs*/
    QMultiMap<int, int> m_IamFound;               /*!< modified by checkDrugInteraction() */
    bool m_LogChrono;
    bool m_initialized;

    QMultiHash<int, int> m_AtcToMol;   /*!< Link Iam_Id to Code_Subst */
    QMultiHash<int, int> m_ClassToAtcs;   /*!< Link ClassIam_Id to Iam_Id */
    QCache<int, AtcLabel> m_AtcLabelCache;
    QCache<int, QString> m_AtcCodeCache;
};

}  // End Internal
}  // End Drugs


bool InteractionsBase::m_InteractionsDatabaseAvailable = false;

bool InteractionsBasePrivate::checkDrugInteraction( DrugsData *drug, const QList<DrugsData *> &drugsList )
{
     QTime t;
     t.start();

     if (drug->numberOfInn() == 0)
         return false;

     const QSet<int> &drug_iams = drug->allInnAndIamClasses();
     QSet<int> d_iams;
     foreach( DrugsData * drug2, drugsList ) {
          if ( drug2 == drug )
              continue;
          foreach( const int i, drug2->allInnAndIamClasses()) {
              if (d_iams.contains(i))
                  continue;
              d_iams << i;
          }
     }

     foreach( int s, d_iams ) {
         foreach( int s2, drug_iams )  {
             if ( m_InteractionsIDs.keys( s ).contains( s2 ) )
                 if ( ( ! m_IamFound.contains( s2, s ) ) && ( ! m_IamFound.contains( s, s2 ) ) )
                     m_IamFound.insertMulti( s2, s );


             if ( ( s > 999 ) && ( s2 > 999 ) && ( s == s2 ) )
                 if ( ! m_IamFound.contains( s, -1 ) )
                     m_IamFound.insertMulti( s, -1 );
         }
     }

     if (m_LogChrono)
         Utils::Log::logTimeElapsed(t, "mfDrugsBase", QString("checkDrugInteraction : %1 ; %2")
                           .arg(drug->denomination()).arg(drugsList.count()) );

     if ( m_IamFound.count() != 0 )
         return true;
     return false;
}


QList<DrugsInteraction*> InteractionsBase::calculateInteractions(const QList<DrugsData *> &drugs)
{
     QTime t;
     t.start();

     QList<DrugsInteraction *> toReturn;
     di->m_IamFound.clear();

     foreach(DrugsData *drug, drugs)
          di->checkDrugInteraction(drug, drugs);

     toReturn = di->getAllInteractionsFound();

     int id1, id2;
     foreach(DrugsInteraction* di, toReturn) {
         id1 = di->value(DrugsInteraction::DI_ATC1).toInt();
         id2 = di->value(DrugsInteraction::DI_ATC2).toInt();
         foreach(DrugsData *drg, drugs)  {
             if (drg->allInnAndIamClasses().contains(id1) || drg->allInnAndIamClasses().contains(id2)) {
                     di->addInteractingDrug(drg);
             }
         }
     }
     if (di->m_LogChrono)
         Utils::Log::logTimeElapsed(t, "InteractionsBase", QString("interactions() : %2 drugs")
                               .arg(drugs.count()) );

     return toReturn;
}


QList<DrugsInteraction *> InteractionsBasePrivate::getInteractionsFromDatabase(const int & _id1, const int & _id2)
{
    int id2 = _id2;
    QSqlDatabase DB = m_DB->database();
    QList<DrugsInteraction *> toReturn;
    if (!DB.isOpen()) {
        if (!DB.open()) {
            Utils::Log::addError("InteractionBase", tkTr(Trans::Constants::UNABLE_TO_OPEN_DATABASE_1_ERROR_2)
                                 .arg(DB.connectionName()).arg(DB.lastError().text()));
            return toReturn;
        }
    }

    if (id2 == -1) {
        DrugsInteraction *dint = 0;
        dint = new DrugsInteraction();
        dint->setValue(DrugsInteraction::DI_Type , "U" );
        dint->setValue(DrugsInteraction::DI_ATC1, _id1 );
        dint->setValue(DrugsInteraction::DI_ATC2, _id1 );
        dint->setValue(DrugsInteraction::DI_RiskFr, tkTr(Trans::Constants::INN_DUPLICATION));
        dint->setValue(DrugsInteraction::DI_RiskEn, Trans::Constants::INN_DUPLICATION);
        dint->setValue(DrugsInteraction::DI_ReferencesLink, QCoreApplication::translate("DrugsBase", "FreeDiams Interactions Engine"));
        id2 = _id1;
        toReturn << dint;
        return toReturn;
    }

    QHashWhere where;
    where.insert(IA_ATC1, QString("=%1").arg(_id1));
    where.insert(IA_ATC2, QString("=%1").arg(_id2));

    QString req = m_DB->select(Table_INTERACTIONS, where);
    QSqlQuery query(req, DB);
    if (query.isActive()) {
        while (query.next()) {
            DrugsInteraction *dint = 0;
            dint = new DrugsInteraction();
            dint->setValue(DrugsInteraction::DI_Id, query.value(IA_ID));
            dint->setValue(DrugsInteraction::DI_ATC1, query.value(IA_ATC1));
            dint->setValue(DrugsInteraction::DI_ATC2, query.value(IA_ATC2));
            dint->setValue(DrugsInteraction::DI_LinkId, query.value(IA_IAK_ID));
            toReturn << dint;
        }
    } else {
        Utils::Log::addQueryError("InteractionsBase", query, __FILE__, __LINE__);
    }
    query.finish();

    foreach(DrugsInteraction *dint, toReturn) {
        where.clear();
        where.insert(Constants::IAK_ID, QString("=%1").arg(dint->value(DrugsInteraction::DI_LinkId).toInt()));
        req = m_DB->select(Table_INTERACTION_KNOWLEDGE, where);
        if (query.exec(req)) {
            if (query.next()) {
                dint->setValue(DrugsInteraction::DI_Type, query.value(Constants::IAK_TYPE));
                dint->setValue(DrugsInteraction::DI_RiskFr, query.value(Constants::IAK_RISK_FR));
                dint->setValue(DrugsInteraction::DI_ManagementFr, query.value(Constants::IAK_MANAGEMENT_FR));
                dint->setValue(DrugsInteraction::DI_RiskEn, query.value(Constants::IAK_RISK_EN));
                dint->setValue(DrugsInteraction::DI_ManagementEn, query.value(Constants::IAK_MANAGEMENT_EN));
                dint->setValue(DrugsInteraction::DI_ReferencesLink, query.value(Constants::IAK_REFERENCES_LINK));
            }
        } else {
            Utils::Log::addQueryError("InteractionBase", query, __FILE__, __LINE__);
        }
        query.finish();
    }


    return toReturn;
}


QList<DrugsInteraction *> InteractionsBasePrivate::getAllInteractionsFound()
{
     QList<DrugsInteraction*> toReturn;
     if ( m_IamFound.isEmpty() )
          return toReturn;

     QSqlDatabase DB = QSqlDatabase::database(Constants::DB_DRUGS_NAME);
     if (!DB.isOpen())
          DB.open();

     QMap<int, int>::const_iterator i = m_IamFound.constBegin();
     while (i != m_IamFound.constEnd()) {
          toReturn << getInteractionsFromDatabase(i.key(), i.value());
          ++i;
     }
     qSort(toReturn.begin(), toReturn.end(), DrugsInteraction::greaterThan);
     return toReturn;
}



InteractionsBase::InteractionsBase()
        : di(0)
{
    di = new InteractionsBasePrivate(this);
    di->m_DB = new Utils::Database();

    di->m_DB->addTable(Table_INTERACTIONS, "INTERACTIONS");
    di->m_DB->addTable(Table_INTERACTION_KNOWLEDGE, "INTERACTION_KNOWLEDGE");
    di->m_DB->addTable(Table_ATC, "ATC");
    di->m_DB->addTable(Table_ATC_CLASS_TREE, "ATC_CLASS_TREE");
    di->m_DB->addTable(Table_SOURCES, "SOURCES");

    di->m_DB->addField(Table_ATC, ATC_ID,    "ID");
    di->m_DB->addField(Table_ATC, ATC_CODE,  "CODE");
    di->m_DB->addField(Table_ATC, ATC_EN,    "ENGLISH");
    di->m_DB->addField(Table_ATC, ATC_FR,    "FRENCH");
    di->m_DB->addField(Table_ATC, ATC_DE,    "DEUTSCH");

    di->m_DB->addField(Table_INTERACTIONS, IA_ID,     "ID");
    di->m_DB->addField(Table_INTERACTIONS, IA_ATC1,   "ATC_ID1");
    di->m_DB->addField(Table_INTERACTIONS, IA_ATC2,   "ATC_ID2");
    di->m_DB->addField(Table_INTERACTIONS, IA_IAK_ID, "INTERACTION_KNOWLEDGE_ID");

    di->m_DB->addField(Table_INTERACTION_KNOWLEDGE, IAK_ID,    "ID");
    di->m_DB->addField(Table_INTERACTION_KNOWLEDGE, IAK_TYPE,    "TYPE");
    di->m_DB->addField(Table_INTERACTION_KNOWLEDGE, IAK_RISK_FR,    "RISK_FR");
    di->m_DB->addField(Table_INTERACTION_KNOWLEDGE, IAK_MANAGEMENT_FR,    "MANAGEMENT_FR");
    di->m_DB->addField(Table_INTERACTION_KNOWLEDGE, IAK_RISK_EN,    "RISK_EN");
    di->m_DB->addField(Table_INTERACTION_KNOWLEDGE, IAK_MANAGEMENT_EN,    "MANAGEMENT_EN");
    di->m_DB->addField(Table_INTERACTION_KNOWLEDGE, IAK_REFERENCES_LINK,    "REFERENCES_LINK");


    di->m_DB->addField(Table_ATC_CLASS_TREE, TREE_ID_CLASS,    "ID_CLASS");
    di->m_DB->addField(Table_ATC_CLASS_TREE, TREE_ID_ATC,      "ID_ATC");
    di->m_DB->addField(Table_ATC_CLASS_TREE, TREE_SOURCE_LINK, "SOURCE_LINK");

    di->m_DB->addField(Table_SOURCES, SOURCES_ID, "ID");
    di->m_DB->addField(Table_SOURCES, SOURCES_SOURCE_LINK, "SOURCE_LINK");
    di->m_DB->addField(Table_SOURCES, SOURCES_TYPE, "TYPE");
    di->m_DB->addField(Table_SOURCES, SOURCES_LINK, "LINK");
    di->m_DB->addField(Table_SOURCES, SOURCES_TEXTUAL_REFERENCE, "TEXTUAL_REFERENCE");
    di->m_DB->addField(Table_SOURCES, SOURCES_ABSTRACT, "ABSTRACT");
    di->m_DB->addField(Table_SOURCES, SOURCES_EXPLANATION, "EXPLANATION");
}

InteractionsBase::~InteractionsBase()
{
    if (di) {
        delete di;
        di=0;
    }
}

bool InteractionsBase::init(bool refreshCache)
{
    if (di->m_initialized && !refreshCache)
        return true;

    QString pathToDb = "";

    if (Utils::isRunningOnMac())
        pathToDb = settings()->databasePath() + QDir::separator() + QString(Constants::DB_DRUGS_NAME);
    else
        pathToDb = settings()->databasePath() + QDir::separator() + QString(Constants::DB_DRUGS_NAME);

    di->m_DB->createConnection(DB_IAM_NAME, DB_IAM_FILENAME, pathToDb,
                               Utils::Database::ReadOnly, Utils::Database::SQLite);


    if (!di->m_DB->database().isOpen())
        if (!di->m_DB->database().open())
            Utils::Log::addError("InteractionsBase", QString("Unable to open database. Error : %1").arg(di->m_DB->database().lastError().text()), __FILE__, __LINE__);

    QList<int> fields;
    fields << IA_ATC1 << IA_ATC2;
    QString req = di->m_DB->select(Table_INTERACTIONS, fields);
    QSqlQuery q(req , di->m_DB->database());
    if (q.isActive())
        while (q.next())
            di->m_InteractionsIDs.insertMulti(q.value(0).toInt(), q.value(1).toInt());

    if (refreshCache) {
        di->m_AtcToMol.clear();
    }
    di->retrieveLinkTables();

    di->m_initialized = true;
    return true;
}

bool InteractionsBase::isInitialized() const
{
     return di->m_initialized;
}


void InteractionsBase::logChronos(bool state)
{
    di->m_LogChrono = state;
}

QString InteractionsBase::iamTable(const int ref) const
{
    return di->m_DB->table(ref);
}

QString InteractionsBase::getIamWhereClause(const int &tableref, const QHash<int, QString> &conditions) const
{
    return di->m_DB->getWhereClause(tableref, conditions);
}

QString InteractionsBase::selectInteractionsSql(const int &tableref, const QList<int> &fieldsref, const QHash<int, QString> &conditions) const
{
    return di->m_DB->select(tableref, fieldsref, conditions);
}

























QVector<MedicalUtils::EbmData *> InteractionsBase::getAllSourcesFromTree(const QList<int> &allInnAndIamClassIds)
{
    QVector<MedicalUtils::EbmData *> ret;
    if (allInnAndIamClassIds.count() == 0)
        return ret;

    QStringList classIds, innIds;
    foreach(int id, allInnAndIamClassIds) {
        if (id >= 200000)
            classIds << QString::number(id);
        else
            innIds <<QString::number(id);
    }

    QString req = QString("%1, %2 WHERE "
                          "`%2`.`%3` IN (%5) AND `%2`.`%4` IN (%6) AND %7")
            .arg(di->m_DB->select(Table_SOURCES))
            .arg(di->m_DB->table(Table_ATC_CLASS_TREE))
            .arg(di->m_DB->fieldName(Table_ATC_CLASS_TREE, TREE_ID_CLASS))
            .arg(di->m_DB->fieldName(Table_ATC_CLASS_TREE, TREE_ID_ATC))
            .arg(classIds.join(","))
            .arg(innIds.join(","))
            .arg(di->m_DB->fieldEquality(Table_ATC_CLASS_TREE, TREE_SOURCE_LINK,
                                         Table_SOURCES, SOURCES_SOURCE_LINK))
            ;

    QStringList links;
    QSqlQuery query(req, di->m_DB->database());
    if (query.isActive()) {
        while (query.next()) {
            if (links.contains(query.value(SOURCES_LINK).toString()))
                continue;
            links << query.value(SOURCES_LINK).toString();
            MedicalUtils::EbmData *ebm = new MedicalUtils::EbmData;
            ebm->setId(query.value(SOURCES_ID));
            ebm->setLink(query.value(SOURCES_LINK).toString());
            ebm->setReferences(query.value(SOURCES_TEXTUAL_REFERENCE).toString());
            ebm->setAbstract(query.value(SOURCES_ABSTRACT).toString());
            ret << ebm;
        }
    } else {
        Utils::Log::addQueryError("InteractionBase", query, __FILE__, __LINE__);
    }

    return ret;
}
