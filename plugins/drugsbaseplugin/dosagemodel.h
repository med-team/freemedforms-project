/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DOSAGEMODEL_H
#define DOSAGEMODEL_H

#include <drugsbaseplugin/drugsbase_exporter.h>
#include <drugsbaseplugin/constants.h>

#include <QObject>
#include <QSqlTableModel>
#include <QSet>

/**
 * \file ./plugins/drugsbaseplugin/dosagemodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsDB {
class DrugsModel;

namespace Internal {

class DRUGSBASE_EXPORT DosageModel : public QSqlTableModel
{
    Q_OBJECT

    enum ScoredTablet
    {
        CompletTablet = 0,
        HalfTablet,
        QuaterTablet
    };

    enum PreDeterminedForms
    {
        DosePerKilo=0,
        SpoonReference,     // cuillère-mesure
        Spoon2_5ml,
        Spoon5ml,
        Puffs,              // bouffées
        Dose,
        Mouthwash,          // bain de bouche
        Inhalation,
        Application,
        Washing,            // lavement
        EyeWash,            // lavage oculaire
        Instillation
    };

public:
    explicit DosageModel(DrugsDB::DrugsModel *parent = 0);

    virtual int columnCount(const QModelIndex & = QModelIndex()) const { return Dosages::Constants::MaxParam; }
    virtual bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);
    virtual QVariant data(const QModelIndex & item, int role = Qt::DisplayRole) const;
    virtual bool insertRows(int row, int count, const QModelIndex & parent = QModelIndex());
    virtual bool removeRows(int row, int count, const QModelIndex & parent = QModelIndex());
    virtual void revertRow(int row);

    void setTable(const QString &) {}

    virtual bool setDrugId(const QVariant &drugId);
    QVariant drugId();

    QStringList isDosageValid(const int row);
    bool isDirty(const int row) const;

    QString toXml(const int row);
    bool addFromXml(const QString &xml);
    void toPrescription(const int row);

#ifdef FREEDIAMS
    bool userCanRead()   { return true; }
    bool userCanWrite()  { return true; }
#else
    bool userCanRead();
    bool userCanWrite();
#endif

public Q_SLOTS:
    bool submitAll();

public:
    //--------------------------------------------------------------------------------------------------------
    //---------------------------------------- STATIC MEMBERS ------------------------------------------------
    //--------------------------------------------------------------------------------------------------------
    // static viewers to use for ui generation
    static void         initStaticData()      { retranslate() ; }
    static int          periodDefault()        { return 4; }
    static QStringList  scoredTabletScheme();
    static QStringList  predeterminedForms();

    // non static viewers for ui generation (need to be instanciate first)
    QStringList        forms();

    // Debugging Information
    void warn(const int row = -1);

private Q_SLOTS:
    void changeEvent(QEvent * event);
    static void retranslate();

    //--------------------------------------------------------------------------------------------------------
    //----------------------------------------- PRIVATE DATA -------------------------------------------------
    //--------------------------------------------------------------------------------------------------------
private:
    DrugsDB::DrugsModel *m_DrugsModel;
    static QStringList   m_ScoredTabletScheme;
    static QStringList   m_PreDeterminedForms;
    static QString       m_ActualLangage;
    QVariant m_UID;
    QSet<int> m_DirtyRows;
    QSet<int> m_DirtyInnLkRows;
    QHash<int, int> _refills;
    mutable QString m_Route;
};

}  // End Internal
}  // End DrugsDB

#endif // DOSAGEMODEL_H
