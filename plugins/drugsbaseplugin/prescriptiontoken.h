/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGSDB_PRESCRIPTIONTOKEN_H
#define DRUGSDB_PRESCRIPTIONTOKEN_H

#ifdef WITH_PAD
#include <drugsbaseplugin/drugsbase_exporter.h>
#include <coreplugin/ipadtools.h>
#include <QPointer>

/**
 * \file ./plugins/drugsbaseplugin/prescriptiontoken.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsDB {
class DrugsModel;

class DRUGSBASE_EXPORT PrescriptionToken : public Core::IToken
{
public:
    PrescriptionToken(const QString &name, const int ref);
    ~PrescriptionToken();

    static void setPrescriptionModel(DrugsDB::DrugsModel *model) {_model = model;}
    static void setPrescriptionModelRow(int row) {_row = row;}

    QVariant testValue() const;
    QVariant value() const;

private:
    static QPointer<DrugsDB::DrugsModel> _model;
    static int _row;
    int _ref;
    bool _isRepeatedDailyScheme, _isDistributedDailyScheme, _isMeal;
};

} // namespace DrugsDB

#endif // WITH_PAD

#endif // DRUGSDB_PRESCRIPTIONTOKEN_H
