/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGINTERACTIONQUERY_H
#define DRUGINTERACTIONQUERY_H

#include <drugsbaseplugin/drugsbase_exporter.h>

#include <coreplugin/itheme.h>

#include <QObject>
#include <QIcon>
#include <QStandardItemModel>
#include <QPointer>

/**
 * \file ./plugins/drugsbaseplugin/druginteractionquery.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsDB {
class IDrug;
class IDrugInteraction;

class DRUGSBASE_EXPORT DrugInteractionQuery : public QObject
{
    Q_OBJECT
public:
    DrugInteractionQuery(const QVector<IDrug *> &testDrugs, QObject *parent = 0);
    DrugInteractionQuery(QObject *parent = 0);
    ~DrugInteractionQuery();

    void clearDrugsList();
    void setDrugsList(const QVector<IDrug *> &list);
    QVector<IDrug *> drugsList() const {return m_Drugs;}

    void addDrug(IDrug *drug);
    void removeDrug(IDrug *drug);
    void removeLastInsertedDrug();

    bool containsDrug(const IDrug *drug) const;
    QStandardItemModel *toStandardModel() const;

    void setTestDrugDrugInteractions(bool test) {m_TestDDI = test;}
    void setTestPatientDrugInteractions(bool test) {m_TestPDI = test;}

    QString warnText() const;

private:
    QVector<IDrug *> m_Drugs;
    bool m_TestDDI, m_TestPDI;
    mutable QPointer<QStandardItemModel> m_StandardModel;
};


}  // End namespace DrugsDB


DRUGSBASE_EXPORT QDebug operator<<(QDebug dbg, const DrugsDB::DrugInteractionQuery *c);
DRUGSBASE_EXPORT QDebug operator<<(QDebug dbg, const DrugsDB::DrugInteractionQuery &c);

#endif // DRUGINTERACTIONRESULT_H
