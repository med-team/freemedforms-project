/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGSTEMPLATEPRINTER_H
#define DRUGSTEMPLATEPRINTER_H

#include <drugsbaseplugin/drugsbase_exporter.h>
#include <templatesplugin/itemplateprinter.h>

namespace DrugsDB {
class DrugsModel;

namespace Internal {

class DRUGSBASE_EXPORT DrugsTemplatePrinter : public Templates::ITemplatePrinter
{
    Q_OBJECT
public:
    DrugsTemplatePrinter(QObject *parent) : Templates::ITemplatePrinter(parent) {}
    ~ DrugsTemplatePrinter() {}

    QString mimeType() const;
    bool printTemplates(const QList<const Templates::ITemplate *> iTemplates) const;
};

}  // End namespace Internal
}  // End namespace DrugsDB

#endif // DRUGSTEMPLATEPRINTER_H
