/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGSDB_IDRUGINTERACTION_H
#define DRUGSDB_IDRUGINTERACTION_H

#include <drugsbaseplugin/drugsbase_exporter.h>
#include <drugsbaseplugin/idrug.h>

/**
 * \file ./plugins/drugsbaseplugin/idruginteraction.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsDB {
class IDrugEngine;
namespace Internal {
class IDrugInteractionPrivate;
}

class DRUGSBASE_EXPORT IDrugInteraction
{
public:
    explicit IDrugInteraction(IDrugEngine *) {}
    virtual ~IDrugInteraction() {}

    virtual IDrugEngine *engine() const = 0;

    virtual bool isDrugDrugInteraction() const = 0;
    virtual bool isPotentiallyInappropriate() const = 0;

    virtual QString type() const = 0;

    virtual QList<IDrug *> drugs() const = 0;

    virtual QIcon icon(const int levelOfWarning = 0, const int iconsize = 0) const = 0;
    virtual QString header(const QString &separator = QString::null) const = 0;
    virtual QString risk(const QString &lang = QString::null) const = 0;
    virtual QString management(const QString &lang = QString::null) const = 0;
    virtual QString referencesLink(const QString &lang = QString::null) const = 0;

    virtual QString toHtml(bool detailled = false) const = 0;

    virtual int sortIndex() const = 0;

    /**
      \brief List sorting helpers.
      This sort helpers uses the sortIndex()
    */
    static bool lessThan(const IDrugInteraction *int1, const IDrugInteraction *int2) {return int1->sortIndex() < int2->sortIndex();}

    /**
      \brief List sorting helpers.
      This sort helpers uses the sortIndex()
    */
    static bool greaterThan(const IDrugInteraction *int1, const IDrugInteraction *int2) {return int1->sortIndex() > int2->sortIndex();}
};

}  // End DrugsDB

DRUGSBASE_EXPORT QDebug operator<<(QDebug dbg, const DrugsDB::IDrugInteraction &c);
DRUGSBASE_EXPORT QDebug operator<<(QDebug dbg, const DrugsDB::IDrugInteraction *c);

#endif  // DRUGSDB_IDRUGINTERACTION_H

