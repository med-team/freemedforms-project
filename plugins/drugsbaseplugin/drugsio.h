/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGSIO_H
#define DRUGSIO_H

#include <drugsbaseplugin/drugsbase_exporter.h>

#include <QObject>
#include <QHash>
#include <QString>

/**
 * \file ./plugins/drugsbaseplugin/drugsio.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsDB {
class DrugBaseCore;
class DrugsModel;
namespace Internal {
class DrugsData;
class DrugsIOPrivate;
}

class DRUGSBASE_EXPORT DrugsIO : public QObject
{
    Q_OBJECT
    friend class DrugsDB::DrugBaseCore;

protected:
    DrugsIO(QObject *parent = 0);
    bool initialize();

public:
    enum Loader {
        AppendPrescription,
        ReplacePrescription
    };
    enum HtmlVersion {
        MedinTuxVersion = 0,
        NormalVersion,
        SimpleVersion,
        DrugsOnlyVersion
    };

    ~DrugsIO();
    bool isInitialized() const;

    bool startsDosageTransmission();
    bool isSendingDosage();
    bool prescriptionFromXml(DrugsDB::DrugsModel *model, const QString &xml, Loader loader = ReplacePrescription);

    bool loadPrescription(DrugsDB::DrugsModel *model, const QString &fileName, QHash<QString,QString> &extraData, Loader loader = ReplacePrescription);
    bool loadPrescription(DrugsDB::DrugsModel *model, const QString &fileName, Loader loader = ReplacePrescription);
    bool loadPrescription(DrugsDB::DrugsModel *model, const QString &fileName, QString &xmlExtraData, Loader loader = ReplacePrescription);

    bool savePrescription(DrugsDB::DrugsModel *model, const QHash<QString,QString> &extraData, const QString &toFileName = QString::null);
    bool savePrescription(DrugsDB::DrugsModel *model, const QString &extraData, const QString &toFileName = QString::null);

    QString prescriptionToXml(DrugsDB::DrugsModel *model, const QString &xmlExtraData = QString::null);
    QString getDrugPrescription(DrugsDB::DrugsModel *model, const int drugRow, bool toHtml = false, const QString &mask = QString::null);

    static QStringList prescriptionMimeTypes();

private Q_SLOTS:
    void dosageTransmissionDone();

Q_SIGNALS:
    void transmissionDone();

private:
//    static DrugsIO *m_Instance;
    Internal::DrugsIOPrivate *d;
};


}  // End DrugsDB

#endif // DRUGSIO_H
