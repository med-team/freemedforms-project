/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "drugsbaseplugin.h"
#include "drugstemplateprinter.h"
#include "drugbasecore.h"
#include "drugsbase.h"
#include "drugsdatabaseselector.h"

#include <utils/log.h>
#include <utils/global.h>

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/isettings.h>
#include <coreplugin/translators.h>

#include <QtCore/QtPlugin>

using namespace DrugsDB::Internal;

static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline Core::ISettings *settings()  { return Core::ICore::instance()->settings(); }
static inline DrugsDB::DrugsBase &drugsBase() {return DrugsDB::DrugBaseCore::instance().drugsBase();}
static inline void messageSplash(const QString &s) {theme()->messageSplashScreen(s); }

DrugsBasePlugin::DrugsBasePlugin() :
    IPlugin()
{
    if (Utils::Log::debugPluginsCreation()) {
#ifdef FREEDIAMS
        qWarning() << "creating FREEDIAMS::DrugsBasePlugin";
#else
        qWarning() << "creating FREEMEDFORMS::DrugsBasePlugin";
#endif
    }

    Core::ICore::instance()->translators()->addNewTranslator("plugin_drugsbase");

    new DrugsDB::DrugBaseCore(this);
}

DrugsBasePlugin::~DrugsBasePlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool DrugsBasePlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "DrugsBasePlugin::initialize";
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);

    messageSplash(tr("Initializing drugs database plugin..."));
    DrugsDB::DrugBaseCore::instance().initialize();

    return true;
}

void DrugsBasePlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "DrugsBasePlugin::extensionsInitialized";

    messageSplash(tr("Initializing drugs database plugin..."));

    if (!drugsBase().checkDatabaseVersion()) {
        Utils::warningMessageBox(tr("Wrong drugs database installed"),
                                 tr("A wrong drugs datbase is installed on your computer. This can "
                                    "be the result of an application updating. You have to open to the "
                                    "datapack manager and update or remove the installed drugs database. \n"
                                    "Please find more documentation on the website %1.")
                                 .arg(settings()->path(Core::ISettings::WebSiteUrl)));
    }

    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));
    addAutoReleasedObject(new DrugsDB::Internal::DrugsTemplatePrinter(this));
}

ExtensionSystem::IPlugin::ShutdownFlag DrugsBasePlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(DrugsBasePlugin)
