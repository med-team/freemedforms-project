/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef GLOBALDRUGSMODEL_H
#define GLOBALDRUGSMODEL_H

#include <drugsbaseplugin/drugsbase_exporter.h>

#include <QSqlQueryModel>
#include <QObject>
class QStandardItemModel;

/**
 * \file ./plugins/drugsbaseplugin/globaldrugsmodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsDB {
class IDrug;

namespace Internal {
class GlobalDrugsModelPrivate;
}  // end namespace Internal


class DRUGSBASE_EXPORT GlobalDrugsModel : public QSqlQueryModel
{
    Q_OBJECT
    friend class Internal::GlobalDrugsModelPrivate;

public:
    enum DataRepresentation {
        BrandName = 0,
        Strength,
        Routes,
        Forms,
        Marketed,
        DrugId,
        ColumnCount
    };

    enum SearchMode {
        SearchByBrandName = 0,
        SearchByMolecularName,
        SearchByInnName
    };

    GlobalDrugsModel(const SearchMode searchMode = SearchByBrandName, QObject * parent = 0);
    ~GlobalDrugsModel();

    void setSearchMode(const int searchMode);

    static void updateAvailableDosages();

//    static QStandardItemModel *drugsPrecautionsModel();

    int columnCount(const QModelIndex & = QModelIndex()) const { return ColumnCount; }

    bool setData(const QModelIndex &, const QVariant &, int = Qt::EditRole) { return false; }
    QVariant data(const QModelIndex & item, int role = Qt::DisplayRole) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    void setFilter(const QString &searchFor);

public Q_SLOTS:
    void updateCachedAvailableDosage();
    void onDrugsDatabaseChanged();

private Q_SLOTS:
    void updateAllergies();

private:
    Internal::GlobalDrugsModelPrivate *d;
};


}  // end namespace DrugsDB


#endif // GLOBALDRUGSMODEL_H
