/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ATCTREEMODEL_H
#define ATCTREEMODEL_H

#include <drugsbaseplugin/drugsbase_exporter.h>

#include <QAbstractItemModel>

/**
 * \file ./plugins/drugsbaseplugin/atctreemodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/


namespace DrugsDB {
namespace Internal {
class AtcTreeModelPrivate;
}  // end namespace Internal


class DRUGSBASE_EXPORT AtcTreeModel : public QAbstractItemModel
{
    Q_OBJECT
    friend class Internal::AtcTreeModelPrivate;

public:
    enum DataRepresentation {
        ATC_Label = 0,
        ATC_Code
    };

    AtcTreeModel(QObject * parent = 0);
    ~AtcTreeModel();

public Q_SLOTS:
    void init();

public:
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &index) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    bool setData(const QModelIndex &, const QVariant &, int) {return false;}
    QVariant data(const QModelIndex & item, int role = Qt::DisplayRole) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

private Q_SLOTS:
    void onDrugsBaseChanged();

private:
    Internal::AtcTreeModelPrivate *d;
};

}  // end namespace DrugsDB

#endif // ATCTREEMODEL_H
