/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGSDB_DRUGSDATABASESELECTOR_H
#define DRUGSDB_DRUGSDATABASESELECTOR_H

#include <drugsbaseplugin/drugsbase_exporter.h>

#include <QString>
#include <QDate>
#include <QStringList>
#include <QHash>

QT_BEGIN_NAMESPACE
class QTreeWidget;
QT_END_NAMESPACE

namespace DrugsDB {
class DrugsBase;

namespace Internal {
//class DatabaseInfosPrivate;
class DrugsDatabaseSelectorPrivate;
}

class DRUGSBASE_EXPORT DatabaseInfos
{
    friend class DrugsDB::DrugsBase;

public:
    DatabaseInfos();

    QString translatedName() const;

    void setDrugsNameConstructor(const QString &s);
    void toTreeWidget(QTreeWidget *tree) const;

    QString identifier, fileName, version, compatVersion, lang_country, connectionName;
    QString provider, author, copyright, license, drugsUidName, packUidName;
    QString drugsNameConstructor,  drugsNameConstructorSearchFilter;
    QString weblink, complementaryWebsite, authorComments, licenseTerms;
    bool atcCompatible, iamCompatible;
    QDate date;
    int moleculeLinkCompletion, sid;
    QHash<QString, QString> names;
};


class DRUGSBASE_EXPORT DrugsDatabaseSelector
{
    DrugsDatabaseSelector();
public:
    static DrugsDatabaseSelector *instance();
    ~DrugsDatabaseSelector();

    void getAllDatabaseInformation() const;
    bool setCurrentDatabase(const QString &dbUid);
    DatabaseInfos currentDatabase() const;
    QVector<DatabaseInfos *> availableDatabases() const;

private:
    static DrugsDatabaseSelector *m_Instance;
    Internal::DrugsDatabaseSelectorPrivate *d;
};



//class DRUGSBASE_EXPORT DrugsDatabaseInfoModel : public QAbstractItemModel
//{
//    Q_OBJECT
//public:
//    DrugsDatabaseInfoModel(QObject *parent = 0);
//    ~DrugsDatabaseInfoModel();
//
//    int columnCount(const QModelIndex &parent = QModelIndex()) const;
//    int	rowCount (const QModelIndex &parent = QModelIndex()) const;
//
//    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
//    Qt::ItemFlags flags(const QModelIndex &index) const;
//
//private:
//
//};

DRUGSBASE_EXPORT QDebug operator<<(QDebug dbg, const DrugsDB::DatabaseInfos &c);
DRUGSBASE_EXPORT QDebug operator<<(QDebug dbg, const DrugsDB::DatabaseInfos *c);

}  // End namespace DrugsDB

#endif // DRUGSDB_DRUGSDATABASESELECTOR_H
