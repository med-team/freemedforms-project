/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGSBASE_INTERNAL_PRESCRIPTIONPRINTER_H
#define DRUGSBASE_INTERNAL_PRESCRIPTIONPRINTER_H

#include <drugsbaseplugin/drugsbase_exporter.h>
#include <QObject>
#include <QString>

/**
 * \file ./plugins/drugsbaseplugin/prescriptionprinter.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsDB {
class DrugsModel;
namespace Internal {
class PrescriptionPrinterPrivate;
class PrescriptionPrinterJobPrivate;
} // namespace Internal

class DRUGSBASE_EXPORT PrescriptionPrinterJob
{
public:
    enum OutputType {
        FullHtml = 0,
        Simplified,
        DrugsNameOnly
    };

    // enum DrugLine {
    //     HtmlOrderedList = 0,    // uses <ol><li></li></ol>
    //     HtmlDiv                 // uses <div style="prescriptionLine">...</div>
    // };

    PrescriptionPrinterJob();
    ~PrescriptionPrinterJob();
    void readSettings();

    QString uuid() const;

    void setVersion(OutputType type);
    OutputType outputType() const;

    // TODO: add more options for the user : select papers, print duplicatas...

    bool sortBeforePrinting() const;
    bool addLineBreakBetweenEachDrugs() const;
    bool printDuplicates() const;

    bool addPatientBiometrics() const;
    QString patientBiometricsToHtml() const;

    void addXmlExtraData(const QString &xmlExtraData);
    QString xmlExtraData() const;

    void setDrugsModel(DrugsModel *drugsModel);
    DrugsModel *drugsModel() const;

private:
    Internal::PrescriptionPrinterJobPrivate *d;
};

class DRUGSBASE_EXPORT PrescriptionPrinter : public QObject
{
    Q_OBJECT
    
public:
    explicit PrescriptionPrinter(QObject *parent = 0);
    ~PrescriptionPrinter();
    bool initialize();
    
    QString prescriptionToHtml(DrugsModel *model);
    QString prescriptionToHtml(const PrescriptionPrinterJob &job);

    bool print(DrugsModel *model);
    bool print(const PrescriptionPrinterJob &job);
    
    void printPreview(DrugsDB::DrugsModel *model);

public Q_SLOTS:
    
private:
    Internal::PrescriptionPrinterPrivate *d;
};

} // namespace DrugsDB

#endif // DRUGSBASE_INTERNAL_PRESCRIPTIONPRINTER_H

