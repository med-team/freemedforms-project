/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef INTERACTIONSMANAGER_H
#define INTERACTIONSMANAGER_H

#include <drugsbaseplugin/drugsbase_exporter.h>

#include <QVector>
#include <QIcon>

class QTreeWidget;


/**
 * \file ./plugins/drugsbaseplugin/interactionmanager.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsDB {
class DrugBaseCore;
class IDrug;
class IDrugInteraction;
class DrugInteractionResult;
class DrugInteractionQuery;

namespace Internal {
class InteractionManagerPrivate;
}  // End Internal


class DRUGSBASE_EXPORT InteractionManager : public QObject
{
    Q_OBJECT
    friend class DrugsDB::DrugBaseCore;

protected:
    InteractionManager(QObject *parent = 0);

public:
    ~InteractionManager();

    DrugInteractionResult *checkInteractions(const DrugInteractionQuery &query, QObject *parent);

//    static QIcon interactionIcon(const int level, const int levelOfWarning = 0, bool medium = false);
//    QIcon iamIcon(const IDrug *drug, const int &levelOfWarning = 0, bool medium = false) const;
//    static QString listToHtml(const QVector<IDrugInteraction *> &list, bool fullInfos);
    static QString drugInteractionSynthesisToHtml(const IDrug *drug, const QVector<IDrugInteraction *> &list, bool fullInfos);
    static QString synthesisToHtml(const QVector<IDrugInteraction *> &list, bool fullInfos);
//    static void synthesisToTreeWidget(const QList<IDrugInteraction *> &list, QTreeWidget *tree);

private Q_SLOTS:
    void onNewObjectAddedToPluginManagerPool(QObject *object);

private:
    Internal::InteractionManagerPrivate *d;
};

}  // End DrugsDB

#endif   // MFINTERACTIONSMANAGER_H
