/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGINTERACTIONINFORMATIONQUERY_H
#define DRUGINTERACTIONINFORMATIONQUERY_H

#include <drugsbaseplugin/drugsbase_exporter.h>

#include <QString>

namespace DrugsDB {
class DrugInteractionResult;
class IDrug;

struct DRUGSBASE_EXPORT DrugInteractionInformationQuery
{
    enum ProcessTime {
        BeforePrescription = 0,
        DuringPrescription,
        AfterPrescription,
        BeforePrinting
    };

    enum MessageType {
        DetailledAlert = 0,
        InformationAlert,
        ShortToolTip,
        DetailledToolTip
    };

    enum IconSize {
        SmallSize = 0,
        MediumSize,
        BigSize
    };

    DrugInteractionInformationQuery(const DrugInteractionInformationQuery &query);
    DrugInteractionInformationQuery();

    ~DrugInteractionInformationQuery();

    int messageType;
    int processTime;
    int iconSize;
    int levelOfWarningStaticAlert, levelOfWarningDynamicAlert;
    QString engineUid;
    DrugsDB::DrugInteractionResult *result;
    DrugsDB::IDrug *relatedDrug;
};

}  // End namespace DrugsDB

#endif // DRUGINTERACTIONINFORMATIONQUERY_H
