/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGSBASE_PROTOCOLSBASE_H
#define DRUGSBASE_PROTOCOLSBASE_H

#include <drugsbaseplugin/drugsbase_exporter.h>
#include <utils/database.h>

#include <QMultiHash>
#include <QString>
#include <QList>
#include <QVariant>

/**
 * \file ./plugins/drugsbaseplugin/protocolsbase.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsDB {
class DrugBaseCore;

namespace Internal {
class ProtocolsBasePrivate;
}

class DRUGSBASE_EXPORT ProtocolsBase : public QObject, public Utils::Database
{
    Q_OBJECT
    friend class DrugsDB::DrugBaseCore;

protected:
    explicit ProtocolsBase(QObject *parent = 0);
    void forceReinitialization();
    bool initialize();

private Q_SLOTS:
    void onCoreFirstRunCreationRequested();

public:
    ~ProtocolsBase();
    bool isInitialized() const;

    QString dosageCreateTableSqlQuery();
    void checkDosageDatabaseVersion();

    QHash<QString, QString> getDosageToTransmit();
    bool markAllDosageTransmitted(const QStringList &dosageUuids);

    QList<QVariant> getAllUIDThatHaveRecordedDosages() const;
    QMultiHash<int,QString> getAllINNThatHaveRecordedDosages() const;

private:
    bool createDatabase(const QString & connectionName , const QString & dbName,
                        const QString & pathOrHostName,
                        TypeOfAccess access, AvailableDrivers driver,
                        const QString & /*login*/, const QString & /*pass*/,
                        const int /*port*/,
                        CreationOption /*createOption*/
                       );
    bool onCoreDatabaseServerChanged();

Q_SIGNALS:
    void protocolsBaseIsAboutToChange();
    void protocolsBaseHasChanged();

private:
    Internal::ProtocolsBasePrivate *d;
};

}  // End namespace DrugsDB

#endif // DRUGSBASE_PROTOCOLSBASE_H
