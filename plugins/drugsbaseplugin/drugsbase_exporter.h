/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#ifndef DRUGSBASE_EXPORTER_H
#define DRUGSBASE_EXPORTER_H

#include <qglobal.h>

#if defined(DRUGSBASE_LIBRARY)
#define DRUGSBASE_EXPORT Q_DECL_EXPORT
#else
#define DRUGSBASE_EXPORT Q_DECL_IMPORT
#endif

#endif  // DRUGSBASE_EXPORTER_H
