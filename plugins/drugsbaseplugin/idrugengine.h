/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGSDB_IDRUGENGINE_H
#define DRUGSDB_IDRUGENGINE_H

#include <drugsbaseplugin/drugsbase_exporter.h>

#include <QObject>
#include <QIcon>
#include <QStandardItemModel>

/**
 * \file ./plugins/drugsbaseplugin/idrugengine.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugsDB {
class IDrugInteraction;
class IDrugInteractionAlert;
class DrugInteractionResult;
class IDrug;

class DRUGSBASE_EXPORT IDrugEngine : public QObject
{
    Q_OBJECT

public:
    IDrugEngine(QObject *parent) : QObject(parent), m_IsActive(true) {}
    virtual ~IDrugEngine() {}

    virtual bool init() = 0;
    virtual bool isActive() const {return m_IsActive;}
    virtual bool isActiveByDefault() const = 0;
    virtual bool canComputeInteractions() const = 0;

    virtual bool isCalculatingDrugDrugInteractions() const = 0;
    virtual bool isCalculatingPatientDrugInteractions() const = 0;
    virtual bool isCalculatingPatientDrugAllergiesAndIntolerances() const = 0;

    virtual QString uid() const = 0;
    virtual QString name() const = 0;
    virtual QString shortName() const = 0;
    virtual QString tooltip() const = 0;
    virtual QIcon icon(const int size = 0) const = 0;
    virtual QString iconFullPath(const int size = 0) const = 0;

    virtual int calculateInteractions(const QVector<IDrug *> &drugs) = 0;
    virtual QVector<IDrugInteraction *> getAllInteractionsFound() = 0;
    virtual QVector<IDrugInteractionAlert *> getAllAlerts(DrugInteractionResult *addToResult) = 0;

    virtual QAbstractItemModel *precautionModel() const = 0;

    virtual QString engineDataReport() const {return QString();}

public Q_SLOTS:
    virtual void setActive(bool state) {m_IsActive = state;}

protected:
    bool m_IsActive;
};

class DRUGSBASE_EXPORT IDrugAllergyEngine : public IDrugEngine
{
    Q_OBJECT

public:
    enum TypeOfInteraction {
        NoInteraction = 0,
        Intolerance,
        SuspectedIntolerance,
        Allergy,
        SuspectedAllergy
    };

    enum TypeOfSubstrat {
        InnCode = 0,
        ClassInn,
        Drug
    };
    /** Construct an active empty drug engine*/
    IDrugAllergyEngine(QObject *parent) : IDrugEngine(parent) {}
    virtual ~IDrugAllergyEngine() {}

    bool isCalculatingPatientDrugAllergiesAndIntolerances() const {return true;}

    virtual void check(const int typeOfInteraction, const QString &uid, const QString &drugGlobalAtcCode = QString::null, const int drugsDatabaseSourceId = -1) = 0;
    virtual bool has(const int typeOfInteraction, const QString &uid, const int drugsDatabaseSourceId = -1) = 0;

    virtual QStandardItemModel *drugPrecautionModel() const = 0;

Q_SIGNALS:
    void allergiesUpdated();
    void intolerancesUpdated();

};


}  // End namespace DrugsDB


#endif // DRUGSDB_IDRUGENGINE_H
