/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGBASECORE_H
#define DRUGBASECORE_H

#include <drugsbaseplugin/drugsbase_exporter.h>
#include <utils/database.h>

/**
 * \file ./plugins/drugsbaseplugin/drugbasecore.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DataPack {
class Pack;
}

namespace DrugsDB {
class DrugsBase;
class ProtocolsBase;
class InteractionManager;
class VersionUpdater;
class DrugsIO;
class PrescriptionPrinter;

namespace Internal {
class DrugsBasePlugin;
class DrugBaseCorePrivate;
}

class DRUGSBASE_EXPORT DrugBaseCore : public QObject
{
    Q_OBJECT
    friend class DrugsDB::Internal::DrugsBasePlugin;

protected:
    DrugBaseCore(QObject *parent = 0);
    bool initialize();

public:
    static DrugBaseCore &instance();
    virtual ~DrugBaseCore();
    bool isInitialized() const;

    DrugsBase &drugsBase() const;
    ProtocolsBase &protocolsBase() const;
    InteractionManager &interactionManager() const;
    VersionUpdater &versionUpdater() const;
    DrugsIO &drugsIo() const;
    PrescriptionPrinter &prescriptionPrinter() const;

private Q_SLOTS:
    void postCoreInitialization();
    void onCoreDatabaseServerChanged();
    void packChanged(const DataPack::Pack &pack);

private:
    static DrugBaseCore *m_Instance;
    Internal::DrugBaseCorePrivate *d;
};

}  // End namespace DrugsDB

#endif // DRUGBASECORE_H
