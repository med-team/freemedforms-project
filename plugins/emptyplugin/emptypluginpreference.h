/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATAPACKPREFERENCE_H
#define DATAPACKPREFERENCE_H

#include <coreplugin/ioptionspage.h>

#include <QPointer>
#include <QWidget>

/**
 * \file ./plugins/emptyplugin/emptypluginpreference.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;
}

namespace DataPackPlugin {
namespace Internal {
namespace Ui {
    class DataPackPreference;
}

class DataPackPreferenceWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DataPackPreferenceWidget(QWidget *parent = 0);
    ~DataPackPreferenceWidget();

    void setDatasToUi();
    static void writeDefaultSettings(Core::ISettings *s = 0);

public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);

protected:
    void changeEvent(QEvent *e);

private:
    Ui::DataPackPreference *ui;
};

class DataPackPreferencePage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    DataPackPreferencePage(QObject *parent = 0);
    ~DataPackPreferencePage();

    QString id() const;
    QString name() const;
    QString category() const;
    QString title() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void applyChanges();
    void finish();

    QString helpPage();

    static void writeDefaultSettings(Core::ISettings *s) {DataPackPreferenceWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);

private:
    QPointer<DataPackPreferenceWidget> m_Widget;
};

}  // End namespace Internal
}  // End namespace DataPackPlugin

#endif // DATAPACKPREFERENCE_H
