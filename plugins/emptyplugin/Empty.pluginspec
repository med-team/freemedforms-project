<plugin name="Empty" version="0.0.1" compatVersion="0.0.1">
    <vendor>FreeMedForms</vendor>
    <copyright>(C) 2008-2017 Eric Maeker, MD</copyright>
    <license>See application's license</license>
    <description>The plugin does nothing. It's only an infra-structure to ease creation of new plugins.</description>
    <url>http://www.freemedforms.com</url>
    <dependencyList>
        <dependency name="Core" version="0.0.1"/>
    </dependencyList>
</plugin>
