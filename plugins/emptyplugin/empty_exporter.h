/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef COREEXPORTER_H
#define COREEXPORTER_H

#include <qglobal.h>

#if defined(BASEFORMWIDGETS_LIBRARY)
#define BASEWIDGETS_EXPORT Q_DECL_EXPORT
#else
#define BASEWIDGETS_EXPORT Q_DECL_IMPORT
#endif

#endif
