#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

TEMPLATE        = lib
TARGET          = Empty

DEFINES += EMPTY_LIBRARY

include(../fmf_plugins.pri)
include( emptyplugin_dependencies.pri )

HEADERS = emptyplugin.h empty_exporter.h

SOURCES = emptyplugin.cpp

OTHER_FILES = Empty.pluginspec

TRANSLATIONS += $${SOURCES_TRANSLATIONS_PATH}/emptyplugin_fr.ts \
                $${SOURCES_TRANSLATIONS_PATH}/emptyplugin_de.ts \
                $${SOURCES_TRANSLATIONS_PATH}/emptyplugin_es.ts
