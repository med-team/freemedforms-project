/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TEMPLATES_TEMPLATESCORE_H
#define TEMPLATES_TEMPLATESCORE_H

#include <templatesplugin/templates_exporter.h>
#include <QObject>

namespace Templates {
class TemplatesModel;

namespace Internal {
class TemplatesPlugin;
class TemplatesCorePrivate;
class TemplateBase;
class TemplatesViewManager;
}

class TEMPLATES_EXPORT TemplatesCore : public QObject
{
    Q_OBJECT
    friend class Templates::Internal::TemplatesPlugin;

protected:
    TemplatesCore(QObject *parent = 0);
    void init();

public:
    static TemplatesCore &instance();

    TemplatesModel *templateModel() const;

    // Internals (not exported, should not be used outside the plugin)
    Internal::TemplateBase *templateBase() const;
    Internal::TemplatesViewManager *templateViewManager() const;

private:
    static TemplatesCore *m_Instance;
    Internal::TemplatesCorePrivate *d;
};

} // namespace Templates

#endif // TEMPLATES_TEMPLATESCORE_H
