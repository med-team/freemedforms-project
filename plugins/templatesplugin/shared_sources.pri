#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

DEFINES += TEMPLATES_LIBRARY

include($${PWD}/../fmf_plugins.pri)
include($${PWD}/templatesplugin_dependencies.pri)

QT += sql

HEADERS += $${PWD}/templatesplugin.h \
    $${PWD}/templates_exporter.h \
    $${PWD}/itemplates.h \
    $${PWD}/itemplateprinter.h \
    $${PWD}/templatebase.h \
    $${PWD}/templatesmodel.h \
    $${PWD}/constants.h \
    $${PWD}/templatesview.h \
    $${PWD}/templatesview_p.h \
    $${PWD}/templateseditdialog.h \
    $${PWD}/templatespreferencespages.h \
    $${PWD}/templatescreationdialog.h \
    $${PWD}/templatescore.h

SOURCES += $${PWD}/templatesplugin.cpp \
    $${PWD}/templatebase.cpp \
    $${PWD}/templatesmodel.cpp \
    $${PWD}/templatesview.cpp \
    $${PWD}/templateseditdialog.cpp \
    $${PWD}/templatespreferencespages.cpp \
    $${PWD}/templatescreationdialog.cpp \
    $${PWD}/itemplates.cpp \
    $${PWD}/templatescore.cpp

FORMS += $${PWD}/templatesview.ui \
    $${PWD}/templateseditdialog.ui \
    $${PWD}/templatescontenteditor.ui \
    $${PWD}/templatespreferenceswidget.ui \
    $${PWD}/templatescreationdialog.ui

# include translations
TRANSLATION_NAME = templates
include($${SOURCES_ROOT_PATH}/buildspecs/translations.pri)
