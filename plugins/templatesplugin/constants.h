/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TEMPLATES_CONSTANTS_H
#define TEMPLATES_CONSTANTS_H

/**
 * \file ./plugins/templatesplugin/constants.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Templates {
namespace Constants {
    const char * const DB_TEMPLATES_NAME      = "templates";
    const char * const DB_TEMPLATES_FILENAME  = "templates.db";
    const char * const DB_ACTUAL_VERSION      = "0.4.0";


    const char * const  S_BACKGROUND_CATEGORIES    = "Templates/Background/Categories";
    const char * const  S_BACKGROUND_TEMPLATES     = "Templates/Background/Templates";
    const char * const  S_FOREGROUND_CATEGORIES    = "Templates/Foreground/Categories";
    const char * const  S_FOREGROUND_TEMPLATES     = "Templates/Foreground/Templates";

    const char * const  S_SPLITTER_SIZES           = "Templates/Splitter/Sizes";

    const char * const  S_ALWAYSSHOWEXPANDED       = "Templates/AlwaysShowExpanded";
    const char * const  S_LOCKCATEGORYVIEW         = "Templates/LockCategoryView";
    const char * const  S_SAVEDATAWITHOUTPROMPTING = "Templates/SaveWithoutPrompting";
    const char * const  S_PROMPTFORDELETION        = "Templates/PromptForDeletion";

    const char * const  S_FONT                     = "Templates/Font";

    const char * const MIMETYPE_TEMPLATE           = "application/template";
    const char * const MIMETYPE_CATEGORY           = "application/template.category";

    const char * const XML_TEMPLATE_TAG            = "Template";
    const char * const XML_TEMPLATE_CONTENT_ATTRIB = "content";
    const char * const XML_TEMPLATE_MIME_ATTRIB    = "mime";
    const char * const XML_TEMPLATE_CHILD_TAG      = "Child";

    enum Tables {
        Table_Templates = 0,
        Table_Categories,
        Table_Version
    };

    enum TemplatesFields {
        TEMPLATE_ID = 0,
        TEMPLATE_UUID,
        TEMPLATE_USER_UID,
        TEMPLATE_GROUP_UID,
        TEMPLATE_ID_CATEGORY,
        TEMPLATE_LABEL,
        TEMPLATE_SUMMARY,
        TEMPLATE_CONTENT,
        TEMPLATE_CONTENTMIMETYPES,
        TEMPLATE_DATECREATION,
        TEMPLATE_DATEMODIF,
        TEMPLATE_THEMEDICON,
        TEMPLATE_TRANSMISSIONDATE,
        TEMPLATE_MaxParam
    };

    enum CategoryFields {
        CATEGORIES_ID = 0,
        CATEGORIES_UUID,
        CATEGORIES_USER_UID,
        CATEGORIES_GROUP_UID,
        CATEGORIES_PARENT_ID,
        CATEGORIES_LABEL,
        CATEGORIES_SUMMARY,
        CATEGORIES_MIMETYPES,
        CATEGORIES_DATECREATION,
        CATEGORIES_DATEMODIF,
        CATEGORIES_THEMEDICON,
        CATEGORIES_TRANSMISSIONDATE,
        CATEGROIES_MaxParam
    };

    enum VersionFields {
        VERSION_ACTUAL = 0
    };

    enum ModelDataRepresentation {
        Data_Label = 0,
        Data_Id,
        Data_Uuid,
        Data_UserUuid,
        Data_ParentId,
        Data_Summary,
        Data_ContentMimeTypes,
        Data_Content,
        Data_ThemedIcon,
        Data_CreationDate,
        Data_ModifDate,
        Data_TransmissionDate,
        Data_IsTemplate,
        Data_IsNewlyCreated,
        Data_Max_Param
    };


}  // end namespace Constants
}  // end namespace Templates


#endif // TEMPLATES_CONSTANTS_H
