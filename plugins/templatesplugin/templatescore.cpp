/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "templatescore.h"
#include "templatebase.h"
#include "templatesview_p.h"

using namespace Templates;
using namespace Internal;

namespace Templates {
namespace Internal {
class TemplatesCorePrivate
{
public:
    TemplatesCorePrivate(TemplatesCore */*parent*/) :
        m_Base(0),
        m_Manager(0) //, q(parent)
    {
    }

public:
    TemplateBase *m_Base;
    TemplatesViewManager *m_Manager;

private:
};
}  // Internal
}  // Templates

TemplatesCore *TemplatesCore::m_Instance = 0;

TemplatesCore &TemplatesCore::instance()
{
    return *m_Instance;
}

TemplatesCore::TemplatesCore(QObject *parent) :
    QObject(parent),
    d(new TemplatesCorePrivate(this))
{
    setObjectName("TemplatesCore");
    d->m_Base = new TemplateBase(this);
    m_Instance = this;
}

void TemplatesCore::init()
{
    d->m_Base->initialize();
    d->m_Manager = new TemplatesViewManager(this);
}

Internal::TemplateBase *TemplatesCore::templateBase() const
{
    return d->m_Base;
}

Internal::TemplatesViewManager *TemplatesCore::templateViewManager() const
{
    return d->m_Manager;
}
