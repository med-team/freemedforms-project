/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "templatescreationdialog.h"
#include "ui_templatescreationdialog.h"

#include <templatesplugin/templatesmodel.h>

#include <translationutils/constanttranslations.h>

#include <QDebug>

using namespace Templates;
using namespace Trans::ConstantTranslations;

TemplatesCreationDialog::TemplatesCreationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Internal::Ui::TemplatesCreationDialog)
{
    ui->setupUi(this);
    setWindowTitle(qApp->applicationName() + " - " + ui->label->text());
    ui->parentCategory->templatesModel()->setObjectName("TemplateCategoryParent");
    ui->parentCategory->templatesModel()->setReadOnly(true);
    ui->parentCategory->setViewContent(TemplatesView::CategoriesOnly);
    ui->parentCategory->setEditMode(TemplatesView::None);
    ui->parentCategory->expandAll();
    ui->parentCategory->setSelectionMode(QAbstractItemView::SingleSelection);
}

TemplatesCreationDialog::~TemplatesCreationDialog()
{
    delete ui;
}

void TemplatesCreationDialog::done(int r)
{
    if (r==QDialog::Accepted) {
        if (m_Content.isEmpty()) {
            QDialog::done(QDialog::Rejected);
            return;
        }
        TemplatesModel *model = new TemplatesModel(this);
        model->setObjectName("TemplateCreatorSaver");
        QModelIndex parent = ui->parentCategory->currentItem();
        int row = model->rowCount(parent);
        if (!model->insertRow(row, parent)) {
            return;
        }
        model->setData(model->index(row, Constants::Data_IsTemplate, parent), true);
        QString tmp = ui->nameLineEdit->text();
        if (tmp.isEmpty())
            tmp = tkTr(Trans::Constants::FILENEW_TEXT).remove("&");
        model->setData(model->index(row, Constants::Data_Label, parent), tmp);
        model->setData(model->index(row, Constants::Data_Summary, parent), ui->summaryTextEdit->toHtml());
        model->setData(model->index(row, Constants::Data_Content, parent), m_Content);
        model->setData(model->index(row, Constants::Data_ContentMimeTypes, parent), m_Mimes);
        model->setData(model->index(row, Constants::Data_IsNewlyCreated, parent), true);
        model->setData(model->index(row, Constants::Data_UserUuid), ui->userLineEdit->text());
        delete model;
    }

    QDialog::done(r);
}

void TemplatesCreationDialog::setTemplateSummary(const QString &summary)
{
    ui->summaryTextEdit->setText(summary);
}

void TemplatesCreationDialog::setTemplateMimeTypes(const QStringList &list)
{
    m_Mimes = list;
}

void TemplatesCreationDialog::setUserUuid(const QString &uuid)
{
    ui->userLineEdit->setText(uuid);
}

void TemplatesCreationDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
