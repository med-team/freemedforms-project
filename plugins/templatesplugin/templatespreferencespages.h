/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TEMPLATESPREFERENCESPAGES_H
#define TEMPLATESPREFERENCESPAGES_H

#include <coreplugin/ioptionspage.h>

#include <QPointer>

#include "ui_templatespreferenceswidget.h"

/**
 * \file ./plugins/templatesplugin/templatespreferencespages.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;
}

namespace Templates {
namespace Internal {

class TemplatesPreferencesWidget : public QWidget, private Ui::TemplatesPreferencesWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(TemplatesPreferencesWidget)

public:
    explicit TemplatesPreferencesWidget(QWidget *parent = 0);
    void setDataToUi();

    static void writeDefaultSettings(Core::ISettings *s);
    static void appliFontToViews(const QFont &font);

public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);

protected:
    virtual void changeEvent(QEvent *e);
};

class TemplatesPreferencesPage : public Core::IOptionsPage
{
    Q_OBJECT

public:
    TemplatesPreferencesPage(QObject *parent = 0);
    ~TemplatesPreferencesPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    QString helpPage() {return "parametrer.html";}

    static void writeDefaultSettings(Core::ISettings *s) {Internal::TemplatesPreferencesWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);
private:
    QPointer<Internal::TemplatesPreferencesWidget> m_Widget;
};

} // namespace Internal
} // namespace Templates

#endif // TEMPLATESPREFERENCESPAGES_H
