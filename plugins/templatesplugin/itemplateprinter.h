/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ITEMPLATEPRINTER_H
#define ITEMPLATEPRINTER_H

#include <templatesplugin/templates_exporter.h>
#include <templatesplugin/itemplates.h>

#include <QString>
#include <QList>

/**
 * \file ./plugins/templatesplugin/itemplateprinter.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Templates {

class TEMPLATES_EXPORT ITemplatePrinter : public QObject
{
    Q_OBJECT
public:
    ITemplatePrinter(QObject *parent = 0) : QObject(parent) {}
    virtual ~ITemplatePrinter() {}

    virtual QString mimeType() const = 0;
    virtual bool printTemplates(const QList<const ITemplate *> iTemplates) const = 0;

};

}  // end namespace Templates

#endif // ITEMPLATEPRINTER_H
