/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "itemplates.h"
#include "constants.h"

#include <utils/global.h>
#include <utils/serializer.h>

#include <QMimeData>
#include <QDomDocument>
#include <QDomElement>
#include <QHash>
#include <QVariant>

enum { base64MimeData = true };

using namespace Templates;


QMimeData *ITemplate::toMimeData()
{
    return 0;
}

bool ITemplate::fromMimeData(QMimeData *)
{
    return false;
}

QString ITemplate::serialize()
{
    return Utils::Serializer::toString(m_Data, base64MimeData);
}

bool ITemplate::deserialize(const QString &serialized)
{
    m_Data.clear();
    m_Data = Utils::Serializer::toVariantHash(serialized, base64MimeData);
    return true;
}
