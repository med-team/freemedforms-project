/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TEMPLATESMODEL_H
#define TEMPLATESMODEL_H

#include <templatesplugin/templates_exporter.h>
#include <templatesplugin/itemplates.h>

#include <QAbstractItemModel>
#include <QStringList>
#include <QObject>

/**
 * \file ./plugins/templatesplugin/templatesmodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Templates {
namespace Internal {
class TemplatesModelPrivate;
}  // end namespace Internal

class TEMPLATES_EXPORT TemplatesModel : public QAbstractItemModel
{
    Q_OBJECT
    friend class Internal::TemplatesModelPrivate;
public:
    TemplatesModel(QObject * parent = 0);
    ~TemplatesModel();
    bool setCurrentUser(const QString &uuid);

    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &index) const;
    bool reparentIndex(const QModelIndex &item, const QModelIndex &parent);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    QVariant data(const QModelIndex & item, int role = Qt::DisplayRole) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    Qt::DropActions supportedDropActions() const;

    bool insertTemplate(const Templates::ITemplate *t);
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());

    QStringList mimeTypes() const;
    QMimeData *mimeData(const QModelIndexList &indexes) const;
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent);
    QModelIndex getTemplateId(const int id);
    QList<QPersistentModelIndex> getIndexesFromMimeData(const QMimeData *mime);

    bool isTemplate(const QModelIndex &index) const;
    bool isCategory(const QModelIndex &index) const {return !isTemplate(index);}

    void categoriesOnly();
    bool isCategoryOnly() const;
    void setReadOnly(const bool state);

    bool isDirty() const;

    const ITemplate *getTemplate(const QModelIndex &item) const;

public Q_SLOTS:
    bool submit();

private Q_SLOTS:
    void onCoreDatabaseServerChanged();

private:
    Internal::TemplatesModelPrivate *d;
};

}  // end namespace Templates


#endif // TEMPLATESMODEL_H
