/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TEMPLATESEDITDIALOG_H
#define TEMPLATESEDITDIALOG_H

#include <QDialog>
#include <QModelIndex>

namespace Templates {
namespace Internal {
class TemplatesEditDialogPrivate;
}
class TemplatesModel;

class TemplatesEditDialog : public QDialog {
    Q_OBJECT
public:
    TemplatesEditDialog(QWidget *parent = 0);
    ~TemplatesEditDialog();

    void setModel(Templates::TemplatesModel *model);
    void setModelIndex(const QModelIndex &index);

protected:
    void done(int r);
    void changeEvent(QEvent *e);

private:
    void refreshComboCategory();
private Q_SLOTS:
    void editContent();

private:
    Internal::TemplatesEditDialogPrivate *d;
};

}  // End namespace Templates

#endif // TEMPLATESEDITDIALOG_H
