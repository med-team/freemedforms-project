/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TEMPLATESCREATIONDIALOG_H
#define TEMPLATESCREATIONDIALOG_H

#include <templatesplugin/templates_exporter.h>

#include <QDialog>

namespace Templates {
namespace Internal {
namespace Ui {
    class TemplatesCreationDialog;
}  // End namespace Ui
}  // End namespace Internal

class TEMPLATES_EXPORT TemplatesCreationDialog : public QDialog
{
    Q_OBJECT
public:
    TemplatesCreationDialog(QWidget *parent = 0);
    ~TemplatesCreationDialog();

    void setTemplateContent(const QString &content) {m_Content = content;}
    void setTemplateSummary(const QString &summary);
    void setTemplateMimeTypes(const QStringList &list);
    void setUserUuid(const QString &uuid);

protected:
    void done(int r);
    void changeEvent(QEvent *e);

private:
    Internal::Ui::TemplatesCreationDialog *ui;
    QString m_Content;
    QStringList m_Mimes;
};

}  // End namespace Templates

#endif // TEMPLATESCREATIONDIALOG_H
