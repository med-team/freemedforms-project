/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TEMPLATES_BASE_H
#define TEMPLATES_BASE_H

#include <utils/database.h>

/**
 * \file ./plugins/templatesplugin/templatebase.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/


namespace Templates {
class TemplatesCore;

namespace Internal {
class TemplateBasePrivate;

class TemplateBase : public QObject, public Utils::Database
{
    Q_OBJECT
    friend class Templates::TemplatesCore;

protected:
    TemplateBase(QObject *parent = 0);
    bool initialize();

public:
    ~TemplateBase();

    // Initializer / Checkers
    bool isInitialized();
    void logChronos(bool state);

private:
    bool createDatabase(const QString & connectionName , const QString & dbName,
                        const QString & pathOrHostName,
                        TypeOfAccess access, AvailableDrivers driver,
                        const QString & login, const QString & pass,
                        const int port,
                        CreationOption createOption
                       );

private Q_SLOTS:
    void onCoreDatabaseServerChanged();
    void onCoreFirstRunCreationRequested();

private:
    TemplateBasePrivate *d;
};

} // End namespace Internal
}  // End namespace Templates


#endif // TEMPLATES_BASE_H
