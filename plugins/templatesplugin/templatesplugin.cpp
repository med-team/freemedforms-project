/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/



#include "templatesplugin.h"
#include "templatescore.h"
#include "templatesmodel.h"
#include "templatesview.h"
#include "templatesview_p.h"
#include "templatespreferencespages.h"

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/translators.h>

#include <utils/log.h>

#include <QtCore/QtPlugin>
#include <QDebug>

using namespace Templates;
using namespace Internal;

TemplatesPlugin::TemplatesPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating TemplatesPlugin";

    Core::ICore::instance()->translators()->addNewTranslator("plugin_templates");

    new Templates::TemplatesCore(this);

    prefPage = new Internal::TemplatesPreferencesPage(this);
    addObject(prefPage);
}

TemplatesPlugin::~TemplatesPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool TemplatesPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "TemplatesPlugin::initialize";
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);

    return true;
}

void TemplatesPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "TemplatesPlugin::extensionsInitialized";

    Templates::TemplatesCore::instance().init();

    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));

    prefPage->checkSettingsValidity();
}

ExtensionSystem::IPlugin::ShutdownFlag TemplatesPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    if (prefPage) {
        removeObject(prefPage);
        delete prefPage; prefPage=0;
    }
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(TemplatesPlugin)
