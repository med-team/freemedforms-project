/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "agendapreferencespage.h"
#include "constants.h"

#include <utils/log.h>
#include <translationutils/constants.h>
#include <translationutils/trans_agenda.h>

#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/constants_menus.h>

#include <QDebug>

using namespace Agenda;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Core::ISettings *settings() { return Core::ICore::instance()->settings(); }

AgendaPreferencesPage::AgendaPreferencesPage(QObject *parent) :
        IOptionsPage(parent), m_Widget(0)
{
    setObjectName("AgendaPreferencesPage");
}

AgendaPreferencesPage::~AgendaPreferencesPage()
{
    if (m_Widget)
        delete m_Widget;
    m_Widget = 0;
}

QString AgendaPreferencesPage::id() const
{
    return objectName();
}

QString AgendaPreferencesPage::displayName() const
{
    return tkTr(Trans::Constants::AGENDA);
}

QString AgendaPreferencesPage::category() const
{
    return tkTr(Trans::Constants::AGENDA);
}

QString AgendaPreferencesPage::title() const
{
    return tr("Agenda preferences");
}

int AgendaPreferencesPage::sortIndex() const
{
    return Core::Constants::OPTIONINDEX_AGENDA;
}


void AgendaPreferencesPage::resetToDefaults()
{
    m_Widget->writeDefaultSettings(settings());
}

void AgendaPreferencesPage::apply()
{
    if (!m_Widget) {
        return;
    }
    m_Widget->saveToSettings(settings());
}

void AgendaPreferencesPage::finish()
{
    delete m_Widget;
}

void AgendaPreferencesPage::checkSettingsValidity()
{
    QHash<QString, QVariant> defaultvalues;

}

QWidget *AgendaPreferencesPage::createPage(QWidget *parent)
{
    if (m_Widget)
        delete m_Widget;
    m_Widget = new AgendaPreferencesWidget(parent);
    return m_Widget;
}



AgendaPreferencesWidget::AgendaPreferencesWidget(QWidget *parent) :
        QWidget(parent)
{
    setupUi(this);
    setDataToUi();
}

void AgendaPreferencesWidget::setDataToUi()
{
}

void AgendaPreferencesWidget::saveToSettings(Core::ISettings *sets)
{
    Q_UNUSED(sets);

}

void AgendaPreferencesWidget::writeDefaultSettings(Core::ISettings *)
{
}

void AgendaPreferencesWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        retranslateUi(this);
        break;
    default:
        break;
    }
}

