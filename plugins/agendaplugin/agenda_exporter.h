/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef AGENDA_EXPORTER_H
#define AGENDA_EXPORTER_H

/**
  \namespace Agenda
  \brief Namespace for the Agenda plugin.
  Namespace for the Agenda plugin.
*/

#include <qglobal.h>

#if defined(AGENDA_LIBRARY)
#define AGENDA_EXPORT Q_DECL_EXPORT
#else
#define AGENDA_EXPORT Q_DECL_IMPORT
#endif

#endif  // AGENDA_EXPORTER_H
