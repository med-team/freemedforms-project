/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef AGENDA_AVAILABILITYCREATORDIALOG_H
#define AGENDA_AVAILABILITYCREATORDIALOG_H

#include <QDialog>
#include <QList>
#include <agendaplugin/usercalendar.h>

namespace Agenda {
namespace Ui {
    class AvailabilityCreatorDialog;
}

class AvailabilityEditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AvailabilityEditDialog(QWidget *parent = 0);
    ~AvailabilityEditDialog();

    QList<DayAvailability> getAvailabilities() const;
    void setAvailability(const int dayOfWeek, const QTime &from, const QTime &to);
    void setDayOfWeek(const int dayOfWeek);
    void disableDayChange();

private Q_SLOTS:
    void on_startTime_timeChanged(const QTime &from);
    void updateUi();

private:
    Ui::AvailabilityCreatorDialog *ui;
};

}  // End namespace Agenda

#endif // AGENDA_AVAILABILITYCREATORDIALOG_H
