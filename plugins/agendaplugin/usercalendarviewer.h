/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef AGENDA_USERCALENDARVIEWER_H
#define AGENDA_USERCALENDARVIEWER_H

#include <QWidget>
QT_BEGIN_NAMESPACE
class QModelIndex;
class QDate;
QT_END_NAMESPACE

/**
 * \file ./plugins/agendaplugin/usercalendarviewer.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Agenda {
class UserCalendar;

namespace Internal {
class UserCalendarViewerPrivate;
class AgendaMode;

class UserCalendarViewer : public QWidget
{
    Q_OBJECT
    friend class Agenda::Internal::AgendaMode;

public:
    enum AgendaOwner {
        OwnAgendas = 0,
        DelegatedAgendas
    };

    explicit UserCalendarViewer(QWidget *parent = 0);
    ~UserCalendarViewer();

protected:
    void recalculateComboAgendaIndex();

private Q_SLOTS:
    void newEvent();
    void newEventAtAvailabity(const QModelIndex &index);
    void refreshAvailabilities();
    void resetDefaultDuration();
    void quickDateSelection(QAction *a);
    void onStartDateChanged(const QDate &start);
    void recalculateAvailabilitiesWithDurationIndex(const int index);
    void on_availableAgendasCombo_activated(const int index);
    void userChanged();
    void updateCalendarData(const QModelIndex &top, const QModelIndex &bottom);
    void onSwitchToPatientClicked();
    void onEditAppointmentClicked();
    void onPrintAppointmentClicked();
    void onDeleteAppointmentClicked();

protected:
    void clear();
    bool event(QEvent *e);

private:
    UserCalendarViewerPrivate *d;
};

}  // End namespace Internal
}  // End namespace Agenda

#endif // AGENDA_USERCALENDARVIEWER_H
