/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef AGENDA_AGENDACORE_H
#define AGENDA_AGENDACORE_H

#include <QObject>

/**
 * \file ./plugins/agendaplugin/agendacore.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Agenda {
class UserCalendarModel;
class CalendarItemModel;

namespace Internal {
class AgendaCorePrivate;
class AgendaBase;
class AgendaPlugin;
}  // End namespace Internal

class AgendaCore : public QObject
{
    Q_OBJECT
    friend class Agenda::Internal::AgendaPlugin;

public:
    static AgendaCore &instance();
    ~AgendaCore();

    UserCalendarModel *userCalendarModel(const QString &userUid = QString::null);
    CalendarItemModel *calendarItemModel(const QVariant &calendarUid);

    bool initializeDatabase();
    Internal::AgendaBase &agendaBase() const;

protected Q_SLOTS:
    void postCoreInitialization();

protected:
    explicit AgendaCore(QObject *parent = 0);
    void extensionsInitialized();
    void removeObjectFromPluginManager();

private:
    static AgendaCore *m_Instance;
    Internal::AgendaCorePrivate *d;
};

}  // End namespace Agenda

#endif // AGENDA_AGENDACORE_H
