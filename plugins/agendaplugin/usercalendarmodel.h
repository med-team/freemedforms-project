/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef AGENDA_USERCALENDARMODEL_H
#define AGENDA_USERCALENDARMODEL_H

#include <calendar/modelanditem/calendar_people.h>

#include <QAbstractTableModel>
#include <QStandardItemModel>

/**
 * \file ./plugins/agendaplugin/usercalendarmodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Agenda {
class AgendaCore;
class DayAvailabilityModel;
class DayAvailability;
class UserCalendar;

namespace Internal {
class UserCalendarModelPrivate;
} // End namespace Internal

class UserCalendarModel : public QAbstractTableModel
{
    Q_OBJECT
    friend class Agenda::AgendaCore;

protected:
    explicit UserCalendarModel(const QString &userUid = QString::null, QObject *parent = 0);

public:
    enum DataRepresentation {
        Label = 0,
        ExtraLabel, // with user delegation
        Description,
        Type,
        Status,
        IsDefault,
        IsPrivate,
        Password,
        LocationUid,
        DefaultDuration,
        Uid,
        ColumnCount
    };

    ~UserCalendarModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());

    UserCalendar *userCalendarAt(const int row) const;
    UserCalendar *defaultUserCalendar() const;
    QModelIndex defaultUserCalendarModelIndex() const;
    void updateUserCalendarChanged(const int row);

    DayAvailabilityModel *availabilityModel(const QModelIndex &index, QObject *parent = 0) const;
    DayAvailabilityModel *availabilityModel(const int index, QObject *parent = 0) const;

    void setPeopleList(const int row, const QList<Calendar::People> &peoples);
    void addPeople(const int row, const Calendar::People &people);
    void removePeople(const int row, const QString &uid);

Q_SIGNALS:
    void defaultAgendaChanged(const QModelIndex &index);

public Q_SLOTS:
    bool submit();
    void revert();

private:
    Internal::UserCalendarModelPrivate *d;
};


} // End namespace Agenda

#endif // AGENDA_USERCALENDARMODEL_H
