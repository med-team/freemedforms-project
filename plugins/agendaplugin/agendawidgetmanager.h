/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef AGENDA_AGENDAWIDGETMANAGER_H
#define AGENDA_AGENDAWIDGETMANAGER_H

#include <agendaplugin/agenda_exporter.h>

#include <coreplugin/contextmanager/icontext.h>

#include <QWidget>
#include <QObject>
#include <QAction>
#include <QPointer>

/**
 * \file ./plugins/agendaplugin/agendawidgetmanager.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
 * \internal
*/

namespace Agenda {
namespace Internal {

//class AgendaContext : public Core::IContext
//{
//public:
//    AgendaContext(DrugsCentralWidget *w) : Core::IContext(w)
//    { setObjectName("AgendaContext"); setWidget(w); }
//};

class AGENDA_EXPORT AgendaActionHandler : public QObject
{
    Q_OBJECT
public:
    AgendaActionHandler(QObject *parent = 0);
    virtual ~AgendaActionHandler() {}

    void setCurrentView(QWidget *view);

private Q_SLOTS:
    void clear();
//    void newEvent();
    void printSelection();
    void printPreviewSelection();

    void showAgendaDatabaseInformation();

private:
    void updateActions();

protected:
    QAction *aClear;
    QAction *aNewEvent;
    QAction *aPrintSelection;
    QAction *aPrintPreviewSelection;
    QAction *aAgendaDatabaseInformation;

    QPointer<QWidget> m_CurrentView;
};

}  // End Internal
}  // End Agenda


namespace Agenda {

class AGENDA_EXPORT AgendaWidgetManager : public Internal::AgendaActionHandler
{
    Q_OBJECT
public:
    static AgendaWidgetManager *instance();
    ~AgendaWidgetManager() {}

    QWidget *currentView() const;

private Q_SLOTS:
    void updateContext(Core::IContext *object, const Core::Context &additionalContexts);

private:
    AgendaWidgetManager(QObject *parent = 0);
    static AgendaWidgetManager *m_Instance;
};

}  // End Agenda

#endif // AGENDA_AGENDAWIDGETMANAGER_H
