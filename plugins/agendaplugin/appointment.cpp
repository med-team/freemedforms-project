/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "appointment.h"
#include "calendaritemmodel.h"

#include <calendar/common.h>

using namespace Agenda;
using namespace Internal;

Appointment::Appointment() :
    m_Modified(false),
    m_uid(-1)
{
    m_Data.insert(Constants::Db_CalId, -1);
    m_Data.insert(Constants::Db_ComId, -1);
    m_Data.insert(Constants::Db_EvId, -1);
    m_Data.insert(Constants::Db_CyclingEvId, -1);
    m_Data.insert(Constants::Db_UserCalId, -1);
    m_Data.insert(Constants::Db_IsValid, false);
}

bool Appointment::isNull() const
{
    if (m_Modified)
        return false;
    if ((m_Data.value(Constants::Db_CalId).toInt() == -1) &&
            (m_Data.value(Constants::Db_ComId).toInt() == -1) &&
            (m_Data.value(Constants::Db_EvId).toInt() == -1) &&
            (m_Data.value(Constants::Db_CyclingEvId).toInt() == -1) &&
            (m_Data.value(Constants::Db_UserCalId).toInt() == -1) &&
            (m_Data.value(Constants::Db_IsValid).toBool() == false)) {
        return true;
    }
    return false;
}

bool Appointment::isValid() const
{
    return true;
}

QVariant Appointment::data(const int ref) const
{
    return m_Data.value(ref, QVariant());
}

bool Appointment::setData(const int ref, const QVariant &value)
{
    m_Modified = true;
    m_Data.insert(ref, value);
    return true;
}

QDateTime Appointment::beginning() const
{
    return data(CalendarItemModel::DateStart).toDateTime();
}

QDateTime Appointment::ending() const
{
    return data(CalendarItemModel::DateEnd).toDateTime();
}

/** compute an intersection value with a day range
             * returns:
             * -1 if item is entirely before first day
             * 0 if item intersects [firstDay, lastDay]
             * 1 if item is entirely after lastDay
*/
int Appointment::intersects(const QDate &firstDay, const QDate &lastDay) const
{
    return Calendar::intersectsDays(beginning(), ending(), firstDay, lastDay);
}


bool Appointment::dateLessThan(const Appointment *item1, const Appointment *item2)
{
    if (item1->beginning() < item2->beginning())
        return true;
    else if (item1->beginning() > item2->beginning())
        return false;
    else if (item1->ending() > item2->ending())
        return true;
    else if (item1->ending() < item2->ending())
        return false;
    return false;
}

QDebug operator<<(QDebug dbg, const Agenda::Internal::Appointment &c)
{
    dbg.nospace() << "Appointment("
                  << c.beginning().toString(QLocale().dateTimeFormat(QLocale::ShortFormat))
                  << ", "
                  << c.ending().toString(QLocale().dateTimeFormat(QLocale::ShortFormat))
                  << ", "
                  << "Valid:" << c.isValid()
                  << ", "
                  << "Virtual:" << c.data(Agenda::Constants::Db_IsVirtual).toBool()
                  << ", "
                  << "CalId:" << c.calendarId()
                  << ", "
                  << "Peoples:" << c.peopleUids(Calendar::People::PeopleAttendee).join(",")
                  << ")";
    return dbg.space();
}

QDebug operator<<(QDebug dbg, const Agenda::Internal::Appointment *c)
{
    if (!c) {
        dbg.nospace() << "Appointment(0x0)";
        return dbg.space();
    }
    return operator<<(dbg, *c);
}

