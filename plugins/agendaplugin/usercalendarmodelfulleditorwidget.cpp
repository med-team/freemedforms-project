/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "usercalendarmodelfulleditorwidget.h"
#include "agendabase.h"
#include "agendacore.h"
#include "usercalendarmodel.h"

#include <coreplugin/icore.h>
#include <coreplugin/iuser.h>
#include <coreplugin/itheme.h>
#include <coreplugin/modemanager/modemanager.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/constants_menus.h>

#include <utils/log.h>
#include <utils/global.h>
#include <translationutils/constants.h>
#include <translationutils/trans_agenda.h>

#include "ui_usercalendarmodelfulleditorwidget.h"

using namespace Agenda;
using namespace Internal;
using namespace Trans::ConstantTranslations;

UserCalendarModelFullEditorWidget::UserCalendarModelFullEditorWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UserCalendarModelFullEditorWidget),
    m_UserCalendarModel(0)
{

    ui->setupUi(this);
    ui->editor->setEnabled(false);

    connect(ui->agendaNamesCombo, SIGNAL(currentIndexChanged(QModelIndex)), this, SLOT(setCurrentIndex(QModelIndex)));
}

UserCalendarModelFullEditorWidget::~UserCalendarModelFullEditorWidget()
{
    delete ui;
}

void UserCalendarModelFullEditorWidget::setUserCalendarModel(UserCalendarModel *model)
{
    Q_ASSERT(model);
    if (!model)
        return;
    if (m_UserCalendarModel==model)
        return;
    m_UserCalendarModel = model;
    ui->editor->setUserCalendarModel(m_UserCalendarModel);
    ui->agendaNamesCombo->setModel(m_UserCalendarModel);
    ui->agendaNamesCombo->setModelColumn(UserCalendarModel::Label);
}

void UserCalendarModelFullEditorWidget::setCurrentIndex(const QModelIndex &index)
{
    ui->editor->setEnabled(index.isValid());
    ui->editor->setCurrentIndex(index);
}

void UserCalendarModelFullEditorWidget::clear()
{
    ui->editor->clear();
    ui->editor->setEnabled(false);
    ui->agendaNamesCombo->setModel(0);
    m_UserCalendarModel = 0;
}

bool UserCalendarModelFullEditorWidget::submit()
{
    ui->editor->submit();
    if (m_UserCalendarModel)
        return m_UserCalendarModel->submit();
    return false;
}

void UserCalendarModelFullEditorWidget::changeEvent(QEvent *e)
{
    if (e->type() == QEvent::LanguageChange) {
        aCreateCalendar->setText(tr("Create a new calendar"));
        aCreateCalendar->setToolTip(aCreateCalendar->text());
    }
}

