/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IUSERCALENDAR_H
#define IUSERCALENDAR_H

#include <agendaplugin/agenda_exporter.h>

#include <QVariant>
QT_BEGIN_NAMESPACE
class QStandardItem;
QT_END_NAMESPACE

namespace Agenda {
namespace Internal {
class AgendaBase;
}

class AGENDA_EXPORT IUserCalendar
{
    friend class Agenda::Internal::AgendaBase;

public:
    enum DataRepresentation {
        UserOwnerUid = 0,
        Label,
        FullContent,
        TypeId,
        StatusId,
        IsDefault,
        IsPrivate,
        Password,
        ThemedIcon,
        DbOnly_CalId,
        DbOnly_CatId,
        DbOnly_UserCalId,
        DbOnly_IsValid
    };

    IUserCalendar();
    virtual ~IUserCalendar();

    virtual bool isValid() const;
    virtual bool isNull() const;

    virtual QVariant data(const int ref) const;
    virtual bool setData(const int ref, const QVariant &value);

    virtual bool isModified() const;
    virtual void setModified(const bool state);

    virtual QStandardItem *toStandardItem() const;
    virtual int calendarId() const;

protected:
    virtual void setDatabaseValue(const int ref, const QVariant &value);
    virtual QString userOwnerUid() const;
    virtual int categoryId() const;
    virtual QString xmlOptions() const;

private:
    QHash<int, QVariant> m_Datas;
    bool m_Modified;
};

}  // End namespace Agenda

#endif // IUSERCALENDAR_H
