/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef AGENDA_USERCALENDARDELEGATESMAPPER_H
#define AGENDA_USERCALENDARDELEGATESMAPPER_H

#include <calendar/icalendaritemdatawidget.h>

#include <QWidget>
#include <QHash>
#include <QPointer>
#include <QModelIndex>

/**
 * \file ./plugins/agendaplugin/usercalendardelegatesmapper.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace UserPlugin {
class UserLineEditCompleterSearch;
}

namespace Calendar {
class CalendarPeopleModel;
}

namespace Agenda {
class UserCalendarModel;

namespace Internal {
namespace Ui {
    class UserCalendarDelegatesMapperWidget;
}

class UserCalendarDelegatesMapperWidget : public QWidget
{
    Q_OBJECT
public:
    UserCalendarDelegatesMapperWidget(QWidget *parent);
    ~UserCalendarDelegatesMapperWidget();

    void clear();
    void setUserCalendarModel(UserCalendarModel *model);

public Q_SLOTS:
    void setUserCalendarIndex(const int index);
    bool submit();

private:
    void addRow(const QString &name, const QString &uid);

private Q_SLOTS:
    void removePerson(QAction *action);
    void onPersonSelected(const QString &name, const QString &uid);
    void handlePressed(const QModelIndex &index);
    void handleClicked(const QModelIndex &index);

private:
    Internal::Ui::UserCalendarDelegatesMapperWidget *ui;
    UserPlugin::UserLineEditCompleterSearch *m_Completer;
    QHash<QString, QWidget *> m_UserWidgets;
    UserCalendarModel *m_UserCalendarModel;
    Calendar::CalendarPeopleModel *m_PeopleModel;
    int m_Row;
};

}  // End namespace Internal
}  // End namespace Agenda

#endif // AGENDA_USERCALENDARDELEGATESMAPPER_H

