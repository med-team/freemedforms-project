/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef AGENDA_USERCALENDAR_H
#define AGENDA_USERCALENDAR_H

#include <agendaplugin/agenda_exporter.h>
#include <calendar/modelanditem/calendar_people.h>

#include <QVariant>
#include <QVector>
#include <QPair>
#include <QTime>
#include <QStandardItemModel>

/**
 * \file ./plugins/agendaplugin/usercalendar.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Agenda {

class AbstractCalendarModel;
class DayAvailabilityModel;
class DayAvailability;

class AGENDA_EXPORT UserCalendar : public Calendar::CalendarPeople
{
    friend class Agenda::DayAvailabilityModel;
public:
    enum DataRepresentation {
        Uid = 0,
        UserOwnerUid,
        UserOwnerFullName,
        Label,
        Description,
        Type,
        Status,
        IsDefault,
        IsPrivate,
        Password,
        LocationUid,
        AbsPathIcon,
        DefaultDuration,
        SortId,
        IsDelegated,
        UserData = 10000
    };

    UserCalendar();
    virtual ~UserCalendar();

    virtual bool isValid() const;
    virtual bool isNull() const;

    virtual QVariant data(const int ref) const;
    virtual bool setData(const int ref, const QVariant &value);

    virtual bool isModified() const;
    virtual void setModified(const bool state);

    virtual QString xmlOptions() const;

    bool hasAvailability() const;
    QVector<DayAvailability> availabilities(const int day = -1) const;
    void addAvailabilities(const DayAvailability &av);
    void setAvailabilities(const QList<DayAvailability> &availabilities);
    void clearAvailabilities();
    void removeAvailabilitiesForWeekDay(const int weekday);
    void removeAvailabilitiesTimeRange(const int weekday, const QTime &from, const QTime &to);


    bool canBeAvailable(const QDateTime &date) const;
    bool canBeAvailable(const QDateTime &date, const int durationInMinutes) const;

    AbstractCalendarModel *model() {return m_model;}

    // Calendar::CalendarPeople interface
    void addPeople(const Calendar::People &people)  {setModified(true); Calendar::CalendarPeople::addPeople(people);}
    void setPeopleName(const int people, const QString &uid, const QString &name) {setModified(true); Calendar::CalendarPeople::setPeopleName(people, uid, name);}
    void removePeople(const QString &uid) {setModified(true); Calendar::CalendarPeople::removePeople(uid);}
    void clearPeople(const int people)    {setModified(true); Calendar::CalendarPeople::clearPeople(people);}

    // getters
    QString uid() const {return data(Uid).toString();}
    int sortId() const {return data(SortId).toInt();}
    bool isDefault() const {return data(IsDefault).toBool();}
    bool isDelegated() const {return data(IsDelegated).toBool();}

    static bool lessThan(const UserCalendar *cal1, const UserCalendar *cal2);

protected:
    void setModel(AbstractCalendarModel *model) {m_model=model;}

private:
    QHash<int, QVariant> m_data;
    bool m_modified;
    QList<DayAvailability> m_availabilities;
    AbstractCalendarModel *m_model;
};

}  // End namespace Agenda


#endif // AGENDA_USERCALENDAR_H
