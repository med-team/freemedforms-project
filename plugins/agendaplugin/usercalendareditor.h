/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef AGENDA_USERCALENDAREDITOR_H
#define AGENDA_USERCALENDAREDITOR_H

#include <agendaplugin/agenda_exporter.h>

#include <QWidget>
#include <QDataWidgetMapper>
#include <QModelIndex>

/**
 * \file ./plugins/agendaplugin/usercalendareditor.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Calendar {
class AbstractCalendarModel;
}

namespace Agenda {
class UserCalendar;
class UserCalendarModel;
class DayAvailabilityModel;

namespace Ui {
class UserCalendarEditorWidget;
}

class AGENDA_EXPORT UserCalendarEditorWidget : public QWidget
{
    Q_OBJECT

public:
    explicit UserCalendarEditorWidget(QWidget *parent = 0);
    ~UserCalendarEditorWidget();

    void clear();

    void setUserCalendarModel(UserCalendarModel *model);

public Q_SLOTS:
    void setCurrentIndex(const QModelIndex &index);
    void addAvailability();
    void removeAvailabilities();
    void editAvailability();
    void editAvailability(const QModelIndex &index);
    void clearAvailabilities();
    void submit();
    void revert();
    void updateUi(const QModelIndex &index = QModelIndex());

protected:
    void changeEvent(QEvent *e);

private:
    Ui::UserCalendarEditorWidget *ui;
    UserCalendarModel *m_UserCalendarModel;
    DayAvailabilityModel *m_AvailabilityModel;
    QDataWidgetMapper *m_Mapper;
};

}  // End namespace Agenda


#endif // AGENDA_USERCALENDAREDITOR_H
