/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef AGENDA_USERCALENDARWIZARDCREATORPAGE_H
#define AGENDA_USERCALENDARWIZARDCREATORPAGE_H

#include <usermanagerplugin/iuserwizardpage.h>

#include <QWidget>

/**
 * \file ./plugins/agendaplugin/usercalendarwizardcreatorpage.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Agenda {
class UserCalendar;

namespace Internal {

namespace Ui {
class UserCalendarWizardCreatorWidget;
}

class UserCalendarWizardPage : public QWizardPage
{
    Q_OBJECT

public:
    explicit UserCalendarWizardPage(QWidget *parent = 0);
    ~UserCalendarWizardPage();

    UserCalendar *getUserCalendar(const QString &userUid);

    bool validatePage();

protected Q_SLOTS:
    void toggleAgendaEditing(bool state);

private:
    void retranslate();
    void changeEvent(QEvent *event);

    Ui::UserCalendarWizardCreatorWidget *ui;
};


class UserCalendarWizardCreatorPage : public UserPlugin::IUserWizardPage
{
    Q_OBJECT

public:
    explicit UserCalendarWizardCreatorPage(QObject *parent = 0);
    ~UserCalendarWizardCreatorPage();

    // Core::IGenericPage
    QString id() const {return objectName();}
    QString displayName() const;
    QString title() const;
    QString category() const;
    int sortIndex() const;

    QWidget *createPage(QWidget *parent = 0) {Q_UNUSED(parent); return 0;}

    // UserPlugin::IUserWizardPage
    QWizardPage *createWizardPage(QWidget *parent);
    void submit(const QString &userUid);

private:
    UserCalendarWizardPage *page;
};

}  // End namespace Internal
}  // End namespace Agenda


#endif // AGENDA_USERCALENDARWIZARDCREATORPAGE_H
