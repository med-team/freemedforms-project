/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "iusercalendar.h"

#include <coreplugin/icore.h>
#include <coreplugin/iuser.h>
#include <coreplugin/itheme.h>

#include <QStandardItem>

using namespace Agenda;

static inline Core::IUser *user() {return Core::ICore::instance()->user();}
static inline Core::ITheme *theme() {return Core::ICore::instance()->theme();}

IUserCalendar::IUserCalendar() :
        m_Modified(false)
{
    m_Datas.insert(DbOnly_UserCalId, -1);
    m_Datas.insert(DbOnly_CalId, -1);
    m_Datas.insert(DbOnly_CatId, -1);
    m_Datas.insert(DbOnly_IsValid, false);
    m_Datas.insert(UserOwnerUid, user()->uuid());
}

IUserCalendar::~IUserCalendar()
{}

bool IUserCalendar::isValid() const
{
    return true;
}

bool IUserCalendar::isNull() const
{
    return false;
}

QVariant IUserCalendar::data(const int ref) const
{
    return m_Datas.value(ref);
}

bool IUserCalendar::setData(const int ref, const QVariant &value)
{
    m_Datas.insert(ref, value);
    m_Modified = true;
    return true;
}

bool IUserCalendar::isModified() const
{
    return m_Modified;
}

void IUserCalendar::setModified(const bool state)
{
    m_Modified=state;
}

QStandardItem *IUserCalendar::toStandardItem() const
{
    QStandardItem *it = new QStandardItem;
    if (m_Datas.keys().contains(ThemedIcon))
        it->setIcon(theme()->icon(m_Datas.value(ThemedIcon).toString()));
    it->setText(m_Datas.value(Label).toString());
    it->setToolTip(it->text());
    return it;
}

void IUserCalendar::setDatabaseValue(const int ref, const QVariant &value)
{
    m_Datas.insert(ref, value);
    m_Modified = true;
}

int IUserCalendar::calendarId() const
{
    return m_Datas.value(DbOnly_CalId).toInt();
}

QString IUserCalendar::userOwnerUid() const
{
    return m_Datas.value(UserOwnerUid).toString();
}

int IUserCalendar::categoryId() const
{
    return m_Datas.value(DbOnly_CatId).toInt();
}

QString IUserCalendar::xmlOptions() const
{
    return QString();
}
