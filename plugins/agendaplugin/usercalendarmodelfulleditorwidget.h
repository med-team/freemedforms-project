/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef AGENDA_UserCalendarModelFullEditorWidget_H
#define AGENDA_UserCalendarModelFullEditorWidget_H

#include <QWidget>
#include <QModelIndex>

/**
 * \file ./plugins/agendaplugin/usercalendarmodelfulleditorwidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Agenda {
class UserCalendarModel;

namespace Ui {
    class UserCalendarModelFullEditorWidget;
}

/** \todo code here : put this in Internal */
class UserCalendarModelFullEditorWidget : public QWidget
{
    Q_OBJECT
public:
    explicit UserCalendarModelFullEditorWidget(QWidget *parent = 0);
    ~UserCalendarModelFullEditorWidget();

    void setUserCalendarModel(UserCalendarModel *model);

public Q_SLOTS:
    void clear();
    bool submit();

private Q_SLOTS:
    void setCurrentIndex(const QModelIndex &index);

private:
    void changeEvent(QEvent *e);

private:
    Ui::UserCalendarModelFullEditorWidget *ui;
    UserCalendarModel *m_UserCalendarModel;
    QAction *aCreateCalendar;
};

}  // End namespace Agenda

#endif // AGENDA_UserCalendarModelFullEditorWidget_H
