/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef AGENDA_AGENDAMODE_H
#define AGENDA_AGENDAMODE_H

#include <coreplugin/modemanager/imode.h>

#include <QStackedWidget>
#include <QModelIndex>

/**
 * \file ./plugins/agendaplugin/agendamode.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Agenda {
class UserCalendarModel;

namespace Internal {
class UserCalendarViewer;

class AgendaMode : public Core::IMode
{
    Q_OBJECT
public:
    explicit AgendaMode(QObject *parent = 0);

private Q_SLOTS:
    void postCoreInitialization();
    void userChanged();
    void rowsChanged(const QModelIndex &parent, int start, int end);

private:
    void updateEnableState();

private:
    QStackedWidget *m_Stack;
    UserCalendarViewer *m_Viewer;
    UserCalendarModel *m_UserCalendarModel;
};

}  // End namespace Internal
}  // End namespace Agenda


#endif // AGENDA_AGENDAMODE_H
