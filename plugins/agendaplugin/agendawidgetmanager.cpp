/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "agendawidgetmanager.h"
#include "agendacore.h"
#include "agendabase.h"
#include <agendaplugin/constants.h>

#include <utils/log.h>
#include <utils/global.h>
#include <utils/widgets/databaseinformationdialog.h>
#include <translationutils/constanttranslations.h>

#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/itheme.h>
#include <coreplugin/ipatient.h>
#include <coreplugin/imainwindow.h>
#include <coreplugin/constants_menus.h>
#include <coreplugin/constants_icons.h>
#include <coreplugin/contextmanager/contextmanager.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/actioncontainer.h>

#include <QDialog>
#include <QGridLayout>
#include <QTreeWidget>
#include <QHeaderView>

using namespace Agenda::Constants;
using namespace Agenda;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Core::ActionManager *actionManager() {return Core::ICore::instance()->actionManager();}
static inline Core::ContextManager *contextManager() { return Core::ICore::instance()->contextManager(); }
static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline Agenda::AgendaCore &agendaCore() {return Agenda::AgendaCore::instance();}

AgendaWidgetManager *AgendaWidgetManager::m_Instance = 0;

AgendaWidgetManager *AgendaWidgetManager::instance()
{
    if (!m_Instance)
        m_Instance = new AgendaWidgetManager(qApp);
    return m_Instance;
}

AgendaWidgetManager::AgendaWidgetManager(QObject *parent) :
    AgendaActionHandler(parent)
{
    connect(Core::ICore::instance()->contextManager(), SIGNAL(contextChanged(Core::IContext*,Core::Context)),
            this, SLOT(updateContext(Core::IContext*,Core::Context)));
    setObjectName("AgendaWidgetManager");
}

void AgendaWidgetManager::updateContext(Core::IContext *object, const Core::Context &additionalContexts)
{
    Q_UNUSED(object);
    Q_UNUSED(additionalContexts);

    QWidget *view = 0;




    if (view) {
        AgendaActionHandler::setCurrentView(view);
    }
}

QWidget *AgendaWidgetManager::currentView() const
{
    return AgendaActionHandler::m_CurrentView;
}

AgendaActionHandler::AgendaActionHandler(QObject *parent) :
        QObject(parent),
        aClear(0),
        aNewEvent(0),
        aPrintSelection(0),
        aPrintPreviewSelection(0),
        aAgendaDatabaseInformation(0),
        m_CurrentView(0)
{
    setObjectName("AgendaActionHandler");
    QAction *a = 0;
    Core::Command *cmd = 0;
    Core::Context ctx(Agenda::Constants::C_AGENDA_PLUGIN);
    Core::Context globalcontext(Core::Constants::C_GLOBAL);







    Core::ActionContainer *fmenu = actionManager()->actionContainer(Core::Constants::M_GENERAL_NEW);
    a = aNewEvent = new QAction(this);
    QIcon icon;
    icon.addFile(theme()->iconFullPath(Constants::I_NEW_AGENDAEVENT, Core::ITheme::SmallIcon), QSize(16, 16));
    icon.addFile(theme()->iconFullPath(Constants::I_NEW_AGENDAEVENT, Core::ITheme::MediumIcon), QSize(32, 32));
    a->setIcon(icon);
    cmd = actionManager()->registerAction(a, Core::Id(Constants::A_NEW_AGENDAEVENT), globalcontext);
    cmd->setTranslations(Trans::Constants::AGENDA_EVENT, Trans::Constants::AGENDA_EVENT);
    cmd->retranslate();
    if (fmenu) {
        fmenu->addAction(cmd, Core::Id(Core::Constants::G_GENERAL_NEW));
    }

    a = aPrintSelection = new QAction(this);
    a->setIcon(theme()->icon(Core::Constants::ICONPRINT));
    cmd = actionManager()->registerAction(a, Core::Id(Constants::A_PRINT_SELECTION), ctx);
    cmd->setTranslations(Constants::PRINTSELECTION_TEXT, Constants::PRINTSELECTION_TEXT, AGENDA_TR_CONTEXT);
    cmd->retranslate();
    connect(aPrintSelection,SIGNAL(triggered()), this, SLOT(printSelection()));



    Core::ActionContainer *hmenu = actionManager()->actionContainer(Core::Id(Core::Constants::M_HELP_DATABASES));
    a = aAgendaDatabaseInformation = new QAction(this);
    a->setIcon(theme()->icon(Core::Constants::ICONHELP));
    cmd = actionManager()->registerAction(a, Core::Id(Constants::A_AGENDADATABASE_INFORMATION), globalcontext);
    cmd->setTranslations(Trans::Constants::AGENDA_DATABASE_INFORMATION);
    cmd->retranslate();
    if (hmenu) {
        hmenu->addAction(cmd, Core::Id(Core::Constants::G_HELP_DATABASES));
    }
    connect(aAgendaDatabaseInformation,SIGNAL(triggered()), this, SLOT(showAgendaDatabaseInformation()));

    contextManager()->updateContext();
    actionManager()->retranslateMenusAndActions();
}


void AgendaActionHandler::setCurrentView(QWidget *view)
{
    Q_ASSERT(view);
    if (!view) { // this should never be the case
        LOG_ERROR("setCurrentView: no view");
        return;
    }


}

void AgendaActionHandler::updateActions()
{
}

void AgendaActionHandler::clear()
{
}


void AgendaActionHandler::printSelection()
{
}

void AgendaActionHandler::printPreviewSelection()
{
}

void AgendaActionHandler::showAgendaDatabaseInformation()
{
    Utils::DatabaseInformationDialog dlg(Core::ICore::instance()->mainWindow());
    dlg.setTitle(tkTr(Trans::Constants::AGENDA_DATABASE_INFORMATION));
    dlg.setDatabase(agendaCore().agendaBase());
    Utils::resizeAndCenter(&dlg);
    dlg.exec();
}
