/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CALENDARITEMEDITORUSERCALENDARMAPPER_H
#define CALENDARITEMEDITORUSERCALENDARMAPPER_H

#include <calendar/icalendaritemdatawidget.h>

#include <QWidget>
#include <QHash>
#include <QPointer>

/**
 * \file ./plugins/agendaplugin/calendaritemeditorusercalendarmapper.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/


namespace Agenda {
class CalendarItemModel;

namespace Internal {
namespace Ui {
class CalendarItemEditorUserCalendarMapper;
}

class CalendarItemEditorUserCalendarMapperWidget : public QWidget
{
    Q_OBJECT
public:
    CalendarItemEditorUserCalendarMapperWidget(QWidget *parent);
    ~CalendarItemEditorUserCalendarMapperWidget();

    void clear();
    void setCalendarItem(const Calendar::CalendarItem &item);

private Q_SLOTS:
    void onAgendaSelected(const int index);

private:
    Internal::Ui::CalendarItemEditorUserCalendarMapper *ui;
    Agenda::CalendarItemModel *m_Model;
};

}  // End namespace Internal

class CalendarItemEditorUserCalendarMapper : public Calendar::ICalendarItemDataWidget
{
    Q_OBJECT
public:
    explicit CalendarItemEditorUserCalendarMapper(QObject *parent = 0);
    ~CalendarItemEditorUserCalendarMapper();

    int insertionPlace() const;
    QWidget *createWidget(QWidget *parent = 0);
    bool setCalendarItem(const Calendar::CalendarItem &item);

    bool clear();
    bool submitChangesToCalendarItem(Calendar::CalendarItem &item);

private:
    QPointer<Internal::CalendarItemEditorUserCalendarMapperWidget> m_Widget;
};


}  // End namespace Agenda

#endif // CALENDARITEMEDITORUSERCALENDARMAPPER_H
