/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef AGENDA_AGENDAPREFERENCESPAGE_H
#define AGENDA_AGENDAPREFERENCESPAGE_H

#include <coreplugin/ioptionspage.h>

#include "ui_agendapreferencespage.h"

#include <QPointer>
#include <QObject>

/**
 * \file ./plugins/agendaplugin/agendapreferencespage.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;
}

namespace Agenda {
namespace Internal {

class AgendaPreferencesWidget : public QWidget, private Ui::AgendaPreferencesWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(AgendaPreferencesWidget)

public:
    explicit AgendaPreferencesWidget(QWidget *parent = 0);

    static void writeDefaultSettings(Core::ISettings *s);
    void setDataToUi();

public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);

protected:
    virtual void changeEvent(QEvent *e);
};


class AgendaPreferencesPage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    AgendaPreferencesPage(QObject *parent = 0);
    ~AgendaPreferencesPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    QString helpPage() {return QString();}

    static void writeDefaultSettings(Core::ISettings *s) {AgendaPreferencesWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);
private:
    QPointer<AgendaPreferencesWidget> m_Widget;
};

}  // End Internal
}  // End Agenda


#endif // AGENDA_AGENDAPREFERENCESPAGE_H
