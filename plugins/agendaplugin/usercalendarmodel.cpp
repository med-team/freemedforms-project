/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "usercalendarmodel.h"
#include "usercalendar.h"
#include "agendabase.h"
#include "agendacore.h"
#include "dayavailability.h"
#include "constants.h"

#include <coreplugin/icore.h>
#include <coreplugin/iuser.h>
#include <coreplugin/itheme.h>

#include <utils/log.h>
#include <translationutils/constants.h>
#include <translationutils/trans_agenda.h>
#include <translationutils/trans_current.h>

using namespace Agenda;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Core::ITheme *theme() {return Core::ICore::instance()->theme();}
static inline Core::IUser *user() {return Core::ICore::instance()->user();}
static inline Agenda::Internal::AgendaBase &base() {return Agenda::AgendaCore::instance().agendaBase();}

namespace Agenda {
namespace Internal {
class UserCalendarModelPrivate
{
public:
    void getUserCalendars()
    {
        qDeleteAll(m_UserCalendars);
        m_UserCalendars.clear();
        m_UserCalendars = base().getUserCalendars(m_UserUid);
    }

public:
    QString m_UserUid;
    QList<UserCalendar*> m_UserCalendars, m_RemovedCalendars;
};
} // End namespace Internal
} // End namespace Agenda


UserCalendarModel::UserCalendarModel(const QString &userUid, QObject *parent) :
    QAbstractTableModel(parent),
    d(new UserCalendarModelPrivate)
{
    if (userUid.isEmpty())
        d->m_UserUid = user()->uuid();
    else
        d->m_UserUid = userUid;
    d->getUserCalendars();
}

UserCalendarModel::~UserCalendarModel()
{
    if (d)
        delete d;
    d = 0;
}

int UserCalendarModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) return 0;

    return d->m_UserCalendars.count();
}

int UserCalendarModel::columnCount(const QModelIndex &) const
{
    return ColumnCount;
}

QVariant UserCalendarModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() < 0 || index.row() >= d->m_UserCalendars.count())
        return QVariant();

    const UserCalendar *u = d->m_UserCalendars.at(index.row());
    switch (role) {
    case Qt::DisplayRole:
    case Qt::EditRole:
    {
        switch (index.column()) {
        case Uid: return u->data(Constants::Db_CalId);
        case Label: return u->data(UserCalendar::Label);
        case ExtraLabel:
        {
            if (u->isDelegated()) {
                return QString("[%1] %2")
                        .arg(u->data(UserCalendar::UserOwnerFullName).toString())
                        .arg(u->data(UserCalendar::Label).toString());
            }
            if (u->isDefault() && d->m_UserCalendars.count() > 1) {
                return QString("%1 *")
                        .arg(u->data(UserCalendar::Label).toString());
            }
            return u->data(UserCalendar::Label);
        }
        case Description: return u->data(UserCalendar::Description);
        case Type: return u->data(UserCalendar::Type);
        case Status: return u->data(UserCalendar::Status);
        case IsDefault: return u->data(UserCalendar::IsDefault);
        case IsPrivate: return u->data(UserCalendar::IsPrivate);
        case Password: return u->data(UserCalendar::Password);
        case LocationUid: return u->data(UserCalendar::LocationUid);
        case DefaultDuration: return u->data(UserCalendar::DefaultDuration);
        }
        break;
    }
    case Qt::ToolTipRole:
    {
        switch (index.column()) {
        case Label: return u->data(UserCalendar::Label);
        }
        break;
    }
    case Qt::FontRole:
    {
        if (u->isDelegated()) {
            QFont italic;
            italic.setItalic(true);
            return italic;
        } else if (u->isDefault()) {
            QFont bold;
            bold.setBold(true);
            return bold;
        }
        break;
    }
    case Qt::DecorationRole:
    {
        if (!u->data(UserCalendar::AbsPathIcon).isNull()) {
            return theme()->icon(u->data(UserCalendar::AbsPathIcon).toString());
        }
        break;
    }
    default: return QVariant();
    } // End switch role

    return QVariant();
}

bool UserCalendarModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    if (index.row() < 0 || index.row() >= d->m_UserCalendars.count())
        return false;

    UserCalendar *userCalendar = d->m_UserCalendars.at(index.row());

    if (role==Qt::EditRole) {
        switch (index.column()) {
        case Label: userCalendar->setData(UserCalendar::Label, value); break;
        case Description: userCalendar->setData(UserCalendar::Description, value); break;
        case Type: userCalendar->setData(UserCalendar::Type, value); break;
        case Status: userCalendar->setData(UserCalendar::Status, value); break;
        case IsDefault:
        {
            bool isDefault = value.toBool();
            if (isDefault) {
                foreach(UserCalendar *calendar, d->m_UserCalendars)
                    calendar->setData(UserCalendar::IsDefault, false);
            }
            userCalendar->setData(UserCalendar::IsDefault, value);
            Q_EMIT defaultAgendaChanged(index);
            break;
        }
        case IsPrivate: userCalendar->setData(UserCalendar::IsPrivate, value); break;
        case Password: userCalendar->setData(UserCalendar::Password, value); break;
        case LocationUid: userCalendar->setData(UserCalendar::LocationUid, value); break;
        case DefaultDuration: userCalendar->setData(UserCalendar::DefaultDuration, value); break;
        default: return false;
        }
        Q_EMIT dataChanged(index, index);
        return true;
    }

    return false;
}

bool UserCalendarModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row+count);
    bool newIsDefault = (rowCount() == 0); // check if there already is an Agenda
    for(int i = 0 ; i < count; ++i) {
        UserCalendar *u = base().createEmptyCalendar(d->m_UserUid);
        u->setData(UserCalendar::IsDefault, newIsDefault);
        base().saveUserCalendar(u);
        d->m_UserCalendars.insert(row+i, u);
    }
    endInsertRows();
    return true;
}

bool UserCalendarModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row+count);
    for(int i = 0 ; i < count; ++i) {
        UserCalendar *u = d->m_UserCalendars.at(row);
        u->setData(Constants::Db_IsValid, 0);
        d->m_RemovedCalendars.append(u);
        d->m_UserCalendars.removeAt(row);
    }
    endRemoveRows();
    return true;
}

UserCalendar *UserCalendarModel::userCalendarAt(const int row) const
{
    if (row < 0 || row >= d->m_UserCalendars.count())
        return 0;
    return d->m_UserCalendars.at(row);
}

UserCalendar *UserCalendarModel::defaultUserCalendar() const
{
    for(int i = 0; i < d->m_UserCalendars.count(); ++i) {
        UserCalendar *u = d->m_UserCalendars.at(i);
        if (u->data(UserCalendar::IsDefault).toBool()) {
            return u;
        }
    }
    if (d->m_UserCalendars.count())
        return d->m_UserCalendars.at(0);
    return 0;
}

QModelIndex UserCalendarModel::defaultUserCalendarModelIndex() const
{
    for(int i = 0; i < d->m_UserCalendars.count(); ++i) {
        UserCalendar *u = d->m_UserCalendars.at(i);
        if (u->data(UserCalendar::IsDefault).toBool()) {
            return index(i, 0);
        }
    }
    if (d->m_UserCalendars.count())
        return index(0, 0);
    return QModelIndex();
}

void UserCalendarModel::updateUserCalendarChanged(const int row)
{
    Q_UNUSED(row);
    beginResetModel();
    endResetModel();
}

DayAvailabilityModel *UserCalendarModel::availabilityModel(const QModelIndex &index, QObject *parent) const
{
    if (!index.isValid())
        return 0;
    return availabilityModel(index.row(), parent);
}

DayAvailabilityModel *UserCalendarModel::availabilityModel(const int index, QObject *parent) const
{
    if (index < 0 || index >= d->m_UserCalendars.count())
        return 0;

    UserCalendar *u = d->m_UserCalendars.at(index);

    DayAvailabilityModel *model = new DayAvailabilityModel(parent);
    model->setUserCalendar(u);
    return model;
}

void UserCalendarModel::setPeopleList(const int row, const QList<Calendar::People> &peoples)
{
    if (row < 0 || row >= d->m_UserCalendars.count())
        return;
    UserCalendar *u = d->m_UserCalendars.at(row);
    u->setPeopleList(peoples);
}

void UserCalendarModel::addPeople(const int row, const Calendar::People &people)
{
    if (row < 0 || row >= d->m_UserCalendars.count())
        return;
    UserCalendar *u = d->m_UserCalendars.at(row);
    u->addPeople(people);
}

void UserCalendarModel::removePeople(const int row, const QString &uid)
{
    if (row < 0 || row >= d->m_UserCalendars.count())
        return;
    UserCalendar *u = d->m_UserCalendars.at(row);
    u->removePeople(uid);
}

bool UserCalendarModel::submit()
{
    bool ok = true;
    for(int i = 0; i < d->m_UserCalendars.count(); ++i) {
        if (!base().saveUserCalendar(d->m_UserCalendars.at(i)))
                ok = false;
    }
    for(int i = 0; i < d->m_RemovedCalendars.count(); ++i) {
        if (!base().saveUserCalendar(d->m_RemovedCalendars.at(i)))
                ok = false;
    }
    return ok;
}

void UserCalendarModel::revert()
{
    beginResetModel();
    d->getUserCalendars();
    endResetModel();
}
