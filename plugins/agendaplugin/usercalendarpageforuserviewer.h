/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef AGENDA_USERCALENDARPAGEFORUSERVIEWER_H
#define AGENDA_USERCALENDARPAGEFORUSERVIEWER_H

#include <usermanagerplugin/widgets/iuserviewerpage.h>

#include <QWidget>
#include <QPointer>
class QDataWidgetMapper;

/**
 * \file ./plugins/agendaplugin/usercalendarpageforuserviewer.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Agenda {
class UserCalendar;
class UserCalendarModel;
class UserCalendarModelFullEditorWidget;

namespace Internal {

class UserCalendarPageForUserViewerWidget : public UserPlugin::IUserViewerWidget
{
    Q_OBJECT
public:
    UserCalendarPageForUserViewerWidget(QWidget *parent = 0);
    ~UserCalendarPageForUserViewerWidget();

    void setParentPageId(const QString &id) {m_parentId=id;}
    const QString &parentUserViewerPageId() const {return m_parentId;}

    void setUserModel(UserPlugin::UserModel *model);
    void setUserIndex(const int index);
    void clear();
    bool submit();

private Q_SLOTS:
    void userChanged();

private:
    UserCalendarModelFullEditorWidget *m_Widget;
    UserPlugin::UserModel *m_UserModel;
    QString m_parentId;
};

class UserCalendarPageForUserViewer : public UserPlugin::IUserViewerPage
{
    Q_OBJECT
public:
    UserCalendarPageForUserViewer(QObject *parent = 0);
    ~UserCalendarPageForUserViewer();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    QWidget *createPage(QWidget *parent);
};

}  // End namespace Internal
}  // End namespace Agenda

#endif // AGENDA_USERCALENDARPAGEFORUSERVIEWER_H
