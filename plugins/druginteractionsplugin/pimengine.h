/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PIMENGINE_H
#define PIMENGINE_H

#include <drugsbaseplugin/idrugengine.h>

/**
 * \file ./plugins/druginteractionsplugin/pimengine.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugInteractions {
namespace Internal {
class PimEnginePrivate;

class PimEngine : public DrugsDB::IDrugEngine
{
    Q_OBJECT
public:
//    enum TypeOfIAM
//    {
//        NoIAM            = 0x0000,
//        Unknown          = 0x0001,
//        Information      = 0x0002,
//        InnDuplication   = 0x0004,
//        ClassDuplication = 0x0008,
//        Precaution       = 0x0010,
//        APrendreEnCompte = 0x0020,
//        P450             = 0x0100,
//        GPG              = 0x0200,
//        Deconseille      = 0x1000,
//        ContreIndication = 0x8000
//    };
//    Q_DECLARE_FLAGS(TypesOfIAM, TypeOfIAM);

    explicit PimEngine(QObject *parent = 0);
    ~PimEngine();

    bool init();
    bool isActive() const;
    bool isActiveByDefault() const {return false;}
    bool canComputeInteractions() const;

    bool isCalculatingDrugDrugInteractions() const {return false;}
    bool isCalculatingPatientDrugInteractions() const {return true;}
    bool isCalculatingPatientDrugAllergiesAndIntolerances() const {return false;}

    QString name() const;
    QString shortName() const;
    QString tooltip() const;
    QString uid() const;
    QIcon icon(const int size = 0) const;
    QString iconFullPath(const int size = 0) const;

    int calculateInteractions(const QVector<DrugsDB::IDrug *> &drugs);
    QVector<DrugsDB::IDrugInteraction *> getAllInteractionsFound();
    QVector<DrugsDB::IDrugInteractionAlert *> getAllAlerts(DrugsDB::DrugInteractionResult *addToResult);

    QAbstractItemModel *precautionModel() const {return 0;}

public Q_SLOTS:
    void setActive(bool state);

private Q_SLOTS:
    void drugsBaseChanged();

private:
    Internal::PimEnginePrivate *d;
};

}  // End namespace Internal
}  // End namespace DrugsDB

//Q_DECLARE_OPERATORS_FOR_FLAGS(DrugInteractions::DrugDrugInteractionEngine::TypesOfIAM)


#endif // PIMENGINE_H
