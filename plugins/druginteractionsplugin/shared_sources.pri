#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

DEFINES += DRUGINTERACTIONS_LIBRARY

include($${PWD}/../fmf_plugins.pri)
include(druginteractionsplugin_dependencies.pri)

HEADERS = $${PWD}/druginteractionsplugin.h $${PWD}/druginteractions_exporter.h \
    $${PWD}/druginteractionsbase.h \
    $${PWD}/allergyengine.h \
    $${PWD}/drugdruginteractionengine.h \
    $${PWD}/pimengine.h

SOURCES = $${PWD}/druginteractionsplugin.cpp \
    $${PWD}/druginteractionsbase.cpp \
    $${PWD}/allergyengine.cpp \
    $${PWD}/drugdruginteractionengine.cpp \
    $${PWD}/pimengine.cpp

# include translations
TRANSLATION_NAME = druginteractions
include($${SOURCES_ROOT_PATH}/buildspecs/translations.pri)
