/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DRUGINTERACTIONBASE_H
#define DRUGINTERACTIONBASE_H

#include <druginteractionsplugin/druginteractions_exporter.h>

/**
 * \file ./plugins/druginteractionsplugin/druginteractionsbase.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DrugInteractions {

class DrugInteractionBase
{
public:
    DrugInteractionBase();
};

}  // End namespace DrugInteractions

#endif // DRUGINTERACTIONBASE_H
