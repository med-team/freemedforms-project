/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "allergyengine.h"

#include <drugsbaseplugin/drugbasecore.h>
#include <drugsbaseplugin/drugsbase.h>
#include <drugsbaseplugin/constants.h>
#include <drugsbaseplugin/idruginteraction.h>
#include <drugsbaseplugin/druginteractionresult.h>
#include <drugsbaseplugin/idruginteractionalert.h>
#include <drugsbaseplugin/idrug.h>

#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/ipatient.h>
#include <coreplugin/isettings.h>

#include <utils/log.h>
#include <utils/global.h>

#include <translationutils/constanttranslations.h>

static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline Core::IPatient *patient()  { return Core::ICore::instance()->patient(); }
static inline DrugsDB::DrugsBase &drugsBase() {return DrugsDB::DrugBaseCore::instance().drugsBase();}
static inline Core::ISettings *settings() {return Core::ICore::instance()->settings();}

using namespace DrugInteractions::Internal;
using namespace DrugsDB;
using namespace DrugsDB::Internal;
using namespace Trans::ConstantTranslations;

namespace {


class AllergyInteraction : public IDrugInteraction {
public:
    enum InteractionLevel {
        InteractionLevel_High = 3,
        InteractionLevel_Medium = 2,
        InteractionLevel_Low = 1
    };

    AllergyInteraction(IDrugEngine *parentEngine) : IDrugInteraction(parentEngine), m_Engine(parentEngine) {}
    ~AllergyInteraction() {}

    void setValue(const int ref, const QVariant &value)
    {
        m_Infos.insert(ref, value);
    }

    QVariant value(const int ref) const
    {
        return m_Infos.value(ref);
    }

    IDrugEngine *engine() const {return m_Engine;}

    bool isDrugDrugInteraction() const {return false;}
    bool isPotentiallyInappropriate() const {return false;}

    QString type() const {return "bla bla";}

    QList<IDrug *> drugs() const {return m_InteractingDrugs;}

    QIcon icon(const int levelOfWarning, const int iconsize) const
    {
        Q_UNUSED(levelOfWarning);
        return theme()->icon(Constants::I_DRUGENGINE, Core::ITheme::IconSize(iconsize));
    }

    QString header(const QString &separator = QString::null) const
    {
        Q_UNUSED(separator);
        return QString("Allergy");
    }

    QString risk(const QString &lang = QString::null) const
    {
        Q_UNUSED(lang);
        return QString();
    }

    QString management(const QString &lang = QString::null) const
    {
        Q_UNUSED(lang);
        return QString();
    }

    QString referencesLink(const QString &lang = QString::null) const
    {
        Q_UNUSED(lang);
        return QString();
    }

    QString toHtml(bool detailled = false) const
    {
        Q_UNUSED(detailled);
        return QString();
    }

    int sortIndex() const {return 1;} //return m_Infos.value(Level).toInt();}

    void addInteractingDrug(IDrug *drug) {m_InteractingDrugs << drug;}

    QString levelToString(const int level)
    {
        switch (level) {
        case 3: return tkTr(Trans::Constants::HIGH);
        case 2: return tkTr(Trans::Constants::MEDIUM);
        case 1: return tkTr(Trans::Constants::LOW);
        }
        return QString();
    }

private:
    IDrugEngine *m_Engine;
    QHash<int, QVariant> m_Infos;
    QList<IDrug *> m_InteractingDrugs;
};


class AllergyAlert : public IDrugInteractionAlert
{
public:
    AllergyAlert(DrugInteractionResult *result, IDrugEngine *engine) :
            IDrugInteractionAlert(engine), m_Overridden(false), m_Result(result)
    {
    }

    ~AllergyAlert() {}

    QString engineUid() const {return Constants::ALLERGY_ENGINE_UID;}

    QIcon icon(const IDrug *drug, const DrugInteractionInformationQuery &query) const
    {
        Q_UNUSED(drug);
        if (!query.engineUid.isEmpty() && query.engineUid!=Constants::ALLERGY_ENGINE_UID) {
            return QIcon();
        }
        Core::ITheme::IconSize size = Core::ITheme::IconSize(query.iconSize);
        return theme()->icon(DrugsDB::Constants::I_ALLERGYENGINE, size);
    }

    QString message(const IDrug *drug, const DrugInteractionInformationQuery &query) const
    {
        Q_UNUSED(query);
        Q_UNUSED(drug);

























        return QString();
    }

    QString message(const DrugInteractionInformationQuery &query) const
    {
        Q_UNUSED(query);
        qWarning() << Q_FUNC_INFO;
        if (!m_Result->testedDrugs().isEmpty())
            return QString();
        return QString();
    }

    bool hasDynamicAlertWidget(const DrugInteractionInformationQuery &query) const
    {
        Q_UNUSED(query);
        return false;
    }

    QWidget *dynamicAlertWidget(const DrugInteractionInformationQuery &query, QWidget *parent = 0)
    {
        Q_UNUSED(query);
        Q_UNUSED(parent);
        return 0;
    }

    void setOverridden(bool overridden) {m_Overridden=overridden;}

    bool wasOverridden() const {return m_Overridden;}

private:
    bool m_Overridden;
    DrugInteractionResult *m_Result;
};

}

struct PimSource {
    int sourceId;
    QMultiHash<int, int> m_AtcIdsByPimId;
    QMultiHash<int, int> m_AtcIdsDoseRelatedByPimId;
    QMultiHash<int, int> m_AtcIdsIcdRelatedByPimId;
};

namespace DrugsDB {
namespace Internal {

class DrugAllergyEnginePrivate
{
public:
    QVector<IDrug *> m_TestedDrugs;
    QHash<int, PimSource> m_SourcesById;
    QMultiHash<int, int> m_FoundPimIdsBySources;
    bool m_LogChrono;
};
}
}

DrugAllergyEngine::DrugAllergyEngine(QObject *parent) :
    IDrugAllergyEngine(parent),
    m_DrugPrecautionModel(0)
{
    setObjectName("DrugAllergyEngine");
}

DrugAllergyEngine::~DrugAllergyEngine()
{
}

bool DrugAllergyEngine::init()
{
    m_IsActive = settings()->value(Constants::S_ACTIVATED_INTERACTION_ENGINES).toStringList().contains(Constants::ALLERGY_ENGINE_UID);
    connect(patient(), SIGNAL(currentPatientChanged()), this, SLOT(onCurrentPatientChanged()));
    connect(patient(), SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(refreshDrugsPrecautions(QModelIndex,QModelIndex)));
    return true;
}

bool DrugAllergyEngine::isActive() const
{
    return settings()->value(Constants::S_ACTIVATED_INTERACTION_ENGINES).toStringList().contains(Constants::ALLERGY_ENGINE_UID);
}

QString DrugAllergyEngine::name() const
{
    return QCoreApplication::translate(Constants::DRUGSBASE_TR_CONTEXT, Constants::ALLERGYENGINE_TEXT);
}

QString DrugAllergyEngine::shortName() const
{
    return QCoreApplication::translate(Constants::DRUGSBASE_TR_CONTEXT, Constants::ALLERGY_TEXT);
}

QString DrugAllergyEngine::tooltip() const
{
    return tr("Detects allergies and intolerances to medications");
}

QString DrugAllergyEngine::uid() const
{
    return Constants::ALLERGY_ENGINE_UID;
}

QIcon DrugAllergyEngine::icon(const int size) const
{
    return theme()->icon(DrugsDB::Constants::I_ALLERGYENGINE, Core::ITheme::IconSize(size));
}

QString DrugAllergyEngine::iconFullPath(const int size) const
{
    return theme()->iconFullPath(DrugsDB::Constants::I_ALLERGYENGINE, Core::ITheme::IconSize(size));
}

int DrugAllergyEngine::calculateInteractions(const QVector<IDrug *> &drugs)
{
    Q_UNUSED(drugs);
    m_Interactions.clear();
    return m_Interactions.count();
}

QVector<IDrugInteraction *> DrugAllergyEngine::getAllInteractionsFound()
{
    QVector<IDrugInteraction *> toReturn;

    return toReturn;
}

QVector<IDrugInteractionAlert *> DrugAllergyEngine::getAllAlerts(DrugInteractionResult *addToResult)
{
    QVector<IDrugInteractionAlert *> alerts;
    alerts << new AllergyAlert(addToResult, this);
    return alerts;
}

void DrugAllergyEngine::setActive(bool activate)
{
    if (isActive()==activate)
        return;
    if (activate) {
        settings()->appendToValue(Constants::S_ACTIVATED_INTERACTION_ENGINES, Constants::ALLERGY_ENGINE_UID);
    } else {
        QStringList l = settings()->value(Constants::S_ACTIVATED_INTERACTION_ENGINES).toStringList();
        l.removeAll(Constants::ALLERGY_ENGINE_UID);
        settings()->setValue(Constants::S_ACTIVATED_INTERACTION_ENGINES, l);
    }
}

void DrugAllergyEngine::clearDrugAllergyCache()
{
    m_ComputedInteractionCache.clear();
    Q_EMIT allergiesUpdated();
}

void DrugAllergyEngine::clearDrugIntoleranceCache()
{
    m_ComputedInteractionCache.clear();
    Q_EMIT intolerancesUpdated();
}

bool DrugAllergyEngine::needTest(const int typeOfInteraction, const int typeOfSubstrat) const
{
    for(int i=0; i<m_DoTests.count();++i) {
        const DrugAllergyEngineDoTest &t = m_DoTests.at(i);
        if (t.typeOfInteraction==typeOfInteraction && t.typeOfSubstrat==typeOfSubstrat)
            return true;
    }
    return false;
}

bool DrugAllergyEngine::test(const int typeOfInteraction, const int typeOfSubstrat, const QString &drugUid, const QStringList &toTest)
{
    if (typeOfSubstrat!=Drug && toTest.isEmpty()) {
        return false;
    }

    for(int i = 0; i < m_Cache.count(); ++i) {
        const DrugAllergyEngineCache &c = m_Cache.at(i);
        if (c.typeOfInteraction!=typeOfInteraction)
            continue;

        const QStringList &l = c.bySubstrat.values(typeOfSubstrat);
        if (l.isEmpty())
            continue;

        if (typeOfSubstrat==ClassInn) {
            foreach(const QString &test, toTest) {
                foreach(const QString &s, l) {
                    if (test.startsWith(s, Qt::CaseInsensitive)) {
                        m_ComputedInteractionCache.insertMulti(drugUid, typeOfInteraction);
                        return true;
                    }
                }
            }
        } else if (typeOfSubstrat==InnCode) {
            foreach(const QString &test, toTest) {
                if (l.contains(test, Qt::CaseInsensitive)) {
                    m_ComputedInteractionCache.insertMulti(drugUid, typeOfInteraction);
                    return true;
                }
            }
        } else if (typeOfSubstrat==Drug) {
            if (l.contains(drugUid, Qt::CaseInsensitive)) {
                m_ComputedInteractionCache.insertMulti(drugUid, typeOfInteraction);
                return true;
            }
        }
    }
    return false;
}

bool DrugAllergyEngine::has(const int typeOfInteraction, const QString &uid, const int drugSource)
{
    Q_UNUSED(drugSource);
    if (m_ComputedInteractionCache.contains(uid)) {
        QList<int> types = m_ComputedInteractionCache.values(uid);
        if (types.contains(typeOfInteraction))
            return true;
    }

    return false;
}

void DrugAllergyEngine::check(const int typeOfInteraction, const QString &uid, const QString &drugGlobalAtcCode, const int drugSource)
{
    Q_UNUSED(drugSource);
    if (m_ComputedInteractionCache.contains(uid)) {
        return;
    }

    bool found = false;

    if (needTest(typeOfInteraction, InnCode)) {
        QStringList inns = drugsBase().getDrugCompositionAtcCodes(uid);
        inns << drugGlobalAtcCode;
        if (test(typeOfInteraction, InnCode, uid, inns))
            found = true;
    }

    if (needTest(typeOfInteraction, ClassInn)) {
        QStringList atcs;
        atcs << drugsBase().getDrugCompositionAtcCodes(uid);
        atcs << drugGlobalAtcCode;
        atcs.removeAll("");
        if (test(typeOfInteraction, ClassInn, uid, atcs))
            found = true;
    }

    if (needTest(typeOfInteraction, Drug)) {
        if (test(typeOfInteraction, Drug, uid))
            found = true;
    }

    if (!found)
        m_ComputedInteractionCache.insert(uid, NoInteraction);

    if (m_ComputedInteractionCache.size() > 10000) {
        m_ComputedInteractionCache.remove(m_ComputedInteractionCache.begin().key());
    }
}

void DrugAllergyEngine::onCurrentPatientChanged()
{
    m_Interactions.clear();
    m_DoTests.clear();
    m_Cache.clear();
    m_ComputedInteractionCache.clear();
    m_ProcessedUid.clear();
    int currentRow = patient()->currentPatientIndex().row();
    refreshDrugsPrecautions(patient()->index(currentRow, 0), patient()->index(currentRow, patient()->columnCount()));
}


void DrugAllergyEngine::refreshDrugsPrecautions(const QModelIndex &topleft, const QModelIndex &bottomright)
{
    if (Utils::inRange(topleft.column(), bottomright.column(), Core::IPatient::DrugsUidAllergies)) {

        clearDrugAllergyCache();
    }

    if (Utils::inRange(topleft.column(), bottomright.column(), Core::IPatient::DrugsInnAllergies)) {

        clearDrugAllergyCache();
    }

    if (Utils::inRange(topleft.column(), bottomright.column(), Core::IPatient::DrugsAtcAllergies)) {
        qWarning() << "DrugAllergyEngine::refreshDrugsPrecautions  Core::IPatient::DrugsAtcAllergies";
        qWarning() << patient()->data(Core::IPatient::DrugsAtcAllergies).toStringList();

        if (!needTest(Allergy, InnCode)) {
            DrugAllergyEngineDoTest test;
            test.typeOfInteraction = Allergy;
            test.typeOfSubstrat = InnCode;
            m_DoTests.append(test);
        }

        bool updated = false;
        for(int i = 0; i < m_Cache.count(); ++i) {
            DrugAllergyEngineCache &c = m_Cache[i];
            if (c.typeOfInteraction==Allergy) {
                c.bySubstrat.clear();
                foreach(const QString &s, patient()->data(Core::IPatient::DrugsAtcAllergies).toStringList()) {
                    c.bySubstrat.insertMulti(InnCode, s);
                }
                updated = true;
                break;
            }
        }

        if (!updated) {
            DrugAllergyEngineCache c;
            c.typeOfInteraction = Allergy;
            foreach(const QString &s, patient()->data(Core::IPatient::DrugsAtcAllergies).toStringList()) {
                c.bySubstrat.insertMulti(InnCode, s);
            }
            m_Cache.append(c);
        }


        clearDrugAllergyCache();
    }

}

static void addBranch(QStandardItem *rootAllergies, QStandardItem *rootIntolerances, const QString &name,
                      const QStringList &allergies, const QStringList &intolerances, bool atc, bool uids,
                      const QBrush &allergiesBrush, const QBrush &intolerancesBrush)
{
    QStandardItem *allergiesItem = new QStandardItem(name);
    QStandardItem *intolerancesItem = new QStandardItem(name);
    allergiesItem->setForeground(allergiesBrush);
    intolerancesItem->setForeground(intolerancesBrush);
    rootAllergies->appendRow(allergiesItem);
    rootIntolerances->appendRow(intolerancesItem);
    QStringList sorted = allergies;
    qSort(sorted);
    foreach(const QString &item, sorted) {
        QString lbl;
        if (atc)
             lbl = drugsBase().getAtcLabel(item);
        else if (uids)
             lbl = drugsBase().getDrugName(item);
        else
            lbl = item;
        if (!lbl.isEmpty()) {
            QStandardItem *i = new QStandardItem(lbl);
            i->setForeground(allergiesBrush);
            i->setToolTip(tkTr(Trans::Constants::ALLERGY_TO_1).arg(lbl));
            allergiesItem->appendRow(i);
        }
    }
    sorted.clear();
    sorted = intolerances;
    qSort(sorted);
    foreach(const QString &item, sorted) {
        QString lbl;
        if (atc)
             lbl = drugsBase().getAtcLabel(item);
        else if (uids)
             lbl = drugsBase().getDrugName(item);
        else
            lbl = item;
        if (!lbl.isEmpty()) {
            QStandardItem *i = new QStandardItem(lbl);
            i->setToolTip(tkTr(Trans::Constants::INTOLERANCE_TO_1).arg(lbl));
            i->setForeground(intolerancesBrush);
            intolerancesItem->appendRow(i);
        }
    }
}

void DrugAllergyEngine::updateDrugsPrecautionsModel() const
{
    if (!m_DrugPrecautionModel)
        m_DrugPrecautionModel = new QStandardItemModel((DrugAllergyEngine*)this);
    m_DrugPrecautionModel->clear();
    QFont bold;
    bold.setBold(true);
    QStandardItem *rootItem = m_DrugPrecautionModel->invisibleRootItem();

    if (patient()->data(Core::IPatient::DrugsAllergiesWithoutPrecision).isNull() &&
            patient()->data(Core::IPatient::DrugsUidAllergies).isNull() &&
            patient()->data(Core::IPatient::DrugsAtcAllergies).isNull() &&
            patient()->data(Core::IPatient::DrugsInnAllergies).isNull() &&
            patient()->data(Core::IPatient::DrugsIntolerancesWithoutPrecision).isNull() &&
            patient()->data(Core::IPatient::DrugsUidIntolerances).isNull() &&
            patient()->data(Core::IPatient::DrugsAtcIntolerances).isNull() &&
            patient()->data(Core::IPatient::DrugsInnIntolerances).isNull() &&
            patient()->data(Core::IPatient::DrugsInnAtcIntolerances).isNull()) {
        QStandardItem *uniqueItem = new QStandardItem(tkTr(Trans::Constants::NO_ALLERGIES_INTOLERANCES));
        uniqueItem->setFont(bold);
        rootItem->appendRow(uniqueItem);
    } else {
        QStandardItem *allergiesItem = new QStandardItem(tkTr(Trans::Constants::KNOWN_ALLERGIES));
        QStandardItem *intolerancesItem = new QStandardItem(tkTr(Trans::Constants::KNOWN_INTOLERANCES));
        allergiesItem->setFont(bold);
        intolerancesItem->setFont(bold);
        QBrush allergiesBrush = QBrush(QColor(settings()->value(DrugsDB::Constants::S_ALLERGYBACKGROUNDCOLOR).toString()).darker(300));
        QBrush intolerancesBrush = QBrush(QColor(settings()->value(DrugsDB::Constants::S_INTOLERANCEBACKGROUNDCOLOR).toString()).darker(300));
        allergiesItem->setForeground(allergiesBrush);
        intolerancesItem->setForeground(intolerancesBrush);

        addBranch(allergiesItem, intolerancesItem, tkTr(Trans::Constants::ATC), patient()->data(Core::IPatient::DrugsAtcAllergies).toStringList(), patient()->data(Core::IPatient::DrugsAtcIntolerances).toStringList(), true, false, allergiesBrush, intolerancesBrush);
        addBranch(allergiesItem, intolerancesItem, tkTr(Trans::Constants::DRUGS), patient()->data(Core::IPatient::DrugsUidAllergies).toStringList(), patient()->data(Core::IPatient::DrugsUidIntolerances).toStringList(), false, true, allergiesBrush, intolerancesBrush);
        addBranch(allergiesItem, intolerancesItem, tkTr(Trans::Constants::INN), patient()->data(Core::IPatient::DrugsInnAllergies).toStringList(), patient()->data(Core::IPatient::DrugsInnIntolerances).toStringList(), false, false, allergiesBrush, intolerancesBrush);

        rootItem->appendRow(allergiesItem);
        rootItem->appendRow(intolerancesItem);
    }
}

QStandardItemModel *DrugAllergyEngine::drugPrecautionModel() const
{
    updateDrugsPrecautionsModel();
    return m_DrugPrecautionModel;
}


