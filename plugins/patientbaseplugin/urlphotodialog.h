/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Christian A Reiter                                  *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PATIENTS_URLPHOTODIALOG_H
#define PATIENTS_URLPHOTODIALOG_H

#include <QDialog>
#include <QNetworkAccessManager>
#include <QBuffer>
#include <QNetworkReply>

QT_BEGIN_NAMESPACE
class QPushButton;
QT_END_NAMESPACE

namespace Utils {
class HttpDownloader;
}

/**
 * \file ./plugins/patientbaseplugin/urlphotodialog.h
 * \author Christian A Reiter
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Patients {
namespace Internal {
namespace Ui {
class UrlPhotoDialog;
}

class UrlPhotoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UrlPhotoDialog(QWidget *parent = 0);
    ~UrlPhotoDialog();

    QPixmap photo() const;

private Q_SLOTS:
    void on_urlChanged(const QString &userUrlText);
    void downloadRequested();
    void onDownloadFinished();

    void updateDownloadProgress(qint64 bytesReceived, qint64 bytesTotal);

private:
    Ui::UrlPhotoDialog *ui;
    QPushButton *m_OkButton;
    int m_httpGetId;
    Utils::HttpDownloader *m_httpDld;
    QNetworkReply *m_reply;
    QByteArray m_picture;
    bool m_alreadyDownloading, m_alreadyUrlChecking;
    qint64 m_progressTotal;
};

} // namespace Internal
} // namespace Patients

#endif // PATIENTS_URLPHOTODIALOG_H
