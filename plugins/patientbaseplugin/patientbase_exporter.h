/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PATIENTBASEEXPORTER_H
#define PATIENTBASEEXPORTER_H

#include <qglobal.h>

#if defined(PATIENTBASE_LIBRARY)
#define PATIENT_EXPORT Q_DECL_EXPORT
#else
#define PATIENT_EXPORT Q_DECL_IMPORT
#endif

#endif  // PATIENTBASEEXPORTER_H
