/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PATIENTS_INTERNAL_PATIENTDATAEXTRACTORDIALOG_H
#define PATIENTS_INTERNAL_PATIENTDATAEXTRACTORDIALOG_H

#include <QDialog>
#include <QModelIndex>

/**
 * \file ./plugins/patientbaseplugin/patientdataextractordialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Patients {
namespace Internal {
class PatientDataExtractorDialogPrivate;

class PatientDataExtractorDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit PatientDataExtractorDialog(QWidget *parent = 0);
    ~PatientDataExtractorDialog();
    
    bool initialize();
    
Q_SIGNALS:
    
private Q_SLOTS:
    void refreshPatientModelFilter();
    void onPatientActivated(const QModelIndex &index);
    void onPatientRemoved(const QModelIndex &index);
    void onAddCurrentClicked();
    void onExportRequested();

private:
    PatientDataExtractorDialogPrivate *d;
};

} // namespace Internal
} // namespace Patients

#endif // PATIENTS_INTERNAL_PATIENTDATAEXTRACTORDIALOG_H

