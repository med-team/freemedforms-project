/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PATIENTCREATORWIZARD_H
#define PATIENTCREATORWIZARD_H

#include <patientbaseplugin/patientbase_exporter.h>

#include <QWizard>
#include <QWizardPage>

/**
 * \file ./plugins/patientbaseplugin/patientcreatorwizard.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Identity {
class IdentityEditorWidget;
}

namespace Patients {
class PatientModel;
class IdentityEditorWidget;
class IdentityPage;

class PATIENT_EXPORT PatientCreatorWizard : public QWizard
{
    Q_OBJECT
public:
    enum Pages {
         Page_Identity
    };

    PatientCreatorWizard(QWidget *parent);

protected:
    void done(int r);

private:
    IdentityPage *m_Page;
    bool m_Saved;
};

class IdentityPage: public QWizardPage
{
    Q_OBJECT
public:
    IdentityPage(QWidget *parent = 0);
    bool validatePage();
    bool isModified();

    QString lastInsertedUuid() const {return m_uuid;}

private:
    PatientModel *m_Model;
    Identity::IdentityEditorWidget *m_Identity;
    QString m_uuid;
};


} // End namespace Patients

#endif // PATIENTCREATORWIZARD_H
