/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Christian A Reiter                                  *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PATIENTS_URLPHOTOPROVIDER_H
#define PATIENTS_URLPHOTOPROVIDER_H

#include <coreplugin/iphotoprovider.h>
#include <QString>

/**
 * \file ./plugins/patientbaseplugin/urlphotoprovider.h
 * \author Christian A Reiter
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Patients {
namespace Internal {
class UrlPhotoProvider : public Core::IPhotoProvider
{
    Q_OBJECT
public:
    explicit UrlPhotoProvider(QObject *parent = 0);
    ~UrlPhotoProvider();

    QString id() const;
    QString name() const;
    QString displayText() const;
    bool isActive() const;
    bool isEnabled() const;
    int priority() const;

public Q_SLOTS:
    void startReceivingPhoto();
};

} // namespace Internal
} // namespace Patients

#endif // PATIENTS_URLPHOTOPROVIDER_H
