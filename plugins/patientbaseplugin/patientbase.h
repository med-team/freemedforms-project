/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PATIENTBASE_H
#define PATIENTBASE_H

#include <utils/database.h>

#include <coreplugin/isettings.h>

#include <patientbaseplugin/patientbase_exporter.h>

#include <QObject>
#include <QString>
#include <QDate>

/**
 * \file ./plugins/patientbaseplugin/patientbase.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Patients {
class PatientCore;
namespace Internal {
class PatientBasePrivate;
class PatientData;

class PATIENT_EXPORT PatientBase : public QObject, public Utils::Database
{
    Q_OBJECT
    friend class Patients::PatientCore;

protected:
    PatientBase(QObject *parent = 0);
    bool initialize();

public:
    // Constructor
    static PatientBase *instance();
    virtual ~PatientBase();

//    // initialize
//    bool initialize(Core::ISettings *settings);
    bool isInitialized() const {return m_initialized;}

    bool createVirtualPatient(const QString &usualName, const QString &otherNames, const QString &firstname,
                              const QString &gender, const int title, const QDate &dob,
                              const QString &country, const QString &note,
                              const QString &street, const QString &zip, const QString &city,
                              QString uuid, const int lkid,
                              const QString &photoFile = QString(), const QDate &death = QDate());

    QString patientUuid(const QString &usualname, const QString &othernames, const QString &firstname,
                        const QString &gender, const QDate &dob) const;
    bool isPatientExists(const QString &usualname, const QString &othernames, const QString &firstname,
                         const QString &gender, const QDate &dob) const;

    bool setPatientActiveProperty(const QString &uuid, bool active);

    void toTreeWidget(QTreeWidget *tree) const;

private:
    bool createDatabase(const QString &connectionName, const QString &dbName,
                          const QString &pathOrHostName,
                          TypeOfAccess access, AvailableDrivers driver,
                          const QString &login, const QString &pass,
                          const int port,
                          CreationOption createOption
                         );

private Q_SLOTS:
    void onCoreDatabaseServerChanged();
    void onCoreFirstRunCreationRequested();

private:
    bool m_initialized;
    static PatientBase *m_Instance;
};

}  // End namespace Internal
}  // End namespace Patients

#endif // PATIENTBASE_H
