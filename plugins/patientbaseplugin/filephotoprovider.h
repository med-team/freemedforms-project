/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Christian A. Reiter <christian.a.reiter@gmail.com>
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PATIENTS_FILEPHOTOPROVIDER_H
#define PATIENTS_FILEPHOTOPROVIDER_H

#include <coreplugin/iphotoprovider.h>

/**
 * \file ./plugins/patientbaseplugin/filephotoprovider.h
 * \author Christian A. Reiter <christian.a.reiter@gmail.com>
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Patients {

class FilePhotoProvider : public Core::IPhotoProvider
{
    Q_OBJECT
public:
    explicit FilePhotoProvider(QObject *parent);
    ~FilePhotoProvider();

    QString id() const;
    QString name() const;
    QString displayText() const;
    bool isEnabled() const;
    bool isActive() const;
    int priority() const;

public Q_SLOTS:
    void startReceivingPhoto();

};

} // namespace Patients

#endif  // PATIENTS_FILEPHOTOPROVIDER_H

