/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PATIENTSEARCHMODE_H
#define PATIENTSEARCHMODE_H

#include <coreplugin/modemanager/imode.h>

#include <QObject>

QT_BEGIN_NAMESPACE
class QIcon;
class QWidget;
QT_END_NAMESPACE

namespace Patients {
class PatientSelector;

namespace Internal {

class PatientSearchMode : public Core::IMode
{
    Q_OBJECT
public:
    PatientSearchMode(QObject *parent);
    ~PatientSearchMode();

    void postCoreInitialization();

private:
    PatientSelector *m_Selector;
};

} // namespace Internal
} // namespace Patients

#endif // PATIENTSEARCHMODE_H
