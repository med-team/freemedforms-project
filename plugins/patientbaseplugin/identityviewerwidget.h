/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric MAEKER
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PATIENTS_INTERNAL_IDENTITYVIEWERWIDGET_H
#define PATIENTS_INTERNAL_IDENTITYVIEWERWIDGET_H

#include <QWidget>
QT_BEGIN_NAMESPACE
class QModelIndex;
QT_END_NAMESPACE

/**
 * \file ./plugins/patientbaseplugin/identityviewerwidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/
namespace Patients {
class PatientModel;

namespace Internal {
class IdentityViewerWidgetPrivate;

class IdentityViewerWidget : public QWidget
{
    Q_OBJECT
public:
    explicit IdentityViewerWidget(QWidget *parent = 0);
    ~IdentityViewerWidget();

    bool initialize();

    Patients::PatientModel *patientModel() const;

//Q_SIGNALS:

private Q_SLOTS:
    void getPatientForms();

public Q_SLOTS:
    void setCurrentPatientModel(Patients::PatientModel *model);
    void setCurrentIndex(const QModelIndex &patientIndex);

private:
    //void IdentityWidget::changeEvent(QEvent *e)

private:
    Internal::IdentityViewerWidgetPrivate *d;
};

} // namespace Internal
} // namespace Patients

#endif // PATIENTS_INTERNAL_IDENTITYVIEWERWIDGET_H

