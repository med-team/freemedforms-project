/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PATIENTS_PATIENTCORE_H
#define PATIENTS_PATIENTCORE_H

#include <patientbaseplugin/patientbase_exporter.h>
#include <QObject>

/**
 * \file ./plugins/patientbaseplugin/patientcore.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Patients {
class PatientModel;
class PatientSelector;
class PatientBar;
namespace Internal {
class PatientBase;
class PatientBasePlugin;
class PatientWidgetManager;
class PatientCorePrivate;
}  // namespace Internal

class PATIENT_EXPORT PatientCore : public QObject
{
    Q_OBJECT
    friend class Patients::Internal::PatientBasePlugin;
    friend class Patients::PatientSelector;

protected:
    explicit PatientCore(QObject *parent = 0);
    bool initialize();
    void registerPatientModel(PatientModel *model);
    bool createDefaultVirtualPatients() const;

public:
    static PatientCore *instance() {return _instance;}
    ~PatientCore();

    Internal::PatientBase *patientBase() const;
    Internal::PatientWidgetManager *patientWidgetManager() const;

    PatientBar *patientBar() const;

public Q_SLOTS:
    bool setCurrentPatientUuid(const QString &uuid);
    void refreshAllPatientModel() const;
    bool removePatient(const QString &uuid);

Q_SIGNALS:

private Q_SLOTS:
    void postCoreInitialization();

private:
    Internal::PatientCorePrivate *d;
    static PatientCore *_instance;
};

} // namespace Patients

#endif  // PATIENTS_PATIENTCORE_H

