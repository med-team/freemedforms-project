/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
/*!
 * \class Patients::FilePhotoProvider
 * \brief Provides a Filedialog for selecting a patient photo
 *
 * This class implements the Core::IPhotoProvider plugin extension.
 * \sa Core::IPhotoProvider
 */

#include "filephotoprovider.h"
#include "constants_settings.h"

#include <translationutils/constants.h>
#include <coreplugin/isettings.h>
#include <coreplugin/icore.h>

#include <QDebug>
#include <QFileDialog>
#include <QApplication>

using namespace Patients;
using namespace Trans::ConstantTranslations;

/*! Constructor of the Patients::FilePhotoProvider class */
FilePhotoProvider::FilePhotoProvider(QObject *parent) :
    IPhotoProvider(parent)
{
}

/*! Destructor of the Patients::FilePhotoProvider class */
FilePhotoProvider::~FilePhotoProvider()
{
}

QString FilePhotoProvider::id() const
{
    return "file";
}

QString FilePhotoProvider::name() const
{
    return "file";
}

QString FilePhotoProvider::displayText() const
{
    return tr("Choose from file...");
}

/*! Returns always true */
bool FilePhotoProvider::isEnabled() const
{
    return true;
}

/*! Returns always true */
bool FilePhotoProvider::isActive() const
{
    return true;
}

/*! Returns a low priority to be listed at last/fallback position. */
int FilePhotoProvider::priority() const
{
    return 100;
}

void FilePhotoProvider::startReceivingPhoto()
{
    QString fileName = QFileDialog::getOpenFileName(QApplication::activeWindow(), tr("Choose a photo"),
                                                    QDir::homePath(),
                                                    "Image (*.png *.jpg *.jpeg *.gif *.tiff)");
    if (!fileName.isEmpty()) {
        QPixmap photo;
        photo.load(fileName);
        Q_EMIT photoReady(photo);
    }
}
