/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "patientbaseplugin.h"
#include "patientcore.h"
#include "patientbase.h"
#include "patientwidgetmanager.h"
#include "patientsearchmode.h"
#include "patientbasepreferencespage.h"
#include "patientmodel.h"
#include "filephotoprovider.h"
#include "urlphotoprovider.h"

#include <coreplugin/icore.h>
#include <coreplugin/iuser.h>
#include <coreplugin/itheme.h>
#include <coreplugin/isettings.h>
#include <coreplugin/translators.h>
#include <coreplugin/modemanager/modemanager.h>
#include <coreplugin/icommandline.h>

#include <utils/log.h>

#include <QtPlugin>
#include <QDir>
#include <QProgressDialog>

#include <QDebug>

using namespace Patients;
using namespace Internal;

static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline Core::IUser *user()  { return Core::ICore::instance()->user(); }
static inline void messageSplash(const QString &s) {theme()->messageSplashScreen(s); }
static inline Core::ISettings *settings()  { return Core::ICore::instance()->settings(); }
static inline Core::ICommandLine *commandLine() {return Core::ICore::instance()->commandLine();}

static inline Patients::PatientCore *patientCore() {return Patients::PatientCore::instance();}

PatientBasePlugin::PatientBasePlugin() :
    m_Mode(0),
    prefpage(0)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating PatientBasePlugin";

    Core::ICore::instance()->translators()->addNewTranslator("plugin_patientbase");

    prefpage = new PatientBasePreferencesPage(this);
    addObject(prefpage);

    new PatientCore(this);

}

PatientBasePlugin::~PatientBasePlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool PatientBasePlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "PatientBasePlugin::initialize";
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);

    messageSplash(tr("Initializing patients database plugin..."));

    if (!patientCore()->initialize())
        return false;

    FilePhotoProvider *filePhotoProvider = new FilePhotoProvider(this);
    addAutoReleasedObject(filePhotoProvider);
    UrlPhotoProvider *urlPhotoProvider = new UrlPhotoProvider(this);
    addAutoReleasedObject(urlPhotoProvider);
    return true;
}

void PatientBasePlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "PatientBasePlugin::extensionsInitialized";

    messageSplash(tr("Initializing patients database plugin..."));

    if (!user())
        return;
    if (user()->uuid().isEmpty())
        return;

    QProgressDialog dlg(tr("Initializing patient database..."), tr("Please wait"), 0, 0);
    dlg.setWindowModality(Qt::WindowModal);
    dlg.setMinimumDuration(1000);
    dlg.show();
    dlg.setFocus();
    dlg.setValue(0);

    if (commandLine()->value(Core::ICommandLine::CreateVirtuals).toBool()) {
        if (!patientCore()->createDefaultVirtualPatients())
            LOG_ERROR("Unable to create default virtual patients");
    }

    prefpage->checkSettingsValidity();
    settings()->sync();

    m_Mode = new PatientSearchMode(this);
    m_Mode->postCoreInitialization();
    addObject(m_Mode);

}

ExtensionSystem::IPlugin::ShutdownFlag PatientBasePlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    if (m_Mode) {
        removeObject(m_Mode);
        delete m_Mode;
        m_Mode = 0;
    }
    if (prefpage) {
        removeObject(prefpage);
        delete prefpage;
        prefpage = 0;
    }
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(PatientBasePlugin)
