/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PATIENTBASE_PATIENTBAR_H
#define PATIENTBASE_PATIENTBAR_H

#include <coreplugin/ipatientbar.h>

#include <QModelIndex>

/**
 * \file ./plugins/patientbaseplugin/patientbar.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Patients {
class PatientModel;
class PatientCore;

namespace Internal {
class PatientBarPrivate;
}

class PatientBar : public Core::IPatientBar
{
    Q_OBJECT
    friend class Patients::PatientCore;

protected:
    PatientBar(QWidget *parent = 0);

public:
    ~PatientBar();

    void addBottomWidget(QWidget *widget);
    void showMessage(const QString &message, int duration_ms = 2000, const QString &css = QString::null);

private Q_SLOTS:
    void onCurrentPatientChanged();
    void onPatientDataChanged(const QModelIndex &top, const QModelIndex &bottom);

protected:
    void paintEvent(QPaintEvent *);
    void changeEvent(QEvent *event);

private:
    Internal::PatientBarPrivate *d;
};

}  // End namespace Patients

#endif // PATIENTBASE_PATIENTBAR_H
