/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "patientselector.h"
#include "patientmodel.h"
#include "patientbar.h"
#include "constants_menus.h"
#include "constants_settings.h"
#include "patientcore.h"

#include "ui_patientselector.h"

#include <coreplugin/icore.h>
#include <coreplugin/iuser.h>
#include <coreplugin/itheme.h>
#include <coreplugin/ipatient.h>
#include <coreplugin/isettings.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/ipatient.h>
#include <coreplugin/imainwindow.h>
#include <coreplugin/constants_icons.h>
#include <coreplugin/constants_menus.h>
#include <coreplugin/modemanager/modemanager.h>

#include <utils/log.h>
#include <utils/global.h>
#include <utils/widgets/datetimedelegate.h>
#include <translationutils/constanttranslations.h>

#include <QToolButton>
#include <QMenu>

#include <QDebug>

using namespace Patients;
using namespace Trans::ConstantTranslations;

static inline Core::ISettings *settings() {return Core::ICore::instance()->settings();}
static inline Core::ITheme *theme() {return Core::ICore::instance()->theme();}
static inline Core::IUser *user() {return Core::ICore::instance()->user();}
static inline Core::IPatient *patient() {return Core::ICore::instance()->patient();}
static inline Core::IMainWindow *mainWindow() {return Core::ICore::instance()->mainWindow();}
static inline Core::ActionManager *actionManager() { return Core::ICore::instance()->actionManager(); }
static inline Patients::PatientCore *patientCore() {return Patients::PatientCore::instance();}

namespace Patients {
namespace Internal {
class PatientSelectorPrivate
{
public:
    PatientSelectorPrivate(PatientSelector *parent) :
            ui(new Ui::PatientSelector),
            m_Model(0),
            m_SearchToolButton(0),
            m_NavigationToolButton(0),
            m_NavigationMenu(0),
            m_SearchMethod(-1),
            m_LastSearch(QString("_##_")),  // Force a first refresh when calling refreshFilter() with an empty QString
            m_refreshMethod(PatientSelector::WhileTyping),
            m_SetActivatedPatientAsCurrent(true),
            q(parent)
    {
    }

    ~PatientSelectorPrivate()
    {
        delete ui;
    }

    void createSearchToolButtons()
    {
        m_SearchToolButton = new QToolButton();   // parent object will be redefined
        m_SearchToolButton->setPopupMode(QToolButton::InstantPopup);
        m_SearchToolButton->setIcon(theme()->icon(Core::Constants::ICONSEARCH));

        QStringList actions;
        actions
                << Constants::A_SEARCH_PATIENTS_BY_NAME
                << Constants::A_SEARCH_PATIENTS_BY_FIRSTNAME
                << Constants::A_SEARCH_PATIENTS_BY_NAMEFIRSTNAME
                << Constants::A_SEARCH_PATIENTS_BY_DOB;

        QList<QAction *> actionList;
        foreach(const QString &a, actions) {
            Core::Command *cmd = actionManager()->command(Core::Id(a));
            m_SearchToolButton->addAction(cmd->action());
            actionList << cmd->action();
        }

        int id = settings()->value(Constants::S_SEARCHMETHOD, 0).toInt();
        if (id < actionList.count() && id >= 0) {
            actionList.at(id)->trigger();
            actionList.at(id)->setChecked(true);
            m_SearchToolButton->setDefaultAction(actionList.at(id));
            m_SearchMethod = id;
        } else {
            m_SearchMethod = 0;
        }

        ui->searchLine->setLeftButton(m_SearchToolButton);

        m_NavigationToolButton = new QToolButton(q);   // parent object will be redefined
        m_NavigationToolButton->setPopupMode(QToolButton::InstantPopup);
        m_NavigationToolButton->setIcon(theme()->icon(Core::Constants::ICONPATIENT));
        m_NavigationMenu = new QMenu(m_NavigationToolButton);
        m_NavigationToolButton->setMenu(m_NavigationMenu);

        ui->searchLine->setRightButton(m_NavigationToolButton);
    }

    void saveSettings()
    {
        settings()->setValue(Constants::S_SEARCHMETHOD, m_SearchMethod);
    }

public:
    Ui::PatientSelector *ui;
    PatientModel *m_Model;
    PatientSelector::FieldsToShow m_Fields;
    QToolButton *m_SearchToolButton, *m_NavigationToolButton;
    QMenu *m_NavigationMenu;
    int m_SearchMethod;
    QString m_LastSearch;
    PatientSelector::RefreshSearchResult m_refreshMethod;
    bool m_SetActivatedPatientAsCurrent;

private:
    PatientSelector *q;
};
}  // End namespace Internal
}  // End namespace Patients


PatientSelector::PatientSelector(QWidget *parent, const FieldsToShow fields) :
    QWidget(parent),
    d(new Internal::PatientSelectorPrivate(this))
{
    d->ui->setupUi(this);
    d->ui->searchLine->setDelayedSignals(true);

    PatientModel *model = new PatientModel(this);
    setPatientModel(model);
    patientCore()->registerPatientModel(model);

    d->ui->tableView->setItemDelegateForColumn(Core::IPatient::DateOfBirth, new Utils::DateTimeDelegate(this, true));

    d->createSearchToolButtons();

    connect(d->m_NavigationToolButton->menu(), SIGNAL(aboutToShow()), this, SLOT(updateNavigationButton()));
    connect(d->ui->tableView->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)), this, SLOT(changeIdentity(QModelIndex,QModelIndex)));
    connect(d->ui->tableView, SIGNAL(activated(QModelIndex)), this, SLOT(onPatientActivated(QModelIndex)));

    updatePatientActions(QModelIndex());
    if (fields == None) {
        d->m_Fields = FieldsToShow(settings()->value(Constants::S_SELECTOR_FIELDSTOSHOW, Default).toInt());
    } else {
        d->m_Fields = fields;
    }
    connect(user(), SIGNAL(userChanged()), this, SLOT(onUserChanged()));

    if (settings()->value(Constants::S_SEARCHWHILETYPING).toBool())
        setRefreshSearchResultMethod(WhileTyping);
    else
        setRefreshSearchResultMethod(ReturnPress);

    connect(patient(), SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(onPatientDataChanged(QModelIndex,QModelIndex)));
}

PatientSelector::~PatientSelector()
{
    if (d) {
        d->saveSettings();
        delete d;
        d = 0;
    }
}

void PatientSelector::initialize()
{
}

void PatientSelector::updateNavigationButton()
{
    d->m_NavigationMenu->clear();

    Core::Command *cmd = actionManager()->command(Core::Constants::A_PATIENT_NEW);
    d->m_NavigationMenu->addAction(cmd->action());
    d->m_NavigationMenu->addSeparator();

    Core::ActionContainer *navMenu = actionManager()->actionContainer(Core::Constants::M_PATIENTS_NAVIGATION);
    if (!navMenu)
        return;
    for(int i = 0; i < navMenu->menu()->actions().count(); ++i) {
        d->m_NavigationMenu->addAction(navMenu->menu()->actions().at(i));
    }
}

void PatientSelector::setSearchMode(const int search)
{
    d->m_SearchMethod = search;
}

void PatientSelector::setPatientModel(PatientModel *m)
{
    Q_ASSERT(m);
    d->m_Model = m;
    d->ui->tableView->setModel(m);
    setFieldsToShow(d->m_Fields);

    d->ui->tableView->horizontalHeader()->setStretchLastSection(false);
#if QT_VERSION < 0x050000
    d->ui->tableView->horizontalHeader()->setResizeMode(Core::IPatient::UsualName, QHeaderView::Stretch);
    d->ui->tableView->horizontalHeader()->setResizeMode(Core::IPatient::OtherNames, QHeaderView::Stretch);
    d->ui->tableView->horizontalHeader()->setResizeMode(Core::IPatient::Firstname, QHeaderView::Stretch);
    d->ui->tableView->horizontalHeader()->setResizeMode(Core::IPatient::FullName, QHeaderView::ResizeToContents);
    d->ui->tableView->horizontalHeader()->setResizeMode(Core::IPatient::IconizedGender, QHeaderView::ResizeToContents);
    d->ui->tableView->horizontalHeader()->setResizeMode(Core::IPatient::Title, QHeaderView::ResizeToContents);
    d->ui->tableView->horizontalHeader()->setResizeMode(Core::IPatient::DateOfBirth, QHeaderView::ResizeToContents);
    d->ui->tableView->horizontalHeader()->setResizeMode(Core::IPatient::FullAddress, QHeaderView::Stretch);
    d->ui->tableView->horizontalHeader()->setResizeMode(Core::IPatient::PractitionnerLkID, QHeaderView::ResizeToContents);
#else
    d->ui->tableView->horizontalHeader()->setSectionResizeMode(Core::IPatient::UsualName, QHeaderView::Stretch);
    d->ui->tableView->horizontalHeader()->setSectionResizeMode(Core::IPatient::OtherNames, QHeaderView::Stretch);
    d->ui->tableView->horizontalHeader()->setSectionResizeMode(Core::IPatient::Firstname, QHeaderView::Stretch);
    d->ui->tableView->horizontalHeader()->setSectionResizeMode(Core::IPatient::FullName, QHeaderView::ResizeToContents);
    d->ui->tableView->horizontalHeader()->setSectionResizeMode(Core::IPatient::IconizedGender, QHeaderView::ResizeToContents);
    d->ui->tableView->horizontalHeader()->setSectionResizeMode(Core::IPatient::Title, QHeaderView::ResizeToContents);
    d->ui->tableView->horizontalHeader()->setSectionResizeMode(Core::IPatient::DateOfBirth, QHeaderView::ResizeToContents);
    d->ui->tableView->horizontalHeader()->setSectionResizeMode(Core::IPatient::FullAddress, QHeaderView::Stretch);
    d->ui->tableView->horizontalHeader()->setSectionResizeMode(Core::IPatient::PractitionnerLkID, QHeaderView::ResizeToContents);
#endif

    d->ui->numberOfPatients->setText(QString::number(m->numberOfFilteredPatients()));
    d->ui->identity->setCurrentPatientModel(m);
    connect(d->m_Model, SIGNAL(currentPatientChanged(QModelIndex)), this, SLOT(setSelectedPatient(QModelIndex)));
}

bool PatientSelector::setFilterPatientModel(const QString &name, const QString &firstName, const QDate &dateOfBirth) const
{
    Q_UNUSED(dateOfBirth);
    d->m_Model->setFilter(name, firstName);
    return true;
}

void PatientSelector::setFieldsToShow(const FieldsToShow fields)
{
    d->m_Fields = fields;
    for(int i=0; i < Core::IPatient::NumberOfColumns; ++i) {
        d->ui->tableView->hideColumn(i);
    }
    if (fields & PatientSelector::UsualName) {
        d->ui->tableView->showColumn(Core::IPatient::UsualName);
    }
    if (fields & PatientSelector::OtherNames) {
        d->ui->tableView->showColumn(Core::IPatient::OtherNames);
    }
    if (fields & PatientSelector::FirstName) {
        d->ui->tableView->showColumn(Core::IPatient::Firstname);
    }
    if (fields & PatientSelector::FullName) {
        d->ui->tableView->showColumn(Core::IPatient::FullName);
    }
    if (fields & PatientSelector::Gender) {
        d->ui->tableView->showColumn(Core::IPatient::IconizedGender);
    }
    if (fields & PatientSelector::Title) {
        d->ui->tableView->showColumn(Core::IPatient::Title);
    }
    if (fields & PatientSelector::DateOfBirth) {
        d->ui->tableView->showColumn(Core::IPatient::DateOfBirth);
    }
    if (fields & PatientSelector::FullAddress) {
        d->ui->tableView->showColumn(Core::IPatient::FullAddress);
    }
}

void PatientSelector::setRefreshSearchResultMethod(RefreshSearchResult method)
{
    disconnect(d->ui->searchLine, SIGNAL(textChanged(QString)), this, SLOT(refreshFilter()));
    disconnect(d->ui->searchLine, SIGNAL(textEdited(QString)), this, SLOT(refreshFilter()));
    d->m_refreshMethod = method;
    if (method == WhileTyping)
        connect(d->ui->searchLine, SIGNAL(textChanged(QString)), this, SLOT(refreshFilter()));
    else
        connect(d->ui->searchLine, SIGNAL(returnPressed()), this, SLOT(refreshFilter()));
}


void PatientSelector::setOnPatientActivatedSetAsCurrent(bool setAsCurrent)
{
    d->m_SetActivatedPatientAsCurrent = setAsCurrent;
}

QString PatientSelector::selectedPatientUid() const
{
    return d->m_Model->patientUuid(d->ui->tableView->currentIndex());
}


void PatientSelector::setSelectedPatient(const QModelIndex &index)
{
    d->ui->tableView->selectRow(index.row());
    updatePatientActions(index);
}


void PatientSelector::setSelectedPatient(int row)
{
    d->ui->tableView->selectRow(row);
}


void PatientSelector::changeIdentity(const QModelIndex &current, const QModelIndex &previous)
{
    Q_UNUSED(previous);
    d->ui->identity->setCurrentIndex(current);
    updatePatientActions(current);
}

/*!
 * \brief Updates (enables/disables) the corresponding QActions for the given patient
 * \internal
 * Receive index from the Core::IPatient model (if the proxy model is in use you must
 * map the proxy index to the Core::IPatient index).
 */
void PatientSelector::updatePatientActions(const QModelIndex &index)
{
    const bool enabled = index.isValid();
    actionManager()->command(Core::Constants::A_PATIENT_VIEWIDENTITY)->action()->setEnabled(enabled);
    actionManager()->command(Core::Constants::A_PATIENT_REMOVE)->action()->setEnabled(enabled);
}


void PatientSelector::refreshFilter()
{
    if (!d->m_Model)
        return;
    QString text = d->ui->searchLine->text();
    if (text == d->m_LastSearch)
        return;
    d->m_LastSearch = text;
    QString name, firstname;
    switch (d->m_SearchMethod) {
    case SearchByName: name = text; break;
    case SearchByNameFirstname: name = text.mid(0,text.indexOf(";")).trimmed(); firstname = text.right(text.indexOf(";")); break;
    case SearchByFirstname: firstname = text; break;
    case SearchByDOB: break;
    }
    d->m_Model->setFilter(name, firstname);
    d->ui->numberOfPatients->setText(QString::number(d->m_Model->numberOfFilteredPatients()));
}


void PatientSelector::onPatientActivated(const QModelIndex &index)
{
    if (!d->m_SetActivatedPatientAsCurrent)
        return;


    mainWindow()->startProcessingSpinner();

    const QString &uuid = d->m_Model->patientUuid(index);
    if (!patientCore()->setCurrentPatientUuid(uuid)) {
        LOG_ERROR("Unable to select the patient: " + uuid);
        mainWindow()->endProcessingSpinner();
    }
}


void PatientSelector::onPatientDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
    if (topLeft.row() == bottomRight.row()) {
        if (IN_RANGE(topLeft.column(), bottomRight.column(), Core::IPatient::FullName))
            d->m_Model->refreshModel();
        else if (IN_RANGE(topLeft.column(), bottomRight.column(), Core::IPatient::FullAddress))
            d->m_Model->refreshModel();
        else if (IN_RANGE(topLeft.column(), bottomRight.column(), Core::IPatient::DateOfBirth))
            d->m_Model->refreshModel();
        else if (IN_RANGE(topLeft.column(), bottomRight.column(), Core::IPatient::DateOfDeath))
            d->m_Model->refreshModel();
        return;
    }
    d->m_Model->refreshModel();
}


void PatientSelector::onUserChanged()
{
    d->ui->searchLine->clear();
    refreshFilter();
    initialize();
}


bool PatientSelector::event(QEvent *event)
{
    switch (event->type()) {
    case QEvent::LanguageChange:
        d->ui->retranslateUi(this);
        break;
    case QEvent::Show:
        patientCore()->patientBar()->hide();
        break;
    case QEvent::Hide:
        patientCore()->patientBar()->show();
        break;
    default:
        break;
    }
    return QWidget::event(event);
}
