/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PATIENTS_SETTINGS_CONSTANTS_H
#define PATIENTS_SETTINGS_CONSTANTS_H

namespace Patients {
namespace Constants {

    const char * const  S_GROUP                  = "Patients";                              /*!< \brief Key for settings. */
    const char * const  S_SELECTOR_FIELDSTOSHOW  = "Patients/Selector/Fields";              /*!< \brief Key for settings. */
    const char * const  S_SELECTOR_USEGENDERCOLORS  = "Patients/Selector/UseGenderColors";
    const char * const  S_SEARCHMETHOD = "Patients/Selector/SearchMethod";
    const char * const  S_PATIENTBARCOLOR = "Patients/Bar/Color";
    const char * const  S_DEFAULTPHOTOSOURCE = "Patients/Photo/Source";
    const char * const  S_SEARCHWHILETYPING = "Patients/SeachWhileTyping";
    const char * const  S_PEDIATRICSAGELIMIT = "Patients/PediatricAgeLimit";

    const char * const  S_NEWPATIENT_DEFAULTCITY  = "Patients/New/Default/City";
    const char * const  S_NEWPATIENT_DEFAULTZIP  = "Patients/New/Default/Zip";
    const char * const  S_NEWPATIENT_DEFAULTCOUNTRY  = "Patients/New/Default/Country";

    const char * const  S_RECENTPATIENT_MAX  = "Patients/Recent/Max";
    const char * const  S_RECENTPATIENT_LIST = "Patients/Recent/List";

}  // End namespace Patients::Constants
}  // End namespace Patients

#endif // PATIENTS_SETTINGS_CONSTANTS_H
