/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PATIENTMODELWRAPPER_H
#define PATIENTMODELWRAPPER_H

#include <coreplugin/ipatient.h>

QT_BEGIN_NAMESPACE
class QModelIndex;
QT_END_NAMESPACE

/**
 * \file ./plugins/patientbaseplugin/patientmodelwrapper.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Patients {
class PatientModel;
namespace Internal {

/** \brief PatientModel wrapper can be accessed using Core::ICore::instance()->patient() */
class PatientModelWrapper : public Core::IPatient
{
    Q_OBJECT
public:
    PatientModelWrapper(QObject *parent = 0);
    ~PatientModelWrapper();
    void initialize(Patients::PatientModel *model);
    Patients::PatientModel *patientModel() {return m_Model;}

    // IPatient interface
    void clear() {}
    bool has(const int ref) const {return (ref>=0 && ref<Core::IPatient::NumberOfColumns);}
    QModelIndex currentPatientIndex() const;
    void setCurrentPatientUid(const QString &uid);

    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    virtual QVariant data(int column) const;

    /** \deprecated **/
    virtual bool setValue(int ref, const QVariant &value); // TODO: remove this and use setData instead
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

    //TODO: Is this needed in freemedforms?
    QString toXml() const {return QString();}
    bool fromXml(const QString &) {return true;}

    Core::IPatientBar *patientBar() const;
    virtual void hidePatientBar();
    virtual void showPatientBar();
    virtual bool isPatientBarVisible() const;

    virtual QHash<QString, QString> fullPatientName(const QString &uuid) const;
    virtual QHash<QString, QString> fullPatientName(const QStringList &uuids) const;

private Q_SLOTS:
    bool submit();
    void onCurrentPatientChanged(const QString &);
    void patientDataChanged(const QModelIndex &index);

private:
    Patients::PatientModel *m_Model;
};

}  // End namespace Internal
}  // End namespace MainWin

#endif // PATIENTMODELWRAPPER_H
