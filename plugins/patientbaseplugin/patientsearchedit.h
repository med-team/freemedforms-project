/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PATIENTBASECOMPLETER_H
#define PATIENTBASECOMPLETER_H

#include <patientbaseplugin/patientbase_exporter.h>
#include <utils/widgets/qbuttonlineedit.h>

#include <QCompleter>
#include <QValidator>

class QSqlTableModel;

/**
 * \file ./plugins/patientbaseplugin/patientsearchedit.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Patients {
namespace Internal {
class PatientBaseCompleterPrivate;

class PatientBaseCompleter : public QCompleter
{
    Q_OBJECT
public:
    enum CompleterModelRepresentation {
        FullName = 0,
        Uid
    };

    explicit PatientBaseCompleter(QObject *parent = 0);
    ~PatientBaseCompleter();

    QValidator *validator() const;

private:
    Internal::PatientBaseCompleterPrivate *d;
};
}  // End namespace Internal

class PATIENT_EXPORT PatientSearchEdit : public Utils::QButtonLineEdit
{
    Q_OBJECT
public:
    explicit PatientSearchEdit(QWidget *parent = 0);
    ~PatientSearchEdit();

Q_SIGNALS:
    void patientSelected(const QString &fullName, const QString &uid);

private Q_SLOTS:
    void onTextChanged(const QString &newText);
    void cancelSearch();
    void onPatientSelected(const QModelIndex &index);

private:
    void focusInEvent(QFocusEvent *event);
//    void keyPressEvent(QKeyEvent *event);

private:
    QString m_LastSearch;
    Internal::PatientBaseCompleter *m_Completer;
};

}

#endif // PATIENTBASECOMPLETER_H
