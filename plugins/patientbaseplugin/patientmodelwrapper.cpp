/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "patientmodelwrapper.h"
#include "patientbar.h"
#include "patientcore.h"

#include <coreplugin/icore.h>
#include <coreplugin/ipatient.h>

#include <formmanagerplugin/formcore.h>
#include <formmanagerplugin/patientformitemdatawrapper.h>

#include <patientbaseplugin/patientmodel.h>

#include <utils/global.h>

static inline Form::FormCore &formCore() {return Form::FormCore::instance();}
static inline Core::IPatient *patient()  { return Core::ICore::instance()->patient(); }
static inline Patients::PatientCore *patientCore() {return Patients::PatientCore::instance();}

using namespace Patients;
using namespace Internal;

PatientModelWrapper::PatientModelWrapper(QObject *parent) :
    Core::IPatient(parent), m_Model(0)
{
}

PatientModelWrapper::~PatientModelWrapper()
{
    Core::ICore::instance()->setPatient(0);
}

/*!
 * \brief Reemits underlying model signals to the public.
 *
 * This slot is connected to the underlying model's PatientModel::currentPatientChanged(QString) signal
 * and just emits the main patient change signals IPatient::currentPatientChanged() and
 * IPatient::currentPatientChanged(QModelIndex).
 * These are the signals that all plugins can use, because they can access IPatient and connect to it's signals.
 *
 * \sa IPatient::currentPatientChanged(), \sa IPatient::currentPatientChanged(QModelIndex)
 */
void PatientModelWrapper::onCurrentPatientChanged(const QString &)
{
    Q_EMIT currentPatientChanged();
}

void PatientModelWrapper::initialize(Patients::PatientModel *model)
{
    m_Model = model;
    connect(model, SIGNAL(currentPatientChanged(QString)), this, SLOT(onCurrentPatientChanged(QString)));
    connect(model, SIGNAL(currentPatientChanged(QString)), this, SIGNAL(patientCreated(QString)));
    Utils::linkSignalsFromFirstModelToSecondModel(model, this, true);
}

QModelIndex PatientModelWrapper::currentPatientIndex() const
{
    if (m_Model->currentPatient().isValid()) {
        QModelIndex index = this->index(m_Model->currentPatient().row(), m_Model->currentPatient().column());
        return index;
    }
    return QModelIndex();
}


void PatientModelWrapper::setCurrentPatientUid(const QString &uid)
{
    patientCore()->setCurrentPatientUuid(uid);
}


QVariant PatientModelWrapper::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (!patient()->currentPatientIndex().isValid())
        return QVariant();

    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    QModelIndex idx = m_Model->index(index.row(), index.column());
    QVariant result = m_Model->data(idx, role);
    if (!result.isNull())
        return result;


    if (index.row() == patient()->currentPatientIndex().row())
        return formCore().patientFormItemDataWrapper().data(index.column());

    return QVariant();
}


QVariant PatientModelWrapper::data(int column) const
{
    if (!m_Model)
        return QVariant();
    QModelIndex idx = m_Model->index(m_Model->currentPatient().row(), column);
    return this->data(idx);
}

bool PatientModelWrapper::setValue(int ref, const QVariant &value)
{
    QModelIndex idx = m_Model->index(m_Model->currentPatient().row(), ref);
    return setData(idx, value);
}

bool PatientModelWrapper::setData(const QModelIndex &item, const QVariant &value, int role)
{
    QModelIndex idx = m_Model->index(item.row(), item.column());
    if (m_Model->setData(idx, value, role)) {
        Q_EMIT dataChanged(idx, idx);
        return true;
    }
    return false;
}

Core::IPatientBar *PatientModelWrapper::patientBar() const
{
    return patientCore()->patientBar();
}

void PatientModelWrapper::hidePatientBar()
{
    patientCore()->patientBar()->hide();
}

void PatientModelWrapper::showPatientBar()
{
    if (m_Model->currentPatient().isValid())
        patientCore()->patientBar()->show();
    else
        patientCore()->patientBar()->hide();
}

bool PatientModelWrapper::isPatientBarVisible() const
{
    return patientCore()->patientBar()->isVisible();
}

QHash<QString, QString> PatientModelWrapper::fullPatientName(const QString &uuid) const
{
    return m_Model->patientName(QStringList() << uuid);
}

QHash<QString, QString> PatientModelWrapper::fullPatientName(const QStringList &uuids) const
{
    return m_Model->patientName(uuids);
}

void PatientModelWrapper::patientDataChanged(const QModelIndex &index)
{
    if (m_Model->currentPatient().row() == index.row())
        Q_EMIT dataChanged(index, index);
}

bool PatientModelWrapper::submit()
{
    return m_Model->submit();
}

