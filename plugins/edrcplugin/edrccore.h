/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef EDRC_EDRCCORE_H
#define EDRC_EDRCCORE_H

#include <edrcplugin/edrc_exporter.h>
#include <QObject>

/**
 * \file ./plugins/edrcplugin/edrccore.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace eDRC {
namespace Internal {
class ConsultResult;
class EdrcPlugin;
class DrcDatabase;
class EdrcCorePrivate;
} // namespace Internal

class EDRC_EXPORT EdrcCore : public QObject
{
    friend class eDRC::Internal::EdrcPlugin;
    Q_OBJECT

protected:
    explicit EdrcCore(QObject *parent = 0);
    bool initialize();
    void extensionInitialized();

public:
    static EdrcCore &instance();
    ~EdrcCore();

    QString currentDatabaseVersion() const;

    QString toHtml(const Internal::ConsultResult &cr);

//protected:
    Internal::DrcDatabase &edrcBase() const;

private:
    Internal::EdrcCorePrivate *d;
    static EdrcCore *_instance;
};

} // namespace eDRC

#endif  // EDRC_EDRCCORE_H

