#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

include($${SOURCES_PLUGINS_PATH}/coreplugin/coreplugin.pri)
include($${SOURCES_PLUGINS_PATH}/texteditorplugin/texteditorplugin.pri)
include($${SOURCES_PLUGINS_PATH}/listviewplugin/listviewplugin.pri)

# FreeMedForms only inclusion
isEmpty(FREEDRC):include($${SOURCES_PLUGINS_PATH}/formmanagerplugin/formmanagerplugin.pri)

QT *= sql
