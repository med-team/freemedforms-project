/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef EDRC_INTERNAL_CRTREEMODEL_H
#define EDRC_INTERNAL_CRTREEMODEL_H

#include <edrcplugin/edrc_exporter.h>
#include <QStandardItemModel>

/**
 * \file ./plugins/edrcplugin/models/crtreemodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace eDRC {
namespace Internal {
class ConsultResult;
class CrTreeModelPrivate;

class EDRC_EXPORT CrTreeModel : public QStandardItemModel
{
    Q_OBJECT

public:
    enum DataRepresentation {
        Label = 0,
        DateOfExamination,
        Validity,
        DiagnosisPosition,
        MedicalFollowUp,
        Html,
        Id,
        Empty1,
        Empty2,
        Empty3,
        ColumnCount
    };

    CrTreeModel(QObject *parent = 0);
    ~CrTreeModel();

    bool setCrList(const QList<ConsultResult> &cr);
    bool addConsultResult(const ConsultResult &cr);
    void updateConsultResult(const QModelIndex &crIndex, const ConsultResult &crToUpdate);
    bool removeItems(const QModelIndex &index);

    bool isConsultResult(const QModelIndex &index) const;
    bool isHistoryIndex(const QModelIndex &index) const;
    QString htmlContent(const QModelIndex &index) const;

    ConsultResult consultResultFromIndex(const QModelIndex &index) const;
    const QList<ConsultResult> &consultResultList() const;

private:
    CrTreeModelPrivate *d;
};

} // namespace Internal
} // namespace eDRC

#endif  // EDRC_INTERNAL_CRTREEMODEL_H
