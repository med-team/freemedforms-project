/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef EDRC_INTERNAL_CRCRITERIASMODEL_H
#define EDRC_INTERNAL_CRCRITERIASMODEL_H

#include <QAbstractTableModel>

/**
 * \file ./plugins/edrcplugin/models/crcriteriasmodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace eDRC {
namespace Internal {
class CrCriteriasModelPrivate;

class CrCriteriasModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    enum Datarepresentation {
        Id = 0,
        Label,
        ItemWeight, // Pondération
        Indentation,
        ColumnCount
    };

    enum CodingStatus {
        NoCodingStarted = 0,
        IncompleteCoding,
        ValidCoding
    };

    CrCriteriasModel(QObject *parent = 0);
    ~CrCriteriasModel();
    void clear();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

    Qt::ItemFlags flags(const QModelIndex &index) const;

    void setFilterOnCrId(const int crId);
    void setSelectedCriteriaIds(const QList<int> &ids);
    int currentConsulResultId() const;
    CodingStatus currentCodingStatus() const;
    QList<int> currentSelectedCriteriaIds() const;

private:
    CrCriteriasModelPrivate *d;
};

} // namespace eDRC
} // namespace Internal

#endif  // EDRC_INTERNAL_CRCRITERIASMODEL_H
