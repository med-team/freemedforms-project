/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef EDRC_CLASSANDClassAndCrTreeModel_H
#define EDRC_CLASSANDClassAndCrTreeModel_H

#include <QStandardItemModel>

/**
 * \file ./plugins/edrcplugin/models/classandcrtreemodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace eDRC {
namespace Internal {
class ClassAndCrTreeModelPrivate;
} // namespace Internal

class ClassAndCrTreeModel : public QStandardItemModel
{
    Q_OBJECT
    
public:
    enum DataRepresentation {
        Label = 0,
        Id
    };

    explicit ClassAndCrTreeModel(QObject *parent = 0);
    ~ClassAndCrTreeModel();
    bool initialize();

    int id(const QModelIndex &index) const;
    QString arguments(const QModelIndex &index) const;
    QStringList authorizedDiagnosis(const QModelIndex &index) const;

private:
    Internal::ClassAndCrTreeModelPrivate *d;
};

} // namespace eDRC

#endif  // EDRC_CLASSANDClassAndCrTreeModel_H

