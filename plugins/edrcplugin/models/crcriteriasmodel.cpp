/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "crcriteriasmodel.h"
#include <edrcplugin/constants.h>
#include <edrcplugin/edrccore.h>
#include <edrcplugin/consultresult.h>
#include <edrcplugin/consultresultvalidator.h>
#include <edrcplugin/database/constants_db.h>
#include <edrcplugin/database/edrcbase.h>

#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>

#include <utils/global.h>

#include <QFont>


using namespace eDRC;
using namespace Internal;

static inline Core::ISettings *settings() {return Core::ICore::instance()->settings();}
static inline eDRC::EdrcCore &edrcCore() {return eDRC::EdrcCore::instance();}
static inline eDRC::Internal::DrcDatabase &edrcBase() {return eDRC::EdrcCore::instance().edrcBase();}

namespace eDRC {
namespace Internal {
class CrCriteriasModelPrivate
{
public:
    CrCriteriasModelPrivate(CrCriteriasModel *parent) :
        _crId(-1),
        q(parent)
    {
        Q_UNUSED(q);
    }

    ~CrCriteriasModelPrivate()
    {}

    void getCriteriasFromDatabase(int crId)
    {
        _crId = crId;
        _criterias.clear();
        _criterias = edrcBase().getOrderedCriteriasForCr(crId);
        _validator.setCrId(_crId);
        _validator.setAvailableCriterias(_criterias);
        _validator.setSelectedCriterias(_checkedIds);
        _validator.check();
    }

public:
    int _crId;
    QList<ConsultResultCriteria> _criterias;
    QList<int> _checkedRows, _checkedIds;
    ConsultResultValidator _validator;

private:
    CrCriteriasModel *q;
};
}
}

CrCriteriasModel::CrCriteriasModel(QObject *parent) :
    QAbstractTableModel(parent),
    d(new CrCriteriasModelPrivate(this))
{
    setFilterOnCrId(-1);
}

CrCriteriasModel::~CrCriteriasModel()
{
    if (d)
        delete d;
    d = 0;
}

void CrCriteriasModel::clear()
{
    setFilterOnCrId(-1);
}

int CrCriteriasModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return d->_criterias.count();
}

int CrCriteriasModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return ColumnCount;
}

QVariant CrCriteriasModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (!IN_RANGE_STRICT_MAX(index.row(), 0, d->_criterias.count()))
        return QVariant();

    const ConsultResultCriteria &crit = d->_criterias.at(index.row());

    switch (role) {
    case Qt::DisplayRole:
    {
        switch (index.column()) {
        case Id: return crit.id();
        case Label: return crit.label(settings()->value(Constants::S_CR_USE_MODERNLABEL, true).toBool());
        case ItemWeight: return crit.weight();
        case Indentation: return crit.indentation();
        } // switch
        break;
    }
    case Qt::ToolTipRole:
    {
        if (index.column() == Label) {
            if (crit.isLineBreak())
                return QVariant();
            return QString("%1")
                    .arg(crit.label(settings()->value(Constants::S_CR_USE_MODERNLABEL, true).toBool()).trimmed())
                    ;
        }
        break;
    }
    case Qt::FontRole:
    {
        if (index.column() == Label) {
            if (crit.weight() == 1) {
                if (settings()->value(Constants::S_CR_MANDATORYLABEL_IN_BOLD, true).toBool()) {
                    if (crit.indentation() == 1) {
                        QFont font;
                        font.setBold(true);
                        return font;
                    } else {
                        int indent = crit.indentation();
                        int row = index.row() - 1;

                        while (indent > 1 && row >= 0) {
                            const ConsultResultCriteria &previous = d->_criterias.at(row);

                            if (previous.isLineBreak())
                                break;

                            if (previous.indentation() < indent) {
                                if (d->_checkedRows.contains(row)) {
                                    QFont font;
                                    font.setBold(true);
                                    return font;
                                } else {
                                    return QVariant();
                                }
                            }
                            --row;
                        }
                    }
                }
            }
        }
        break;
    }
    case Qt::BackgroundRole:
    {
        if (d->_checkedRows.contains(index.row())) {
            QColor c(QColor(settings()->value(Constants::S_CRVALIDATOR_COLORS_SELECTED_BACKGROUND).toString()));
            c.setAlpha(125);
            return c;
        }
        if (settings()->value(Constants::S_REALTIME_CR_CODING_CHECKING, true).toBool()) {
            if (d->_validator.wrongCriteriaIds().contains(crit.id())) {
                QColor c(QColor(settings()->value(Constants::S_CRVALIDATOR_COLORS_ERROR_BACKGROUND).toString()));
                c.setAlpha(125);
                return c;
            }
        }
        break;
    }
    case Qt::ForegroundRole:
    {
        if (d->_checkedRows.contains(index.row())) {
            QColor c(QColor(settings()->value(Constants::S_CRVALIDATOR_COLORS_SELECTED_FOREGROUND).toString()));
            return c;
        }
        if (settings()->value(Constants::S_REALTIME_CR_CODING_CHECKING, true).toBool()) {
            if (d->_validator.wrongCriteriaIds().contains(crit.id())) {
                QColor c(QColor(settings()->value(Constants::S_CRVALIDATOR_COLORS_ERROR_FOREGROUND).toString()));
                return c;
            }
        }
        break;
    }
    default: break;
    } // switch

    return QVariant();
}

bool CrCriteriasModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    Q_UNUSED(value);
    if (!index.isValid())
        return false;

    if (role == Qt::CheckStateRole) {

        if (!IN_RANGE_STRICT_MAX(index.row(), 0, d->_criterias.count()))
            return false;

        const ConsultResultCriteria &crit = d->_criterias.at(index.row());
        if (crit.isLineBreak())
            return true;

        if (!d->_checkedRows.contains(index.row())) {
            int indent = crit.indentation();
            int row = index.row() - 1;
            while (indent > 1 && row >= 0) {
                const ConsultResultCriteria &previous = d->_criterias.at(row);

                if (previous.isLineBreak())
                    break;

                if (previous.indentation() < indent) {
                    indent = previous.indentation();
                    if (!d->_checkedRows.contains(row)) {
                        d->_checkedRows << row;
                        d->_checkedIds << previous.id();
                    }
                    Q_EMIT dataChanged(this->index(row, Label), this->index(row, Label));
                }
                --row;
            }
            d->_checkedRows << index.row();
            d->_checkedIds << crit.id();
        } else {
            int indent = crit.indentation();
            int row = index.row() + 1;
            while (row < d->_criterias.count()) {
                const ConsultResultCriteria &next = d->_criterias.at(row);

                if (next.isLineBreak())
                    break;

                if (next.indentation() > indent) {
                    d->_checkedRows.removeAll(row);
                    d->_checkedIds.removeAll(next.id());
                    Q_EMIT dataChanged(this->index(row, Label), this->index(row, Label));
                }
                ++row;
            }
            d->_checkedRows.removeAll(index.row());
            d->_checkedIds.removeAll(crit.id());
        }
        Q_EMIT dataChanged(index, index);

        d->_validator.setSelectedCriterias(d->_checkedIds);
        d->_validator.check();

    }
    return true;
}

Qt::ItemFlags CrCriteriasModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index);
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;// | Qt::ItemIsUserCheckable;
}

void CrCriteriasModel::setFilterOnCrId(const int crId)
{
    beginResetModel();
    d->_checkedRows.clear();
    d->_checkedIds.clear();
    d->getCriteriasFromDatabase(crId);
    endResetModel();
}

void CrCriteriasModel::setSelectedCriteriaIds(const QList<int> &ids)
{
    beginResetModel();
    d->_checkedIds = ids;
    for(int i = 0; i < d->_criterias.count(); ++i) {
        const ConsultResultCriteria &crit = d->_criterias.at(i);
        if (ids.contains(crit.id()))
            d->_checkedRows << i;
    }
    d->_validator.setSelectedCriterias(ids);
    d->_validator.check();
    endResetModel();
}

int CrCriteriasModel::currentConsulResultId() const
{
    return d->_crId;
}

CrCriteriasModel::CodingStatus CrCriteriasModel::currentCodingStatus() const
{
    if (d->_checkedRows.isEmpty())
        return NoCodingStarted;

    if (d->_validator.wrongCriteriaIds().isEmpty())
        return ValidCoding;

    return IncompleteCoding;
}

QList<int> CrCriteriasModel::currentSelectedCriteriaIds() const
{
    return d->_checkedIds;
}
