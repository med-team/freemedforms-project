/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef EDRC_INTERNAL_CRLISTMODEL_H
#define EDRC_INTERNAL_CRLISTMODEL_H

#include <QList>
#include <QAbstractTableModel>

/**
 * \file ./plugins/edrcplugin/models/crlistmodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace eDRC {
namespace Internal {
class ConsultResult;

class CrListModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    enum DataRepresentation {
        Id = 0,
        Label,
        DateOfExamination,
        Validity,
        DiagnosisPosition,
        //RiskLevel,
        MedicalFollowUp,
        Html,
        ColumnCount
    };

    CrListModel(QObject *parent = 0);
    ~CrListModel();

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    Qt::ItemFlags flags(const QModelIndex &index) const;

    void setCrList(const QList<ConsultResult> &cr);

private:
    QList<ConsultResult> _list;
};

} // namespace Internal
} // namespace eDRC

#endif  // EDRC_INTERNAL_CRLISTMODEL_H
