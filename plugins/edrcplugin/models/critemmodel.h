/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef EDRC_INTERNAL_CRITEMMODEL_H
#define EDRC_INTERNAL_CRITEMMODEL_H

#include <QSqlTableModel>

/**
 * \file ./plugins/edrcplugin/models/critemmodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace eDRC {
namespace Internal {

class CrItemModel : public QSqlTableModel
{
    Q_OBJECT

public:
    enum Datarepresentation {
        Id = 0,
        Label
    };

    CrItemModel(QObject *parent = 0);
    ~CrItemModel();
    void clear();

    QVariant data(const QModelIndex &index, int role) const;

    Qt::ItemFlags flags(const QModelIndex &index) const;

    void setFilterOnCrId(const int rcId);
};

} // namespace eDRC
} // namespace Internal

#endif  // EDRC_INTERNAL_CRITEMMODEL_H
