/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef EDRC_PLUGIN_CONSULTRESULTVALIDATOR_H
#define EDRC_PLUGIN_CONSULTRESULTVALIDATOR_H

#include <QList>

/**
 * \file ./plugins/edrcplugin/consultresultvalidator.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace eDRC {
namespace Internal {
class ConsultResultCriteriaGroup;
class ConsultResultValidatorPrivate;
class ConsultResultCriteria;

struct ConsultResultError {
    enum ErrorType {
        MissingMandatoryItem = 0,
        MissingSignifiantItem
    };

    ConsultResultError() : criteriaId(-1), type(-1) {}
    ConsultResultError(int _criteriaId, int _type) : criteriaId(_criteriaId), type(_type) {}

    int criteriaId;
    int type;
    QString errorShortMessage() const;
};

class ConsultResultValidator
{
public:
    ConsultResultValidator();
    ConsultResultValidator(int crId, const QList<ConsultResultCriteria> &availableCriterias);
    ~ConsultResultValidator();
    static QString version();

    void setCrId(int crId);
    void setAvailableCriterias(const QList<ConsultResultCriteria> &availableCriterias);
    void clearSelectedCriterias();
    void setSelectedCriterias(const QList<int> &selectedId);

    bool check();
    QString errorShortMessage(int criteriaId) const;
    const QList<int> &wrongCriteriaIds() const;

private:
    ConsultResultValidatorPrivate *d;
};

} // namespace eDRC
} // namespace Internal

QDebug operator<<(QDebug dbg, const eDRC::Internal::ConsultResultCriteriaGroup &group);

#endif // EDRC_PLUGIN_CONSULTRESULTVALIDATOR_H
