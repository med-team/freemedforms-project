/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef EDRC_EDCRBASE_H
#define EDRC_EDCRBASE_H

#include <utils/database.h>

/**
 * \file ./plugins/edrcplugin/database/edrcbase.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace eDRC {
class EdrcCore;
namespace Internal {
class ConsultResultCriteria;

class DrcDatabase : public Utils::Database
{
#ifdef QT_TESTLIB_LIB
public:
#else
    friend class eDRC::EdrcCore;
protected:
#endif
    DrcDatabase(const QString &absPathToDb);
    bool initialize(bool createIfNotExists, const QString &absPathToCsvRawSourceFiles);

public:
    ~DrcDatabase();
    bool isInitialized() const {return _initialized;}
    QString version() const;

    QHash<int, QString> getCrClasses() const;
    QHash<int, QString> getCrForClasses(int classId) const;
    QHash<int, QString> getClassesForCr(int crId) const;
    QHash<int, QString> getSeeAlsoRcForCr(int rcId) const;
    QString getCrLabel(const int rcId, bool onlyValid = true) const;
    QString getCrArguments(const int rcId, bool toHtml = false, bool onlyValid = true) const;
    QStringList getCrAuthorizedDiagnosis(const int rcId, bool onlyValid = true) const;
    QStringList getCrIcd10RelatedCodes(const int rcId, bool onlyValid) const;

    QList<ConsultResultCriteria> getOrderedCriteriasForCr(int crId) const;

private:
    bool createDatabase(const QString &connection, const QString &prefixedDbName,
                        const Utils::DatabaseConnector &connector,
                        CreationOption createOption);

protected:
    bool setVersion(const Utils::Field &field, const QString &version);
    bool checkDatabaseVersion() const;

public:
//    void toTreeWidget(QTreeWidget *tree) const;

private Q_SLOTS:
//    void onCoreDatabaseServerChanged();
//    void onCoreFirstRunCreationRequested();
private:
    bool _initialized;
    QString _databasePath, _absPathToCsvRawSourceFiles;
};

} // namespace Internal
} // namespace eDRC

#endif  // EDRC_EDCRBASE_H
