/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers: Eric MAEKER, <eric.maeker@gmail.com>                 *
 *   Contributors:                                                         *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
/*!
 * \class eDRC::EdrcCore
 */

#include "edrccore.h"
#include <edrcplugin/database/constants_db.h>
#include <edrcplugin/database/edrcbase.h>
#include <edrcplugin/widgets/edrcwidgetmanager.h>
#include <edrcplugin/consultresult.h>
#include <edrcplugin/constants.h>

#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>

#include <translationutils/constants.h>

#include <QFileInfo>
#include <QDir>

#include <QDebug>

using namespace eDRC;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Core::ISettings *settings()  { return Core::ICore::instance()->settings(); }

namespace eDRC {
namespace Internal {
class EdrcCorePrivate
{
public:
    EdrcCorePrivate(EdrcCore *parent) :
        _edrcBase(0),
        q(parent)
    {
        Q_UNUSED(q);
    }
    
    ~EdrcCorePrivate()
    {
    }
    
    QString databasePath() const
    {
        QString dbRelPath = QString("/%1/%2").arg(Constants::DB_NAME).arg(Constants::DB_FILENAME);
        QString tmp;
        tmp = settings()->dataPackInstallPath() + dbRelPath;
        if (QFileInfo(tmp).exists())
            return settings()->dataPackInstallPath();
        return settings()->dataPackApplicationInstalledPath();
    }

    QString csvFilesPath() const
    {
        QString pathToCsv = settings()->path(Core::ISettings::BundleResourcesPath);
        pathToCsv += "/nonfree/edrc";
        return pathToCsv;
    }

public:
    DrcDatabase *_edrcBase;
    EdrcWidgetManager *_widgetManager;
    
private:
    EdrcCore *q;
};
} // namespace Internal
} // end namespace eDRC

EdrcCore *EdrcCore::_instance = 0;


eDRC::EdrcCore &eDRC::EdrcCore::instance() // static
{
    Q_ASSERT(_instance);
    return *_instance;
}

/*! Constructor of the eDRC::EdrcCore class */
EdrcCore::EdrcCore(QObject *parent) :
    QObject(parent),
    d(new EdrcCorePrivate(this))
{
    _instance = this;
}

/*! Destructor of the eDRC::EdrcCore class */
EdrcCore::~EdrcCore()
{
    _instance = 0;
    delete d->_edrcBase;
    d->_edrcBase = 0;
    if (d)
        delete d;
    d = 0;
}

/*! Initializes the object with the default values. Return true if initialization was completed. */
bool EdrcCore::initialize()
{
    d->_edrcBase = new DrcDatabase(d->databasePath());
    d->_edrcBase->initialize(true, d->csvFilesPath());
    d->_widgetManager = new EdrcWidgetManager(this);
    return true;
}

void EdrcCore::extensionInitialized()
{
    d->_widgetManager = new EdrcWidgetManager(this);
}


QString EdrcCore::currentDatabaseVersion() const
{
    const QString &v = d->_edrcBase->version();
    if (v.isEmpty())
        return tr("No eDRC database");
    return v;
}


QString EdrcCore::toHtml(const Internal::ConsultResult &cr)
{
    return cr.toHtml(settings()->value(Constants::S_TOKEN_HTMLGLOBALMASK).toString(),
                     settings()->value(Constants::S_TOKEN_HTMLCRITERIASMASK).toString(),
                     *d->_edrcBase);
}

DrcDatabase &EdrcCore::edrcBase() const
{
    return *d->_edrcBase;
}
