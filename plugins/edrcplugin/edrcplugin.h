/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Eric Maeker <eric.maeker@gmail.com>                  *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef EDRCPLUGIN_INTERNAL_IPLUGIN_H
#define EDRCPLUGIN_INTERNAL_IPLUGIN_H

#include <extensionsystem/iplugin.h>

/**
 * \file ./plugins/edrcplugin/edrcplugin.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace eDRC {
class EdrcCore;
namespace Internal {
class EdrcPreferencesPage;
#ifdef FREEMEDFORMS
class EdrcWidgetFactory;
#endif

class EdrcPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.freemedforms.FreeMedForms.ToolsPlugin"  FILE "eDRC.json")

public:
    EdrcPlugin();
    ~EdrcPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

private Q_SLOTS:
    void postCoreInitialization();

#ifdef WITH_TESTS
private Q_SLOTS:
    void initTestCase();
    void testConsultResultObject();
    void testConsultResultXml();
    void testValidatorTestOne();
    void testValidatorTestTwo();
    void testValidatorTestThree();
    void testCrToHtml();
    void cleanupTestCase();
#endif

private:
    EdrcCore *_core;
    EdrcPreferencesPage *_pref;
#ifdef FREEMEDFORMS
    EdrcWidgetFactory *_factory;
#endif
};

} // namespace Internal
} // namespace eDRC

#endif // EDRCPLUGIN_INTERNAL_IPLUGIN_H

