#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

include(shared_sources.pri)

!with-edrc{
    error(eDRC plugin not requested)
} else {
    message(Building eDRC plugin)
}

greaterThan(QT_MAJOR_VERSION, 4) {
    QT *= printsupport
}

# include FreeMedForms specific sources
HEADERS += \
    freemedforms/edrcwidgetfactory.h

SOURCES += \
    freemedforms/edrcwidgetfactory.cpp

OTHER_FILES += eDRC.pluginspec
