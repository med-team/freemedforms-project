/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "consultresultvalidator.h"
#include "consultresult.h"
#include "constants.h"

#include <utils/global.h>

#include <QList>
#include <QApplication>

#include <QDebug>

enum { WarnGroupCreation = false, WarnSelectionChecking = false };

using namespace eDRC;
using namespace Internal;

namespace eDRC {
namespace Internal {
class ConsultResultCriteriaGroup
{
public:
    ConsultResultCriteriaGroup() :
        _parentId(-1)
    {}

    void addCriteria(const ConsultResultCriteria &crit)
    {
        _criterias << crit;
    }

    const QList<ConsultResultCriteria> &criterias() const
    {
        return _criterias;
    }

    int parentIndex() const
    {
        return _parentId;
    }

    void setCriteriaParentId(int criteriaId)
    {
        _parentId = criteriaId;
    }

    static ConsultResultCriteriaGroup &joinGroups(const QList<ConsultResultCriteriaGroup> &groups)
    {
        ConsultResultCriteriaGroup *gr = new ConsultResultCriteriaGroup;
        foreach(const ConsultResultCriteriaGroup &group, groups) {
            gr->_criterias << group._criterias;
            gr->_parentId = group.parentIndex();
        }
        return *gr;
    }

    int minimumItemToBeSelected() const
    {
        if (_criterias.isEmpty())
            return 0;

        const QString &label = _criterias.at(0).label();
        if (label.contains("++1|"))
            return 1;
        else if (label.contains("++2|"))
            return 2;
        else if (label.contains("++3|"))
            return 3;
        else if (label.contains("++4|"))
            return 4;
        else if (label.contains("++5|"))
            return 5;
        else if (label.contains("++6|"))
            return 6;
        return 0;
    }


    QList<ConsultResultError> isSelectionValid(const QList<int> &selectedCriteriaIds) const
    {
        if (WarnSelectionChecking)
            qWarning() << "IS SELECTION VALID?";
        QList<ConsultResultError> errors;
        if (_parentId != -1 && !selectedCriteriaIds.contains(_parentId)) {
            if (WarnSelectionChecking)
                qWarning() << "    HAS PARENT AND PARENT NOT SELECTED";
            return errors;
        }

        int nbSelected = 0;
        int signifiance = 0;
        foreach(const ConsultResultCriteria &crit, _criterias) {
            if (WarnSelectionChecking) {
                QString sel;
                selectedCriteriaIds.contains(crit.id())? sel="Selected":sel="NotSelected";
                qWarning() << "    " <<  sel << crit.label();
            }

            if (crit.isSelectionMandatory()) {
                if (!selectedCriteriaIds.contains(crit.id())) {
                    errors << ConsultResultError(crit.id(), ConsultResultError::MissingMandatoryItem);
                }
            } else if (crit.isSelectionSignifiant()) {
                if (selectedCriteriaIds.contains(crit.id())) {
                    ++nbSelected;
                }
                signifiance = crit.weight() - 1;
            }
        }
        if (WarnSelectionChecking)
            qWarning() << "    " << signifiance << nbSelected;

        if (signifiance > 0 && (nbSelected < signifiance)) {
            foreach(const ConsultResultCriteria &crit, _criterias) {
                if (crit.isSelectionSignifiant() && !selectedCriteriaIds.contains(crit.id())) {
                    errors << ConsultResultError(crit.id(), ConsultResultError::MissingSignifiantItem);
                }
            }
        }

        if (WarnSelectionChecking)
            qWarning() << "END IS SELECTION VALID =" << errors.isEmpty();

        return errors;
    }

private:
    QList<ConsultResultCriteria> _criterias;
    int _parentId;
};

class ConsultResultValidatorPrivate
{
public:
    ConsultResultValidatorPrivate(ConsultResultValidator *parent) :
        crId(-1),
        q(parent)
    {
        Q_UNUSED(q);
    }

    ~ConsultResultValidatorPrivate()
    {}

    int createGroup(int firstCriteria, int parentCriteria = -1)
    {

        if (!IN_RANGE_STRICT_MAX(firstCriteria, 0, _criterias.count())) {
            if (WarnGroupCreation)
                qWarning() << "ERROR";
            return -1;
        }

        int currentIndentation = _criterias.at(firstCriteria).indentation();
        if (currentIndentation > 1 && parentCriteria == -1) {
            int i = firstCriteria;
            while (i >= 0) {
                const ConsultResultCriteria &crit = _criterias.at(i);
                if (crit.indentation() == (currentIndentation-1) &&
                        !crit.label().trimmed().isEmpty()) {
                    parentCriteria = i;
                    break;
                }
                --i;
            }
        }

        if (WarnGroupCreation)
            qWarning() << QString("%1CREATE").arg(QString().fill(' ', (currentIndentation-1)*2)) << firstCriteria << parentCriteria << _criterias.count();

        int i = firstCriteria;
        ConsultResultCriteriaGroup group;
        if (parentCriteria != -1) {
            if (!IN_RANGE_STRICT_MAX(parentCriteria, 0, _criterias.count()))
                return -1;
            const ConsultResultCriteria &crit = _criterias.at(parentCriteria);
            group.setCriteriaParentId(crit.id());
        }

        while (i < _criterias.count()) {
            const ConsultResultCriteria &crit = _criterias.at(i);

            if (crit.indentation() > currentIndentation) {
                i = createGroup(i, i-1);
                if (i==-1) {
                    if (WarnGroupCreation)
                        qWarning() << QString("%1END").arg(QString().fill(' ', (currentIndentation-1)*2)) << i;
                    return -1;
                }
                continue;
            }

            if ((crit.weight() >= 7 && crit.isLineBreak())
                    || crit.indentation() < currentIndentation) {
                if (parentCriteria != -1) {
                    _groups.prepend(group);
                    if (WarnGroupCreation)
                        qWarning() << QString("%1END (child)").arg(QString().fill(' ', (currentIndentation-1)*2)) << i;
                    return i;
                }
                i = createGroup(i+1);
                continue;
            }

            if (crit.indentation() == currentIndentation) {
                if (WarnGroupCreation)
                    qWarning() << QString("%1  ADD").arg(QString().fill(' ', (currentIndentation-1)*2)) << crit.label();
                group.addCriteria(crit);
            }

            ++i;
        }
        _groups.prepend(group);
        if (WarnGroupCreation)
            qWarning() << QString("%1END (branch)").arg(QString().fill(' ', (currentIndentation-1)*2)) << i;
        return i;
    }

    void joinGroups()
    {
        QList<ConsultResultCriteriaGroup> groups;
        for(int i = _groups.count() - 1; i >= 0; --i) {
            const ConsultResultCriteriaGroup &crit = _groups.at(i);
            if (crit.criterias().count() == 1 &&
                    crit.criterias().at(0).label().contains("++") &&
                    crit.parentIndex() == -1) {
                groups.prepend(_groups.takeAt(i));
            }
        }
        if (!groups.isEmpty()) {
            _groups.prepend(ConsultResultCriteriaGroup::joinGroups(groups));
        }
    }

public:
    int crId;
    QList<ConsultResultCriteria> _criterias;
    QList<ConsultResultCriteriaGroup> _groups;
    QList<int> _selectedCriterias;
    QList<ConsultResultError> _errors;
    QList<int> _wrongCriteriaIds;

private:
    ConsultResultValidator *q;
};
}
}

QString ConsultResultError::errorShortMessage() const
{
    switch (type) {
    case MissingMandatoryItem: return QApplication::translate("ConsultResultError", "Mandatory item is not selected");
    case MissingSignifiantItem: return QApplication::translate("ConsultResultError", "Missing signifiant item in selection");
    }
    return QString();
}

ConsultResultValidator::ConsultResultValidator() :
    d(new ConsultResultValidatorPrivate(this))
{
}

ConsultResultValidator::ConsultResultValidator(int crId, const QList<ConsultResultCriteria> &availableCriterias) :
    d(new ConsultResultValidatorPrivate(this))
{
    d->crId = crId;
    setAvailableCriterias(availableCriterias);
}

ConsultResultValidator::~ConsultResultValidator()
{
    if (d)
        delete d;
    d = 0;
}

QString ConsultResultValidator::version()
{
    return Constants::VALIDATOR_VERSION;
}

void ConsultResultValidator::setCrId(int crId)
{
    d->crId = crId;
}


void ConsultResultValidator::setAvailableCriterias(const QList<ConsultResultCriteria> &availableCriterias)
{
    d->_criterias = availableCriterias;
    d->_groups.clear();
    d->createGroup(0);
    d->joinGroups();
    if (WarnGroupCreation) {
        foreach(const ConsultResultCriteriaGroup &group, d->_groups)
            qWarning() << group;
    }
}

void ConsultResultValidator::clearSelectedCriterias()
{
    d->_selectedCriterias.clear();
}


void ConsultResultValidator::setSelectedCriterias(const QList<int> &selectedId)
{
    d->_selectedCriterias = selectedId;
}


bool ConsultResultValidator::check()
{
    d->_errors.clear();
    d->_wrongCriteriaIds.clear();

    foreach(const ConsultResultCriteriaGroup &group, d->_groups)
        d->_errors << group.isSelectionValid(d->_selectedCriterias);

    foreach(const ConsultResultError &error, d->_errors)
        d->_wrongCriteriaIds << error.criteriaId;

    qWarning() << "VALID = " << d->_errors.isEmpty();
    return d->_errors.isEmpty();
}

QString ConsultResultValidator::errorShortMessage(int criteriaId) const
{
    foreach(const ConsultResultError &error, d->_errors) {
        if (error.criteriaId == criteriaId)
            return error.errorShortMessage();
    }
    return QString();
}

const QList<int> &ConsultResultValidator::wrongCriteriaIds() const
{
    return d->_wrongCriteriaIds;
}

QDebug operator<<(QDebug dbg, const eDRC::Internal::ConsultResultCriteriaGroup &group)
{
    QString out;
    foreach(const eDRC::Internal::ConsultResultCriteria &crit, group.criterias()) {
        out += QString("    %1 (W:%2; I%3)\n")
               .arg(crit.label())
               .arg(crit.weight())
               .arg(crit.indentation());
    }
    if (out.isEmpty())
        dbg.nospace() << "CriteriaGroup(Empty)";
    else
        dbg.nospace() << QString("CriteriaGroup(\n%1)").arg(out);
    return dbg.space();
}


