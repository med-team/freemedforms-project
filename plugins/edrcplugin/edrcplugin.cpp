/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Eric Maeker <eric.maeker@gmail.com>                  *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "edrcplugin.h"
#include "edrccore.h"
#include <edrcplugin/widgets/edrcpreferences.h>
#ifdef FREEMEDFORMS
#include <edrcplugin/freemedforms/edrcwidgetfactory.h>
#endif

#include <coreplugin/icore.h>
#include <coreplugin/iuser.h>
#include <coreplugin/itheme.h>
#include <coreplugin/ipatient.h>
#include <coreplugin/isettings.h>
#include <coreplugin/translators.h>
#include <coreplugin/constants_icons.h>
#include <coreplugin/constants_menus.h>
#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/contextmanager/contextmanager.h>

#include <utils/log.h>
#include <utils/global.h>
#include <extensionsystem/pluginmanager.h>

#include <QtPlugin>
#include <QAction>
#include <QDebug>

using namespace eDRC;
using namespace Internal;

static inline Core::IUser *user()  { return Core::ICore::instance()->user(); }
static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline Core::ActionManager *actionManager()  { return Core::ICore::instance()->actionManager(); }
static inline Core::ISettings *settings()  { return Core::ICore::instance()->settings(); }
static inline Core::IPatient *patient()  { return Core::ICore::instance()->patient(); }
static inline ExtensionSystem::PluginManager *pluginManager() {return ExtensionSystem::PluginManager::instance();}
static inline void messageSplash(const QString &s) {theme()->messageSplashScreen(s); }

EdrcPlugin::EdrcPlugin() :
    ExtensionSystem::IPlugin(),
    _core(0),
    _pref(0),
    _factory(0)
{
    setObjectName("eDRCPlugin");
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating eDRCPlugin";

    Core::ICore::instance()->translators()->addNewTranslator("plugin_edrc");


    _pref = new EdrcPreferencesPage(this);
    addObject(_pref);

}

EdrcPlugin::~EdrcPlugin()
{
}

bool EdrcPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    Q_UNUSED(arguments)
    Q_UNUSED(errorString)

    if (Utils::Log::debugPluginsCreation()) {
        qWarning() << "EdrcPlugin::initialize";
    }

    _core = new EdrcCore(this);
    _core->initialize();

#ifdef FREEDRC
    _pref->checkSettingsValidity();
#endif

#ifdef FREEMEDFORMS
    _factory = new eDRC::Internal::EdrcWidgetFactory(this);
    _factory->initialize(arguments, errorString);
    addObject(_factory);
#endif


    return true;
}

void EdrcPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation()) {
        qWarning() << "EdrcPlugin::extensionsInitialized";
    }



    messageSplash(tr("Initializing eDRC..."));

    _core->extensionInitialized();
#ifdef FREEMEDFORMS
    _factory->extensionInitialized();
#endif

    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));
    connect(Core::ICore::instance(), SIGNAL(coreOpened()), this, SLOT(postCoreInitialization()), Qt::UniqueConnection);
}

void EdrcPlugin::postCoreInitialization()
{
}

ExtensionSystem::IPlugin::ShutdownFlag EdrcPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    if (_pref)
        removeObject(_pref);
    if (_factory)
        removeObject(_factory);
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(EdrcPlugin)
