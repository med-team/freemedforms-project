/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef EDRC_INTERNAL_SFMGABOUTDIALOG_H
#define EDRC_INTERNAL_SFMGABOUTDIALOG_H

#include <QDialog>

/**
 * \file ./plugins/edrcplugin/widgets/sfmgaboutdialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace eDRC {
namespace Internal {

namespace Ui {
class SfmgAboutDialog;
}

class SfmgAboutDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SfmgAboutDialog(QWidget *parent = 0);
    ~SfmgAboutDialog();
    
private:
    Ui::SfmgAboutDialog *ui;
};

} // namespace Internal
} // namespace eDRC

#endif // EDRC_INTERNAL_SFMGABOUTDIALOG_H
