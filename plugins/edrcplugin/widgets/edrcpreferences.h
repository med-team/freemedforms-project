/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developpers:                                                     *
 *       Eric Maeker <e>                             *
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef EDRCPLUGIN_INTERNAL_EDRCPREFERENCES_H
#define EDRCPLUGIN_INTERNAL_EDRCPREFERENCES_H

#include <coreplugin/ioptionspage.h>

#include <QWidget>
#include <QPointer>

/**
 * \file ./plugins/edrcplugin/widgets/edrcpreferences.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;
}

namespace eDRC {
namespace Internal {
namespace Ui {
class EdrcPreferencesPageWidget;
}

class EdrcPreferencesPageWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit EdrcPreferencesPageWidget(QWidget *parent = 0);
    ~EdrcPreferencesPageWidget();
    
    void setDataToUi();
    QString searchKeywords() const;
    
    static void writeDefaultSettings(Core::ISettings *s);
    
public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);
    
private:
    void retranslateUi();
    void changeEvent(QEvent *e);
    
private:
    Ui::EdrcPreferencesPageWidget *ui;
};


class EdrcPreferencesPage : public Core::IOptionsPage
{
    Q_OBJECT

public:
    EdrcPreferencesPage(QObject *parent = 0);
    ~EdrcPreferencesPage();
    
    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;
    
    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();
    
    bool matches(const QString &s) const;
    
    QString helpPage() {return QString();}
    
    static void writeDefaultSettings(Core::ISettings *s) {EdrcPreferencesPageWidget::writeDefaultSettings(s);}
    
    QWidget *createPage(QWidget *parent = 0);
    
private:
    QPointer<Internal::EdrcPreferencesPageWidget> m_Widget;
    QString m_searchKeywords;
};


} // namespace Internal
} // namespace eDRC

#endif // EDRCPLUGIN_INTERNAL_EDRCPREFERENCES_H

