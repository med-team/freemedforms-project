/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers: Eric MAEKER, <eric.maeker@gmail.com>                 *
 *   Contributors:                                                         *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
/*!
 * \class eDRC::Internal::CrEditorDialog
 */

#include "creditordialog.h"
#include <edrcplugin/constants.h>
#include <edrcplugin/consultresult.h>
#include <edrcplugin/widgets/sfmgaboutdialog.h>
#include <edrcplugin/widgets/crargumentsdialog.h>

#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/constants_icons.h>

#include <utils/global.h>
#include <translationutils/constants.h>

#include <QPushButton>

#include <QDebug>

#include "ui_creditordialog.h"

static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }

using namespace eDRC;
using namespace Internal;
using namespace Trans::ConstantTranslations;

namespace eDRC {
namespace Internal {
class CrEditorDialogPrivate
{
public:
    CrEditorDialogPrivate(CrEditorDialog *parent) :
        ui(new Ui::CrEditorDialog),
        q(parent)
    {
        Q_UNUSED(q);
    }
    
    ~CrEditorDialogPrivate()
    {
        delete ui;
    }
    
public:
    Ui::CrEditorDialog *ui;
    ConsultResult _cr;
    
private:
    CrEditorDialog *q;
};
} // namespace Internal
} // end namespace eDRC


/*! Constructor of the eDRC::Internal::CrEditorDialog class */
CrEditorDialog::CrEditorDialog(QWidget *parent) :
    QDialog(parent),
    d(new CrEditorDialogPrivate(this))
{
    d->ui->setupUi(this);

    QPushButton *sfmg = d->ui->buttonBox->addButton(tr("About SFMG"), QDialogButtonBox::HelpRole);
    sfmg->setIcon(theme()->icon(Constants::ICON_SFMG_LOGO, Core::ITheme::SmallIcon));
    connect(sfmg, SIGNAL(clicked()), this, SLOT(onSmfgAboutClicked()));

    QPushButton *args = d->ui->buttonBox->addButton(tr("Consult result arguments"), QDialogButtonBox::HelpRole);
    args->setIcon(theme()->icon(Core::Constants::ICONINFORMATION, Core::ITheme::SmallIcon));
    connect(args, SIGNAL(clicked()), this, SLOT(onArgumentsClicked()));

    setWindowTitle(tr("Consult result editor and creator dialog"));
    setWindowIcon(theme()->icon(Core::Constants::ICONFREEDRC));

    Utils::resizeAndCenter(this, parent);
}

/*! Destructor of the eDRC::Internal::CrEditorDialog class */
CrEditorDialog::~CrEditorDialog()
{
    if (d)
        delete d;
    d = 0;
}

/*! Initializes the object with the default values. Return true if initialization was completed. */
bool CrEditorDialog::initialize(const ConsultResult &cr)
{
    d->ui->widget->setConsultResult(cr);
    return true;
}

ConsultResult CrEditorDialog::submit() const
{
    return d->ui->widget->submit();
}

void CrEditorDialog::onSmfgAboutClicked()
{
    SfmgAboutDialog dlg(this);
    dlg.exec();
}

void CrEditorDialog::onArgumentsClicked()
{
    QPushButton *but = qobject_cast<QPushButton*>(sender());
    Q_ASSERT(but);
    if (but)
        but->setEnabled(false);
    int id = d->ui->widget->currentEditingConsultResultId();
    if (id == -1)
        return;
    CrArgumentsDialog dlg(this);
    dlg.setRcId(id);
    dlg.exec();
    if (but)
        but->setEnabled(true);
}

