/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                      *
 *       Eric Maeker <eric.maeker@gmail.com>                             *
 *   Contributors:                                                         *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef EDRC_INTERNAL_EDRC_CONTEXTUALWIDGET_MANAGER_H
#define EDRC_INTERNAL_EDRC_CONTEXTUALWIDGET_MANAGER_H

#include <coreplugin/contextmanager/icontext.h>
#include <QObject>
#include <QPointer>

/**
 * \file ./plugins/edrcplugin/widgets/edrcwidgetmanager.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class IContext;
}

namespace eDRC {
namespace Internal {
class EdrcActionManager;
class EdrcContextualWidget;

class EdrcActionManager : public QObject
{
    Q_OBJECT
public:
    EdrcActionManager(QObject *parent = 0);
    virtual ~EdrcActionManager() {}
    
    void setCurrentView(EdrcContextualWidget *view);
    
private Q_SLOTS:
    void fileOpen();
    void fileSave();
    void fileSaveAs();
    void fileSavePDF();
    void filePrint();
    void filePrintPreview();
    void editItem();
    void addItem();
    void removeItem();
    void clearItems();
    void renewItem();
    void showDatabaseInformation();
    void aboutSFMG();

private Q_SLOTS:
    void updateActions();
    
protected:
    QAction *aClear, *aFileOpen, *aFileSave, *aFileSaveAs, *aFileSavePDF, *aFilePrint, *aFilePrintPreview;
    QAction *aAddItem, *aRemoveItem, *aEditItem, *aRenewItem;
    QAction *aShowDatabaseInformation, *aAboutSFMG;
    
    QPointer<EdrcContextualWidget> m_CurrentView;
};

class EdrcWidgetManager : public EdrcActionManager
{
    Q_OBJECT
public:
    explicit EdrcWidgetManager(QObject *parent = 0);
    ~EdrcWidgetManager();
    
    EdrcContextualWidget *currentView() const;
    
private Q_SLOTS:
    void updateContext(Core::IContext *object, const Core::Context &additionalContexts);
};

} // namespace Internal
} // namespace eDRC

#endif // EDRC_INTERNAL_EDRC_CONTEXTUALWIDGET_MANAGER_H

