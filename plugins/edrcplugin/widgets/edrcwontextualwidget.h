/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                      *
 *       Eric Maeker <eric.maeker@gmail.com>                             *
 *   Contributors:                                                         *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef EDRC_INTERNAL_EDRCCONTEXTUALUALWIDGET_H
#define EDRC_INTERNAL_EDRCCONTEXTUALUALWIDGET_H

#include <coreplugin/contextmanager/icontext.h>
#include <QWidget>
#include <QList>

/**
 * \file ./plugins/edrcplugin/widgets/edrcwontextualwidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

#include <QDebug>

namespace eDRC {
namespace Internal {
class EdrcContext;

class EdrcContextualWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit EdrcContextualWidget(QWidget *parent = 0);
    ~EdrcContextualWidget();

    EdrcContext *context() const {return m_Context;}

public Q_SLOTS:
    virtual void fileOpen() = 0;
    virtual void fileSave() = 0;
    virtual void fileSaveAs() = 0;
    virtual void fileSavePDF() = 0;
    virtual void filePrint() = 0;
    virtual void filePrintPreview() = 0;
    virtual void editItem() = 0;
    virtual void addItem() {qWarning() << "addItem";}
    virtual void removeItem() = 0;
    virtual void clearItems() = 0;
    virtual void renewItem() = 0;

private:
    EdrcContext *m_Context;
};

class EdrcContext : public Core::IContext
{
    Q_OBJECT
public:
    EdrcContext(EdrcContextualWidget *w) :
        Core::IContext(w)
    {
        setObjectName("EdrcContext");
        setWidget(w);
    }
};

} // namespace Internal
} // namespace eDRC

#endif // EDRC_INTERNAL_EDRCCONTEXTUALUALWIDGET_H

