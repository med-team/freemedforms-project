/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                      *
 *       Eric Maeker <eric.maeker@gmail.com>                               *
 *   Contributors:                                                         *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "edrcwontextualwidget.h"
#include <edrcplugin/constants.h>

#include <coreplugin/contextmanager/contextmanager.h>
#include <coreplugin/icore.h>

using namespace eDRC;
using namespace Internal;

static inline Core::ContextManager *contextManager() { return Core::ICore::instance()->contextManager(); }

EdrcContextualWidget::EdrcContextualWidget(QWidget *parent) :
    QWidget(parent),
    m_Context(0)
{
    Core::Context context(Constants::C_EDRC_PLUGIN);
    m_Context = new Internal::EdrcContext(this);
    m_Context->setContext(context);
    
    contextManager()->addContextObject(m_Context);
}

EdrcContextualWidget::~EdrcContextualWidget()
{
    contextManager()->removeContextObject(m_Context);
}


