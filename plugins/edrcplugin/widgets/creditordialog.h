/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef EDRC_INTERNAL_CREDITORDIALOG_H
#define EDRC_INTERNAL_CREDITORDIALOG_H

#include <QDialog>

/**
 * \file ./plugins/edrcplugin/widgets/creditordialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace eDRC {
namespace Internal {
class ConsultResult;
class CrEditorDialogPrivate;

class CrEditorDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit CrEditorDialog(QWidget *parent = 0);
    ~CrEditorDialog(); 
    bool initialize(const ConsultResult &cr);
    
    ConsultResult submit() const;
    
private Q_SLOTS:
    void onSmfgAboutClicked();
    void onArgumentsClicked();

private:
    CrEditorDialogPrivate *d;
};

} // namespace Internal
} // namespace EDRC

#endif // EDRC_INTERNAL_CREDITORDIALOG_H

