/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Christian A. Reiter <christian.a.reiter@gmail.com>                  *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "feedbackplugin/feedbackplugin.h"
#include "feedbackplugin/feedbackconstants.h"
#include "feedbackplugin/bugreportdialog.h"

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/itheme.h>
#include <coreplugin/iuser.h>
#include <coreplugin/translators.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/constants_menus.h>
#include <translationutils/trans_menu.h>
#include <translationutils/trans_spashandupdate.h>

#include <utils/log.h>
#include <extensionsystem/pluginmanager.h>

#include <QtPlugin>
#include <QDebug>
#include <QAction>

using namespace Feedback::Internal;

static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline void messageSplash(const QString &s) {theme()->messageSplashScreen(s); }

FeedbackPlugin::FeedbackPlugin()
{
    setObjectName("FeedbackPlugin");
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating Feedback";

    Core::ICore::instance()->translators()->addNewTranslator("plugin_feedback");



    connect(Core::ICore::instance(), SIGNAL(coreAboutToClose()), this, SLOT(coreAboutToClose()));
}

FeedbackPlugin::~FeedbackPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool FeedbackPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    Q_UNUSED(arguments)
    Q_UNUSED(errorString)

    if (Utils::Log::debugPluginsCreation()) {
        qWarning() << "creating Feedback";
    }







    Core::ActionManager *am = Core::ICore::instance()->actionManager();
    QAction *a = new QAction(this);

    a->setIcon(theme()->icon("face-sad.png"));
    Core::Command *cmd = am->registerAction(a, Core::Constants::A_BUGREPORT, Core::Context(Core::Constants::C_GLOBAL));
    cmd->setTranslations(Trans::Constants::BUG_REPORT);
    Core::ActionContainer *menu = am->actionContainer(Core::Constants::M_HELP);
    menu->addAction(cmd, Core::Id(Core::Constants::G_HELP_DEBUG));

    connect(a, SIGNAL(triggered()), this, SLOT(reportBug()));

    return true;
}

void FeedbackPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation()) {
        qWarning() << "Feedback::extensionsInitialized";
    }




    messageSplash(tr("Initializing Feedback..."));


    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));
    connect(Core::ICore::instance(), SIGNAL(coreOpened()), this, SLOT(postCoreInitialization()));
}

void FeedbackPlugin::postCoreInitialization()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

ExtensionSystem::IPlugin::ShutdownFlag FeedbackPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;


    return SynchronousShutdown;
}

void FeedbackPlugin::reportBug()
{
    Feedback::BugReportDialog dlg;
    QStringList categories;
    categories << tr("General comment")
               << tr("Forms management")
               << tr("Patient management")
               << tr("User management")
               << tr("Drugs management")
               << tr("Agenda management")
               << tr("Printings")
               << tr("Installation")
               << tr("Configuration");
    dlg.setBugCategories(categories);
    int result = dlg.exec();
    Q_UNUSED(result);
    return;
}


void FeedbackPlugin::coreAboutToClose()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

Q_EXPORT_PLUGIN(FeedbackPlugin)

