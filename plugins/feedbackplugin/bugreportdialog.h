/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Christian A Reiter, Eric Maeker                     *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FEEDBACK_BUGREPORTDIALOG_H
#define FEEDBACK_BUGREPORTDIALOG_H

#include <feedbackplugin/feedback_exporter.h>
#include <qglobal.h>
#include <QDialog>

/**
 * \file ./plugins/feedbackplugin/bugreportdialog.h
 * \author Christian A Reiter, Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
namespace Constants {
const char * const URL_ONLINEDOCUMENTATION     = "http://www.freemedforms.com/en/manuals/freemedforms/toc";
}  // End Constants
}  // End Utils

namespace Feedback {
namespace Internal {
class BugReportDialogPrivate;
} // namespace Internal

class FEEDBACK_EXPORT BugReportDialog : public QDialog
{
    Q_OBJECT

public:
    explicit BugReportDialog(QWidget *parent = 0);
    ~BugReportDialog();

    void setBugCategories(const QStringList &cat);


private Q_SLOTS:
    void validateInputs();
    void sendBugReport();
    void openFmfDocUrl();

private:
    void changeEvent(QEvent *e);

private:
    Internal::BugReportDialogPrivate *d;
};

} // namespace Feedback

#endif // FEEDBACK_BUGREPORTDIALOG_H
