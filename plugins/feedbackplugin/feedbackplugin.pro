#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

TARGET = Feedback
TEMPLATE = lib

DEFINES += FEEDBACK_LIBRARY
BUILD_PATH_POSTFIXE = FreeMedForms

INCLUDEPATH += ../

include(../fmf_plugins.pri)
include(feedback_dependencies.pri)

!with-feedback{
    error(Feedback plugin not requested)
} else {
    message(Building Feedback plugin)
}

HEADERS += \
    feedbackplugin.h\
    feedback_exporter.h\
    feedbackconstants.h \
    bugreportdialog.h

SOURCES += \
    feedbackplugin.cpp \
    bugreportdialog.cpp

OTHER_FILES = Feedback.pluginspec

#FREEMEDFORMS_SOURCES=%FreeMedFormsSources%
#IDE_BUILD_TREE=%FreeMedFormsBuild%

PROVIDER = FreeMedForms

#include translations
TRANSLATION_NAME = feedback
include($${SOURCES_ROOT_PATH}/buildspecs/translations.pri)

FORMS += \
    bugreportdialog.ui

