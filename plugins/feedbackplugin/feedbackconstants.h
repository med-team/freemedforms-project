/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Christian A. Reiter <christian.a.reiter@gmail.com>                  *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FEEDBACKCONSTANTS_H
#define FEEDBACKCONSTANTS_H

namespace Feedback {
namespace Constants {

const char * const ACTION_ID = "Feedback.Action";
const char * const MENU_ID = "Feedback.Menu";

} // namespace Feedback
} // namespace Constants

#endif // FEEDBACKCONSTANTS_H

