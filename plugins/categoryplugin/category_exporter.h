/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#ifndef CATEGORYEXPORTER_H
#define CATEGORYEXPORTER_H

#include <qglobal.h>

#if defined(CATEGORY_LIBRARY)
#define CATEGORY_EXPORT Q_DECL_EXPORT
#else
#define CATEGORY_EXPORT Q_DECL_IMPORT
#endif

#endif  // CATEGORYEXPORTER_H
