/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/






#include "categoryitem.h"

#include <coreplugin/icore.h>
#include <coreplugin/ipatient.h>
#include <coreplugin/iuser.h>
#include <coreplugin/constants_tokensandsettings.h>

#include <translationutils/constants.h>
#include <translationutils/trans_database.h>

#include <QHash>

#include <QDebug>

using namespace Category;
using namespace Internal;

namespace Category {
namespace Internal {
class CategoryItemPrivate
{
public:
    CategoryItemPrivate(CategoryItem */*parent*/) :
        m_Parent(0),
        m_IsDirty(false)  // , q(parent)
    {}

    ~CategoryItemPrivate()
    {
    }

public:
    CategoryItem *m_Parent;
    QHash<int, QVariant> m_Data;
    QHash<QString, QString> m_Labels; //K=lang, V=label
    QList<CategoryItem *> m_Children;
    QList<ICategoryContentItem *> m_ContentChildren;
    bool m_IsDirty;

private:
};
}
}

CategoryItem::CategoryItem() :
        d(new CategoryItemPrivate(this))
{
    d->m_Data.insert(DbOnly_IsValid, true);
    d->m_Data.insert(DbOnly_Id, -1);
    d->m_Data.insert(DbOnly_ParentId, -1);
    d->m_Data.insert(DbOnly_LabelId, -1);
}

CategoryItem::~CategoryItem()
{
    if (d) {
        delete d;
    }
    d = 0;
}


void CategoryItem::setParent(CategoryItem *parent)
{
    d->m_Parent = parent;
    if (parent)
        setData(DbOnly_ParentId, parent->id());
    d->m_IsDirty = true;
}

CategoryItem *CategoryItem::parent() const
{
    return d->m_Parent;
}


void CategoryItem::addChild(CategoryItem *child)
{
    d->m_Children.append(child);
    child->setParent(this);
}


void CategoryItem::addChildren(const QVector<CategoryItem *> &children)
{
    d->m_Children.append(children.toList());
    for(int i=0; i<children.count(); ++i)
        children.at(i)->setParent(this);
}

void CategoryItem::insertChild(CategoryItem *child, int row)
{
    d->m_Children.insert(row, child);
}

void CategoryItem::updateChildrenSortId()
{
    for(int i=0; i < d->m_Children.count(); ++i) {
        d->m_Children[i]->setData(CategoryItem::SortId, i+1);
    }
}

void CategoryItem::clearChildren()
{
    qDeleteAll(d->m_Children);
    d->m_Children.clear();
}

CategoryItem *CategoryItem::child(int number) const
{
    if (number < d->m_Children.count()) {
        return d->m_Children.at(number);
    }
    return 0;
}

QList<CategoryItem *> CategoryItem::children() const
{
    return d->m_Children;
}

int CategoryItem::childCount() const
{
    return d->m_Children.count();
}

int CategoryItem::childNumber() const
{
    if (d->m_Parent)
        return d->m_Parent->children().indexOf(const_cast<CategoryItem*>(this));
    return 0;
}

bool CategoryItem::setData(const int ref, const QVariant &value)
{
    if (d->m_Data.value(ref)==value)
        return true;
    if (ref==ThemedIcon) {
        QString name = value.toString();
        if (name.startsWith(Core::Constants::TAG_APPLICATION_THEME_PATH)) {
            name = name.remove(Core::Constants::TAG_APPLICATION_THEME_PATH);
            if (name.startsWith("/"))
                name = name.mid(1);
        }
        d->m_IsDirty = true;
        d->m_Data.insert(ref, name);
        return true;
    }
    d->m_IsDirty = true;
    d->m_Data.insert(ref, value);
    return true;
}

QVariant CategoryItem::data(const int ref) const
{
    return d->m_Data.value(ref);
}

bool CategoryItem::isDirty() const
{
    return d->m_IsDirty;
}

void CategoryItem::setDirty(bool state)
{
    d->m_IsDirty = state;
}

bool CategoryItem::setLabel(const QString &label, const QString &lang)
{
    if (d->m_Labels.value(lang) == label)
        return true;
    if (lang.isEmpty()) {
        d->m_Labels.insert(QLocale().name().left(2), label);
    } else {
        d->m_Labels.insert(lang, label);
    }
    d->m_IsDirty = true;
    return true;
}

QString CategoryItem::label(const QString &lang) const
{
    if (lang.isEmpty() || (!d->m_Labels.keys().contains(lang))) {
        const QString &lang = QLocale().name().left(2);
        QString t = d->m_Labels.value(lang, QString::null);
        if (t.isEmpty()) {
            t = d->m_Labels.value(Trans::Constants::ALL_LANGUAGE, QString::null);
        }
        return t;
    }
    return d->m_Labels.value(lang, QString::null);
}

QStringList CategoryItem::allLanguagesForLabel() const
{
    return d->m_Labels.keys();
}

void CategoryItem::clearLabels()
{
    d->m_Labels.clear();
    d->m_IsDirty = true;
}

void CategoryItem::removeLabel(const QString &lang)
{
    if (lang.isEmpty()) {
        d->m_Labels.remove(QLocale().name().left(2));
        d->m_Labels.remove(Trans::Constants::ALL_LANGUAGE);
    } else {
        d->m_Labels.remove(lang);
    }
    d->m_IsDirty = true;
}

bool CategoryItem::sortChildren()
{
    qSort(d->m_Children.begin(), d->m_Children.end(), CategoryItem::lessThan);
    return true;
}

bool CategoryItem::lessThan(const CategoryItem *c1, const CategoryItem *c2)
{
    return c1->sortId() < c2->sortId();
}

void CategoryItem::addContentItem(ICategoryContentItem *data)
{
    d->m_ContentChildren.append(data);
}

QList<ICategoryContentItem *> CategoryItem::contentItems() const
{
    return d->m_ContentChildren;
}

void CategoryItem::clearContentItems()
{
    d->m_ContentChildren.clear();
}

void CategoryItem::warn() const
{
    QString tmp = "Category(";
    tmp += QString("Id:%1; ").arg(data(DbOnly_Id).toString());
    tmp += QString("LId:%1; ").arg(data(DbOnly_LabelId).toString());
    tmp += QString("ParentId:%1; ").arg(data(DbOnly_ParentId).toString());
    tmp += QString("IsValid:%1; ").arg(data(DbOnly_IsValid).toString());
    tmp += QString("IsDirty:%1; ").arg(isDirty());
    tmp += QString("SortId:%1; ").arg(data(SortId).toString());
    tmp += "\n          ";
    tmp += QString("Label:%1; ").arg(label());
    tmp += QString("Mime:%1; ").arg(data(DbOnly_Mime).toString());
    tmp += "\n          ";
    tmp += QString("Icon:%1; ").arg(data(ThemedIcon).toString());
    tmp += QString("Password:%1; ").arg(data(Password).toString());
    tmp += "\n          ";
    tmp += QString("Extra:%1; ").arg(data(ExtraXml).toString());
    tmp.chop(2);
    qWarning() << tmp+")";
}
