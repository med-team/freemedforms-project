/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CATEGORY_CORE_H
#define CATEGORY_CORE_H

#include <categoryplugin/category_exporter.h>
#include <QObject>
#include <QStringList>

namespace Category {
class CategoryItem;
class ICategoryContentItem;
namespace Internal {
class CategoryCorePrivate;
}

class CATEGORY_EXPORT CategoryCore : public QObject
{
    Q_OBJECT

protected:
    CategoryCore(QObject *parent = 0);

public:
    // Constructor
    static CategoryCore *instance(QObject *parent = 0);
    virtual ~CategoryCore();

    bool initialize();

    QVector<CategoryItem *> getCategories(const QString &mime, const QStringList &uuids = QStringList()) const;
    QList<CategoryItem *> createCategoryTree(const QVector<CategoryItem *> &cats) const;
    QVector<Category::CategoryItem *> flattenCategoryTree(const QVector<Category::CategoryItem *> &category);

    bool linkContentItemWithCategoryItem(const QVector<Category::CategoryItem *> &cats, const QVector<Category::ICategoryContentItem *> &contents) const;

    bool saveCategory(CategoryItem *category);
    bool saveCategories(const QVector<CategoryItem *> &categories);
    bool removeAllExistingCategories(const QString &mime);

    Category::CategoryItem *findCategory(const int usingReference, const QVariant &searchValue, Category::CategoryItem *categories);

private:
    static CategoryCore *m_Instance;
    Internal::CategoryCorePrivate *d;
};

}  // End namespace Category


#endif // CATEGORY_CORE_H
