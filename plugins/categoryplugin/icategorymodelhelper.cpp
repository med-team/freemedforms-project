/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "icategorymodelhelper.h"
#include "categoryonlyproxymodel.h"

#include <QDebug>

using namespace Category;

void ICategoryModelHelper::updateCategoryLabel(const Category::CategoryItem *category)
{
    QModelIndex idx = indexForCategory(category);
    Q_EMIT dataChanged(idx, idx);
}

CategoryOnlyProxyModel *ICategoryModelHelper::categoryOnlyModel()
{
    if (!m_Proxy) {
        m_Proxy = new CategoryOnlyProxyModel(this);
    }
    return m_Proxy;
}
