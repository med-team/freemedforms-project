/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "categoryplugin.h"
#include "categorycore.h"

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/translators.h>

#include <utils/log.h>

#include <QtCore/QtPlugin>
#include <QDebug>

using namespace Category;
using namespace Internal;

CategoryPlugin::CategoryPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating CategoryPlugin";

    Core::ICore::instance()->translators()->addNewTranslator("plugin_category");

    CategoryCore::instance(this);
}

CategoryPlugin::~CategoryPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool CategoryPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "CategoryPlugin::initialize";
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);

    return true;
}

void CategoryPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "CategoryPlugin::extensionsInitialized";
    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));

    if (!CategoryCore::instance()->initialize())
        LOG_ERROR("Category core not initialized");
}

ExtensionSystem::IPlugin::ShutdownFlag CategoryPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(CategoryPlugin)
