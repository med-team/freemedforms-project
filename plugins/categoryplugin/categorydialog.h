/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CATEGORYPLUGIN_CATEGORYDIALOG_H
#define CATEGORYPLUGIN_CATEGORYDIALOG_H

#include <categoryplugin/category_exporter.h>

#include <QDialog>
#include <QAbstractTableModel>

namespace Category {
class ICategoryModelHelper;

namespace Internal {
class CategoryDialogPrivate;
}

class CATEGORY_EXPORT CategoryDialog : public QDialog
{
    Q_OBJECT
public:
    explicit CategoryDialog(QWidget *parent = 0);
    void setCategoryModel(ICategoryModelHelper *model, const int labelColumn);

public Q_SLOTS:
    void editItem(const QModelIndex &current, const QModelIndex &previous);

    void done(int r);

private:
    Internal::CategoryDialogPrivate *d;
};

}  // End namespace PMH

#endif // CATEGORYPLUGIN_CATEGORYDIALOG_H
