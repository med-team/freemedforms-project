/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "categoryonlyproxymodel.h"
#include "icategorymodelhelper.h"
#include "categoryitem.h"

#include <translationutils/constanttranslations.h>

#include <QItemSelectionRange>
#include <QDebug>

using namespace Category;
using namespace Internal;
using namespace Trans::ConstantTranslations;

namespace Category {
namespace Internal {
class CategoryOnlyProxyModelPrivate
{
public:
    CategoryOnlyProxyModelPrivate() : m_Model(0) {}
    ~CategoryOnlyProxyModelPrivate() {}

public:
    ICategoryModelHelper *m_Model;
    QMap<QPersistentModelIndex, QPersistentModelIndex> mapping;
    QMap<QPersistentModelIndex, QPersistentModelIndex> proxySourceParent;
    bool m_HidePmh;
};

}  // End namespace Internal
}  // End namespace PMH


CategoryOnlyProxyModel::CategoryOnlyProxyModel(ICategoryModelHelper *parent) :
        QAbstractProxyModel(parent), d(new CategoryOnlyProxyModelPrivate)
{
    d->m_Model = parent;
    d->m_HidePmh = true;
    setSourceModel(parent);
    updateModel();
    connect(parent, SIGNAL(modelReset()), this, SLOT(updateModel()));
    connect(parent, SIGNAL(layoutChanged()), this, SLOT(updateModel()));
    connect(parent, SIGNAL(rowsRemoved(QModelIndex,int,int)), this, SLOT(updateModel()));
    connect(parent, SIGNAL(rowsInserted(QModelIndex,int,int)), this, SLOT(updateModel()));
    connect(parent, SIGNAL(rowsMoved(QModelIndex,int,int,QModelIndex,int)), this, SLOT(updateModel()));
    connect(parent, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(emitDataChanged(QModelIndex,QModelIndex)));
}

CategoryOnlyProxyModel::~CategoryOnlyProxyModel()
{
    if (d)
        delete d;
    d=0;
}

int CategoryOnlyProxyModel::rowCount(const QModelIndex &parent) const
{
    QModelIndex sourceParent;
    if (parent.isValid())
        sourceParent = mapToSource(parent);
    int count = 0;
    QMapIterator<QPersistentModelIndex, QPersistentModelIndex> it(d->proxySourceParent);
    while (it.hasNext()) {
        it.next();
        if (it.value() == sourceParent)
            count++;
    }
    return count;
}

int CategoryOnlyProxyModel::columnCount(const QModelIndex &) const
{
    return 1; // d->m_Model->columnCount(index); // Causes views to be non selectables...
}

QModelIndex CategoryOnlyProxyModel::index(int row, int column, const QModelIndex &parent) const
{
    QModelIndex sourceParent;
    if (parent.isValid())
        sourceParent = mapToSource(parent);
    QMapIterator<QPersistentModelIndex, QPersistentModelIndex> it(d->proxySourceParent);
    while (it.hasNext()) {
        it.next();
        if (it.value() == sourceParent && it.key().row() == row &&
            it.key().column() == column)
            return it.key();
    }
    return QModelIndex();
}

QModelIndex CategoryOnlyProxyModel::parent(const QModelIndex &child) const
{
    QModelIndex mi = d->proxySourceParent.value(child);
    if (mi.isValid())
        return mapFromSource(mi);
    return QModelIndex();
}

QModelIndex CategoryOnlyProxyModel::mapToSource(const QModelIndex &proxyIndex) const
{
    if (!proxyIndex.isValid())
        return QModelIndex();
    return d->mapping.key(proxyIndex);
}

QModelIndex CategoryOnlyProxyModel::mapFromSource(const QModelIndex &sourceIndex) const
{
    if (!sourceIndex.isValid())
        return QModelIndex();
    return d->mapping.value(sourceIndex);
}











bool CategoryOnlyProxyModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(row);
    Q_UNUSED(count);
    Q_UNUSED(parent);
    return true;
}

bool CategoryOnlyProxyModel::insertRows(int row, int count, const QModelIndex &parent)
{
    for(int i=0; i < count; ++i) {
        Category::CategoryItem *cat = new Category::CategoryItem;
        cat->setParent(d->m_Model->categoryForIndex(mapToSource(parent)));
        cat->setData(CategoryItem::DbOnly_Mime, d->m_Model->mime());
        cat->setLabel(tkTr(Trans::Constants::FILENEW_TEXT).remove("&"), QLocale().name().left(2));
        d->m_Model->addCategory(cat, row+i, parent);
    }
    return true;
}


void CategoryOnlyProxyModel::updateBranch(QModelIndex &rootIndex)
{
    if (d->m_Model->isCategory(rootIndex)) {
        QModelIndex proxy = createIndex(rootIndex.row(), rootIndex.column(), rootIndex.internalPointer());
        d->mapping.insert(QPersistentModelIndex(rootIndex), proxy);
        QModelIndex sourceParent;
        if (rootIndex.parent().isValid())
            sourceParent = rootIndex.parent();
        d->proxySourceParent.insert(proxy, sourceParent);

        for(int i = 0; i < d->m_Model->rowCount(rootIndex); ++i) {
            QModelIndex idx = d->m_Model->index(i, 0, rootIndex);
            updateBranch(idx);
        }
    }
}

void CategoryOnlyProxyModel::updateModel()
{
    d->mapping.clear();
    d->proxySourceParent.clear();
    for (int i = 0; i<d->m_Model->rowCount(); ++i) {
        QModelIndex idx = d->m_Model->index(i, 0);
        updateBranch(idx);
    }
    Q_EMIT layoutChanged();
}

void CategoryOnlyProxyModel::emitDataChanged(const QModelIndex &left, const QModelIndex &right)
{
    Q_EMIT dataChanged(mapFromSource(left), mapFromSource(right));
}
