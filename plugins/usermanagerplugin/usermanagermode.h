/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef USERMANAGERMODE_H
#define USERMANAGERMODE_H

#include <coreplugin/modemanager/imode.h>

#include <QtCore/QObject>

namespace UserPlugin {
namespace Internal {

class UserManagerMode : public Core::IMode
{
    Q_OBJECT
public:
    UserManagerMode(QObject *parent = 0);
    ~UserManagerMode();

    QString name() const;

private Q_SLOTS:
    void onUserChanged();

private:
    bool inPool;
};

}  // End namespace Internal
}  // End namespace UserPlugin


#endif // USERMANAGERMODE_H
