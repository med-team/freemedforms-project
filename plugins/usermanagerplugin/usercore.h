/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef USERPLUGIN_USERCORE_H
#define USERPLUGIN_USERCORE_H

#include <usermanagerplugin/usermanager_exporter.h>
#include <QObject>

/**
 * \file ./plugins/usermanagerplugin/usercore.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace UserPlugin {
class UserModel;
class UserCreationPage;

namespace Internal {
class UserCorePrivate;
class UserManagerPlugin;
class UserBase;
} // namespace Internal

class USER_EXPORT UserCore : public QObject
{
    Q_OBJECT
    friend class UserPlugin::Internal::UserManagerPlugin;
    friend class UserPlugin::UserCreationPage;

protected:
    explicit UserCore(QObject *parent = 0);
    bool initialize();
    bool forceReInitialization();
    bool postCoreInitialization();

public:
    static UserCore &instance();
    ~UserCore();

    bool isInitialized() const;
    Internal::UserBase *userBase() const;
    UserModel *userModel() const;

Q_SIGNALS:

public Q_SLOTS:

private:
    Internal::UserCorePrivate *d;
    static UserCore *_instance;
};

} // namespace UserPlugin

#endif  // USERPLUGIN_USERCORE_H

