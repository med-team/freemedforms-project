/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "userlineeditcompletersearch.h"
#include <usermanagerplugin/usercore.h>
#include <usermanagerplugin/database/userbase.h>
#include <usermanagerplugin/constants.h>

#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/constants_icons.h>

#include <translationutils/constanttranslations.h>

#include <QSqlTableModel>
#include <QAbstractItemView>
#include <QToolButton>

using namespace UserPlugin;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline UserPlugin::UserCore &userCore() {return UserPlugin::UserCore::instance();}
static inline UserPlugin::Internal::UserBase *userBase() {return userCore().userBase();}

namespace {

class UserCompleterModel : public QSqlQueryModel
{
public:
    enum { ColumnFullName = 0 , Uid};

    enum FilterListIndex {
        NameIndex = 0,
        FirstNameIndex,
        SecondNameIndex
    };

    UserCompleterModel(QObject *parent) : QSqlQueryModel(parent)
    {
    }

    void setNameFilter(const QStringList &names)
    {
        QHash<int, QString> where;
        where.insert(Constants::USER_VALIDITY, "=1");
        if (!names.at(NameIndex).isEmpty())
            where.insert(Constants::USER_USUALNAME, QString("like '%1%'").arg(names.at(NameIndex)));
        if (!names.at(FirstNameIndex).isEmpty())
            where.insert(Constants::USER_FIRSTNAME, QString("like '%1%'").arg(names.at(FirstNameIndex)));
        if (!names.at(SecondNameIndex).isEmpty())
            where.insert(Constants::USER_OTHERNAMES, QString("like '%1%'").arg(names.at(SecondNameIndex)));
        QString req = userBase()->select(Constants::Table_USERS,
                                            QList<int>()
                                            << Constants::USER_USUALNAME
                                            << Constants::USER_FIRSTNAME
                                            << Constants::USER_OTHERNAMES
                                            << Constants::USER_UUID,
                                            where
                                            );
        req += QString("\n  ORDER BY `%1` ASC").arg(userBase()->fieldName(Constants::Table_USERS, Constants::USER_USUALNAME));
        req += "\n  LIMIT 20";
        setQuery(req, userBase()->database());
    }

    QVariant data(const QModelIndex &idx, int role) const
    {
        if (!idx.isValid())
            return QVariant();

        if (idx.column()==ColumnFullName) {
            if (role==Qt::DisplayRole || role==Qt::EditRole || role==Qt::ToolTipRole) {
                QString name = QSqlQueryModel::data(QSqlQueryModel::index(idx.row(), 0, idx.parent())).toString();
                QString firstName = QSqlQueryModel::data(QSqlQueryModel::index(idx.row(), 1, idx.parent())).toString();
                QString secName = QSqlQueryModel::data(QSqlQueryModel::index(idx.row(), 2, idx.parent())).toString();
                QString r = QString("%1 %2 %3").arg(name).arg(firstName).arg(secName).simplified();
                return r;
            }
        } else if (idx.column()==Uid && role==Qt::DisplayRole) {
            return QSqlQueryModel::data(QSqlQueryModel::index(idx.row(), 3)).toString();
        }


        return QVariant();
    }

private:
    QString m_NameFilter;
};

class UserValidator : public QValidator
{
public:
    UserValidator(QObject *parent) :
        QValidator(parent),
        m_Model(0)
    {
    }

    QValidator::State validate(QString &string, int &) const
    {
        if ((string.size() - m_LastString.size()) > 1) {
            return QValidator::Acceptable;
        }
        m_LastString = string;
        QString separator;
        if (string.contains("/"))
            separator = "/";
        if (string.contains(":"))
            separator = ":";
        if (string.contains(","))
            separator = ",";
        if (string.contains(";"))
            separator = ";";
        if (string.contains(" "))
            separator = " ";

        QString sql = string;
        sql = sql.replace("*", "%");

        if (!separator.isEmpty()) {
            QStringList list = sql.split(separator, QString::KeepEmptyParts);
            for(int i = 0; list.count()-4; ++i) {
                list << "";
            }
            m_Model->setNameFilter(list);
            return QValidator::Acceptable;
        }
        m_Model->setNameFilter(QStringList() << sql << "" << "" << "");
        return QValidator::Acceptable;
    }

    void fixup(QString &) const {}

    void setModelToFilter(UserCompleterModel *model) {m_Model=model;}

private:
    UserCompleterModel *m_Model;
    mutable QString m_LastString;
};
}


namespace UserPlugin {
namespace Internal {
class UserCompleterPrivate
{
public:
    UserCompleterPrivate() : m_Model(0), m_Validator(0) {}
    ~UserCompleterPrivate()
    {
        if (m_Model) {
            delete m_Model;
            m_Model = 0;
        }
        if (m_Validator) {
            delete m_Validator;
            m_Validator = 0;
        }
    }

    UserCompleterModel *m_Model;
    UserValidator *m_Validator;
};
}
}

UserCompleter::UserCompleter(QObject *parent) :
    QCompleter(parent),
    d(new Internal::UserCompleterPrivate)
{
    d->m_Model = new UserCompleterModel(this);
    d->m_Validator = new UserValidator(this);
    d->m_Validator->setModelToFilter(d->m_Model);
    setModel(d->m_Model);
    setCaseSensitivity(Qt::CaseInsensitive);
    setCompletionColumn(UserCompleterModel::ColumnFullName);
    setCompletionMode(QCompleter::UnfilteredPopupCompletion);
    popup()->setAlternatingRowColors(true);
}

UserCompleter::~UserCompleter()
{
    if (d) delete d;
    d = 0;
}

QValidator *UserCompleter::validator() const
{
    return static_cast<QValidator*>(d->m_Validator);
}

UserLineEditCompleterSearch::UserLineEditCompleterSearch(QWidget *parent) :
    Utils::QButtonLineEdit(parent),
    m_Completer(0)
{
    QToolButton *cancel = new QToolButton;
    cancel->setIcon(theme()->icon(Core::Constants::ICONCLEARLINEEDIT));
    cancel->setToolTip(tkTr(Trans::Constants::CLEAR));
    setRightButton(cancel);
    connect(cancel, SIGNAL(clicked()), this, SLOT(cancelSearch()));

    m_Completer = new UserCompleter;
    setCompleter(m_Completer);
    setValidator(m_Completer->validator());

    connect(this, SIGNAL(textChanged(QString)), this, SLOT(textChanged(QString)));
    connect(m_Completer, SIGNAL(activated(QModelIndex)), this, SLOT(userSelected(QModelIndex)));
}

UserLineEditCompleterSearch::~UserLineEditCompleterSearch()
{
}

void UserLineEditCompleterSearch::textChanged(const QString &newText)
{
    int diff = newText.size() - m_LastSearch.size();
    if (diff > 1 || diff < -1) {
        return;
    }
    m_LastSearch = newText;
    m_Completer->validator()->validate(m_LastSearch, diff);
    m_Completer->setCompletionPrefix(m_LastSearch);
}

void UserLineEditCompleterSearch::cancelSearch()
{
    setText("");
    QRect cr = rect();
    m_Completer->complete(cr); // popup it up!
}

void UserLineEditCompleterSearch::userSelected(const QModelIndex &index)
{
    QString uid = m_Completer->model()->index(index.row(), UserCompleter::Uid, index.parent()).data().toString();
    qWarning() << index.data().toString() << uid;
    Q_EMIT selectedUser(index.data().toString(), uid);
}



