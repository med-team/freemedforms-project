/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef USERRIGHTSWIDGET_H
#define USERRIGHTSWIDGET_H

#include <usermanagerplugin/constants.h>

#include <QListView>
#include <QAbstractListModel>
class QEvent;

#include <QDebug>

/**
 * \file ./plugins/usermanagerplugin/widgets/userrightswidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace UserPlugin {
namespace Internal {

class UserRightsModel : public QAbstractListModel
{
    Q_OBJECT
public:
    UserRightsModel(QObject *parent);
    ~UserRightsModel() {}

    void setRights(const int r)     { m_Rights = r; }
    int  getRights()                { return m_Rights; }

    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

    Qt::ItemFlags flags(const QModelIndex &index) const;

    void retranslate();

private:
    QStringList m_RightsName;
    QHash<int, int> m_NameToRole;
    int m_Rights;
};

class UserRightsWidget : public QListView
{
    Q_OBJECT
    Q_PROPERTY(int rights    READ getRights    WRITE setRights    USER true)
public:
    UserRightsWidget(QWidget * parent = 0);

    void setRights(const int r)     { m_Model->setRights(r); }
    int  getRights()                { return m_Model->getRights(); }

private:
    void changeEvent(QEvent *e);

private:
    UserRightsModel *m_Model;
};

}  // End Internal
}  // End UserPlugin

#endif // USERRIGHTSWIDGET_H
