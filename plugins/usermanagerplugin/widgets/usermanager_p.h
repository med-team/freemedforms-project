/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef USERMANAGERWIDGET_P_H
#define USERMANAGERWIDGET_P_H

#include <QWidget>

QT_BEGIN_NAMESPACE
class QModelIndex;
class QAction;
QT_END_NAMESPACE

/**
 * \file ./plugins/usermanagerplugin/widgets/usermanager_p.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace UserPlugin {
namespace Internal {
class UserManagerContext;
class UserManagerWidgetPrivate;

class UserManagerWidget : public QWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(UserManagerWidget)

public:
    explicit UserManagerWidget(QWidget *parent = 0); // work with usermodel
    ~UserManagerWidget();
    bool initialize();
    void resizeSplitter();

    bool canCloseParent();

private Q_SLOTS:
    void onCurrentUserChanged();
    void onSearchRequested();
    void onSearchToolButtonTriggered(QAction *act);
    void onSaveRequested();
    void onCurrentSelectedIndexChanged(const QModelIndex &current, const QModelIndex &previous);
    void onCreateUserRequested();
    void onClearModificationRequested();
    void onDeleteUserRequested();
    void toggleSearchView(bool checked);

    void showUserDebugDialog(const QModelIndex &id);

private:
    void selectuserTreeView(int row);
    void changeEvent(QEvent *e);
    void retranslate();

Q_SIGNALS:
    void closeRequested();

private:
    Internal::UserManagerWidgetPrivate *d;

//public:
//    UserManagerContext *m_Context;
};

}  // End Internal
}  // End UserPlugin


#endif // USERMANAGER_P_H
