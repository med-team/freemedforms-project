/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IUSERVIEWERPAGE_H
#define IUSERVIEWERPAGE_H

#include <usermanagerplugin/usermanager_exporter.h>
#include <coreplugin/igenericpage.h>

/**
 * \file ./plugins/usermanagerplugin/widgets/iuserviewerpage.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace UserPlugin {
class UserModel;

class USER_EXPORT IUserViewerWidget : public QWidget
{
    Q_OBJECT
public:
    explicit IUserViewerWidget(QWidget *parent = 0) : QWidget(parent) {}
    virtual ~IUserViewerWidget() {}

    virtual const QString &parentUserViewerPageId() const = 0;
    virtual void setUserModel(UserModel *model) = 0;
    virtual void setUserIndex(const int row) = 0;
    virtual void clear() = 0;
    virtual bool submit() = 0;
};

class USER_EXPORT IUserViewerPage : public Core::IGenericPage
{
    Q_OBJECT
public:
    explicit IUserViewerPage(QObject *parent = 0) : Core::IGenericPage(parent) {}
    virtual ~IUserViewerPage() {}
};

}  // End namespace UserPlugin

#endif // IUSERVIEWERPAGE_H
