/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef USERMANAGER_MAINWINDOW_H
#define USERMANAGER_MAINWINDOW_H

#include <usermanagerplugin/usermanager_exporter.h>
#include <coreplugin/imainwindow.h>

#include <QDialog>
#include <QStyledItemDelegate>

/**
 * \file ./plugins/usermanagerplugin/widgets/usermanager.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace UserPlugin {
namespace Internal {
class UserManagerModel;
class UserManagerWidget;
namespace Ui{
class UserViewerTreeDelegateWidget;
}  // namespace Ui
}  // namespace Internal

class USER_EXPORT UserManagerMainWindow : public Core::IMainWindow
{
    Q_OBJECT
    Q_DISABLE_COPY(UserManagerMainWindow)
public:
    explicit UserManagerMainWindow(QWidget *parent = 0);
    ~UserManagerMainWindow();

    virtual bool initialize(const QStringList &arguments = QStringList(), QString *errorString = 0);
    virtual void extensionsInitialized();

protected:
    void closeEvent(QCloseEvent *event);

private:
    Internal::UserManagerWidget *m_Widget;
};



class USER_EXPORT UserManagerDialog : public QDialog
{
    Q_OBJECT
    Q_DISABLE_COPY(UserManagerDialog)
public:
    explicit UserManagerDialog(QWidget *parent = 0);
    ~UserManagerDialog();

    virtual bool initialize();
    virtual bool initializeAfterShowing();

protected:
    void done(int r);

private:
    void showEvent(QShowEvent *event);

private:
    Internal::UserManagerWidget *m_Widget;
};


}  // End UserPlugin

#endif // USERMANAGER_MAINWINDOW_H
