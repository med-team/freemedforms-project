/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       Christian A. Reiter <christian.a.reiter@gmail.com>>               *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "useridentityandloginpage.h"
#include <usermanagerplugin/usercore.h>
#include <usermanagerplugin/database/userbase.h>

#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/constants_icons.h>
#include <coreplugin/translators.h>

#include <identityplugin/identityeditorwidget.h>

#include <utils/log.h>
#include <utils/global.h>
#include <translationutils/constants.h>

#include <QHBoxLayout>

using namespace UserPlugin;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline UserPlugin::UserCore &userCore() {return UserPlugin::UserCore::instance();}
static inline UserPlugin::Internal::UserBase *userBase() {return userCore().userBase();}

UserIdentityAndLoginPage::UserIdentityAndLoginPage(QWidget *parent) :
    QWizardPage(parent),
    _identity(0),
    _showErrorLabels(false)
{

    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->setSizeConstraint(QLayout::SetDefaultConstraint);
    layout->setMargin(0);
    layout->setSpacing(0);

    _identity = new Identity::IdentityEditorWidget(this);
    _identity->setAvailableWidgets(Identity::IdentityEditorWidget::FullIdentity | Identity::IdentityEditorWidget::Photo | Identity::IdentityEditorWidget::FullLogin);
    layout->addWidget(_identity);
    setLayout(layout);

    registerField("UsualName*", _identity, "usualName");
    registerField("Firstname*", _identity, "firstName");
    registerField("OtherNames", _identity, "otherNames");
    registerField("Title", _identity, "title");
    registerField("TitleIndex", _identity, "titleIndex");
    registerField("GenderIndex", _identity, "genderIndex");
    registerField("Gender", _identity, "gender");
    registerField("Language*", _identity, "language");

    registerField("Login*", _identity, "clearLogin");
    registerField("Password*", _identity, "clearPassword");

    connect(_identity, SIGNAL(clearLoginEditionFinished()), this, SLOT(checkCompleteState()));
    connect(_identity, SIGNAL(passwordConfirmed()), this, SLOT(checkCompleteState()));
    connect(_identity, SIGNAL(titleChanged(QString)), this, SLOT(checkCompleteState()));
    connect(_identity, SIGNAL(titleIndexChanged(int)), this, SLOT(checkCompleteState()));
    connect(_identity, SIGNAL(usualNameChanged(QString)), this, SLOT(checkCompleteState()));
    connect(_identity, SIGNAL(otherNamesChanged(QString)), this, SLOT(checkCompleteState()));
    connect(_identity, SIGNAL(firstNameChanged(QString)), this, SLOT(checkCompleteState()));
    connect(_identity, SIGNAL(dateOfBirthChanged(QDate)), this, SLOT(checkCompleteState()));
    connect(_identity, SIGNAL(genderIndexChanged(int)), this, SLOT(checkCompleteState()));
    connect(_identity, SIGNAL(genderChanged(QString)), this, SLOT(checkCompleteState()));
    connect(_identity, SIGNAL(languageChanged(QString)), this, SLOT(checkCompleteState()));
    connect(_identity, SIGNAL(clearLoginChanged(QString)), this, SLOT(checkCompleteState()));
    connect(_identity, SIGNAL(clearPasswordChanged(QString)), this, SLOT(checkCompleteState()));
}

UserIdentityAndLoginPage::~UserIdentityAndLoginPage()
{
}


bool UserIdentityAndLoginPage::checkLogin() const
{
    const QString &login = _identity->currentClearLogin();
    if (login.length() < 6 || login.length() > 16 ) // FIXME : avoid magic number \sa void PasswordWidget::onLoginChanged(const QString &login)
        return false;

    if (userBase()->isLoginAlreadyExists(_identity->currentClearLogin())) {
        Utils::warningMessageBox(tr("Login error"), tr("Login already in use. Please select another login"));
        return false;
    }

    return true;
}

void UserIdentityAndLoginPage::checkCompleteState()
{
    if (checkLogin() && _identity->isIdentityValid(false)) {
        Q_EMIT completeChanged();
    }
}


void UserIdentityAndLoginPage::onPasswordConfirmed()
{
    if (checkLogin())
        Q_EMIT completeChanged();
}

void UserIdentityAndLoginPage::changeEvent(QEvent *e)
{
    if (e->type() == QEvent::LanguageChange)
        retranslate();
}

QSize UserIdentityAndLoginPage::sizeHint() const
{
    return _identity->sizeHint();
}

QSize UserIdentityAndLoginPage::minimumSizeHint() const
{
    return _identity->minimumSizeHint();
}

void UserIdentityAndLoginPage::retranslate()
{
    setTitle(tr("Create a new user"));
    setSubTitle(tr("Please enter your identity."));
}

void UserIdentityAndLoginPage::initializePage()
{
    wizard()->resize(_identity->sizeHint() + QSize(100, 50));
}

bool UserIdentityAndLoginPage::isComplete() const
{
    return (!_identity->currentUsualName().isEmpty()
            && !_identity->currentFirstName().isEmpty()
            && !_identity->currentGender().isEmpty()
            && !_identity->currentLanguage().isEmpty()
            && checkLogin()
            && _identity->isPasswordCompleted());
}


bool UserIdentityAndLoginPage::validatePage()
{
    return true;
}
