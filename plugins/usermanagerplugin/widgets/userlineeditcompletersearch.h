/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef USERPLUGINLINEEDITCOMPLETER_H
#define USERPLUGINLINEEDITCOMPLETER_H

#include <usermanagerplugin/usermanager_exporter.h>
#include <utils/widgets/qbuttonlineedit.h>

#include <QCompleter>
#include <QValidator>

class QSqlTableModel;

/**
 * \file ./plugins/usermanagerplugin/widgets/userlineeditcompletersearch.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace UserPlugin {
namespace Internal {
class UserCompleterPrivate;

class UserCompleter : public QCompleter
{
    Q_OBJECT
public:
    enum CompleterModelRepresentation {
        FullName = 0,
        Uid
    };

    explicit UserCompleter(QObject *parent = 0);
    ~UserCompleter();

    QValidator *validator() const;

private:
    Internal::UserCompleterPrivate *d;
};
}  // End namespace Internal

class USER_EXPORT UserLineEditCompleterSearch : public Utils::QButtonLineEdit
{
    Q_OBJECT
public:
    explicit UserLineEditCompleterSearch(QWidget *parent = 0);
    ~UserLineEditCompleterSearch();

Q_SIGNALS:
    void selectedUser(const QString &uid, const QString &fullName);

private Q_SLOTS:
    void textChanged(const QString &newText);
    void cancelSearch();
    void userSelected(const QModelIndex &index);

//private:
//    void keyPressEvent(QKeyEvent *event);

private:
    QString m_LastSearch;
    Internal::UserCompleter *m_Completer;
};

}  // End namespace UserPlugin

#endif // USERPLUGINLINEEDITCOMPLETER_H
