/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef USERPLUGIN_USERCREATORWIZARD_H
#define USERPLUGIN_USERCREATORWIZARD_H

#include <usermanagerplugin/usermanager_exporter.h>

#include <QObject>
#include <QWidget>
#include <QWizardPage>
#include <QWizard>
#include <QHash>
#include <QString>

QT_BEGIN_NAMESPACE
class QLabel;
class QEvent;
class QLineEdit;
class QPushButton;
class QCheckBox;
class QComboBox;
class QGroupBox;
class QTreeWidget;
QT_END_NAMESPACE

/**
 * \file ./plugins/usermanagerplugin/widgets/usercreatorwizard.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace UserPlugin {
namespace Internal {
class UserCreatorWizardPrivate;

namespace Ui {
class UserWizardContactWidget;
}  // End namespace Ui
}  // End namespace Internal

class USER_EXPORT UserCreatorWizard : public QWizard
{
    Q_OBJECT
public:
    enum Pages {
        IdentityAndLoginPage = 0,
        ContactPage,
        ProfilPage,
        RightsPage,
        SpecialiesQualificationsPage,
        ExtraPages,
        LastPage = 10000
    };

    UserCreatorWizard(QWidget *parent = 0);
    ~UserCreatorWizard();

    static bool checkUserRights();

    static void setUserPaper(const int ref, const QString &xml);
    static void setUserRights(const int role, const int value);
    static int userRights(const int role);

protected Q_SLOTS:
    void initializePage(int id);
    void done(int r);

private:
    void showEvent(QShowEvent *event);

private:
    Internal::UserCreatorWizardPrivate *d;
};

}  // End namespace UserPlugin


#endif // USERPLUGIN_USERCREATORWIZARD_H
