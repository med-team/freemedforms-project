/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       Christian A. Reiter <christian.a.reiter@gmail.com>>               *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef USERIDENTITYANDLOGINPAGE_H
#define USERIDENTITYANDLOGINPAGE_H

#include <usermanagerplugin/iuserwizardpage.h>

#include <QWidget>

/**
 * \file ./plugins/usermanagerplugin/widgets/useridentityandloginpage.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Identity {
class IdentityEditorWidget;
}

namespace UserPlugin {
namespace Internal {
namespace Ui {
class UserIdentityAndLoginPage;
}

class UserIdentityAndLoginPage: public QWizardPage
{
    Q_OBJECT

public:
    explicit UserIdentityAndLoginPage(QWidget *parent = 0);
    ~UserIdentityAndLoginPage();

    void initializePage();
    bool isComplete() const;
    bool validatePage();

private Q_SLOTS:
    void checkCompleteState();
    void onPasswordConfirmed();
//    void onNamesEditionFinished();

private:
    bool checkLogin() const;
    void changeEvent(QEvent *e);
    QSize sizeHint() const;
    QSize minimumSizeHint() const;
    void retranslate();

private:
    Identity::IdentityEditorWidget *_identity;
    bool _showErrorLabels;
};

} // namespace Internal
} // namespace UserPlugin

#endif // USERIDENTITYANDLOGINPAGE_H
