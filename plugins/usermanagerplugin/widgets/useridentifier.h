/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef USERIDENTIFIER_H
#define USERIDENTIFIER_H

#include <usermanagerplugin/usermanager_exporter.h>

#include <QDialog>

/**
 * \file ./plugins/usermanagerplugin/widgets/useridentifier.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/
namespace UserPlugin {
namespace Internal {
namespace Ui {
class  UserIdentifier;
}  // End Ui

class USER_EXPORT UserIdentifier : public QDialog
{
    Q_OBJECT
    Q_DISABLE_COPY(UserIdentifier)

    enum { MaxNumberOfTries = 4 };

public:
    explicit UserIdentifier(QWidget *parent = 0 );

    QString login() const;
    QString login64crypt() const;

    QString password() const;
    QString cryptedPassword() const;

private:
    void done(int result);

private:
    Ui::UserIdentifier *m_ui;
    int m_NumberOfTries;
};

}  // End Internal
}  // End namespace UserPlugin

#endif // USERIDENTIFIER_H
