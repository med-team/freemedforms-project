/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef USERVIEWER_H
#define USERVIEWER_H

#include <usermanagerplugin/usermanager_exporter.h>
#include <coreplugin/icorelistener.h>

#include <QWidget>
#include <QObject>

/**
 * \file ./plugins/usermanagerplugin/widgets/userviewer.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace UserPlugin {
class UserViewer;
class IUserViewerPage;

namespace Internal {
class UserViewerPrivate;
class UserManagerModel;

class UserViewerModelCoreListener : public Core::ICoreListener
{
    Q_OBJECT
public:
    UserViewerModelCoreListener(UserPlugin::UserViewer *parent);
    ~UserViewerModelCoreListener();

    bool coreAboutToClose();

private:
    UserPlugin::UserViewer *_viewer;
};

}  // End Internal

class USER_EXPORT UserViewer : public QWidget
{
    friend class UserPlugin::Internal::UserViewerModelCoreListener;
    Q_OBJECT
    Q_DISABLE_COPY(UserViewer)
public:
    explicit UserViewer(QWidget *parent = 0);
    ~UserViewer();

    bool initialize(Internal::UserManagerModel *model);

    void submitChangesToModel();

public Q_SLOTS:
    void setCurrentUser(const QString &userUid);
    void setCurrentPage(int index);

private:
    Internal::UserViewerPrivate *d;
};

}  // End UserPlugin

#endif // USERVIEWER_H
