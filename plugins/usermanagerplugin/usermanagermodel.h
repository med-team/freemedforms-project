/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker                                         *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef USERPLUGIN_INTERNAL_USERMANAGERMODEL_H
#define USERPLUGIN_INTERNAL_USERMANAGERMODEL_H

#include <QStandardItemModel>

/**
 * \file ./plugins/usermanagerplugin/usermanagermodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace UserPlugin {
class IUserViewerPage;

namespace Internal {
class UserManagerModelFilter {
public:
    UserManagerModelFilter(const QString &usualName = QString::null, const QString &firstName = QString::null) :
        _usual(usualName), _first(firstName)
    {}

    QString _usual, _first;
};

class UserManagerModelPrivate;

class UserManagerModel : public QStandardItemModel
{
    Q_OBJECT

public:
    explicit UserManagerModel(QObject *parent = 0);
    ~UserManagerModel();
    bool initialize();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;

    void setFilter(const UserManagerModelFilter &filter);

    int pageIndexFromIndex(const QModelIndex &index) const;
    QList<IUserViewerPage *> pages() const;
    QString userUuid(const QModelIndex &index) const;
    int genderIndex(const QModelIndex &index) const;
    QString lastLoggedIn(const QModelIndex &index) const;
    QString title(const QModelIndex &index) const;

Q_SIGNALS:

//private Q_SLOTS:
//    void pluginManagerObjectAdded(QObject *o);
//    void pluginManagerObjectRemoved(QObject *o);

private:
    Internal::UserManagerModelPrivate *d;
};

} // namespace Internal
} // namespace UserPlugin

#endif // USERPLUGIN_INTERNAL_USERMANAGERMODEL_H

