/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
/*!
 * \class UserPlugin::UserCore
 * Central place for the UserPlugin.
 */

#include "usercore.h"
#include <usermanagerplugin/database/userbase.h>
#include <usermanagerplugin/usermodel.h>
#include <usermanagerplugin/coreusermodelwrapper.h>

#include <coreplugin/icore.h>
#include <coreplugin/translators.h>
#include <coreplugin/isettings.h>
#include <coreplugin/iuser.h>
#include <coreplugin/constants_tokensandsettings.h>

#include <translationutils/constants.h>

#include <QDebug>

using namespace UserPlugin;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Core::ISettings *settings() {return Core::ICore::instance()->settings();}
static inline Core::IUser *user() {return Core::ICore::instance()->user();}

namespace UserPlugin {
namespace Internal {
class UserCorePrivate
{
public:
    UserCorePrivate(UserCore */*parent*/) :
        _base(0),
        _model(0),
        _coreUserModelWrapper(0)  // , q(parent)
    {
    }

    ~UserCorePrivate()
    {
    }

public:
    UserBase *_base;
    UserModel *_model;
    CoreUserModelWrapper *_coreUserModelWrapper;

private:
};
} // namespace Internal
} // end namespace UserPlugin

UserCore *UserCore::_instance = 0;


UserPlugin::UserCore &UserPlugin::UserCore::instance() // static
{
    Q_ASSERT(_instance);
    return *_instance;
}

/*! Constructor of UserPlugin::UserCore class */
UserCore::UserCore(QObject *parent) :
    QObject(parent),
    d(new UserCorePrivate(this))
{
    _instance = this;
    d->_base = new UserBase(this);
}

/*! Destructor of UserPlugin::UserCore class */
UserCore::~UserCore()
{
    _instance = 0;
    if (d)
        delete d;
    d = 0;
}

/*!
 * Initializes the object with default values.
 * Return true if initialization was completed.
 */
bool UserCore::initialize()
{
    if (d->_model)
        return true;

    if (!d->_base->initialize())
        return false;

    d->_model = new UserModel(this);
    d->_model->initialize();
    d->_coreUserModelWrapper = new CoreUserModelWrapper(this);
    d->_coreUserModelWrapper->initialize(d->_model);
    Core::ICore::instance()->setUser(d->_coreUserModelWrapper);
    connect(settings(), SIGNAL(userSettingsSynchronized()), d->_model, SLOT(updateUserPreferences()), Qt::UniqueConnection);

    return true;
}

/*!
 * Return true if core initialization is completed.
 */
bool UserCore::isInitialized() const
{
    return (d->_model!=0)
            && (d->_coreUserModelWrapper!=0)
            && (Core::ICore::instance()->user()!=0)
            && d->_base->isInitialized()
            ;
}


bool UserCore::forceReInitialization()
{
    delete d->_model;
    d->_model = 0;
    delete d->_coreUserModelWrapper;
    d->_coreUserModelWrapper = 0;
    Core::ICore::instance()->setUser(0);
    d->_base->onCoreDatabaseServerChanged(); // force database re-initialization
    return initialize();
}


bool UserCore::postCoreInitialization()
{
    if (!user())
        return false;
    d->_model->checkUserPreferencesValidity();
    d->_model->emitUserConnected();
    Core::ICore::instance()->translators()->changeLanguage(settings()->value(Core::Constants::S_PREFERREDLANGUAGE, user()->value(Core::IUser::LanguageISO).toString()).toString());
    return true;
}


Internal::UserBase *UserCore::userBase() const
{
    return d->_base;
}


UserModel *UserCore::userModel() const
{
    return d->_model;
}
