/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IUSERWIZARDPAGE_H
#define IUSERWIZARDPAGE_H

#include <usermanagerplugin/usermanager_exporter.h>
#include <coreplugin/igenericpage.h>

#include <QWizardPage>

/**
 * \file ./plugins/usermanagerplugin/iuserwizardpage.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
 */

namespace UserPlugin {
class UserModel;

class USER_EXPORT IUserWizardPage : public Core::IGenericPage
{
    Q_OBJECT
public:
    /** IUserWizardPage is an extension of the user creator QWizard. \sa UserPlugin::UserWizard. */
    explicit IUserWizardPage(QObject *parent = 0) : Core::IGenericPage(parent) {}
    virtual ~IUserWizardPage() {}

    /** Create the QWizardPage to include in the user creator QWizard. */
    virtual QWizardPage *createWizardPage(QWidget *parent) = 0;

    /** When the user creator QWizard is validated, this member is called to allow extra page
     * to submit their data to their own database. */
    virtual void submit(const QString &userUid) = 0;
};

}  // End namespace UserPlugin


#endif // IUSERWIZARDPAGE_H
