/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "currentuserpreferencespage.h"
#include <usermanagerplugin/usercore.h>
#include <usermanagerplugin/usermodel.h>
#include <usermanagerplugin/constants.h>
#include <usermanagerplugin/widgets/userviewer.h>

#include <utils/log.h>
#include <utils/global.h>
#include <translationutils/constanttranslations.h>

#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/constants_menus.h>

using namespace UserPlugin;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Core::ISettings *settings() { return Core::ICore::instance()->settings(); }
static inline UserPlugin::UserCore &userCore() {return UserPlugin::UserCore::instance();}
static inline UserPlugin::UserModel *userModel() {return userCore().userModel();}

CurrentUserPreferencesPage::CurrentUserPreferencesPage(QObject *parent) :
        IOptionsPage(parent), m_Widget(0) { setObjectName("CurrentUserPreferencesPage"); }

CurrentUserPreferencesPage::~CurrentUserPreferencesPage()
{
    if (m_Widget) delete m_Widget;
    m_Widget = 0;
}

QString CurrentUserPreferencesPage::id() const { return objectName(); }
QString CurrentUserPreferencesPage::displayName() const { return tkTr(Trans::Constants::USER); }
QString CurrentUserPreferencesPage::category() const { return tkTr(Trans::Constants::GENERAL_PREFERENCES); }
QString CurrentUserPreferencesPage::title() const { return tkTr(Trans::Constants::USER_PREFERENCES); }

int CurrentUserPreferencesPage::sortIndex() const
{
    return Core::Constants::OPTIONINDEX_MAIN;
}

void CurrentUserPreferencesPage::resetToDefaults()
{
    m_Widget->writeDefaultSettings(settings());
    m_Widget->setDataToUi();
}

void CurrentUserPreferencesPage::apply()
{
    if (!m_Widget) {
        return;
    }
    m_Widget->saveToSettings(settings());
}

void CurrentUserPreferencesPage::finish() { delete m_Widget; }

void CurrentUserPreferencesPage::checkSettingsValidity()
{
}

QWidget *CurrentUserPreferencesPage::createPage(QWidget *parent)
{
    if (m_Widget)
        delete m_Widget;
    m_Widget = new CurrentUserPreferencesWidget(parent);
    return m_Widget;
}

CurrentUserPreferencesWidget::CurrentUserPreferencesWidget(QWidget *parent) :
        QWidget(parent), m_Viewer(0)
{
    setupUi(this);
    m_Viewer = new UserViewer(this);
    userLayout->addWidget(m_Viewer);
    setDataToUi();
}

void CurrentUserPreferencesWidget::setDataToUi()
{
}

void CurrentUserPreferencesWidget::saveToSettings(Core::ISettings *)
{
    if (userModel()->hasCurrentUser()) {
        this->setFocus();
        m_Viewer->submitChangesToModel();
        userModel()->submitRow(userModel()->currentUserIndex().row());
    }
}

void CurrentUserPreferencesWidget::writeDefaultSettings(Core::ISettings *)
{
}

void CurrentUserPreferencesWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        retranslateUi(this);
        break;
    default:
        break;
    }
}
