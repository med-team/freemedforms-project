/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef USERPLUGIN_INTERNAL_COREUSERMODELWRAPPER_H
#define USERPLUGIN_INTERNAL_COREUSERMODELWRAPPER_H

#include <coreplugin/iuser.h>

/**
 * \file ./plugins/usermanagerplugin/coreusermodelwrapper.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace UserPlugin {
class UserModel;
namespace Internal {
class CoreUserModelWrapperPrivate;

class CoreUserModelWrapper : public Core::IUser
{
    Q_OBJECT
public:
    explicit CoreUserModelWrapper(QObject *parent = 0);
    ~CoreUserModelWrapper();
    bool initialize(UserModel *model);

    // IUser interface
    void clear() {}
    bool has(const int ref) const;
    bool hasCurrentUser() const;

    QVariant value(const int ref) const;
    bool setValue(const int ref, const QVariant &value);

    QString toXml() const {return QString();}
    bool fromXml(const QString &) {return true;}

    bool saveChanges();

    QString fullNameOfUser(const QVariant &uid);

private Q_SLOTS:
    void newUserConnected(const QString &uid);

private:
    Internal::CoreUserModelWrapperPrivate *d;
};

} // namespace Internal
} // namespace UserPlugin

#endif // USERPLUGIN_INTERNAL_COREUSERMODELWRAPPER_H

