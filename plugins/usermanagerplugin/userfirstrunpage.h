/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef USERFIRSTRUNPAGE_H
#define USERFIRSTRUNPAGE_H

#include <coreplugin/ifirstconfigurationpage.h>

#include <QWizardPage>

namespace UserPlugin {
class UserManagerDialog;
class UserCreatorWizard;

namespace Ui {
    class FirstRunUserCreationWidget;
}

class UserCreationPage : public QWizardPage
{
    Q_OBJECT

public:
    explicit UserCreationPage(QWidget *parent = 0);
    ~UserCreationPage();

    void initializePage();
    bool validatePage();

protected:
    void retranslate();
    void changeEvent(QEvent *e);

private Q_SLOTS:
    void userManager();
    void userWizard();

private:
    Ui::FirstRunUserCreationWidget *ui;
    UserManagerDialog *_userManagerDialog;
    UserCreatorWizard *_userWizard;
};

class FirstRun_UserCreation : public Core::IFirstConfigurationPage
{
public:
    FirstRun_UserCreation(QObject *parent = 0) : Core::IFirstConfigurationPage(parent) {}
    ~FirstRun_UserCreation() {}
    int id() const {return Core::IFirstConfigurationPage::UserCreation;}
    QWizardPage *createPage(QWidget *parent) {return new UserPlugin::UserCreationPage(parent);}
};

}  // End namespace UserPlugin

#endif // USERFIRSTRUNPAGE_H
