/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CURRENTUSERPREFERENCESPAGE_H
#define CURRENTUSERPREFERENCESPAGE_H

#include <coreplugin/ioptionspage.h>
#include "ui_currentuserpreferenceswidget.h"

#include <QPointer>
#include <QString>

/**
 * \file ./plugins/usermanagerplugin/currentuserpreferencespage.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;
}

namespace UserPlugin {
class UserViewer;

namespace Internal {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////  CurrentUserPreferencesWidget  ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

class CurrentUserPreferencesWidget : public QWidget, private Ui::CurrentUserPreferencesWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(CurrentUserPreferencesWidget)

public:
    explicit CurrentUserPreferencesWidget(QWidget *parent = 0);
    void setDataToUi();

    static void writeDefaultSettings( Core::ISettings *s );

public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);

protected:
    virtual void changeEvent(QEvent *e);

private:
    UserPlugin::UserViewer *m_Viewer;
};

}  // End Internal


class CurrentUserPreferencesPage : public Core::IOptionsPage
{
public:
    CurrentUserPreferencesPage(QObject *parent = 0);
    ~CurrentUserPreferencesPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    QString helpPage() {return QString();}

    static void writeDefaultSettings(Core::ISettings *s) {Internal::CurrentUserPreferencesWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);

private:
    QPointer<Internal::CurrentUserPreferencesWidget> m_Widget;
};

}  // End namespace UserPlugin

#endif // CURRENTUSERPREFERENCESPAGE_H
