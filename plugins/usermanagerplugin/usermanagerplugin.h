/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef USERPLUGIN_INTERNAL_USERMANAGERPLUGIN_H
#define USERPLUGIN_INTERNAL_USERMANAGERPLUGIN_H

#include <extensionsystem/iplugin.h>

#include <QtCore/QObject>
#include <QPointer>
class QAction;

/**
 * \file ./plugins/usermanagerplugin/usermanagerplugin.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace UserPlugin {
class UserManagerDialog;
class FirstRun_UserConnection;
class FirstRun_UserCreation;
class UserManagerMainWindow;
namespace Internal {
class UserManagerMode;

class UserManagerPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.freemedforms.FreeMedForms.UserManagerPlugin" FILE "UserManager.json")

public:
    UserManagerPlugin();
    ~UserManagerPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

private:
    bool identifyUser();

private Q_SLOTS:
    void postCoreInitialization();
    void createUser();
    void changeCurrentUser();
    void updateActions();
    void showUserManager();
    void showDatabaseInformation();

#ifdef WITH_TESTS
    void initTestCase();
    void test_userdynamicdata_basics();
    void test_userdata_basics();
    void test_usercore_initialization();
    void test_userbase_test_with_virtualuser();
    void test_userbase_basics();
    void test_usermodel_basics();
    void cleanupTestCase();
#endif

private:
    QAction *aCreateUser;
    QAction *aChangeUser;
    QAction *aUserManager;
    QAction *aAboutDatabase;

    FirstRun_UserCreation *m_FirstCreation;
    Internal::UserManagerMode *m_Mode;
    UserManagerMainWindow *m_UserManagerMainWin;
};

} // namespace Internal
} // namespace UserPlugin

#endif  // USERPLUGIN_INTERNAL_USERMANAGERPLUGIN_H
