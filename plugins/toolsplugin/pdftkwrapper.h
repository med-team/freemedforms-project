/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_INTERNAL_PDFTKWRAPPER_H
#define TOOLS_INTERNAL_PDFTKWRAPPER_H

#include <QProcess>
#include <QObject>

/**
 * \file ./plugins/toolsplugin/pdftkwrapper.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Tools {
namespace Internal {
class ToolsPlugin;
class PdfTkWrapperPrivate;

class PdfTkWrapper : public QObject
{
    friend class Tools::Internal::ToolsPlugin;
    Q_OBJECT
    Q_PROPERTY(bool isAvailable READ isAvailable())

protected:
    explicit PdfTkWrapper(QObject *parent = 0);

public:
    static PdfTkWrapper *instance();
    ~PdfTkWrapper();

    bool initialize();

Q_SIGNALS:

public Q_SLOTS:
    // Checker
    bool isAvailable() const;

    // FDF creation helpers
    void beginFdfEncoding();
    void addFdfValue(const QString &fieldName, const QString &value, bool toUpper = true);
//    void addFdfValueFromFormItem(const QString &fieldName, const QString &itemUuid, bool toUpper = true);
    void endFdfEncoding(const QString &filename);
    QString getFdfContent();

    // PDF creation
    bool fillPdfWithFdf(const QString &absPdfFile, const QString &fdfContent, const QString &absFileNameOut, const QString &isoEncoding);

private Q_SLOTS:
    void onProcessError(QProcess::ProcessError);
    void onProcessFinished(int exitCode);

private:
    Internal::PdfTkWrapperPrivate *d;
    static PdfTkWrapper *_instance;
};

} // namespace Internal
} // namespace Tools

#endif // TOOLS_INTERNAL_PDFTKWRAPPER_H

