/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric Maeker <eric.maeker@gmail.com>                             *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_INTERNAL_TOOLSPREFERENCES_H
#define TOOLS_INTERNAL_TOOLSPREFERENCES_H

#include <coreplugin/ioptionspage.h>

#include <QWidget>
#include <QPointer>

namespace Core {
class ISettings;
}

namespace Tools {
namespace Internal {
namespace Ui {
class ToolsPreferencesWidget;
}

class ToolsPreferencesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ToolsPreferencesWidget(QWidget *parent = 0);
    ~ToolsPreferencesWidget();

    void setDataToUi();
    QString searchKeywords() const;

    static void writeDefaultSettings(Core::ISettings *s);

public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);

private:
    void retranslateUi();
    void changeEvent(QEvent *e);

private:
    Ui::ToolsPreferencesWidget *ui;
};


class ToolsPreferencesPage : public Core::IOptionsPage
{
    Q_OBJECT

public:
    ToolsPreferencesPage(QObject *parent = 0);
    ~ToolsPreferencesPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const {return displayName();}
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    QString helpPage() {return QString();}

    static void writeDefaultSettings(Core::ISettings *s) {ToolsPreferencesWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);

private:
    QPointer<Internal::ToolsPreferencesWidget> m_Widget;
};


} // namespace Internal
} // namespace Tools
#endif // TOOLS_INTERNAL_TOOLSPREFERENCES_H

