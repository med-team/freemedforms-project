/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Eric Maeker <eric.maeker@gmail.com>                  *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_IPLUGIN_H
#define TOOLS_IPLUGIN_H

#include "tools_exporter.h"
#include "toolspreferences.h"

#include <extensionsystem/iplugin.h>

namespace Tools {
namespace Internal {
class PdfTkWrapper;
class FspPrinterPreferencesPage;
class ChequePrinterPreferencesPage;
class HprimPreferencesPage;

class ToolsPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.freemedforms.FreeMedForms.ToolsPlugin" FILE "Tools.json")

public:
    ToolsPlugin();
    ~ToolsPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

private Q_SLOTS:
    void postCoreInitialization();
    void printCheque();
    void printFsp();

private:
    ToolsPreferencesPage *m_prefPage;
    PdfTkWrapper *pdf;
    FspPrinterPreferencesPage *m_FspPage;
    ChequePrinterPreferencesPage *m_ChequePage;
    HprimPreferencesPage *m_HprimPage;
};

} // namespace Internal
} // namespace Tools

#endif // TOOLS_IPLUGIN_H

