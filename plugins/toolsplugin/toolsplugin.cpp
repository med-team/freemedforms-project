/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Eric Maeker <eric.maeker@gmail.com>                  *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "toolsplugin.h"
#include "toolsconstants.h"
#include "pdftkwrapper.h"

#ifdef WITH_CHEQUE_PRINTING
#include "cheque/chequeprinterdialog.h"
#include "cheque/chequeprinter_preferences.h"
#endif

#ifdef WITH_FRENCH_FSP
#include "fsp/fspprinterpreferences.h"
#include "fsp/fspprinterdialog.h"
#endif

#ifdef WITH_FRENCH_HPRIM_INTEGRATOR
#include "hprimintegrator/hprimpreferences.h"
#include "hprimintegrator/hprimintegrator.h"
#include "hprimintegrator/constants.h"
#endif

#include <coreplugin/icore.h>
#include <coreplugin/iuser.h>
#include <coreplugin/itheme.h>
#include <coreplugin/ipatient.h>
#include <coreplugin/isettings.h>
#include <coreplugin/translators.h>
#include <coreplugin/iscriptmanager.h>
#include <coreplugin/constants_icons.h>
#include <coreplugin/constants_menus.h>
#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/contextmanager/contextmanager.h>

#include <utils/log.h>
#include <utils/global.h>
#include <extensionsystem/pluginmanager.h>

#include <QtPlugin>
#include <QAction>
#include <QDebug>

using namespace Tools;
using namespace Internal;

static inline Core::IScriptManager *scriptManager()  { return Core::ICore::instance()->scriptManager(); }
static inline Core::IUser *user()  { return Core::ICore::instance()->user(); }
static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline Core::ActionManager *actionManager()  { return Core::ICore::instance()->actionManager(); }
static inline Core::ISettings *settings()  { return Core::ICore::instance()->settings(); }
static inline ExtensionSystem::PluginManager *pluginManager() {return ExtensionSystem::PluginManager::instance();}
static inline void messageSplash(const QString &s) {theme()->messageSplashScreen(s); }

namespace {
const char* const PRINT_CHEQUE = QT_TRANSLATE_NOOP("Tools", "Print a cheque");
const char* const PRINT_FSP    = QT_TRANSLATE_NOOP("Tools", "Print a french 'FSP'");
}

ToolsPlugin::ToolsPlugin() :
    ExtensionSystem::IPlugin(),
    m_prefPage(0),
    pdf(0),
    m_FspPage(0),
    m_ChequePage(0),
    m_HprimPage(0)
{
    setObjectName("ToolsPlugin");
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating Tools";

    Core::ICore::instance()->translators()->addNewTranslator("plugin_tools");


#ifdef WITH_CHEQUE_PRINTING
    addAutoReleasedObject(m_ChequePage = new ChequePrinterPreferencesPage(this));
#endif

#ifdef WITH_FRENCH_FSP
    addAutoReleasedObject(m_FspPage = new FspPrinterPreferencesPage(this));
#endif

#ifdef WITH_FRENCH_HPRIM_INTEGRATOR
    addAutoReleasedObject(m_HprimPage = new HprimPreferencesPage(this));
#endif

    connect(Core::ICore::instance(), SIGNAL(coreOpened()), this, SLOT(postCoreInitialization()));
}

ToolsPlugin::~ToolsPlugin()
{
}

bool ToolsPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    Q_UNUSED(arguments)
    Q_UNUSED(errorString)

    if (Utils::Log::debugPluginsCreation()) {
        qWarning() << "Tools::initialize";
    }




    return true;
}

void ToolsPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation()) {
        qWarning() << "Tools::extensionsInitialized";
    }


    if (!user())
        return;
    if (user()->uuid().isEmpty())
        return;

    messageSplash(tr("Initializing Tools..."));


#if defined(WITH_CHEQUE_PRINTING) || defined(WITH_FRENCH_FSP)
    Core::ActionContainer *menu = actionManager()->createMenu(Core::Constants::M_GENERAL);
    QAction *action = 0;
    Core::Command *cmd = 0;
#endif

#ifdef WITH_CHEQUE_PRINTING
    action = new QAction(this);
    action->setEnabled(ChequePrinterDialog::isAvailable());
    action->setIcon(theme()->icon(Core::Constants::ICONCHEQUE));
    cmd = actionManager()->registerAction(action, "aTools.PrintCheque", Core::Context(Core::Constants::C_GLOBAL));
    cmd->setTranslations(::PRINT_CHEQUE, ::PRINT_CHEQUE, "Tools");
    cmd->setDefaultKeySequence(QKeySequence(tr("Ctrl+Shift+C")));
    connect(action, SIGNAL(triggered()), this, SLOT(printCheque()));
    menu->addAction(cmd, Core::Id(Core::Constants::G_GENERAL_PRINT));

    if (m_ChequePage)
        m_ChequePage->checkSettingsValidity();
#endif

#ifdef WITH_FRENCH_FSP
    action = new QAction(this);
    action->setEnabled(FspPrinterDialog::isAvailable());
    action->setIcon(theme()->icon(Core::Constants::ICONCHEQUE));
    cmd = actionManager()->registerAction(action, "aTools.PrintFsp", Core::Context(Core::Constants::C_GLOBAL));
    cmd->setTranslations(::PRINT_FSP, ::PRINT_FSP, "Tools");
    cmd->setDefaultKeySequence(QKeySequence(tr("Ctrl+Shift+F")));
    connect(action, SIGNAL(triggered()), this, SLOT(printFsp()));
    menu->addAction(cmd, Core::Id(Core::Constants::G_GENERAL_PRINT));

    if (m_FspPage)
        m_FspPage->checkSettingsValidity();
#endif


    pdf = new PdfTkWrapper(this);
    pdf->initialize();
    QScriptValue pdfValue = scriptManager()->addScriptObject(pdf);
    scriptManager()->evaluate("namespace.com.freemedforms").setProperty("pdf", pdfValue);

    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));
}

void ToolsPlugin::postCoreInitialization()
{
#ifdef WITH_FRENCH_HPRIM_INTEGRATOR
    bool enabled = settings()->value(Constants::S_ACTIVATION).toInt() == Constants::Enabled;
    enabled = enabled || (settings()->value(Constants::S_ACTIVATION).toInt() == Constants::OnlyForFrance
            && QLocale().country() == QLocale::France);
    if (enabled) {
        HprimIntegratorMode *mode = new HprimIntegratorMode(this);
        addObject(mode);
    }

    if (m_HprimPage)
        m_HprimPage->checkSettingsValidity();
#endif
}

ExtensionSystem::IPlugin::ShutdownFlag ToolsPlugin::aboutToShutdown()
{

#ifdef WITH_FRENCH_HPRIM_INTEGRATOR
    HprimIntegratorMode *mode = pluginManager()->getObject<HprimIntegratorMode>();
    qWarning() << mode;
    if (mode) {
        removeObject(mode);
        delete mode;
        mode = 0;
    }
#endif

    return SynchronousShutdown;
}

void ToolsPlugin::printCheque()
{
#ifdef WITH_CHEQUE_PRINTING
    ChequePrinterDialog printDialog;
    printDialog.initializeWithSettings();
    printDialog.exec();
#endif
}

void ToolsPlugin::printFsp()
{
#ifdef WITH_FRENCH_FSP

    FspPrinterDialog dlg;
    dlg.exec();






#endif
}

Q_EXPORT_PLUGIN(ToolsPlugin)

