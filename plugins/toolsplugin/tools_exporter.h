/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Eric Maeker <eric.maeker@gmail.com>                  *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_EXPORTER_H
#define TOOLS_EXPORTER_H

#include <QtCore/QtGlobal>

#if defined(TOOLS_LIBRARY)
#  define TOOLS_EXPORT Q_DECL_EXPORT
#else
#  define TOOLS_EXPORT Q_DECL_IMPORT
#endif

#endif // TOOLS_EXPORTER_H

