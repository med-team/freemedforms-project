/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_INTERNAL_CHEQUEPRINTFORMATMODEL_H
#define TOOLS_INTERNAL_CHEQUEPRINTFORMATMODEL_H

#include <QStandardItemModel>

/**
 * \file ./plugins/toolsplugin/cheque/chequeprintformatmodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Tools {
namespace Internal {
class ChequePrintFormat;
class ChequePrintFormatModelPrivate;

class ChequePrintFormatModel : public QStandardItemModel
{
    Q_OBJECT    
public:
    explicit ChequePrintFormatModel(QObject *parent = 0);
    ~ChequePrintFormatModel();
    bool initialize();
    
    Qt::ItemFlags flags(const QModelIndex &) const;

    const ChequePrintFormat &chequePrintFormat(const QModelIndex &index) const;

private:
    ChequePrintFormatModelPrivate *d;
};

} // namespace Internal
} // namespace Tools

#endif // TOOLS_INTERNAL_CHEQUEPRINTFORMATMODEL_H

