/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_CHEQUEPRINTERDIALOG_H
#define TOOLS_CHEQUEPRINTERDIALOG_H

#include <toolsplugin/tools_exporter.h>
#include <QDialog>
#include <QDate>

/**
 * \file ./plugins/toolsplugin/cheque/chequeprinterdialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
 * \note requires Qt 4.8+
*/

namespace Tools {
namespace Internal {
class ChequePrintFormatModel;
}

namespace Ui {
class ChequePrinterDialog;
}

class TOOLS_EXPORT ChequePrinterDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ChequePrinterDialog(QWidget *parent = 0);
    ~ChequePrinterDialog();
    static bool isAvailable();
    static QString datapackPath();

    void initializeWithSettings();

    void setOrder(const QString &order);
    void setPlace(const QString &place);
    void setDate(const QDate &date);
    void setAmount(double amount);
    void setDefaultAmounts(const QStringList &values);

private:
    void done(int result);

private Q_SLOTS:
    bool printCheque();
    void previewCheque();

private:
    Ui::ChequePrinterDialog *ui;
    Internal::ChequePrintFormatModel *_printFormatModel;
};

} // namespace Tools

#endif // TOOLS_CHEQUEPRINTERDIALOG_H
