/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric Maeker <>                             *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_INTERNAL_CHEQUEPRINTER_PREF_H
#define TOOLS_INTERNAL_CHEQUEPRINTER_PREF_H

#include <coreplugin/ioptionspage.h>

#include <QWidget>
#include <QPointer>
#include <QModelIndex>

/**
 * \file ./plugins/toolsplugin/cheque/chequeprinter_preferences.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;
}

namespace Tools {
namespace Internal {
class ChequePrintFormatModel;

namespace Ui {
class ChequePrinterPreferencesWidget;
}

class ChequePrinterPreferencesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ChequePrinterPreferencesWidget(QWidget *parent = 0);
    ~ChequePrinterPreferencesWidget();

    void setDataToUi();
    QString searchKeywords() const;

    static void writeDefaultSettings(Core::ISettings *s);

public Q_SLOTS:
    void onChequeFormatActivated(const QModelIndex &index);
    bool onPrintTestClicked();
    void saveToSettings(Core::ISettings *s = 0);

private:
    void retranslateUi();
    void changeEvent(QEvent *e);

private:
    ChequePrintFormatModel *_model;
    Ui::ChequePrinterPreferencesWidget *ui;
};


class ChequePrinterPreferencesPage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    ChequePrinterPreferencesPage(QObject *parent = 0);
    ~ChequePrinterPreferencesPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    bool matches(const QString &s) const;

    QString helpPage() {return QString();}

    static void writeDefaultSettings(Core::ISettings *s) {ChequePrinterPreferencesWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);

private:
    QPointer<Internal::ChequePrinterPreferencesWidget> m_Widget;
    QString m_searchKeywords;
};

} // namespace Internal
} // namespace Tools

#endif // TOOLS_INTERNAL_CHEQUEPRINTER_PREF_H
