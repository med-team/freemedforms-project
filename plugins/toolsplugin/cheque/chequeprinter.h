/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
<<<<<<< HEAD:plugins/toolsplugin/cheque/chequeprinter.h
 *   Main developers : Eric Maeker                                         *
=======
 *   Main developers : Eric Maeker
>>>>>>> 05812a06306f1d862154c30b64da662ece672ed4:plugins/toolsplugin/chequeprinter.h
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_CHEQUEPRINTER_H
#define TOOLS_CHEQUEPRINTER_H

#include <toolsplugin/tools_exporter.h>
#include <QPixmap>
#include <QDate>

/**
 * \file ./plugins/toolsplugin/cheque/chequeprinter.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
 * \note requires Qt 4.8+
*/

namespace Tools {
namespace Internal {
class ChequePrintFormat;
class ChequePrinterPrivate;
} // namespace Internal

class TOOLS_EXPORT ChequePrinter
{
public:
    explicit ChequePrinter();
    ~ChequePrinter();
    bool initialize();
    
    void setDrawRects(bool drawRects);
    void setOrder(const QString &order);
    void setPlace(const QString &place);
    void setDate(const QDate &date);
    void setAmount(double amount);

    bool print(const Internal::ChequePrintFormat &format);
    QPixmap preview(const Internal::ChequePrintFormat &format);

private:
    Internal::ChequePrinterPrivate *d;
};

} // namespace Tools

#endif  // TOOLS_CHEQUEPRINTER_H

