/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Eric Maeker <eric.maeker@gmail.com>                  *
 *   Contributors:                                                         *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_CHEQUECONSTANTS_H
#define TOOLS_CHEQUECONSTANTS_H

/**
 * \file ./plugins/toolsplugin/cheque/chequeconstants.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Tools {
namespace Constants {

const char * const S_ORDER = "Tools/ChequePrinter/Order";
const char * const S_PLACE = "Tools/ChequePrinter/Place";
const char * const S_VALUES = "Tools/ChequePrinter/Values";

// Datapack path
const char * const DATAPACK_PATH = "/cheques/";

}
}

#endif // TOOLS_CHEQUECONSTANTS_H
