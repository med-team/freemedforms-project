/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker <eric.maeker@gmail.com>                 *
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_INTERNAL_FSPTEMPLATEMODEL_H
#define TOOLS_INTERNAL_FSPTEMPLATEMODEL_H

#include <QStandardItemModel>

/**
 * \file ./plugins/toolsplugin/fsp/fsptemplatemodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Tools {
namespace Internal {
class Fsp;
class FspTemplateModelPrivate;

class FspTemplateModel : public QStandardItemModel
{
    Q_OBJECT
    
public:
    explicit FspTemplateModel(QObject *parent = 0);
    ~FspTemplateModel();
    
    bool initialize();
    
    Qt::ItemFlags flags(const QModelIndex &index) const;

    const Fsp &fsp(const QModelIndex &index) const;

Q_SIGNALS:
    
public Q_SLOTS:
    
private:
    FspTemplateModelPrivate *d;
};

} // namespace Internal
} // namespace Tools

#endif // TOOLS_INTERNAL_FSPTEMPLATEMODEL_H

