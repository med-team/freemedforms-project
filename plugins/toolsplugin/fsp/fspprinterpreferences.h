/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric Maeker <eric.maeker@gmail.com>                               *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_INTERNAL_FSPPRINTER_PREFERENCES_H
#define TOOLS_INTERNAL_FSPPRINTER_PREFERENCES_H

#include <coreplugin/ioptionspage.h>

#include <QWidget>
#include <QPointer>

/**
 * \file ./plugins/toolsplugin/fsp/fspprinterpreferences.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;
}

namespace Tools {
namespace Internal {
namespace Ui {
class FspPrinterPreferencesWidget;
} // namespace Ui

class FspPrinterPreferencesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit FspPrinterPreferencesWidget(QWidget *parent = 0);
    ~FspPrinterPreferencesWidget();

    void setDataToUi();
    QString searchKeywords() const;

    static void writeDefaultSettings(Core::ISettings *s);

public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);
    void printTest();
    void viewCerfa();

private:
    void retranslateUi();
    void changeEvent(QEvent *e);

private:
    Ui::FspPrinterPreferencesWidget *ui;
    QPixmap _background;
    double _xcoef, _ycoef;
};

class FspPrinterPreferencesPage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    FspPrinterPreferencesPage(QObject *parent = 0);
    ~FspPrinterPreferencesPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    bool matches(const QString &s) const;

    QString helpPage() {return QString();}

    static void writeDefaultSettings(Core::ISettings *s) {FspPrinterPreferencesWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);

private:
    QPointer<Internal::FspPrinterPreferencesWidget> m_Widget;
    QString m_searchKeywords;
};

} // namespace Internal
} // namespace Tools

#endif // TOOLS_INTERNAL_FSPPRINTER_PREFERENCES_H

