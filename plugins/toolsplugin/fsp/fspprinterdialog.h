/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_INTERNAL_FSPPRINTERDIALOG_H
#define TOOLS_INTERNAL_FSPPRINTERDIALOG_H

#include <QDialog>
QT_BEGIN_NAMESPACE
class QModelIndex;
QT_END_NAMESPACE

/**
 * \file ./plugins/toolsplugin/fsp/fspprinterdialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Tools {
namespace Internal {
class FspPrinterDialogPrivate;
class Fsp;

class FspPrinterDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FspPrinterDialog(QWidget *parent = 0);
    ~FspPrinterDialog();
    bool initialize(const Fsp &fsp);

    static bool isAvailable();
    static QString datapackPath();

private Q_SLOTS:
    void toggleView(bool complex);
    void expandChildren(const QModelIndex &index);
    void useTemplate(const QModelIndex &index);
    void printFsp();
    void previewFsp();
    void printCheque();
    void updatePreview();

private:
    FspPrinterDialogPrivate *d;
};

} // namespace Internal
} // namespace Tools

#endif // TOOLS_INTERNAL_FSPPRINTERDIALOG_H

