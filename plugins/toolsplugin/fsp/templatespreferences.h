/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developpers:                                                     *
 *       Eric Maeker <e>                             *
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_INTERNAL_FSPPRINTER_PREFERENCES_H
#define TOOLS_INTERNAL_FSPPRINTER_PREFERENCES_H

#include <coreplugin/ioptionspage.h>

#include <QWidget>
#include <QPointer>

/**
 * \file ./plugins/toolsplugin/fsp/templatespreferences.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date
*/

namespace Core {
class ISettings;
}

namespace Templates {
namespace Internal {
namespace Ui {
class TemplatesPreferencesWidget;
}

class TemplatesPreferencesWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit TemplatesPreferencesWidget(QWidget *parent = 0);
    ~TemplatesPreferencesWidget();
    
    void setDataToUi();
    QString searchKeywords() const;
    
    static void writeDefaultSettings(Core::ISettings *s);
    
public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);
    
private:
    void retranslateUi();
    void changeEvent(QEvent *e);
    
private:
    Ui::TemplatesPreferencesWidget *ui;
};


class TemplatesPreferencesPage : public Core::IOptionsPage
{
public:
    TemplatesPreferencesPage(QObject *parent = 0);
    ~TemplatesPreferencesPage();
    
    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;
    
    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();
    
    bool matches(QString &s) const;
    
    QString helpPage() {return QString();}
    
    static void writeDefaultSettings(Core::ISettings *s) {TemplatesPreferencesWidget::writeDefaultSettings(s);}
    
    QWidget *createPage(QWidget *parent = 0);
    
private:
    QPointer<Internal::TemplatesPreferencesWidget> m_Widget;
    QString m_searchKeywords;
};

} // namespace Internal
} // namespace Tools

#endif // TOOLS_INTERNAL_FSPPRINTER_PREFERENCES_H

