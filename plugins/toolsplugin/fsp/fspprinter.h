/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_INTERNAL_FSPPRINTER_H
#define TOOLS_INTERNAL_FSPPRINTER_H

#include <QImage>

/**
 * \file ./plugins/toolsplugin/fsp/fspprinter.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Tools {
namespace Internal {
class Fsp;
class FspPrinterPrivate;

class FspPrinter
{
public:
    enum Cerfa {
        S12541_01 = 0,
        S12541_02,
        S12541_02_2
    };

    enum PrintDirection {
        TopToBottom = 0,
        BottomToTop
    };

    explicit FspPrinter();
    ~FspPrinter();

    void setDrawRects(bool drawRects);
    bool print(const Fsp &fsp, Cerfa cerfa = S12541_01, bool printCerfaAsBackground = false);
    QPixmap preview(const Fsp &fsp, Cerfa cerfa);

private:
    FspPrinterPrivate *d;
};

} // namespace Internal
} // namespace Tools

#endif // TOOLS_INTERNAL_FSPPRINTER_H

