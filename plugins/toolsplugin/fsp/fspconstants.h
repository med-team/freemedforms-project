/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric Maeker <eric.maeker@gmail.com>                               *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_FSPCONSTANTS_H
#define TOOLS_FSPCONSTANTS_H

/**
 * \file ./plugins/toolsplugin/fsp/fspconstants.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Tools {
namespace Constants {

const char * const S_DEFAULTCERFA = "Tools/Fsp/DefaultCerfa";

// Settings Values
const char * const S_CERFA_01 = "cerfa01";
const char * const S_CERFA_02 = "cerfa02";
const char * const S_CERFA_02_V2 = "cerfa02v2";

// Datapack path
const char * const DATAPACK_PATH = "/fsp/";

}
}
#endif // TOOLS_FSPCONSTANTS_H
