/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_INTERNAL_HPRIMFILEMODEL_H
#define TOOLS_INTERNAL_HPRIMFILEMODEL_H

#include <QSortFilterProxyModel>
#include <QFileSystemModel>

/**
 * \file ./plugins/toolsplugin/hprimintegrator/hprimfilemodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Tools {
namespace Internal {
class HprimFileModelPrivate;

class HprimFileModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    enum DataRepresentation {
        PatientName = 0,
        PatientDateOfBirth,
        FileName,
        FileDate,
        ColumnCount
    };

    explicit HprimFileModel(QObject *parent = 0);
    ~HprimFileModel();
    bool setRootPath(const QString &path);

    int columnCount(const QModelIndex &) const {return ColumnCount;}

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    QModelIndex fileRootPath() const;
    QFileInfo fileInfo(const QModelIndex &index) const;
    QString fileContent(const QModelIndex &index) const;

    Qt::ItemFlags flags(const QModelIndex &index) const;

private Q_SLOTS:
    void _onDirectoryLoaded(const QString &absPath);

private:
    HprimFileModelPrivate *d;
};

} // namespace Internal
} // namespace Tools

#endif // TOOLS_INTERNAL_HPRIMFILEMODEL_H

