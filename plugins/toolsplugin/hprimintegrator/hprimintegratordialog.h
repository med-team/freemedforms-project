/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_INTERNAL_HPRIMINTEGRATORDIALOG_H
#define TOOLS_INTERNAL_HPRIMINTEGRATORDIALOG_H

#include <QDialog>

/**
 * \file ./plugins/toolsplugin/hprimintegrator/hprimintegratordialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Tools {
namespace Internal {
class HprimIntegratorDialogPrivate;

class HprimIntegratorDialog : public QDialog
{
    Q_OBJECT

public:
    explicit HprimIntegratorDialog(QWidget *parent = 0);
    ~HprimIntegratorDialog();
    bool initialize(const QString &hprimContent);
    
Q_SIGNALS:
    
public Q_SLOTS:
    void done(int r);

private Q_SLOTS:
    void onPatientSelected(const QString &fullName, const QString &uid);

private:
    HprimIntegratorDialogPrivate *d;
};

} // namespace Internal
} // namespace Tools

#endif // TOOLS_INTERNAL_HPRIMINTEGRATORDIALOG_H

