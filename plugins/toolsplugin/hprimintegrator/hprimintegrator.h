/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_INTERNAL_HPRIMINTEGRATOR_H
#define TOOLS_INTERNAL_HPRIMINTEGRATOR_H

#include <coreplugin/modemanager/imode.h>
#include <QObject>

QT_BEGIN_NAMESPACE
class QModelIndex;
QT_END_NAMESPACE

/**
 * \file ./plugins/toolsplugin/hprimintegrator/hprimintegrator.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Tools {
namespace Internal {
class HprimIntegratorWidgetPrivate;

class HprimIntegratorWidget : public QWidget
{
    Q_OBJECT

public:
    explicit HprimIntegratorWidget(QWidget *parent = 0);
    ~HprimIntegratorWidget();
    
    bool initialize();
    void refreshSettings();

private Q_SLOTS:
    void onFileSelected(const QModelIndex &current, const QModelIndex &);
    void onPatientSelected(const QString &fullName, const QString &uid);
    void onDataIntegrationRequested();

private:
    HprimIntegratorWidgetPrivate *d;
};

class HprimIntegratorMode : public Core::IMode
{
    Q_OBJECT
public:
    HprimIntegratorMode(QObject *parent = 0);
    ~HprimIntegratorMode();

    void refreshSettings();

private:
    HprimIntegratorWidget *_widget;
};
} // namespace Internal
} // namespace Tools

#endif // TOOLS_INTERNAL_HPRIMINTEGRATOR_H

