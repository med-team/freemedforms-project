/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developpers:                                                     *
 *       Eric Maeker <e>                             *
 *   Contributors:                                                         *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef HPRIM_INTEGRATOR_CONSTANTS_H
#define HPRIM_INTEGRATOR_CONSTANTS_H

namespace Tools {
namespace Constants {

const char *const S_DEFAULT_FILE_ENCODING           = "Tools/HprimIntegrator/DefaultFileEncoding";
const char *const S_PATH_TO_SCAN                    = "Tools/HprimIntegrator/PathToScan";
const char *const S_FORMITEM_UUIDS                  = "Tools/HprimIntegrator/FormItemUuids";
const char *const S_FILE_MANAGEMENT                 = "Tools/HprimIntegrator/FileManagement";
const char *const S_FILE_MANAGEMENT_STORING_PATH    = "Tools/HprimIntegrator/FileStoringPath";
const char *const S_ACTIVATION                      = "Tools/HprimIntegrator/Activation";

enum FileManagement {
    RemoveFileDefinitively = 0,
    RemoveFileOneMonthAfterIntegration,
    StoreFileInPath
};

enum ServiceActivation {
    OnlyForFrance = 0,
    Enabled,
    Disabled
};

enum FileEncoding {
    AutoDetect = 0,
    ForceUtf8,
    ForceMacRoman,
    ForceIso8859_1
};

} // namespace Constants
} // namespace Tools

#endif // HPRIM_INTEGRATOR_CONSTANTS_H
