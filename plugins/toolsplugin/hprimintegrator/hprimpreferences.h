/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developpers:                                                     *
 *       Eric Maeker <eric.maeker@gmail.com>                               *
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TOOLS_INTERNAL_HprimPreferences_H
#define TOOLS_INTERNAL_HprimPreferences_H

#include <coreplugin/ioptionspage.h>

#include <QWidget>
#include <QPointer>

/**
 * \file ./plugins/toolsplugin/hprimintegrator/hprimpreferences.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;
}

namespace Tools {
namespace Internal {
namespace Ui {
class HprimPreferencesWidget;
}

class HprimPreferencesWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit HprimPreferencesWidget(QWidget *parent = 0);
    ~HprimPreferencesWidget();
    
    void setDataToUi();
    QString searchKeywords() const;
    
    static void writeDefaultSettings(Core::ISettings *s);
    
public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);
    
private Q_SLOTS:
    void onFileManagementChanged(int);

private:
    void retranslateUi();
    void changeEvent(QEvent *e);
    
private:
    Ui::HprimPreferencesWidget *ui;
};


class HprimPreferencesPage : public Core::IOptionsPage
{
    Q_OBJECT

public:
    HprimPreferencesPage(QObject *parent = 0);
    ~HprimPreferencesPage();
    
    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;
    
    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();
    
    bool matches(const QString &s) const;
    
    QString helpPage() {return QString();}
    
    static void writeDefaultSettings(Core::ISettings *s) {HprimPreferencesWidget::writeDefaultSettings(s);}
    
    QWidget *createPage(QWidget *parent = 0);
    
private:
    QPointer<Internal::HprimPreferencesWidget> m_Widget;
    QString m_searchKeywords;
};


} // namespace Internal
} // namespace Tools

#endif // TOOLS_INTERNAL_HprimPreferences_H

