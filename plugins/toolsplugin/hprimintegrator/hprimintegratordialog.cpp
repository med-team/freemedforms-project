/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers: Eric MAEKER, <eric.maeker@gmail.com>                 *
 *   Contributors:                                                         *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
/*!
 * \class Tools::Internal::HprimIntegratorDialog
 */

#include "hprimintegratordialog.h"
#include "constants.h"
#include "ui_hprimintegratordialog.h"

#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/ipatient.h>
#include <coreplugin/isettings.h>

#include <formmanagerplugin/formcore.h>
#include <formmanagerplugin/iformitem.h>
#include <formmanagerplugin/formmanager.h>
#include <formmanagerplugin/episodemodel.h>
#include <formmanagerplugin/iformitemdata.h>
#include <formmanagerplugin/episodemanager.h>
#include <formmanagerplugin/constants_settings.h>

#include <patientbaseplugin/patientmodel.h>

#include <utils/log.h>
#include <utils/global.h>
#include <utils/hprimparser.h>
#include <translationutils/constants.h>

#include <QStringListModel>
#include <QCryptographicHash>
#include <QPushButton>

#include <QDebug>

using namespace Tools;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Core::ISettings *settings() {return Core::ICore::instance()->settings();}
static inline Core::IPatient *patient() {return Core::ICore::instance()->patient();}
static inline Form::FormManager &formManager() {return Form::FormCore::instance().formManager();}
static inline Form::EpisodeManager &episodeManager() {return Form::FormCore::instance().episodeManager();}

namespace Tools {
namespace Internal {
class HprimIntegratorDialogPrivate
{
public:
    HprimIntegratorDialogPrivate(HprimIntegratorDialog *parent) :
        ui(0),
        _formItemsUidModel(0),
        q(parent)
    {
    }
    
    ~HprimIntegratorDialogPrivate()
    {
        delete ui;
    }

    QByteArray sha1(const QString &string)
    {
        QByteArray hashData = QCryptographicHash::hash(string.toUtf8(), QCryptographicHash::Sha1);
        return hashData.toHex();
    }

    Utils::HPRIM::HprimMessage &parseHprimContent(const QString &hprimContent)
    {
        return Utils::HPRIM::parseHprimRawSource(hprimContent);
    }

    void findFormItem()
    {
        _formItems.clear();
        if (!_formItemsUidModel) {
            _formItemsUidModel = new QStringListModel(q);
        }

        QStringList itemUid = settings()->value(Constants::S_FORMITEM_UUIDS).toStringList(); //"GP::Basic::Consultation::Results::Textual";
        QStringList itemLabel;

        QList<Form::FormMain*> emptyRoot = formManager().allEmptyRootForms();
        foreach(Form::FormMain *root, emptyRoot) {
            foreach(Form::FormItem *i, root->flattenedFormItemChildren()) {
                if (i->spec()->useForHprimImportation()
                        || itemUid.contains(i->uuid())) {
                    _formItems << i;
                    _itemUidForModel << i->uuid();
                    QString label = i->spec()->label();
                    Form::FormMain *parent = i->parentFormMain();
                    while (parent) {
                        if (!parent->spec()->label().isEmpty())
                            label.prepend(QString("%1 / ").arg(parent->spec()->label()));
                        parent = parent->parentFormMain();
                    }
                    itemLabel << label;
                }
            }
        }
        if (_formItems.isEmpty())
            LOG_ERROR_FOR(q, "No FormItem found");
        _formItemsUidModel->setStringList(itemLabel);
    }

    QString getPatientUid(const Utils::HPRIM::HprimHeader &hdr)
    {
        ui->patientSearch->setText(QString("%1 %2").arg(hdr.patientName()).arg(hdr.patientFirstName()));
        Patients::PatientModel *model = new Patients::PatientModel(q);
        model->setFilter(hdr.patientName(), hdr.patientFirstName());
        if (model->rowCount() == 1) {
            ui->patientInfo->setText(QCoreApplication::translate("HprimIntegratorDialog", "Patient correctly detected: %1; %2")
                                        .arg(model->data(model->index(0, Core::IPatient::FullName)).toString())
                                     .arg(QLocale().toString(model->data(model->index(0, Core::IPatient::DateOfBirth)).toDate(), QLocale::LongFormat)));
            QModelIndex index = model->index(0, Core::IPatient::Uid);
            return model->data(index).toString();
        } else if (model->rowCount() == 0) {
            ui->patientInfo->setText(QCoreApplication::translate("HprimIntegratorDialog", "No patient found with the following names: %1; %2")
                                        .arg(hdr.patientName())
                                        .arg(hdr.patientFirstName()));
        } else {
            ui->patientInfo->setText(QCoreApplication::translate("HprimIntegratorDialog", "There are multiple patient with the same names: %1 %2. You will have to select the correct one.")
                                        .arg(hdr.patientName())
                                        .arg(hdr.patientFirstName()));
        }
        return QString::null;
    }

    QString populateFormItem(Form::FormItem *item)
    {
        Form::EpisodeModel *model = new Form::EpisodeModel(item->parentFormMain(), q);
        model->initialize();
        model->setCurrentPatient(_patientUid);

        if (!model->insertRow(0)) {
            LOG_ERROR_FOR(q, "Unable to create an episode");
            return QString::null;
        }

        int row = model->rowCount() - 1;
        model->setData(model->index(row, Form::EpisodeModel::Label), "HPRIM");
        if (!item->itemData()) {
            LOG_ERROR_FOR(q, "No item data to fill");
            return QString::null;
        }

        QString html = QString("<pre>%1</pre>").arg(_hprimMessage.toRawSource().replace("<", "&lt;"));
        item->itemData()->setData(0, html);

        QHash<QString, QString> xmlData;
        foreach(Form::FormItem *it, item->parentFormMain()->flattenedFormItemChildren()) {
            if (it->itemData()) {
                xmlData.insert(it->uuid(), it->itemData()->storableData().toString());
            }
        }
        QString xml = Utils::createXml(Form::Constants::XML_FORM_GENERAL_TAG, xmlData, 2, false);
        model->setData(model->index(row, Form::EpisodeModel::XmlContent), xml);

        model->submit();
        QString data = item->itemData()->data(0).toString().replace("<pre ", "<p ").replace("</pre", "</p");
        return sha1(data.toUtf8());
    }

    bool checkFormItemContent(const QString &contentSha1, Form::FormItem *item)
    {
        Q_ASSERT(item);
        if (!item)
            return false;
        if (!item->itemData())
            return false;
        if (!item->parentFormMain())
            return false;
        item->parentFormMain()->clear();
        Form::EpisodeModel *model = new Form::EpisodeModel(item->parentFormMain(), q);
        model->initialize();
        model->setCurrentPatient(_patientUid);
        model->populateFormWithLatestValidEpisodeContent();
        QString data = item->itemData()->data(0).toString().replace("<pre ", "<p ").replace("</pre", "</p");

        if (sha1(data.toUtf8()) != contentSha1) {
            LOG_ERROR_FOR(q, "Wrong importation SHA1 validation");
            return false;
        } else {
            LOG_FOR(q, "Importation correctly checked");
        }
        return true;
    }

    Form::FormItem *getSelectedFormItem()
    {
        Q_ASSERT(!_formItems.isEmpty());
        if (_formItems.isEmpty())
            return 0;
        if (!ui->formItemListView->selectionModel()->hasSelection())
            return _formItems.at(0);
        QString uid = _itemUidForModel.at(ui->formItemListView->selectionModel()->currentIndex().row());
        foreach(Form::FormItem *item, _formItems) {
            if (item->uuid() == uid)
                return item;
        }
        return _formItems.at(0);
    }

public:
    Ui::HprimIntegratorDialog *ui;
    QString _hprimContent, _patientUid;
    QList<Form::FormItem*> _formItems;
    Utils::HPRIM::HprimMessage _hprimMessage;
    QStringListModel *_formItemsUidModel;
    QStringList _itemUidForModel;

private:
    HprimIntegratorDialog *q;
};
} // namespace Internal
} // end namespace Tools


/*! Constructor of the Tools::Internal::HprimIntegratorDialog class */
HprimIntegratorDialog::HprimIntegratorDialog(QWidget *parent) :
    QDialog(parent),
    d(new HprimIntegratorDialogPrivate(this))
{
    d->ui = new Ui::HprimIntegratorDialog;
    d->ui->setupUi(this);
    d->findFormItem();
    d->ui->formItemListView->setModel(d->_formItemsUidModel);
    connect(d->ui->patientSearch, SIGNAL(patientSelected(QString,QString)), this, SLOT(onPatientSelected(QString,QString)));
}

/*! Destructor of the Tools::Internal::HprimIntegratorDialog class */
HprimIntegratorDialog::~HprimIntegratorDialog()
{
    if (d)
        delete d;
    d = 0;
}

/*! Initializes the object with the default values. Return true if initialization was completed. */
bool HprimIntegratorDialog::initialize(const QString &hprimContent)
{
    d->_hprimContent = hprimContent;
    d->_hprimMessage = d->parseHprimContent(hprimContent);

    d->ui->hprimContent->clear();
    d->ui->hprimContent->appendHtml(d->_hprimMessage.toBasicHtml());
    QTextCursor cursor = d->ui->hprimContent->textCursor();
    cursor.movePosition(QTextCursor::Start, QTextCursor::MoveAnchor);
    d->ui->hprimContent->setTextCursor(cursor);

    d->_patientUid = d->getPatientUid(d->_hprimMessage.header());

    QPushButton *ok = d->ui->buttonBox->button(QDialogButtonBox::Ok);
    if (ok)
        ok->setEnabled(!d->_patientUid.isEmpty());
    return true;
}

void HprimIntegratorDialog::onPatientSelected(const QString &fullName, const QString &uid)
{
    d->_patientUid = uid;
    d->ui->patientInfo->setText(
                QCoreApplication::translate("HprimIntegratorDialog",
                                            "You selected the following patient: %1")
                .arg(fullName)
                );
    QPushButton *ok = d->ui->buttonBox->button(QDialogButtonBox::Ok);
    if (ok)
        ok->setEnabled(!d->_patientUid.isEmpty());
}

void HprimIntegratorDialog::done(int r)
{
    if (r == QDialog::Rejected) {
        QDialog::done(r);
        return;
    }

    if (d->_patientUid.isEmpty()) {
        QDialog::done(QDialog::Rejected);
        return;
    }

    Form::FormItem *item = d->getSelectedFormItem();
    if (!item) {
        QDialog::done(QDialog::Rejected);
        return;
    }

    QString sha1 = d->populateFormItem(item);
    if (sha1.isEmpty()) {
        QDialog::done(QDialog::Rejected);
        return;
    }

    if (!d->checkFormItemContent(sha1, item)) {
        QDialog::done(QDialog::Accepted);
        return;
    }

    QDialog::done(QDialog::Accepted);
}
