/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, <eric.maeker@gmail.com>,                             *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "alertitemeditordialog.h"
#include "alertitemeditorwidget.h"
#include "ui_alertitemeditordialog.h"

#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QLabel>

using namespace Alert;

AlertItemEditorDialog::AlertItemEditorDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AlertItemEditorDialog)
{
    ui->setupUi(this);
    setWindowTitle(ui->title->text());
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    QPushButton *but = ui->buttonBox->button(QDialogButtonBox::Reset);
    connect(but, SIGNAL(clicked()), this, SLOT(reset()));
}

AlertItemEditorDialog::~AlertItemEditorDialog()
{
    delete ui;
}


void AlertItemEditorDialog::setEditableParams(EditableParams params)
{
    ui->editor->setLabelVisible(params.testFlag(Label));
    ui->editor->setCategoryVisible(params.testFlag(Category));
    ui->editor->setDescriptionVisible(params.testFlag(Description));
    ui->editor->setIconVisible(params.testFlag(Icon));
    ui->editor->setRelationVisible(params.testFlag(Relation));
    ui->editor->setViewTypeVisible(params.testFlag(ViewType));
    ui->editor->setContentTypeVisible(params.testFlag(ContentType));
    ui->editor->setPriorityVisible(params.testFlag(Priority));
    ui->editor->setOverridingCommentVisible(params.testFlag(OverrideNeedsComment));
    if (!(params.testFlag(Timing)))
        ui->editor->hideTimingTab();
    if (!(params.testFlag(CSS)))
        ui->editor->hideStyleSheetTab();
    if (!(params.testFlag(ExtraXml)))
        ui->editor->hideExtraXmlTab();
    if (!(params.testFlag(Scripts)))
        ui->editor->hideScriptsTab();
}

void AlertItemEditorDialog::setAlertItem(const AlertItem &item)
{
    ui->editor->setAlertItem(item);
}


void AlertItemEditorDialog::reset()
{
    ui->editor->reset();
}


bool AlertItemEditorDialog::submit(AlertItem &item)
{
    return ui->editor->submit(item);
}
