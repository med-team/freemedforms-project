/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "alertplugin.h"
#include "alertcore.h"
#include "alertitem.h"
#include "alertpreferences.h"
#include "patientbaralertplaceholder.h"

#include <utils/log.h>

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/itheme.h>
#include <coreplugin/iuser.h>
#include <coreplugin/ipatient.h>
#include <coreplugin/ipatientbar.h>
#include <coreplugin/translators.h>

#include <QtPlugin>
#include <QDebug>

using namespace Alert;
using namespace Internal;

static inline Core::IUser *user()  { return Core::ICore::instance()->user(); }
static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline Core::IPatient *patient() {return Core::ICore::instance()->patient();}
static inline void messageSplash(const QString &s) {theme()->messageSplashScreen(s); }

AlertPlugin::AlertPlugin() :
    ExtensionSystem::IPlugin(),
    _prefPage(0),
    _patientPlaceHolder(0)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating AlertPlugin";
    setObjectName("AlertPlugin");

    Core::ICore::instance()->translators()->addNewTranslator("plugin_alert");


    _prefPage = new AlertPreferencesPage(this);
    addObject(_prefPage);

    new AlertCore(this);

    connect(Core::ICore::instance(), SIGNAL(coreOpened()), this, SLOT(postCoreInitialization()));
    connect(Core::ICore::instance(), SIGNAL(coreAboutToClose()), this, SLOT(coreAboutToClose()));
}

AlertPlugin::~AlertPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool AlertPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "AlertPlugin::initialize";
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);



    return true;
}

void AlertPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "AlertPlugin::extensionsInitialized";

    if (!user())
        return;
    if (user()->uuid().isEmpty())
        return;

    messageSplash(tr("Initializing AlertPlugin..."));

    AlertCore::instance().initialize();


    LOG("Creating patient alert placeholder");
    _patientPlaceHolder = new PatientBarAlertPlaceHolder(this);
    addObject(_patientPlaceHolder);
    if (patient()->patientBar())
        patient()->patientBar()->addBottomWidget(_patientPlaceHolder->createWidget(patient()->patientBar()));

    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));
    connect(Core::ICore::instance(), SIGNAL(coreOpened()), this, SLOT(postCoreInitialization()));
}

void AlertPlugin::postCoreInitialization()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << Q_FUNC_INFO;

    AlertCore::instance().postCoreInitialization();
}

void AlertPlugin::coreAboutToClose()
{
}

ExtensionSystem::IPlugin::ShutdownFlag AlertPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    if (_prefPage)
        removeObject(_prefPage);
    if (_patientPlaceHolder)
        removeObject(_patientPlaceHolder);
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(AlertPlugin)
