/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, <eric.maeker@gmail.com>,                             *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "alertscriptmanager.h"

#include <coreplugin/icore.h>
#include <coreplugin/iscriptmanager.h>

#include <QScriptEngine>

#include <QDebug>
enum { WarnExecutedScripts = false };

using namespace Alert;
using namespace Internal;

static inline Core::IScriptManager *scriptManager() {return Core::ICore::instance()->scriptManager();}

AlertScriptManager::AlertScriptManager(QObject *parent) :
    QObject(parent),
    _wrapper(0),
    _test(0)
{
    setObjectName("AlertScriptManager");
    if (!scriptManager()) {
        _test = new QScriptEngine(this);
    }
}


QVariant AlertScriptManager::execute(AlertItem &item, const int scriptType)
{
    if (_wrapper) {
        delete _wrapper;
        _wrapper = 0;
    }

    const QString &script = item.scriptType(AlertScript::ScriptType(scriptType)).script();
    if (WarnExecutedScripts) {
        qWarning() << "Alert::Execute script"
                   << AlertScript::typeToXml(AlertScript::ScriptType(scriptType))
                   << "\n    "
                   << item.uuid()
                   << "\n    "
                   << item.label()
                   << "\n    "
                   << script;
    }

    if (script.isEmpty())
        return QVariant();

    if (scriptManager()) {
        _wrapper = new AlertItemScriptWrapper(item);
        QScriptValue wrapperValue = scriptManager()->addScriptObject(_wrapper);
        scriptManager()->evaluate("namespace.com.freemedforms").setProperty("alert", wrapperValue);
    } else {
        _wrapper = new AlertItemScriptWrapper(item);
        QScriptValue wrapperValue = _test->newQObject(_wrapper, QScriptEngine::QtOwnership);
        _test->globalObject().setProperty("alert", wrapperValue);
    }

    QScriptValue toReturn;
    if (scriptManager())
        toReturn = scriptManager()->evaluate(script);
    else
        toReturn = _test->evaluate(script);

    if (_wrapper) {
        delete _wrapper;
        _wrapper = 0;
    }

    if (WarnExecutedScripts)
        qWarning() << "Alert::returned value" << toReturn.toVariant();

    return toReturn.toVariant();
}

