/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric MAEKER, <eric.maeker@gmail.com>                *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ALERTPLUGIN_EXPORTER_H
#define ALERTPLUGIN_EXPORTER_H

/**
  \namespace Alert
  \brief Namespace for the Alert plugin.
  Namespace for the Alert plugin.
*/

#include <qglobal.h>

#if defined(ALERT_LIBRARY)
#define ALERT_EXPORT Q_DECL_EXPORT
#else
#define ALERT_EXPORT Q_DECL_IMPORT
#endif

#endif  // ALERTPLUGIN_EXPORTER_H
