/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, <eric.maeker@gmail.com>,                             *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "alertcore.h"
#include "alertbase.h"
#include "alertitem.h"
#include "ialertplaceholder.h"
#include "alertscriptmanager.h"
#include "alertpackdescription.h"
#include "alertplaceholderwidget.h"
#include "constants.h"
#include "blockingalertdialog.h"

#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/ipatient.h>
#include <coreplugin/iuser.h>

#include <extensionsystem/pluginmanager.h>
#include <utils/log.h>
#include <utils/global.h>
#include <translationutils/constants.h>
#include <translationutils/trans_filepathxml.h>

#include <datapackutils/datapackcore.h>
#include <datapackutils/ipackmanager.h>
#include <datapackutils/pack.h>

#include <QDir>

using namespace Alert;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline ExtensionSystem::PluginManager *pluginManager() {return ExtensionSystem::PluginManager::instance();}
static inline Core::IPatient *patient() {return Core::ICore::instance()->patient();}
static inline Core::IUser *user() {return Core::ICore::instance()->user();}

static inline DataPack::DataPackCore &dataPackCore() { return DataPack::DataPackCore::instance(); }
static inline DataPack::IPackManager *packManager() { return dataPackCore().packManager(); }

AlertCore *AlertCore::_instance = 0;

AlertCore &AlertCore::instance()
{
    Q_ASSERT(_instance);
    if (!_instance)
        _instance = new AlertCore(qApp);
    return *_instance;
}

namespace Alert {
namespace Internal {
class AlertCorePrivate
{
public:
    AlertCorePrivate(AlertCore *parent) :
        _initialized(false),
        _alertBase(0),
        _alertScriptManager(0),
        q(parent)
    {
        Q_UNUSED(q);
    }

    ~AlertCorePrivate()
    {
    }

public:
    bool _initialized;
    AlertBase *_alertBase;
    AlertScriptManager *_alertScriptManager;

private:
    AlertCore *q;
};
}
}

AlertCore::AlertCore(QObject *parent) :
    QObject(parent),
    d(new Internal::AlertCorePrivate(this))
{
    _instance = this;
    setObjectName("AlertCore");

    d->_alertBase = new Internal::AlertBase(this);
    d->_alertScriptManager = new Internal::AlertScriptManager(this);

    connect(packManager(), SIGNAL(packInstalled(DataPack::Pack)), this, SLOT(packInstalled(DataPack::Pack)));
    connect(packManager(), SIGNAL(packRemoved(DataPack::Pack)), this, SLOT(packRemoved(DataPack::Pack)));
}

AlertCore::~AlertCore()
{
    if (d) {
        delete d;
        d = 0;
    }
}

bool AlertCore::initialize()
{
    if (d->_initialized)
        return true;
    if (!d->_alertBase->initialize())
        return false;
    d->_initialized = true;
    return true;
}

bool AlertCore::isInitialized() const
{
    return d->_initialized;
}

#ifdef WITH_TESTS
Internal::AlertBase &AlertCore::alertBase() const
{
    return *d->_alertBase;
}
#endif

QVector<AlertItem> AlertCore::getAlertItemForCurrentUser() const
{
    Internal::AlertBaseQuery query;
    query.addCurrentUserAlerts();
    query.setAlertValidity(Internal::AlertBaseQuery::ValidAlerts);
    return d->_alertBase->getAlertItems(query);
}

QVector<AlertItem> AlertCore::getAlertItemForCurrentPatient() const
{
    Internal::AlertBaseQuery query;
    query.addCurrentPatientAlerts();
    query.setAlertValidity(Internal::AlertBaseQuery::ValidAlerts);
    return d->_alertBase->getAlertItems(query);
}

QVector<AlertItem> AlertCore::getAlertItemForCurrentApplication() const
{
    Internal::AlertBaseQuery query;
    query.addApplicationAlerts(qApp->applicationName().toLower());
    query.setAlertValidity(Internal::AlertBaseQuery::ValidAlerts);
    return d->_alertBase->getAlertItems(query);
}

bool AlertCore::saveAlert(AlertItem &item)
{
    return d->_alertBase->saveAlertItem(item);
}

bool AlertCore::saveAlerts(QList<AlertItem> &items)
{
    bool ok = true;
    for(int i=0; i < items.count(); ++i) {
        AlertItem &item = items[i];
        if (!d->_alertBase->saveAlertItem(item))
            ok = false;
    }
    return ok;
}


bool AlertCore::checkAlerts(AlertsToCheck check)
{
    Internal::AlertBaseQuery query;
    if (check & CurrentUserAlerts)
        query.addCurrentUserAlerts();
    if (check & CurrentPatientAlerts)
        query.addCurrentPatientAlerts();
    if (check & CurrentApplicationAlerts)
        query.addApplicationAlerts(qApp->applicationName().toLower());
    query.setAlertValidity(Internal::AlertBaseQuery::ValidAlerts);

    QVector<AlertItem> alerts = d->_alertBase->getAlertItems(query);
    processAlerts(alerts, true);
    return true;
}


bool AlertCore::registerAlert(const AlertItem &item)
{
    QVector<AlertItem> items;
    items << item;
    processAlerts(items, false);
    return true;
}


bool AlertCore::updateAlert(const AlertItem &item)
{
    QList<Alert::IAlertPlaceHolder*> placeHolders = pluginManager()->getObjects<Alert::IAlertPlaceHolder>();
    foreach(Alert::IAlertPlaceHolder *ph, placeHolders) {
        ph->updateAlert(item);
    }

    if (item.viewType() == AlertItem::BlockingAlert) {
        if (item.isUserValidated() || !item.isValid())
            return true;
        BlockingAlertDialog::executeBlockingAlert(item);
    }
    return true;
}


bool AlertCore::removeAlert(const AlertItem &item)
{
    bool ok =true;
    if (item.viewType() == AlertItem::NonBlockingAlert) {
        QList<Alert::IAlertPlaceHolder*> placeHolders = pluginManager()->getObjects<Alert::IAlertPlaceHolder>();
        foreach(Alert::IAlertPlaceHolder *ph, placeHolders) {
            if (!ph->removeAlert(item))
                ok = false;
        }
    }
    return ok;
}


bool AlertCore::registerAlertPack(const QString &absPath)
{
    LOG(tr("Registering alert pack: %1").arg(QDir::cleanPath(absPath)));
    QDir path(absPath);
    if (!path.exists()) {
        LOG_ERROR(tkTr(Trans::Constants::PATH_1_DOESNOT_EXISTS).arg(QDir::cleanPath(absPath)));
        return false;
    }

    QFileInfo descrFile(QString("%1/%2").arg(absPath).arg(Constants::PACK_DESCRIPTION_FILENAME));
    if (!descrFile.exists()) {
        LOG_ERROR(tr("No alert pack description"));
        return false;
    }
    AlertPackDescription descr;
    descr.fromXmlFile(descrFile.absoluteFilePath());
    if (!d->_alertBase->saveAlertPackDescription(descr)) {
        LOG_ERROR("Unable to save alert pack description to database");
        return false;
    }

    QFileInfoList files = Utils::getFiles(path, "*.xml", Utils::Recursively);
    if (files.isEmpty()) {
        LOG_ERROR(tkTr(Trans::Constants::PATH_1_IS_EMPTY));
        return false;
    }
    QList<AlertItem> alerts;
    foreach(const QFileInfo &info, files) {
        if (info.fileName()==QString(Constants::PACK_DESCRIPTION_FILENAME))
            continue;
        AlertItem alert = AlertItem::fromXml(Utils::readTextFile(info.absoluteFilePath(), Utils::DontWarnUser));
        if (alert.isValid())
            alerts << alert;
    }
    return saveAlerts(alerts);
}


bool AlertCore::removeAlertPack(const QString &uid)
{
    return d->_alertBase->removeAlertPack(uid);
}

AlertPackDescription AlertCore::getAlertPackDescription(const QString &uuid)
{
    return d->_alertBase->getAlertPackDescription(uuid);
}


void AlertCore::processAlerts(QVector<AlertItem> &alerts, bool clearPlaceHolders)
{
    QList<Alert::IAlertPlaceHolder*> placeHolders = pluginManager()->getObjects<Alert::IAlertPlaceHolder>();
    if (clearPlaceHolders) {
        foreach(Alert::IAlertPlaceHolder *ph, placeHolders)
            ph->clear();
    }

    QList<AlertItem> blockings;
    for(int i = 0; i < alerts.count(); ++i) {
        AlertItem &item = alerts[i];

        QVariant checkValid = d->_alertScriptManager->execute(item, AlertScript::CheckValidityOfAlert);
        if (checkValid.isValid() && checkValid.canConvert(QVariant::Bool) && !checkValid.toBool())
            continue;

        for(int i =0; i < item.timings().count(); ++i) {
            AlertTiming &timing = item.timingAt(i);
            if (timing.isCycling()) {
                QVariant startDate = d->_alertScriptManager->execute(item, AlertScript::CyclingStartDate);
                if (startDate.isValid() && startDate.canConvert(QVariant::DateTime)) {
                }
            }
        }

        if (item.viewType() == AlertItem::BlockingAlert) {
            if (!item.isValid() || item.isUserValidated())
                continue;
            blockings << item;
        } else {
            foreach(Alert::IAlertPlaceHolder *ph, placeHolders) {
                ph->addAlert(item);
            }
        }
    }

    if (!blockings.isEmpty()) {
        BlockingAlertResult result = BlockingAlertDialog::executeBlockingAlert(blockings);
        BlockingAlertDialog::applyResultToAlerts(blockings, result);
        if (!saveAlerts(blockings))
            LOG_ERROR("Unable to save validated blockings alerts");
    }
}

void AlertCore::postCoreInitialization()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << Q_FUNC_INFO;
    if (patient())
        connect(patient(), SIGNAL(currentPatientChanged()), this, SLOT(checkPatientAlerts()));
    if (user())
        connect(user(), SIGNAL(userChanged()), this, SLOT(checkUserAlerts()));
}


QVariant AlertCore::execute(AlertItem &item, const int scriptType)
{
    return d->_alertScriptManager->execute(item, scriptType);
}

void AlertCore::packInstalled(const DataPack::Pack &pack)
{
    if (pack.dataType() == DataPack::Pack::AlertPacks) {
        if (!registerAlertPack(pack.unzipPackToPath())) {
            LOG_ERROR(tr("Unable to register AlertPack. Path: %1").arg(pack.unzipPackToPath()));
            return;
        }
        checkAllAlerts();
    }
}

void AlertCore::packRemoved(const DataPack::Pack &pack)
{
    if (pack.dataType() == DataPack::Pack::AlertPacks) {
        if (!removeAlertPack(pack.uuid()))
            LOG_ERROR("Unable to remove AlertPack " + pack.uuid());
    }
}
