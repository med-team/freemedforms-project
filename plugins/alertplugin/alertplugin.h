/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ALERT_PLUGIN_H
#define ALERT_PLUGIN_H

#include <extensionsystem/iplugin.h>

#include <QtCore/QObject>

#ifdef WITH_TESTS
#include <QStringList>
#endif

/**
 * \file ./plugins/alertplugin/alertplugin.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Alert {
namespace Internal {
class AlertPreferencesPage;
class PatientBarAlertPlaceHolder;

class AlertPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.freemedforms.FreeMedForms.AlertPlugin" FILE "Alert.json")

public:
    AlertPlugin();
    ~AlertPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

private Q_SLOTS:
    void postCoreInitialization();
    void coreAboutToClose();

#ifdef WITH_TESTS
private Q_SLOTS:
    void test_alertscript_object();
    void test_alertrelation_object();
    void test_alertvalidation_object();
    void test_alerttiming_object();
    void test_alertitem_object();
    void test_alertcore_init();
    void test_alertbase_basics();
    void test_alertbase_cycling_alerts();
    void test_alertbase_complex_query();
    void test_alertbase_complex_query_with_cycling_alerts();
    void test_blockingalert_dialog();
    void cleanupTestCase();
private:
    QStringList uidsToPurge;
#endif

private:
    AlertPreferencesPage *_prefPage;
    PatientBarAlertPlaceHolder *_patientPlaceHolder;
};

}  // End namespace Internal
}  // End namespace Alert

#endif  // End ALERT_PLUGIN_H
