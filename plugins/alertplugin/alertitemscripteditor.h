/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, <eric.maeker@gmail.com>,                             *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ALERT_INTERNAL_ALERTITEMSCRIPTEDITOR_H
#define ALERT_INTERNAL_ALERTITEMSCRIPTEDITOR_H

#include <alertplugin/alertitem.h>
#include <QWidget>
#include <QVector>
QT_BEGIN_NAMESPACE
class QMenu;
QT_END_NAMESPACE

/**
 * \file ./plugins/alertplugin/alertitemscripteditor.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Alert {
namespace Internal {
namespace Ui {
class AlertItemScriptEditor;
}

class AlertItemScriptEditor : public QWidget
{
    Q_OBJECT
public:
    explicit AlertItemScriptEditor(QWidget *parent = 0);
    ~AlertItemScriptEditor();

    void clear();
    void setAlertItem(const AlertItem &alert);

    QVector<AlertScript> scripts() const;

public Q_SLOTS:
    void submit();

private:
    void refreshScriptCombo();

private Q_SLOTS:
    void onTypesSelected(int index);
    void addAction(QAction *a);

private:
    int _previousIndex;
    Ui::AlertItemScriptEditor *ui;
    QList<AlertScript> _scriptsCache, _scripts;
    QMenu *_menu;
};

} // namespace Internal
} // namespace Alert

#endif // ALERT_INTERNAL_ALERTITEMSCRIPTEDITOR_H
