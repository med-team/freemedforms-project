/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "patientbaralertplaceholder.h"

using namespace Alert;
using namespace Internal;

PatientBarAlertPlaceHolder::PatientBarAlertPlaceHolder(QObject *parent) :
    Alert::AlertPlaceHolderWidget(parent)
{
    setIconSize(QSize(16,16));
    setMargin(0);
    setSpacing(2);
    setBorderSize(0);
    setDrawBackgroundUsingAlertPriority(false);
    setAutoSaveOnValidationOrOverriding(true);
    setAutoSaveOnEditing(true);
    setObjectName("Patients::Internal::PatientBarAlertPlaceHolder");
}

QString PatientBarAlertPlaceHolder::uuid() const
{
    return objectName();
}

QString PatientBarAlertPlaceHolder::name(const QString &lang) const
{
    Q_UNUSED(lang);
    return tr("Patient non-blocking alerts");
}

QString PatientBarAlertPlaceHolder::category(const QString &lang) const
{
    Q_UNUSED(lang);
    return tr("Patient alerts");
}

QString PatientBarAlertPlaceHolder::description(const QString &lang) const
{
    Q_UNUSED(lang);
    return tr("Placeholder for patient related non-blocking alerts.");
}

Alert::AlertItem PatientBarAlertPlaceHolder::getDefaultEmptyAlert() const
{
    AlertItem item;
    item.setValidity(true);
    item.setEditable(true);
    item.setCreationDate(QDateTime::currentDateTime());
    item.setContentType(AlertItem::PatientCondition);
    item.setViewType(AlertItem::NonBlockingAlert);
    item.addTiming(AlertTiming(QDateTime(QDate::currentDate(), QTime(0,0,0)), QDateTime(QDate::currentDate(), QTime(23, 59, 59)).addYears(1)));
    return item;
}
