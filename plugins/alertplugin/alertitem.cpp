/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, <eric.maeker@gmail.com>,                             *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


























#include "alertitem.h"
#include "alertcore.h"
#include "alertpackdescription.h"

#include <coreplugin/icore.h>
#include <coreplugin/iuser.h>
#include <coreplugin/ipatient.h>
#include <coreplugin/itheme.h>
#include <coreplugin/constants_icons.h>
#include <coreplugin/constants_colors.h>

#include <utils/log.h>
#include <utils/global.h>
#include <utils/genericdescription.h>
#include <translationutils/constants.h>
#include <translationutils/trans_current.h>
#include <translationutils/trans_datetime.h>
#include <translationutils/trans_filepathxml.h>
#include <translationutils/trans_msgerror.h>
#include <translationutils/multilingualclasstemplate.h>

#include <QTreeWidgetItem>
#include <QDomDocument>

#include <QDebug>

enum { WarnAlertItemConstructionDestruction = false };

using namespace Alert;
using namespace Trans::ConstantTranslations;

static inline Core::IUser *user() {return Core::ICore::instance()->user();}
static inline Core::IPatient *patient() {return Core::ICore::instance()->patient();}
static inline Core::ITheme *theme() {return Core::ICore::instance()->theme();}
static inline Alert::AlertCore &alertCore() {return Alert::AlertCore::instance();}

namespace {
const char * const XML_ROOT_TAG = "Alert";
const char * const XML_DESCRIPTION_ROOTTAG = "Descr";
const char * const XML_TIMING_ROOTTAG = "Timings";
const char * const XML_RELATED_ROOTTAG = "Rels";
const char * const XML_VALIDATION_ROOTTAG = "Vals";
const char * const XML_SCRIPT_ROOTTAG = "Scripts";
const char * const XML_EXTRAXML_ROOTTAG = "Xtra";

const char * const XML_TIMING_ELEMENTTAG = "Timing";
const char * const XML_RELATED_ELEMENTTAG = "Rel";
const char * const XML_VALIDATION_ELEMENTTAG = "Val";
const char * const XML_SCRIPT_ELEMENTTAG = "Script";

}

namespace Alert {
namespace Internal {

class AlertXmlDescription  : public Utils::GenericDescription
{
public:
    enum NonTr {
        CryptedPass = Utils::GenericDescription::NonTranslatableExtraData + 1,
        ViewType,
        ContentType,
        Priority,
        OverrideRequiresUserComment,
        MustBeRead,
        RemindLater,
        Editable,
        StyleSheet,
        PackUid
    };
    enum Tr {
        Comment = Utils::GenericDescription::TranslatableExtraData + 1
    };

    AlertXmlDescription() : Utils::GenericDescription(::XML_DESCRIPTION_ROOTTAG)
    {
        addNonTranslatableExtraData(CryptedPass, "cryptedPassword");
        addNonTranslatableExtraData(ViewType, "viewType");
        addNonTranslatableExtraData(ContentType, "contentType");
        addNonTranslatableExtraData(Priority, "prior");
        addNonTranslatableExtraData(OverrideRequiresUserComment, "overrideComment");
        addNonTranslatableExtraData(MustBeRead, "mustBeRead");
        addNonTranslatableExtraData(RemindLater, "remindLater");
        addNonTranslatableExtraData(Editable, "editable");
        addNonTranslatableExtraData(StyleSheet, "styleSheet");
        addNonTranslatableExtraData(PackUid, "packUid");
        addTranslatableExtraData(Comment, "comment");
    }

    ~AlertXmlDescription() {}
};

class AlertValueBook {
public:
    void toTreeWidgetItem(QTreeWidgetItem *l) const
    {
        Q_UNUSED(l);
    }

public:
    QString _label, _toolTip, _category, _descr, _comment;
};

class AlertItemPrivate : public Trans::MultiLingualClass<AlertValueBook>
{
public:
    AlertItemPrivate(AlertItem *parent) :
        _id(-1),
        _valid(true), _modified(false),
        _editable(true), _overrideRequiresUserComment(false),
        _mustBeRead(false), _remindAllowed(false),
        _viewType(AlertItem::NonBlockingAlert),
        _contentType(AlertItem::ApplicationNotification),
        _priority(AlertItem::Medium),
        q(parent)
    {}

    ~AlertItemPrivate() {}

    QString viewTypeToXml()
    {
        switch (_viewType) {
        case AlertItem::BlockingAlert: return "blocking";
        case AlertItem::NonBlockingAlert: return "nonblocking";
        }
        return QString::null;
    }

    QString contentTypeToXml()
    {
        switch (_contentType) {
        case AlertItem::ApplicationNotification: return "applicationNotification";
        case AlertItem::PatientCondition: return "patientCondition";
        case AlertItem::UserNotification: return "userNotification";
        }
        return QString::null;
    }

    QString priorityToXml()
    {
        switch (_priority) {
        case AlertItem::High: return "high";
        case AlertItem::Medium: return "medium";
        case AlertItem::Low: return "low";
        }
        return QString::null;
    }

    void viewTypeFromXml(const QString &xml)
    {
        _viewType = AlertItem::BlockingAlert;
        if (xml.compare("static", Qt::CaseInsensitive)==0) {
            _viewType = AlertItem::NonBlockingAlert;
        }
    }

    void contentTypeFromXml(const QString &xml)
    {
        _contentType = AlertItem::PatientCondition;
        if (xml.compare("applicationNotification", Qt::CaseInsensitive)==0) {
            _contentType = AlertItem::ApplicationNotification;
        } else if (xml.compare("userNotification", Qt::CaseInsensitive)==0) {
            _contentType = AlertItem::UserNotification;
        }
    }

    void priorityFromXml(const QString &xml)
    {
        _priority = AlertItem::Low;
        if (xml.compare("high", Qt::CaseInsensitive)==0) {
            _priority = AlertItem::High;
        } else if (xml.compare("medium", Qt::CaseInsensitive)==0) {
            _priority = AlertItem::Medium;
        }
    }

    void feedItemWithXmlDescription()
    {
        _id = -1;
        _uid = descr.data(AlertXmlDescription::Uuid).toString();
        _pass = descr.data(AlertXmlDescription::CryptedPass).toString();
        _themedIcon = descr.data(AlertXmlDescription::GeneralIcon).toString();
        _css = descr.data(AlertXmlDescription::StyleSheet).toString();
        _valid = descr.data(AlertXmlDescription::Validity).toBool();
        _overrideRequiresUserComment = descr.data(AlertXmlDescription::OverrideRequiresUserComment).toBool();
        _mustBeRead = descr.data(AlertXmlDescription::MustBeRead).toBool();
        _remindAllowed = descr.data(AlertXmlDescription::RemindLater).toBool();
        _editable = descr.data(AlertXmlDescription::Editable).toBool();
        viewTypeFromXml(descr.data(AlertXmlDescription::ViewType).toString());
        contentTypeFromXml(descr.data(AlertXmlDescription::ContentType).toString());
        priorityFromXml(descr.data(AlertXmlDescription::Priority).toString());
        _creationDate = QDateTime::fromString(descr.data(AlertXmlDescription::CreationDate).toString(), Qt::ISODate);
        _update = QDateTime::fromString(descr.data(AlertXmlDescription::LastModificationDate).toString(), Qt::ISODate);
        _packUid = descr.data(AlertXmlDescription::PackUid).toString();

        foreach(const QString &l, descr.availableLanguages()) {
            q->setLabel(descr.data(Internal::AlertXmlDescription::Label, l).toString(), l);
            q->setToolTip(descr.data(Internal::AlertXmlDescription::ToolTip, l).toString(), l);
            q->setCategory(descr.data(Internal::AlertXmlDescription::Category, l).toString(), l);
            q->setDescription(descr.data(Internal::AlertXmlDescription::ShortDescription, l).toString(), l);
            q->setComment(descr.data(Internal::AlertXmlDescription::Comment, l).toString(), l);
        }
    }

    QString categoryForTreeWiget() const {return QString::null;}

    bool validationsContainsValidatedUuid(const QString &uuid)
    {
        for(int i=0; i< _validations.count(); ++i) {
            const AlertValidation &val = _validations.at(i);
            if (val.validatedUid().compare(uuid, Qt::CaseInsensitive)==0)
                return true;
        }
        return false;
    }

public:
    QString _uid, _packUid, _pass, _themedIcon, _css, _extraXml;
    int _id;
    bool _valid, _modified, _editable, _overrideRequiresUserComment, _mustBeRead, _remindAllowed;
    AlertItem::ViewType _viewType;
    AlertItem::ContentType _contentType;
    AlertItem::Priority _priority;
    QHash<int, QVariant> _db;
    QDateTime _creationDate, _update;
    AlertXmlDescription descr;

    QVector<AlertRelation> _relations;
    QVector<AlertScript> _scripts;
    QVector<AlertTiming> _timings;
    QVector<AlertValidation> _validations;
    AlertRelation _nullRelation;
    AlertScript _nullScript;
    AlertTiming _nullTiming;
    AlertValidation _nullValidation;

private:
    AlertItem *q;
};

}
}

AlertItem::AlertItem() :
    d(new Internal::AlertItemPrivate(this))
{
    if (WarnAlertItemConstructionDestruction)
        qWarning() << "AlertItem" << d->_uid << d;
}

AlertItem::AlertItem(const AlertItem &cp) :
    d(new Internal::AlertItemPrivate(*cp.d))
{
    if (WarnAlertItemConstructionDestruction)
        qWarning() << "AlertItem(cp)" << d->_uid << d;
}

AlertItem &AlertItem::operator=(const AlertItem &cp)
{
    if (d) {
        if (cp.d == d)
           return *this;
        delete d;
    }
    d = new Internal::AlertItemPrivate(*cp.d);
    if (WarnAlertItemConstructionDestruction)
        qWarning() << "AlertItem =()" << d->_uid << d;
    return *this;
}

AlertItem::~AlertItem()
{
    if (WarnAlertItemConstructionDestruction)
        qWarning() << "~AlertItem" << d->_uid << d;
    if (d)
        delete d;
    d = 0;
}

void AlertItem::setDb(int ref, const QVariant &value)
{
    if (!value.toString().isEmpty())
        d->_db.insert(ref, value);
}

QVariant AlertItem::db(int ref) const
{
    return d->_db.value(ref, QVariant());
}

void AlertItem::setValidity(bool isValid)
{
    d->_valid = isValid;
}

bool AlertItem::isValid() const
{
    return d->_valid;
}

bool AlertItem::isModified() const
{
    if (d->_modified)
        return true;
    for(int i=0; i < d->_timings.count(); ++i) {
        if (d->_timings.at(i).isModified())
            return true;
    }
    for(int i=0; i < d->_relations.count(); ++i) {
        if (d->_relations.at(i).isModified())
            return true;
    }
    for(int i=0; i < d->_scripts.count(); ++i) {
        if (d->_scripts.at(i).isModified())
            return true;
    }
    for(int i=0; i < d->_validations.count(); ++i) {
        if (d->_relations.at(i).isModified())
            return true;
    }
    return false;
}

void AlertItem::setModified(bool modified)
{
    d->_modified = modified;
    for(int i=0; i < d->_timings.count(); ++i) {
        d->_timings[i].setModified(modified);
    }
    for(int i=0; i < d->_relations.count(); ++i) {
        d->_relations[i].setModified(modified);
    }
    for(int i=0; i < d->_scripts.count(); ++i) {
        d->_scripts[i].setModified(modified);
    }
    for(int i=0; i < d->_validations.count(); ++i) {
        d->_validations[i].setModified(modified);
    }
}

QString AlertItem::uuid() const
{
    return d->_uid;
}

void AlertItem::setUuid(const QString &uid) const
{
    d->_uid = uid;
}

QString AlertItem::packUid() const
{
    return d->_packUid;
}

void AlertItem::setPackUid(const QString &uid) const
{
    d->_packUid = uid;
}

QString AlertItem::cryptedPassword() const
{
    return d->_pass;
}

void AlertItem::setCryptedPassword(const QString &pass)
{
    d->_pass = pass;
}

QString AlertItem::label(const QString &lang) const
{
    Internal::AlertValueBook *v = d->getLanguage(lang);
    if (!v) {
        v = d->getLanguage(Trans::Constants::ALL_LANGUAGE);
        if (!v) {
            v = d->getLanguage("en");
            if (!v)
                return QString::null;
        }
    }
    return v->_label;
}

QString AlertItem::toolTip(const QString &lang) const
{
    Internal::AlertValueBook *v = d->getLanguage(lang);
    if (!v) {
        v = d->getLanguage(Trans::Constants::ALL_LANGUAGE);
        if (!v) {
            v = d->getLanguage("en");
            if (!v)
                return QString::null;
        }
    }
    return v->_toolTip;
}

QString AlertItem::category(const QString &lang) const
{
    Internal::AlertValueBook *v = d->getLanguage(lang);
    if (!v) {
        v = d->getLanguage(Trans::Constants::ALL_LANGUAGE);
        if (!v) {
            v = d->getLanguage("en");
            if (!v)
                return QString::null;
        }
    }
    return v->_category;
}

QString AlertItem::description(const QString &lang) const
{
    Internal::AlertValueBook *v = d->getLanguage(lang);
    if (!v) {
        v = d->getLanguage(Trans::Constants::ALL_LANGUAGE);
        if (!v) {
            v = d->getLanguage("en");
            if (!v)
                return QString::null;
        }
    }
    return v->_descr;
}

QString AlertItem::comment(const QString &lang) const
{
    Internal::AlertValueBook *v = d->getLanguage(lang);
     if (!v) {
         v = d->getLanguage(Trans::Constants::ALL_LANGUAGE);
         if (!v) {
             v = d->getLanguage("en");
             if (!v)
                 return QString::null;
         }
     }
     return v->_comment;
}

void AlertItem::setLabel(const QString &txt, const QString &lang)
{
    Internal::AlertValueBook *v = 0;
    QString l = lang;
    if (l.isEmpty())
        l = Trans::Constants::ALL_LANGUAGE;
    if (d->hasLanguage(l))
        v = d->getLanguage(l);
    else
        v = d->createLanguage(l);
    v->_label = txt;
}

void AlertItem::setToolTip(const QString &txt, const QString &lang)
{
    Internal::AlertValueBook *v = 0;
    QString l = lang;
    if (l.isEmpty())
        l = Trans::Constants::ALL_LANGUAGE;
    if (d->hasLanguage(l))
        v = d->getLanguage(l);
    else
        v = d->createLanguage(l);
    v->_toolTip= txt;
}

void AlertItem::setCategory(const QString &txt, const QString &lang)
{
    Internal::AlertValueBook *v = 0;
    QString l = lang;
    if (l.isEmpty())
        l = Trans::Constants::ALL_LANGUAGE;
    if (d->hasLanguage(l))
        v = d->getLanguage(l);
    else
        v = d->createLanguage(l);
    v->_category = txt;
}

void AlertItem::setDescription(const QString &txt, const QString &lang)
{
    Internal::AlertValueBook *v = 0;
    QString l = lang;
    if (l.isEmpty())
        l = Trans::Constants::ALL_LANGUAGE;
    if (d->hasLanguage(l))
        v = d->getLanguage(l);
    else
        v = d->createLanguage(l);
    v->_descr = txt;
}

void AlertItem::setComment(const QString &txt, const QString &lang)
{
    Internal::AlertValueBook *v = 0;
    QString l = lang;
    if (l.isEmpty())
        l = Trans::Constants::ALL_LANGUAGE;
    if (d->hasLanguage(l))
        v = d->getLanguage(l);
    else
        v = d->createLanguage(l);
    v->_comment = txt;
}

QStringList AlertItem::availableLanguages() const
{
    return d->languages();
}

void AlertItem::removeAllLanguages()
{
    d->clear();
}

AlertItem::ViewType AlertItem::viewType() const
{
    return d->_viewType;
}

AlertItem::ContentType AlertItem::contentType() const
{
    return d->_contentType;
}

AlertItem::Priority AlertItem::priority() const
{
    return d->_priority;
}

QString AlertItem::priorityToString() const
{
    switch (d->_priority) {
    case High: return tkTr(Trans::Constants::HIGH);
    case Medium: return tkTr(Trans::Constants::MEDIUM);
    case Low: return tkTr(Trans::Constants::LOW);
    }
    return QString::null;
}

bool AlertItem::isOverrideRequiresUserComment() const
{
    return d->_overrideRequiresUserComment;
}

bool AlertItem::mustBeRead() const
{
    return d->_mustBeRead;
}

bool AlertItem::isRemindLaterAllowed() const
{
    return d->_remindAllowed;
}

bool AlertItem::isEditable() const
{
    return d->_editable;
}

void AlertItem::setViewType(AlertItem::ViewType type)
{
    d->_viewType = type;
}

void AlertItem::setContentType(AlertItem::ContentType content)
{
    d->_contentType = content;
}

void AlertItem::setPriority(AlertItem::Priority priority)
{
    d->_priority = priority;
}

void AlertItem::setOverrideRequiresUserComment(bool required)
{
    d->_overrideRequiresUserComment = required;
}

void AlertItem::setMustBeRead(bool mustberead)
{
    d->_mustBeRead = mustberead;
}

void AlertItem::setRemindLaterAllowed(bool allowed)
{
    d->_remindAllowed = allowed;
}

void AlertItem::setEditable(bool editable)
{
    d->_editable = editable;
}

QDateTime AlertItem::creationDate() const
{
    return d->_creationDate;
}

void AlertItem::setCreationDate(const QDateTime &dt)
{
    d->_creationDate = QDateTime(dt.date(), QTime(dt.time().hour(), dt.time().minute(), dt.time().second()));
}

QDateTime AlertItem::lastUpdate() const
{
    return d->_update;
}

void AlertItem::setLastUpdate(const QDateTime &dt)
{
    d->_update = QDateTime(dt.date(), QTime(dt.time().hour(), dt.time().minute(), dt.time().second()));
}

QString AlertItem::themedIcon() const
{
    return d->_themedIcon;
}

void AlertItem::setThemedIcon(const QString &icon)
{
    d->_themedIcon = icon;
}

QString AlertItem::styleSheet() const
{
    return d->_css;
}

void AlertItem::setStyleSheet(const QString &css)
{
    d->_css = css;
}

QString AlertItem::priorityBackgroundColor() const
{
    QString background;
    switch (d->_priority) {
    case AlertItem::Low: background = Core::Constants::COLOR_BACKGROUND_ALERT_LOW; break;
    case AlertItem::Medium: background = Core::Constants::COLOR_BACKGROUND_ALERT_MEDIUM; break;
    case AlertItem::High: background = Core::Constants::COLOR_BACKGROUND_ALERT_HIGH; break;
    }
    return background;
}

QIcon AlertItem::priorityBigIcon(Priority priority)
{
    QString icon;
    switch (priority) {
    case AlertItem::High: icon = Core::Constants::ICONCRITICAL; break;
    case AlertItem::Medium: icon = Core::Constants::ICONWARNING; break;
    case AlertItem::Low: icon = Core::Constants::ICONINFORMATION; break;
    }
    return theme()->icon(icon, Core::ITheme::BigIcon).pixmap(64,64);
}

QIcon AlertItem::priorityBigIcon() const
{
    return priorityBigIcon(d->_priority);
}

static const char *CSS = "text-indent:5px;padding:5px;font-weight:bold;font-size:large;background:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0.464 rgba(255, 255, 176, 149), stop:1 rgba(255, 255, 255, 0))";

QString AlertItem::htmlToolTip(bool showCategory) const
{
    QString toolTip;
    QString header;
    if (showCategory)
        header = QString("<table border=0 margin=0 width=100%>"
                         "<tr>"
                         "<td valign=middle width=70% style=\"%1\">%2</td>"
                         "<td valign=middle align=center style=\"font-size:large;font-weight:bold;background-color:%4;text-transform:uppercase\">%5</td>"
                         "</tr>"
                         "<tr>"
                         "<td colspan=2 style=\"font-weight:bold;color:#101010;padding-left:10px\">%3</td>"
                         "</tr>"
                         "</table>")
                .arg(CSS)
                .arg(category())
                .arg(label())
                .arg(priorityBackgroundColor())
                .arg(priorityToString())
                ;
    else
        header = QString("<table border=0 margin=0 width=100%>"
                         "<tr>"
                         "<td valign=middle width=70% style=\"font-weight:bold\">%1</td>"
                         "<td valign=middle align=center style=\"font-size:large;font-weight:bold;background-color:%2;text-transform:uppercase\">%3</td>"
                         "</tr>"
                         "</table>")
                .arg(label())
                .arg(priorityBackgroundColor())
                .arg(priorityToString())
                ;

    QString descr;
    if (!description().isEmpty()) {
        descr += QString("<span style=\"color:black\">%1</span>"
                         "<hr align=center width=50% color=lightgray size=1>").arg(description());
    }

    QStringList related;
    for(int i = 0; i < relations().count(); ++i) {
        const AlertRelation &rel = relationAt(i);
        related += QString("%1").arg(rel.relationTypeToString());
    }

    QString content;
    if (!related.isEmpty())
        content += QString("<span style=\"font-size:small;color:#303030\">%1</span><br />").arg(related.join("<br />"));

    QStringList tim;
    for(int i =0; i < timings().count(); ++i) {
        AlertTiming &timing = timingAt(i);
        if (timing.isCycling()) {
            tim << QString(QApplication::translate("Alert::AlertItem", "Started on: %1<br />Cycling every: %2<br />Expires on: %3"))
                       .arg(timing.currentCycleStartDate().toString(QLocale().dateFormat()))
                       .arg(timing.cyclingDelayInDays())
                       .arg(timing.currentCycleExpirationDate().toString(QLocale().dateFormat()));
        } else {
            tim << QString(QApplication::translate("Alert::AlertItem", "Started on: %1<br />Expires on: %2"))
                       .arg(timing.start().toString(QLocale().dateFormat()))
                       .arg(timing.expiration().toString(QLocale().dateFormat()));
        }
    }
    if (!tim.isEmpty())
        content += QString("<span style=\"font-size:small;color:#303030\">%1</span>").arg(tim.join("<br />"));

    if (!packUid().isEmpty()) {
        AlertPackDescription pack = alertCore().getAlertPackDescription(packUid());
        content += QString("<span style=\"font-size:small;color:#303030\">%1</span>")
                .arg(tkTr(Trans::Constants::FROM) + " " + pack.label());
    }
    toolTip = QString("%1"
                      "<table border=0 cellpadding=0 cellspacing=0 width=100%>"
                      "<tr><td style=\"padding-left:10px;\">%2</td></tr>"
                      "<tr><td align=center>%3</td></tr>"
                      "</table>")
            .arg(header)
            .arg(descr)
            .arg(content)
            ;
    return toolTip;
}

QString AlertItem::extraXml() const
{
    return d->_extraXml;
}

void AlertItem::setExtraXml(const QString &xml)
{
    d->_extraXml = xml;
}


void AlertItem::clearRelations()
{
    d->_modified = true;
    d->_relations.clear();
}

AlertRelation &AlertItem::relation(int id) const
{
    for(int i=0; i<d->_relations.count();++i) {
        if (d->_relations.at(i).id()==id)
            return d->_relations[i];
    }
    return d->_nullRelation;
}

QVector<AlertRelation> &AlertItem::relations() const
{
    return d->_relations;
}

AlertRelation &AlertItem::relationAt(int id) const
{
    if (IN_RANGE_STRICT_MAX(id, 0, d->_relations.count()))
        return d->_relations[id];
    return d->_nullRelation;
}

void AlertItem::addRelation(const AlertRelation &relation)
{
    d->_modified = true;
    d->_relations << relation;
}

void AlertItem::clearTimings()
{
    d->_modified = true;
    d->_timings.clear();
}

AlertTiming &AlertItem::timing(int id) const
{
    for(int i=0; i<d->_timings.count();++i) {
        if (d->_timings.at(i).id()==id)
            return d->_timings[i];
    }
    return d->_nullTiming;
}

QVector<AlertTiming> &AlertItem::timings() const
{
    return d->_timings;
}

AlertTiming &AlertItem::timingAt(int id) const
{
    if (IN_RANGE_STRICT_MAX(id, 0, d->_timings.count()))
        return d->_timings[id];
    return d->_nullTiming;
}

void AlertItem::addTiming(const AlertTiming &timing)
{
    d->_modified = true;
    d->_timings.append(timing);
}

void AlertItem::clearScripts()
{
    d->_modified = true;
    d->_scripts.clear();
}

AlertScript &AlertItem::script(int id) const
{
    for(int i=0; i<d->_scripts.count();++i) {
        if (d->_scripts.at(i).id()==id)
            return d->_scripts[i];
    }
    return d->_nullScript;
}

AlertScript &AlertItem::scriptType(AlertScript::ScriptType type) const
{
    for(int i=0; i < d->_scripts.count(); ++i) {
        AlertScript &script = d->_scripts[i];
        if (script.type()==type)
            return script;
    }
    return d->_nullScript;
}

QVector<AlertScript> &AlertItem::scripts() const
{
    return d->_scripts;
}

AlertScript &AlertItem::scriptAt(int id) const
{
    if (IN_RANGE_STRICT_MAX(id, 0, d->_scripts.count()))
        return d->_scripts[id];
    return d->_nullScript;
}

void AlertItem::addScript(const AlertScript &script)
{
    d->_modified = true;
    d->_scripts << script;
}

void AlertItem::setScripts(const QVector<AlertScript> &scripts)
{
    d->_modified = true;
    d->_scripts.clear();
    d->_scripts = scripts;
}


bool AlertItem::setRemindLater()
{
    alertCore().removeAlert(*this);
    return true;
}


bool AlertItem::validateAlertWithCurrentUserAndConfirmationDialog()
{
    bool yes = Utils::yesNoMessageBox(
                QApplication::translate("Alert::AlertItem", "Alert validation."),
                QApplication::translate("Alert::AlertItem",
                                        "You are about to validate this alert:<br />"
                                        "<b>%1</b><br /><br />"
                                        "Do you really want to validate this alert ?")
                .arg(label()), "",
                QApplication::translate("Alert::AlertItem", "Alert validation."));
    if (yes) {
        QString validator;
        user() ? validator = user()->uuid() : validator = "UnknownUser";
        return validateAlert(validator, false, "", QDateTime::currentDateTime());
    }
    return false;
}


bool AlertItem::validateAlert(const QString &validatorUid, bool override, const QString &overrideComment, const QDateTime &dateOfValidation)
{
    AlertValidation val;
    val.setDateOfValidation(QDateTime::currentDateTime());
    val.setValidatorUuid(validatorUid);
    val.setAccepted(!override);
    val.setOverriden(override);
    val.setUserComment(overrideComment);
    val.setDateOfValidation(dateOfValidation);

    if (d->_relations.count()  > 0) {
        const AlertRelation &rel = d->_relations.at(0);
        switch (rel.relatedTo())
        {
        case AlertRelation::RelatedToPatient:
        case AlertRelation::RelatedToAllPatients:
        {
            if (patient())
                val.setValidatedUuid(patient()->uuid());
            else if (!Utils::isReleaseCompilation())
                val.setValidatedUuid("patient1");
            break;
        }
        case AlertRelation::RelatedToFamily: // TODO: manage family
            break;
        case AlertRelation::RelatedToUser:
        case AlertRelation::RelatedToAllUsers:
        {
            if (user())
                val.setValidatedUuid(user()->uuid());
            else if (!Utils::isReleaseCompilation())
                val.setValidatedUuid("user1");
            break;
        }
        case AlertRelation::RelatedToUserGroup: // TODO: manage user groups
            break;
        case AlertRelation::RelatedToApplication:
        {
            val.setValidatedUuid(qApp->applicationName().toLower());
            break;
        }
        }
    }
    addValidation(val);
    alertCore().updateAlert(*this);
    return true;
}

bool AlertItem::isUserValidated() const
{
    if (d->_validations.count()==0)
        return false;

    if (d->_relations.count() > 0) {
        const AlertRelation &rel = d->_relations.at(0);
        switch (rel.relatedTo())
        {
        case AlertRelation::RelatedToPatient:
        case AlertRelation::RelatedToAllPatients:
        {
            if (patient())
                return d->validationsContainsValidatedUuid(patient()->uuid());
            else if (!Utils::isReleaseCompilation())
                return d->validationsContainsValidatedUuid("patient1");
            break;
        }
        case AlertRelation::RelatedToFamily: // TODO: manage family
            break;
        case AlertRelation::RelatedToUser:
        case AlertRelation::RelatedToAllUsers:
        {
            if (user())
                return d->validationsContainsValidatedUuid(user()->uuid());
            else if (!Utils::isReleaseCompilation())
                return d->validationsContainsValidatedUuid("user1");
            break;
        }
        case AlertRelation::RelatedToUserGroup: // TODO: manage user groups
            break;
        case AlertRelation::RelatedToApplication:
        {
            return d->validationsContainsValidatedUuid(qApp->applicationName().toLower());
        }
        }
    }
    LOG_ERROR_FOR("AlertItem", "No relation to link validation");
    return false;
}

void AlertItem::clearValidations()
{
    d->_modified = true;
    d->_validations.clear();
}

AlertValidation &AlertItem::validation(int id) const
{
    for(int i=0; i<d->_validations.count();++i) {
        if (d->_validations.at(i).id()==id)
            return d->_validations[i];
    }
    return d->_nullValidation;
}

QVector<AlertValidation> &AlertItem::validations() const
{
    return d->_validations;
}

AlertValidation &AlertItem::validationAt(int id) const
{
    if (IN_RANGE_STRICT_MAX(id, 0, d->_validations.count()))
        return d->_validations[id];
    return d->_nullValidation;
}

void AlertItem::addValidation(const AlertValidation &val)
{
    d->_modified = true;
    d->_validations << val;
}


bool AlertItem::operator==(const AlertItem &other) const
{
    if (other.uuid() != uuid())
        return false;

    if (other.cryptedPassword() != cryptedPassword() ||
            other.creationDate() != creationDate() ||
            other.lastUpdate() != lastUpdate() ||
            other.availableLanguages() != availableLanguages() ||
            other.viewType() != viewType() ||
            other.contentType() != contentType() ||
            other.priority() != priority() ||
            other.themedIcon() != themedIcon() ||
            other.styleSheet() != styleSheet() ||
            other.extraXml() != extraXml()
            )
        return false;

    if (other.relations().count() != relations().count() ||
            other.scripts().count() != scripts().count() ||
            other.validations().count() != validations().count() ||
            other.timings().count() != timings().count()
            )
        return false;

    for(int i = 0; i < d->_timings.count(); ++i) {
        const AlertTiming &first = d->_timings.at(i);
        bool ok = false;
        for(int j = 0; j < other.d->_timings.count(); ++j) {
            const AlertTiming &second = other.d->_timings.at(j);
            if (first == second) {
                ok = true;
                break;
            }
        }
        if (!ok)
            return false;
    }

    for(int i = 0; i < d->_relations.count(); ++i) {
        const AlertRelation &first = d->_relations.at(i);
        bool ok = false;
        for(int j = 0; j < other.d->_relations.count(); ++j) {
            const AlertRelation &second = other.d->_relations.at(j);
            if (first == second) {
                ok = true;
                break;
            }
        }
        if (!ok)
            return false;
    }

    for(int i = 0; i < d->_validations.count(); ++i) {
        const AlertValidation &first = d->_validations.at(i);
        bool ok = false;
        for(int j = 0; j < other.d->_validations.count(); ++j) {
            const AlertValidation &second = other.d->_validations.at(j);
            if (first == second) {
                ok = true;
                break;
            }
        }
        if (!ok)
            return false;
    }

    for(int i = 0; i < d->_scripts.count(); ++i) {
        const AlertScript &first = d->_scripts.at(i);
        bool ok = false;
        for(int j = 0; j < other.d->_scripts.count(); ++j) {
            const AlertScript &second = other.d->_scripts.at(j);
            if (first == second) {
                ok = true;
                break;
            }
        }
        if (!ok)
            return false;
    }
    return true;
}

bool AlertItem::operator!=(const AlertItem &other) const
{
    return !(operator==(other));
}

bool AlertItem::priorityLowerThan(const AlertItem &item1, const AlertItem &item2)
{
    return item1.priority() < item2.priority();
}

bool AlertItem::categoryLowerThan(const AlertItem &item1, const AlertItem &item2)
{
    return item1.category() < item2.category();
}

QString AlertItem::toXml() const
{
    d->descr.setData(Internal::AlertXmlDescription::Uuid, d->_uid);
    d->descr.setData(Internal::AlertXmlDescription::Validity, d->_valid);
    d->descr.setData(Internal::AlertXmlDescription::CreationDate, d->_creationDate);
    d->descr.setData(Internal::AlertXmlDescription::LastModificationDate, d->_update);
    d->descr.setData(Internal::AlertXmlDescription::CryptedPass, d->_pass);
    d->descr.setData(Internal::AlertXmlDescription::ViewType, d->viewTypeToXml());
    d->descr.setData(Internal::AlertXmlDescription::ContentType, d->contentTypeToXml());
    d->descr.setData(Internal::AlertXmlDescription::Priority, d->priorityToXml());
    d->descr.setData(Internal::AlertXmlDescription::OverrideRequiresUserComment, d->_overrideRequiresUserComment);
    d->descr.setData(Internal::AlertXmlDescription::MustBeRead, d->_mustBeRead);
    d->descr.setData(Internal::AlertXmlDescription::RemindLater, d->_remindAllowed);
    d->descr.setData(Internal::AlertXmlDescription::Editable, d->_editable);
    d->descr.setData(Internal::AlertXmlDescription::StyleSheet, d->_css);
    d->descr.setData(Internal::AlertXmlDescription::GeneralIcon, d->_themedIcon);
    d->descr.setData(Internal::AlertXmlDescription::PackUid, d->_packUid);

    foreach(const QString &l, availableLanguages()) {
        d->descr.setData(Internal::AlertXmlDescription::Label, label(l) , l);
        d->descr.setData(Internal::AlertXmlDescription::Category, category(l), l);
        d->descr.setData(Internal::AlertXmlDescription::ToolTip, toolTip(l), l);
        d->descr.setData(Internal::AlertXmlDescription::ShortDescription, description(l), l);
        d->descr.setData(Internal::AlertXmlDescription::Comment, comment(l), l);
    }

    QString xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
    xml += "<!DOCTYPE FreeMedForms>\n";
    xml += QString("<%1>\n").arg(::XML_ROOT_TAG);
    xml += d->descr.toXml();
    int n = d->_timings.count();
    if (n) {
        xml += QString("<%1>\n").arg(::XML_TIMING_ROOTTAG);
        for(int i=0; i < n; ++i) {
            xml += d->_timings.at(i).toXml();
        }
        xml += QString("</%1>\n").arg(::XML_TIMING_ROOTTAG);
    }
    n = d->_relations.count();
    if (n) {
        xml += QString("<%1>\n").arg(::XML_RELATED_ROOTTAG);
        for(int i=0; i < n; ++i) {
            xml += d->_relations.at(i).toXml();
        }
        xml += QString("</%1>\n").arg(::XML_RELATED_ROOTTAG);
    }
    n = d->_scripts.count();
    if (n) {
        xml += QString("<%1>\n").arg(::XML_SCRIPT_ROOTTAG);
        for(int i=0; i < n; ++i) {
            xml += d->_scripts.at(i).toXml();
        }
        xml += QString("</%1>\n").arg(::XML_SCRIPT_ROOTTAG);
    }
    n = d->_validations.count();
    if (n) {
        xml += QString("<%1>\n").arg(::XML_VALIDATION_ROOTTAG);
        for(int i=0; i < n; ++i) {
            xml += d->_validations.at(i).toXml();
        }
        xml += QString("</%1>\n").arg(::XML_VALIDATION_ROOTTAG);
    }
    if (!d->_extraXml.isEmpty())
        xml += QString("<%1>%2</%1>").arg(::XML_EXTRAXML_ROOTTAG).arg(d->_extraXml);
    xml += QString("</%1>\n").arg(::XML_ROOT_TAG);
    QDomDocument doc;
    QString err;
    int l, c;
    if (!doc.setContent(xml, &err, &l, &c))
        LOG_ERROR_FOR("AlertItem", tkTr(Trans::Constants::ERROR_1_LINE_2_COLUMN_3).arg(l).arg(c).arg(err));
    return doc.toString(2);
}

AlertItem &AlertItem::fromXml(const QString &xml)
{
    AlertItem *item = new AlertItem();
    QDomDocument doc;
    QString err;
    int l, c;
    if (!doc.setContent(xml, &err, &l, &c)) {
        LOG_ERROR_FOR("AlertItem", tkTr(Trans::Constants::ERROR_1_LINE_2_COLUMN_3).arg(l).arg(c).arg(err));
        return *item;
    }
    QDomElement root = doc.firstChildElement(::XML_ROOT_TAG);

    item->d->descr.fromDomElement(root.firstChildElement(::XML_DESCRIPTION_ROOTTAG));
    item->d->feedItemWithXmlDescription();

    QDomElement main = root.firstChildElement(::XML_TIMING_ROOTTAG);
    if (!main.isNull()) {
        QDomElement element = main.firstChildElement(::XML_TIMING_ELEMENTTAG);
        while (!element.isNull()) {
            AlertTiming timing = AlertTiming::fromDomElement(element);
            item->addTiming(timing);
            element = element.nextSiblingElement(::XML_TIMING_ELEMENTTAG);
        }
    }

    main = root.firstChildElement(::XML_RELATED_ROOTTAG);
    if (!main.isNull()) {
        QDomElement element = main.firstChildElement(::XML_RELATED_ELEMENTTAG);
        while (!element.isNull()) {
            AlertRelation rel = AlertRelation::fromDomElement(element);
            item->addRelation(rel);
            element = element.nextSiblingElement(::XML_RELATED_ELEMENTTAG);
        }
    }

    main = root.firstChildElement(::XML_VALIDATION_ROOTTAG);
    if (!main.isNull()) {
        QDomElement element = main.firstChildElement(::XML_VALIDATION_ELEMENTTAG);
        while (!element.isNull()) {
            AlertValidation val = AlertValidation::fromDomElement(element);
            item->addValidation(val);
            element = element.nextSiblingElement(::XML_VALIDATION_ELEMENTTAG);
        }
    }

    main = root.firstChildElement(::XML_SCRIPT_ROOTTAG);
    if (!main.isNull()) {
        QDomElement element = main.firstChildElement(::XML_SCRIPT_ELEMENTTAG);
        while (!element.isNull()) {
            AlertScript scr = AlertScript::fromDomElement(element);
            item->addScript(scr);
            element = element.nextSiblingElement(::XML_SCRIPT_ELEMENTTAG);
        }
    }

    int begin = xml.indexOf(QString("<%1>").arg(::XML_EXTRAXML_ROOTTAG), Qt::CaseInsensitive);
    if (begin > 0) {
        begin = xml.indexOf("<", begin+2);
        int end = xml.indexOf(::XML_EXTRAXML_ROOTTAG, begin);
        end = xml.lastIndexOf(">", end) + 1;
        if (end > begin) {
            item->d->_extraXml = xml.mid(begin, end-begin);
        }
    }

    item->setModified(false);
    return *item;
}

AlertTiming::AlertTiming() :
    _id(-1), _nCycle(0), _currentCycle(-1),
    _delayInMins(0), _valid(true), _isCycle(false), _modified(false)
{}

AlertTiming::AlertTiming(const QDateTime &start, const QDateTime &expirationDate) :
    _id(-1), _nCycle(0), _currentCycle(-1),
    _start(QDateTime(start.date(), QTime(start.time().hour(), start.time().minute(), start.time().second()))),
    _end(QDateTime(expirationDate.date(), QTime(expirationDate.time().hour(), expirationDate.time().minute(), expirationDate.time().second()))),
    _delayInMins(0), _valid(true), _isCycle(false), _modified(true)
{}

AlertTiming::AlertTiming(const AlertTiming &copy) :
    _id(copy._id), _nCycle(copy._nCycle), _currentCycle(copy._currentCycle),
    _start(copy._start), _end(copy._end), _next(copy._next),
    _delayInMins(copy._delayInMins),
    _valid(copy._valid), _isCycle(copy._isCycle),
    _modified(copy._modified),
    _cycleStartDate(copy._cycleStartDate), _cycleExpirationDate(copy._cycleExpirationDate)
{}

void AlertTiming::cyclingDelay(qlonglong *mins, qlonglong *hours, qlonglong *days, qlonglong *weeks, qlonglong *months, qlonglong *years, qlonglong *decades) const
{
    qlonglong tmp = _delayInMins;
    *decades = cyclingDelayInDecades();
    tmp -= (*decades)*60*24*365.25*10;

    *years = tmp/60/24/365.25;
    tmp -= (*years)*60*24*365.25;

    *months = tmp/60/24/30;
    tmp -= (*months)*60*24*30;

    *weeks = tmp/60/24/7;
    tmp -= (*weeks)*60*24*7;

    *days = tmp/60/24;
    tmp -= (*days)*60/24;

    *hours = tmp/60;
    tmp -= (*hours)*60;

    *mins = tmp;
}


void AlertTiming::cyclingDelayPeriodModulo(int *period, int *mod) const
{
    Q_ASSERT(period);
    Q_ASSERT(mod);
    if (!period || !mod)
        return;
    QList<int> ops;
    ops << 60 << 60*24 << 60*24*7 << 60*24*30 << int(60*24*365.25) << int(60*24*365.25*10);

    *period = -1;
    *mod = 0;
    for(int i = 0; i < ops.count(); ++i) {
        if ((_delayInMins % ops.at(i)) == 0) {
            *period = i;
        }
    }
    if (*period != -1)
        *mod = _delayInMins / ops.at(*period);
    switch (*period) {
    case -1:
        *period = Trans::Constants::Time::Minutes;
        break;
    case 0:
        *period = Trans::Constants::Time::Hours;
        break;
    case 1:
        *period = Trans::Constants::Time::Days;
        break;
    case 2:
        *period = Trans::Constants::Time::Weeks;
        break;
    case 3:
        *period = Trans::Constants::Time::Months;
        break;
    case 4:
        *period = Trans::Constants::Time::Year;
        break;
    case 5:
        *period = Trans::Constants::Time::Decade;
        break;
    }
}


void AlertTiming::setNumberOfCycles(int n)
{
    _modified=true;
    _nCycle=n;
    _isCycle=(n>0);
    computeCycle();
}

void AlertTiming::computeCycle()
{
    if (!_isCycle || _nCycle <= 0 || _delayInMins <= 0) {
        _nCycle = 0;
        _isCycle = false;
        return;
    }

    if (_currentCycle > 0 &&
            _cycleStartDate.isValid() && !_cycleStartDate.isNull() &&
            _cycleExpirationDate.isValid() && !_cycleExpirationDate.isNull()) {
        return;
    }

    _cycleStartDate = QDateTime();
    _cycleExpirationDate = QDateTime();
    _currentCycle = 0;

    if (!_start.isValid() || _start.isNull()) {
        return;
    }

    _currentCycle = int((_start.secsTo(QDateTime::currentDateTime())/60) / _delayInMins);
    _cycleStartDate = _start.addSecs(_delayInMins*60*_currentCycle);
    _cycleExpirationDate = _start.addSecs(_delayInMins*60*(_currentCycle+1));
}

QString AlertTiming::toXml() const
{
    QDomDocument doc;
    QDomElement el = doc.createElement(::XML_TIMING_ELEMENTTAG);
    el.setAttribute("id", _id);
    el.setAttribute("valid", _valid ? "true" : "false");
    el.setAttribute("start", _start.toString(Qt::ISODate));
    el.setAttribute("end", _end.toString(Qt::ISODate));
    el.setAttribute("isCycle", _isCycle ? "true" : "false");
    if (_isCycle) {
        el.setAttribute("ncycle", _nCycle);
        el.setAttribute("delayInMin", _delayInMins);
        el.setAttribute("next", _next.toString(Qt::ISODate));
    }
    doc.appendChild(el);
    return doc.toString();
}

AlertTiming &AlertTiming::fromXml(const QString &xml)
{
    QDomDocument doc;
    int line = 0;
    int col = 0;
    QString error;
    if (!doc.setContent(xml, &error, &line, &col)) {
        LOG_ERROR_FOR("AlertTiming", tkTr(Trans::Constants::ERROR_1_LINE_2_COLUMN_3).arg(line).arg(col).arg(error));
        AlertTiming *t = new AlertTiming;
        return *t;
    }
    QDomElement el = doc.documentElement();
    if (el.tagName().compare(::XML_TIMING_ELEMENTTAG, Qt::CaseInsensitive) != 0)
        el = el.firstChildElement(::XML_TIMING_ELEMENTTAG);
    if (el.isNull()) {
        LOG_ERROR_FOR("AlertTiming", tkTr(Trans::Constants::XML_WRONG_NUMBER_OF_TAG_1).arg(::XML_TIMING_ELEMENTTAG));
        AlertTiming *t = new AlertTiming;
        return *t;
    }
    return fromDomElement(el);
}

AlertTiming &AlertTiming::fromDomElement(const QDomElement &element)
{
    AlertTiming *timing = new AlertTiming;
    if (element.tagName().compare(::XML_TIMING_ELEMENTTAG, Qt::CaseInsensitive)!=0)
        return *timing;
    if (!element.attribute("id").isEmpty())
        timing->setId(element.attribute("id").toInt());
    timing->setValid(element.attribute("valid").compare("true",Qt::CaseInsensitive)==0);
    if (!element.attribute("start").isEmpty())
        timing->setStart(QDateTime::fromString(element.attribute("start"), Qt::ISODate));
    if (!element.attribute("end").isEmpty())
        timing->setEnd(QDateTime::fromString(element.attribute("end"), Qt::ISODate));
    if (!element.attribute("next").isEmpty())
        timing->setNextDate(QDateTime::fromString(element.attribute("next"), Qt::ISODate));
    timing->setCycling(element.attribute("isCycle").compare("true",Qt::CaseInsensitive)==0);
    timing->setCyclingDelayInMinutes(element.attribute("delayInMin").toLongLong());
    timing->setNumberOfCycles(element.attribute("ncycle").toInt());
    timing->setModified(false);
    return *timing;
}

bool AlertTiming::operator==(const AlertTiming &other) const
{
    return _id == other._id &&
            _nCycle == other._nCycle &&
            _valid == other._valid &&
            _isCycle == other._isCycle &&
            _modified == other._modified &&
            _delayInMins == other._delayInMins &&
            _start == other._start &&
            _end == other._end &&
            _next == other._next;
}

QString AlertScript::typeToString(ScriptType type)
{
    switch (type) {
    case CheckValidityOfAlert: return QApplication::translate("Alert::AlertScript", "Check alert validity");
    case CyclingStartDate: return QApplication::translate("Alert::AlertScript", "Compute cycling starting date");
    case OnAboutToShow: return QApplication::translate("Alert::AlertScript", "About to show alert");
    case DuringAlert: return QApplication::translate("Alert::AlertScript", "During the alert presentation");
    case OnAboutToValidate: return QApplication::translate("Alert::AlertScript", "About to validate the alert");
    case OnAboutToOverride: return QApplication::translate("Alert::AlertScript", "On alert about to be overridden");
    case OnOverridden: return QApplication::translate("Alert::AlertScript", "On alert overridden");
    case OnPatientAboutToChange: return QApplication::translate("Alert::AlertScript", "On patient about to change");
    case OnUserAboutToChange: return QApplication::translate("Alert::AlertScript", "On user about to change");
    case OnEpisodeAboutToSave: return QApplication::translate("Alert::AlertScript", "On episode about to save");
    case OnEpisodeLoaded: return QApplication::translate("Alert::AlertScript", "On episode loaded");
    case OnRemindLater: return QApplication::translate("Alert::AlertScript", "On remind later requested");
    }

    return QString::null;
}

QString AlertScript::typeToXml(ScriptType type)
{
    switch (type) {
    case CheckValidityOfAlert: return "check";
    case CyclingStartDate: return "cyclingStartDate";
    case OnAboutToShow: return "onabouttoshow";
    case DuringAlert: return "during";
    case OnAboutToValidate: return "onabouttovalidate";
    case OnAboutToOverride: return "onabouttooverride";
    case OnOverridden: return "onoverride";
    case OnPatientAboutToChange: return "onpatientabouttochange";
    case OnUserAboutToChange: return "onuserabouttochange";
    case OnEpisodeAboutToSave: return "onepisodeabouttosave";
    case OnEpisodeLoaded: return "onepisodeloaded";
    case OnRemindLater: return "onremindlater";
    }
    return QString::null;
}

AlertScript::ScriptType AlertScript::typeFromXml(const QString &xml)
{
    if (xml.compare("check", Qt::CaseInsensitive)==0)
        return CheckValidityOfAlert;
    else if (xml.compare("cyclingStartDate", Qt::CaseInsensitive)==0)
        return CyclingStartDate;
    else if (xml.compare("onabouttoshow", Qt::CaseInsensitive)==0)
        return OnAboutToShow;
    else if (xml.compare("onabouttovalidate", Qt::CaseInsensitive)==0)
        return OnAboutToValidate;
    else if (xml.compare("during", Qt::CaseInsensitive)==0)
        return DuringAlert;
    else if (xml.compare("onabouttooverride", Qt::CaseInsensitive)==0)
        return OnAboutToOverride;
    else if (xml.compare("onoverride", Qt::CaseInsensitive)==0)
        return OnOverridden;
    else if (xml.compare("onpatientabouttochange", Qt::CaseInsensitive)==0)
        return OnPatientAboutToChange;
    else if (xml.compare("onuserabouttochange", Qt::CaseInsensitive)==0)
        return OnUserAboutToChange;
    else if (xml.compare("onepisodeabouttosave", Qt::CaseInsensitive)==0)
        return OnEpisodeAboutToSave;
    else if (xml.compare("onepisodeloaded", Qt::CaseInsensitive)==0)
        return OnEpisodeLoaded;
    else if (xml.compare("onremindlater", Qt::CaseInsensitive)==0)
        return OnRemindLater;
    return CheckValidityOfAlert;
}

QString AlertScript::toXml() const
{
    QDomDocument doc;
    QDomElement el = doc.createElement(::XML_SCRIPT_ELEMENTTAG);
    el.setAttribute("id", _id);
    el.setAttribute("valid", _valid ? "true" : "false");
    el.setAttribute("type", typeToXml(_type));
    el.setAttribute("uid", _uid);
    QDomText text = doc.createTextNode(_script);
    el.appendChild(text);
    doc.appendChild(el);
    return doc.toString();
}


AlertScript &AlertScript::fromXml(const QString &xml)
{
    QDomDocument doc;
    int line = 0;
    int col = 0;
    QString error;
    if (!doc.setContent(xml, &error, &line, &col)) {
        LOG_ERROR_FOR("AlertScript", tkTr(Trans::Constants::ERROR_1_LINE_2_COLUMN_3).arg(line).arg(col).arg(error));
        AlertScript *script = new AlertScript;
        return *script;
    }
    QDomElement el = doc.documentElement();
    if (el.tagName().compare(::XML_SCRIPT_ELEMENTTAG, Qt::CaseInsensitive) != 0)
        el = el.firstChildElement(::XML_SCRIPT_ELEMENTTAG);
    if (el.isNull()) {
        LOG_ERROR_FOR("AlertScript", tkTr(Trans::Constants::XML_WRONG_NUMBER_OF_TAG_1).arg(::XML_SCRIPT_ELEMENTTAG));
        AlertScript *script = new AlertScript;
        return *script;
    }
    return fromDomElement(el);
}


AlertScript &AlertScript::fromDomElement(const QDomElement &element)
{
    AlertScript *script = new AlertScript;
    if (element.tagName().compare(::XML_SCRIPT_ELEMENTTAG, Qt::CaseInsensitive)!=0)
        return *script;
    if (!element.attribute("id").isEmpty())
        script->setId(element.attribute("id").toInt());
    script->setUuid(element.attribute("uid"));
    script->setValid(element.attribute("valid").compare("true", Qt::CaseInsensitive) == 0);
    script->setType(typeFromXml(element.attribute("type")));
    script->setScript(element.text());
    script->setModified(false);
    return *script;
}

bool AlertScript::operator<(const AlertScript &script) const
{
    return this->type() < script.type();
}

bool AlertScript::operator==(const AlertScript &other) const
{
    return _id == other._id &&
            _modified == other._modified &&
            _valid == other._valid &&
            _type == other._type &&
            _uid == other._uid &&
            _script == other._script;
}

QString AlertValidation::toXml() const
{
    QDomDocument doc;
    QDomElement el = doc.createElement(::XML_VALIDATION_ELEMENTTAG);
    el.setAttribute("id", _id);
    el.setAttribute("overridden", _overridden ? "true" : "false");
    el.setAttribute("validator", _validator);
    el.setAttribute("comment", _userComment);
    el.setAttribute("dt", _date.toString(Qt::ISODate));
    el.setAttribute("validated", _validated);
    doc.appendChild(el);
    return doc.toString();
}

AlertValidation &AlertValidation::fromXml(const QString &xml)
{
    QDomDocument doc;
    int line = 0;
    int col = 0;
    QString error;
    if (!doc.setContent(xml, &error, &line, &col)) {
        LOG_ERROR_FOR("AlertValidation", tkTr(Trans::Constants::ERROR_1_LINE_2_COLUMN_3).arg(line).arg(col).arg(error));
        AlertValidation *val = new AlertValidation();
        return *val;
    }
    QDomElement el = doc.documentElement();
    if (el.tagName().compare(::XML_VALIDATION_ELEMENTTAG, Qt::CaseInsensitive) != 0)
        el = el.firstChildElement(::XML_VALIDATION_ELEMENTTAG);
    if (el.isNull()) {
        LOG_ERROR_FOR("AlertValidation", tkTr(Trans::Constants::XML_WRONG_NUMBER_OF_TAG_1).arg(::XML_VALIDATION_ELEMENTTAG));
        AlertValidation *val = new AlertValidation();
        return *val;
    }
    return fromDomElement(el);
}

AlertValidation &AlertValidation::fromDomElement(const QDomElement &element)
{
    AlertValidation *val = new AlertValidation();
    if (element.tagName().compare(::XML_VALIDATION_ELEMENTTAG, Qt::CaseInsensitive)!=0)
        return *val;
    if (!element.attribute("id").isEmpty())
        val->setId(element.attribute("id").toInt());
    val->setOverriden(element.attribute("overridden").compare("true", Qt::CaseInsensitive)==0);
    val->setValidatorUuid(element.attribute("validator"));
    val->setUserComment(element.attribute("comment"));
    val->setValidatedUuid(element.attribute("validated"));
    val->setDateOfValidation(QDateTime::fromString(element.attribute("dt"), Qt::ISODate));
    val->setModified(false);
    return *val;
}

bool AlertValidation::operator==(const AlertValidation &other) const
{
    return _id == other._id &&
            _modified == other._modified &&
            _overridden == other._overridden &&
            _validator == other._validator &&
            _userComment == other._userComment &&
            _validated == other._validated &&
            _date == other._date;
}

QString AlertRelation::relationTypeToString() const
{
    switch (_related) {
    case RelatedToPatient:
    {
        if (patient()) {
            if (patient()->uuid().compare(_relatedUid, Qt::CaseInsensitive)==0) {
                return Utils::firstLetterUpperCase(tkTr(Trans::Constants::RELATED_TO_CURRENT_PATIENT));
            } else {
                return Utils::firstLetterUpperCase(tkTr(Trans::Constants::RELATED_TO_PATIENT_1)
                                                   .arg(patient()->fullPatientName(_relatedUid).value(_relatedUid)));
            }
        }
        return Utils::firstLetterUpperCase(tkTr(Trans::Constants::RELATED_TO_CURRENT_PATIENT));
    }
    case RelatedToFamily: return tkTr(Trans::Constants::RELATED_TO_PATIENT_FAMILY_1).arg("");
    case RelatedToAllPatients: return Utils::firstLetterUpperCase(tkTr(Trans::Constants::RELATED_TO_ALL_PATIENTS));
    case RelatedToUser:
    {
        if (user()) {
            if (user()->uuid().compare(_relatedUid, Qt::CaseInsensitive)==0) {
                return Utils::firstLetterUpperCase(tkTr(Trans::Constants::RELATED_TO_CURRENT_USER));
            } else {
                return Utils::firstLetterUpperCase(tkTr(Trans::Constants::RELATED_TO_USER_1)
                                                   .arg(user()->fullNameOfUser(_relatedUid)));
            }
        }
        return Utils::firstLetterUpperCase(tkTr(Trans::Constants::RELATED_TO_CURRENT_USER));
    }
    case RelatedToAllUsers: return Utils::firstLetterUpperCase(tkTr(Trans::Constants::RELATED_TO_ALL_USERS));
    case RelatedToUserGroup: return Utils::firstLetterUpperCase(tkTr(Trans::Constants::RELATED_TO_USER_GROUP_1).arg(""));
    case RelatedToApplication: return Utils::firstLetterUpperCase(tkTr(Trans::Constants::RELATED_TO_APPLICATION));
    }
    return QString::null;
}

QString AlertRelation::relationTypeToXml(AlertRelation::RelatedTo rel) // static
{
    switch (rel) {
    case RelatedToPatient: return "patient";
    case RelatedToAllPatients: return "allPatients";
    case RelatedToFamily: return "family";
    case RelatedToUser: return "user";
    case RelatedToAllUsers: return "allUsers";
    case RelatedToUserGroup: return "userGroup";
    case RelatedToApplication: return "application";
    }
    return QString::null;
}

AlertRelation::RelatedTo AlertRelation::relationTypeFromXml(const QString &xmlValue) // static
{
    if (xmlValue.compare("patient", Qt::CaseInsensitive) == 0)
        return RelatedToPatient;
    else if (xmlValue.compare("allPatients", Qt::CaseInsensitive) == 0)
        return RelatedToAllPatients;
    else if (xmlValue.compare("family", Qt::CaseInsensitive) == 0)
        return RelatedToFamily;
    else if (xmlValue.compare("user", Qt::CaseInsensitive) == 0)
        return RelatedToUser;
    else if (xmlValue.compare("allUsers", Qt::CaseInsensitive) == 0)
        return RelatedToAllUsers;
    else if (xmlValue.compare("userGroup", Qt::CaseInsensitive) == 0)
        return RelatedToUserGroup;
    else if (xmlValue.compare("application", Qt::CaseInsensitive) == 0)
        return RelatedToApplication;
    return RelatedToApplication;
}

QString AlertRelation::toXml() const
{
    QDomDocument doc;
    QDomElement el = doc.createElement(::XML_RELATED_ELEMENTTAG);
    el.setAttribute("id", _id);
    el.setAttribute("to", relationTypeToXml(_related));
    el.setAttribute("uid", _relatedUid);
    doc.appendChild(el);
    return doc.toString();
}

AlertRelation &AlertRelation::fromXml(const QString &xml)
{
    QDomDocument doc;
    int line = 0;
    int col = 0;
    QString error;
    if (!doc.setContent(xml, &error, &line, &col)) {
        LOG_ERROR_FOR("AlertRelation", tkTr(Trans::Constants::ERROR_1_LINE_2_COLUMN_3)
                      .arg(error).arg(line).arg(col));
        qWarning() << xml;
        AlertRelation *rel = new AlertRelation;
        return *rel;
    }
    QDomElement el = doc.documentElement();
    if (el.tagName().compare(::XML_RELATED_ELEMENTTAG, Qt::CaseInsensitive) != 0)
        el = el.firstChildElement(::XML_RELATED_ELEMENTTAG);
    if (el.isNull()) {
        LOG_ERROR_FOR("AlertRelation", tkTr(Trans::Constants::XML_WRONG_NUMBER_OF_TAG_1).arg(::XML_RELATED_ELEMENTTAG));
        AlertRelation *rel = new AlertRelation;
        return *rel;
    }
    return fromDomElement(el);
}

AlertRelation &AlertRelation::fromDomElement(const QDomElement &element)
{
    AlertRelation *rel = new AlertRelation;
    if (element.tagName().compare(::XML_RELATED_ELEMENTTAG, Qt::CaseInsensitive)!=0)
        return *rel;
    if (!element.attribute("id").isEmpty())
        rel->setId(element.attribute("id").toInt());
    rel->setRelatedTo(AlertRelation::relationTypeFromXml(element.attribute("to")));
    rel->setRelatedToUid(element.attribute("uid"));
    rel->setModified(false);
    return *rel;
}

bool AlertRelation::operator==(const AlertRelation &other) const
{
    return _id == other._id &&
            _related == other._related &&
            _modified == other._modified &&
            _relatedUid == other._relatedUid;
}

QDebug operator<<(QDebug dbg, const Alert::AlertTiming *c)
{
    if (!c) {
        dbg.nospace() << "AlertTiming(0x0)";
        return dbg.space();
    }
    return operator<<(dbg, *c);
}

QDebug operator<<(QDebug dbg, const Alert::AlertTiming &a)
{
    QStringList s;
    s << QString("AlertTiming(%1; %2").arg(a.id()).arg(a.isValid()?"valid":"invalid");
    if (a.isModified())
        s << "modified";
    else
        s << "non-modified";
    s << QString("Start: %1").arg(a.start().toString(Qt::ISODate));
    s << QString("End: %1").arg(a.end().toString(Qt::ISODate));
    s << QString("Expiration: %1").arg(a.expiration().toString(Qt::ISODate));
    if (a.isCycling()) {
        s << "\n             cycling";
        s << QString("Delay(mins): %1").arg(a.cyclingDelayInMinutes());
        s << QString("NCycle: %1").arg(a.numberOfCycles());
        s << QString("currentCycleStart: %1").arg(a.currentCycleStartDate().toString(Qt::ISODate));
        s << QString("currentCycleExpiration: %1").arg(a.currentCycleExpirationDate().toString(Qt::ISODate));
        s << QString("currentCycle: %1").arg(a.currentCycle());
    }
    dbg.nospace() << s.join("; ")
                  << ")";
    return dbg.space();
}

QDebug operator<<(QDebug dbg, const Alert::AlertItem *c)
{
    if (!c) {
        dbg.nospace() << "AlertItem(0x0)";
        return dbg.space();
    }
    return operator<<(dbg, *c);
}

QDebug operator<<(QDebug dbg, const Alert::AlertItem &a)
{
    QStringList s;
    s << "AlertItem(" + a.uuid();
    if (a.isValid()) {
        if (a.isModified())
            s << "valid*";
        else
            s << "valid";
    } else {
        if (a.isModified())
            s << "notValid*";
        else
            s << "notValid";
    }
    switch (a.priority()) {
    case AlertItem::High:
        s << "high";
        break;
    case AlertItem::Medium:
        s << "medium";
        break;
    case AlertItem::Low:
        s << "low";
        break;
    }
    if (!a.cryptedPassword().isEmpty())
        s << "pass:" + a.cryptedPassword();
    s << "lbl:" + a.label();
    s << "cat:" + a.category();
    s << "availableLang:" + a.availableLanguages().join(";");
    switch (a.viewType()) {
    case AlertItem::BlockingAlert:
        s << "view:blocking";
        break;
    case AlertItem::NonBlockingAlert:
        s << "view:nonblocking";
        break;
    default:
        s << "view:" + QString::number(a.viewType());
        break;
    }
    switch (a.contentType()) {
    case AlertItem::ApplicationNotification:
        s << "content:appNotification";
        break;
    case AlertItem::PatientCondition:
        s << "content:patientCondition";
        break;
    case AlertItem::UserNotification:
        s << "content:userNotification";
        break;
    default:
        s << "content:" + QString::number(a.contentType());
        break;
    }
    s << "create:" + a.creationDate().toString(Qt::ISODate);
    s << QString::number(a.timingAt(0).cyclingDelayInMinutes());
    dbg.nospace() << s.join(",\n           ")
                  << ")";
    return dbg.space();
}
