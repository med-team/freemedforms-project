/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, <eric.maeker@gmail.com>,                             *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ALERT_INTERNAL_ALERTPREFERENCES_H
#define ALERT_INTERNAL_ALERTPREFERENCES_H

#include <coreplugin/ioptionspage.h>

#include <QWidget>
#include <QPointer>

/**
 * \file ./plugins/alertplugin/alertpreferences.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;
}

namespace Alert {
namespace Internal {
namespace Ui {
class AlertPreferencesWidget;
}

class AlertPreferencesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AlertPreferencesWidget(QWidget *parent = 0);
    ~AlertPreferencesWidget();

    void setDataToUi();

    static void writeDefaultSettings(Core::ISettings *s);

public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);

private:
    void retranslateUi();
    void changeEvent(QEvent *e);

private:
    Ui::AlertPreferencesWidget *ui;
};


class AlertPreferencesPage : public Core::IOptionsPage
{
public:
    AlertPreferencesPage(QObject *parent = 0);
    ~AlertPreferencesPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const {return displayName();}
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    QString helpPage() {return QString();}

    static void writeDefaultSettings(Core::ISettings *s) {AlertPreferencesWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);

private:
    QPointer<Internal::AlertPreferencesWidget> m_Widget;
};


} // namespace Internal
} // namespace Alert
#endif // ALERT_INTERNAL_ALERTPREFERENCES_H
