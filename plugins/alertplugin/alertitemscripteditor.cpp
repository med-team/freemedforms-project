/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, <eric.maeker@gmail.com>,                             *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "alertitemscripteditor.h"

#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/constants_icons.h>

#include "ui_alertitemscripteditor.h"

#include <QMenu>
#include <QDebug>

using namespace Alert;
using namespace Internal;

static inline Core::ITheme *theme() {return Core::ICore::instance()->theme();}

AlertItemScriptEditor::AlertItemScriptEditor(QWidget *parent) :
    QWidget(parent),
    _previousIndex(-1),
    ui(new Ui::AlertItemScriptEditor)
{
    ui->setupUi(this);
    layout()->setMargin(0);
    ui->add->setIcon(theme()->icon(Core::Constants::ICONADD));
    ui->remove->setIcon(theme()->icon(Core::Constants::ICONREMOVE));
    _menu = new QMenu(this);
    for(int i=0; i < 1000; ++i) {
        QString type = AlertScript::typeToString(AlertScript::ScriptType(i));
        if (type.isEmpty())
            break;
        QAction *a = new QAction(_menu);
        a->setText(type);
        a->setData(i);
        _menu->addAction(a);
    }
    ui->add->setMenu(_menu);
    connect(_menu, SIGNAL(triggered(QAction*)), this, SLOT(addAction(QAction*)));
}

AlertItemScriptEditor::~AlertItemScriptEditor()
{
    delete ui;
}

void AlertItemScriptEditor::clear()
{
    ui->types->clear();
    ui->plainTextEdit->clear();
}

void AlertItemScriptEditor::setAlertItem(const AlertItem &alert)
{
    _scriptsCache = alert.scripts().toList();
    _scripts = _scriptsCache;
    refreshScriptCombo();
}

void AlertItemScriptEditor::refreshScriptCombo()
{
    ui->types->disconnect(this);
    ui->types->clear();
    qSort(_scripts);

    for(int i=0; i<_scripts.count(); ++i) {
        ui->types->addItem(AlertScript::typeToString(_scripts.at(i).type()));
    }

    foreach(QAction *a, _menu->actions()) {
        a->setEnabled(true);
        for(int i=0; i<_scripts.count(); ++i) {
            if (a->data().toInt() == _scripts.at(i).type()) {
                a->setEnabled(false);
                break;
            }
        }
    }

    connect(ui->types, SIGNAL(currentIndexChanged(int)), this, SLOT(onTypesSelected(int)));
    onTypesSelected(0);
}

QVector<AlertScript> AlertItemScriptEditor::scripts() const
{
    return _scriptsCache.toVector();
}

void AlertItemScriptEditor::submit()
{
    if (_scripts.count()) {
        _scripts[ui->types->currentIndex()].setScript(ui->plainTextEdit->toPlainText());
    }
    _scriptsCache.clear();
    _scriptsCache = _scripts;
}

void AlertItemScriptEditor::onTypesSelected(int index)
{
    if (_scripts.count() <= index)
        return;

    if (_previousIndex != -1) {
        _scripts[_previousIndex].setScript(ui->plainTextEdit->toPlainText());
    }
    _previousIndex = index;
    ui->plainTextEdit->setPlainText(_scripts.at(index).script());
}

void AlertItemScriptEditor::addAction(QAction *a)
{
    AlertScript::ScriptType type = AlertScript::ScriptType(a->data().toInt());
    for(int i=0; i < _scripts.count(); ++i) {
        if (_scripts.at(i).type()==type)
            return;
    }

    AlertScript script;
    script.setType(type);
    _scripts.append(script);

    refreshScriptCombo();

    for(int i=0; i < _scripts.count(); ++i) {
        if (_scripts.at(i).type()==type)
            ui->types->setCurrentIndex(i);
    }
}
