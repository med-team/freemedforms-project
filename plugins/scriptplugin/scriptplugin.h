/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef SCRIPT_PLUGIN_H
#define SCRIPT_PLUGIN_H

#include <extensionsystem/iplugin.h>

#include <QObject>
QT_BEGIN_NAMESPACE
class QAction;
QT_END_NAMESPACE

/**
 * \file ./plugins/scriptplugin/scriptplugin.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Script {
namespace Internal {
class ScriptManager;

class ScriptPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.freemedforms.FreeMedForms.ScriptPlugin" FILE "Script.json")

public:
    ScriptPlugin();
    ~ScriptPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

private Q_SLOTS:
    void postCoreInitialization();
    void patientSelected();
    void onScriptDialogTriggered();

private:
    ScriptManager *m_Manager;
    QAction *aScriptDialog;
};

}  // namespace Internal
}  // namespace Script

#endif  // End SCRIPT_PLUGIN_H
