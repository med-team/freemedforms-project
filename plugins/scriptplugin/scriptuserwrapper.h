/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef SCRIPTUSERWRAPPER_H
#define SCRIPTUSERWRAPPER_H

#include <QObject>
#include <QDate>
#include <QVariant>
#include <QString>
#include <QStringList>

/**
 * \file ./plugins/scriptplugin/scriptuserwrapper.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Script {
namespace Internal {

class ScriptUserWrapper : public QObject  //, public QScriptClass
{
    Q_OBJECT
    Q_PROPERTY(bool     isActive    READ isActive())
    Q_PROPERTY(QString  usualName   READ usualName())
    Q_PROPERTY(QString  otherNames  READ otherNames())
    Q_PROPERTY(QString  firstName   READ firstName())
    Q_PROPERTY(QString  fullName    READ fullName())

    Q_PROPERTY(QString  street      READ street())
    Q_PROPERTY(QString  city        READ city())
    Q_PROPERTY(QString  zipcode     READ zipcode())
    Q_PROPERTY(QString  state       READ state())
    Q_PROPERTY(QString  country     READ country())
    Q_PROPERTY(QString  fullAddress READ fullAddress())

    Q_PROPERTY(QDate    dateOfBirth READ dateOfBirth())
    Q_PROPERTY(bool     isMale      READ isMale())
    Q_PROPERTY(bool     isFemale    READ isFemale())

    Q_PROPERTY(QStringList specialties READ specialties())
    Q_PROPERTY(QStringList qualifications READ qualifications())
    Q_PROPERTY(QStringList identifiants READ identifiants())

public:
    ScriptUserWrapper(QObject *parent);

public Q_SLOTS:
    bool isActive() const;

    QString fullName() const;
    QString usualName() const;
    QString otherNames() const;
    QString firstName() const;

    QString street() const;
    QString city() const;
    QString zipcode() const;
    QString state() const;
    QString country() const;
    QString fullAddress() const;

    QDate dateOfBirth() const;
    bool isMale() const;
    bool isFemale() const;

    QStringList specialties() const;
    QStringList qualifications() const;
    QStringList identifiants() const;

};

}  // namespace Internal
}  // namespace Script

#endif // SCRIPTUSERWRAPPER_H
