<plugin name="Script" version="0.0.1" compatVersion="0.0.1">
    <vendor>FreeMedForms</vendor>
    <copyright>(C) 2008-2017 Eric Maeker, MD</copyright>
    <license>See application's license</license>
    <category>Script engine</category>
    <description>Provides the form scripting engine.</description>
    <url>http://www.freemedforms.com</url>
    <dependencyList>
        <dependency name="Core" version="0.0.1"/>
        <dependency name="FormManager" version="0.0.1"/>
    </dependencyList>
</plugin>
