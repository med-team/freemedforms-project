/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef SCRIPT_INTERNAL_SCRIPTLOG_H
#define SCRIPT_INTERNAL_SCRIPTLOG_H

#include <QObject>

/**
 * \file ./plugins/scriptplugin/scriptlog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Script {
namespace Internal {
class ScriptLog : public QObject
{
    Q_OBJECT
public:
    explicit ScriptLog(QObject *parent = 0);
    ~ScriptLog();

public Q_SLOTS:
    void message(const QString &owner, const QString &message);
    void error(const QString &owner, const QString &message);
};

} // namespace Internal
} // namespace Script

#endif // SCRIPT_INTERNAL_SCRIPTLOG_H

