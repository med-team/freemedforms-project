/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef SCRIPT_SCRIPTMANAGER_H
#define SCRIPT_SCRIPTMANAGER_H

#include <scriptplugin/script_exporter.h>
#include <coreplugin/iscriptmanager.h>

#include <QScriptEngine>

/**
 * \file ./plugins/scriptplugin/scriptmanager.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Script {
namespace Internal {
class UiTools;
class ScriptPatientWrapper;
class ScriptUserWrapper;
class FormManagerScriptWrapper;
class Tools;
class ScriptLog;

class ScriptManager : public Core::IScriptManager
{
    Q_OBJECT

public:
    ScriptManager(QObject *parent);

    QScriptValue evaluate(const QString &script);
    QScriptValue addScriptObject(QObject *object);
//    QScriptValue addScriptObject(QObject *object, const QString &objectNamespace, const QString &objectScriptName);

    QScriptEngine *engine() {return m_Engine;}

private Q_SLOTS:
    void onAllFormsLoaded();
    void onSubFormLoaded(const QString &subFormUuid);

private:
    static ScriptManager *m_Instance;
    QScriptEngine *m_Engine;
    ScriptPatientWrapper *patient;
    ScriptUserWrapper *user;
    FormManagerScriptWrapper *forms;
    UiTools *uitools;
    Tools *tools;
    ScriptLog *_log;
};

}  // namespace Internal
}  // namespace Script

#endif // SCRIPT_SCRIPTMANAGER_H
