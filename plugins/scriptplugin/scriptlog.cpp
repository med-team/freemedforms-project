/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
/*!
 * \class Script::Internal::ScriptLog
 * Script wrapper over the Utils::Log static class
 * \sa Utils::Log
 */

#include "scriptlog.h"

#include <utils/log.h>

#include <QDebug>

using namespace Script;
using namespace Internal;

/*! Constructor of the Script::Internal::ScriptLog class */
ScriptLog::ScriptLog(QObject *parent) :
    QObject(parent)
{
}

/*! Destructor of the Script::Internal::ScriptLog class */
ScriptLog::~ScriptLog()
{
}

void ScriptLog::message(const QString &owner, const QString &message)
{
    LOG_FOR(owner, message);
}

void ScriptLog::error(const QString &owner, const QString &message)
{
    LOG_ERROR_FOR(owner, message);
}
