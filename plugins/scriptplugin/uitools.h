/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef SCRIPT_UITOOLS_H
#define SCRIPT_UITOOLS_H

#include <QObject>
#include <QStringList>
class QWidget;
class QScriptValue;

/**
 * \file ./plugins/scriptplugin/uitools.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Script {
namespace Internal {

class FormItemScriptWrapper;

class UiTools : public QObject
{
    Q_OBJECT
public:
    explicit UiTools(QObject *parent = 0);

Q_SIGNALS:

public Q_SLOTS:
    void printQObjectChildrenList(QObject *obj);

    bool addItem(QWidget *widget, const QString &item);
    bool addItems(QWidget *widget, const QStringList &items);
    bool addJoinedItem(QWidget *widget, const QString &item, const QString &separator);

    bool setItemText(QWidget *widget, const int row, const QString &item);

    bool clear(QWidget *widget);

    QStringList selectedItems(QWidget *widget);

    void showScreenshot(const QString &formUid, const QString &fileName) const;

};

}  // namespace Internal
}  // namespace Script

#endif // SCRIPT_UITOOLS_H
