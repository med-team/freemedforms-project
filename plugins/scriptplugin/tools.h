/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef SCRIPT_INTERNAL_TOOLS_H
#define SCRIPT_INTERNAL_TOOLS_H

#include <QObject>
#include <QDate>
#include <QString>

/**
 * \file ./plugins/scriptplugin/tools.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Script {
namespace Internal {

class Tools : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString userDocumentPath READ userDocumentPath)

public:
    explicit Tools(QObject *parent = 0);

public Q_SLOTS:
    QString userDocumentPath();

    bool checkDirCreateIfNotExists(const QString &absolutePath);

    QString dateToString(const QVariant &date, const QString &format);
    QDate addDays(const QDate &date, int days);
    QDate addWeeks(const QDate &date, int weeks);
    int daysTo(const QDate &from, const QDate &to);

    void openUrl(const QString &url);
    QString lineWrapString(const QString &text, int lineLength);

};

} // namespace Internal
} // namespace Script

#endif // SCRIPT_INTERNAL_TOOLS_H
