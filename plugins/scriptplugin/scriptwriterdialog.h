/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef SCRIPT_INTERNAL_SCRIPTWRITERDIALOG_H
#define SCRIPT_INTERNAL_SCRIPTWRITERDIALOG_H

#include <QDialog>

/**
 * \file ./plugins/scriptplugin/scriptwriterdialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Script {
namespace Internal {
class ScriptWriterDialogPrivate;

class ScriptWriterDialog : public QDialog
{
    Q_OBJECT
public:
    explicit ScriptWriterDialog(QWidget *parent = 0);
    ~ScriptWriterDialog();
    bool initialize();

Q_SIGNALS:

public Q_SLOTS:
    void onReadFileTriggered();
    void onSaveFileTriggered();
    void onExecuteScriptTriggered();

private:
    Internal::ScriptWriterDialogPrivate *d;
};

} // namespace Internal
} // namespace Script

#endif // SCRIPT_INTERNAL_SCRIPTWRITERDIALOG_H

