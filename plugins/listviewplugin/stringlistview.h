/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef STRINGLISTVIEW_H
#define STRINGLISTVIEW_H

#include <listviewplugin/listview_exporter.h>
#include <listviewplugin/listview.h>

#include <QObject>

/**
 * \file ./plugins/listviewplugin/stringlistview.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Views {

class LISTVIEW_EXPORT StringListView : public ListView
{
    Q_OBJECT
    Q_PROPERTY(QVariant stringList         READ getStringList         WRITE setStringList        USER true)
    Q_PROPERTY(QVariant checkedStringList  READ getCheckedStringList  WRITE setCheckedStringList USER true)

public:
    StringListView(QWidget * parent = 0);
    ~StringListView();

    QVariant getStringList() const;
    void setStringList(const QVariant &list);

    void setItemsCheckable(bool state);
    bool isItemCheckable() const;
    QVariant getCheckedStringList() const;
    void setCheckedStringList(const QVariant &list);

Q_SIGNALS:
    void stringListChanged();
};

}  // End Views

#endif // STRINGLISTVIEW_H
