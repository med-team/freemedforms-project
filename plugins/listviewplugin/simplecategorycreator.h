/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef SIMPLECATEGORYCREATOR_H
#define SIMPLECATEGORYCREATOR_H

#include <QDialog>
#include <QModelIndex>
class QAbstractItemModel;

namespace Views {

namespace Ui {
    class SimpleCategoryCreator;
}

class SimpleCategoryCreator : public QDialog
{
    Q_OBJECT

public:
    explicit SimpleCategoryCreator(QWidget *parent = 0);
    ~SimpleCategoryCreator();

    void setModel(QAbstractItemModel *model, const QModelIndex &parent, const int columnToShow = 0);

    void setLabelColumn(int col) {m_LabelCol=col;}
    void setIconColumn(int col) {m_IconCol=col;}

public Q_SLOTS:
    void done(int r);

protected:
    void changeEvent(QEvent *e);

private:
    Ui::SimpleCategoryCreator *ui;
    QAbstractItemModel *m_Model;
    QModelIndex m_Parent;
    int m_LabelCol, m_IconCol;
    QString m_ThemedIconFileName;
};

}  // End namespace Views


#endif // SIMPLECATEGORYCREATOR_H
