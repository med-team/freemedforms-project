/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "listview.h"
#include "stringlistmodel.h"
#include "constants.h"
#include "viewmanager.h"

#include <translationutils/constanttranslations.h>
#include <utils/log.h>

#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/contextmanager/contextmanager.h>
#include <coreplugin/itheme.h>
#include <coreplugin/icore.h>
#include <coreplugin/constants_menus.h>
#include <coreplugin/constants_icons.h>

#include <QLayout>
#include <QAction>
#include <QMenu>
#include <QWidget>
#include <QListView>
#include <QStringListModel>
#include <QToolButton>
#include <QToolBar>
#include <QToolTip>

using namespace Views;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Core::ContextManager *contextManager() { return Core::ICore::instance()->contextManager(); }


namespace Views {
namespace Internal {

class ListViewPrivate
{
public:
    ListViewPrivate(QWidget *parent, Constants::AvailableActions actions) :
        m_Parent(parent),
        m_ListView(0),
        m_Actions(actions),
        m_Context(0),
        m_ExtView(0),
        m_MaxRows(-1)
    {
    }

    ~ListViewPrivate()
    {
        if (m_ExtView)
            delete m_ExtView;
        m_ExtView = 0;
    }

    void calculateContext()
    {
        Core::Context context;
        if (m_Actions & Constants::MoveUpDown)
            context.add(Constants::C_BASIC_MOVE);

        if (m_Actions & Constants::AddRemove)
            context.add(Constants::C_BASIC_ADDREMOVE);

        m_Context->setContext(context);
    }

public:
    QWidget *m_Parent;
    QListView *m_ListView;
    Constants::AvailableActions m_Actions;
    ViewContext *m_Context;
    QToolBar *m_ToolBar;
    QString m_ContextName;
    ExtendedView *m_ExtView;
    int m_MaxRows;
};

}  // End Internal
}  // End Views


ListView::ListView(QWidget *parent, Constants::AvailableActions actions) :
    IView(parent),
    d(0)
{
    static int handler = 0;
    ++handler;
    QObject::setObjectName("ListView_"+QString::number(handler));
    setProperty(Constants::HIDDEN_ID, "xx");
    d = new Internal::ListViewPrivate(this, actions);

    d->m_ListView = new QListView(this);
    setItemView(d->m_ListView);

    d->m_Context = new ViewContext(this);
    d->calculateContext();
    contextManager()->addContextObject(d->m_Context);

    d->m_ExtView = new ExtendedView(this, actions);
}

ListView::~ListView()
{
    contextManager()->removeContextObject(d->m_Context);
    if (d) {
        delete d;
        d = 0;
    }
}

QAbstractItemView *ListView::itemView() const
{
    return d->m_ListView;
}

void ListView::setModelColumn(int column)
{
    d->m_ListView->setModelColumn(column);
}


void ListView::setActions(Constants::AvailableActions actions)
{
    d->m_Actions = actions;
    d->calculateContext();
    d->m_ExtView->setActions(actions);
}


void ListView::setCommands(const QStringList &commandsUid)
{
    d->m_Actions = 0;
    d->calculateContext();
    d->m_ExtView->setCommands(commandsUid);
}


void ListView::addContext(const Core::Context &context)
{
    Core::Context current = d->m_Context->context();
    current.add(context);
    d->m_Context->setContext(current);
}

void ListView::hideButtons() const
{
    d->m_ExtView->hideButtons();
}

void ListView::showButtons()
{
    d->m_ExtView->showButtons();
}

void ListView::useContextMenu(bool state)
{
    d->m_ExtView->useContextMenu(state);
}

void ListView::setMaximumRows(int max)
{
    d->m_MaxRows = max;
}

int ListView::maximumRows() const
{
    return d->m_MaxRows;
}

void ListView::addItem()
{
    setFocus();
    bool add = true;
    if (d->m_MaxRows > 0) {
        if (model()->rowCount() >= d->m_MaxRows) {
            QPoint pos = itemView()->mapToGlobal(itemView()->rect().bottomLeft());
            QToolTip::showText(pos - QPoint(0,32),
                               tr("Unable to add a new line, you have reached "
                                  "the maximum autorized lines."),
                               itemView());
            add = false;
        }
    }
    if (add) {
        Q_EMIT addRequested();
        Q_EMIT aboutToAddItem();
        d->m_ExtView->addItem();
        Q_EMIT itemAdded();
    }
}

void ListView::removeItem()
{
    Q_EMIT removeRequested();
    Q_EMIT aboutToRemove();
    setFocus();
    d->m_ExtView->removeItem();
    Q_EMIT itemRemoved();
}

void ListView::moveDown()
{
    setFocus();
    d->m_ExtView->moveDown();
}

void ListView::moveUp()
{
    setFocus();
    d->m_ExtView->moveUp();
}

