/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#ifndef LISTVIEWEXPORTER_H
#define LISTVIEWEXPORTER_H

#include <qglobal.h>

#if defined(LISTVIEW_LIBRARY)
#define LISTVIEW_EXPORT Q_DECL_EXPORT
#else
#define LISTVIEW_EXPORT Q_DECL_IMPORT
#endif

#endif
