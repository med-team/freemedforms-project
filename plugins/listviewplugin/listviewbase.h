/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef VIEWS_BASE_H
#define VIEWS_BASE_H

#include <utils/database.h>

namespace Views {
namespace Internal {
class ViewBasePrivate;

class ViewBase : public QObject, public Utils::Database
{
    Q_OBJECT

protected:
    ViewBase(QObject *parent = 0);

public:
    // Constructor
    static ViewBase *instance();
    virtual ~ViewBase();

    // initialize
    bool init();


private:
    bool createDatabase(const QString &connectionName, const QString &dbName,
                          const QString &pathOrHostName,
                          TypeOfAccess access, AvailableDrivers driver,
                          const QString &login, const QString &pass,
                          const int port,
                          CreationOption createOption
                         );

private Q_SLOTS:
    void onCoreDatabaseServerChanged();

private:
    static bool m_initialized;
    static ViewBase *m_Instance;
};

}  // End namespace Internal
}  // End namespace Views


#endif // VIEWS_BASE_H
