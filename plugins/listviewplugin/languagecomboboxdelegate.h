/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef VIEWS_LANGUAGECOMBOBOXDELEGATE_H
#define VIEWS_LANGUAGECOMBOBOXDELEGATE_H

#include <listviewplugin/listview_exporter.h>

#include <utils/widgets/languagecomboboxdelegate.h>


namespace Views {

    class LISTVIEW_EXPORT LanguageComboBoxDelegate : public Utils::LanguageComboBoxDelegate
{
    Q_OBJECT
public:
    explicit LanguageComboBoxDelegate(QObject *parent = 0, DisplayMode mode = AvailableTranslations);
};

}  // End namespace View


#endif // VIEWS_LANGUAGECOMBOBOXDELEGATE_H
