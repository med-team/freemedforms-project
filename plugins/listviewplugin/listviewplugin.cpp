/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/


#include "listviewplugin.h"
#include "viewmanager.h"

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <utils/log.h>

#include <QtPlugin>
#include <QDebug>

using namespace Views;
using namespace Internal;

ListViewPlugin::ListViewPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating ListViewPlugin";
}

ListViewPlugin::~ListViewPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool ListViewPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "ListViewPlugin::initialize";
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);
    Internal::ViewManager::instance(this);
    return true;
}

void ListViewPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "ListViewPlugin::extensionsInitialized";
    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));
}

ExtensionSystem::IPlugin::ShutdownFlag ListViewPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(ListViewPlugin)
