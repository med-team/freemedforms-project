/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "countrycombobox.h"

#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>

using namespace Views;

static inline Core::ISettings *settings() {return Core::ICore::instance()->settings();}

CountryComboBox::CountryComboBox(QWidget *parent) :
    Utils::CountryComboBox(parent)
{
    setFlagPath(settings()->path(Core::ISettings::SmallPixmapPath) + "/flags/");
    initialize();
}
