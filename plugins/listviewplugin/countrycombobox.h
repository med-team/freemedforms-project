/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef VIEWS_COUNTRYCOMBOBOX_H
#define VIEWS_COUNTRYCOMBOBOX_H

#include <listviewplugin/listview_exporter.h>
#include <utils/widgets/countrycombobox.h>

namespace Views {

class LISTVIEW_EXPORT CountryComboBox : public Utils::CountryComboBox
{
    Q_OBJECT
public:
    CountryComboBox(QWidget *parent = 0);

};

}  // End namespace Views

#endif // VIEWS_COUNTRYCOMBOBOX_H
