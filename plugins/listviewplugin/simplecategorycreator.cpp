/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "simplecategorycreator.h"
#include "simplecategorymodel.h"

#include "ui_simplecategorycreator.h"

using namespace Views;

SimpleCategoryCreator::SimpleCategoryCreator(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SimpleCategoryCreator),
    m_LabelCol(0), m_IconCol(1)
{
    ui->setupUi(this);
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));}

SimpleCategoryCreator::~SimpleCategoryCreator()
{
    delete ui;
}

void SimpleCategoryCreator::done(int r)
{
    if (r == QDialog::Accepted) {
        if (!m_Model->insertRow(m_Model->rowCount(m_Parent), m_Parent))
            return;
        m_Model->setData(m_Model->index(m_Model->rowCount(m_Parent)-1, m_LabelCol, m_Parent), ui->categoryLabel->text());
        m_Model->setData(m_Model->index(m_Model->rowCount(m_Parent)-1, m_IconCol, m_Parent), m_ThemedIconFileName);
    }
    QDialog::done(r);
}

void SimpleCategoryCreator::setModel(QAbstractItemModel *model, const QModelIndex &parent, const int columnToShow)
{
    m_Model = model;
    if (parent.model() != m_Model)
        return;
    m_Parent = parent;
    ui->treeView->setModel(model);
    ui->treeView->setCurrentIndex(parent);
    for(int i = 0; i < m_Model->columnCount(m_Parent); ++i) {
        ui->treeView->hideColumn(i);
    }
    ui->treeView->setColumnHidden(columnToShow, false);
    ui->treeView->header()->hide();
}

void SimpleCategoryCreator::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
