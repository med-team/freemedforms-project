/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef VIEWS_LANGUAGECOMBOBOX_H
#define VIEWS_LANGUAGECOMBOBOX_H

#include <listviewplugin/listview_exporter.h>

#include <utils/widgets/languagecombobox.h>

namespace Views {

class LISTVIEW_EXPORT LanguageComboBox : public Utils::LanguageComboBox
{
    Q_OBJECT
public:
    explicit LanguageComboBox(QWidget* parent = 0);
};


}  // End namespace Views

#endif // VIEWS_LANGUAGECOMBOBOX_H
