/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef VIEWS_PLUGIN_CONSTANTS_H
#define VIEWS_PLUGIN_CONSTANTS_H

#include <QtGlobal>
#include <QString>

namespace Views {
namespace Constants {

    // Translate context
    const char * const  VIEWS_CONSTANTS_TR_CONTEXT = "Views";

    const char* const C_BASIC            = "context.ListView.basic";
    const char* const C_BASIC_ADDREMOVE  = "context.ListView.AddRemove";
    const char* const C_BASIC_MOVE       = "context.ListView.Move";
    const char * const HIDDEN_ID         = "@#HiDdEnId#@";


    /** \brief Enumerates the available default actions that can be added. Actions are pop-upped. */
    enum AvailableAction {
        AddRemove      = 0x01,
        MoveUpDown     = 0x02,
        Edit           = 0x04,
        DefaultActions = AddRemove,
        AllActions     = AddRemove | MoveUpDown
    };
    Q_DECLARE_FLAGS(AvailableActions, AvailableAction)

}  // End namespace Constants
}  // End namespace Views

Q_DECLARE_OPERATORS_FOR_FLAGS(Views::Constants::AvailableActions)

#endif // VIEWS_PLUGIN_CONSTANTS_H
