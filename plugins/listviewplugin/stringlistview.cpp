/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "stringlistview.h"
#include "stringlistmodel.h"

#include <QAction>
#include <QMenu>
#include <QStringListModel>

#include <QDebug>

using namespace Views;

StringListView::StringListView(QWidget *parent) :
    ListView(parent)
{
}

StringListView::~StringListView()
{
}

QVariant StringListView::getStringList() const
{
    QStringListModel *model = qobject_cast<QStringListModel*>(this->model());
    if (model) {
        return model->stringList();
    } else {
        Views::StringListModel *model = qobject_cast<Views::StringListModel*>(this->model());
        if (model)
            return model->getStringList();
    }
    return QVariant();
}

void StringListView::setStringList(const QVariant &list)
{
    QStringListModel *model = qobject_cast<QStringListModel*>(this->model());
    if (model) {
        model->setStringList(list.toStringList());
    } else {
        Views::StringListModel *model = qobject_cast<Views::StringListModel*>(this->model());
        if (model)
            model->setStringList(list.toStringList());
    }
}

QVariant StringListView::getCheckedStringList() const
{
    Q_ASSERT_X( qobject_cast<StringListModel*>(this->model()), "StringListView::getCheckedStringList()",
                "This member can only be used if the model is a tkStringListModel.");
    StringListModel *m = qobject_cast<StringListModel*>(this->model());
    if (!m)
        return QVariant();
    return m->getCheckedItems();
}

void StringListView::setCheckedStringList(const QVariant &list)
{
    Q_ASSERT_X(qobject_cast<StringListModel*>(this->model()), "StringListView::setCheckedStringList()",
                "This member can only be used if the model is a tkStringListModel.");
    StringListModel *m = qobject_cast<StringListModel*>(this->model());
    if (!m)
        return;
    m->setCheckedItems(list.toStringList());
}

void StringListView::setItemsCheckable(bool state)
{
    StringListModel *m = qobject_cast<StringListModel*>(this->model());
    if (m)
        m->setCheckable(state);
}

bool StringListView::isItemCheckable() const
{
    StringListModel *m = qobject_cast<StringListModel*>(this->model());
    if (m)
        return m->isCheckable();
    return false;
}
