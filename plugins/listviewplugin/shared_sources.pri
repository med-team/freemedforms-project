#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

DEFINES += LISTVIEW_LIBRARY

include($${PWD}/../fmf_plugins.pri)
include($${PWD}/listviewplugin_dependencies.pri)

HEADERS += \
    $${PWD}/listview.h \
    $${PWD}/listviewplugin.h \
    $${PWD}/listview_exporter.h \
    $${PWD}/stringlistmodel.h \
    $${PWD}/stringlistview.h \
    $${PWD}/fancytreeview.h \
    $${PWD}/extendedview.h \
    $${PWD}/tableview.h \
    $${PWD}/constants.h \
    $${PWD}/viewmanager.h \
    $${PWD}/treeview.h \
    $${PWD}/languagecombobox.h \
    $${PWD}/languagecomboboxdelegate.h \
    $${PWD}/countrycombobox.h \
    $${PWD}/addremovecombobox.h

SOURCES += \
    $${PWD}/listview.cpp \
    $${PWD}/listviewplugin.cpp \
    $${PWD}/stringlistmodel.cpp \
    $${PWD}/stringlistview.cpp \
    $${PWD}/fancytreeview.cpp \
    $${PWD}/extendedview.cpp \
    $${PWD}/tableview.cpp \
    $${PWD}/viewmanager.cpp \
    $${PWD}/treeview.cpp \
    $${PWD}/languagecombobox.cpp \
    $${PWD}/languagecomboboxdelegate.cpp \
    $${PWD}/countrycombobox.cpp \
    $${PWD}/addremovecombobox.cpp

FORMS += \
    $${PWD}/fancytreeview.ui \

# include translations
TRANSLATION_NAME = listview
include($${SOURCES_ROOT_PATH}/buildspecs/translations.pri)
