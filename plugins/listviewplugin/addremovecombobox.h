/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers: Christian A Reiter                                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ADDREMOVECOMBOBOX_H
#define ADDREMOVECOMBOBOX_H

#include <listviewplugin/listview_exporter.h>
#include <listviewplugin/extendedview.h>

#include <QWidget>
#include <QComboBox>
#include <QGroupBox>

QT_BEGIN_NAMESPACE
class QPushButton;
class QLabel;
QT_END_NAMESPACE

/**
 * \file ./plugins/listviewplugin/addremovecombobox.h
 * \author Christian A. Reiter
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Views {
class LISTVIEW_EXPORT AddRemoveComboBox : public QWidget
{
    Q_OBJECT

public:
    explicit AddRemoveComboBox(QWidget *parent = 0);
    explicit AddRemoveComboBox(const QString &labelText, QWidget *parent = 0);
    void initialize();
    ~AddRemoveComboBox();

    void setLabel(const QString &text);

    inline void setModel(QAbstractItemModel *model) { Q_ASSERT(mCombo); mCombo->setModel(model); }
    inline void setModelColumn(int column) { Q_ASSERT(mCombo); mCombo->setModelColumn(column); }

    inline int currentIndex() const { Q_ASSERT(mCombo); return  mCombo->currentIndex(); }
    inline void setCurrentIndex(int index) { Q_ASSERT(mCombo); mCombo->setCurrentIndex(index); }
    inline void setMinimumRowsAllowed(int rows) { minimumRowsAllowed = rows < 0? 0 : rows; }

    void changeEvent(QEvent *e);

Q_SIGNALS:
    void aboutToAddItem();                              //!< emitted before Item is added
    void itemAdded(const QModelIndex &index);           //!< item was successfully added
    void aboutToRemoveItem(const QModelIndex &index);   //!< emitted before Item removal
    void itemRemoved();                                 //!< the Item was removed
    void currentIndexChanged(int index);                //!< current index was changed (int version)
    void currentIndexChanged(const QModelIndex &index); //!< current index was changed (QModelIndex version)

public Q_SLOTS:
    void setEditText(const QString &text);

    void addItem();
    void removeItem();

private Q_SLOTS:
    void updateUi();
    void translateIntIndexChanged(int index);

private:
    QModelIndex intIndexToQModelIndex(int intIndex) const;
    QLabel *mLabel;
    QComboBox *mCombo;
    QPushButton *mAddButton;
    QPushButton *mRemoveButton;
    int minimumRowsAllowed;
};

} // end namespace Views
#endif // ADDREMOVECOMBOBOX_H
