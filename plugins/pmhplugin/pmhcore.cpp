/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "pmhcore.h"
#include "pmhcategorymodel.h"
#include "pmhwidgetmanager.h"
#include "pmhcontentexporter.h"
#include "pmhtokens.h"

#include <formmanagerplugin/formcore.h>
#include <formmanagerplugin/formmanager.h>

#include <coreplugin/icore.h>
#include <coreplugin/ipadtools.h>

#include <utils/log.h>
#include <extensionsystem/pluginmanager.h>

#include <QApplication>

#include <../tests/modeltest/modeltest.h>

using namespace PMH;
using namespace Internal;

static inline Core::IPadTools *padTools() {return Core::ICore::instance()->padTools();}
static inline Form::FormManager &formManager() {return Form::FormCore::instance().formManager();}
static inline ExtensionSystem::PluginManager *pluginManager() {return ExtensionSystem::PluginManager::instance();}

PmhCore *PmhCore::m_Instance = 0;

PmhCore *PmhCore::instance(QObject *parent)
{
    if (!m_Instance) {
        if (!parent)
            m_Instance = new PmhCore(qApp);
        else
            m_Instance = new PmhCore(parent);
    }
    return m_Instance;
}

class ModelTest;

namespace PMH {
namespace Internal {
class PmhCorePrivate
{
public:
    PmhCorePrivate() :
        m_PmhCategoryModel(0),
        m_PmhWidgetManager(0),
        m_Exporter(0)
    {
    }

    ~PmhCorePrivate()
    {
        if (m_PmhCategoryModel)
            delete m_PmhCategoryModel;
        m_PmhCategoryModel = 0;
#ifdef WITH_PAD
        foreach(PmhTokens *tok, m_Tokens)
            padTools()->tokenPool()->removeToken(tok);
        qDeleteAll(m_Tokens);
        m_Tokens.clear();
#endif
    }

public:
    PmhCategoryModel *m_PmhCategoryModel;
    PmhWidgetManager *m_PmhWidgetManager;
    PmhContentExporter *m_Exporter;
    QList<PmhTokens *> m_Tokens;
};

} // namespace Internal
} // namespace PMH

PmhCore::PmhCore(QObject *parent) :
    QObject(parent), d(new PmhCorePrivate)
{
    setObjectName("PmhCore");
    d->m_PmhCategoryModel = new PmhCategoryModel(this);
    d->m_PmhWidgetManager = new PmhWidgetManager(this);
    d->m_Exporter = new PmhContentExporter(this);
    d->m_Exporter->initialize();
    pluginManager()->addObject(d->m_Exporter);
    connect(&formManager(), SIGNAL(patientFormsLoaded()), this, SLOT(onPatientFormsLoaded()));

#ifdef WITH_PAD
    LOG("Creating PMHx tokens");
    PmhTokens *tok = new PmhTokens;
    tok->setOutputType(PmhTokens::HtmlOutput);
    tok->initialize(d->m_PmhCategoryModel);
    d->m_Tokens << tok;
    padTools()->tokenPool()->addToken(tok);

    tok = new PmhTokens;
    tok->setOutputType(PmhTokens::PlainTextOutput);
    tok->initialize(d->m_PmhCategoryModel);
    d->m_Tokens << tok;
    padTools()->tokenPool()->addToken(tok);
#endif
}

PmhCore::~PmhCore()
{
    pluginManager()->removeObject(d->m_Exporter);
    if (d)
        delete d;
    d = 0;
}

PmhCategoryModel *PmhCore::pmhCategoryModel() const
{
    return d->m_PmhCategoryModel;
}

void PmhCore::onPatientFormsLoaded()
{
    d->m_PmhCategoryModel->setRootFormUid(formManager().centralFormUid());
    d->m_PmhCategoryModel->refreshFromDatabase();
}
