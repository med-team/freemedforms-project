/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "pmhplugin.h"
#include "pmhmode.h"
#include "pmhbase.h"
#include "pmhcore.h"
#include "pmhpreferencespage.h"

#include <utils/log.h>

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/iuser.h>
#include <coreplugin/translators.h>

#include <QtCore/QtPlugin>
#include <QDebug>

using namespace PMH::Internal;
static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline Core::IUser *user()  { return Core::ICore::instance()->user(); }
static inline void messageSplash(const QString &s) {theme()->messageSplashScreen(s); }

PmhPlugin::PmhPlugin() :
    mode(0)
{
    if (Utils::Log::debugPluginsCreation())
        qDebug() << "creating PmhPlugin";

    Core::ICore::instance()->translators()->addNewTranslator("plugin_pmh");

    m_PrefPage = new Internal::PmhPreferencesPage(this);
    addAutoReleasedObject(m_PrefPage);

    new PmhBase(this);
}

PmhPlugin::~PmhPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool PmhPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qDebug() << "PmhPlugin::initialize";
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);

    messageSplash(tr("Initializing PMHx database plugin..."));

    return true;
}

void PmhPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qDebug() << "PmhPlugin::extensionsInitialized";

    if (!user())
        return;
    if (user()->uuid().isEmpty())
        return;

    messageSplash(tr("Initializing PMHx database plugin..."));
    PmhBase::instance()->initialize();

    PmhCore::instance(this);
    mode = new Internal::PmhMode(this);

    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));
    connect(Core::ICore::instance(), SIGNAL(coreOpened()), this, SLOT(postCoreInitialization()));
}

void PmhPlugin::postCoreInitialization()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

ExtensionSystem::IPlugin::ShutdownFlag PmhPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    delete PmhCore::instance();
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(PmhPlugin)
