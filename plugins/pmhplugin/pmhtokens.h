/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PMH_INTERNAL_PMHTOKENS_H
#define PMH_INTERNAL_PMHTOKENS_H

#include <coreplugin/ipadtools.h>
#include <QObject>

/**
 * \file ./plugins/pmhplugin/pmhtokens.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace PMH {
class PmhCategoryModel;
namespace Internal {
class PmhTokensPrivate;

class PmhTokens : public Core::IToken
{
public:
    enum OutputType {
        HtmlOutput = 0,
        PlainTextOutput
    };

    PmhTokens();
    ~PmhTokens();
    bool initialize(PmhCategoryModel *model);
    
    void setOutputType(OutputType type);
    QString tooltip() const;

    QVariant testValue() const;
    QVariant value() const;

private:
    PmhTokensPrivate *d;
};

} // namespace Internal
} // namespace PMH

#endif // PMH_INTERNAL_PMHTOKENS_H

