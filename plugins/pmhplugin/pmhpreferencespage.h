/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PMHPREFERENCESPAGE_H
#define PMHPREFERENCESPAGE_H

#include <coreplugin/ioptionspage.h>

#include <QPointer>

#include "ui_pmhpreferencespage.h"

/**
 * \file ./plugins/pmhplugin/pmhpreferencespage.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/
namespace Core {
class ISettings;
}


namespace PMH {
namespace Internal {

class PmhPreferencesWidget : public QWidget, private Ui::PmhPreferencesWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(PmhPreferencesWidget)

public:
    explicit PmhPreferencesWidget(QWidget *parent = 0);
    void setDataToUi();

    static void writeDefaultSettings(Core::ISettings *s);
    static void applyToModel();

public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);

private Q_SLOTS:
    void on_changePmhFont_clicked();
    void on_changeCatFont_clicked();

protected:
    virtual void changeEvent(QEvent *e);
};

class PmhPreferencesPage : public Core::IOptionsPage
{
public:
    PmhPreferencesPage(QObject *parent = 0);
    ~PmhPreferencesPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    QString helpPage() {return "parametrer.html";}

    static void writeDefaultSettings(Core::ISettings *s) {Internal::PmhPreferencesWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);
private:
    QPointer<Internal::PmhPreferencesWidget> m_Widget;
};


}
}

#endif // PMHPREFERENCESPAGE_H
