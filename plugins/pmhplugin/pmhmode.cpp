/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/




#include "pmhbase.h"
#include "pmhmode.h"
#include "pmhmodel.h"
#include "pmhcore.h"
#include "pmhcategorymodel.h"
#include "pmhcreatordialog.h"
#include "pmhviewer.h"
#include "constants.h"

#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/ipatient.h>
#include <coreplugin/imainwindow.h>
#include <coreplugin/constants_menus.h>
#include <coreplugin/constants_icons.h>
#include <coreplugin/actionmanager/actionmanager.h>

#include <categoryplugin/categoryitem.h>
#include <categoryplugin/categorydialog.h>

#include <formmanagerplugin/iformitem.h>
#include <formmanagerplugin/iformwidgetfactory.h>

#include <utils/global.h>
#include <utils/log.h>
#include <translationutils/constanttranslations.h>
#include <extensionsystem/pluginmanager.h>

#include <QItemSelectionModel>
#include <QToolBar>
#include <QPushButton>
#include <QScrollBar>

#include "ui_pmhmodewidget.h"

using namespace PMH;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline ExtensionSystem::PluginManager *pluginManager() { return ExtensionSystem::PluginManager::instance(); }

static inline PmhCategoryModel *catModel() {return PmhCore::instance()->pmhCategoryModel();}
static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline Core::IPatient *patient()  { return Core::ICore::instance()->patient(); }
static inline Core::ActionManager *actionManager() {return Core::ICore::instance()->actionManager();}
static inline PmhCore *pmhCore() {return PmhCore::instance();}
static inline PmhBase *base() {return PmhBase::instance();}

namespace {

const char * const TREEVIEW_SHEET =
        " QTreeView {"
        "    show-decoration-selected: 1;"
        "}"

        "QTreeView::item {"
        "    border: 0px solid #d9d9d9;"
        "}"

        "QTreeView::item:hover {"
        "    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #e7effd, stop: 1 #cbdaf1);"
        "}"



        "QTreeView::item:selected {"
        "    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #6ea1f1, stop: 1 #567dbc);"
        "}"


        ;
}

PmhModeWidget::PmhModeWidget(QWidget *parent) :
        PmhContextualWidget(parent),
        ui(new Ui::PmhModeWidget),
        m_EditButton(0)
{
    qDebug() << Q_FUNC_INFO;
    ui->setupUi(this);
    ui->pmhViewer->setEditMode(PmhViewer::ReadOnlyMode);
    ui->treeViewLayout->setMargin(0);
    layout()->setMargin(0);

    ui->formDataMapper->initialize();
    ui->treeView->setActions(0);
    ui->treeView->setCommands(QStringList()
                              << Constants::A_PMH_NEW
                              << Constants::A_PMH_REMOVE
                              << Constants::A_PMH_CATEGORYMANAGER);
    ui->treeView->addContext(Core::Context(Constants::C_PMH_PLUGINS));
    ui->treeView->setModel(catModel());
    ui->treeView->header()->hide();
    ui->treeView->setStyleSheet(::TREEVIEW_SHEET);

    Core::Command *cmd = 0;
    cmd = actionManager()->command(Constants::A_PMH_REMOVE);
    connect(cmd->action(), SIGNAL(triggered()), this, SLOT(removeItem()));

    m_EditButton = new QPushButton(ui->buttonBox);
    m_EditButton->setText(tkTr(Trans::Constants::M_EDIT_TEXT));
    ui->buttonBox->addButton(m_EditButton, QDialogButtonBox::YesRole);
    ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(false);
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setEnabled(false);

    for(int i=0; i < catModel()->columnCount(); ++i)
        ui->treeView->hideColumn(i);
    ui->treeView->showColumn(PmhCategoryModel::Label);
    ui->treeView->header()->setStretchLastSection(false);
    ui->treeView->header()->setSectionResizeMode(PmhCategoryModel::Label, QHeaderView::Stretch);

    cmd = actionManager()->command(Constants::A_PMH_NEW);
    connect(cmd->action(), SIGNAL(triggered()), this, SLOT(createPmh()));

    connect(ui->treeView->selectionModel(), SIGNAL(currentChanged(QModelIndex, QModelIndex)),
            this, SLOT(currentChanged(QModelIndex, QModelIndex)));

    connect(ui->treeView->model(), SIGNAL(rowsInserted(QModelIndex,int,int)),
            this, SLOT(pmhModelRowsInserted(QModelIndex,int,int)));

    connect(ui->buttonBox, SIGNAL(clicked(QAbstractButton*)), this, SLOT(onButtonClicked(QAbstractButton*)));

    connect(patient(), SIGNAL(currentPatientChanged()), this, SLOT(onCurrentPatientChanged()));
}

PmhModeWidget::~PmhModeWidget()
{
    delete ui;
}

int PmhModeWidget::currentSelectedCategory() const
{
    qDebug() << Q_FUNC_INFO;
    if (!ui->treeView->selectionModel()->hasSelection())
        return -1;
    QModelIndex item = ui->treeView->selectionModel()->currentIndex();
    while (!catModel()->isCategory(item)) {
        item = item.parent();
    }
    Category::CategoryItem *cat = catModel()->categoryForIndex(item);
    if (cat) {
        return cat->id();
    }
    return -1;
}

void PmhModeWidget::currentChanged(const QModelIndex &current, const QModelIndex &previous)
{
    qDebug() << Q_FUNC_INFO;
    if (previous.isValid()) {
        if (catModel()->isForm(previous)) {
            if (ui->formDataMapper->isDirty())
                ui->formDataMapper->submit();
            ui->formDataMapper->clear();
            catModel()->refreshSynthesis();
        }
    }
    if (!current.isValid())
        return;
    if (patient()->uuid().isEmpty())
        return;

    ui->formDataMapper->setCurrentForm(0);

    if (catModel()->isSynthesis(current)) {
        ui->pmhSynthesisBrowser->setHtml(catModel()->synthesis());
        ui->stackedWidget->setCurrentWidget(ui->pageSynthesis);
    } else if (catModel()->isCategory(current)) {
        ui->pmhSynthesisBrowser->setHtml(catModel()->synthesis(current));
        ui->stackedWidget->setCurrentWidget(ui->pageSynthesis);
    } else if (catModel()->isForm(current)) {
        const QString &formUid = catModel()->index(current.row(), PmhCategoryModel::Id, current.parent()).data().toString();
        ui->stackedWidget->setCurrentWidget(ui->formPage);
        ui->formDataMapper->setCurrentForm(formUid);
        ui->formDataMapper->setLastEpisodeAsCurrent();
    } else if (catModel()->isPmhx(current)) {
        ui->stackedWidget->setCurrentWidget(ui->pageEditor);
        ui->pmhViewer->setPmhData(catModel()->pmhDataforIndex(current));
    }

    ui->scrollArea->horizontalScrollBar()->setValue(0);
    ui->scrollArea->verticalScrollBar()->setValue(0);
}

void PmhModeWidget::onButtonClicked(QAbstractButton *button)
{
    qDebug() << Q_FUNC_INFO;
    if (button==m_EditButton) {
        ui->pmhViewer->setEditMode(PmhViewer::ReadWriteMode);
        ui->buttonBox->button(QDialogButtonBox::Cancel)->setEnabled(true);
        ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(true);
        return;
    }

    switch (ui->buttonBox->standardButton(button)) {
    case QDialogButtonBox::Save:
        {
            Internal::PmhData *pmh = ui->pmhViewer->modifiedPmhData();
            pmhCore()->pmhCategoryModel()->addPmhData(pmh);
            pmhCore()->pmhCategoryModel()->addPmhData(pmh);
            QModelIndex idx = catModel()->indexForPmhData(pmh);
            ui->treeView->expand(idx);
            ui->pmhViewer->setEditMode(PmhViewer::ReadOnlyMode);
            ui->buttonBox->button(QDialogButtonBox::Cancel)->setEnabled(false);
            ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(false);
            break;
        }
    case QDialogButtonBox::Cancel:
        ui->pmhViewer->revert();
        ui->pmhViewer->setEditMode(PmhViewer::ReadOnlyMode);
        ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(false);
        ui->buttonBox->button(QDialogButtonBox::Cancel)->setEnabled(false);
        break;
    default: break;
    }
}

void PmhModeWidget::createCategory()
{
    qDebug() << Q_FUNC_INFO;
    Category::CategoryDialog dlg(this);
    dlg.setModal(true);
    dlg.setCategoryModel(catModel(), PmhCategoryModel::Label);
    dlg.exec();
}

void PmhModeWidget::removeItem()
{
    qDebug() << Q_FUNC_INFO;
    if (!ui->treeView->selectionModel()->hasSelection())
        return;
    QModelIndex item = ui->treeView->selectionModel()->currentIndex();

    if (catModel()->isCategory(item) || catModel()->isForm(item))
        return;

    QModelIndex category;
    while (true) {
        if (catModel()->isCategory(item.parent())) {
            category = item.parent();
            break;
        }
    }
    qDebug() << "item: " << item;
    qDebug() << "item category: " << category;

    bool yes = Utils::yesNoMessageBox(tr("Remove PMHx"),
                                      tr("Do you really want to remove the PMHx called <br />&nbsp;&nbsp;&nbsp;<b>%1</b>?").arg(item.data().toString()));
    if (yes) {
        qDebug() << "item.row(): " << item.row()
                 << "item.parent(): " << item.parent();
        catModel()->removeRow(item.row(), item.parent());
    }
}

void PmhModeWidget::onCurrentPatientChanged()
{
    if (ui->formDataMapper->isDirty())
        ui->formDataMapper->submit();
    ui->formDataMapper->clear();
    catModel()->refreshSynthesis();
    ui->treeView->selectionModel()->select(catModel()->index(0,0), QItemSelectionModel::SelectCurrent);
    ui->treeView->expandAll();
}

void PmhModeWidget::createPmh()
{
    qDebug() << Q_FUNC_INFO;
    PmhCreatorDialog dlg(this);
    if (ui->treeView->selectionModel()->hasSelection()) {
        QModelIndex item = ui->treeView->selectionModel()->currentIndex();
        while (!catModel()->isCategory(item)) {
            item = item.parent();
        }
        dlg.setCategory(catModel()->categoryForIndex(item));
    }
    Utils::resizeAndCenter(&dlg, Core::ICore::instance()->mainWindow());
    dlg.exec();
}

void PmhModeWidget::pmhModelRowsInserted(const QModelIndex &parent, int start, int end)
{
    qDebug() << Q_FUNC_INFO;
    ui->treeView->expand(parent);
    for(int i=start; i != end+1; ++i) {
        ui->treeView->expand(catModel()->index(i, PmhCategoryModel::Label, parent));
    }
    catModel()->refreshSynthesis();
    if (catModel()->isSynthesis(ui->treeView->currentIndex())) {
        ui->pmhSynthesisBrowser->setHtml(catModel()->synthesis());
        ui->stackedWidget->setCurrentWidget(ui->pageSynthesis);
    }

}

void PmhModeWidget::hideEvent(QHideEvent *event)
{
    if (isVisible() && ui->formDataMapper->isDirty()) {
        ui->formDataMapper->submit();
        catModel()->refreshSynthesis();
    }
    QWidget::hideEvent(event);
}

void PmhModeWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        m_EditButton->setText(tkTr(Trans::Constants::M_EDIT_TEXT));
        break;
    default:
        break;
    }
}

PmhMode::PmhMode(QObject *parent) :
    Core::IMode(parent),
    m_inPluginManager(false)
{
    setDisplayName(tkTr(Trans::Constants::PMHX));
    setIcon(theme()->icon(Core::Constants::ICONPATIENTHISTORY, Core::ITheme::BigIcon));
    setPriority(Core::Constants::P_MODE_PATIENT_HISTORY);
    setId(Core::Constants::MODE_PATIENT_HISTORY);
    setPatientBarVisibility(true);
    setEnabledOnlyWithCurrentPatient(true);


    m_Widget = new PmhModeWidget;
    setWidget(m_Widget);
    onCurrentPatientChanged();
    connect(patient(), SIGNAL(currentPatientChanged()), this, SLOT(onCurrentPatientChanged()));
}

PmhMode::~PmhMode()
{
    if (m_inPluginManager) {
        pluginManager()->removeObject(this);
    }
}

QString PmhMode::name() const
{
    return tkTr(Trans::Constants::PMHX);
}

void PmhMode::onCurrentPatientChanged()
{
    if (!m_inPluginManager) {
        pluginManager()->addObject(this);
        m_inPluginManager = true;
    }
}

