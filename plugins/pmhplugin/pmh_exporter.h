/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef MEDICALHISTORYEXPORTER_H
#define MEDICALHISTORYEXPORTER_H

/**
 * \file ./plugins/pmhplugin/pmh_exporter.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

#include <qglobal.h>

#if defined(PMH_LIBRARY)
#define PMH_EXPORT Q_DECL_EXPORT
#else
#define PMH_EXPORT Q_DECL_IMPORT
#endif

#endif
