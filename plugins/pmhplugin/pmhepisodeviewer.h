/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PMHEPISODEVIEWER_H
#define PMHEPISODEVIEWER_H

#include <QWidget>
class QModelIndex;

/**
 * \file ./plugins/pmhplugin/pmhepisodeviewer.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace PMH {
namespace Internal {
class PmhEpisodeViewerPrivate;
class PmhData;
}

namespace Ui {
    class PmhEpisodeViewer;
}

class PmhEpisodeViewer : public QWidget
{
    Q_OBJECT

public:
    explicit PmhEpisodeViewer(QWidget *parent = 0);
    ~PmhEpisodeViewer();

    void setPmhData(Internal::PmhData *pmh);
    void clear();

private Q_SLOTS:
    void itemActivated(const QModelIndex &item);
    void resizeTableView();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::PmhEpisodeViewer *ui;
    Internal::PmhEpisodeViewerPrivate *d;
};

}  // End namespace PMH

#endif // PMHEPISODEVIEWER_H
