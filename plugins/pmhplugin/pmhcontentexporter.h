/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PMH_INTERNAL_PMHCONTENTEXPORTER_H
#define PMH_INTERNAL_PMHCONTENTEXPORTER_H

#include <coreplugin/ipatientdataexporter.h>
#include <QObject>

/**
 * \file ./plugins/pmhplugin/pmhcontentexporter.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace PMH {
namespace Internal {
class PmhContentExporterPrivate;

class PmhContentExporter : public Core::IPatientDataExporter
{
    Q_OBJECT
    
public:
    explicit PmhContentExporter(QObject *parent = 0);
    ~PmhContentExporter();
    
    bool initialize();
    bool isBusy() const;

public Q_SLOTS:
    Core::PatientDataExtraction *startExportationJob(const Core::PatientDataExporterJob &job);

private:
    PmhContentExporterPrivate *d;
};

} // namespace Internal
} // namespace PMH

#endif // PMH_INTERNAL_PMHCONTENTEXPORTER_H

