/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PMHMODE_H
#define PMHMODE_H

#include <coreplugin/modemanager/imode.h>
#include <pmhplugin/pmhcontextualwidget.h>

#include <QObject>
#include <QHash>
#include <QString>
#include <QStyledItemDelegate>

QT_BEGIN_NAMESPACE
class QModelIndex;
class QAbstractButton;
class QPushButton;
class QToolBar;
QT_END_NAMESPACE

/**
 * \file ./plugins/pmhplugin/pmhmode.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace PMH {
namespace Internal {
namespace Ui {
class PmhModeWidget;
}

class PmhModeWidget : public PMH::PmhContextualWidget
{
    Q_OBJECT
public:
    PmhModeWidget(QWidget *parent = 0);
    ~PmhModeWidget();

    int currentSelectedCategory() const;


private Q_SLOTS:
    void currentChanged(const QModelIndex &current, const QModelIndex &previous);
    void onButtonClicked(QAbstractButton *button);
    void createCategory();
    void removeItem();
    void onCurrentPatientChanged();
    void createPmh();
    void pmhModelRowsInserted(const QModelIndex &parent, int start, int end);

private:
    void hideEvent(QHideEvent *event);
    void changeEvent(QEvent *e);
//    bool eventFilter(QObject *o, QEvent *e);

private:
    Ui::PmhModeWidget *ui;
    QToolBar *m_ToolBar;
    QPushButton *m_EditButton;
};

class PmhMode : public Core::IMode
{
    Q_OBJECT
public:
    explicit PmhMode(QObject *parent = 0);
    ~PmhMode();

    QString name() const;

private Q_SLOTS:
    void onCurrentPatientChanged();

private:
    QWidget *m_Widget;
    bool m_inPluginManager;
};

}  // End namespace Internal
}  // End namespace PMH

#endif // PMHMODE_H

