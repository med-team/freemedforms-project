/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PMHCREATORDIALOG_H
#define PMHCREATORDIALOG_H

#include <QDialog>
class QAbstractButton;

/**
 * \file ./plugins/pmhplugin/pmhcreatordialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Category {
class CategoryItem;
}

namespace PMH {
namespace Ui {
    class PmhCreatorDialog;
}

class PmhCreatorDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PmhCreatorDialog(QWidget *parent = 0);
    ~PmhCreatorDialog();

    void setCategory(Category::CategoryItem *category);

private Q_SLOTS:
    void on_buttonBox_clicked(QAbstractButton *button);
    void helpRequested();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::PmhCreatorDialog *ui;
};

}

#endif // PMHCREATORDIALOG_H
