/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PMHCONTEXTUALWIDGET_H
#define PMHCONTEXTUALWIDGET_H

#include <QWidget>

/**
 * \file ./plugins/pmhplugin/pmhcontextualwidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
 * \internal
*/

namespace PMH {
namespace Internal {
class PmhContext;
}

class PmhContextualWidget : public QWidget
{
    Q_OBJECT
public:
    PmhContextualWidget(QWidget *parent = 0);
    virtual ~PmhContextualWidget();

private:
    Internal::PmhContext *m_Context;
};

}  // End namespace PMH

#endif // PMHCONTEXTUALWIDGET_H
