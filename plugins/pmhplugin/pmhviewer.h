/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PMHVIEWER_H
#define PMHVIEWER_H

#include <QWidget>

/**
 * \file ./plugins/pmhplugin/pmhviewer.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Category {
class CategoryItem;
}

namespace PMH {
namespace Internal {
class PmhViewerPrivate;
class PmhData;
}  // End namespace Internal

class PmhViewer : public QWidget
{
    Q_OBJECT

public:
    enum EditMode {
        ReadOnlyMode,
        ReadWriteMode
    };
    enum ViewMode {
        ExtendedMode,
        SimpleMode
    };

    PmhViewer(QWidget *parent = 0, EditMode editMode = ReadOnlyMode, ViewMode viewMode = ExtendedMode);
    ~PmhViewer();

    void setPatientInfoVisible(bool visible);
    void setEditMode(EditMode mode);
    void setPmhData(Internal::PmhData *pmh);
    void setCategoryForPmh(Category::CategoryItem *category);
    void createNewPmh();
    void revert();

    Internal::PmhData *modifiedPmhData() const;

protected Q_SLOTS:
    void onSimpleViewIcdClicked();
    void onSimpleViewLabelChanged(const QString &text);

protected:
    void changeEvent(QEvent *e);

private:
    Internal::PmhViewerPrivate *d;
};

}  // End namespace PMH

#endif // PMHVIEWER_H
