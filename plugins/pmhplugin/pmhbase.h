/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PMHBASE_H
#define PMHBASE_H

#include <utils/database.h>

/**
 * \file ./plugins/pmhplugin/pmhbase.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Category {
class CategoryItem;
}

namespace PMH {
namespace Internal {
class PmhPlugin;
class PmhData;
class PmhCategory;
class PmhEpisodeData;

class PmhBase : public QObject, public Utils::Database
{
    Q_OBJECT
    friend class PMH::Internal::PmhPlugin;

protected:
    PmhBase(QObject *parent = 0);

public:
    // Constructor
    static PmhBase *instance();
    virtual ~PmhBase();

    // initialize
    bool initialize();

    // Database getters
    QVector<PmhData *> getPmh(const QString &patientUid = QString::null) const;
    QVector<Category::CategoryItem *> getPmhCategory(const QString &uid) const;
    QList<Category::CategoryItem *> createCategoryTree(const QVector<Category::CategoryItem *> &cats) const;
    bool linkPmhWithCategory(const QVector<Category::CategoryItem *> &cats, const QVector<PmhData *> &pmhs) const;

    // Database setters
    bool savePmhData(PmhData *pmh);
    bool updatePmhData(PmhData *pmh);
    bool savePmhEpisodeData(PmhEpisodeData *episode);
    bool updatePmhEpsisodeData(PmhEpisodeData *episode);
    bool savePmhCategory(Category::CategoryItem *category);

private:
    bool createDatabase(const QString &connectionName, const QString &dbName,
                          const QString &pathOrHostName,
                          TypeOfAccess access, AvailableDrivers driver,
                          const QString &login, const QString &pass,
                          const int port,
                          CreationOption createOption
                         );

private Q_SLOTS:
    void onCoreDatabaseServerChanged();
    void onCoreFirstRunCreationRequested();

private:
    bool m_initialized;
    static PmhBase *m_Instance;
};

}  // End namespace Internal
}  // End namespace PMH


#endif // PMHBASE_H
