/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PMHDATA_H
#define PMHDATA_H

#include <translationutils/constanttranslations.h>

#include <categoryplugin/icategorycontentitem.h>

#include <QVariant>

/**
 * \file ./plugins/pmhplugin/pmhdata.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace ICD {
class IcdCollectionModel;
}

namespace PMH {
class PmhEpisodeModel;

namespace Internal {
class PmhData;
class PmhDataPrivate;
class PmhEpisodeDataPrivate;

class PmhEpisodeData {
public:
    enum DataRepresentation {
        Label = 0,
        DateStart,
        DateEnd,
        ConfidenceIndex,
        IcdCodeList,
        IcdLabelStringList,
        IcdLabelHtmlList,
        IcdXml,
        Comment,
        DbOnly_Id,
        DbOnly_IsValid,
        DbOnly_MasterId
    };

    PmhEpisodeData();
    ~PmhEpisodeData();

    bool setData(const int ref, const QVariant &value);
    QVariant data(const int ref) const;

    ICD::IcdCollectionModel *icdModel() const;

private:
    PmhEpisodeDataPrivate *d;
};


class PmhData : public Category::ICategoryContentItem
{
public:
    enum DataRepresentation {
        Uid = 0,
        UserOwner,
        PatientUid,
        Label,
        Type,
        State,
        IsValid,
        ConfidenceIndex,
        Comment,
        CategoryId,
        IsPrivate,
        DbOnly_MasterEpisodeId,
        DbOnly_MasterContactId,
    };

    PmhData();
    ~PmhData();

    void populateWithCurrentData();
    bool setData(const int ref, const QVariant &value);
    QVariant data(const int ref) const;

    void addEpisode(PmhEpisodeData *episode);
    bool insertEpisode(int pos, PmhEpisodeData *episode);
    bool removeEpisode(PmhEpisodeData *episode);
    QList<PmhEpisodeData *> episodes() const;
    PmhEpisodeModel *episodeModel();


    // Category::ICategoryContentItem
    int categoryId() const {return data(CategoryId).toInt();}
    void setCategory(Category::CategoryItem *cat);
    Category::CategoryItem *category() const;

private:
    PmhDataPrivate *d;
};


} // End namespace Internal
} // End namespace PMH

#endif // PMHDATA_H
