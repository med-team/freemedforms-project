/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "pmhcategorydialog.h"
#include "pmhcategorymodel.h"
#include "categorylabelsmodel.h"

#include <listviewplugin/languagecombobox.h>
#include <listviewplugin/languagecomboboxdelegate.h>

#include "ui_pmhcategorywidget.h"

#include <QGridLayout>
#include <QDebug>


using namespace PMH;
using namespace Internal;

namespace PMH {
namespace Internal {
class PmhCategoryDialogPrivate
{
public:
    PmhCategoryDialogPrivate() : ui(0), m_Model(0), m_CatLabelsModel(0)
    {
    }

    ~PmhCategoryDialogPrivate()
    {
        if (m_CatLabelsModel)
            delete m_CatLabelsModel;
        m_CatLabelsModel = 0;
    }

public:
    Ui::PmhCategoryWidget *ui;
    PmhCategoryModel *m_Model;
    CategoryLabelsModel *m_CatLabelsModel;
};

}
}



PmhCategoryDialog::PmhCategoryDialog(QWidget *parent) :
        QDialog(parent), d(new Internal::PmhCategoryDialogPrivate)
{
    d->ui = new Ui::PmhCategoryWidget;
    d->ui->setupUi(this);
    d->ui->treeView->header()->hide();
    d->ui->treeView->header()->setStretchLastSection(true);
    connect(d->ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(d->ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}

void PmhCategoryDialog::setPmhCategoryModel(PmhCategoryModel *model)
{
    Q_ASSERT(model);
    d->m_Model = model;
    d->ui->treeView->setModel(model->categoryOnlyModel());
    d->ui->treeView->hideColumn(PmhCategoryModel::Id);
    d->ui->treeView->hideColumn(PmhCategoryModel::Type);
    d->ui->treeView->hideColumn(PmhCategoryModel::EmptyColumn);
    d->ui->treeView->header()->setStretchLastSection(false);
    d->ui->treeView->header()->setResizeMode(PmhCategoryModel::Label, QHeaderView::Stretch);
    connect(d->ui->treeView->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this, SLOT(editItem(QModelIndex, QModelIndex)));
}

void PmhCategoryDialog::editItem(const QModelIndex &current, const QModelIndex &)
{
    QModelIndex sourceItem = d->m_Model->categoryOnlyModel()->mapToSource(current);
    PmhCategory *cat = d->m_Model->pmhCategoryforIndex(sourceItem);
    if (!cat)
        return;
    if (!d->m_CatLabelsModel) {
        d->m_CatLabelsModel = new CategoryLabelsModel(this);
    }
    d->m_CatLabelsModel->setPmhCategoryData(cat);
    d->ui->tableView->setModel(d->m_CatLabelsModel);
    d->ui->tableView->setItemDelegateForColumn(CategoryLabelsModel::Lang, new Views::LanguageComboBoxDelegate(this));
    d->ui->tableView->horizontalHeader()->setStretchLastSection(true);
}

void PmhCategoryDialog::done(int r)
{
    QDialog::done(r);
}
