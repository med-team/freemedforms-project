/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "pmhcreatordialog.h"
#include "pmhcore.h"
#include "pmhbase.h"
#include "pmhdata.h"
#include "pmhcategorymodel.h"
#include "constants.h"

#include <coreplugin/dialogs/helpdialog.h>
#include <coreplugin/icore.h>

#include "ui_pmhcreatordialog.h"

#include <QDebug>

using namespace PMH;

static inline PmhCore *pmhCore() {return PmhCore::instance();}

PmhCreatorDialog::PmhCreatorDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PmhCreatorDialog)
{
    ui->setupUi(this);
    setWindowTitle(tr("Past Medical History Creator"));
    ui->pmhViewer->setEditMode(PmhViewer::ReadWriteMode);
    ui->pmhViewer->createNewPmh();

}

PmhCreatorDialog::~PmhCreatorDialog()
{
    delete ui;
}

void PmhCreatorDialog::setCategory(Category::CategoryItem *category)
{
    ui->pmhViewer->setCategoryForPmh(category);
}

void PmhCreatorDialog::on_buttonBox_clicked(QAbstractButton *button)
{
    switch (ui->buttonBox->standardButton(button)) {
    case QDialogButtonBox::Save:
        {
            Internal::PmhData *pmh = ui->pmhViewer->modifiedPmhData();
            pmhCore()->pmhCategoryModel()->addPmhData(pmh);
            accept();
            break;
        }
    case QDialogButtonBox::Cancel: reject(); break;
    case QDialogButtonBox::Help: helpRequested(); break;
    default: break;
    }
}

void PmhCreatorDialog::helpRequested()
{
    Core::HelpDialog::showPage(Constants::H_PMH_CREATOR_PAGE);
}

void PmhCreatorDialog::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
