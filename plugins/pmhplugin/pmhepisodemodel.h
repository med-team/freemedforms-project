/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PMHEPISODEMODEL_H
#define PMHEPISODEMODEL_H

#include <QAbstractListModel>

/**
 * \file ./plugins/pmhplugin/pmhepisodemodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace PMH {
namespace Internal {
class PmhEpisodeModelPrivate;
class PmhData;
}  // End namespace Internal

class PmhEpisodeModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum DataRepresentation {
        DateStart = 0,
        DateEnd,
        Label,
        IcdCodeList,
        IcdLabelStringList,
        IcdXml,
        Contact,
        EmptyColumn,
        ColumnCount
    };

    explicit PmhEpisodeModel(Internal::PmhData *pmh, QObject *parent = 0);
    ~PmhEpisodeModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());

    Qt::ItemFlags flags(const QModelIndex &index) const;

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

private:
    Internal::PmhEpisodeModelPrivate *d;
};


}  // End namespace PMH


#endif // PMHEPISODEMODEL_H
