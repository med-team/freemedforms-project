/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PMHWIDGETMANAGER_H
#define PMHWIDGETMANAGER_H

#include <pmhplugin/pmh_exporter.h>
#include <coreplugin/contextmanager/icontext.h>

#include <QObject>
#include <QPointer>

/**
 * \file ./plugins/pmhplugin/pmhwidgetmanager.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace PMH {
class PmhCore;
class PmhContextualWidget;
namespace Internal {

class PmhActionHandler : public QObject
{
    Q_OBJECT
public:
    PmhActionHandler(QObject *parent = 0);
    virtual ~PmhActionHandler() {}

    void setCurrentView(PmhContextualWidget *view);

private Q_SLOTS:
    void onCurrentPatientChanged();
    void showPmhDatabaseInformation();
    void categoryManager();

private:
    void updateActions();

protected:
    QAction *aAddPmh;
    QAction *aRemovePmh;
    QAction *aAddCat;
    QAction *aCategoryManager;
    QAction *aPmhDatabaseInformation;
    QPointer<PmhContextualWidget> m_CurrentView;
};

class PmhWidgetManager : private PmhActionHandler
{
    Q_OBJECT
    friend class PMH::PmhCore;

protected:
    PmhWidgetManager(QObject *parent = 0);

public:
    PmhContextualWidget *currentView() const;

private Q_SLOTS:
    void updateContext(Core::IContext *object, const Core::Context &additionalContexts);
};

}  // End namespace Internal
}  // End namespace PMH

#endif // PMHWIDGETMANAGER_H
