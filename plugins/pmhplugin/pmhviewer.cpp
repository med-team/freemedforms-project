/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "pmhviewer.h"
#include "pmhdata.h"
#include "constants.h"
#include "pmhepisodemodel.h"
#include "pmhcore.h"
#include "pmhcategorymodel.h"

#include <coreplugin/icore.h>
#include <coreplugin/ipatient.h>
#include <coreplugin/iuser.h>

#include <icdplugin/icdcollectiondialog.h>
#include <icdplugin/icdio.h>

#include <categoryplugin/categoryonlyproxymodel.h>

#include <utils/global.h>
#include <utils/log.h>
#include <translationutils/constants.h>

#include "ui_pmhviewer.h"

#include <QStringListModel>


using namespace PMH;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Core::IUser *user() {return Core::ICore::instance()->user();}
static inline Core::IPatient *patient() {return Core::ICore::instance()->patient();}
static inline PMH::PmhCore *pmhCore() { return PMH::PmhCore::instance(); }


namespace PMH {
namespace Internal {

class PmhViewerPrivate {
public:
    PmhViewerPrivate(PmhViewer *parent) :
        ui(0),
        m_Pmh(0),
        q(parent)
    {
        m_ShowPatient = patient() != 0;
    }

    ~PmhViewerPrivate()
    {
        delete ui; ui=0;
    }

    void setEditMode(PmhViewer::EditMode mode)
    {
        m_Mode = mode;
        Q_ASSERT(ui);
        bool enable = (mode == PmhViewer::ReadWriteMode);
        q->setEnabled(enable);
    }

    void populateUiWithPmh(PmhData *pmh)
    {
        m_Pmh = pmh;
        ui->personalLabel->setText(pmh->data(PmhData::Label).toString());
        ui->userNameLabel->setText(user()->fullNameOfUser(pmh->data(PmhData::UserOwner)));
        ui->typeCombo->setCurrentIndex(pmh->data(PmhData::Type).toInt());
        ui->statusCombo->setCurrentIndex(pmh->data(PmhData::State).toInt());
        ui->confIndexSlider->setValue(pmh->data(PmhData::ConfidenceIndex).toInt());
        ui->makePrivateBox->setChecked(pmh->data(PmhData::IsPrivate).toBool());
        ui->comment->setHtml(pmh->data(PmhData::Comment).toString());
        QModelIndex cat = pmhCore()->pmhCategoryModel()->indexForCategory(pmh->category());
        cat = pmhCore()->pmhCategoryModel()->categoryOnlyModel()->mapFromSource(cat);
        ui->categoryTreeView->setCurrentIndex(cat);

        ui->episodeViewer->setPmhData(pmh);

        ui->simple_start_date->clear();
        ui->simple_end_date->clear();
        m_IcdLabelModel->setStringList(QStringList());
        if (pmh->episodeModel()->rowCount()) {
            ui->simple_start_date->setDate(pmh->episodeModel()->index(0, PmhEpisodeModel::DateStart).data().toDate());
            ui->simple_end_date->setDate(pmh->episodeModel()->index(0, PmhEpisodeModel::DateEnd).data().toDate());
            m_IcdLabelModel->setStringList(pmh->episodeModel()->index(0, PmhEpisodeModel::IcdLabelStringList).data().toStringList());
        }


        ui->simple_icd10->setEnabled(ICD::IcdIO::isDatabaseInitialized());
    }

    void populatePmhWithUi()
    {
        qDebug() << Q_FUNC_INFO;
        m_Pmh->setData(PmhData::Label, ui->personalLabel->text());
        m_Pmh->setData(PmhData::Type, ui->typeCombo->currentIndex());
        m_Pmh->setData(PmhData::State, ui->statusCombo->currentIndex());
        m_Pmh->setData(PmhData::ConfidenceIndex, ui->confIndexSlider->value());
        m_Pmh->setData(PmhData::Comment, ui->comment->textEdit()->toHtml());
        m_Pmh->setData(PmhData::IsPrivate, ui->makePrivateBox->isChecked());
        QModelIndex cat = pmhCore()->pmhCategoryModel()->categoryOnlyModel()->mapToSource(ui->categoryTreeView->currentIndex());
        qDebug() << cat;
        cat = pmhCore()->pmhCategoryModel()->index(cat.row(), PmhCategoryModel::Id, cat.parent());
        m_Pmh->setData(PmhData::CategoryId, cat.data().toInt());
        qDebug() << cat;
        if (m_Pmh->episodeModel()->rowCount() == 0) {
            m_Pmh->episodeModel()->insertRow(0);
        }
        m_Pmh->episodeModel()->setData(m_Pmh->episodeModel()->index(0, PmhEpisodeModel::DateStart), ui->simple_start_date->date());
        m_Pmh->episodeModel()->setData(m_Pmh->episodeModel()->index(0, PmhEpisodeModel::DateEnd), ui->simple_end_date->date());
        m_Pmh->episodeModel()->setData(m_Pmh->episodeModel()->index(0, PmhEpisodeModel::IcdLabelStringList), m_IcdLabelModel->stringList());
    }

    void clearUi()
    {
        ui->personalLabel->clear();
        ui->typeCombo->clear();
        ui->statusCombo->clear();
        ui->categoryTreeView->setModel(0);
        ui->episodeViewer->clear();
        ui->simple_icd10->setEnabled(ICD::IcdIO::isDatabaseInitialized());
    }

public:
    Internal::Ui::PmhViewer *ui;
    PmhViewer::EditMode m_Mode;
    PmhViewer::ViewMode m_ViewMode;
    PmhData *m_Pmh;
    bool m_ShowPatient;
    QStringListModel *m_IcdLabelModel;

private:
    PmhViewer *q;
};

} // End namespace Internal
} // End namespace PMH


/** \brief Creates a new PMH::PmhViewer with the specified \e editMode.
 *
 * ViewMode can be set to SimpleMode or ExtendedMode (with widgetBar:
 * Episodes, Management, Contacts, Links, Comment)
 * By default ViewMode is set to ExtendedMode.
 * tabWidget is hidden for now until we fully implement episode management.
 * \sa PMH::PmhViewer::EditMode
*/
PmhViewer::PmhViewer(QWidget *parent, EditMode editMode, ViewMode viewMode) :
    QWidget(parent), d(new PmhViewerPrivate(this))
{
    d->ui = new Internal::Ui::PmhViewer;
    d->ui->setupUi(this);
    d->ui->simple_start_date->setNullable(true);
    d->ui->simple_start_date->setCalendarPopup(true);
    d->ui->simple_start_date->setDate(QDate()); // Set date as null with QDate()
    d->ui->simple_end_date->setNullable(true);
    d->ui->simple_end_date->setCalendarPopup(true);
    d->ui->simple_end_date->setDate(QDate()); // Set date as null with QDate()
    d->ui->simple_icd10->setEnabled(ICD::IcdIO::isDatabaseInitialized());
    d->m_IcdLabelModel = new QStringListModel(this);

    d->ui->typeCombo->addItems(Constants::availableTypes());
    d->ui->statusCombo->addItems(Constants::availableStatus());

    d->ui->tabWidget->setCurrentWidget(d->ui->episodesTab);
    d->ui->comment->toogleToolbar(true);

    d->setEditMode(editMode);
    setPatientInfoVisible(d->m_ShowPatient);

    d->m_ViewMode = viewMode;
    if (viewMode==ExtendedMode) {
        connect(d->ui->personalLabel, SIGNAL(textChanged(QString)), this, SLOT(onSimpleViewLabelChanged(QString)));
        d->ui->tabWidget->setTabEnabled(0, false); // Episodes tab disabled
        d->ui->tabWidget->setTabEnabled(1, false); // Management tab disabled
        d->ui->tabWidget->setTabEnabled(2, false); // Contacts tab disabled
        d->ui->tabWidget->setTabEnabled(3, false); // Links tab disabled
        d->ui->tabWidget->hide();
        d->ui->icdCodes->setModel(d->m_IcdLabelModel);
    } else {  // SimpleMode
        d->ui->tabWidget->hide();
        d->ui->icdCodes->setModel(d->m_IcdLabelModel);
        connect(d->ui->personalLabel, SIGNAL(textChanged(QString)), this, SLOT(onSimpleViewLabelChanged(QString)));
    }

    d->ui->categoryTreeView->setModel(pmhCore()->pmhCategoryModel()->categoryOnlyModel());
    d->ui->categoryTreeView->expandAll();
    connect(pmhCore()->pmhCategoryModel()->categoryOnlyModel(), SIGNAL(layoutChanged()),
            d->ui->categoryTreeView, SLOT(expandAll()));
    connect(d->ui->simple_icd10, SIGNAL(clicked()), this, SLOT(onSimpleViewIcdClicked()));
}

PmhViewer::~PmhViewer()
{
    delete d;
    d = 0;
}

void PmhViewer::setPatientInfoVisible(bool visible)
{
    QString text;
    if (visible) {
        text = QString("%1, %2").arg(
                    patient()->data(Core::IPatient::FullName).toString(),
                    patient()->data(Core::IPatient::DateOfBirth).toString());
    } else {
        text = tkTr(Trans::Constants::PASTMEDICALHISTORY);
    }
    d->ui->titleLabel->setText(text);
}

void PmhViewer::setEditMode(EditMode mode)
{
    d->ui->simple_icd10->setEnabled(ICD::IcdIO::isDatabaseInitialized());
    d->setEditMode(mode);
}

void PmhViewer::setPmhData(Internal::PmhData *pmh)
{
    if (!pmh) {
        d->m_Pmh = 0;
    }
    if (d->m_Pmh) {
        if (d->m_Pmh == pmh)
            return;
    }
    d->populateUiWithPmh(pmh);
}

void PmhViewer::setCategoryForPmh(Category::CategoryItem *category)
{
    Q_ASSERT(d->m_Pmh);
    if (!d->m_Pmh)
        return;
    d->m_Pmh->setCategory(category);
    QModelIndex cat = pmhCore()->pmhCategoryModel()->indexForCategory(d->m_Pmh->category());
    cat = pmhCore()->pmhCategoryModel()->categoryOnlyModel()->mapFromSource(cat);
    d->ui->categoryTreeView->setCurrentIndex(cat);
}

void PmhViewer::createNewPmh()
{
    if (d->m_Pmh) {
        Utils::warningMessageBox(tr("Replacing pmh data"),"","");
    }
    PmhData *pmh = new PmhData;
    pmh->populateWithCurrentData();
    d->populateUiWithPmh(pmh);
}

void PmhViewer::revert()
{
    if (d->m_Pmh)
        d->populateUiWithPmh(d->m_Pmh);
}

Internal::PmhData *PmhViewer::modifiedPmhData() const
{
    qDebug() << Q_FUNC_INFO;
    if (d->m_Mode==ReadOnlyMode) {
        return d->m_Pmh;
    }

    d->populatePmhWithUi();
    return d->m_Pmh;
}

void PmhViewer::onSimpleViewIcdClicked()
{
    ICD::IcdCollectionDialog dlg(this);
    PmhEpisodeModel *model = d->m_Pmh->episodeModel();
    QString xml = model->index(0, PmhEpisodeModel::IcdXml).data(Qt::EditRole).toString();
    dlg.setXmlIcdCollection(xml);
    if (dlg.exec()==QDialog::Accepted) {
        d->m_Pmh->episodeModel()->setData(model->index(0, PmhEpisodeModel::IcdXml), dlg.xmlIcdCollection());
        const QStringList &icdLabels = d->m_Pmh->episodeModel()->index(0, PmhEpisodeModel::IcdLabelStringList).data().toStringList();
        d->m_IcdLabelModel->setStringList(icdLabels);
        if (d->m_Pmh->data(PmhData::Label).toString().isEmpty() && d->ui->personalLabel->text().isEmpty()) {
            d->m_Pmh->setData(PmhData::Label, icdLabels.join(", "));
            d->ui->personalLabel->setText(d->m_Pmh->data(PmhData::Label).toString());
        }
    }
}

void PmhViewer::onSimpleViewLabelChanged(const QString &text)
{
    d->m_Pmh->episodeModel()->setData(d->m_Pmh->episodeModel()->index(0, PmhEpisodeModel::Label), text);
}

void PmhViewer::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        d->ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
