/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PMHCORE_H
#define PMHCORE_H

#include <pmhplugin/pmh_exporter.h>

#include <QObject>

/**
 * \file ./plugins/pmhplugin/pmhcore.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace PMH {
namespace Internal {
class PmhCorePrivate;
}
class PmhCategoryModel;


class PMH_EXPORT PmhCore : public QObject
{
    Q_OBJECT
    explicit PmhCore(QObject *parent = 0);

public:
    static PmhCore *instance(QObject *parent = 0);
    ~PmhCore();

    PmhCategoryModel *pmhCategoryModel() const;

private Q_SLOTS:
    void onPatientFormsLoaded();

private:
    static PmhCore *m_Instance;
    Internal::PmhCorePrivate *d;
};

}  // End PMH

#endif // PMHCORE_H
