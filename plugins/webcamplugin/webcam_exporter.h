/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Christian A. Reiter <christian.a.reiter@gmail.com>    *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef WEBCAM_EXPORTER_H
#define WEBCAM_EXPORTER_H

#include <QtCore/QtGlobal>

#if defined(WEBCAM_LIBRARY)
#  define WEBCAMSHARED_EXPORT Q_DECL_EXPORT
#else
#  define WEBCAMSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // WEBCAM_EXPORTER_H

