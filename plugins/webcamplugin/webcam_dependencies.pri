#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

include($${SOURCES_PLUGINS_PATH}/coreplugin/coreplugin.pri)

# Add the opencv dependencies
win32 {
    LIBS +=-lopencv_core242 \
        -lopencv_highgui242 \
        -lopencv_objdetect242
} else {
    LIBS +=-lopencv_core \
        -lopencv_highgui \
        -lopencv_objdetect
}
