/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Christian A. Reiter <christian.a.reiter@gmail.com>    *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef WEBCAM_H
#define WEBCAM_H

#include "webcam_exporter.h"
#include "webcamphotoprovider.h"
#include "webcampreferences.h"

#include <extensionsystem/iplugin.h>

#include <QMap>

/**
 * \file ./plugins/webcamplugin/webcamplugin.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Webcam {
namespace Internal {

class WebcamPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.freemedforms.FreeMedForms.WebcamPlugin" FILE "Webcam.json")

public:
    WebcamPlugin();
    ~WebcamPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

private Q_SLOTS:
    void postCoreInitialization();
    void coreAboutToClose();
    //    void triggerAction();

private:
    void detectDevices();

    Internal::WebcamPreferencesPage *m_prefPage;
};

}
} // namespace Webcam

#endif // WEBCAM_H

