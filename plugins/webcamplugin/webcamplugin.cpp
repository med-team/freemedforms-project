/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Christian A. Reiter <christian.a.reiter@gmail.com>    *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/

#include <opencv2/highgui/highgui.hpp>

#include "webcamplugin.h"
#include "webcamconstants.h"
#include "webcamphotoprovider.h"
#include "webcampreferences.h"

#include <utils/log.h>

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/itheme.h>
#include <coreplugin/iuser.h>
#include <coreplugin/translators.h>

#include <extensionsystem/pluginmanager.h>

#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/actionmanager/actioncontainer.h>

#include <QtPlugin>
#include <QDebug>

using namespace Webcam;
using namespace Internal;

static inline Core::IUser *user()  { return Core::ICore::instance()->user(); }
static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline void messageSplash(const QString &s) {theme()->messageSplashScreen(s); }

WebcamPlugin::WebcamPlugin() :
    ExtensionSystem::IPlugin(),
    m_prefPage(0)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating Webcam";
    setObjectName("WebcamPlugin");

    Core::ICore::instance()->translators()->addNewTranslator("plugin_webcam");



    connect(Core::ICore::instance(), SIGNAL(coreOpened()), this, SLOT(postCoreInitialization()));
    connect(Core::ICore::instance(), SIGNAL(coreAboutToClose()), this, SLOT(coreAboutToClose()));
}

WebcamPlugin::~WebcamPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool WebcamPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    Q_UNUSED(arguments)
    Q_UNUSED(errorString)

    if (Utils::Log::debugPluginsCreation()) {
        qDebug() << "WebcamPlugin::initialize";
    }






    messageSplash(tr("Initializing Webcam..."));


    return true;
}

void WebcamPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation()) {
        qDebug() << "Webcam::extensionsInitialized";
    }


    if (!user())
        return;
    if (user()->uuid().isEmpty())
        return;

    messageSplash(tr("Initializing Webcam..."));
    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    detectDevices();



    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));
}

void WebcamPlugin::postCoreInitialization()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << Q_FUNC_INFO;
}

ExtensionSystem::IPlugin::ShutdownFlag WebcamPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;

    return SynchronousShutdown;
}


void WebcamPlugin::detectDevices()
{
    int nbDevice = 0;
    for(int deviceId = 0; deviceId<10; deviceId++) {
        cv::VideoCapture cap(deviceId);
        cv::Mat frame;
        if (cap.isOpened()) {
            cap.read(frame);
            if (!frame.empty()) {
                ++nbDevice;
                bool alreadyThere = false;
                foreach(WebcamPhotoProvider *provider, WebcamPhotoProvider::getProviders()) {
                    if (provider->deviceId() == deviceId)
                        alreadyThere = true;
                }
                if (!alreadyThere)
                    addAutoReleasedObject(new WebcamPhotoProvider(deviceId, this));
            }
        }
    }
    LOG(QString("Found %1 webcam device").arg(nbDevice));
}

void WebcamPlugin::coreAboutToClose()
{
}

Q_EXPORT_PLUGIN(WebcamPlugin)
