/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Christian A. Reiter <christian.a.reiter@gmail.com>    *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef WEBCAMCONSTANTS_H
#define WEBCAMCONSTANTS_H

namespace Webcam {
namespace Constants {

// Actions
const char * const ACTION_ID = "Webcam.Action";
const char * const MENU_ID = "Webcam.Menu";

// ICONS

const char * const ICON_WEBCAM_FREEZE = "record.png";
const char * const ICON_WEBCAM_RETRY = "retry.png";

// Settings
const char * const  S_DATEFORMAT           = "Webcam/Enabled";

} // namespace Webcam
} // namespace Constants

#endif // WEBCAMCONSTANTS_H

