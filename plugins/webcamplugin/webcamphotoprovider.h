/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developer: Christian A. Reiter <christian.a.reiter@gmail.com>    *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef WEBCAMPHOTOPROVIDER_H
#define WEBCAMPHOTOPROVIDER_H

class WebcamDevice;

#include <QObject>
#include <QPixmap>

#include <coreplugin/iphotoprovider.h>

namespace Webcam {
/*!
 * \brief The WebcamPhotoProvider class
 *
 * It implements the Core::IPhotoProvider interface. For every Webcam that is detected there should be one WebcamPhotoProvider.
 */
class WebcamPhotoProvider : public Core::IPhotoProvider
{
    Q_OBJECT
public:
    explicit WebcamPhotoProvider(int deviceId, QObject *parent = 0);
    ~WebcamPhotoProvider();

    QString id() const;
    int deviceId() const;
    QString name() const;
    QString displayText() const;
    bool isEnabled() const;
    bool isActive() const;
    int priority() const;

    static QList<WebcamPhotoProvider *> getProviders();

public Q_SLOTS:
    void startReceivingPhoto();

private:
    int m_deviceId;
    static QMap<int, WebcamPhotoProvider*> m_webcamsPool;
};

} // end Webcam

#endif // WEBCAMPHOTOPROVIDER_H
