/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker <eric.maeker@gmail.com>                 *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef BASEWIDGETS_TREEVIEWFORMITEM_H
#define BASEWIDGETS_TREEVIEWFORMITEM_H

#include <formmanagerplugin/iformwidgetfactory.h>
#include <formmanagerplugin/iformitemdata.h>

QT_BEGIN_NAMESPACE
class QTreeView;
class QAbstractItemModel;
QT_END_NAMESPACE

/**
 * \file ./plugins/basewidgetsplugin/treeviewformitem.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace BaseWidgets {

class TreeViewFormItem : public Form::IFormWidget
{
    Q_OBJECT
public:
    explicit TreeViewFormItem(Form::FormItem *formItem, QWidget *parent = 0);
    ~TreeViewFormItem();

    void addWidgetToContainer(Form::IFormWidget *widget);
    bool isContainer() const;

    // Printing
    QString printableHtml(bool withValues = true) const;

public Q_SLOTS:
    void retranslate();

public:
    QTreeView *m_Tree;
    QAbstractItemModel *m_Model;
};

class TreeViewFormItemData : public Form::IFormItemData
{
    Q_OBJECT
public:
    TreeViewFormItemData(Form::FormItem *item, TreeViewFormItem *formWidget);
    ~TreeViewFormItemData();
    void clear();

    Form::FormItem *parentItem() const {return m_FormItem;}
    bool isModified() const;
    void setModified(bool modified);

    void setReadOnly(bool readOnly);
    bool isReadOnly() const;

    // Use setData/Data for episode data
    bool setData(const int ref, const QVariant &data, const int role = Qt::EditRole);
    QVariant data(const int ref, const int role = Qt::DisplayRole) const;

    // Used storable data for forms
    void setStorableData(const QVariant &modified);
    QVariant storableData() const;

public Q_SLOTS:
    void onValueChanged();

private:
    Form::FormItem *m_FormItem;
    TreeViewFormItem *m_Form;
    bool m_Modified;
    // m_OriginalData;
};

} // namespace BaseWidgets

#endif  // BASEWIDGETS_TREEVIEWFORMITEM_H

