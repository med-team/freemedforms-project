/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef BASEWIDGETSPLUGIN_BASEFORMWIDGESTPLUGIN_H
#define BASEWIDGETSPLUGIN_BASEFORMWIDGESTPLUGIN_H

#include <extensionsystem/iplugin.h>

#include <QtCore/QObject>
#include <QPointer>

/**
 * \file ./plugins/basewidgetsplugin/basewidgetsplugin.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace BaseWidgets {
namespace Internal {
class BaseFormWidgetsOptionsPage;
class CalculationWidgetsFactory;
class BaseWidgetsFactory;

class BaseWidgetsPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.freemedforms.FreeMedForms.BaseWidgetsPlugin" FILE "BaseWidgets.json")

public:
    BaseWidgetsPlugin();
    ~BaseWidgetsPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

private:
    QPointer<Internal::BaseWidgetsFactory> m_Factory;
    QPointer<Internal::CalculationWidgetsFactory> m_CalcFactory;
    QPointer<Internal::BaseFormWidgetsOptionsPage> m_OptionsPage;
};

} // namespace Internal
} // End BaseWidgets

#endif  // BASEWIDGETSPLUGIN_BASEFORMWIDGESTPLUGIN_H
