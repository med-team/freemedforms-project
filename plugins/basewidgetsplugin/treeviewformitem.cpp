/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker                                *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
/*!
 * \class BaseWidgets::TreeViewFormItem
 */

#include "treeviewformitem.h"
#include "constants.h"

#include <formmanagerplugin/iformitem.h>
#include <formmanagerplugin/iformitemspec.h>

#include <utils/log.h>
#include <utils/global.h>

#include <translationutils/constants.h>

#include <QTreeView>
#include <QDebug>

using namespace BaseWidgets;
using namespace Trans::ConstantTranslations;

/*! Constructor of the BaseWidgets::TreeViewFormItem class */
TreeViewFormItem::TreeViewFormItem(Form::FormItem *formItem, QWidget *parent)
    : Form::IFormWidget(formItem, parent)
{
    setObjectName("TreeViewFormItem");

    const QString &widget = formItem->spec()->value(Form::FormItemSpec::Spec_UiWidget).toString();
    if (!widget.isEmpty()) {
        QTreeView *le = formItem->parentFormMain()->formWidget()->findChild<QTreeView*>(widget);
        if (le) {
            m_Tree = le;
        } else {
            LOG_ERROR("Using the QtUiLinkage, item not found in the ui: " + formItem->uuid());
            m_Tree = new QTreeView(this);
        }
        m_Label = Constants::findLabel(formItem);
    } else {
        QBoxLayout * hb = getBoxLayout(OnLeft, m_FormItem->spec()->label(), this);
        hb->addWidget(m_Label);

        m_Tree = new QTreeView(this);
        m_Tree->setObjectName("Tree_" + m_FormItem->uuid());
        m_Tree->setAlternatingRowColors(true);
        m_Tree->setSizePolicy(QSizePolicy::Expanding , QSizePolicy::Expanding);
        hb->addWidget(m_Tree);
    }

    if (!m_FormItem->extraData().value("xmlmodel").isEmpty()) {
    }

    setFocusedWidget(m_Tree);

    TreeViewFormItemData *data = new TreeViewFormItemData(m_FormItem, this);
    m_FormItem->setItemData(data);

}

/*! Destructor of the BaseWidgets::TreeViewFormItem class */
TreeViewFormItem::~TreeViewFormItem()
{
}

void TreeViewFormItem::addWidgetToContainer(Form::IFormWidget *widget)
{
    Q_UNUSED(widget);
}

bool TreeViewFormItem::isContainer() const
{
    return false;
}

QString TreeViewFormItem::printableHtml(bool withValues) const
{
    Q_UNUSED(withValues);
    return QString::null;
}

void TreeViewFormItem::retranslate()
{
    if (m_Label)
        m_Label->setText(m_FormItem->spec()->label());
}

TreeViewFormItemData::TreeViewFormItemData(Form::FormItem *item, TreeViewFormItem *formWidget) :
    m_FormItem(item),
    m_Form(formWidget)
{
}

TreeViewFormItemData::~TreeViewFormItemData()
{
}

void TreeViewFormItemData::clear()
{
}

bool TreeViewFormItemData::isModified() const
{
    return false;
}

void TreeViewFormItemData::setModified(bool modified)
{
    Q_UNUSED(modified);
}

void TreeViewFormItemData::setReadOnly(bool readOnly)
{
    Q_UNUSED(readOnly);
}

bool TreeViewFormItemData::isReadOnly() const
{
    return false;
}

bool TreeViewFormItemData::setData(const int ref, const QVariant &data, const int role)
{
    Q_UNUSED(ref);
    Q_UNUSED(data);
    Q_UNUSED(role);
    return true;
}

QVariant TreeViewFormItemData::data(const int ref, const int role) const
{
    Q_UNUSED(ref);
    Q_UNUSED(role);
    return QVariant();
}

void TreeViewFormItemData::setStorableData(const QVariant &data)
{
    Q_UNUSED(data);
    if (m_Form) {
    }
}

QVariant TreeViewFormItemData::storableData() const
{
    if (m_Form) {
    }
    return QVariant();
}

void TreeViewFormItemData::onValueChanged()
{
    Constants::executeOnValueChangedScript(m_FormItem);
    Q_EMIT dataChanged(0);
}

