/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "basedetailswidget.h"
#include "constants.h"

#include <formmanagerplugin/iformitem.h>
#include <formmanagerplugin/iformitemspec.h>

#include <utils/log.h>
#include <utils/global.h>
#include <utils/widgets/detailswidget.h>


#include <QUiLoader>
#include <QBuffer>

using namespace BaseWidgets;
using namespace Internal;

BaseDetailsWidget::BaseDetailsWidget(Form::FormItem *formItem, QWidget *parent) :
    Form::IFormWidget(formItem, parent),
    _detailsWidget(0) //,
{
    setObjectName("BaseDetailsWidge_" + m_FormItem->uuid());

    _detailsWidget = new Utils::DetailsWidget(this);
    _detailsWidget->setSummaryText(formItem->spec()->label());

    QWidget *content = 0;
    const QString &uiContent = formItem->spec()->value(Form::FormItemSpec::Spec_UiFileContent).toString();
    if (!uiContent.isEmpty()) {
        QUiLoader loader;
        QBuffer buf;
        buf.setData(uiContent.toUtf8());
        content = loader.load(&buf, _detailsWidget);

    } else {
        LOG_ERROR("Ui file not found: " + formItem->spec()->value(Form::FormItemSpec::Spec_UiFileContent).toString());
    }

    _detailsWidget->setWidget(content);
    if (Constants::hasOption(formItem, Constants::SUMMARY_FONT_BOLD))
        _detailsWidget->setSummaryFontBold(true);
    if (Constants::hasOption(formItem, Constants::EXTRAS_GROUP_EXPANDED))
        _detailsWidget->setState(Utils::DetailsWidget::Expanded);

    const QString &uiLayout = formItem->spec()->value(Form::FormItemSpec::Spec_UiInsertIntoLayout).toString();
    if (!uiLayout.isEmpty()) {
        QLayout *lay = formItem->parentFormMain()->formWidget()->findChild<QLayout*>(uiLayout);
        if (lay) {
            lay->addWidget(_detailsWidget);
        } else {
            LOG_ERROR("Using the QtUiLinkage, layout not found in the ui: " + formItem->uuid());
        }
    } else {
        QVBoxLayout *layout = new QVBoxLayout(this);
        setLayout(layout);
        layout->addWidget(_detailsWidget);
    }


    retranslate();

}

BaseDetailsWidget::~BaseDetailsWidget()
{
}

void BaseDetailsWidget::addWidgetToContainer(Form::IFormWidget *widget)
{
    Q_UNUSED(widget);
}

bool BaseDetailsWidget::isContainer() const
{
    return true;
}

QString BaseDetailsWidget::printableHtml(bool withValues) const
{
    if (m_FormItem->getOptions().contains(Constants::NOT_PRINTABLE))
        return QString();

    QStringList html;
    QList<Form::FormItem*> items = m_FormItem->formItemChildren();
    for(int i = 0; i < items.count(); ++i) {
        Form::IFormWidget *w = items.at(i)->formWidget();
        if (w)
            html << w->printableHtml(withValues);
    }
    html.removeAll("");

    if (html.isEmpty() && Constants::dontPrintEmptyValues(m_FormItem))
        return QString();

    int i = 0;
    int previousrow = 0;
    QString header, content;
    header += QString("<table width=100% border=2 cellpadding=0 cellspacing=0  style=\"margin: 5px 0px 0px 0px\">"
                    "<thead>"
                    "<tr>"
                    "<td style=\"vertical-align: top;padding: 5px\">"
                    "<center><span style=\"font-weight: 600;\">%1</span><br />"
                    "%2"
                      "</center>"
                    "</td>"
                    "</tr>"
                    "</thead>"
                    "</table>")
            .arg(m_FormItem->spec()->label())
            .arg("LABEL");

    int numberColumns = 1;
    foreach(const QString &s, html) {
        int r = (i / numberColumns);
        if (r > previousrow) {
            previousrow = r;
            content += "</tr><tr>";
        }
        content += QString("<td style=\"vertical-align: top; align: left\">"
                           "%1"
                           "</td>").arg(s);
        ++i;
    }

    return QString("%1"
                   "<table width=100% border=0 cellpadding=0 cellspacing=0 style=\"margin:0px\">"
                   "<tbody>"
                   "<tr>"
                   "%2"
                   "</tr>"
                   "</tbody>"
                   "</table>")
            .arg(header).arg(content);
}

void BaseDetailsWidget::retranslate()
{
    Q_ASSERT(_detailsWidget);

        _detailsWidget->setSummaryText(m_FormItem->spec()->label());
}
