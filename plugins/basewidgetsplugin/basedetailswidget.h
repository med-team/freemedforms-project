/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef BASEWIDGETS_INTERNAL_BASEDETAILSWIDGET_H
#define BASEWIDGETS_INTERNAL_BASEDETAILSWIDGET_H

#include <formmanagerplugin/iformwidgetfactory.h>

/**
 * \file ./plugins/basewidgetsplugin/basedetailswidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
class DetailsWidget;
}

namespace BaseWidgets {
namespace Internal {

class BaseDetailsWidget : public Form::IFormWidget
{
public:
    BaseDetailsWidget(Form::FormItem *formItem, QWidget *parent = 0);
    ~BaseDetailsWidget();

    void addWidgetToContainer(Form::IFormWidget *widget);
    bool isContainer() const;

    // Printing
    QString printableHtml(bool withValues = true) const;

public Q_SLOTS:
    void retranslate();

private:
    Utils::DetailsWidget *_detailsWidget;
    //int numberColumns, i, col, row;
};

} // namespace Internal
} // namespace BaseWidgets

#endif // BASEWIDGETS_INTERNAL_BASEDETAILSWIDGET_H
