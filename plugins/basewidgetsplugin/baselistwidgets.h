/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       Guillaume DENRY <guillaume.denry@gmail.com>                       *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef BASEWIDGETS_BASELISTWIDGETS_H
#define BASEWIDGETS_BASELISTWIDGETS_H

//#include "basewigets_exporter.h"

#include <memory>
#include <vector>

#include <QWidget>
#include <QVariant>

#include <formmanagerplugin/iformwidgetfactory.h>
#include <formmanagerplugin/iformitemdata.h>

QT_BEGIN_NAMESPACE
class QListView;
class QComboBox;
class QStandardItem;
class QStandardItemModel;
class QStringListModel;
class QSortFilterProxyModel;
QT_END_NAMESPACE

/**
 * \file ./plugins/basewidgetsplugin/baselistwidgets.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Views {
class StringListView;
}

namespace BaseWidgets {
namespace Internal {
///////////////////////////////////////////////////////////////////////////////
/////////////////////////////   BaseList   ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
class BaseList : public Form::IFormWidget
{
     Q_OBJECT
public:
     BaseList(Form::FormItem *linkedObject, QWidget *parent = 0, bool uniqueList = true);
     ~BaseList();

     QString printableHtml(bool withValues = true) const;

public Q_SLOTS:
     void retranslate();

public:
     QListView *m_List;
     QStringListModel *m_Model;
};

class BaseEditableStringList : public Form::IFormWidget
{
     Q_OBJECT
public:
     BaseEditableStringList(Form::FormItem *linkedObject, QWidget *parent = 0);
     ~BaseEditableStringList();

     QString printableHtml(bool withValues = true) const;

public Q_SLOTS:
     void retranslate();

public:
     Views::StringListView *m_StringListView;
};

class BaseListData : public Form::IFormItemData
{
    Q_OBJECT
public:
    BaseListData(Form::FormItem *item);
    ~BaseListData();

    void setBaseList(BaseList* list) {m_List = list; clear();}
    void setBaseStringListView(BaseEditableStringList* list) {m_EditableList = list; clear();}
    void setSelectedItems(const QString &s);

    void clear();

    Form::FormItem *parentItem() const {return m_FormItem;}
    bool isModified() const;
    void setModified(bool modified);

    void setReadOnly(bool readOnly);
    bool isReadOnly() const;

    bool setData(const int ref, const QVariant &data, const int role = Qt::EditRole);
    QVariant data(const int ref, const int role = Qt::DisplayRole) const;

    void setStorableData(const QVariant &data);
    QVariant storableData() const;

private Q_SLOTS:
    void onValueChanged();

private:
    Form::FormItem *m_FormItem;
    BaseList* m_List;
    BaseEditableStringList *m_EditableList;
    QStringList m_OriginalValue;
};

///////////////////////////////////////////////////////////////////////////////
/////////////////////////////   BaseCombo   ///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
class BaseCombo : public Form::IFormWidget
{
    friend class BaseComboData;
    Q_OBJECT
public:
     BaseCombo(Form::FormItem *linkedObject, QWidget *parent = 0);
     ~BaseCombo();

     QString printableHtml(bool withValues = true) const;

public Q_SLOTS:
     void retranslate();

private:
    QComboBox *m_Combo;
    bool m_Sort;
    bool m_Default;
};

class BaseComboData : public Form::IFormItemData
{
    friend class BaseCombo;
    Q_OBJECT
public:
    BaseComboData(Form::FormItem *item);
    ~BaseComboData();

    void setBaseCombo(BaseCombo* combo) {m_BaseCombo = combo; clear();}
    int selectedItem(const QString &s);
    void setDefaultIndex(int index);
    int defaultIndex() const;

    void clear();

    Form::FormItem *parentItem() const {return m_FormItem;}
    bool isModified() const;
    void setModified(bool modified);

    void setReadOnly(bool readOnly);
    bool isReadOnly() const;

    bool setData(const int ref, const QVariant &data, const int role = Qt::EditRole);
    QVariant data(const int ref, const int role = Qt::DisplayRole) const;

    void setStorableData(const QVariant &data);
    QVariant storableData() const;
    int comboIndexFromUuid(const QString s);
    void setModel();
    void setVectors();
    void populateWithPeriods();

private Q_SLOTS:
    void onValueChanged();

private:
    Form::FormItem *m_FormItem;
    BaseCombo* m_BaseCombo;
    int m_OriginalValue;
    int m_DefaultIndex;
    QString m_DefaultValue;
    QStandardItemModel *m_Model;
    QSortFilterProxyModel* m_Proxy;
    std::vector<std::unique_ptr<QStandardItem>> m_v_Possibles;
    std::vector<std::unique_ptr<QStandardItem>> m_v_Uuids;
};

}  // End namespace Internal
}  // End namespace BaseWidgets

#endif // BASEWIDGETS_BASELISTWIDGETS_H
