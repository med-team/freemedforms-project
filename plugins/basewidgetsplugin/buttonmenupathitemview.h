/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker                                *
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef BASEWIDGETS_INTERNAL_BUTTONMENUPATHITEMVIEW_H
#define BASEWIDGETS_INTERNAL_BUTTONMENUPATHITEMVIEW_H

#include <formmanagerplugin/iformwidgetfactory.h>
#include <formmanagerplugin/iformitemdata.h>
#include <QToolButton>
#include <QFileSystemModel>

/**
 * \file ./plugins/basewidgetsplugin/buttonmenupathitemview.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
class QMenuItemView;
}

namespace BaseWidgets {
namespace Internal {

class ButtonMenuPathItemView : public Form::IFormWidget
{
    Q_OBJECT
    
public:
    explicit ButtonMenuPathItemView(Form::FormItem *linkedObject, QWidget *parent = 0);
    ~ButtonMenuPathItemView();
    
    void addWidgetToContainer(Form::IFormWidget *widget);
    bool isContainer() const;
    
    // Printing
    QString printableHtml(bool withValues = true) const;
    
public Q_SLOTS:
    void onIndexTriggered(const QModelIndex &index);
    void retranslate();
    
private:
    Utils::QMenuItemView *_menu;
    QToolButton *_button;
    QFileSystemModel *_model;
};

} // namespace Internal
} // namespace BaseWidgets

#endif // BASEWIDGETS_INTERNAL_BUTTONMENUPATHITEMVIEW_H

