/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric MAEKER, <eric.maeker@gmail.com>                *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef BASEWIDGETS_INTERNAL_BASEZIPCODESWIDGET_H
#define BASEWIDGETS_INTERNAL_BASEZIPCODESWIDGET_H

#include <formmanagerplugin/iformwidgetfactory.h>
#include <formmanagerplugin/iformitemdata.h>

/**
 * \file ./plugins/basewidgetsplugin/basezipcodeswidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace BaseWidgets {
namespace Internal {
class BaseDateCompleterData;

class BaseDateCompleterWidget : public Form::IFormWidget
{
    Q_OBJECT
    friend class BaseWidgets::Internal::BaseDateCompleterData;

public:
    BaseDateCompleterWidget(Form::FormItem *formItem, QWidget *parent = 0);
    ~BaseDateCompleterWidget();

    void addWidgetToContainer(Form::IFormWidget *widget);
    bool isContainer() const;

    // Printing
    QString printableHtml(bool withValues = true) const;

public Q_SLOTS:
    void retranslate();

protected:
};

class BaseDateCompleterData : public Form::IFormItemData
{
    Q_OBJECT
public:
    BaseDateCompleterData(Form::FormItem *item);
    ~BaseDateCompleterData();

    void setBaseDate(BaseDateCompleterWidget* date) {m_Date = date; clear();}
    void setDate(const QString &s);

    void clear();

    Form::FormItem *parentItem() const {return m_FormItem;}
    bool isModified() const;
    void setModified(bool modified);

    void setReadOnly(bool readOnly);
    bool isReadOnly() const;

    bool setData(const int ref, const QVariant &data, const int role = Qt::EditRole);
    QVariant data(const int ref, const int role = Qt::DisplayRole) const;

    void setStorableData(const QVariant &data);
    QVariant storableData() const;

private Q_SLOTS:
    void onValueChanged();

private:
    Form::FormItem *m_FormItem;
    BaseDateCompleterWidget* m_Date;
    QString m_OriginalValue;
};

} // namespace Internal
} // namespace BaseWidgets

#endif // BASEWIDGETS_INTERNAL_BASEZIPCODESWIDGET_H
