/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef TEXTEDITORFACTORY_H
#define TEXTEDITORFACTORY_H

#include <texteditorplugin/texteditor.h>

#include <formmanagerplugin/iformwidgetfactory.h>
#include <formmanagerplugin/iformitemdata.h>

#include <QStringList>

/**
 * \file ./plugins/basewidgetsplugin/texteditorfactory.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace BaseWidgets {

class TextEditorFactory : public Form::IFormWidgetFactory
{
    Q_OBJECT
public:
    TextEditorFactory(QObject *parent = 0);
    ~TextEditorFactory();

    bool initialize(const QStringList &arguments, QString *errorString);
    bool extensionInitialized();
    bool isInitialized() const;

    bool isContainer(const int idInStringList) const;
    QStringList providedWidgets() const;
    Form::IFormWidget *createWidget(const QString &name, Form::FormItem *object, QWidget *parent = 0);
};

class TextEditorForm : public Form::IFormWidget
{
    Q_OBJECT
public:
    TextEditorForm(Form::FormItem *linkedObject, QWidget *parent = 0);
    ~TextEditorForm();

    void addWidgetToContainer(Form::IFormWidget *) {}
    bool isContainer() const {return false;}

    QString printableHtml(bool withValues = true) const;

public Q_SLOTS:
    void retranslate();

private:
    Editor::TextEditor *m_Text;
};

class TextEditorData : public Form::IFormItemData
{
    Q_OBJECT
public:
    TextEditorData(Form::FormItem *item);
    ~TextEditorData();

    void setEditor(Editor::TextEditor* editor) {m_Editor = editor; clear();}

    void clear();

    Form::FormItem *parentItem() const {return m_FormItem;}
    bool isModified() const;
    void setModified(bool modified);

    void setReadOnly(bool readOnly);
    bool isReadOnly() const;

    bool setData(const int ref, const QVariant &data, const int role);
    QVariant data(const int ref, const int role) const;

    void setStorableData(const QVariant &data);
    QVariant storableData() const;

public Q_SLOTS:
    void onValueChanged();

private:
    Form::FormItem *m_FormItem;
    Editor::TextEditor* m_Editor;
    QString m_OriginalValue;
    bool m_ForceModified;
};

}  // End namespace BaseWidgets

#endif // TEXTEDITORFACTORY_H
