/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef BASEFORMWIDGETSOPTIONSPAGE_H
#define BASEFORMWIDGETSOPTIONSPAGE_H

#include <coreplugin/ioptionspage.h>
#include <QWidget>

#include <QPointer>

/**
 * \file ./plugins/basewidgetsplugin/baseformwidgetsoptionspage.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace BaseWidgets {
namespace Internal {

namespace Ui {
    class BaseFormWidgetsOptionsPage;
}

class BaseFormSettingsWidget : public QWidget
{
    Q_OBJECT
public:
    BaseFormSettingsWidget(QWidget *parent = 0);
    ~BaseFormSettingsWidget();

    void applyChanges();
    void resetToDefaults();

private:
    Ui::BaseFormWidgetsOptionsPage *m_ui;
};

class BaseFormWidgetsOptionsPage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    BaseFormWidgetsOptionsPage(QObject *parent = 0);
    ~BaseFormWidgetsOptionsPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const {return displayName();}
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    /** \todo add help page */
    QString helpPage() {return QString();}

    QWidget *createPage(QWidget *parent = 0);

private:
    QPointer<BaseFormSettingsWidget> m_Widget;
};

} // End Internal
} // End BaseWidgets

#endif // BASEFORMWIDGETSOPTIONSPAGE_H
