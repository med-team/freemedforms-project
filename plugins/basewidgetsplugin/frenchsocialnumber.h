/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric MAEKER, MD <eric.maeker@gmail.com>              *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FRENCHSOCIALNUMBER_H
#define FRENCHSOCIALNUMBER_H

#include <formmanagerplugin/iformwidgetfactory.h>
#include <formmanagerplugin/iformitemdata.h>

#include <QWidget>
#include <QVariant>
QT_BEGIN_NAMESPACE
class QLineEdit;
QT_END_NAMESPACE

/**
 * \file ./plugins/basewidgetsplugin/frenchsocialnumber.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace BaseWidgets {
namespace Internal {
class FrenchSocialNumberWidget;
}  // End namespace Internal

class FrenchSocialNumberFormData;

class FrenchSocialNumberFormWidget: public Form::IFormWidget
{
    Q_OBJECT
public:
    FrenchSocialNumberFormWidget(Form::FormItem *formItem, QWidget *parent = 0);
    ~FrenchSocialNumberFormWidget();

    QString printableHtml(bool withValues = true) const;

    void addWidgetToContainer(Form::IFormWidget *) {}
    bool isContainer() const {return false;}

public Q_SLOTS:
    void retranslate();

private:
    FrenchSocialNumberFormData *m_ItemData;
    Internal::FrenchSocialNumberWidget *m_NSS;
};

class FrenchSocialNumberFormData : public Form::IFormItemData
{
public:
    FrenchSocialNumberFormData(Form::FormItem *item);
    ~FrenchSocialNumberFormData();

    void setWidget(Internal::FrenchSocialNumberWidget *w) {m_Widget = w; clear();}
    void clear();
    void populateWithPatientData();

    Form::FormItem *parentItem() const {return m_FormItem;}
    bool isModified() const;
    void setModified(bool modified);

    void setReadOnly(bool readOnly);
    bool isReadOnly() const;

    // Use setData/Data for episode data
    bool setData(const int ref, const QVariant &data, const int role);
    QVariant data(const int ref, const int role) const;

    void setStorableData(const QVariant &);
    QVariant storableData() const;

private:
    Form::FormItem *m_FormItem;
    Internal::FrenchSocialNumberWidget *m_Widget;
    QString m_OriginalValue;
};

}  // End namespace BaseWidgets


#endif // FRENCHSOCIALNUMBER_H
