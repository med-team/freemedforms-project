#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

include( ../coreplugin/coreplugin.pri )
include( ../texteditorplugin/texteditorplugin.pri )
include( ../formmanagerplugin/formmanagerplugin.pri )
include( ../patientbaseplugin/patientbaseplugin.pri )
include( ../identityplugin/identityplugin.pri )
QT *= script
isEqual(QT_MAJOR_VERSION, 5) {
    QT *= uitools
} else {
    CONFIG *= uitools
}
