/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef BASEWIDGETS_INTERNAL_FRENCHSOCIALNUMBERWIDGET_H
#define BASEWIDGETS_INTERNAL_FRENCHSOCIALNUMBERWIDGET_H

#include <QWidget>

/**
 * \file ./plugins/basewidgetsplugin/frenchsocialnumberwidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace BaseWidgets {
namespace Internal {
class FrenchSocialNumberWidgetPrivate;

class FrenchSocialNumberWidget : public QWidget
{
    Q_OBJECT
public:
    explicit FrenchSocialNumberWidget(QWidget *parent = 0);
    ~FrenchSocialNumberWidget();
    bool initialize();
    void clear();

    void setNumberWithControlKey(const QString &number);
    void setNumberWithoutControlKey(const QString &number);

    bool isValid() const;
    bool isValid(const QString &number, const QString &key) const;
    int controlKey(const QString &number) const;

    QString numberWithControlKey() const;
    QString numberWithoutControlKey() const;

    QString emptyHtmlMask() const;
    QString toHtml() const;

private Q_SLOTS:
    void checkControlKey();

private:
    Internal::FrenchSocialNumberWidgetPrivate *d;
};

} // namespace Internal
} // namespace BaseWidgets

#endif // BASEWIDGETS_INTERNAL_FRENCHSOCIALNUMBERWIDGET_H

