/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "basewidgetsplugin.h"
#include "baseformwidgets.h"
#include "baseformwidgetsoptionspage.h"
#include "texteditorfactory.h"
#include "identitywidgetfactory.h"
#include "calculationwidgets.h"

#include <coreplugin/dialogs/pluginaboutpage.h>

#include <utils/log.h>

#include <QtPlugin>
#include <QDebug>

using namespace BaseWidgets;
using namespace Internal;

BaseWidgetsPlugin::BaseWidgetsPlugin() :
        m_Factory(0),
        m_OptionsPage(0)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating BaseWidgetsPlugin";
}

BaseWidgetsPlugin::~BaseWidgetsPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool BaseWidgetsPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "BaseWidgetsPlugin::initialize";

    Q_UNUSED(arguments);
    Q_UNUSED(errorString);
    m_Factory = new BaseWidgetsFactory(this);
    m_Factory->initialize(arguments,errorString);
    m_CalcFactory = new CalculationWidgetsFactory(this);
    m_CalcFactory->initialize(arguments,errorString);
    return true;
}

void BaseWidgetsPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "BaseWidgetsPlugin::extensionsInitialized";

    m_OptionsPage = new Internal::BaseFormWidgetsOptionsPage(this);
    m_OptionsPage->checkSettingsValidity();

    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));
    addObject(m_Factory);
    addObject(m_CalcFactory);
    addAutoReleasedObject(new TextEditorFactory(this));
    addAutoReleasedObject(new IdentityWidgetFactory(this));

}

ExtensionSystem::IPlugin::ShutdownFlag BaseWidgetsPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    if (m_Factory) {
        removeObject(m_Factory);
        delete m_Factory;
        m_Factory = 0;
    }
    if (m_CalcFactory) {
        removeObject(m_CalcFactory);
        delete m_CalcFactory;
        m_CalcFactory = 0;
    }
    if (m_OptionsPage) {
        delete m_OptionsPage;
        m_OptionsPage = 0;
    }
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(BaseWidgetsPlugin)
