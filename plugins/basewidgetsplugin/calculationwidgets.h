/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef __CALCULATIONWIDGETS_H
#define __CALCULATIONWIDGETS_H

#include <formmanagerplugin/iformwidgetfactory.h>
#include <texteditorplugin/texteditor.h>
#include <QVariant>
#include <QLineEdit>

/**
 * \file ./plugins/basewidgetsplugin/calculationwidgets.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/


namespace BaseWidgets {
namespace Internal {

class CalculationWidgetsFactory : public Form::IFormWidgetFactory
{
    Q_OBJECT
public:
    CalculationWidgetsFactory(QObject *parent = 0);
    ~CalculationWidgetsFactory();

    bool initialize(const QStringList &arguments, QString *errorString);
    bool extensionInitialized();
    bool isInitialized() const;

    bool isContainer(const int idInStringList) const;
    QStringList providedWidgets() const;
    Form::IFormWidget *createWidget(const QString &name, Form::FormItem *object, QWidget *parent = 0);
};

//--------------------------------------------------------------------------------------------------------
//-------------------------------------- SumWidget implementation ----------------------------------------
//--------------------------------------------------------------------------------------------------------
//class SumWidgetData;
class SumWidget : public Form::IFormWidget
{
    Q_OBJECT
public:
    SumWidget(Form::FormItem *formItem, QWidget *parent = 0);
    ~SumWidget();

    QString printableHtml(bool withValues = true) const;

private Q_SLOTS:
    void retranslate();
    void connectFormItems();
    void recalculate(const int modifiedRef);

private:
    QLineEdit *line;
//    SumWidgetData *m_ItemData;
};

class ScriptWidget : public Form::IFormWidget
{
    Q_OBJECT
public:
    ScriptWidget(Form::FormItem *formItem, QWidget *parent = 0);
    ~ScriptWidget();

    QString printableHtml(bool withValues = true) const;

private Q_SLOTS:
    void retranslate();
    void connectFormItems();
    void recalculate(const int modifiedRef);

private:
    QLineEdit *line;
    Editor::TextEditor* m_Editor;
};

}  // End namespace Internal
}  // End namespace BaseWidgets

#endif // __CALCULATIONWIDGETS_H
