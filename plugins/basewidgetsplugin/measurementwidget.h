/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef MEASUREMENTWIDGET_H
#define MEASUREMENTWIDGET_H

#include <formmanagerplugin/iformwidgetfactory.h>
#include <formmanagerplugin/iformitemdata.h>

QT_BEGIN_NAMESPACE
class QComboBox;
class QDoubleSpinBox;
QT_END_NAMESPACE

/**
 * \file ./plugins/basewidgetsplugin/measurementwidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace BaseWidgets {
class MeasurementWidgetData;

class MeasurementWidget : public Form::IFormWidget
{
    Q_OBJECT
    friend class BaseWidgets::MeasurementWidgetData;

public:
    MeasurementWidget(Form::FormItem *linkedObject, QWidget *parent = 0);
    ~MeasurementWidget();

    void setTabOrder(bool consoleWarn);
    void addWidgetToContainer(Form::IFormWidget *) {}
    bool isContainer() const {return false;}

    QString printableHtml(bool withValues = true) const;

public Q_SLOTS:
    void retranslate();

private:
    void populateWithLength();
    void populateWithWeight();

protected:
    QComboBox *m_units;
    QDoubleSpinBox *m_value;
    int m_defaultUnitId;
    bool m_isWeight, m_isLength;
};

class MeasurementWidgetData : public Form::IFormItemData
{
    Q_OBJECT
public:
    MeasurementWidgetData(Form::FormItem *item);
    ~MeasurementWidgetData();

    void setMeasurementWidget(MeasurementWidget *widget) {m_Measurement = widget; clear();}
    void setDefaultUnitIndex(int index) {m_defaultUnitId = index;}
    void setSelectedUnit(const QString &uuid);
    QString selectedUnitUuid() const;

    void clear();

    Form::FormItem *parentItem() const {return m_FormItem;}
    bool isModified() const;
    void setModified(bool modified);

    void setReadOnly(bool readOnly);
    bool isReadOnly() const;

    bool setData(const int ref, const QVariant &data, const int role);
    QVariant data(const int ref, const int role) const;

    void setStorableData(const QVariant &data);
    QVariant storableData() const;

public Q_SLOTS:
    void onValueChanged();

private:
    Form::FormItem *m_FormItem;
    MeasurementWidget *m_Measurement;
    int m_defaultUnitId;
    QString m_OriginalValue;
};

}  // End namespace BaseWidgets

#endif // MEASUREMENTWIDGET_H
