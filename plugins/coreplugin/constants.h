/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CORE_CONSTANTS_H
#define CORE_CONSTANTS_H

#include <coreplugin/constants_menus.h>
#include <coreplugin/constants_icons.h>
#include <coreplugin/constants_tokensandsettings.h>
#include <coreplugin/constants_trans.h>

/**
 * \file ./plugins/coreplugin/constants.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
namespace Constants {

const char* const   FREEDIAMS_FILEFILTER = QT_TRANSLATE_NOOP("tkConstants", "FreeDiams Prescriptions (*.di)");
const char* const   FREEDRC_FILEFILTER = QT_TRANSLATE_NOOP("tkConstants", "FreeDRC Consultation files (*.cr)");
const char* const   FREEMEDFORMS_FILEFILTER = QT_TRANSLATE_NOOP("tkConstants", "FreeMedForms Forms File (*.xml)");
const char* const   FREEDDIMANAGER_FILEFILTER = QT_TRANSLATE_NOOP("tkConstants", "FreeDDIManager files (*.ddi)");
const char* const   DATABASE_FILEFILTER = QT_TRANSLATE_NOOP("tkConstants", "FreeMedForms database files (*.db)");

}  // end Constants
} // end Core

#endif // CORE_CONSTANTS_H
