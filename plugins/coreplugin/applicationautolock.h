/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CORE_APPLICATIONAUTOLOCK_H
#define CORE_APPLICATIONAUTOLOCK_H

#include <coreplugin/core_exporter.h>
#include <QObject>

/**
 * \file ./plugins/coreplugin/applicationautolock.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
namespace Internal {
class ApplicationAutoLockPrivate;
} // namespace Internal

class CORE_EXPORT ApplicationAutoLock : public QObject
{
    Q_OBJECT
    
public:
    explicit ApplicationAutoLock(QObject *parent = 0);
    ~ApplicationAutoLock();
    bool initialize();
    void setTimeBeforeLocking(int durationInMs);
    void startListening();

    bool isLocked() const;

Q_SIGNALS:
    void lockRequired();

private:
    bool eventFilter(QObject *obj, QEvent *event);

private Q_SLOTS:
    void timerTimeOut();

private:
    Internal::ApplicationAutoLockPrivate *d;
};

} // namespace Core

#endif  // CORE_APPLICATIONAUTOLOCK_H

