/***************************************************************************
 *   FreeMedicalForms                                                      *
 *   (C) 2008-2017 by Eric MAEKER, MD                                     **
 *   eric.maeker@gmail.com                                                   *
 *   All rights reserved.                                                  *
 *                                                                         *
 *   This program is a free and open source software.                      *
 *   It is released under the terms of the new BSD License.                *
 *                                                                         *
 *   Redistribution and use in source and binary forms, with or without    *
 *   modification, are permitted provided that the following conditions    *
 *   are met:                                                              *
 *   - Redistributions of source code must retain the above copyright      *
 *   notice, this list of conditions and the following disclaimer.         *
 *   - Redistributions in binary form must reproduce the above copyright   *
 *   notice, this list of conditions and the following disclaimer in the   *
 *   documentation and/or other materials provided with the distribution.  *
 *   - Neither the name of the FreeMedForms' organization nor the names of *
 *   its contributors may be used to endorse or promote products derived   *
 *   from this software without specific prior written permission.         *
 *                                                                         *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS   *
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT     *
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS     *
 *   FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE        *
 *   COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,  *
 *   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,  *
 *   BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;      *
 *   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
 *   CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT    *
 *   LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE       *
 *   POSSIBILITY OF SUCH DAMAGE.                                           *
 ***************************************************************************/
#include "mainwindow.h"
#include "dialogs/plugindialog.h"

#include <translationutils/constanttranslations.h>

#include <coreplugin/icore.h>
#include <coreplugin/constants.h>
#include <coreplugin/log.h>
#include <coreplugin/translators.h>
#include <coreplugin/itheme.h>

#include <coreplugin/actionmanager/mainwindowactionhandler.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/contextmanager/contextmanager.h>

#include <extensionsystem/pluginerrorview.h>
#include <extensionsystem/pluginview.h>






#include <QTextEdit>
#include <QTextStream>
#include <QCloseEvent>
#include <QFileDialog>
#include <QStringList>
#include <QCoreApplication>
#include <QDir>
#include <QStatusBar>



using namespace Core;
using namespace Core::Internal;

MainWindow::MainWindow( QWidget * parent )
          : MainWindowActionHandler(parent)//, fileToolBar(0), editToolBar(0),
{
    setObjectName( "Core::MainWindow" );
    recentFiles.clear();
    setWindowTitle( qApp->applicationName() );
}

bool MainWindow::initialize()
{
    qWarning() << "mainWin initialize";
    createMenusAndActions();
    createToolBars();
    createStatusBar();
    retranslateUi();

    return true;
}

bool MainWindow::extensionInitialized()
{
    show();
    return true;
}

MainWindow::~MainWindow()
{
}

void MainWindow::afterTheShow()
{
}

QStatusBar *MainWindow::statusBar()
{
    return QMainWindow::statusBar();
}

bool MainWindow::aboutQt()
{
    aboutPlugins();
    return true;
}


void MainWindow::createMenusAndActions()
{
    createFileMenu();
    createEditMenu();
    createFormatMenu();
    createPluginsMenu();
    createConfigurationMenu();
    createHelpMenu();

    createFileActions(A_FileNew | A_FileOpen | A_FileSave | A_FileSaveAs | A_FilePrint | A_FileQuit);
    createEditActions();
    createConfigurationActions(A_AppPreferences | A_PluginsPreferences | A_LangageChange);
    createHelpActions(A_AppAbout | A_AppHelp | A_DebugDialog | A_FormsAbout | A_QtAbout);

    connectFileActions();
    connectConfigurationActions();
    connectHelpActions();

    QAction *a = 0;
    Command *cmd = 0;
    ActionManager *am = Core::ICore::instance()->actionManager();
    ITheme *theme = Core::ICore::instance()->theme();
    QList<int> ctx = QList<int>() << Constants::C_GLOBAL_ID;
    ActionContainer *menu = am->actionContainer(Constants::M_PLUGINS);
    Q_ASSERT(menu);
    if (!menu)
        return;

    a = aUserManager = new QAction(this);
    a->setIcon(theme->icon(Constants::ICONUSERMANAGER));
    cmd = am->registerAction(a, Constants::A_USERMANAGER, ctx);
    cmd->setTranslations(Trans::Constants::USERMANAGER_TEXT);
    menu->addAction(cmd, Constants::G_PLUGINS_USERMANAGER);
    connect(aUserManager, SIGNAL(triggered()), this, SLOT(userManagerRequested()));



    Core::ICore::instance()->contextManager()->updateContext();
    Core::ICore::instance()->actionManager()->retranslateMenusAndActions();
}


void MainWindow::createToolBars()
{
}

void MainWindow::createStatusBar()
{
    statusBar()->showMessage( tr( "Ready" ) );
}

void MainWindow::aboutPlugins()
{
    PluginDialog dialog(this);
    dialog.exec();
}



void MainWindow::retranslateUi()
{
}


