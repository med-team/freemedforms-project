/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/


/*!
 \enum Core::ISettings::Paths
 Defines the availables paths to use with setPath() and path().\n
 Some paths are calculated when setting the
 Core::ISettings::ApplicationPath,
 Core::ISettings::BundleRootPath and the
 Core::ISettings::UserResourcesPath.
*/

/*! \var Core::ISettings::Paths Core::ISettings::UserResourcesPath
 * Defines the users' resources path. This path is readable and writable for the user.
 * When setting this path, the Core::ISettings::ReadWriteDatabasesPath is automatically calculated.
*/

/*! \var Core::ISettings::Paths Core::ISettings::ApplicationPath
 * Defines the application path. This path is to be considered as read-only for the application.
 * When this path is defined, some paths are automatically calculated:
     - Core::ISettings::QtFrameWorksPath
     - Core::ISettings::FMFPlugInsPath
     - Core::ISettings::QtPlugInsPath
     - Core::ISettings::BundleRootPath
*/

/*! \var Core::ISettings::Paths Core::ISettings::BundleRootPath
 * Defines the root path of the bundle. On MacOs, the path is the one where lies the \e Application.app. On other OS,
 * the path is the path where stands the \e Application path.
*/

/*! \var Core::ISettings::Paths Core::ISettings::BundleResourcesPath
 * Defines the application bundle resources path. This path is to be considered as read-only for the application.
 * When setting this path, some paths are calculated:
     - Core::ISettings::BundleResourcesPath
     - Core::ISettings::ReadOnlyDatabasesPath
     - Core::ISettings::TranslationsPath
     - Core::ISettings::ThemeRootPath
     - Core::ISettings::SmallPixmapPath
     - Core::ISettings::MediumPixmapPath
     - Core::ISettings::BigPixmapPath
     - Core::ISettings::CompleteFormsPath
     - Core::ISettings::SubFormsPath
     - Core::ISettings::DocumentationPath
*/

/*! \var Core::ISettings::Paths Core::ISettings::ReadOnlyDatabasesPath
 * Defines the read only databases path (mainly stored in application's bundle resources path).
*/

/*! \var Core::ISettings::Paths Core::ISettings::ReadWriteDatabasesPath
 * Defines the read-write databases path (mainly stored in user's resources path).
*/

/*! \var Core::ISettings::Paths Core::ISettings::TranslationsPath
 * Defines the translations dictionaries path.
*/

/*! \var Core::ISettings::Paths Core::ISettings::QtPlugInsPath
 * Linked to the application path. Represents the application linked Qt plugins path.
*/

/*! \var Core::ISettings::Paths Core::ISettings::QtFrameWorksPath
 * Linked to the application path. Represents the application linked Qt framework path.
*/

/*! \var Core::ISettings::Paths Core::ISettings::FMFPlugInsPath
 * Linked to the application path. Represents the application plugin path.
*/

/*! \var Core::ISettings::Paths Core::ISettings::ThemeRootPath
 * Defines the theme root path.
*/

/*! \var Core::ISettings::Paths Core::ISettings::SmallPixmapPath
 * Defines the theme 16x16 pixmap.
*/

/*! \var Core::ISettings::Paths Core::ISettings::MediumPixmapPath
 * Defines the theme 32x32 pixmap.
*/

/*! \var Core::ISettings::Paths Core::ISettings::BigPixmapPath
 * Defines the theme 64x64 pixmap.
*/

/*! \var Core::ISettings::Paths Core::ISettings::SystemTempPath
 * Defines the system temporary path (accessible in read-write mode).
*/

/*! \var Core::ISettings::Paths Core::ISettings::ApplicationTempPath
 * Defines the temporary application path.
*/

/*! \var Core::ISettings::Paths Core::ISettings::CompleteFormsPath
 * Defines the Bundled full patient forms (used only with FreeMedForms).
*/

/*! \var Core::ISettings::Paths Core::ISettings::SubFormsPath
 * Defines the Bundled sub-forms (used only with FreeMedForms).
*/

/*! \var Core::ISettings::Paths Core::ISettings::DocumentationPath
 * Defines the users' manual path.
 * When the application is built with the LINUX_INTEGRATED config flag, the DocumentationPath is set to <em>/usr/share/doc/ApplicationName-doc/html/</em>.
 * Otherwise it is set inside the Bundle.
*/

/*! \var Core::ISettings::Paths Core::ISettings::DataPackPersistentTempPath
 * The datapack persistent cache path is the place where pack and config files are downloaded by the DataPack lib.
*/

/*! \var Core::ISettings::Paths Core::ISettings::DataPackInstallPath
 * Datapack user specific install path.
*/

/*! \var Core::ISettings::Paths Core::ISettings::DataPackApplicationPath
 * Datapacks provided by default with the application are stored in this path.
*/

/*! \var Core::ISettings::Paths Core::ISettings::WebSiteUrl
 * Defines the application main web site.
*/

/*! \var Core::ISettings::Paths Core::ISettings::UpdateUrl
 * Defines the URL to use for the update changelog retriever.
*/












#include "isettings.h"
#include "settings_p.h"

#include <translationutils/constanttranslations.h>
#include <utils/log.h>

#include <utils/global.h>

#include <QApplication>
#include <QMainWindow>
#include <QDockWidget>
#include <QTreeWidget>
#include <QFileInfo>
#include <QDir>
#include <QRegExp>
#include <QDesktopWidget>

namespace {

    const char* const WEBSITE              = "http://www.freemedforms.org/";

    const char* const READONLYDATABASE     = "/databases";
    const char* const TRANSLATIONS_PATH    = "/translations";
    const char* const DEFAULTFORMS         = "/forms";
    const char* const DEFAULTDATAPACK_APPINSTALLED               = "/datapacks/appinstalled";
    const char* const DEFAULTDATAPACK_APPINSTALLED_SUBFORMS      = "/datapacks/appinstalled/forms/subforms";
    const char* const DEFAULTDATAPACK_APPINSTALLED_COMPLETEFORMS = "/datapacks/appinstalled/forms/completeforms";
    const char* const DEFAULT_BUNDLED_ALERTPACKS = "/alertpacks";
    const char* const DEFAULTTHEME_PATH    = "";
    const char* const DEFAULTTHEME_PIXMAP  = "/pixmap";
    const char* const USERMANUAL_PATH      = "/doc/%1";
    const char* const LINUX_USERMANUAL_PATH  = "/usr/share/doc/freemedforms-project/%1/html";
    const char* const USER_SUBFORMSPATH       = "/forms/subforms";
    const char* const USER_COMPLETEFORMSPATH  = "/forms/completeforms";

    const char* const NONMAC_PLUGINSPATH   = "/plugins";
    const char* const ALL_QTPLUGINSPATH    = "/qt";
    const char* const MAC_QTFRAMEWORKPATH  = "/../FrameWorks";
    const char* const WIN_QTFRAMEWORKPATH  = "";
    const char* const UNIX_QTFRAMEWORKPATH = "/../libs";
    const char* const MAC_TOBUNDLEROOTPATH = "/../../..";
    const char* const WIN_TOBUNDLEROOTPATH = "/..";
    const char* const NUX_TOBUNDLEROOTPATH = "/..";
    const char* const BSD_TOBUNDLEROOTPATH = "/..";


    const char* const S_LICENSE_VERSION    = "License/AcceptedVersion";
    const char* const S_DATABASECONNECTOR  = "Network/Db";
    const char* const S_DEFAULTFORM        = "Form/Default";

}

using namespace Core;
using namespace Core::Internal;



SettingsPrivate::SettingsPrivate(QObject *parent, const QString &appName, const QString &fileName) :
    ISettings(parent),
    m_NetworkSettings(0),
    m_UserSettings(0),
    m_NeedsSync(false)
{
    setObjectName("SettingsPrivate");

    QString userFileName = getIniFile(appName, fileName);
    QFileInfo file(userFileName);
    QString networkFileName = QString("%1/%2-net.%3")
            .arg(file.absolutePath())
            .arg(file.baseName())
            .arg(file.completeSuffix());
    m_NetworkSettings = new QSettings(networkFileName, QSettings::IniFormat, this);
    LOG_FOR("Settings", "Using network ini file " + networkFileName);

    m_UserSettings = new QSettings(userFileName, QSettings::IniFormat, this);
    QString resourcesPath;
    QString applicationName;

    if (appName.isEmpty())
        applicationName = qApp->applicationName();
    else
        applicationName = appName;
    if (applicationName.contains(" "))
        applicationName = applicationName.left(applicationName.indexOf(" "));
    Q_ASSERT(!applicationName.isEmpty());

    setPath(ApplicationPath, qApp->applicationDirPath());
    setPath(ApplicationTempPath, QString("%1/%2-%3").arg(QDir::tempPath()).arg(qApp->applicationName()).arg(Utils::createUid()));
    setPath(SystemTempPath, path(ApplicationTempPath));
    setPath(WebSiteUrl, WEBSITE);
    setPath(UserDocumentsPath, QDir::homePath() + QDir::separator() + applicationName.remove("-alpha").remove("_alpha").remove("_debug").remove("-debug") + QDir::separator() + "Documents");

    if (!QDir(path(ApplicationTempPath)).exists()) {
        QDir().mkpath(path(ApplicationTempPath));
    }


    if (Utils::isDebugWithoutInstallCompilation()) {
        LOG("Compiled with the debug_and_release configuration");
        QString res;
        if (Utils::isRunningOnMac())
            res = qApp->applicationDirPath() + "/../../../../../global_resources";
        else
            res = qApp->applicationDirPath() + "/../../global_resources";

        res = QDir::cleanPath(res);
        resourcesPath = res + "/";
        setPath(UserResourcesPath, QFileInfo(file).absolutePath());
        setPath(BundleResourcesPath, resourcesPath);
    } else {
        if (Utils::isLinuxIntegratedCompilation()) {
            setPath(BundleResourcesPath, QString("/usr/share/freemedforms"));
        } else {
            QString bundleResourcesPath;
            if (!Utils::isDebugWithoutInstallCompilation())
                bundleResourcesPath = "Resources";  // resources are located into global_resources paths

            if (Utils::isRunningOnMac())
                setPath(BundleResourcesPath, qApp->applicationDirPath() + "/../" + bundleResourcesPath);
            else
                setPath(BundleResourcesPath, qApp->applicationDirPath() + QDir::separator() + bundleResourcesPath);
        }
        m_FirstTime = value("FirstTimeRunning", true).toBool();
        setPath(UserResourcesPath, QFileInfo(file).absolutePath());//QDir::homePath() + "/." + applicationName);//resourcesPath);
    }

    readDatabaseConnector();
    LOG(databaseConnector().toString());

    if (parent)
        setParent(parent);
    else
        setParent(qApp);
}

SettingsPrivate::~SettingsPrivate()
{
    if (m_NetworkSettings) {
        m_NetworkSettings->sync();
        delete m_NetworkSettings;
        m_NetworkSettings = 0;
    }
    if (m_UserSettings) {
        delete m_UserSettings;
        m_UserSettings = 0;
    }
    QString error;
    Utils::removeDirRecursively(path(ApplicationTempPath), &error);
    if (!error.isEmpty())
        LOG_ERROR(QString("Unable to remove application temporary path: %1; %2").arg(path(ApplicationTempPath)).arg(error));
}

void SettingsPrivate::setUserSettings(const QString &content)
{
    QString fileName = path(ApplicationTempPath) + QDir::separator() + Utils::Database::createUid() + ".ini";
    QFile f(fileName);
    while (f.exists()) {
        fileName = path(ApplicationTempPath) + QDir::separator() + Utils::Database::createUid() + ".ini";
        f.setFileName(fileName);
    }

    if (!Utils::saveStringToFile(content, fileName, Utils::Overwrite, Utils::DontWarnUser))
        LOG_ERROR("Unable to save user preferences content");

    if (m_UserSettings) {
        delete m_UserSettings;
        m_UserSettings = 0;
    }
    m_UserSettings = new QSettings(fileName, QSettings::IniFormat, this);
    m_NeedsSync = false;
}

QString SettingsPrivate::userSettings() const
{
    return Utils::readTextFile(m_UserSettings->fileName(), Utils::DontWarnUser);
}


QSettings *SettingsPrivate::getQSettings()
{
    return m_UserSettings;
}

void SettingsPrivate::beginGroup(const QString &prefix) { m_UserSettings->beginGroup(prefix); m_NeedsSync=true; }
QStringList SettingsPrivate::childGroups() const { return m_UserSettings->childGroups(); }
QStringList SettingsPrivate::childKeys() const { return m_UserSettings->childKeys(); }
bool SettingsPrivate::contains(const QString &key) const { return m_UserSettings->contains(key); }
void SettingsPrivate::endGroup() { m_UserSettings->endGroup(); m_NeedsSync=true; }
QString SettingsPrivate::fileName() const { return m_UserSettings->fileName(); }
QString SettingsPrivate::group() const { return m_UserSettings->group();}

void SettingsPrivate::setValue(const QString &key, const QVariant &value)
{
    if (m_UserSettings->value(key, QVariant()) == value)
        return;
    m_UserSettings->setValue(key, value);
    m_NeedsSync=true;
}

QVariant SettingsPrivate::value(const QString &key, const QVariant &defaultValue) const
{
    return m_UserSettings->value(key, defaultValue);
}

void SettingsPrivate::sync()
{
    if (!m_NeedsSync)
        return;
    if (m_UserSettings)
        m_UserSettings->sync();
    if (m_NetworkSettings)
        m_NetworkSettings->sync();
    m_NeedsSync = false;
    Q_EMIT userSettingsSynchronized();
}


void SettingsPrivate::setPath(const int type, const QString & absPath)
{
    switch (type)
    {
        case WebSiteUrl :
        {
            m_Enum_Path.insert(type, absPath);
            qApp->setOrganizationDomain(absPath);
            break;
        }
        case UpdateUrl :
        {
            m_Enum_Path.insert(type, absPath);
            break;
        }
        case UserResourcesPath :
        {
            QString resourcesPath = QDir::cleanPath(absPath);
            m_Enum_Path.insert(UserResourcesPath, resourcesPath);
            QString dbPath = resourcesPath + "/databases";
            Utils::checkDir(dbPath, true, "Settings::ReadWriteDatabasesPath");
            m_Enum_Path.insert(ReadWriteDatabasesPath, dbPath);
            QString packtmp = resourcesPath + "/datapacks/tmp";
            Utils::checkDir(packtmp, true, "Settings::DataPackPersistentTempPath");
            m_Enum_Path.insert(DataPackPersistentTempPath, packtmp);
            QString packinst = resourcesPath + "/datapacks/install";
            Utils::checkDir(packinst, true, "Settings::DataPackPersistentTempPath");
            m_Enum_Path.insert(DataPackInstallPath, packinst);
            packinst = resourcesPath + "/datapacks/install/forms/completeforms";
            Utils::checkDir(packinst, true, "Settings::DataPackCompleteFormsInstallPath");
            m_Enum_Path.insert(DataPackCompleteFormsInstallPath, packinst);
            packinst = resourcesPath + "/datapacks/install/forms/subforms";
            Utils::checkDir(packinst, true, "Settings::DataPackSubFormsInstallPath");
            m_Enum_Path.insert(DataPackSubFormsInstallPath, packinst);
            break;
        }
        case BundleResourcesPath :
        {
            if (m_Enum_Path.value(BundleResourcesPath)==QDir::cleanPath(absPath))
                break;
            QString bundlePath = QDir::cleanPath(absPath);
            m_Enum_Path.insert(BundleResourcesPath, bundlePath);
            m_Enum_Path.insert(ReadOnlyDatabasesPath, bundlePath + READONLYDATABASE);
            m_Enum_Path.insert(TranslationsPath, bundlePath + TRANSLATIONS_PATH);
            m_Enum_Path.insert(ThemeRootPath, bundlePath + DEFAULTTHEME_PATH);
            m_Enum_Path.insert(SmallPixmapPath, bundlePath + DEFAULTTHEME_PIXMAP + "/16x16/");
            m_Enum_Path.insert(MediumPixmapPath, bundlePath + DEFAULTTHEME_PIXMAP + "/32x32/");
            m_Enum_Path.insert(BigPixmapPath, bundlePath + DEFAULTTHEME_PIXMAP + "/64x64/");
            m_Enum_Path.insert(SvgPixmapPath, bundlePath + DEFAULTTHEME_PIXMAP + "/svg/");
            m_Enum_Path.insert(SplashScreenPixmapPath, bundlePath + DEFAULTTHEME_PIXMAP + "/splashscreens/");
            m_Enum_Path.insert(SubFormsPath, bundlePath + DEFAULTFORMS + "/subforms");
            m_Enum_Path.insert(CompleteFormsPath, bundlePath + DEFAULTFORMS + "/completeforms");
            m_Enum_Path.insert(DataPackApplicationPath, bundlePath + DEFAULTDATAPACK_APPINSTALLED);
            m_Enum_Path.insert(DataPackCompleteFormsPath, bundlePath + DEFAULTDATAPACK_APPINSTALLED_COMPLETEFORMS);
            m_Enum_Path.insert(DataPackSubFormsPath, bundlePath + DEFAULTDATAPACK_APPINSTALLED_SUBFORMS);
            m_Enum_Path.insert(BundledAlertPacks, bundlePath + DEFAULT_BUNDLED_ALERTPACKS);
            QString appname = qApp->applicationName().toLower();
            if (qApp->applicationName().contains(" ")) {
                appname = appname.left(appname.indexOf(" "));
            }
            if (!Utils::isReleaseCompilation()) {
                if (appname.contains("_d"))
                    appname = appname.left(appname.indexOf("_d"));
            }
            if (Utils::isLinuxIntegratedCompilation()) {
                if (QDir(QString(LINUX_USERMANUAL_PATH).arg(appname)).exists()) {
                    m_Enum_Path.insert(DocumentationPath, QString(LINUX_USERMANUAL_PATH).arg(appname));
                } else {
                    m_Enum_Path.insert(DocumentationPath, bundlePath + QString(USERMANUAL_PATH).arg(appname));
                }
            } else {
                m_Enum_Path.insert(DocumentationPath, bundlePath + QString(USERMANUAL_PATH).arg(appname));
            }
            break;
        }
        case ApplicationPath :
        {
            if (m_Enum_Path.value(ApplicationPath)==QDir::cleanPath(absPath))
                break;
            m_Enum_Path.insert(ApplicationPath, absPath);
            if (Utils::isRunningOnMac()) {
                QString pluginPath;
                if (Utils::isDebugWithoutInstallCompilation())
                    pluginPath = "/../../../plugins";
                else
                    pluginPath = "/../plugins";
                m_Enum_Path.insert(QtFrameWorksPath, QDir::cleanPath(absPath + MAC_QTFRAMEWORKPATH));
                m_Enum_Path.insert(FMFPlugInsPath, QDir::cleanPath(absPath + pluginPath));
                m_Enum_Path.insert(QtPlugInsPath, QDir::cleanPath(absPath + pluginPath + ALL_QTPLUGINSPATH));
                m_Enum_Path.insert(BundleRootPath, QDir::cleanPath(absPath + MAC_TOBUNDLEROOTPATH));
            } else if (Utils::isRunningOnLinux()) {
#ifndef LINUX_INTEGRATED
                m_Enum_Path.insert(QtFrameWorksPath, QDir::cleanPath(absPath + UNIX_QTFRAMEWORKPATH));
                m_Enum_Path.insert(QtPlugInsPath, QDir::cleanPath(absPath + NONMAC_PLUGINSPATH + ALL_QTPLUGINSPATH));
#else
                m_Enum_Path.insert(QtFrameWorksPath, QDir::cleanPath(LINUX_QT_PATH));
                m_Enum_Path.insert(QtPlugInsPath, QDir::cleanPath(LINUX_QT_PLUGINS_PATH));
#endif
                m_Enum_Path.insert(FMFPlugInsPath, QDir::cleanPath(absPath + NONMAC_PLUGINSPATH));
                m_Enum_Path.insert(BundleRootPath, QDir::cleanPath(absPath + NUX_TOBUNDLEROOTPATH));
            } else if (Utils::isRunningOnWin()) {
                m_Enum_Path.insert(QtFrameWorksPath, QDir::cleanPath(absPath + WIN_QTFRAMEWORKPATH));
                m_Enum_Path.insert(FMFPlugInsPath, QDir::cleanPath(absPath + NONMAC_PLUGINSPATH));
                m_Enum_Path.insert(QtPlugInsPath, QDir::cleanPath(absPath + NONMAC_PLUGINSPATH + ALL_QTPLUGINSPATH));
                m_Enum_Path.insert(BundleRootPath, QDir::cleanPath(absPath + WIN_TOBUNDLEROOTPATH));
            } else if (Utils::isRunningOnFreebsd()) {
                m_Enum_Path.insert(QtFrameWorksPath, QDir::cleanPath(absPath + UNIX_QTFRAMEWORKPATH));
                m_Enum_Path.insert(FMFPlugInsPath, QDir::cleanPath(absPath + NONMAC_PLUGINSPATH));
                m_Enum_Path.insert(QtPlugInsPath, QDir::cleanPath(absPath + NONMAC_PLUGINSPATH + ALL_QTPLUGINSPATH));
                m_Enum_Path.insert(BundleRootPath, QDir::cleanPath(absPath + BSD_TOBUNDLEROOTPATH));
            }
            break;
        }
    case UserDocumentsPath:
    {
        const QString &cleanPath = QDir::cleanPath(absPath);
        Utils::checkDir(cleanPath, true, "Settings::UserDocumentsPath");
        Utils::checkDir(cleanPath + USER_SUBFORMSPATH, true, "Settings::UserSubFormsPath");
        Utils::checkDir(cleanPath + USER_COMPLETEFORMSPATH, true, "Settings::UserCompleteFormsPath");
        m_Enum_Path.insert(UserDocumentsPath, cleanPath);
        m_Enum_Path.insert(UserSubFormsPath, cleanPath + USER_SUBFORMSPATH);
        m_Enum_Path.insert(UserCompleteFormsPath, cleanPath + USER_COMPLETEFORMSPATH);
        break;
    }
    default: m_Enum_Path.insert(type, QDir::cleanPath(absPath)); break;
    }
}


QString SettingsPrivate::path(const int type) const
{
    if (type == ISettings::DocumentationPath) {
        QString tmp = m_Enum_Path.value(type);
        QString trPath = QString("%1/%2/html")
                .arg(tmp).arg(QLocale()
                              .name().left(2));
        if (QDir(trPath).exists())
            return trPath;
        else
            return QString("%1/en/html").arg(tmp);

    }
    return m_Enum_Path.value(type);
}


bool SettingsPrivate::firstTimeRunning(const QString &subProcess) const
{
    if (subProcess.isEmpty())
        return m_NetworkSettings->value("FirstTimeRunning", true).toBool();
    return m_UserSettings->value("FirstTimeRunning/" + subProcess, true).toBool();
}


void SettingsPrivate::noMoreFirstTimeRunning(const QString &subProcess)
{
    if (subProcess.isEmpty()) {
        m_NetworkSettings->setValue("FirstTimeRunning", false);
        m_NetworkSettings->sync();
        m_FirstTime = false;
    } else {
        m_UserSettings->setValue("FirstTimeRunning/" + subProcess, false);
        m_NeedsSync = true;
        Q_EMIT userSettingsSynchronized();
    }
}


void SettingsPrivate::setFirstTimeRunning(const bool state, const QString &subProcess)
{
    if (subProcess.isEmpty()) {
        m_NetworkSettings->setValue("FirstTimeRunning", state);
        m_NetworkSettings->sync();
        m_FirstTime = false;
    } else {
        m_UserSettings->setValue("FirstTimeRunning/" + subProcess, state);
        m_NeedsSync = true;
        Q_EMIT userSettingsSynchronized();
    }
}


QString SettingsPrivate::licenseApprovedApplicationNumber() const
{
    return m_NetworkSettings->value(S_LICENSE_VERSION).toString();
}


void SettingsPrivate::setLicenseApprovedApplicationNumber(const QString &version)
{
    m_NetworkSettings->setValue(S_LICENSE_VERSION, version);
}


void SettingsPrivate::setDefaultForm(const QString &formUid)
{
    m_NetworkSettings->setValue(S_DEFAULTFORM, formUid);
}


QString SettingsPrivate::defaultForm() const
{
    return m_NetworkSettings->value(S_DEFAULTFORM).toString();
}


QString SettingsPrivate::getIniFile(const QString & appName, const QString & fileName)
{
    QString tmpAppName = appName;
    QString tmpFileName = fileName;
    if (appName.isEmpty()) {
        tmpAppName = qApp->applicationName();
        if (tmpAppName.contains(" "))
            tmpAppName = tmpAppName.left(tmpAppName.indexOf(" "));
    }
    tmpAppName.prepend(".");
    if (Utils::isAlpha())
        tmpAppName.append("_alpha");

    if (fileName.isEmpty())
        tmpFileName = "config.ini";

    QStringList list = QCoreApplication::arguments();
    int index = list.indexOf(QRegExp("--config=*", Qt::CaseSensitive, QRegExp::Wildcard));
    if (index != -1) {
        QString iniFileName = list[index];
        iniFileName = iniFileName.mid(iniFileName.indexOf("=") + 1);
        LOG_FOR("Settings", tr("Passing command line ini file: %1").arg(iniFileName));

        if (QDir::isRelativePath(iniFileName))
            iniFileName.prepend(qApp->applicationDirPath() + QDir::separator());
        iniFileName = QDir::cleanPath(iniFileName);

        QFileInfo info(iniFileName);
        if (info.exists() && info.isReadable()) {
            if (info.isWritable()) {
                LOG_FOR("Settings", tr("Using ini file %1.").arg(iniFileName));
                return iniFileName;
            } else {
                LOG_ERROR_FOR("Settings", tr("Ini file %1 is not writable. Can not use it.").arg(iniFileName));
            }
        } else {
            QFile file(iniFileName);
            if (file.open(QIODevice::ReadWrite | QIODevice::Text)) {
                LOG_FOR("Settings", tr("Using ini file %1").arg(iniFileName));
                return iniFileName;
            } else {
                LOG_FOR("Settings", tr("WARNING: Ini file %1 can not be used.").arg(iniFileName));
            }
        }
    }

    if (QFile(QString("%1/pathtoconfig.ini").arg(qApp->applicationDirPath())).exists()) {
        QString content = Utils::readTextFile(QString("%1/pathtoconfig.ini").arg(qApp->applicationDirPath()), Utils::DontWarnUser);
        content = content.trimmed();
        if (!content.isEmpty()) {
            LOG_FOR("Settings", tr("Found a configuration file next to the binary. Reading file."));
            if (content.startsWith("~/"))
                content.replace("~/", QDir::homePath() + "/");
            if (QDir::isRelativePath(content))
                content.prepend(qApp->applicationDirPath() + QDir::separator());
            content = QDir::cleanPath(content);
            QFileInfo info(content);
            if (info.exists() && info.isReadable()) {
                if (info.isWritable()) {
                    LOG_FOR("Settings", tr("Using ini file %1.").arg(content));
                    return content;
                } else {
                    LOG_ERROR_FOR("Settings", tr("Ini file %1 is not writable. Can not use it.").arg(content));
                }
            } else {
                if (!info.absoluteDir().exists()) {
                    if (!QDir().mkpath(info.absolutePath()))
                        LOG_ERROR_FOR("Settings", tr("File does not exists and can not be created: %1").arg(content));
                }
                QFile file(content);
                if (file.open(QIODevice::ReadWrite | QIODevice::Text)) {
                    LOG_FOR("Settings", tr("Using ini file %1").arg(content));
                    return content;
                } else {
                    LOG_FOR("Settings", tr("WARNING: Ini file %1 can not be used.").arg(content));
                }
            }
            LOG_ERROR_FOR("Settings", tr("File does not exists and can not be created: %1").arg(content));
        }
    }

    QString iniFile;

    iniFile = QString("%1/%2/%3").arg(QDir::homePath(), tmpAppName, tmpFileName);
    LOG_FOR("Settings", tr("Trying ini file %1").arg(iniFile));
    QDir dir(QFileInfo(iniFile).absolutePath());

    if (!dir.exists()) {
        dir.cdUp();
        if (!dir.mkdir(tmpAppName)) {
            LOG_ERROR_FOR("Settings" , tr("Unable to create dir: %1, no Ini File can be used.").arg(dir.absolutePath() + QDir::separator() + tmpAppName));
            return QString::null;
        }
    }

    LOG_FOR("Settings", tr("Using ini file %1").arg(iniFile));
    return iniFile;
}


void SettingsPrivate::restoreState(QMainWindow *window, const QString & prefix)
{
    if (!window)
        return;
    QString keyGeo = prefix + "MainWindow/Geometry";
    QString keyState = prefix + "MainWindow/State";
    if (value(keyGeo).toByteArray().isEmpty()) {
        int height = qApp->desktop()->height() * 0.75;
        int width = qApp->desktop()->width() * 0.75;
        double screenRatio = (double)width/(double)height;
        QSize ratio;
        if (screenRatio < 1.5) {
            ratio = QSize(4,3);
        } else {
            ratio = QSize(16, 9);
        }
        QSize result(width, height);
        ratio.scale(result, Qt::KeepAspectRatio);
        QRect appScreen(QPoint(0,0), ratio);
        QRect rect = qApp->desktop()->rect();
        appScreen.moveCenter(rect.center());
        window->setGeometry(appScreen);
    } else {
        window->restoreGeometry(value(keyGeo).toByteArray());
        window->restoreState(value(keyState).toByteArray());
        QStringList k = m_UserSettings->allKeys().filter(QRegExp(QString(prefix + "Dock/"), Qt::CaseSensitive, QRegExp::Wildcard));
        QWidget *w = 0;
        foreach(const QString &s, k) {
            w = window->findChild<QDockWidget*>(s.mid(s.indexOf("Dock/")+5));
            if (w) {
                w->restoreGeometry(value(prefix + "/Dock/" + w->objectName()).toByteArray());
            }
            w = 0;
        }
    }
}


void SettingsPrivate::saveState(QMainWindow * window, const QString & prefix)
{
    if (!window)
        return;
    setValue(prefix + "MainWindow/Geometry", window->saveGeometry());
    setValue(prefix + "MainWindow/State", window->saveState());
    foreach(QDockWidget * w, window->findChildren<QDockWidget*>()) {
        setValue(prefix + "Dock/" + w->objectName(), w->saveGeometry());
    }
}


void SettingsPrivate::appendToValue(const QString &key, const QString &value)
{
    QVariant q = this->value(key);
    if (q.isNull()) {
        this->setValue(key, value);
        return;
    }
    if (q.toStringList().indexOf(value) == -1) {
        QStringList list = q.toStringList();
        list.append(value);
        this->setValue(key, list);
    }
}


QTreeWidget* SettingsPrivate::getTreeWidget(QWidget *parent) const
{
    QTreeWidget * tree = qobject_cast<QTreeWidget*>(parent);
    if (!tree)
        tree = new QTreeWidget(parent);

    tree->setColumnCount(2);
    tree->setHeaderLabels(QStringList() << tr("Name") << tr("Value"));
    QFont bold;
    bold.setBold(true);

    QTreeWidgetItem * sysItem = new QTreeWidgetItem(tree, QStringList() << tr("System information"));
    sysItem->setFont(0,bold);
    new QTreeWidgetItem(sysItem, QStringList() << tr("Operating System") << Utils::osName());
#ifndef Q_OS_WIN32
    new QTreeWidgetItem(sysItem, QStringList() << tr("uname output") << Utils::uname());
#endif

    QTreeWidgetItem * compileItem = new QTreeWidgetItem(tree, QStringList() << Trans::ConstantTranslations::tkTr(Trans::Constants::BUILD_INFORMATION));
    compileItem->setFont(0,bold);
    new QTreeWidgetItem(compileItem, QStringList() << tr("Compile Qt version") << QString("%1").arg(QT_VERSION_STR));
    new QTreeWidgetItem(compileItem, QStringList() << tr("Actual Qt version") << QString("%1").arg(qVersion()));
    new QTreeWidgetItem(compileItem, QStringList() << Trans::ConstantTranslations::tkTr(Trans::Constants::BUILD_VERSION_1).arg("") << qApp->applicationVersion());
    if (Utils::isReleaseCompilation()) {
        new QTreeWidgetItem(compileItem, QStringList() << tr("Compile mode") << Trans::ConstantTranslations::tkTr(Trans::Constants::BUILD_RELEASE));
    } else {
        if (Utils::isDebugWithoutInstallCompilation()) {
            new QTreeWidgetItem(compileItem, QStringList() << tr("Compile mode") << Trans::ConstantTranslations::tkTr(Trans::Constants::BUILD_DEBUG) + " - no install");
        } else {
            new QTreeWidgetItem(compileItem, QStringList() << tr("Compile mode") << Trans::ConstantTranslations::tkTr(Trans::Constants::BUILD_DEBUG));
        }
    }
    new QTreeWidgetItem(compileItem, QStringList() << tr("GIT revision") << QString(GIT_REVISION_HASH));


    QDir appDir(qApp->applicationDirPath());
    QMap<QString, QString> paths;
    paths.insert(tr("Binary"), path(ApplicationPath));
    paths.insert(tr("UserResourcesPath"), path(UserResourcesPath));
    paths.insert(tr("Read only Databases"), path(ReadOnlyDatabasesPath));
    paths.insert(tr("Writable databases"), path(ReadWriteDatabasesPath));
    paths.insert(tr("Bundle root path"), path(BundleRootPath));
    paths.insert(tr("Bundle resources path"), path(BundleResourcesPath));
    paths.insert(tr("Translations path"), path(TranslationsPath));
    paths.insert(tr("Qt Plugins path"), path(QtPlugInsPath));
    paths.insert(tr("Qt FrameWorks path"), path(QtFrameWorksPath));
    paths.insert(tr("FreeMedForms PlugIns path"), path(FMFPlugInsPath));
    paths.insert(tr("SmallPixmapPath"), path(SmallPixmapPath));
    paths.insert(tr("MediumPixmapPath"), path(MediumPixmapPath));
    paths.insert(tr("BigPixmapPath"), path(BigPixmapPath));
    paths.insert(tr("SystemTempPath"), path(SystemTempPath));
    paths.insert(tr("ApplicationTempPath"), path(ApplicationTempPath));
    paths.insert(tr("CompleteFormsPath"), path(CompleteFormsPath));
    paths.insert(tr("SubFormsPath"), path(SubFormsPath));
    paths.insert(tr("UserCompleteFormsPath"), path(UserCompleteFormsPath));
    paths.insert(tr("UserSubFormsPath"), path(UserSubFormsPath));
    paths.insert(tr("Default installed datapack path"), path(DataPackApplicationPath));
    paths.insert(tr("Datapack persistent temporary path"), path(DataPackPersistentTempPath));
    paths.insert(tr("Datapack installation path"), path(DataPackInstallPath));
    paths.insert(tr("Datapack Complete Forms installation path"), path(DataPackCompleteFormsInstallPath));
    paths.insert(tr("Datapack SubForms installation path"), path(DataPackSubFormsInstallPath));
    paths.insert(tr("DocumentationPath"), path(DocumentationPath));

    QTreeWidgetItem * absPathsItem = new QTreeWidgetItem(tree, QStringList() << tr("Absolute Paths"));
    absPathsItem->setFont(0,bold);
    new QTreeWidgetItem(absPathsItem, QStringList() << tr("Using Ini File") << fileName());

    QTreeWidgetItem * relPathsItem = new QTreeWidgetItem(tree, QStringList() << tr("Relative Paths"));
    relPathsItem->setFont(0,bold);
    new QTreeWidgetItem(relPathsItem, QStringList() << tr("Using Ini File") << appDir.relativeFilePath(QFileInfo(fileName()).absoluteFilePath()));

    foreach(const QString & p, paths.keys()) {
        new QTreeWidgetItem(relPathsItem, QStringList() << p << appDir.relativeFilePath(QFileInfo(paths[p]).absoluteFilePath()));
        new QTreeWidgetItem(absPathsItem, QStringList() << p << paths[p]);
    }
    new QTreeWidgetItem(relPathsItem, QStringList() << tr("WebSiteUrl") << path(WebSiteUrl));
    new QTreeWidgetItem(absPathsItem, QStringList() << tr("WebSiteUrl") << path(WebSiteUrl));


    QTreeWidgetItem * settingsItem = new QTreeWidgetItem(tree, QStringList() << tr("Settings values"));
    settingsItem->setFont(0,bold);
    settingsItem->setExpanded(true);
    QTreeWidgetItem * orphan = new QTreeWidgetItem(settingsItem, QStringList() << tr("Orphan settings"));
    orphan->setFont(0,bold);
    QTreeWidgetItem * group = 0;
    QStringList list = m_UserSettings->allKeys();
    qSort(list);
    foreach(const QString & k, list) {
        if (k.contains("/")) {
            QString tmp = k.left(k.indexOf("/"));
            if (!group) {
                group = new QTreeWidgetItem(settingsItem, QStringList() << tmp);
                group->setFont(0,bold);
            }
            if (!k.startsWith(group->text(0))) {
                group = new QTreeWidgetItem(settingsItem, QStringList() << tmp);
                group->setFont(0,bold);
            }
            new QTreeWidgetItem(group, QStringList() << k.mid(k.indexOf("/") + 1)  << value(k).toString());
        }
        else
            new QTreeWidgetItem(orphan, QStringList() << k << value(k).toString());
    }

    tree->resizeColumnToContents(0);
    tree->resizeColumnToContents(1);

    return tree;
}

QString SettingsPrivate::toString() const
{
    QString tmp;
    tmp += "===== SETTINGS =====\n";

    tmp += "\n^ Object ^ Value ^";
    tmp += "\n| " + tr("Running version: %1").arg(qApp->applicationName()).replace(":", "|") + " |";
    tmp += "\n| " + tr("Qt Build version: %1").arg(QT_VERSION_STR).replace(":", "|") + " |";
    tmp += "\n| " + tr("Qt running version: %1").arg(qVersion()).replace(":", "|") + " |";
    tmp += "\n| " + tr("Application Version: %1").arg(qApp->applicationVersion()).replace(":", "|") + " |";
    if (!Utils::isReleaseCompilation())
        tmp += "\n| " + tr("Actual build: Debug").replace(":", "|") + " |";
    else
        tmp += "\n| " + tr("Actual build: Release").replace(":", "|") + " |";
    tmp += "\n| " + tr("GIT revision: %1").arg(GIT_REVISION_HASH).replace(":", "|") + " |";
    tmp += "\n| " + tr("Application path: %1").arg(qApp->applicationDirPath()).replace(":", "|") + " |";
    tmp += "\n| " + QString("Ini File Name: %2").arg(fileName()).replace(":", "|") + " |";
    tmp += "\n| " + tr("Using Ini File") + " | " + fileName() + " |";
    if (Utils::isRunningOnLinux()) {
        tmp.append("\n| " + tr("Running on Linux"));
        tmp += "| " + tr("uname returns: %1").arg(Utils::uname()).replace("\n"," ") + " |";
    }
    else if (Utils::isRunningOnMac()) {
        tmp.append("\n| " + tr("Running on MacOs"));
        tmp += "| " + tr("uname returns: %1").arg(Utils::uname()).replace("\n"," ") + " |";
    }
    else if (Utils::isRunningOnWin())
        tmp.append("| " + tr("Running on Windows"))+ " | |";

    tmp += "\n\n";

    tmp += "===== PATH =====\n\n";
    QMap<QString, QString> paths;
    paths.insert(tr("Binary"), path(ApplicationPath));
    paths.insert(tr("UserResourcesPath"), path(UserResourcesPath));
    paths.insert(tr("Read only Databases"), path(ReadOnlyDatabasesPath));
    paths.insert(tr("Writable databases"), path(ReadWriteDatabasesPath));
    paths.insert(tr("Bundle root path"), path(BundleRootPath));
    paths.insert(tr("Bundle resources path"), path(BundleResourcesPath));
    paths.insert(tr("Translations path"), path(TranslationsPath));
    paths.insert(tr("Qt Plugins path"), path(QtPlugInsPath));
    paths.insert(tr("Qt FrameWorks path"), path(QtFrameWorksPath));
    paths.insert(tr("FreeMedForms PlugIns path"), path(FMFPlugInsPath));
    paths.insert(tr("SmallPixmapPath"), path(SmallPixmapPath));
    paths.insert(tr("MediumPixmapPath"), path(MediumPixmapPath));
    paths.insert(tr("BigPixmapPath"), path(BigPixmapPath));
    paths.insert(tr("SystemTempPath"), path(SystemTempPath));
    paths.insert(tr("ApplicationTempPath"), path(ApplicationTempPath));
    paths.insert(tr("FormsPath"), path(CompleteFormsPath));
    paths.insert(tr("UserCompleteFormsPath"), path(UserCompleteFormsPath));
    paths.insert(tr("UserSubFormsPath"), path(UserSubFormsPath));
    paths.insert(tr("Default installed datapack path"), path(DataPackApplicationPath));
    paths.insert(tr("Datapack persistent temporary path"), path(DataPackPersistentTempPath));
    paths.insert(tr("Datapack installation path"), path(DataPackInstallPath));
    paths.insert(tr("Datapack Complete Forms installation path"), path(DataPackCompleteFormsInstallPath));
    paths.insert(tr("Datapack SubForms installation path"), path(DataPackSubFormsInstallPath));
    paths.insert(tr("DocumentationPath"), path(DocumentationPath));
    paths.insert(tr("WebSiteUrl"), path(WebSiteUrl));
    tmp += "^ Resource ^ Path ^\n";
    foreach(const QString &p, paths.keys())
        tmp += "| " + p + " | " + paths[p] + " |\n";


    tmp += "===== USER INI VALUES =====\n\n";
    tmp += "^ Group ^ Name ^ Value ^\n";
    QStringList keys = m_UserSettings->childKeys();
    keys.sort();
    foreach(const QString &k, keys)
        tmp += QString("| RootKeys | %1 | <nowiki>%2</nowiki> |\n").arg(k, m_UserSettings->value(k).toString());

    QStringList groups = m_UserSettings->childGroups();
    groups.sort();
    foreach(const QString &g, groups) {
        keys.clear();
        m_UserSettings->beginGroup(g);
        keys = m_UserSettings->allKeys();
        keys.sort();
        QString groupName;
        foreach(const QString &k, keys) {
            if (groupName.isEmpty())
                groupName = g;
            tmp += QString("| %1 | %2 | <nowiki>%3</nowiki> |\n")
                    .arg(groupName)
                    .arg(k).arg(m_UserSettings->value(k).toString());
            groupName = ":::";
        }
        m_UserSettings->endGroup();
    }

    tmp += "\n\n";

    tmp += "===== NETWORK INI VALUES =====\n\n";
    tmp += "^ Name ^ Value ^\n";
    foreach(const QString &k, m_NetworkSettings->allKeys())
        tmp += QString("| %1 | <nowiki>%2</nowiki> |\n").arg(k, m_NetworkSettings->value(k).toString());

    return tmp;
}

Utils::DatabaseConnector SettingsPrivate::databaseConnector() const
{
    return m_DbConnector;
}


void SettingsPrivate::setDatabaseConnector(Utils::DatabaseConnector &dbConnector)
{
    m_DbConnector = dbConnector;
    m_DbConnector.setAbsPathToReadOnlySqliteDatabase(path(Core::ISettings::ReadOnlyDatabasesPath));
    if (m_DbConnector.absPathToSqliteReadWriteDatabase().isEmpty())
        m_DbConnector.setAbsPathToReadWriteSqliteDatabase(path(Core::ISettings::ReadWriteDatabasesPath));
    Utils::Database::setDatabasePrefix(m_DbConnector.globalDatabasePrefix());
    writeDatabaseConnector();
}


void SettingsPrivate::readDatabaseConnector()
{
    m_DbConnector.fromSettings(m_NetworkSettings->value(S_DATABASECONNECTOR).toString());
    m_DbConnector.setAbsPathToReadOnlySqliteDatabase(path(Core::ISettings::ReadOnlyDatabasesPath));
    if (m_DbConnector.absPathToSqliteReadWriteDatabase().isEmpty())
        m_DbConnector.setAbsPathToReadWriteSqliteDatabase(path(Core::ISettings::ReadWriteDatabasesPath));
    Utils::Database::setDatabasePrefix(m_DbConnector.globalDatabasePrefix());
}


void SettingsPrivate::writeDatabaseConnector()
{
    m_NetworkSettings->setValue(S_DATABASECONNECTOR, m_DbConnector.forSettings());
    m_NetworkSettings->sync();
}
