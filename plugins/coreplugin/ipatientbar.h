/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CORE_IPATIENTBAR_H
#define CORE_IPATIENTBAR_H

#include <coreplugin/core_exporter.h>
#include <QWidget>

/**
 * \file ./plugins/coreplugin/ipatientbar.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {

class CORE_EXPORT IPatientBar : public QWidget
{
    Q_OBJECT

public:
    explicit IPatientBar(QWidget *parent = 0) : QWidget(parent) {}
    virtual ~IPatientBar() {}

    virtual void addBottomWidget(QWidget *widget) = 0;
    virtual void showMessage(const QString &message, int duration_ms = 2000, const QString &css = QString::null) = 0;
};

} // namespace Core

#endif // CORE_IPATIENTBAR_H
