/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IABOUTPAGE_H
#define IABOUTPAGE_H

#include <coreplugin/core_exporter.h>
#include <coreplugin/igenericpage.h>

#include <QObject>
#include <QString>
#include <QWidget>

/**
 * \file ./plugins/coreplugin/iaboutpage.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
 * \class Core::IAboutPage
 * \brief Derive object from this interface and set it inside the PluginManager object pool to get your page in the Core::AboutDialog.
*/

namespace Core {

class CORE_EXPORT IAboutPage : public Core::IGenericPage
{
    Q_OBJECT
public:
    IAboutPage(QObject *parent = 0) : Core::IGenericPage(parent) {}
    virtual ~IAboutPage() {}

    QString title() const {return displayName();}
};

} // namespace Core

#endif // IABOUTPAGE_H
