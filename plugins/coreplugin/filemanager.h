/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CORE_FILEMANAGER_H
#define CORE_FILEMANAGER_H

#include <coreplugin/core_exporter.h>

#include <QObject>
#include <QString>
#include <QDateTime>
#include <QWidget>
#include <QVariant>
#include <QPointer>
#include <QHash>

QT_BEGIN_NAMESPACE
class QTreeWidget;
class QTreeWidgetItem;
QT_END_NAMESPACE


/**
 * \file ./plugins/coreplugin/filemanager.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {

class CORE_EXPORT FileManager : public QObject
{
    Q_OBJECT

public:
    FileManager(QObject *parent = 0);
    virtual ~FileManager() {}

    // settings management
    void setSettingsKey(const QString &key) {m_Key = key;}
    QString settingsKey() const {return m_Key;}
    void saveRecentFiles() const;
    void getRecentFilesFromSettings();
    void getMaximumRecentFilesFromSettings();

    // Recent files management
    void addToRecentFiles(const QString &fileName);
    void setRecentFiles(const QStringList &files) {m_recentFiles = files;}
    QStringList recentFiles() const;
    void setMaximumRecentFiles(int max) {m_maxRecentFiles = max;}

    // current file
    void setCurrentFile(const QString &filePath);
    QString currentFile() const;

private:
    QStringList m_recentFiles;
    int m_maxRecentFiles;
    QString m_currentFile;
    QString m_Key;
};

}  // end Core

#endif  // CORE_FILEMANAGER_H
