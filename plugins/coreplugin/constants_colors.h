/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CORE_CONSTANTS_COLORS_H
#define CORE_CONSTANTS_COLORS_H

/**
 * \file ./plugins/coreplugin/constants_colors.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
namespace Constants {

const char *const COLOR_BACKGROUND_ALERT_HIGH  = "#FF3030";
const char *const COLOR_BACKGROUND_ALERT_MEDIUM  = "#FF8080";
const char *const COLOR_BACKGROUND_ALERT_LOW  = "#FFD0D0";

}  // end Constants
}  // end Core

#endif // CORE_CONSTANTS_COLORS_H
