/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "imainwindow.h"

#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/constants_icons.h>

#include <utils/global.h>

#include <QWidget>
#include <QLabel>
#include <QGridLayout>
#include <QMovie>
#include <QApplication>

#include <QDebug>




using namespace Core;
using namespace Core::Internal;

namespace {
    QDialog *dlg = 0;
    QWidget *widget = 0;
}


void IMainWindow::startProcessingSpinner()
{
    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
}

void IMainWindow::endProcessingSpinner()
{
    QApplication::restoreOverrideCursor();
    if (widget) {
        widget->hide();
        delete widget;
        widget = 0;
    }
    if (dlg) {
        dlg->hide();
        delete dlg;
        dlg = 0;
    }
}
