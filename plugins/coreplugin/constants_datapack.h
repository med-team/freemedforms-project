/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CORE_CONSTANTS_DATAPACK_H
#define CORE_CONSTANTS_DATAPACK_H

/**
 * \file ./plugins/coreplugin/constants_datapack.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
namespace Constants {

const char * const  FREEMEDFORMS_DEFAULT_DATAPACK_HTTPSERVER = "http://test.freemedforms.com/";


}  // end Constants
} // end Core

#endif // CORE_CONSTANTS_DATAPACK_H
