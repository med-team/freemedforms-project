/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "theme.h"

#include <translationutils/constanttranslations.h>
#include <utils/log.h>
#include <utils/global.h>
#include <coreplugin/icore.h>
#include <coreplugin/constants_icons.h>

#include <QCache>
#include <QString>
#include <QApplication>
#include <QDir>
#include <QDesktopWidget>

using namespace Core;
using namespace Core::Internal;

class ThemePrivatePrivate
{
public:
    QIcon icon(const QString &fileName, ThemePrivate::IconSize size);
    QString iconFullPath(const QString &fileName, ThemePrivate::IconSize size);
    QString transformFileName(const QString &fileName, ThemePrivate::IconSize size);

};


ThemePrivate::ThemePrivate(QObject *parent, const int cacheSize)
    : ITheme(parent), m_Splash(0)
{
    if (!parent)
        setParent(qApp);
    setObjectName("ThemePrivate");
    m_IconCache.setMaxCost(cacheSize);
}

ThemePrivate::~ThemePrivate()
{
    if (m_Splash)
        delete m_Splash;
    m_Splash = 0;
}

void ThemePrivate::setThemeRootPath(const QString &absPath)
{
    if (QDir(absPath).exists()) {
        m_AbsolutePath = QDir::cleanPath(absPath);
        LOG(QString("Setting theme path to: %1").arg(m_AbsolutePath));
    }
    else
        LOG_ERROR(Trans::ConstantTranslations::tkTr(Trans::Constants::PATH_1_DOESNOT_EXISTS).arg(absPath));
}

void ThemePrivate::setThemeRelativeRootPath(const QString &relPathFromAppBinary)
{
    QString path = QDir::cleanPath(qApp->applicationDirPath() + QDir::separator() + relPathFromAppBinary);
    if (QDir(path).exists()) {
        m_AbsolutePath = path;
        Utils::Log::addMessage(this, QString("INFO: theme path set to: %1").arg(path));
    }
    else
        LOG_ERROR(Trans::ConstantTranslations::tkTr(Trans::Constants::PATH_1_DOESNOT_EXISTS).arg(relPathFromAppBinary));
}

void ThemePrivate::refreshCache()
{
}

void ThemePrivate::setCacheMaxCost(const int max)
{
    m_IconCache.setMaxCost(max);
}

void ThemePrivate::setSmallIconPath(const QString &absPath)
{
    if (QDir(absPath).exists())
        m_SmallIconPath = absPath;
    else
        LOG_ERROR(Trans::ConstantTranslations::tkTr(Trans::Constants::PATH_1_DOESNOT_EXISTS).arg("SmallIcon: "+absPath));
}

void ThemePrivate::setMediumIconPath(const QString &absPath)
{
    if (QDir(absPath).exists())
        m_MediumIconPath = absPath;
    else
        LOG_ERROR(Trans::ConstantTranslations::tkTr(Trans::Constants::PATH_1_DOESNOT_EXISTS).arg("MediumIcon: "+absPath));
}

void ThemePrivate::setBigIconPath(const QString &absPath)
{
    if (QDir(absPath).exists())
        m_BigIconPath = absPath;
    else
        LOG_ERROR(Trans::ConstantTranslations::tkTr(Trans::Constants::PATH_1_DOESNOT_EXISTS).arg("BigIcon: "+absPath));
}

QIcon ThemePrivate::icon(const QString &fileName, IconSize size)
{
    Q_UNUSED(size);
    Q_ASSERT_X(!m_AbsolutePath.isEmpty(), "ThemePrivate::icon", "No path set");
    QString uid = QString("%1/%2").arg(m_AbsolutePath).arg(fileName);

    if (m_IconCache.contains(uid))
        return QIcon(*m_IconCache[uid]);

    QIcon *i = new QIcon;
    QString fullName;

    fullName = iconFullPath(fileName, SmallIcon);
    if (QFile(fullName).exists())
        i->addFile(fullName, QSize(16,16));

    fullName = iconFullPath(fileName, MediumIcon);
    if (QFile(fullName).exists())
        i->addFile(fullName, QSize(32,32));

    fullName = iconFullPath(fileName, BigIcon);
    if (QFile(fullName).exists())
        i->addFile(fullName, QSize(64,64));

    m_IconCache.insert(uid, i);
    return QIcon(*i);
}

QString ThemePrivate::iconFullPath(const QString &fileName, IconSize size)
{
    QString path = m_AbsolutePath + QDir::separator() + "pixmap" + QDir::separator();
    if (size == ThemePrivate::SmallIcon) {
        if (!m_SmallIconPath.isEmpty())
            path = m_SmallIconPath;
        else
            path += "16x16";
    }
    else if (size == ThemePrivate::MediumIcon) {
        if (!m_MediumIconPath.isEmpty())
            path = m_MediumIconPath;
        else
            path += "32x32";
    }
    else if (size == ThemePrivate::BigIcon) {
        if (!m_BigIconPath.isEmpty())
            path = m_BigIconPath;
        else
            path += "64x64";
    }
    path = QDir::cleanPath(path) + QDir::separator() + fileName;
    return path;
}

QPixmap ThemePrivate::splashScreenPixmap(const QString &fileName, const IconSize size)
{
    QString file = fileName;
    QString extra = "-stable";
    if (Utils::isBeta()) {
        extra = "-beta";
    } else {
        switch (QDate::currentDate().month()) {
        case 1: extra = "-birthday"; break;
        case 7:
        case 8: extra = "-summer"; break;
        case 10 : extra = "-halloween"; break;
        case 12 : extra = "-christmas"; break;
        }
    }
    QFileInfo fi(file);
    if (size==BigIcon) {
        QString tmp = fi.baseName() + extra + "-600." + fi.completeSuffix();
        if (QFile(m_AbsolutePath + "/pixmap/splashscreens/" + tmp).exists()) {
            file = tmp;
        } else {
            tmp = fi.baseName() + "-600." + fi.completeSuffix();
            if (QFile(m_AbsolutePath + "/pixmap/splashscreens/" + tmp).exists()) {
                file = tmp;
            } else {
                file = fileName;
            }
        }
    } else {
        QString tmp = fi.baseName() + extra + "." + fi.completeSuffix();
        if (QFile(m_AbsolutePath + "/pixmap/splashscreens/" + tmp).exists()) {
            file = tmp;
        }
    }

    if (QFile(m_AbsolutePath + "/pixmap/splashscreens/" + file).exists()) {
        LOG(tr("Using splash: %1").arg(m_AbsolutePath + "/pixmap/splashscreens/" + file));
        return QPixmap(m_AbsolutePath + "/pixmap/splashscreens/" + file);
    } else {
        LOG_ERROR(QString("SplashScreen file does not exist %1").arg(m_AbsolutePath + "/pixmap/splashscreens/" + fileName));
    }
    return QPixmap();
}

QPixmap ThemePrivate::defaultGenderPixmap(int gender, const ITheme::IconSize size)
{
    switch (gender) {
    case 0:  return QPixmap(iconFullPath(Core::Constants::ICONMALE, size));
    case 1:  return QPixmap(iconFullPath(Core::Constants::ICONFEMALE, size));
    case 2:  return QPixmap(iconFullPath(Core::Constants::ICONHERMAPHRODISM , size));
    default: return QPixmap();
    }
}

void ThemePrivate::createSplashScreen(const QString &fileName)
{
    if (!m_Splash) {
        LOG_FOR("Theme", "Creating splashscreen");
        if (qApp->desktop()->screenGeometry().width() > 1024) {
            m_Splash = new QSplashScreen(splashScreenPixmap(fileName, BigIcon));
        } else {
            m_Splash = new QSplashScreen(splashScreenPixmap(fileName));
        }
        QFont ft(m_Splash->font());
        ft.setPointSize(ft.pointSize() - 2);
        ft.setBold(true);
        m_Splash->setFont(ft);
        m_Splash->show();
    }
}

void ThemePrivate::messageSplashScreen(const QString &msg)
{
    Q_ASSERT(m_Splash);
    if (m_Splash)
        m_Splash->showMessage(msg, Qt::AlignLeft | Qt::AlignBottom, Qt::black);
}

void ThemePrivate::finishSplashScreen(QWidget *widget)
{
    Q_ASSERT(m_Splash);
    if (m_Splash) {
        m_Splash->finish(widget);
        delete m_Splash;
        m_Splash = 0;
    }
}
