/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef THEMEPRIVATE_H
#define THEMEPRIVATE_H

#include <coreplugin/itheme.h>

#include <coreplugin/core_exporter.h>

#include <QObject>
#include <QIcon>
#include <QPointer>
#include <QCache>

/**
 * \file ./plugins/coreplugin/theme.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
namespace Internal {

class CORE_EXPORT ThemePrivate : public Core::ITheme
{
    Q_OBJECT
public:
    ThemePrivate( QObject *parent = 0, const int cacheSize = 100 );
    ~ThemePrivate();

    void setThemeRootPath( const QString & absPath );
    void setThemeRelativeRootPath( const QString & relativePathFromAppBinary );

    void refreshCache();
    void setCacheMaxCost( const int max );

    QIcon icon( const QString & fileName, IconSize size = SmallIcon );
    QString iconFullPath( const QString &fileName, IconSize size = SmallIcon );
    QPixmap splashScreenPixmap( const QString &fileName, const IconSize size = MediumIcon );
    QPixmap defaultGenderPixmap(int gender, const IconSize size = BigIcon);

    void createSplashScreen(const QString &fileName);
    void messageSplashScreen(const QString &msg);
    void finishSplashScreen(QWidget *widget);
    virtual QSplashScreen *splashScreen() const {return m_Splash;}

protected:
    void setSmallIconPath( const QString &absPath );
    void setMediumIconPath( const QString &absPath );
    void setBigIconPath( const QString &absPath );

private:
    QCache<QString, QIcon> m_IconCache;
    QString m_AbsolutePath;
    QString m_SmallIconPath, m_MediumIconPath, m_BigIconPath;
    QSplashScreen *m_Splash;
};

} // end Internal
} // end Core

#endif // THEMEPRIVATE_H
