#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

# SOURCES and HEADERS needed by action manager
HEADERS += $${PWD}/imode.h \
#    $${PWD}/basemode.h \
    $${PWD}/modemanager.h

SOURCES += \
#    $${PWD}/basemode.cpp \
    $${PWD}/imode.cpp \
    $${PWD}/modemanager.cpp \
