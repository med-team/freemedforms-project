/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IPATIENTLISTENER_H
#define IPATIENTLISTENER_H

#include <coreplugin/core_exporter.h>

#include <QtCore/QObject>

/**
 * \file ./plugins/coreplugin/ipatientlistener.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {

/*!
 * \brief The IPatientListener class provides an interface for a listener to patient events.
 *
 * Currently only the currentPatientAboutToChange() method is available.
 * \sa Patients::PatientModel
 */
class CORE_EXPORT IPatientListener : public QObject
{
    Q_OBJECT
public:
    IPatientListener(QObject *parent) : QObject(parent) {}
    virtual ~IPatientListener() {}

    /*! This method is called in the PatientModel \b before the patient is changed.
     *
     * The listener can do last things here like saving data.
     * @return bool \e true if it is safe to change patient. If returned \e false
     * the current patient is \e not changed.
     */
    virtual bool currentPatientAboutToChange() {return true;}
};

}

#endif // IPATIENTLISTENER_H
