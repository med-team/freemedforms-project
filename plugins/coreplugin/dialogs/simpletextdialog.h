/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef SIMPLETEXTDIALOG_H
#define SIMPLETEXTDIALOG_H

#include <coreplugin/core_exporter.h>
#include <coreplugin/idocumentprinter.h>

#include <QDialog>
#include <QString>

/**
 * \file ./plugins/coreplugin/dialogs/simpletextdialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/


namespace Core {
namespace Internal {
namespace Ui {
class SimpleTextDialog;
}
}  // End Internal

class CORE_EXPORT SimpleTextDialog : public QDialog
{
    Q_OBJECT

public:
    SimpleTextDialog(const QString &title, const QString &zoomSettingKey = QString::null, QWidget *parent = 0);
    ~SimpleTextDialog();

    void setHtml(const QString &html);
    void setPlainText(const QString &text);
    void setHelpPageUrl(const QString &url) {m_HelpUrl = url;}
    void setUserPaper(const int iDocumentPaper) {m_Papers = iDocumentPaper;}
    void setPrintDuplicata(const bool state) {m_Duplicata = state;}

protected:
    void changeEvent(QEvent *e);

protected Q_SLOTS:
    void print();
    void showHelp();
    void zoomIn();
    void zoomOut();

private:
    Internal::Ui::SimpleTextDialog *ui;
    int m_Zoom;
    QString m_Key, m_HelpUrl;
    int m_Papers;
    bool m_Duplicata;
};

}  // End namespace Core

#endif // SIMPLETEXTDIALOG_H
