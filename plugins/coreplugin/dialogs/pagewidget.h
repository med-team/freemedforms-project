/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CORE_PAGEWIDGET_H
#define CORE_PAGEWIDGET_H

#include <coreplugin/core_exporter.h>

#include <QWidget>
#include <QList>
#include <QHash>
#include <QLabel>
#include <QToolButton>
#include <QModelIndex>

class QTreeWidgetItem;

/**
 * \file ./plugins/coreplugin/dialogs/pagewidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class IGenericPage;

namespace Internal {
namespace Ui{
    class PageWidget;
} // Ui
} // Internal

class CORE_EXPORT PageWidget : public QWidget
{
    Q_OBJECT
public:
    PageWidget(QWidget *parent);
    ~PageWidget();

    template <class T>
    void setPages(const QList<T*> &pages) {
        m_pages.clear();
        for(int i = 0; i < pages.count(); ++i) {
            IGenericPage *p = qobject_cast<IGenericPage*>(pages.at(i));
            if (p)
                m_pages << p;
        }
    }
    void setSettingKey(const QString &settingKey) {m_settingKey = settingKey;}
    void setStartingPage(const QString &initialCategory, const QString &initialPage) {m_currentCategory=initialCategory; m_currentPage=initialPage;}
    void setCategoryInBold(bool inBold) {m_categoryInBold=inBold;}

    void setupUi(bool sortCategoryView = true);

    void expandAllCategories();
    void expandFirstCategories();
    void setSplitterSizes(const QList<int> &sizes);

    IGenericPage *currentPage() const;
    void saveState();

    QList<QWidget *> pageWidgets() const;

    bool isViewExpanded() const;
    void setViewExpanded(bool expand);

private:
    QWidget *createPageWidget(IGenericPage *page);
    void changeEvent(QEvent *event);

private Q_SLOTS:
    void pageSelected();
    void expandView();
    void expandItem(const QModelIndex &index);
private:
    Internal::Ui::PageWidget *m_ui;
    QList<Core::IGenericPage*> m_pages;
    bool m_applied, m_categoryInBold;
    QString m_currentCategory;
    QString m_currentPage;
    QString m_settingKey;
    QList<QWidget *> m_AddedWidgets;
    QVector<QToolButton *> m_Buttons;
    QHash<Core::IGenericPage*, QLabel *> m_Labels;
    QHash<Core::IGenericPage*, QTreeWidgetItem *> m_Items, m_Categories;
};

} // namespace Core

#endif // CORE_PAGEWIDGET_H
