/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#ifndef PLUGINDIALOG_H
#define PLUGINDIALOG_H

#include <coreplugin/core_exporter.h>

#include <QDialog>

QT_BEGIN_NAMESPACE
class QPushButton;
QT_END_NAMESPACE

/**
 * \file ./plugins/coreplugin/dialogs/plugindialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/


namespace ExtensionSystem {
class PluginSpec;
class PluginView;
}

namespace Core {

class CORE_EXPORT PluginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PluginDialog(QWidget *parent);

private Q_SLOTS:
    void updateButtons();
    void openDetails();
    void openDetails(ExtensionSystem::PluginSpec *spec);
    void openErrorDetails();

private:
    ExtensionSystem::PluginView *m_view;

    QPushButton *m_detailsButton;
    QPushButton *m_errorDetailsButton;
    QPushButton *m_closeButton;
};

} // namespace Core

#endif // PLUGINDIALOG_H
