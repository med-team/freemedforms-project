/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef APPLICATIONGENERALPREFERENCES_H
#define APPLICATIONGENERALPREFERENCES_H

#include <coreplugin/core_exporter.h>
#include <coreplugin/ioptionspage.h>

#include <QWidget>

#include <QPointer>
#include <QObject>

/**
 * \file ./plugins/coreplugin/dialogs/applicationgeneralpreferences.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;

namespace Internal {
namespace Ui {
class ApplicationGeneralPreferencesWidget;
}

class ApplicationGeneralPreferencesWidget : public QWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(ApplicationGeneralPreferencesWidget)

public:
    explicit ApplicationGeneralPreferencesWidget(QWidget *parent = 0);

    static void writeDefaultSettings(Core::ISettings *s);
    void setDataToUi();

public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);

protected:
    virtual void changeEvent(QEvent *e);

private:
    Ui::ApplicationGeneralPreferencesWidget *ui;
};

}  // End namespace Internal



class CORE_EXPORT ApplicationGeneralPreferencesPage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    ApplicationGeneralPreferencesPage(QObject *parent = 0);
    ~ApplicationGeneralPreferencesPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    QString helpPage() {return "parametrer.html";}

    static void writeDefaultSettings(Core::ISettings *s) {Internal::ApplicationGeneralPreferencesWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);
private:
    QPointer<Internal::ApplicationGeneralPreferencesWidget> m_Widget;
};



}  // End namespace Core


#endif // APPLICATIONGENERALPREFERENCES_H
