/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef HELPDIALOG_H
#define HELPDIALOG_H

#include <coreplugin/core_exporter.h>

#include <QDialog>
QT_BEGIN_NAMESPACE
class QTreeWidgetItem;
class QUrl;
QT_END_NAMESPACE

/**
 * \file ./plugins/coreplugin/dialogs/helpdialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
namespace Internal {
class HelpDialogPrivate;
}

class CORE_EXPORT HelpDialog : public QDialog
{
    Q_OBJECT
public:
    static void showPage(const QString &page);
    static void showIndex();

protected:
    explicit HelpDialog( const QString &page = QString::null, QWidget *parent = 0 );
    void changeEvent(QEvent *event);

private Q_SLOTS:
    void updateWindowTitle();
    void fullScreen();
    void treeActivated(QTreeWidgetItem *item);
//    void changePage(const QUrl &url);

private:
    Internal::HelpDialogPrivate *d;
};

}  // End Core

#endif // HELPDIALOG_H
