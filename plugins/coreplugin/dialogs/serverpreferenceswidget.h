/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef COREPLUGIN_SERVERPREFERENCESWIDGET_H
#define COREPLUGIN_SERVERPREFERENCESWIDGET_H

#include <coreplugin/core_exporter.h>
#include <utils/database.h>

#include <QWidget>

namespace Core {
class ISettings;

namespace Internal {
class ServerPreferencesWidgetPrivate;

namespace Ui {
class ServerPreferencesWidget;
}  // namespace Ui
}  // namespace Internal

class CORE_EXPORT ServerPreferencesWidget : public QWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(ServerPreferencesWidget)
    friend class Core::Internal::ServerPreferencesWidgetPrivate;

public:
    explicit ServerPreferencesWidget(QWidget *parent = 0);
    ~ServerPreferencesWidget();

    bool connectionSucceeded() const;
    void setUserLoginGroupTitle(const QString &trContext, const QString &untranslatedtext);

    void showUseDefaultAdminLogCheckbox(bool show);

    QString hostName() const;
    int port() const;
    QString login() const;
    QString password() const;
    Utils::Database::Grants grantsOnLastConnectedDatabase() const;

    static void writeDefaultSettings(Core::ISettings *s);

Q_SIGNALS:
    void hostConnectionChanged(bool coonected);
    void userConnectionChanged(bool coonected);

protected Q_SLOTS:
    void checkHostNameLength(const QString &hostName);
    void testHost();
    void testHost(const QString &hostName);
    void saveToSettings(Core::ISettings *s = 0);

private Q_SLOTS:
    void on_testMySQLButton_clicked();
    void toggleLogPass(bool state);

protected:
    virtual void changeEvent(QEvent *e);

private:
    Internal::ServerPreferencesWidgetPrivate *d;
};

}  // End namespace Core

#endif // COREPLUGIN_SERVERPREFERENCESWIDGET_H
