/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CORE_INTERNAL_NETWORKPREFERENCES_H
#define CORE_INTERNAL_NETWORKPREFERENCES_H

#include <QWidget>

#include <coreplugin/core_exporter.h>
#include <coreplugin/ioptionspage.h>

#include <QWidget>

#include <QPointer>
#include <QObject>

/**
 * \file ./plugins/coreplugin/dialogs/networkpreferences.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;

namespace Internal {
namespace Ui {
class ProxyPreferencesWidget;
}

class ProxyPreferencesWidget : public QWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(ProxyPreferencesWidget)

public:
    explicit ProxyPreferencesWidget(QWidget *parent = 0);

    static void writeDefaultSettings(Core::ISettings *s);
    void setDataToUi();

public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);
    void autoDetectProxy();

protected:
    virtual void changeEvent(QEvent *e);

private:
    Ui::ProxyPreferencesWidget *ui;
};

class CORE_EXPORT ProxyPreferencesPage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    ProxyPreferencesPage(QObject *parent = 0);
    ~ProxyPreferencesPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    QString helpPage() {return "parametrer.html";}

    static void writeDefaultSettings(Core::ISettings *s) {Internal::ProxyPreferencesWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);
private:
    QPointer<Internal::ProxyPreferencesWidget> m_Widget;
};


} // namespace Internal
} // namespace Core


#endif // CORE_INTERNAL_NETWORKPREFERENCES_H
