/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "helpdialog.h"

#include <coreplugin/icore.h>
#include <coreplugin/imainwindow.h>
#include <coreplugin/itheme.h>
#include <coreplugin/isettings.h>
#include <coreplugin/constants_icons.h>

#include <translationutils/constanttranslations.h>
#include <utils/global.h>
#include <utils/widgets/minisplitter.h>

#include <QApplication>
#include <QTextBrowser>
#include <QAction>
#include <QToolBar>
#include <QComboBox>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QGridLayout>
#include <QHash>
#include <QString>
#include <QDir>
#include <QHeaderView>
#include <QSplitter>
#include <QLabel>
#include <QVBoxLayout>
#include <QDialogButtonBox>

#include <QDebug>

using namespace Core;
using namespace Core::Internal;
using namespace Trans::ConstantTranslations;

static inline Core::ISettings *settings() {return Core::ICore::instance()->settings();}

namespace Core {
namespace Internal {
const char * const INDEX_FILENAME = "toc.html";

class HelpDialogPrivate
{
public:
    HelpDialogPrivate(QDialog *dlg) :
            m_Browser(0), m_TocBrowser(0), q(dlg)
    {
        Core::ITheme *theme = Core::ICore::instance()->theme();
        m_Browser = new QTextBrowser(dlg);
        m_Browser->setOpenExternalLinks(true);
        m_Tree = new QTreeWidget(dlg);
        m_ToolBar = new QToolBar(tkTr(Trans::Constants::NAVIGATION), dlg);
#if QT_VERSION < 0x040600
        m_ToolBar->setIconSize(QSize(16,16));
#endif
        aNext = new QAction(dlg);
        aPrevious = new QAction(dlg);
        aHome = new QAction(dlg);
        aFullScreen = new QAction(dlg);
        aZoomIn = new QAction(dlg);
        aZoomOut = new QAction(dlg);
        aClose = new QAction(dlg);
        aNext->setIcon(theme->icon(Constants::ICONNEXT));
        aPrevious->setIcon(theme->icon(Constants::ICONPREVIOUS));
        aHome->setIcon(theme->icon(Constants::ICONHOME));
        aFullScreen->setIcon(theme->icon(Constants::ICONFULLSCREEN));
        aZoomIn->setIcon( Core::ICore::instance()->theme()->icon(Core::Constants::ICONFONTBIGGER) );
        aZoomOut->setIcon( Core::ICore::instance()->theme()->icon(Core::Constants::ICONFONTSMALLER) );
        aClose->setIcon(theme->icon(Constants::ICONEXIT));
        m_ToolBar->addAction(aHome);
        m_ToolBar->addSeparator();
        m_ToolBar->addAction(aPrevious);
        m_ToolBar->addAction(aNext);
        m_ToolBar->addSeparator();
        m_ToolBar->addAction(aFullScreen);
        m_ToolBar->addAction(aZoomIn);
        m_ToolBar->addAction(aZoomOut);
        m_ToolBar->addSeparator();
        m_ToolBar->addAction(aClose);
    }
    ~HelpDialogPrivate() {}

    void retranslate()
    {
        aNext->setToolTip(tkTr(Trans::Constants::NEXT_TEXT));
        aPrevious->setToolTip(tkTr(Trans::Constants::PREVIOUS_TEXT));
        aHome->setToolTip(tkTr(Trans::Constants::HOME_TEXT));
        aClose->setToolTip(tkTr(Trans::Constants::CLOSE_TEXT));
        aFullScreen->setToolTip(tkTr(Trans::Constants::FULLSCREEN_TEXT));
        aZoomIn->setToolTip(tkTr(Trans::Constants::ZOOMIN_TEXT));
        aZoomOut->setToolTip(tkTr(Trans::Constants::ZOOMOUT_TEXT));
    }

    void createConnections()
    {
        Q_ASSERT(q);
        q->connect(aHome, SIGNAL(triggered()), m_Browser, SLOT(home()));
        q->connect(aNext, SIGNAL(triggered()), m_Browser, SLOT(forward()));
        q->connect(aPrevious, SIGNAL(triggered()), m_Browser, SLOT(backward()));
        q->connect(aHome, SIGNAL(triggered()), m_Browser, SLOT(home()));
        q->connect(aClose, SIGNAL(triggered()), q, SLOT(close()));
        q->connect(m_Browser, SIGNAL(sourceChanged(QUrl)), q, SLOT(updateWindowTitle()));
        q->connect(aFullScreen, SIGNAL(triggered()), q, SLOT(fullScreen()));
        q->connect(aZoomIn, SIGNAL(triggered()), m_Browser, SLOT(zoomIn()));
        q->connect(aZoomOut, SIGNAL(triggered()), m_Browser, SLOT(zoomOut()));
        q->connect(aFullScreen, SIGNAL(triggered()), q, SLOT(fullScreen()));
        q->connect(m_Tree, SIGNAL(itemClicked(QTreeWidgetItem*,int)),q, SLOT(treeActivated(QTreeWidgetItem*)));
    }

    void populateTree()
    {
        QDir dir(settings()->path(ISettings::DocumentationPath));
        QStringList files = dir.entryList(QStringList() << "*.htm" << "*.html", QDir::Files | QDir::Readable);
        m_Tree->clear();
        foreach(const QString &f, files) {
            QString tmp = Utils::readTextFile(dir.absolutePath() + QDir::separator() + f);
            if (tmp.contains("<title>")) {
                int begin = tmp.indexOf("<title>") + 7;
                int end = tmp.indexOf("</title");
                QString title = tmp.mid(begin, end-begin);
                title.replace("&#39;","'");
                m_Title_Page.insert(title, f);
                new QTreeWidgetItem(m_Tree, QStringList() << title);
            }
        }
    }

    QTextBrowser *m_Browser;
    QTextBrowser *m_TocBrowser;
    QTreeWidget *m_Tree;
    QDialog *q;
    QHash<QString,QString> m_Title_Page;
    QToolBar *m_ToolBar;
    QAction *aNext, *aPrevious, *aHome, *aClose, *aFullScreen, *aZoomIn, *aZoomOut;
};

}  // End Internal
}  // End Core


HelpDialog::HelpDialog(const QString &page, QWidget *parent) :
    QDialog(parent),
    d(0)
{
    Q_UNUSED(page);
    setObjectName("HelpDialog");
    setAttribute(Qt::WA_DeleteOnClose);

    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);
    QLabel *lbl = new QLabel(tr("<center>Documentation is available on line only.</center>"), this);
    QLabel *lblLink = new QLabel(QString("<center><a href='%1'>%1</a></center>").arg(settings()->path(Core::ISettings::WebSiteUrl)));
    QDialogButtonBox *but = new QDialogButtonBox(QDialogButtonBox::Ok, Qt::Horizontal, this);
    connect(but, SIGNAL(accepted()), this, SLOT(accept()));
    layout->addWidget(lbl);
    layout->addWidget(lblLink);
    layout->addWidget(but);
    setWindowTitle(tkTr(Trans::Constants::HELP_TEXT));







}

void HelpDialog::showPage(const QString &page)
{
    HelpDialog *hb = new HelpDialog(page, qApp->activeWindow());
    hb->show();
}

void HelpDialog::showIndex()
{
    showPage(QString(::INDEX_FILENAME));
}

void HelpDialog::updateWindowTitle()
{
    setWindowTitle( tkTr(Trans::Constants::HELP_TEXT) + ": " + d->m_Browser->documentTitle());
    QList<QTreeWidgetItem *> list = d->m_Tree->findItems(d->m_Browser->documentTitle(),Qt::MatchExactly,0);
    if (list.count())
        d->m_Tree->setCurrentItem(list.at(0));
}

void HelpDialog::fullScreen()
{
    return;
    Utils::setFullScreen(this, !this->isFullScreen());
}

void HelpDialog::treeActivated(QTreeWidgetItem *item)
{
    d->m_Browser->setSource(d->m_Title_Page.value(item->text(0)));
}


void HelpDialog::changeEvent(QEvent *event)
{
    if (event->type()==QEvent::LanguageChange) {
        d->retranslate();
    }
}
