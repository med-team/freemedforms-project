/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef COREDEBUGDIALOG_H
#define COREDEBUGDIALOG_H

#include <QDialog>
#include <QHash>
#include <QMessageBox>
#include <utils/messagesender.h>

/**
 * \file ./plugins/coreplugin/dialogs/debugdialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

QT_BEGIN_NAMESPACE
class QTreeWidgetItem;
class QStackedLayout;
QT_END_NAMESPACE

namespace Core {
class IDebugPage;
}

namespace Core {
namespace Internal {

namespace Ui {
    class DebugDialog;
}

class DebugDialog : public QDialog
{
    Q_OBJECT
    Q_DISABLE_COPY(DebugDialog)

public:
    explicit DebugDialog(QWidget * parent);
    ~DebugDialog();

private Q_SLOTS:
    bool saveLogToFile();

private:
    Ui::DebugDialog *m_ui;
    QStackedLayout  *m_slayout;
    QHash<QTreeWidgetItem *, QWidget *> m_Widgets;
    Utils::MessageSender  m_sender;
    bool            m_sending;
    QMessageBox *   m_infoMessageBox;
    bool            m_MessageViewExpanded, m_ErrorViewExpanded;
};

} // End Internal
} // End Core

#endif // COREDEBUGDIALOG_H
