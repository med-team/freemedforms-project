/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef COREABOUTDIALOG_H
#define COREABOUTDIALOG_H

#include <QDialog>
#include <QHash>

/**
 * \file ./plugins/coreplugin/dialogs/aboutdialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

QT_BEGIN_NAMESPACE
class QTreeWidgetItem;
class QWidget;
class QStackedLayout;
QT_END_NAMESPACE


namespace Core {
    class IAboutPage;
namespace Internal {
  namespace Ui {
      class AboutDialog;
  }
}
}

namespace Core {
namespace Internal {

class AboutDialog : public QDialog
{
    Q_OBJECT
    Q_DISABLE_COPY(AboutDialog)

public:
    explicit AboutDialog(QWidget *parent = 0);
    virtual ~AboutDialog();

private:
    Ui::AboutDialog *m_ui;
};

} // End Internal
} // End Core

#endif // COREABOUTDIALOG_H
