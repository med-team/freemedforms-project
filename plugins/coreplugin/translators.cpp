/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/


#include "translators.h"

#include <translationutils/constanttranslations.h>
#include <utils/log.h>
#include <utils/global.h>

#include <QTranslator>
#include <QFileInfo>
#include <QDir>
#include <QLocale>
#include <QApplication>
#include <QLibraryInfo>

using namespace Core;

enum { WarnTranslatorsErrors = false };

QString Translators::m_PathToTranslations = "";
Translators * Translators::m_Instance = 0;

Translators *Translators::instance(QObject *parent)
{
    if (!m_Instance) {
        if (parent)
            m_Instance = new Translators(qApp);
        else
            m_Instance = new Translators(parent);
    }
    return m_Instance;
}

Translators::Translators(QObject * parent)
          : QObject(parent)
{
    setObjectName("Translators");
    m_Translators.clear();
    m_Instance = this;
}

Translators::~Translators()
{
}


bool Translators::setPathToTranslations(const QString & path)
{
    if (QDir(path).exists()) {
        m_PathToTranslations = QDir::cleanPath(path);
        LOG_FOR("Translators", Trans::ConstantTranslations::tkTr(Trans::Constants::SETTING_1_PATH_TO_2)
                .arg(Trans::ConstantTranslations::tkTr(Trans::Constants::TRANSLATORS_TEXT),
                     QDir::cleanPath(path)));
        return true;
    }
    if (WarnTranslatorsErrors) {
        LOG_ERROR_FOR("Translators", Trans::ConstantTranslations::tkTr(Trans::Constants::PATH_1_DOESNOT_EXISTS)
                      .arg(QDir::cleanPath(path)));
    }
    return false;
}

QString Translators::pathToTranslations()
{
    return m_PathToTranslations;
}


void Translators::changeLanguage(const QString &lang)
{
    if (WarnTranslatorsErrors)
        qWarning() << "Translators::changeLanguage" << lang;
    QString l = lang.left(2);
    QLocale::setDefault(l);

        foreach(const QString & fileMask, m_Translators.keys()) {
            QFileInfo f(fileMask);
            QString path = "";
            if (fileMask.contains(QDir::separator()))
                path = f.absolutePath();
            else
                path = m_PathToTranslations;

            if (!m_Translators[fileMask]->load(f.fileName() + "_" + lang, path)) {
                if (WarnTranslatorsErrors)
                    LOG_ERROR(tr("Can not load %1, path: %2").arg(f.fileName() + "_" + lang , path));
            } else {
                if (WarnTranslatorsErrors) {
                    LOG(Trans::ConstantTranslations::tkTr(Trans::Constants::FILE_1_LOADED).arg(f.fileName() + "_" + lang));
                }
            }
        }

    Q_EMIT languageChanged();
}

void Translators::changeLanguage(QLocale::Language lang)
{
    if (WarnTranslatorsErrors)
        qWarning() << "Translators::changeLanguage" << lang;
    changeLanguage(QLocale(lang).name().left(2));
}

/*!
 * \brief Add a translator to the known application translators. It can be for the main application or
 * for plugins.
 * This member will automatically add the translator located at fileMask into the QApplication translators.
 * \param fileMask: full path to qm file. Like this: "/path/to/qm/file" without "_en.qm" for example.
 * \return true if all gone ok, false in the other case
 */
bool Translators::addNewTranslator(const QString & fileMask, bool fromDefaultPath)
{
    QTranslator *t = new QTranslator(qApp);
    QString lang = QLocale().name().left(2).toLower();
    QString path;
    if (fileMask.compare("qt", Qt::CaseInsensitive) == 0) {
        if (Utils::isLinuxIntegratedCompilation() || Utils::isRunningOnLinux() || Utils::isRunningOnFreebsd())
            path = QLibraryInfo::location(QLibraryInfo::TranslationsPath);
    }
    QFileInfo file(fileMask);

    if (path.isEmpty()) {
        if (fromDefaultPath)
            path = m_PathToTranslations;
        else
            path = file.absolutePath();
    }

    if (t->load(file.fileName() + "_" + lang, path)) {
        if (!m_Translators.contains(QDir::cleanPath(fileMask))) {
            m_Translators.insert(QDir::cleanPath(fileMask) , t);
            qApp->installTranslator(t);
            if (WarnTranslatorsErrors) {
                LOG(tr("Add Translator %1.").arg(file.fileName() + "_" + lang));
            }
        }
        return true;
    }

    if (WarnTranslatorsErrors) {
        LOG_ERROR(tr("WARNING: %1 can not be loaded or is already loaded.").arg(file.absoluteFilePath() + "_" + lang));
    }
    delete t;
    return false;
}

bool Translators::addNewTranslator(const QString & path, const QString & fileTemplate)
{
    return addNewTranslator(path + QDir::separator() + fileTemplate, false);
}

QStringList Translators::availableLocales()
{
    return availableLocalesAndLanguages().keys();
}

QStringList Translators::availableLanguages()
{
    return availableLocalesAndLanguages().values();
}

QMap<QString, QString> Translators::availableLocalesAndLanguages()
{
    static QMap<QString, QString> toReturn;
    if (!toReturn.isEmpty())
        return toReturn;

    toReturn.insert("en", "English");

    if (m_PathToTranslations.isEmpty())
        return toReturn;

    QDir dir(m_PathToTranslations);
    QStringList fileNames = dir.entryList(QStringList() << QString("%1_*.qm").arg(Trans::Constants::CONSTANTS_TRANSLATOR_NAME));
    foreach(QString s, fileNames) {
        QString locale = s;
        locale.remove(0, locale.lastIndexOf('_') + 1);
        locale.truncate(locale.lastIndexOf('.'));
        QTranslator translator;
        translator.load(s, m_PathToTranslations);
        QString lang = translator.translate(Trans::Constants::CONSTANTS_TR_CONTEXT, Trans::Constants::ENGLISH);
        toReturn.insert(locale, lang);
    }
    return toReturn;
}
