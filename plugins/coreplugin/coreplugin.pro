#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

# first definitions
TEMPLATE        = lib
TARGET          = Core
PACKAGE_VERSION = 0.0.9

# include(interfaces/interfaces.pri)
include(common_core.pri)

# include(private/private.pri)
HEADERS += \
    coreplugin.h \
    coreimpl.h \
#    maininterface/mainwindow.h \
    mfObject.h \
    mfObjectFundamental.h \
    iformio.h

SOURCES += \
    coreplugin.cpp \
    coreimpl.cpp \
#    maininterface/mainwindow.cpp \

