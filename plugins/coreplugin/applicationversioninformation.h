/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CORE_APPLICATIONVERSIONINFORMATION_H
#define CORE_APPLICATIONVERSIONINFORMATION_H

#include <coreplugin/core_exporter.h>
#include <QObject>

/**
 * \file ./plugins/coreplugin/applicationversioninformation.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
namespace Internal {
class ApplicationVersionInformationPrivate;
} // namespace Internal

class CORE_EXPORT ApplicationVersionInformation
{    
public:
//    enum License {
//        LGPLv2 = 0,
//        LGPLv2_1,
//        GPLv2,
//        GPLv3,
//        BSD,
//        BSD3Clause,
//        Unknown
//    };

    ApplicationVersionInformation();
    ApplicationVersionInformation(const ApplicationVersionInformation &copy);

    ~ApplicationVersionInformation();

    // Version update
    void setVersionUpdateDetected(bool updateDetected);
    bool isVersionUpdateDetected() const;

    void setPreviousVersion(const QString &version);
    void setCurrentVersion(const QString &version);
    const QString &previousVersion() const;
    const QString &currentVersion() const;

    void setUpdatePreferencesAccepted(bool updateAccepted);
    bool isUpdatePreferencesAccepted() const;

    void setUpdateFormsAccepted(bool updateAccepted);
    bool isUpdateFormsAccepted() const;

    void setUpdateTokenedDocumentsAccepted(bool updateAccepted);
    bool isUpdateTokenedDocumentsAccepted() const;

private:
    Internal::ApplicationVersionInformationPrivate *d;
};

} // namespace Core

#endif  // CORE_APPLICATIONVERSIONINFORMATION_H

