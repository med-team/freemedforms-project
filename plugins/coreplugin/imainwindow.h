/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IMAINWINDOW_H
#define IMAINWINDOW_H

#include <coreplugin/actionmanager/mainwindowactionhandler.h>
#include <coreplugin/core_exporter.h>


#include <QObject>
#include <QIcon>
#include <QPointer>

/**
 * \file ./plugins/coreplugin/imainwindow.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {

class CORE_EXPORT IMainWindow : public Internal::MainWindowActionHandler
{
    Q_OBJECT
public:
    IMainWindow(QWidget *parent = 0) : MainWindowActionHandler(parent) {}
    virtual ~IMainWindow() {}

    virtual bool initialize(const QStringList &arguments, QString *errorString) = 0;
    virtual void extensionsInitialized() = 0;

//    virtual void postCoreInitialization() = 0;

    void startProcessingSpinner();
    void endProcessingSpinner();
};

} // end Core

#endif // IFORMIO_H
