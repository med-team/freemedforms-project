/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CORE_CONTEXTMANAGER_H
#define CORE_CONTEXTMANAGER_H

// Adapted from QtCreator 2.5.2 coreplugin/mainwindow

#include <coreplugin/core_exporter.h>
#include <coreplugin/contextmanager/icontext.h>

#include <QObject>

/**
 * \file ./plugins/coreplugin/contextmanager/contextmanager.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {

class CORE_EXPORT ContextManager : public QObject
{
    Q_OBJECT
public:
    ContextManager(QObject *parent = 0) : QObject(parent) {}
    virtual ~ContextManager() {}

    virtual IContext *contextObject(QWidget *widget) = 0;
    virtual void addContextObject(IContext *context) = 0;
    virtual void updateContextObject(IContext *context) = 0;
    virtual void removeContextObject(IContext *context) = 0;
    virtual void resetContext() = 0;

    virtual IContext *currentContextObject() const = 0;
    virtual void updateAdditionalContexts(const Context &remove, const Context &add) = 0;
    virtual void addAdditionalContext(int context) = 0;
    virtual void removeAdditionalContext(int context) = 0;
    virtual bool hasContext(int context) const = 0;

    virtual void updateContext() = 0;

Q_SIGNALS:
    void contextAboutToChange(Core::IContext *context);
    void contextChanged(Core::IContext *context, const Core::Context &additionalContexts);
};

} // End Core

#endif // CORE_CONTEXTMANAGER_H
