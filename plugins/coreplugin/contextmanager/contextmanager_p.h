/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CORE_CONTEXTMANAGERPRIVATE_H
#define CORE_CONTEXTMANAGERPRIVATE_H
#include "contextmanager.h"
#include <coreplugin/core_exporter.h>

#include <QObject>
#include <QWidget>
#include <QMainWindow>
#include <QMap>

#include <coreplugin/contextmanager/icontext.h>

/**
 * \file ./plugins/coreplugin/contextmanager/contextmanager_p.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
namespace Internal {

class ContextManagerPrivate : public ContextManager
{
    Q_OBJECT
public:
    ContextManagerPrivate(QMainWindow *mainWin);
    ContextManagerPrivate() {}

    IContext *contextObject(QWidget *widget);
    void addContextObject(IContext *context);
    void removeContextObject(IContext *context);
    void resetContext();

    IContext *currentContextObject() const;
    void addAdditionalContext(int context);
    void updateAdditionalContexts(const Context &remove, const Context &add);
    void removeAdditionalContext(int context);
    bool hasContext(int context) const;

    void updateContext();

private Q_SLOTS:
    void updateFocusWidget(QWidget *old, QWidget *now);

private:
    void updateContextObject(IContext *context);

private:
    Context m_globalContext;
    Context m_additionalContexts;
    IContext *m_activeContext;
    QMap<QWidget *, IContext *> m_contextWidgets;
    QMainWindow *m_mainWindow;
};


} // End Internal
} // End Core

#endif // CORE_CONTEXTMANAGERPRIVATE_H
