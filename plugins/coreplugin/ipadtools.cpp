/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 ***************************************************************************/
#include "ipadtools.h"









#include <coreplugin/icore.h>

#include <translationutils/constants.h>
#include <translationutils/trans_current.h>

#include <QApplication>

using namespace Core;
using namespace Trans::ConstantTranslations;

static inline Core::ITokenPool *tokenPool() {return Core::ICore::instance()->padTools()->tokenPool();}

TokenDescription::TokenDescription(const QString &uid) :
    _uid(uid),
    _trContext(Trans::Constants::CONSTANTS_TR_CONTEXT)
{
}

QString TokenDescription::humanReadableName() const
{
    if (!_trContext.isEmpty())
        return QApplication::translate(_trContext.toUtf8(), _human.toUtf8());
    return QApplication::translate(Trans::Constants::CONSTANTS_TR_CONTEXT, _human.toUtf8());;
}

static QString fullNamespace(Core::TokenNamespace &ns)
{
    QStringList r;
    Core::TokenNamespace *current = const_cast<Core::TokenNamespace *>(&ns);
    while (current->children().count() > 0) {
        r << current->humanReadableName();
        current = const_cast<Core::TokenNamespace *>(&current->children().at(0));
    }
    if (current)
        r << current->humanReadableName();
    return r.join(".");
}


QString IToken::tooltip() const
{
    Core::TokenNamespace ns = tokenPool()->getTokenNamespace(uid());
    QString name;
    humanReadableName().isEmpty() ? name = uid() : name = humanReadableName();
    QString tooltip;
    tooltip = QString("<b>%1<br /><b>%2<br /><b>%3")
            .arg(tkTr(Trans::Constants::TOKEN_NAMESPACE_1).arg(fullNamespace(ns).replace(" ", "&nbsp;")))
            .arg(tkTr(Trans::Constants::TOKEN_1).arg(name))
            .arg(tkTr(Trans::Constants::VALUE_1).arg(value().toString()))
            .replace(":", ":</b>");
    if (!TokenDescription::tooltip().isEmpty())
        tooltip += "<br />" + TokenDescription::tooltip().replace("\n", "<br />");
    return tooltip;
}

QString TokenDescription::tooltip() const
{
    if (!_trContext.isEmpty())
        return QApplication::translate(_trContext.toUtf8(), _tooltip.toUtf8());
    return QApplication::translate(Trans::Constants::CONSTANTS_TR_CONTEXT, _tooltip.toUtf8());;
}

QString TokenDescription::helpText() const
{
    if (!_trContext.isEmpty())
        return QApplication::translate(_trContext.toUtf8(), _help.toUtf8());
    return QApplication::translate(Trans::Constants::CONSTANTS_TR_CONTEXT, _help.toUtf8());;
}

QString TokenDescription::shortHtmlDescription() const
{
    if (!_trContext.isEmpty())
        return QApplication::translate(_trContext.toUtf8(), _descr.toUtf8());
    return QApplication::translate(Trans::Constants::CONSTANTS_TR_CONTEXT, _descr.toUtf8());;
}

bool TokenDescription::operator<(const TokenDescription &descr) const
{
    return _uid < descr._uid;
}

void TokenNamespace::addChild(const Core::TokenNamespace &child)
{
    _children.append(child);
}

QList<Core::TokenNamespace> TokenNamespace::children() const
{
    return _children;
}

void TokenNamespace::clearChildren()
{
    _children.clear();
}




























