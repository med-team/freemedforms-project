#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

# SOURCES and HEADERS needed by action manager
SOURCES += $${PWD}/command.cpp \
#           $${PWD}/commandsfile.cpp \
           $${PWD}/actionmanager.cpp \
           $${PWD}/actioncontainer.cpp \
           $${PWD}/mainwindowactionhandler.cpp

HEADERS += $${PWD}/command_p.h \
#           $${PWD}/commandsfile.h \
           $${PWD}/command.h \
           $${PWD}/actionmanager_p.h \
           $${PWD}/actionmanager.h \
           $${PWD}/actioncontainer_p.h \
           $${PWD}/actioncontainer.h \
           $${PWD}/mainwindowactionhandler.h \
           $${PWD}/mainwindowactions.h
