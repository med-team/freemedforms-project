/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IFIRSTCONFIGURATIONPAGE_H
#define IFIRSTCONFIGURATIONPAGE_H

#include <coreplugin/core_exporter.h>

#include <QObject>
#include <QString>
#include <QWizardPage>

/**
 * \file ./plugins/coreplugin/ifirstconfigurationpage.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
 * \class Core::IFirstConfigurationPage
 * \brief Derive object from this interface and set it inside the PluginManager object pool to get your page in the application configurator.
 * You should create the pages in the constructor of your plugin. They should be used before the ool initialize(const QStringList &arguments, QString *errorMessage = 0);
*/

namespace Core {

class CORE_EXPORT IFirstConfigurationPage : public QObject
{
    Q_OBJECT
public:
    enum Pages {
        FirstPage = 0,
        ProxyConfig,
        ServerClientConfig,
        ServerConfig,
        UserDbConnection,
        DatabaseCreationPage,
        UserCreation,
        PatientForm,
        VirtualPatients,
        OtherPage,
        LastPage = 100000
    };
    IFirstConfigurationPage(QObject *parent = 0) : QObject(parent) {}
    virtual ~IFirstConfigurationPage() {}

    virtual int id() const = 0;

    // widget will be deleted after the show
    virtual QWizardPage *createPage(QWidget *parent) = 0;
};

} // namespace Core

#endif // IFIRSTCONFIGURATIONPAGE_H
