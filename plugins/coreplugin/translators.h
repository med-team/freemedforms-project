/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CORETRANSLATORS_H
#define CORETRANSLATORS_H

#include <coreplugin/core_exporter.h>
#include <coreplugin/icore.h>

#include <QObject>
#include <QMap>
#include <QLocale>

class QTranslator;

/**
 * \file ./plugins/coreplugin/translators.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {

class CORE_EXPORT Translators : public QObject
{
    Q_OBJECT
public:
    static Translators* instance( QObject * parent = 0) ;
    Translators(QObject * parent = 0);
    ~Translators();

    bool addNewTranslator( const QString & fileName, bool fromDefaultPath = true );
    bool addNewTranslator( const QString & path, const QString & file );

public Q_SLOTS:
    void changeLanguage(const QString &lang);
    void changeLanguage(QLocale::Language lang);

public:
    static bool setPathToTranslations( const QString & path );
    static QString pathToTranslations();

    static QStringList availableLocales();
    static QStringList availableLanguages();
    static QMap<QString, QString> availableLocalesAndLanguages();

Q_SIGNALS:
    void languageChanged();


private:
    QMap<QString, QTranslator*>   m_Translators;  // String is file location and mask
    static QString                m_PathToTranslations;
    static Translators           *m_Instance;
};

} // end Core

#endif  // CORETRANSLATORS_H
