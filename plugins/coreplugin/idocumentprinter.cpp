/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "idocumentprinter.h"



/** \fn virtual void Core::IDocumentPrinter::clearTokens() = 0;
  Clear all registered tokens.
 */

/** \fn virtual void Core::IDocumentPrinter::addTokens(const int tokenWhere, const QHash<QString, QVariant> &tokensAndValues) = 0;
  Register tokens \e tokensAndValues for a specific document, or a global usage with \e tokenWhere.
  Use the Core::IDocumentPrinter::TokensWhere enumerator to define the \e tokenWhere value.
  The tokens are represented in a hash of QString (key), QVariant (value).
  For more information about the token management and usage please refer to the web site:
  - French: http://www.freemedforms.com/fr/manuals/freemedforms/tokenmanager
  - English: http://www.freemedforms.com/en/manuals/freemedforms/tokenmanager
 */

/** \fn virtual bool Core::IDocumentPrinter::print(const QTextDocument &text, const int papers = Papers_Generic_User, bool printDuplicata = false) const = 0;
  Prints a QTextDocument \e text using a set of user papers (header, footer, watermark) \e papers. Use the enumerator Core::IDocumentPrinter::PapersToUse to define the /e papers.
  You can automatically print a duplicate.
 */

/** \fn virtual bool Core::IDocumentPrinter::print(QTextDocument *text, const int papers = Papers_Generic_User, bool printDuplicata = false) const = 0;
  Prints a QTextDocument \e text using a set of user papers (header, footer, watermark) \e papers. Use the enumerator Core::IDocumentPrinter::PapersToUse to define the /e papers.
  You can automatically print a duplicate.
 */

/** \fn virtual bool Core::IDocumentPrinter::print(const QString &html, const int papers = Papers_Generic_User, bool printDuplicata = false) const = 0;
  Prints an HTML \e text using a set of user papers (header, footer, watermark) \e papers. Use the enumerator Core::IDocumentPrinter::PapersToUse to define the /e papers.
  You can automatically print a duplicate.
 */

/** \fn virtual bool Core::IDocumentPrinter::printPreview(const QString &html, const int papers = Papers_Generic_User, bool printDuplicata = false) const = 0;
  Show a print preview. \sa print()
 */

/** \fn static void Core::IDocumentPrinter::addPrintedDoc(const QString &fileName, const QString &docName, const QDateTime &dt, const QString &userUid);
  You can use a PDF cache system. This cache system is automatically managed by the print process.
 */

/** \fn static Core::IDocumentPrinter::QList<PrintedDocumentTracer> printedDocs()
  You can use a PDF cache system. This cache system is automatically managed by the print process.
 */


using namespace Core;

QList<PrintedDocumentTracer> Core::IDocumentPrinter::m_Docs;

void IDocumentPrinter::addPrintedDoc(const QString &fileName, const QString &docName, const QDateTime &dt, const QString &userUid)
{
    PrintedDocumentTracer d;
    d.m_docName = docName;
    d.m_fileName = fileName;
    d.m_userUid = userUid;
    d.m_dateTime = dt;
    m_Docs.append(d);
}
