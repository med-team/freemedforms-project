/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#ifndef IOPTIONSPAGE_H
#define IOPTIONSPAGE_H

#include <coreplugin/core_exporter.h>
#include <coreplugin/igenericpage.h>

#include <QObject>
#include <QString>
#include <QWidget>

/**
 * \file ./plugins/coreplugin/ioptionspage.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
 * \class Core::IOptionsPage
 * \brief Represents an application preferences page. Derive objects from this interface and set it
 * inside the PluginManager object pool to get the page into the Core::SettingsDialog.
*/

namespace Core {

class CORE_EXPORT IOptionsPage : public IGenericPage
{
    Q_OBJECT
public:
    IOptionsPage(QObject *parent = 0) : IGenericPage(parent) {}
    virtual ~IOptionsPage() {}

    // implemented in IGenericPage:
//    virtual QString id() const = 0;
//    virtual QString displayName() const = 0;
//    virtual QString category() const = 0;
//    virtual int sortIndex() const = 0;
//    virtual QWidget *createPage(QWidget *parent = 0) = 0;

    /*! Resets the current page to the defaults. */
    virtual void resetToDefaults() = 0;

    /** Check the validity of settings. You don't need to sync the settings. This will be automatically done. */
    virtual void checkSettingsValidity() = 0;

    /** Apply changes to the settings. You don't need to sync the settings. This will be automatically done. */
    virtual void apply() = 0;

    virtual void finish() = 0;

    virtual bool matches(const QString & searchKeyWord) const {Q_UNUSED(searchKeyWord); return false; }

    virtual QString helpPage() = 0;

};

} // namespace Core

#endif // IOPTIONSPAGE_H
