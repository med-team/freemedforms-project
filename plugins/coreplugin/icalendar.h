/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 ***************************************************************************/

#ifndef ICALENDAR_H
#define ICALENDAR_H

#include <QObject>
#include <QString>
#include <QWidget>

#include <coreplugin/core_exporter.h>

class QTextEdit;

/**
 * \file ./plugins/coreplugin/icalendar.h
 * \author Guillaume DENRY <guillaume.denry@gmail.com>
 * \version 1.0.0
 * \date 05 March 2017
 */

namespace Core {
	/**
       \brief Use this class to avoid any plugin dependencies (other than Core), when needing to access the \e current \e pad data.
	*/
	class CORE_EXPORT ICalendar : public QObject
	{
        Q_OBJECT

	public:
		ICalendar(QObject * parent = 0) : QObject(parent) {}
		virtual ~ICalendar() {}

		/**
		 * \brief returns a new calendar widget
		 */
		virtual QWidget *createCalendarWidget(QWidget *parent = 0) = 0;
	};
}

#endif
