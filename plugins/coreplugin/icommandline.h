/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CORE_ICOMMANDLINE_H
#define CORE_ICOMMANDLINE_H

#include <coreplugin/core_exporter.h>

#include <QObject>
#include <QVariant>

/**
 * \file ./plugins/coreplugin/icommandline.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
 * \class Core::ICommandLine
 * \brief Command line parser interface.
*/

namespace Core {

class CORE_EXPORT ICommandLine : public QObject
{
    Q_OBJECT

public:
    enum Params {
        Chrono = 1000,
        ConfigFile,
        RunningUnderWine,
        ClearUserDatabases,
        CreateVirtuals,
        ResetUserPreferences,
        UserClearLogin,
        UserClearPassword,
        CheckFormUpdates,
        AlwaysUpdateForms,
        MaxParam
    };

    ICommandLine(QObject *parent = 0) : QObject(parent) {}
    virtual ~ICommandLine() {}

    virtual QVariant value(int param, const QVariant &def = QVariant()) const = 0;
    virtual QString paramName(int param) const = 0;
};

} // end Core

#endif // CORE_ICOMMANDLINE_H
