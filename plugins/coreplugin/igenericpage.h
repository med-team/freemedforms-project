/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#ifndef CORE_IGENERICPAGE_H
#define CORE_IGENERICPAGE_H

#include <coreplugin/core_exporter.h>

#include <QObject>
#include <QString>
#include <QWidget>
#include <QIcon>

/**
 * \file ./plugins/coreplugin/igenericpage.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {

class CORE_EXPORT IGenericPage : public QObject
{
    Q_OBJECT
public:
    IGenericPage(QObject *parent = 0) : QObject(parent) {}
    virtual ~IGenericPage() {}

    virtual QString id() const = 0;
    virtual QString displayName() const = 0;
    virtual QString title() const = 0;
    virtual QIcon categoryIcon() const {return QIcon();}

    virtual QString category() const = 0;
    //TODO: make displayCategory pure virtual, implement in all implementing classes
    virtual QString displayCategory() const { return QString(); }

    virtual QWidget *createPage(QWidget *parent = 0) = 0;
    virtual int sortIndex() const = 0;

    static bool lessThan(IGenericPage *one, IGenericPage *two);
    static bool sortIndexLessThan(IGenericPage *one, IGenericPage *two);
};

} // namespace Core

#endif // CORE_IGENERICPAGE_H
