/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#ifndef IDEBUGPAGE_H
#define IDEBUGPAGE_H

#include <coreplugin/core_exporter.h>
#include <coreplugin/igenericpage.h>

#include <QObject>
#include <QString>
#include <QWidget>

/**
 * \file ./plugins/coreplugin/idebugpage.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
 * \class Core::IDebugPage
 * \brief Derive objects from this interface and set it inside the PluginManager object pool to get the page in the Core::DebugDialog.
*/

namespace Core {

class CORE_EXPORT IDebugPage : public Core::IGenericPage
{
    Q_OBJECT
public:
    IDebugPage(QObject *parent = 0) : Core::IGenericPage(parent) {}
    virtual ~IDebugPage() {}
    QString title() const {return displayName();}
};

} // namespace Core

#endif // IDEBUGPAGE_H
