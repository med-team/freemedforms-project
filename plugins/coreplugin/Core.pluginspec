<plugin name="Core" version="0.0.1" compatVersion="0.0.1">
    <vendor>FreeMedForms</vendor>
    <copyright>(C) 2008-2017 Eric Maeker, MD</copyright>
    <license>See application's license</license>
    <description>The core plugin for FreeMedForms.</description>
    <url>http://www.ericmaeker.fr/FreeMedForms/</url>
</plugin>
