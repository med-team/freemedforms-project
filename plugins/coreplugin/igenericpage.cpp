/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#include "igenericpage.h"

#include <QDebug>



bool Core::IGenericPage::lessThan(IGenericPage *one, IGenericPage *two)
{
    if (one->category() < two->category())
        return true;
    else if (one->category() == two->category()
             && one->sortIndex() < two->sortIndex())
             return true;
    return false;
}

bool Core::IGenericPage::sortIndexLessThan(IGenericPage *one, IGenericPage *two)
{
    return one->sortIndex() < two->sortIndex();
}
