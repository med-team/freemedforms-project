/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/


#include "saverestoreplugin.h"
#include "saverestorepage.h"

#include <utils/log.h>

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/translators.h>

#include <QtCore/QtPlugin>
#include <QDebug>

using namespace SaveRestore;
using namespace Internal;

SaveRestorePlugin::SaveRestorePlugin() :
    page(0)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating SaveRestorePlugin";
}

SaveRestorePlugin::~SaveRestorePlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool SaveRestorePlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "SaveRestorePlugin::initialize";
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);

    Core::ICore::instance()->translators()->addNewTranslator("plugin_saverestore");

    return true;
}

void SaveRestorePlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "SaveRestorePlugin::extensionsInitialized";

    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));
    page = new SaveRestorePage(this);
    addObject(page);
}

ExtensionSystem::IPlugin::ShutdownFlag SaveRestorePlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;

    if (page) {
        removeObject(page);
        delete page;
        page = 0;
    }

    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(SaveRestorePlugin)
