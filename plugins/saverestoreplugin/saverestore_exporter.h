/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#ifndef SAVERESTORE_EXPORTER_H
#define SAVERESTORE_EXPORTER_H

#include <qglobal.h>

#if defined(SAVERESTORE_LIBRARY)
#define SAVERESTORE_EXPORT Q_DECL_EXPORT
#else
#define SAVERESTORE_EXPORT Q_DECL_IMPORT
#endif

#endif
