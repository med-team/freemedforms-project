#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

DEFINES += SAVERESTORE_LIBRARY

include($${PWD}/../fmf_plugins.pri)
include($${PWD}/saverestoreplugin_dependencies.pri)

HEADERS = $${PWD}/saverestoreplugin.h \
    $${PWD}/saverestore_exporter.h \
    $${PWD}/saverestorepage.h

SOURCES = $${PWD}/saverestoreplugin.cpp \
    $${PWD}/saverestorepage.cpp

FORMS = $${PWD}/saverestorepage.ui

# Translators
TRANSLATIONS += $${SOURCES_TRANSLATIONS_PATH}/saverestoreplugin_fr.ts \
                $${SOURCES_TRANSLATIONS_PATH}/saverestoreplugin_de.ts \
                $${SOURCES_TRANSLATIONS_PATH}/saverestoreplugin_es.ts

