/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef SAVERESTORE_PAGE_H
#define SAVERESTORE_PAGE_H

#include <coreplugin/ioptionspage.h>

#include <QPointer>
#include <QString>

#include "ui_saverestorepage.h"

/**
 * \file ./plugins/saverestoreplugin/saverestorepage.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;
}

namespace Print {
class PrinterPreviewer;
class TextDocumentExtra;
}

namespace SaveRestore {
namespace Internal {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////  SaveRestoreWidget  //////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

class SaveRestoreWidget : public QWidget, private Ui::SaveRestoreWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(SaveRestoreWidget)

public:
    explicit SaveRestoreWidget(QWidget *parent = 0);
    void setDatasToUi();

    static void writeDefaultSettings( Core::ISettings *s );

public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);
    void on_saveButton_clicked();
    bool on_restoreButton_clicked();
    void on_importButton_clicked();
    void on_exportButton_clicked();

protected:
    virtual void changeEvent(QEvent *e);
};

}  // End Internal


class SaveRestorePage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    SaveRestorePage(QObject *parent = 0);
    ~SaveRestorePage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    QString helpPage() {return "saverestore.html";}

    static void writeDefaultSettings(Core::ISettings *s) {Internal::SaveRestoreWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);
private:
    QPointer<Internal::SaveRestoreWidget> m_Widget;
};

}  // End namespace SaveRestore

#endif // SAVERESTORE_PAGE_H
