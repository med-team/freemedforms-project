<plugin name="SaveRestore" version="0.0.1" compatVersion="0.0.1">
    <vendor>FreeMedForms</vendor>
    <copyright>(C) 2008-2017 by Eric MAEKER, MD</copyright>
    <license>See application's license</license>
    <description>The plugin manages user's datas saving and restoring.</description>
    <url>http://www.freemedforms.com</url>
    <dependencyList>
        <dependency name="Core" version="0.0.1"/>
    </dependencyList>
</plugin>
