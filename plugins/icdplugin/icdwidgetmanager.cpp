/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "icdwidgetmanager.h"
#include "constants.h"
#include "icddownloader.h"
#include "icdcentralwidget.h"
#include "icddatabase.h"

#include <utils/log.h>
#include <utils/global.h>
#include <utils/widgets/databaseinformationdialog.h>
#include <translationutils/constants.h>
#include <translationutils/trans_menu.h>
#include <translationutils/trans_database.h>

#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/isettings.h>
#include <coreplugin/imainwindow.h>
#include <coreplugin/constants_icons.h>
#include <coreplugin/constants_menus.h>
#include <coreplugin/contextmanager/contextmanager.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/actioncontainer.h>

#include <QGridLayout>
#include <QDialog>
#include <QTreeWidget>
#include <QHeaderView>

using namespace ICD;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline Core::ActionManager *actionManager() {return Core::ICore::instance()->actionManager();}
static inline Core::ContextManager *contextManager() { return Core::ICore::instance()->contextManager(); }

IcdWidgetManager *IcdWidgetManager::m_Instance = 0;

IcdWidgetManager *IcdWidgetManager::instance()
{
    if (!m_Instance)
        m_Instance = new IcdWidgetManager(qApp);
    return m_Instance;
}

IcdWidgetManager::IcdWidgetManager(QObject *parent) : IcdActionHandler(parent)
{
    connect(Core::ICore::instance()->contextManager(), SIGNAL(contextChanged(Core::IContext*,Core::Context)),
            this, SLOT(updateContext(Core::IContext*,Core::Context)));
    setObjectName("IcdWidgetManager");
}

void IcdWidgetManager::updateContext(Core::IContext *object, const Core::Context &additionalContexts)
{
    Q_UNUSED(additionalContexts);
    IcdCentralWidget *view = 0;
    do {
        if (!object) {
            if (!m_CurrentView)
                return;

            break;
        }
        view = qobject_cast<IcdCentralWidget *>(object->widget());
        if (!view) {
            if (!m_CurrentView)
                return;

            break;
        }

        if (view == m_CurrentView) {
            return;
        }

    } while (false);
    if (view) {
        IcdActionHandler::setCurrentView(view);
    }
}

IcdCentralWidget *IcdWidgetManager::currentView() const
{
    return IcdActionHandler::m_CurrentView;
}


IcdActionHandler::IcdActionHandler(QObject *parent) :
        QObject(parent),
        aRecreateDatabase(0),
        aShowDatabaseInformation(0),
        aSearchByLabel(0),
        aSearchByCode(0),
        gSearchMethod(0), gModes(0),
        aSelectorSimpleMode(0), aSelectorFullMode(0),
        aCollectionModelFullMode(0), aCollectionModelSimpleMode(0),
        aToggleSelector(0), aClear(0), aRemoveRow(0), aPrint(0), aPrintPreview(0),
        m_CurrentView(0)
{
    setObjectName("IcdActionHandler");
    Core::ITheme *th = Core::ICore::instance()->theme();
    QAction *a = 0;
    Core::Command *cmd = 0;
    Core::Context ctx(ICD::Constants::C_ICD_PLUGINS);
    Core::Context globalcontext(Core::Constants::C_GLOBAL);


    Core::ActionContainer *hmenu = actionManager()->actionContainer(Core::Id(Core::Constants::M_HELP_DATABASES));
    if (!Utils::isReleaseCompilation()) {
        a = aRecreateDatabase = new QAction(this);
        a->setObjectName("aRecreateDatabase");
        cmd = actionManager()->registerAction(a, Core::Id(Constants::A_RECREATE_ICD_DB), globalcontext);
        cmd->setTranslations(Constants::RECREATE_DATABASE_TEXT, Constants::RECREATE_DATABASE_TEXT, Constants::ICDCONSTANTS_TR_CONTEXT);
        hmenu->addAction(cmd, Core::Id(Core::Constants::G_HELP_DATABASES));
        connect(a, SIGNAL(triggered()), this, SLOT(recreateDatabase()));
    }

    a = aShowDatabaseInformation = new QAction(this);
    a->setIcon(th->icon(Core::Constants::ICONHELP));
    cmd = actionManager()->registerAction(a, Core::Id(Constants::A_DATABASE_INFOS), globalcontext);
    cmd->setTranslations(Constants::DATABASE_INFOS_TEXT, Constants::DATABASE_INFOS_TEXT, Constants::ICDCONSTANTS_TR_CONTEXT);
    cmd->retranslate();
    hmenu->addAction(cmd, Core::Id(Core::Constants::G_HELP_DATABASES));
    connect(aShowDatabaseInformation,SIGNAL(triggered()), this, SLOT(showDatabaseInformation()));

    Core::ActionContainer *searchmenu = actionManager()->actionContainer(Core::Id(Constants::M_ICD_SEARCH));
    if (!searchmenu) {
        searchmenu = actionManager()->createMenu(Constants::M_ICD_SEARCH);
        searchmenu->appendGroup(Core::Id(Constants::G_ICD_SEARCH));
        searchmenu->setTranslations(Constants::SEARCHMENU_TEXT, Constants::ICDCONSTANTS_TR_CONTEXT);
#ifndef FREETOOLBOX
        Core::ActionContainer *menu = actionManager()->actionContainer(Core::Id(Core::Constants::M_EDIT));
        menu->addMenu(searchmenu, Core::Id(Core::Constants::G_EDIT_FIND));
#endif
    }
    Q_ASSERT(searchmenu);

    gSearchMethod = new QActionGroup(this);
    a = aSearchByLabel = new QAction(this);
    a->setCheckable(true);
    a->setChecked(false);
    a->setIcon(th->icon(Constants::I_SEARCH_LABEL));
    cmd = actionManager()->registerAction(a, Core::Id(Constants::A_SEARCH_LABEL), ctx);
    cmd->setTranslations(Constants::SEARCHLABEL_TEXT, Constants::SEARCHLABEL_TEXT, Constants::ICDCONSTANTS_TR_CONTEXT);
    searchmenu->addAction(cmd, Core::Id(Constants::G_ICD_SEARCH));
    gSearchMethod->addAction(a);

    a = aSearchByCode = new QAction(this);
    a->setCheckable(true);
    a->setChecked(false);
    a->setIcon(th->icon(Constants::I_SEARCH_CODE));
    cmd = actionManager()->registerAction(a, Core::Id(Constants::A_SEARCH_CODE), ctx);
    cmd->setTranslations(Constants::SEARCHCODE_TEXT, Constants::SEARCHCODE_TEXT, Constants::ICDCONSTANTS_TR_CONTEXT);
    searchmenu->addAction(cmd, Core::Id(Constants::G_ICD_SEARCH));
    gSearchMethod->addAction(a);
    connect(gSearchMethod,SIGNAL(triggered(QAction*)),this,SLOT(searchActionChanged(QAction*)));

    Core::ActionContainer *modesmenu = actionManager()->actionContainer(Constants::M_ICD_MODES);
    if (!modesmenu) {
        modesmenu = actionManager()->createMenu(Constants::M_ICD_MODES);
        modesmenu->appendGroup(Core::Id(Constants::G_ICD_SELECTORMODE));
        modesmenu->appendGroup(Core::Id(Constants::G_ICD_COLLECTIONMODE));
        modesmenu->setTranslations(Constants::MODESMENU_TEXT, Constants::ICDCONSTANTS_TR_CONTEXT);
#ifndef FREETOOLBOX
        Core::ActionContainer *menu = actionManager()->actionContainer(Core::Constants::M_EDIT);
        menu->addMenu(modesmenu, Core::Id(Core::Constants::G_EDIT_OTHER));
#endif
    }
    Q_ASSERT(modesmenu);

    gModes = new QActionGroup(this);
    a = aSelectorSimpleMode = new QAction(this);
    a->setObjectName("aSelectorSimpleMode");
    a->setCheckable(true);
    a->setChecked(false);
    cmd = actionManager()->registerAction(a, Core::Id(Constants::A_SELECTOR_SIMPLEMODE), ctx);
    cmd->setTranslations(Constants::SELECTORSIMPLEMODE_TEXT, Constants::SELECTORSIMPLEMODE_TEXT, Constants::ICDCONSTANTS_TR_CONTEXT);
    modesmenu->addAction(cmd, Core::Id(Constants::G_ICD_SELECTORMODE));
    gModes->addAction(a);

    a = aSelectorFullMode = new QAction(this);
    a->setObjectName("aSelectorFullMode");
    a->setCheckable(true);
    a->setChecked(false);
    cmd = actionManager()->registerAction(a, Core::Id(Constants::A_SELECTOR_FULLMODE), ctx);
    cmd->setTranslations(Constants::SELECTORFULLMODE_TEXT, Constants::SELECTORFULLMODE_TEXT, Constants::ICDCONSTANTS_TR_CONTEXT);
    modesmenu->addAction(cmd, Core::Id(Constants::G_ICD_SELECTORMODE));
    gModes->addAction(a);

    a = aCollectionModelSimpleMode = new QAction(this);
    a->setObjectName("aCollectionModelSimpleMode");
    a->setCheckable(true);
    a->setChecked(false);
    cmd = actionManager()->registerAction(a, Core::Id(Constants::A_COLLECTION_SIMPLEMODE), ctx);
    cmd->setTranslations(Constants::COLLECTIONSIMPLEMODE_TEXT, Constants::COLLECTIONSIMPLEMODE_TEXT, Constants::ICDCONSTANTS_TR_CONTEXT);
    modesmenu->addAction(cmd, Core::Id(Constants::G_ICD_COLLECTIONMODE));
    gModes->addAction(a);

    a = aCollectionModelFullMode = new QAction(this);
    a->setObjectName("aCollectionModelFullMode");
    a->setCheckable(true);
    a->setChecked(false);
    cmd = actionManager()->registerAction(a, Core::Id(Constants::A_COLLECTION_FULLMODE), ctx);
    cmd->setTranslations(Constants::COLLECTIONFULLMODE_TEXT, Constants::COLLECTIONFULLMODE_TEXT, Constants::ICDCONSTANTS_TR_CONTEXT);
    modesmenu->addAction(cmd, Core::Id(Constants::G_ICD_COLLECTIONMODE));
    gModes->addAction(a);
    connect(gModes, SIGNAL(triggered(QAction*)), this, SLOT(modeActionChanged(QAction*)));

    a = aToggleSelector = new QAction(this);
    a->setObjectName("aToggleSelector");
    a->setIcon(th->icon(Constants::I_TOGGLEICDSELECTOR));
    cmd = actionManager()->registerAction(a, Core::Id(Constants::A_TOGGLE_ICDSELECTOR), ctx);
    cmd->setTranslations(Constants::TOGGLEICDSELECTOR_TEXT, Constants::TOGGLEICDSELECTOR_TEXT, Constants::ICDCONSTANTS_TR_CONTEXT);
    connect(a, SIGNAL(triggered()), this, SLOT(toggleSelector()));

    a = aClear = new QAction(this);
    a->setIcon(th->icon(Core::Constants::ICONCLEAR));
    cmd = actionManager()->registerAction(a, Core::Id(Core::Constants::A_LIST_CLEAR), ctx);
    cmd->setTranslations(Trans::Constants::LISTCLEAR_TEXT);
    connect(a, SIGNAL(triggered()), this, SLOT(clear()));

    a = aRemoveRow = new QAction(this);
    a->setIcon(th->icon(Core::Constants::ICONREMOVE));
    cmd = actionManager()->registerAction(a, Core::Id(Core::Constants::A_LIST_REMOVE), ctx);
    cmd->setTranslations(Trans::Constants::LISTREMOVE_TEXT);
    connect(a, SIGNAL(triggered()), this, SLOT(removeItem()));



    contextManager()->updateContext();
    actionManager()->retranslateMenusAndActions();
}

void IcdActionHandler::setCurrentView(IcdCentralWidget *view)
{
    Q_ASSERT(view);
    if (!view) { // this should never be the case
        Utils::Log::addError(this, "setCurrentView: no view", __FILE__, __LINE__);
        return;
    }

    if (m_CurrentView) {
        if (view == m_CurrentView.data())
            return;
    }
    m_CurrentView = view;

    updateActions();
}

void IcdActionHandler::updateActions()
{
    if (!m_CurrentView)
        return;

    if (m_CurrentView->selectorMode() == IcdCentralWidget::SelectorSimpleMode) {
        aSelectorSimpleMode->setChecked(true);
        aSelectorFullMode->setChecked(false);
    } else {
        aSelectorSimpleMode->setChecked(false);
        aSelectorFullMode->setChecked(true);
    }

    if (m_CurrentView->collectionMode() == IcdCentralWidget::CollectionSimpleMode) {
        aCollectionModelSimpleMode->setChecked(true);
        aCollectionModelFullMode->setChecked(false);
    } else {
        aCollectionModelSimpleMode->setChecked(false);
        aCollectionModelFullMode->setChecked(true);
    }
}

void IcdActionHandler::recreateDatabase()
{
    if (!m_Downloader)
        m_Downloader = new IcdDownloader(this);
    m_Downloader->createDatabase();
    connect(m_Downloader, SIGNAL(processEnded()), m_Downloader, SLOT(deleteLater()));
}

void IcdActionHandler::showDatabaseInformation()
{
    Utils::DatabaseInformationDialog dlg(Core::ICore::instance()->mainWindow());
    dlg.setTitle(tkTr(Trans::Constants::ICD_DATABASE_INFORMATION));
    dlg.setDatabase(*IcdDatabase::instance());
    Utils::resizeAndCenter(&dlg);
    dlg.exec();
}

void IcdActionHandler::searchActionChanged(QAction *a)
{
    Q_UNUSED(a);
}

void IcdActionHandler::modeActionChanged(QAction *a)
{
    if (!m_CurrentView)
        return;
    if (a == aSelectorSimpleMode) {
        m_CurrentView->setSelectorMode(IcdCentralWidget::SelectorSimpleMode);
    } else if (a == aSelectorFullMode) {
        m_CurrentView->setSelectorMode(IcdCentralWidget::SelectorSimpleMode);
    } else if (a == aCollectionModelSimpleMode) {
        m_CurrentView->setCollectionMode(IcdCentralWidget::CollectionSimpleMode);
    } else if (a == aCollectionModelFullMode) {
        m_CurrentView->setCollectionMode(IcdCentralWidget::CollectionFullMode);
    }
}

void IcdActionHandler::toggleSelector()
{
    if (m_CurrentView)
        m_CurrentView->toggleSelector();
}

void IcdActionHandler::clear()
{
    if (m_CurrentView)
        m_CurrentView->clear();
}

void IcdActionHandler::removeItem()
{
    if (m_CurrentView)
        m_CurrentView->removeItem();
}

void IcdActionHandler::print()
{
    if (m_CurrentView)
        m_CurrentView->print();
}
