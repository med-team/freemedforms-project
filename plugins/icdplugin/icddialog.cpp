/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "icddialog.h"
#include "icdviewer.h"
#include "simpleicdmodel.h"
#include "fullicdcodemodel.h"
#include "icdassociation.h"

#include <utils/global.h>

#include <QGridLayout>
#include <QDialogButtonBox>

#include <QDebug>

using namespace ICD;

IcdDialog::IcdDialog(const QVariant &SID, QWidget *parent) :
    QDialog(parent),
    m_View(0)
{
    QGridLayout *lay = new QGridLayout(this);
    setLayout(lay);
    m_View = new IcdViewer(this);
    lay->addWidget(m_View, 0, 0);
    m_View->setCodeSid(SID);

    QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    lay->addWidget(buttonBox, 10, 0);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    Utils::resizeAndCenter(this, parent);
}

void IcdDialog::done(int result)
{
    QDialog::done(result);
}

bool IcdDialog::isSelectionValid() const
{
    return m_View->icdModel()->isSelectionValid();
}


bool IcdDialog::isUniqueCode() const
{
    if (!m_View)
        return false;
    if (!m_View->icdModel())
        return false;
    return (m_View->icdModel()->codeCanBeUsedAlone() &&
            (m_View->icdModel()->dagStarModel()->numberOfCheckedItems()==0));
}


bool IcdDialog::isAssociation() const
{
    if (!m_View)
        return false;
    if (!m_View->icdModel())
        return false;

    return (m_View->icdModel()->isSelectionValid() &&
            (m_View->icdModel()->dagStarModel()->numberOfCheckedItems()>0));
}


QVariant IcdDialog::getSidCode() const
{
    if (isAssociation())
        return QVariant();
    return m_View->icdModel()->getCodeSid();
}


QVector<Internal::IcdAssociation> IcdDialog::getAssocation() const
{
    if (!isAssociation())
        return QVector<Internal::IcdAssociation>();
    return m_View->icdModel()->dagStarModel()->getCheckedAssociations();
}
