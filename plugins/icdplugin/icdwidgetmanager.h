/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ICDWIDGETMANAGER_H
#define ICDWIDGETMANAGER_H

#include <icdplugin/icd_exporter.h>
#include <coreplugin/contextmanager/icontext.h>

#include <QWidget>
#include <QObject>
#include <QAction>
#include <QPointer>

/**
 * \file ./plugins/icdplugin/icdwidgetmanager.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
 * \internal
*/

namespace ICD {
class IcdWidgetManager;
class IcdDownloader;
class IcdCentralWidget;

namespace Internal {

class IcdActionHandler : public QObject
{
    Q_OBJECT
public:
    enum Modes {
        Prescriber = 0,
        SelectOnly
    };

    IcdActionHandler(QObject *parent = 0);
    virtual ~IcdActionHandler() {}

    void setCurrentView(IcdCentralWidget *view);

private Q_SLOTS:
    void recreateDatabase();
    void showDatabaseInformation();
    void searchActionChanged(QAction*);
    void modeActionChanged(QAction*);
    void toggleSelector();
    void clear();
    void removeItem();
    void print();

private:
    void updateActions();

protected:
    QAction *aRecreateDatabase;
    QAction *aShowDatabaseInformation;
    QAction *aSearchByLabel, *aSearchByCode;
    QActionGroup *gSearchMethod, *gModes;
    QAction *aSelectorSimpleMode, *aSelectorFullMode;
    QAction *aCollectionModelFullMode, *aCollectionModelSimpleMode;
    QAction *aToggleSelector, *aClear, *aRemoveRow, *aPrint, *aPrintPreview;
    QPointer<IcdCentralWidget> m_CurrentView;
    QPointer<IcdDownloader> m_Downloader;
};

}  // End Internal
}  // End DrugsWidget


namespace ICD {
class IcdPlugin;

class ICD_EXPORT IcdWidgetManager : public Internal::IcdActionHandler
{
    Q_OBJECT
    friend class ICD::IcdPlugin;

public:
    static IcdWidgetManager *instance();
    ~IcdWidgetManager() {}

    IcdCentralWidget *currentView() const;

protected:
    void refreshViews();


private Q_SLOTS:
    void updateContext(Core::IContext *object, const Core::Context &additionalContexts);

private:
    IcdWidgetManager(QObject *parent = 0);
    static IcdWidgetManager *m_Instance;
};

}  // End namespace ICD

#endif // ICDWIDGETMANAGER_H
