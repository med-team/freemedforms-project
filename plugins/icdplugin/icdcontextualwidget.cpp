/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "icdcontextualwidget.h"
#include "icdwidgetmanager.h"
#include "constants.h"

#include <coreplugin/contextmanager/contextmanager.h>
#include <coreplugin/icore.h>

using namespace ICD;

static inline Core::ContextManager *contextManager() { return Core::ICore::instance()->contextManager(); }

namespace ICD {
namespace Internal {

class IcdContext : public Core::IContext
{
public:
    IcdContext(IcdContextualWidget *w) : Core::IContext(w)
    {
        setObjectName("IcdContext");
        setWidget(w);
    }
};

}  // End namespace Internal
}  // End namespace ICD



IcdContextualWidget::IcdContextualWidget(QWidget *parent) :
    QWidget(parent), m_Context(0)
{
    m_Context = new Internal::IcdContext(this);
    m_Context->setContext(Core::Context(Constants::C_ICD_PLUGINS));

    contextManager()->addContextObject(m_Context);
}

IcdContextualWidget::~IcdContextualWidget()
{
    contextManager()->removeContextObject(m_Context);
}
