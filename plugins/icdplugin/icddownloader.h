/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ICDDOWNLOADER_H
#define ICDDOWNLOADER_H

#include <icdplugin/icd_exporter.h>

#include <QObject>
QT_BEGIN_NAMESPACE
class QProgressDialog;
QT_END_NAMESPACE

/**
 * \file ./plugins/icdplugin/icddownloader.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/


namespace Utils {
class HttpDownloader;
}

namespace ICD {

class ICD_EXPORT IcdDownloader : public QObject
{
    Q_OBJECT

public:
    IcdDownloader(QObject *parent = 0);
    ~IcdDownloader();

public Q_SLOTS:
    bool createDatabase();

protected Q_SLOTS:
    bool downloadRawSources();
    bool downloadFinished();
    bool populateDatabaseWithRawSources();

Q_SIGNALS:
    void processEnded();

private:
    Utils::HttpDownloader *m_Downloader;
    QProgressDialog *m_Progress;
};

}  // End namespace ICD

#endif // ICDDOWNLOADER_H
