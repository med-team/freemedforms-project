/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ICDVIEWER_H
#define ICDVIEWER_H

#include <icdplugin/icd_exporter.h>

#include <QWidget>


namespace ICD {
namespace Internal {
class IcdViewerPrivate;
}  // End namespace Internal
class FullIcdCodeModel;

namespace Ui {
    class IcdViewer;
}

class ICD_EXPORT IcdViewer : public QWidget
{
    Q_OBJECT

public:
    explicit IcdViewer(QWidget *parent = 0);
    ~IcdViewer();

    FullIcdCodeModel *icdModel() const;

public Q_SLOTS:
    void setCodeSid(const QVariant &sid);

protected:
    void changeEvent(QEvent *e);

private:
    Internal::IcdViewerPrivate *d;
};


}  // End namespace ICD


#endif // ICDVIEWER_H
