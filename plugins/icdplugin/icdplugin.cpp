/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "icdplugin.h"
#include "icdwidgetmanager.h"
#include "icddatabase.h"
#include "icddownloader.h"

#ifdef FREEMEDFORMS
#    include "icdwidgetfactory.h"
#endif

#include <utils/log.h>

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/itheme.h>
#include <coreplugin/translators.h>

#include <QtPlugin>
#include <QDebug>

using namespace ICD::Internal;

static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline void messageSplash(const QString &s) {theme()->messageSplashScreen(s); }

IcdPlugin::IcdPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating IcdPlugin";

    IcdDatabase::instance();
}

IcdPlugin::~IcdPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool IcdPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "IcdPlugin::initialize";
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);

    Core::ICore::instance()->translators()->addNewTranslator("plugin_icd");
    messageSplash(tr("Initializing ICD10 plugin..."));

    return true;
}

void IcdPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "IcdPlugin::extensionsInitialized";

    messageSplash(tr("Initializing ICD10 plugin..."));

    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));

#ifdef FREEMEDFORMS
    addAutoReleasedObject(new IcdWidgetFactory(this));
#endif

    connect(Core::ICore::instance(), SIGNAL(coreOpened()), this, SLOT(postCoreInitialization()));
}

void IcdPlugin::postCoreInitialization()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;

    IcdDatabase::instance()->initialize();

#ifndef FREETOOLBOX
    IcdWidgetManager::instance();
#endif
}

ExtensionSystem::IPlugin::ShutdownFlag IcdPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(IcdPlugin)
