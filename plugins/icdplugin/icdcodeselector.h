/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ICDCODESELECTOR_H
#define ICDCODESELECTOR_H

#include <icdplugin/icd_exporter.h>
#include <icdplugin/icdcontextualwidget.h>

QT_BEGIN_NAMESPACE
class QToolButton;
class QModelIndex;
QT_END_NAMESPACE


namespace ICD {
class IcdSearchModel;

namespace Ui {
    class IcdCodeSelector;
}

class ICD_EXPORT IcdCodeSelector : public IcdContextualWidget
{
    Q_OBJECT

public:
    explicit IcdCodeSelector(QWidget *parent = 0);
    ~IcdCodeSelector();

    void initialize();
    void setModel(IcdSearchModel *model);
    IcdSearchModel *model() const;

Q_SIGNALS:
    void entered(const QVariant &SID);
    void activated(const QVariant &SID);

protected:
    void changeEvent(QEvent *e);

private:
    void populateToolButtons();

private Q_SLOTS:
    void onEntered(const QModelIndex &);
    void onActivated(const QModelIndex &);
    void setSearchByLabel();
    void setSearchByCode();
    void setFilter(const QString &search);

private:
    Ui::IcdCodeSelector *ui;
    QToolButton *m_SearchToolButton;
};

}  // End namespace ICD

#endif // ICDCODESELECTOR_H
