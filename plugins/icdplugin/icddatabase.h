/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ICDDATABASE_H
#define ICDDATABASE_H

#include <icdplugin/icd_exporter.h>

#include <utils/database.h>

/**
 * \file ./plugins/icdplugin/icddatabase.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DataPack {
class Pack;
}

namespace ICD {

namespace Internal {
class IcdDatabasePrivate;
class IcdPlugin;
class IcdAssociation;
}

class ICD_EXPORT IcdDatabase : public QObject, public Utils::Database
{
    Q_OBJECT
    friend class ICD::Internal::IcdPlugin;
    IcdDatabase(QObject *parent = 0);

public:
    static IcdDatabase *instance();
    ~IcdDatabase();

    // Initializer / Checkers
    static bool isInitialized() { return m_initialized; }
    void logChronos(bool state);
    QString getDatabaseVersion();
    bool isDownloadAndPopulatingNeeded() const;

    QList<int> getHeadersSID(const QVariant &SID);

    QVariant getSid(const QString &code);
    QVariant getIcdCode(const QVariant &SID);
    QString getDagStarCode(const QVariant &SID);
    QString invertDagCode(const QString &dagCode) const;
    bool isDagetADag(const QString &dagCode) const;
    QString getHumanReadableIcdDaget(const QVariant &SID);
    QVariant getIcdCodeWithDagStar(const QVariant &SID);

    QVector<int> getDagStarDependencies(const QVariant &SID);
    Internal::IcdAssociation getAssociation(const QVariant &mainSID, const QVariant &associatedSID);

    bool codeCanBeUsedAlone(const QVariant &SID);

    QString getLabelFromLid(const QVariant &LID);
    QString getSystemLabel(const QVariant &SID);
    QStringList getAllLabels(const QVariant &SID, const int libelleFieldLang = -1);
    QString getAssociatedLabel(const QVariant &mainSID, const QVariant &associatedSID);
    QStringList getIncludedLabels(const QVariant &SID);

    QVector<int> getExclusions(const QVariant &SID);

    QString getMemo(const QVariant &SID);

Q_SIGNALS:
    void databaseInitialized();

protected:
    bool initialize();
    bool refreshDatabase();

private Q_SLOTS:
    void packChanged(const DataPack::Pack &pack);
    void refreshLanguageDependCache();

Q_SIGNALS:
    void databaseChanged();

private:
    // intialization state
    static IcdDatabase *m_Instance;
    static bool m_initialized;
    Internal::IcdDatabasePrivate *d;
};

} // End namespace ICD


#endif // ICDDATABASE_H
