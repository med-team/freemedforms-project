/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "fullicdcodemodel.h"
#include "simpleicdmodel.h"
#include "icddatabase.h"
#include "constants.h"

#include <coreplugin/icore.h>
#include <coreplugin/translators.h>

#include <translationutils/constanttranslations.h>

#include <QString>
#include <QLocale>
#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QSqlError>
#include <QStringListModel>
#include <QPointer>
#include <QStandardItemModel>

#include <QDebug>

using namespace ICD;

using namespace Trans::ConstantTranslations;

static inline ICD::IcdDatabase *icdBase() {return ICD::IcdDatabase::instance();}

namespace ICD {
namespace Internal {

class FullIcdCodeModelPrivate
{
public:
    FullIcdCodeModelPrivate(FullIcdCodeModel *parent) :
            m_CodeTreeModel(0), m_LabelModel(0), m_ExcludeModel(0), m_DagStarModel(0), m_Include(0),
            q(parent)
    {
    }

    ~FullIcdCodeModelPrivate()
    {
    }

    void createCodeTreeModel(const QVariant &SID)
    {
        if (!m_CodeTreeModel) {
            m_CodeTreeModel = new QStandardItemModel(0, 1, q);
        } else {
            m_CodeTreeModel->clear();
        }
        QList<int> ids = icdBase()->getHeadersSID(SID); // get all ids from 1 to 7
        QStandardItem *parentItem = m_CodeTreeModel->invisibleRootItem();
        QFont bold;
        bold.setBold(true);
        QString systemLabel;
        foreach(int id, ids) {
            if (id == 0)
                break;
            systemLabel = icdBase()->getSystemLabel(id);
            QString title = QString("%1 - %2")
                            .arg(icdBase()->getIcdCode(id).toString())
                            .arg(systemLabel);
            QStandardItem *item = new QStandardItem(title);
            item->setToolTip(title);
            parentItem->appendRow(item);
            item->setFont(bold);
            parentItem = item;
        }
        foreach(const QString &label, icdBase()->getAllLabels(SID)) {
            if (label.isEmpty())
                continue;
            if (label==systemLabel)
                continue;
            QString title = QString("%1 - %2")
                            .arg(icdBase()->getIcdCode(SID).toString())
                            .arg(label);
            QStandardItem *item = new QStandardItem(label);
            item->setToolTip(title);
            parentItem->appendRow(item);
        }
    }

public:
    QStandardItemModel *m_CodeTreeModel;
    SimpleIcdModel *m_LabelModel;
    SimpleIcdModel *m_ExcludeModel;
    SimpleIcdModel *m_DagStarModel;
    QStringListModel *m_Include;
    QVariant m_SID;

private:
    FullIcdCodeModel *q;
};


}  // End namespace Internal
}  // End namespace ICD


FullIcdCodeModel::FullIcdCodeModel(QObject *parent) :
        QAbstractTableModel(parent), d(0)
{
    d = new Internal::FullIcdCodeModelPrivate(this);
    connect(Core::ICore::instance()->translators(), SIGNAL(languageChanged()), this, SLOT(updateTranslations()));
}

FullIcdCodeModel::~FullIcdCodeModel()
{
    if (d)
        delete d;
    d=0;
}

void FullIcdCodeModel::setCode(const int SID)
{
    if (SID < 0)
        return;

    d->m_SID = SID;

    d->m_LabelModel = new SimpleIcdModel(this);  // auto-translations
    d->m_LabelModel->addCodes(QVector<int>() << SID);  // add true if you want the combolabel to show all available labels

    d->m_Include = new QStringListModel(this);

    d->m_ExcludeModel = new SimpleIcdModel(this);  // auto-translations
    d->m_ExcludeModel->addCodes(icdBase()->getExclusions(SID));

    d->m_DagStarModel = new SimpleIcdModel(this);  // auto-translations
    d->m_DagStarModel->setUseDagDependencyWithSid(SID);
    d->m_DagStarModel->setCheckable(true);
    d->m_DagStarModel->addCodes(icdBase()->getDagStarDependencies(SID));


    updateTranslations();
}

QVariant FullIcdCodeModel::getCodeSid() const
{
    return d->m_SID;
}

bool FullIcdCodeModel::codeCanBeUsedAlone() const
{
    return icdBase()->codeCanBeUsedAlone(d->m_SID);
}

bool FullIcdCodeModel::codeMustBeAssociated() const
{
    return (!icdBase()->codeCanBeUsedAlone(d->m_SID));
}

bool FullIcdCodeModel::isSelectionValid() const
{
    if (icdBase()->codeCanBeUsedAlone(d->m_SID))
        return true;
    if (d->m_DagStarModel->numberOfCheckedItems()>=1)
        return true;

    return false;
}

int FullIcdCodeModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) return 0;

    return 1;
}

int FullIcdCodeModel::columnCount(const QModelIndex &) const
{
    return ColumnCount;
}

QVariant FullIcdCodeModel::data(const QModelIndex &index, int role) const
{
    if (index.column()==Memo && (role == Qt::DisplayRole || Qt::EditRole)) {
        return icdBase()->getMemo(d->m_SID);
    }
    return d->m_LabelModel->data(index, role);
}

QStringListModel *FullIcdCodeModel::labelsModel()
{
    return d->m_LabelModel->labelsModel(d->m_LabelModel->index(0,0));
}

QAbstractItemModel *FullIcdCodeModel::codeTreeModel()
{
    return d->m_CodeTreeModel;
}

QStringListModel *FullIcdCodeModel::includedLabelsModel()
{
    return d->m_Include;
}

QAbstractItemModel *FullIcdCodeModel::excludedModel()
{
    return d->m_ExcludeModel;
}

SimpleIcdModel *FullIcdCodeModel::dagStarModel()
{
    return d->m_DagStarModel;
}


void FullIcdCodeModel::updateTranslations()
{
    beginResetModel();
    d->createCodeTreeModel(d->m_SID);

    d->m_Include->setStringList(icdBase()->getIncludedLabels(d->m_SID));
    endResetModel();
}
