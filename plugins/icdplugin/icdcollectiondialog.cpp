/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "icdcollectiondialog.h"
#include "icdcentralwidget.h"
#include "simpleicdmodel.h"
#include "fullicdcodemodel.h"
#include "icdassociation.h"

#include <utils/global.h>

#include <QGridLayout>
#include <QDialogButtonBox>

#include <QDebug>

using namespace ICD;

IcdCollectionDialog::IcdCollectionDialog(QWidget *parent) :
    QDialog(parent)
{
    QGridLayout *lay = new QGridLayout(this);
    setLayout(lay);
    m_View = new IcdCentralWidget(this);
    lay->addWidget(m_View, 0, 0);

    QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    lay->addWidget(buttonBox, 10, 0);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    Utils::resizeAndCenter(this, parent);
}

IcdCollectionDialog::~IcdCollectionDialog()
{
}

void IcdCollectionDialog::setIcdCollectionModel(IcdCollectionModel *model)
{
    Q_ASSERT(m_View);
    if (!m_View)
        return;
    m_View->setIcdCollectionModel(model);
}

void IcdCollectionDialog::setXmlIcdCollection(const QString &xml)
{
    Q_ASSERT(m_View);
    if (!m_View)
        return;
    if (xml.isEmpty())
        m_View->clear();
    else
        m_View->readXmlCollection(xml);
}

QString IcdCollectionDialog::xmlIcdCollection()
{
    Q_ASSERT(m_View);
    if (!m_View)
        return QString();
    return m_View->collectionToXml();
}

void IcdCollectionDialog::done(int r)
{
    QDialog::done(r);
}
