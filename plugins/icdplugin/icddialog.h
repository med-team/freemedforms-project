/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ICDDIALOG_H
#define ICDDIALOG_H

#include <icdplugin/icd_exporter.h>

#include <QDialog>

namespace ICD {
namespace Internal {
class IcdAssociation;
}

class IcdViewer;

class ICD_EXPORT IcdDialog : public QDialog
{
    Q_OBJECT
public:
    explicit IcdDialog(const QVariant &SID, QWidget *parent = 0);

    bool isSelectionValid() const;
    bool isUniqueCode() const;
    bool isAssociation() const;

    QVariant getSidCode() const;
    QVector<Internal::IcdAssociation> getAssocation() const;

public Q_SLOTS:
    void done(int result);

private:
    IcdViewer *m_View;

};


}  // End namespace ICD

#endif // ICDDIALOG_H
