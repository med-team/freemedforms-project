/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ICDCONTEXTUALWIDGET_H
#define ICDCONTEXTUALWIDGET_H

#include <icdplugin/icd_exporter.h>

#include <QWidget>

/**
 * \file ./plugins/icdplugin/icdcontextualwidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
 * \internal
*/


namespace ICD {
namespace Internal {
class IcdContext;
}

class ICD_EXPORT IcdContextualWidget : public QWidget
{
    Q_OBJECT
public:
    IcdContextualWidget(QWidget *parent = 0);
    virtual ~IcdContextualWidget();

private:
    Internal::IcdContext *m_Context;
};

}  // End namespace ICD

#endif // ICDCONTEXTUALWIDGET_H
