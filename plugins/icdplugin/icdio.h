/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ICDIO_H
#define ICDIO_H

#include <icdplugin/icd_exporter.h>
#include <QtGlobal>

QT_BEGIN_NAMESPACE
class QString;
QT_END_NAMESPACE

/**
 * \file ./plugins/icdplugin/icdio.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace ICD {
class IcdCollectionModel;
namespace Internal {
class IcdIOPrivate;
}  // End namespace Internal


class ICD_EXPORT IcdIO
{
public:
    enum ModelManagement {
        ReplaceModelContent = 0,
        AddToModel
    };

    IcdIO();
    ~IcdIO();

    static bool isDatabaseInitialized();

    QString icdCollectionToXml(const IcdCollectionModel *model);
    bool icdCollectionFromXml(IcdCollectionModel *model, const QString &xml, const ModelManagement management = ReplaceModelContent);

    QString icdCollectionToHtml(const IcdCollectionModel *model);

private:
    Internal::IcdIOPrivate *d;
};

}  // End namespace ICD

#endif // ICDIO_H
