/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ICDCOLLECTIONDIALOG_H
#define ICDCOLLECTIONDIALOG_H

#include <icdplugin/icd_exporter.h>

#include <QDialog>

namespace ICD {
class IcdCollectionModel;
class IcdCentralWidget;

class ICD_EXPORT IcdCollectionDialog : public QDialog
{
    Q_OBJECT
public:
    explicit IcdCollectionDialog(QWidget *parent = 0);
    ~IcdCollectionDialog();

    void setIcdCollectionModel(IcdCollectionModel *model);
    void setXmlIcdCollection(const QString &xml);
    QString xmlIcdCollection();

public Q_SLOTS:
    void done(int r);

private:
    IcdCollectionModel *m_CollectionModel;
    IcdCentralWidget *m_View;
};

}  // End namespace ICD


#endif // ICDCOLLECTIONDIALOG_H
