<plugin name="ICD" version="0.0.1" compatVersion="0.0.1">
    <vendor>FreeMedForms</vendor>
    <copyright>(C) 2008-2017 by Eric MAEKER, MD</copyright>
    <license>See application's license</license>
    <category>Classifications</category>
    <description>Provides a full ICD10 database and lookup service.</description>
    <url>http://www.freemedforms.com/</url>
    <dependencyList>
      <dependency name="Core" version="0.0.1"/>
      <dependency name="ListView" version="0.0.1"/>
      <dependency name="FormManager" version="0.0.1"/>
    </dependencyList>
</plugin>
