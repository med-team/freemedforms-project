/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PRINTERPREFERENCES_H
#define PRINTERPREFERENCES_H

#include <coreplugin/ioptionspage.h>

#include "ui_printerpreferences.h"

#include <QPointer>
#include <QObject>

/**
 * \file ./plugins/printerplugin/printerpreferences.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ISettings;
}

namespace Print {
namespace Internal {

class PrinterPreferencesWidget : public QWidget, private Ui::PrinterPreferences
{
    Q_OBJECT
    Q_DISABLE_COPY(PrinterPreferencesWidget)

public:
    explicit PrinterPreferencesWidget(QWidget *parent = 0);
    void setDataToUi();

    static void writeDefaultSettings(Core::ISettings *s);

public Q_SLOTS:
    void saveToSettings(Core::ISettings *s = 0);
    void on_selectFolderButton_clicked();

protected:
    virtual void changeEvent(QEvent *e);
};


class PrinterPreferencesPage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    PrinterPreferencesPage(QObject *parent = 0);
    ~PrinterPreferencesPage();

    QString id() const;
    QString displayName() const;
    QString category() const;
    QString title() const;
    int sortIndex() const;

    void resetToDefaults();
    void checkSettingsValidity();
    void apply();
    void finish();

    QString helpPage() {return "parametrer.html";}

    static void writeDefaultSettings(Core::ISettings *s) {PrinterPreferencesWidget::writeDefaultSettings(s);}

    QWidget *createPage(QWidget *parent = 0);
private:
    QPointer<PrinterPreferencesWidget> m_Widget;
};

}  // End Internal
}  // End Printer

#endif // PRINTERPREFERENCES_H
