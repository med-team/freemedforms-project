/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL>                                                       *
 ***************************************************************************/
#ifndef PRINTERPREVIEWER_PRIVATE_H
#define PRINTERPREVIEWER_PRIVATE_H

#include <printerplugin/printer.h>

#include <texteditorplugin/texteditor.h>

#include <QWidget>
#include <QPixmap>

#include "ui_printerpreviewer_p.h"

/**
 * \file ./plugins/printerplugin/printerpreviewer_p.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Print {
namespace Internal {
class PrinterPreviewerPrivate : public PrinterPreviewer, private Ui::PrinterPreviewerPrivate
{
    Q_OBJECT

public:
    explicit PrinterPreviewerPrivate(QWidget *parent = 0);
    ~PrinterPreviewerPrivate() {}

    void initialize();

    QTextEdit *headerEditor() const;
    QTextEdit *footerEditor() const;
    QTextEdit *watermarkEditor() const;

    QComboBox *headerPresenceCombo() const;
    QComboBox *footerPresenceCombo() const;
    QComboBox *watermarkPresenceCombo() const;

    void setHeaderHtml(const QString &html);
    void setFooterHtml(const QString &html);
    void setWatermarkHtml(const QString &html);

    QString headerToHtml() const;
    QString footerToHtml() const;
    QString watermarkToHtml() const;

    void setHeaderPresence(const int presence);
    void setFooterPresence(const int presence);
    void setWatermarkPresence(const int presence);

    int headerPresence() const;
    int footerPresence() const;
    int watermarkPresence() const;

    void setHeader(const TextDocumentExtra *extra);
    void setFooter(const TextDocumentExtra *extra);
    void setWatermark(const TextDocumentExtra *extra);

    void headerToPointer(TextDocumentExtra *extra);
    void footerToPointer(TextDocumentExtra *extra);
    void watermarkToPointer(TextDocumentExtra *extra);


    void setExtraDocument(const QVariant &doc);
    QVariant extraDocument() const;


private Q_SLOTS:
    void on_updatePreviewButton_clicked();
    void on_automaticUpdateCheck_stateChanged( int checkstate );
    void on_duplicataCheck_stateChanged( int state );
    void on_pageNumberSpinBox_valueChanged( int value );
//    void on_tooglePreviewButton_clicked();

private:
    void connectPreview(Editor::TextEditor *t);
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent *e);

private:
    mutable Editor::TextEditor *m_EditorHeader;
    mutable Editor::TextEditor *m_EditorFooter;
    mutable Editor::TextEditor *m_EditorWatermark;
    bool m_AutoCheck;
    Printer printer;
    QPixmap m_PreviewPixmap;
};

}  // End Internal
}  // End Print
#endif // PRINTERPREVIEWER_PRIVATE_H
