/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/


#include "printerplugin.h"
#include "printerpreferences.h"
#include "printcorrectionpreferences.h"
#include "documentprinter.h"

#include <utils/log.h>
#include <utils/global.h>

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/iuser.h>
#include <coreplugin/isettings.h>
#include <coreplugin/translators.h>

#include <QtPlugin>
#include <QtPrintSupport/QPrinterInfo>

#include <QDebug>

using namespace Print;
using namespace Internal;

static inline Core::IUser *user() { return Core::ICore::instance()->user(); }
static inline Core::ISettings *settings() {return Core::ICore::instance()->settings();}

PrinterPlugin::PrinterPlugin() :
    prefPage(0),
    printCorrectionPage(0),
    docPrinter(0)
{
    setObjectName("PrinterPlugin");
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating PrinterPlugin";

    prefPage = new Print::Internal::PrinterPreferencesPage(this);
    addObject(prefPage);
    printCorrectionPage = new Print::Internal::PrintCorrectionPreferencesPage(this);
    addObject(printCorrectionPage);
    docPrinter = new Internal::DocumentPrinter(this);
    addObject(docPrinter);
}

PrinterPlugin::~PrinterPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool PrinterPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "PrinterPlugin::initialize";
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);

    Core::ICore::instance()->translators()->addNewTranslator("plugin_printer");

    return true;
}

void PrinterPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "PrinterPlugin::extensionsInitialized";

    if (!user())
        return;
    if (user()->uuid().isEmpty())
        return;

    prefPage->checkSettingsValidity();
    printCorrectionPage->checkSettingsValidity();
#ifndef FREEMEDFORMS
    settings()->sync();
#endif

    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));

}

ExtensionSystem::IPlugin::ShutdownFlag PrinterPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    if (prefPage) {
        removeObject(prefPage);
        delete prefPage; prefPage=0;
    }
    if (printCorrectionPage) {
        removeObject(printCorrectionPage);
        delete printCorrectionPage; printCorrectionPage = 0;
    }
    if (docPrinter) {
        removeObject(docPrinter);
        delete docPrinter; docPrinter=0;
    }
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(PrinterPlugin)
