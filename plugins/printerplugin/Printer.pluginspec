<plugin name="Printer" version="0.0.1" compatVersion="0.0.1">
    <vendor>FreeMedForms</vendor>
    <copyright>(C) 2008-2017 by Eric MAEKER, MD</copyright>
    <license>See application's license</license>
    <category>Print engine</category>
    <description>Provides printing receipts, documents etc. for FreeMedForms.</description>
    <url>http://www.freemedforms.com/</url>
    <dependencyList>
        <dependency name="Core" version="0.0.1"/>
        <dependency name="TextEditor" version="0.0.1"/>
    </dependencyList>
</plugin>
