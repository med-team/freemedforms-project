/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PRINTERTPLUGIN_H
#define PRINTERTPLUGIN_H

#include <extensionsystem/iplugin.h>

#include <QtCore/QObject>

/**
 * \file ./plugins/printerplugin/printerplugin.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Print {
namespace Internal {
class PrinterPreferencesPage;
class PrintCorrectionPreferencesPage;
class DocumentPrinter;

class PrinterPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.freemedforms.FreeMedForms.PrintPlugin" FILE "Printer.json")

public:
    PrinterPlugin();
    ~PrinterPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

private:
    Print::Internal::PrinterPreferencesPage *prefPage;
    Print::Internal::PrintCorrectionPreferencesPage *printCorrectionPage;
    Internal::DocumentPrinter *docPrinter;
};

}
} // End Print

#endif  // PRINTERTPLUGIN_H
