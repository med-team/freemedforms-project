/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#ifndef PRINTDIALOG_H
#define PRINTDIALOG_H

#include <QDialog>
#include <QList>
#include <QtPrintSupport/QPrinterInfo>

QT_BEGIN_NAMESPACE
class QLabel;
QT_END_NAMESPACE

/**
 * \file ./plugins/printerplugin/printdialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Print {
class Printer;

namespace Internal {
namespace Ui {
class PrintDialog;
}

class PrintDialog : public QDialog
{
    Q_OBJECT
public:
    PrintDialog(QWidget *parent = 0);
    ~PrintDialog();

    void setPrinter(Print::Printer *printer);
    Print::Printer *printer() const;

    void setTwoNUp(bool state);
    bool isTwoNUp() const;

    void setPdfCache(bool state);
    bool isPdfCacheEnabled() const;

    void previewPage(int n);

protected:
    void changeEvent(QEvent *e);

private Q_SLOTS:
    void accept();
    void toFile(QAction *action);
    void on_duplicates_toggled(bool);
    void on_nup_toggled(bool);
    void on_nextButton_clicked();
    void on_prevButton_clicked();
    void on_firstButton_clicked();
    void on_lastButton_clicked();
    void on_pageFrom_valueChanged(int);
    void on_pageTo_valueChanged(int);
    void on_printerCombo_currentIndexChanged(int index);


private:
    Ui::PrintDialog *ui;
    QLabel *m_previewLabel;
    Print::Printer *m_Printer;
    int m_PreviewingPage;
    QList<QPrinterInfo> m_AvailPrinterAtDialogOpens;
    QAction *aSavePdf, *aMailPdf, *aSaveHtml, *aMailHtml;
};

}  // End namespace Internal
}  // End namespace Print

#endif // PRINTDIALOG_H
