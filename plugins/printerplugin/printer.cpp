/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Filipe Azevedo <>                                                 *
 *       Jourdan Romain <>                                                 *
 ***************************************************************************/
#include "printer.h"
#include "printerpreviewer_p.h"
#include "textdocumentextra.h"
#include "constants.h"

#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>

#include <utils/log.h>
#include <utils/global.h>

#include <QApplication>
#include <QPainter>
#include <QPixmap>
#include <QPicture>
#include <QSizeF>
#include <QRectF>
#include <QRect>
#include <QSizeF>
#include <QTextBlock>
#include <QAbstractTextDocumentLayout>
#include <QTextLayout>
#include <QTextFrame>
#include <QTextTable>
#include <QPointer>
#include <QFileInfo>

#include <QtPrintSupport/QPrinterInfo>
#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>
#include <QtPrintSupport/QPrintPreviewDialog>

#include <QTextBrowser>
#include <QDialog>
#include <QGridLayout>
#include <QDialogButtonBox>
#include <QLabel>

#include <math.h>



























static const int FOOTER_BOTTOM_MARGIN = 15;

using namespace Print;
using namespace Print::Internal;

static inline Core::ISettings *settings() {return Core::ICore::instance()->settings();}

namespace Print {
namespace Internal {

class PrinterPrivate
{
public:
    PrinterPrivate(Printer */*parent*/) :
        m_WatermarkPresence(-1),
        m_TwoNUp(false),
        m_Printer(0),
        m_Content(0),
        m_WithDuplicata(false),
        m_PrintingDuplicata(false)  //, q(parent)
    {
        m_TwoNUp = settings()->value(Print::Constants::S_TWONUP).toBool();
    }
    ~PrinterPrivate()
    {
        if (m_Printer)
            delete m_Printer;
        m_Printer=0;
        if (m_Content)
            delete m_Content;
        m_Content = 0;
    }

    QTextDocument *content() { return m_Content; }
    void renewContent()
    {
        if (m_Content) delete m_Content;
        m_Content = 0;
        m_Content = new QTextDocument();
    }

    QList<QTextDocument*> headers(int pageNumber)
    {
        QList<QTextDocument *> list;
        foreach(const TextDocumentExtra *doc, m_Headers) {
            if (presenceIsRequiredAtPage(pageNumber, doc->presence()))
                list << doc->document();
        }
        return list;
    }

    QTextDocument *header(Printer::Presence p) // Returns 0 if there is no header for the presence
    {
        Q_UNUSED(p);
        if (m_Headers.count() < 1)
            return 0;
        return m_Headers.at(0)->document();
    }

    QList<QTextDocument*> footers(int pageNumber)
    {
        QList< QTextDocument *> list;
        foreach(const TextDocumentExtra *doc, m_Footers) {
            if (presenceIsRequiredAtPage(pageNumber, doc->presence()))
                list << doc->document();
        }
        return list;
    }

    QTextDocument *footer(Printer::Presence p) // Returns 0 if there is no footer for the presence
    {
        Q_UNUSED(p);
        if (m_Footers.count() < 1)
            return 0;
        return m_Footers.at(0)->document();
    }

    int pageWidth()
    {
        if (m_Printer)
            return m_Printer->paperRect().width() - 20;
        return 0;
    }

    void setTextWidth(int width)
    {
        if (m_Content)
            m_Content->setTextWidth(width);
        foreach (TextDocumentExtra *doc, m_Headers)
            doc->setTextWidth(width);
        foreach (TextDocumentExtra *doc, m_Footers)
            doc->setTextWidth(width);
    }

    void renewPrinter()
    {
        if (m_Printer) {
            delete m_Printer;
            m_Printer=0;
        }
        m_Printer = new QPrinter;
        m_Printer->setColorMode(QPrinter::ColorMode(settings()->value(Constants::S_COLOR_PRINT).toInt()));
        m_Printer->setPaperSize(QPrinter::A4);
    }

    int complexDrawNewPage(QPainter &p, QSizeF & headerSize, QSizeF & footerSize,
                            QSizeF & pageSize, int & correctedY, QSizeF & drawnedSize,
                            int currentPageNumber);

    static QRectF rotatedBoundingRect(const QRectF &rect, const int rotation)
    {
        QRectF centeredRect = rect.translated(- rect.center());
        QPolygonF polygon(centeredRect);
        QTransform transform;
        transform.rotate(rotation);
        polygon = polygon * transform;
        return polygon.boundingRect().translated(rect.center());
    }
    static double medianAngle(const QRectF & rect)
    {
        double pi = 3.14159265;
        double calculatedTang = rect.height() / rect.width();
        return -atan(calculatedTang) * 180.0 / pi;
    }
    static int calculateWatermarkRotation(const QRectF paperRect, const Qt::Alignment watermarkAlignment)
    {
        int angle = 0;
        if ((watermarkAlignment == (Qt::AlignHCenter | Qt::AlignVCenter)) || (watermarkAlignment == Qt::AlignCenter)) {
            angle = medianAngle(paperRect);
        } else if (watermarkAlignment == Qt::AlignBottom) {
            angle = 0;
        } else if (watermarkAlignment == Qt::AlignTop) {
            angle = 0;
        } else if (watermarkAlignment == Qt::AlignRight) {
            angle = 90;
        } else if (watermarkAlignment == Qt::AlignLeft) {
            angle = 270;
        }
        return angle;
    }
    static void moveTextRect(QRectF & textRect, const QRectF paperRect, const Qt::Alignment watermarkAlignment, const double scaleFactor = 1.0)
    {
        double textHeight  = textRect.height() * scaleFactor;
        if ((watermarkAlignment == (Qt::AlignHCenter | Qt::AlignVCenter)) || (watermarkAlignment == Qt::AlignCenter)) {
            textRect.moveCenter(paperRect.center());
        } else if (watermarkAlignment == Qt::AlignBottom) {
            textRect.moveCenter(QPointF(paperRect.center().x(), (paperRect.height() - (textHeight/2))));
        } else if (watermarkAlignment == Qt::AlignTop) {
            textRect.moveCenter(QPointF(paperRect.center().x(), textHeight/2));
        } else if (watermarkAlignment == Qt::AlignRight) {
            textRect.moveCenter(QPointF(paperRect.width() - (textHeight/2), paperRect.center().y()));
        } else if (watermarkAlignment == Qt::AlignLeft) {
            textRect.moveCenter(QPointF((textHeight/2), paperRect.center().y()));
        }
    }

    bool presenceIsRequiredAtPage(const int page, const int presence)
    {
        if ((presence == Printer::DuplicatesOnly) && (m_PrintingDuplicata))
            return true;
        if (presence == Printer::EachPages)
            return true;
        if ((presence == Printer::OddPages) && ((page % 2) == 1))
            return true;
        if ((presence == Printer::EvenPages) && ((page % 2) == 0))
            return true;
        if ((presence == Printer::FirstPageOnly) && (page==1))
            return true;
        if ((presence == Printer::SecondPageOnly) && (page==2))
            return true;
        if ((presence == Printer::ButFirstPage) && (page!=1))
            return true;
        return false;

        return true;
    }

    bool isSimple() const
    {
        return ((m_Headers.count()==1) && (m_Footers.count()==1));//&& m_Watermark.isNull());
    }

    bool simpleDraw();
    bool simpleDrawPreparePages(QRect &contentRect);

    bool complexDraw();

    QSizeF getSimpleDrawContentPageSize()
    {
        int height = m_Printer->paperRect().height();
        bool footerMargin = false;
        foreach(QTextDocument *doc, headers(1)) {
            height -= doc->size().height();
        }
        foreach(QTextDocument *doc, footers(1)) {
            footerMargin = true;
            height -= doc->size().height();
        }
        if (footerMargin)
            height -= FOOTER_BOTTOM_MARGIN;
        return QSizeF(this->pageWidth(), height);
    }

    QRect getSimpleDrawCurrentRectangle(int pageNumber)
    {
        int headerHeight = 0;
        foreach(QTextDocument *doc, headers(pageNumber)) {
            headerHeight += doc->size().height();
        }
        int footerHeight = FOOTER_BOTTOM_MARGIN;
        foreach(QTextDocument *doc, footers(pageNumber)) {
            footerHeight += doc->size().height();
        }
        if (footerHeight==FOOTER_BOTTOM_MARGIN)
            footerHeight=0;
        int currentHeight = m_Printer->paperRect().height() - headerHeight - footerHeight;
        return QRect(QPoint(0,0), QSize(pageWidth(), currentHeight));
    }

    void simpleDrawHeaderFooter(QPainter &painter,
                                  QSizeF &headerSize, QSizeF &footerSize, const int currentPageNumber)
    {
        int headerHeight = 0;
        painter.save();         // keep page beginning location
        foreach(QTextDocument *doc, headers(currentPageNumber)) {
            headerSize = doc->size();
            headerHeight += doc->size().height();
            QRectF headRect = QRectF(QPoint(0,0), headerSize);
            doc->drawContents(&painter, headRect);
            painter.translate(0, doc->size().height());
        }
        headerSize.setHeight(headerHeight);
        painter.restore();    // return to page beginning

        int footerHeight = FOOTER_BOTTOM_MARGIN;
        foreach(QTextDocument *doc, footers(currentPageNumber)) {
            footerSize = doc->size();
            footerHeight += doc->size().height();
            painter.save();
            painter.translate(0, m_Printer->paperRect().bottom() - footerHeight);
            QRectF footRect = QRectF(QPoint(0,0), QSizeF(doc->size().width(), footerHeight));
            doc->drawContents(&painter, footRect);
            painter.restore();
        }
        if (footerHeight==FOOTER_BOTTOM_MARGIN)
            footerHeight=0;
        footerSize.setHeight(footerHeight);
    }

private:
    void simpleDrawWatermark(QPainter &painter, const int pageNumber)
    {
        if (presenceIsRequiredAtPage(pageNumber, m_WatermarkPresence)) {
            painter.save();
            painter.drawPixmap(m_Printer->paperRect().topLeft(), m_Watermark);
            painter.restore();
        }
    }


    void simpleDrawContent(QPainter &painter, const QSizeF &headerSize, QRect &currentRect, const int drawnedHeight)
    {
        painter.save();
        painter.translate(0, headerSize.height());
        painter.translate(0, -drawnedHeight);
        currentRect.translate(0, drawnedHeight);
        m_Content->drawContents(&painter, currentRect);
        painter.restore();
    }


public:
    QPixmap m_Watermark; // null watermark at constructor time
    int m_WatermarkPresence;
    bool m_TwoNUp;
    QPrinter *m_Printer;
    QList<TextDocumentExtra*> m_Headers;
    QList<TextDocumentExtra*> m_Footers;
    QTextDocument *m_Content;                             // TODO transform to QPointer<QTextDocument> ?
    bool m_WithDuplicata, m_PrintingDuplicata;
    QList<QPicture *> m_Pages;

private:
};

}  // End Internal
}  // End Print


bool PrinterPrivate::complexDraw()
{
    QPainter painter(m_Printer);
    QTextFrame *frame = m_Content->rootFrame();


    int _pageWidth = pageWidth();                     //TODO add margins
    setTextWidth(_pageWidth);

    QSizeF pageSize, headerSize, footerSize, blockSize, actualSize, drawnedSize;
    QRectF lastDrawnedRect, blockRect;
    int correctedY = 0;
    int pageNumber = 0;

    QTextBlock block;
    painter.save();

    QTextFrame::iterator it;

    for (it = frame->begin(); !(it.atEnd()); ++it) {
        QTextFrame *table = qobject_cast<QTextTable*>(it.currentFrame());
        block = it.currentBlock();

        if (table) {
            QRectF tableRect = m_Content->documentLayout()->frameBoundingRect(it.currentFrame());
            painter.drawRect(tableRect);
            painter.drawText(tableRect, QString("\n Tables are not yet supported in complex drawing."));

            if (tableRect.height() + drawnedSize.height() > pageSize.height())
                pageNumber = complexDrawNewPage(painter, headerSize, footerSize, pageSize,
                                                 correctedY, drawnedSize, pageNumber);

            drawnedSize.setHeight(drawnedSize.height() + tableRect.size().height() +
                                   (tableRect.top() - lastDrawnedRect.bottom()));
            lastDrawnedRect = tableRect;

        } else if (block.isValid()) {

            blockRect = m_Content->documentLayout()->blockBoundingRect(block);

            if ((drawnedSize.height() + blockRect.size().height()) > pageSize.height()) {

                QTextLayout *layout = block.layout();
                if (layout->lineCount() > 1) {
                    int heightSave = drawnedSize.height();
                    int i = 0;
                    while (layout->lineAt(i).height() + drawnedSize.height() < pageSize.height()) {
                        drawnedSize.setHeight(drawnedSize.height() + layout->lineAt(i).height());
                        ++i;
                    }
                    drawnedSize.setHeight(heightSave);
                }
                pageNumber = complexDrawNewPage(painter, headerSize, footerSize, pageSize,
                                                 correctedY, drawnedSize, pageNumber);
            }

            block.layout()->draw(&painter, QPointF(0,0));

            drawnedSize.setHeight(drawnedSize.height() + blockRect.size().height() +
                                   (blockRect.top() - lastDrawnedRect.bottom()));
            lastDrawnedRect = blockRect;
        }
    }
    painter.restore();
    painter.end();
    return true;
}

int PrinterPrivate::complexDrawNewPage(QPainter &p, QSizeF & headerSize, QSizeF & footerSize,
                                          QSizeF & pageSize, int & correctedY, QSizeF & drawnedSize,
                                          int currentPageNumber)
{


    if (currentPageNumber != 0) {
        m_Printer->newPage();
        p.restore();
        int previousHeaderHeight = 0;
        foreach(QTextDocument *doc, headers(currentPageNumber)) {
            previousHeaderHeight += doc->size().height();
        }
        p.translate(0, -drawnedSize.height() - previousHeaderHeight);
        correctedY += drawnedSize.height();
        p.save();
    }

    if (presenceIsRequiredAtPage(currentPageNumber+1 , m_WatermarkPresence)) {
        p.save();
        p.translate(0, correctedY);
        p.drawPixmap(m_Printer->paperRect(), m_Watermark);
        p.restore();
    }

    int specialY = correctedY;
    int headerHeight = 0;
    foreach(QTextDocument *doc, headers(currentPageNumber + 1)) {
        headerSize = doc->size();
        p.save();
        p.translate(0, specialY);
        specialY = 0;
        headerHeight += doc->size().height();
        QRectF headRect = QRectF(QPoint(0,0), headerSize);
        doc->drawContents(&p, headRect);
        p.restore();
        p.restore();
        p.translate(0, doc->size().height());
        p.save();
    }
    headerSize.setHeight(headerHeight);


    int footHeight = 0;
    foreach(QTextDocument *doc, footers(currentPageNumber + 1)) {
        footerSize = QSizeF(doc->size().width(),0);
        footHeight += doc->size().height();
        p.save();
        p.translate(0, m_Printer->paperRect().bottom() + correctedY - footHeight - headerSize.height());
        QRectF footRect = QRectF(QPoint(0,0), QSizeF(doc->size().width(), footHeight));
        doc->drawContents(&p, footRect);
        p.restore();
    }
    footerSize.setHeight(footHeight);

    pageSize = QSizeF(pageWidth(),
                       m_Printer->paperRect().height() - headerSize.height() - footerSize.height());

    drawnedSize = QSizeF(0,0);

    return currentPageNumber + 1;
}


bool PrinterPrivate::simpleDraw()
{
    if (!m_Content) {
        LOG_ERROR_FOR("Printer", QCoreApplication::translate("tkPrinter", "No content to preview (simpleDraw)."));
        return false;
    }
    m_PrintingDuplicata = false;
    int _pageWidth = pageWidth();                     //TODO add margins
    this->setTextWidth(_pageWidth);

    m_Content->setPageSize(getSimpleDrawContentPageSize());
    m_Content->setUseDesignMetrics(true);

    QRect contentRect = QRect(QPoint(0,0), m_Content->size().toSize());     // whole document drawing rectangle

    if (!simpleDrawPreparePages(contentRect))
        return false;

    return true;
}


bool PrinterPrivate::simpleDrawPreparePages(QRect &contentRect)
{
    qDeleteAll(m_Pages);
    m_Pages.clear();
    int _pageWidth = contentRect.size().width();
    QSizeF headerSize(_pageWidth, 0);
    QSizeF footerSize(_pageWidth, 0);
    int pageNumber = 1;
    int drawnHeight = 0;
    QRect currentRect = contentRect;
    int fromPage = m_Printer->fromPage();
    int toPage = m_Printer->toPage();
    bool fromToPage = ((fromPage>0) || (toPage>0));
    while (currentRect.intersects(contentRect)) {
        QPicture *pic = new QPicture;
        QPainter painter;
        painter.begin(pic);
        currentRect = getSimpleDrawCurrentRectangle(pageNumber);
        if (fromToPage) {
            if (pageNumber>toPage)
                break;
            if (pageNumber<fromPage) {
                drawnHeight += currentRect.height();
                pageNumber++;
                continue;
            }
        }
        simpleDrawWatermark(painter, pageNumber);
        simpleDrawHeaderFooter(painter, headerSize, footerSize, pageNumber);

        simpleDrawContent(painter, headerSize, currentRect, drawnHeight);

        if (m_WithDuplicata) {
            if (m_PrintingDuplicata) {
                drawnHeight += currentRect.height();
                currentRect.translate(0, currentRect.height());
                pageNumber++;
            }
            m_PrintingDuplicata = !m_PrintingDuplicata;
        } else {
            drawnHeight += currentRect.height();
            currentRect.translate(0, currentRect.height());
            pageNumber++;
        }

        if (currentRect.intersects(contentRect)) {
            if (fromToPage) {
                if ((pageNumber>=fromPage) && (pageNumber<=toPage))
                    m_Printer->newPage();
            } else {
                    m_Printer->newPage();
            }
        }
        m_Pages.append(pic);
    }
    return true;
}

Printer::Printer(QObject * parent)
    : QObject(parent),
    d(0)
{
    setObjectName("Printer");
    d = new PrinterPrivate(this);
    Q_ASSERT(d);
}

Printer::~Printer()
{
    clearHeaders();
    clearFooters();
    if (d) delete d;
    d = 0;
}

void Printer::clearHeaders()
{
    qDeleteAll(d->m_Headers);
    d->m_Headers.clear();
}

void Printer::clearFooters()
{
    qDeleteAll(d->m_Footers);
    d->m_Footers.clear();
}

void Printer::clearWatermark()
{
    d->m_Watermark = QPixmap();
    d->m_WatermarkPresence = Printer::EachPages;
}

bool Printer::getUserPrinter()
{
    delete d->m_Printer;
    d->m_Printer = 0;
    d->m_TwoNUp = settings()->value(Print::Constants::S_TWONUP).toBool();
    const QString &name = settings()->value(Constants::S_DEFAULT_PRINTER).toString();
    if (name.compare("system",Qt::CaseInsensitive)==0 || name.compare("user", Qt::CaseInsensitive)==0) {
        if (QPrinterInfo::defaultPrinter().isNull()) {
            d->m_Printer = new QPrinter;
            d->m_Printer->setResolution(QPrinter::ScreenResolution);
        } else {
            d->m_Printer = new QPrinter(QPrinterInfo::defaultPrinter(), QPrinter::ScreenResolution);
        }
    } else {
        foreach(const QPrinterInfo &info, QPrinterInfo::availablePrinters()) {
            if (info.printerName() == name) {
                d->m_Printer = new QPrinter(info, QPrinter::ScreenResolution);
                break;
            }
        }
    }
    if (d->m_Printer) {
        d->m_Printer->setColorMode(QPrinter::ColorMode(settings()->value(Constants::S_COLOR_PRINT).toInt()));
        d->m_Printer->setPaperSize(QPrinter::A4);
        return true;
    }
    return false;
}


bool Printer::askForPrinter(QWidget *parent)
{
    d->renewPrinter();
    QPrintDialog dialog(d->m_Printer, parent);
    dialog.setWindowTitle(tr("Print document"));
    if (dialog.exec() == QDialog::Accepted)
        return true;
    return false;
}

void Printer::setPrinter(QPrinter *printer)
{
    if (printer) {
        if (d->m_Printer) {
            delete d->m_Printer;
            d->m_Printer = 0;
        }
        d->m_Printer = printer;
    } else {
        d->renewPrinter();
    }
}

QPrinter *Printer::printer()
{
    return d->m_Printer;
}


void Printer::setHeader(const QString & html, Presence presence, Printer::Priority prior)
{
    d->m_Headers.append(new TextDocumentExtra(html, presence, prior));
}


void Printer::setFooter(const QString & html, Presence presence, Printer::Priority prior)
{
    d->m_Footers.append(new TextDocumentExtra(html, presence, prior));
}

void Printer::setContent(const QString & html)
{
    d->renewContent();
    d->content()->setHtml(html);
}

void Printer::setContent(const QTextDocument & docToPrint)
{
    d->renewContent();
    d->m_Content = docToPrint.clone();
}

QString Printer::htmlContent() const
{
    return d->m_Content->toHtml();
}

QString Printer::toHtml() const
{
    QString html, content, css, header, body;
    QTextDocument *doc = 0;
    doc = d->header(EachPages);
    if (doc) {
        content = doc->toHtml("UTF-8");
        css = Utils::htmlTakeAllCssContent(content);
        content = Utils::htmlBodyContent(content);
        content.prepend(css);
        html += content;
    }
    QTextDocument *doc2 = 0;
    doc2 = d->header(FirstPageOnly);
    if (doc2 && doc2 != doc) {
        content = doc->toHtml("UTF-8");
        css = Utils::htmlTakeAllCssContent(content);
        content = Utils::htmlBodyContent(content);
        content.prepend(css);
        html += content;
    }
    content = d->m_Content->toHtml("UTF-8");
    css = Utils::htmlTakeAllCssContent(content);
    content = Utils::htmlBodyContent(content);
    content.prepend(css);
    html += content;
    doc = d->footer(EachPages);
    if (doc) {
        content = doc->toHtml("UTF-8");
        css = Utils::htmlTakeAllCssContent(content);
        content = Utils::htmlBodyContent(content);
        content.prepend(css);
        html += content;
    }
    css = Utils::htmlTakeAllCssContent(html);
    content = Utils::htmlBodyContent(html);
    body = QString("<body>%1</body>").arg(content);
    header = QString("<head><meta charset=\"UTF-8\"><title></title>%1</head>").arg(css);
    html = body.prepend(header);
    html = QString("<!DOCTYPE html><html>%1</html>").arg(html);
    return html;
}

bool Printer::useDefaultPrinter()
{
    QPrinterInfo def = QPrinterInfo::defaultPrinter();
    if (def.isNull())
        return false;
    if (d->m_Printer) {
        delete d->m_Printer;
        d->m_Printer = 0;
    }
    d->m_Printer = new QPrinter(def, QPrinter::ScreenResolution);
    return true;
}

void Printer::setOrientation(QPrinter::Orientation orientation)
{
    if (!d->m_Printer)
        d->renewPrinter();
    d->m_Printer->setOrientation(orientation);
    d->setTextWidth(d->pageWidth());
}

void Printer::setPaperSize(QPrinter::PaperSize size)
{
    if (!d->m_Printer)
        d->renewPrinter();
    d->m_Printer->setPaperSize(size);
    d->setTextWidth(d->pageWidth());
}

void Printer::setTwoNUp(bool state)
{
    d->m_TwoNUp = state;
}

bool Printer::isTwoNUp() const
{
    return d->m_TwoNUp;
}

bool Printer::preparePages()
{
    if (!d->m_Printer)
        return false;

    if (d->isSimple()) {
        LOG("Printing using simpleDraw method.");
        return d->simpleDraw();
    }
    else {
        LOG("WARNING: Printing using complexDraw method (should be buggy).");
        return d->complexDraw();
    }
}

QList<QPicture *> Printer::pages() const
{
    return d->m_Pages;
}


bool Printer::previewDialog(QWidget *parent, bool test)
{
    if (!d->m_Printer)
        d->m_Printer = new QPrinter(QPrinter::ScreenResolution);
    Q_UNUSED(test);
    preparePages();
    QPrintPreviewDialog dialog(d->m_Printer, parent, Qt::Window | Qt::CustomizeWindowHint | Qt::WindowSystemMenuHint | Qt::WindowCloseButtonHint | Qt::WindowMinMaxButtonsHint);
    connect(&dialog, SIGNAL(paintRequested(QPrinter *)), this, SLOT(print(QPrinter *)));
    dialog.exec();
    return true;
}


bool Printer::print(const QTextDocument &docToPrint)
{
    d->renewContent();
    d->m_Content = docToPrint.clone();
    print(d->m_Printer);
    return true;
}


bool Printer::print(const QString &htmlToPrint)
{
    QTextDocument t;
    t.setHtml(htmlToPrint);
    return print(t);
}

bool Printer::reprint(QPrinter *printer)
{
    if (!printer)
        return false;
    if (!printer->isValid())
        return false;

    if (d->m_TwoNUp)
        printer->setOrientation(QPrinter::Landscape);
    else
        printer->setOrientation(QPrinter::Portrait);

    QPainter print;
    print.begin(printer);
    int from;
    int to;
    if (printer->printRange()==QPrinter::PageRange) {
        from = printer->fromPage();
        to = printer->toPage();
    } else {
        from = 1;
        to = d->m_Pages.count();
    }


    while (from <= to) {
        pageToPainter(&print, from, d->m_TwoNUp, false);
        d->m_TwoNUp ? from += 2 : ++from;
        if (from <= to)
            if (!printer->newPage())
                return false;
    }
    print.end();
    return true;
}


void Printer::setPrintWithDuplicata(bool state)
{
    d->m_WithDuplicata=state;
}

bool Printer::printWithDuplicatas() const
{
    return d->m_WithDuplicata;
}


bool Printer::print(QPrinter *printer)
{
    if (!printer)
        printer = d->m_Printer;

    if (!printer)
        return false;

    reprint(d->m_Printer);
    return true;
}

void Printer::addPixmapWatermark(const QPixmap & pix, const Presence p , const Qt::AlignmentFlag watermarkAlignment)
{
    if (! d->m_Printer)
        return;
    d->m_WatermarkPresence = p;
    QRectF paperRect = d->m_Printer->paperRect();

    if (d->m_Watermark.isNull()) {
        d->m_Watermark = QPixmap(paperRect.width(), paperRect.height());
        d->m_Watermark.fill();
    }

    QRectF pixRect = pix.rect();
    int rotationAngle = d->calculateWatermarkRotation(paperRect, watermarkAlignment);

    QPainter painter;
    painter.begin(&d->m_Watermark);
    painter.translate(-paperRect.topLeft());  // TODO : this is wrong because we loose the margins
    painter.save();
    if (rotationAngle != 0) {
        painter.translate(pixRect.center());
        painter.rotate(rotationAngle);
        QRectF boundingRect = d->rotatedBoundingRect(pixRect, rotationAngle);
        double scale = qMin(paperRect.width() / boundingRect.width(), paperRect.height() / boundingRect.height());
        painter.scale(scale, scale);
        painter.translate(-pixRect.center());
    }
    painter.drawRect(pixRect);
    painter.drawPixmap(pixRect, pix, QRectF());
    painter.restore();
    painter.end();
}

void Printer::addHtmlWatermark(const QString & html,
                                  const Presence p,
                                  const Qt::Alignment watermarkAlignment,
                                  const int orientation)
{
    if (! d->m_Printer)
        return;
    d->m_WatermarkPresence = p;


    QRectF paperRect = d->m_Printer->paperRect();

    d->m_Watermark = QPixmap(paperRect.width(), paperRect.height());
    d->m_Watermark.fill();
    previewHtmlWatermark(d->m_Watermark, html, p, watermarkAlignment, orientation);
}


void Printer::previewDocumentWatermark(QPixmap &drawTo,
                                          QTextDocument *doc,
                                          const Presence p,
                                          const Qt::Alignment watermarkAlignment,
                                          const int orientation)
{
    Q_UNUSED(p);
    Q_UNUSED(orientation);
    QSizeF docSizeSave = doc->size();
    QTextOption docOptionSave = doc->defaultTextOption();
    QTextOption opt;
    opt.setWrapMode(QTextOption::NoWrap);
    doc->setDefaultTextOption(opt);
    doc->adjustSize();
    QPointF pageCenter(drawTo.rect().center());
    QRect paperRect = drawTo.rect();
    QRectF textRect = QRectF(QPointF(0,0), doc->size());
    int rotationAngle = PrinterPrivate::calculateWatermarkRotation(paperRect, watermarkAlignment);

    QPainter painter;
    painter.begin(&drawTo);
    painter.translate(-paperRect.topLeft());  // TODO : this is wrong because we loose the margins
    painter.save();
    {
        QRectF boundingRect = PrinterPrivate::rotatedBoundingRect(textRect, rotationAngle);
        double scale = qMin(paperRect.width() / boundingRect.width(), paperRect.height() / boundingRect.height());
        PrinterPrivate::moveTextRect(textRect, paperRect, watermarkAlignment, scale);
        painter.translate(textRect.center());
        painter.rotate(rotationAngle);
        painter.scale(scale, scale);
        painter.translate(-textRect.center());
    }

    painter.translate(textRect.topLeft());
    doc->drawContents(&painter);//, textRect);
    painter.translate(-textRect.topLeft());

    painter.restore();
    painter.end();
    doc->setDefaultTextOption(docOptionSave);
    doc->setPageSize(docSizeSave);
}

void Printer::previewHtmlWatermark(QPixmap &drawTo,
                                      const QString & html,
                                      const Presence p,
                                      const Qt::Alignment watermarkAlignment,
                                      const int orientation)
{
    QTextDocument wm;
    wm.setHtml(html);
    previewDocumentWatermark(drawTo, &wm, p, watermarkAlignment, orientation);
}

void Printer::previewTextWatermark(QPixmap &drawTo,
                                      const QString & plainText,
                                      const Presence p,
                                      const Qt::Alignment watermarkAlignment,
                                      const int orientation)
{
    QTextDocument wm;
    wm.setPlainText(plainText);
    previewDocumentWatermark(drawTo, &wm, p, watermarkAlignment, orientation);
}

void Printer::addTextWatermark(const QString & plainText,
                                  const Presence p,
                                  const Qt::Alignment watermarkAlignment,
                                  const Qt::Alignment textAlignment,
                                  const QFont & font,
                                  const QColor & color,
                                  const int orientation)
{
    if (! d->m_Printer)
        return;
    d->m_WatermarkPresence = p;


    QRectF paperRect = d->m_Printer->paperRect();

    if (d->m_Watermark.isNull()) {
        d->m_Watermark = QPixmap(paperRect.width(), paperRect.height());
        d->m_Watermark.fill();
    }

    QString html = QString("<html><body><p %1 style=\"%2\">%3</p></body></html>")
                   .arg(Utils::textAlignmentToHtml(textAlignment))
                   .arg(Utils::fontToHtml(font, color))
                   .arg(plainText);
    html.replace("\n", "<br/>");
    previewHtmlWatermark(d->m_Watermark, html, p, watermarkAlignment, orientation);
}


PrinterPreviewer *Printer::previewer(QWidget *parent)
{
    PrinterPreviewerPrivate *prev= new PrinterPreviewerPrivate(parent);
    prev->initialize();
    return prev;
}


void Printer::previewToPixmap(QPixmap &drawTo, QPrinter *printer)
{
    Q_ASSERT(printer);
    if (!d->m_Content) {
        d->m_Content = new QTextDocument(this);
        d->m_Content->setHtml("<p>This is the previewing mode</p><p&nbsp;</p><p>This is the previewing mode</p><p&nbsp;</p><p>This is the previewing mode</p><p&nbsp;</p>");
    }
    d->m_PrintingDuplicata = false;

    int _pageWidth = printer->paperRect().width();//d->pageWidth();
    d->setTextWidth(_pageWidth);

    d->m_Content->setPageSize(printer->paperRect().size());//d->getSimpleDrawContentPageSize());
    d->m_Content->setUseDesignMetrics(true);


    drawTo = QPixmap(_pageWidth, printer->paperRect().height() + 30);
    drawTo.fill();
    QPainter painter;
    painter.begin(&drawTo);

    drawTo = drawTo.scaled(250, 250, Qt::KeepAspectRatio, Qt::SmoothTransformation);
}

void Printer::previewHeaderFooter(QPixmap &drawTo,
                                     const QString &headerHtml,
                                     const QString &footerHtml)
{
    QPrinter *printer = new QPrinter;
    printer->setPaperSize(QPrinter::A4);
    setPrinter(printer);

    setHeader(headerHtml);
    setFooter(footerHtml);
    setContent("<html><body><p>xxxxxx xx xxxxx xxx xxxxx xxx xx xxxx<br />xxxxx xxxx xx x xxxxx xx xxxxxx x x x xxx x</p></body></html>");

    int _pageWidth = d->pageWidth();                     //TODO add margins
    QSizeF headerSize(_pageWidth, 0);
    QSizeF footerSize(_pageWidth, 0);

    QTextDocument *headerDoc = d->header(Printer::EachPages);
    if (headerDoc) {
        headerDoc->setTextWidth(_pageWidth);
        headerSize.setHeight(headerDoc->size().height());
    }
    QTextDocument *footerDoc = d->footer(Printer::EachPages);
    if (footerDoc) {
        footerDoc->setTextWidth(_pageWidth);
        footerSize.setHeight(footerDoc->size().height());
    }

    drawTo = QPixmap(_pageWidth, printer->paperRect().height());
    drawTo.fill();
    QPainter painter;
    painter.begin(&drawTo);
    d->simpleDrawHeaderFooter(painter, headerSize, footerSize, 1);
    painter.end();
    drawTo = drawTo.scaled(250, 250, Qt::KeepAspectRatio, Qt::SmoothTransformation);
}


bool Printer::pageToPainter(QPainter *paint, const int pageNumber, bool twoNUp, bool pixmapPreview)
{
    if (!d->m_Printer)
        return false;

    if (pageNumber>d->m_Pages.count())
        return false;

    if (pageNumber<=0)
        return false;

    const QSizeF &paperSize = d->m_Printer->paperRect(QPrinter::DevicePixel).size();
    const QSizeF &pageSize = d->m_Printer->pageRect(QPrinter::DevicePixel).size();

    if (paint)
        paint->save();

    if (!twoNUp) {
        QPicture *page = d->m_Pages.at(pageNumber-1);
        if (!page) {
            paint->restore();
            return false;
        }
        paint->scale(0.95, 0.95); // here is the pseudo margins management
        if (pixmapPreview)
            paint->translate((paperSize.width() - pageSize.width())/2.0, (paperSize.height()-pageSize.height())/2.0);
        page->play(paint);
    } else {
        QPicture *page1 = d->m_Pages.at(pageNumber-1);

        QSizeF pageS = pageSize;
        QSizeF paperS = paperSize;
        if (pageSize.width() > pageSize.height()) {
            pageS = QSizeF(pageSize.height(), pageSize.width());
        }
        if (paperSize.width() > paperSize.height()) {
            paperS = QSizeF(paperSize.height(), paperSize.width());
        }

        if (!page1) {
            paint->restore();
            return false;
        }
        QPicture *page2 = 0;
        if (pageNumber < d->m_Pages.count())
            page2 = d->m_Pages.at(pageNumber);

        paint->scale(0.7, 0.68);
        if (pixmapPreview)
            paint->translate((paperS.height() - pageS.height())/2.0, (paperS.width()-pageS.width())/2.0);  // here is the pseudo margins management
        page1->play(paint);

        if (page2) {
            if (pixmapPreview)
                paint->translate(pageSize.width()+(paperSize.width() - pageSize.width())/2.0, 0);  // here is the pseudo margins management
            else
                paint->translate(pageS.width() + ((paperS.height() - pageS.height())/2.0), 0);
            page2->play(paint);
        }
    }
    paint->restore();
    return true;
}


bool Printer::toPdf(const QString &fileName, const QString &docName)
{
    Q_ASSERT(!fileName.isEmpty());
    if (fileName.isEmpty())
        return false;
    QString tmp = fileName;
    if (QFileInfo(tmp).suffix().isEmpty())
        tmp.append(".pdf");

    d->m_Printer->setResolution(QPrinter::HighResolution);

    QPrinter::OutputFormat format = d->m_Printer->outputFormat();
    #ifdef Q_OS_MAC                                                             
    d->m_Printer->setOutputFormat(QPrinter::PdfFormat);                           
    #else
    d->m_Printer->setOutputFormat(QPrinter::PdfFormat);
    #endif

    d->m_Printer->setCreator(qApp->applicationName() + " " + qApp->applicationVersion());
    d->m_Printer->setOutputFileName(tmp);
    d->m_Printer->setDocName(docName);
    bool ok = reprint(d->m_Printer);
    d->m_Printer->setOutputFormat(format);
    return ok;
}


bool Printer::toPdf(const QString &fileName, const QTextDocument &docToPrint)
{
    d->m_Content->setHtml(docToPrint.toHtml("UTF-8"));
    return toPdf(fileName, "");
}
