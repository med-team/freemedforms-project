/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef XMLFORMNAME_H
#define XMLFORMNAME_H

#include <QString>
#include <QMultiHash>

/**
 * \file ./plugins/xmlioplugin/xmlformname.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/


namespace XmlForms {
namespace Internal {

struct XmlFormName {
    XmlFormName();
    XmlFormName(const QString &uid);

    bool operator==(const XmlFormName &other) const;

    bool isValid, isAvailableFromDatabase;
    QString uid, absFileName, absPath, modeName;
    QString fileVersion, databaseVersion;
    QMultiHash<int, QString> databaseAvailableContents;
    int dbId;
};

QDebug operator<<(QDebug dbg, const XmlFormName &c);

}
}


#endif // XMLFORMNAME_H
