/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef XMLIO_IPLUGIN_H
#define XMLIO_IPLUGIN_H

#include <extensionsystem/iplugin.h>

#include <QtCore/QObject>

/**
 * \file ./plugins/xmlioplugin/xmlioplugin.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace XmlForms {
namespace Internal {
class XmlFormIO;
class XmlFormContentReader;

class XmlFormIOPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.freemedforms.FreeMedForms.XmlIOPlugin" FILE "XmlIO.json")

public:
    XmlFormIOPlugin();
    ~XmlFormIOPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

private Q_SLOTS:
    void showDatabaseInformation();

#ifdef WITH_TESTS
    void initTestCase();
    void test_XmlFormName();
    void cleanupTestCase();
#endif

private:
    Internal::XmlFormContentReader *m_XmlReader;
    Internal::XmlFormIO *m_FormIo;
};

}  // namespace Internal
}  // namespace XmlForms

#endif  // End XMLIO_IPLUGIN_H
