<plugin name="XmlIO" version="1.0.0" compatVersion="1.0.0">
    <vendor>FreeMedForms</vendor>
    <copyright>(C) 2008-2017 by Eric MAEKER, MD</copyright>
    <license>See application's license</license>
    <description>The XML form loader/saver for FreeMedForms.</description>
    <category>Patient data</category>
    <url>https://freemedforms.com/</url>
    <dependencyList>
      <dependency name="Core" version="1.0.0"/>
      <dependency name="FormManager" version="1.0.0"/>
      <dependency name="Category" version="1.0.0"/>
      <dependency name="PMH" version="1.0.0"/>
      <dependency name="Alert" version="1.0.0"/>
    </dependencyList>
</plugin>
