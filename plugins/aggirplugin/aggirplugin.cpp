/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "aggirplugin.h"
#include "girwidget.h"

#include <coreplugin/dialogs/pluginaboutpage.h>

#include <utils/log.h>

#include <QtPlugin>

#include <QDebug>

using namespace Gir;
using namespace Gir::Internal;

GirPlugin::GirPlugin() : m_Factory(0)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating GirPlugin";
}

GirPlugin::~GirPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
}

bool GirPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "GirPlugin::initialize";

    Q_UNUSED(arguments);
    Q_UNUSED(errorString);
    m_Factory = new GirWidgetFactory(this);
    addObject(m_Factory);
    return true;
}

void GirPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "GirPlugin::extensionsInitialized";

        addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));
}

ExtensionSystem::IPlugin::ShutdownFlag GirPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    if (m_Factory){
        removeObject(m_Factory);
        delete m_Factory;
        m_Factory = 0;
    }
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(GirPlugin)
