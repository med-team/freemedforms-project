#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

TEMPLATE = lib
TARGET = Gir

DEFINES += AGGIR_LIBRARY

BUILD_PATH_POSTFIXE = FreeMedForms

include(../fmf_plugins.pri)
include( aggirplugin_dependencies.pri )
HEADERS = \
    aggirplugin.h \
    aggir_exporter.h \
    girwidget.h \
    girmodel.h

SOURCES = \
    aggirplugin.cpp \
    girwidget.cpp \
    girmodel.cpp

FORMS += girwidget.ui

OTHER_FILES = Gir.pluginspec

# include translations
TRANSLATION_NAME = aggir
include($${SOURCES_ROOT_PATH}/buildspecs/translations.pri)
