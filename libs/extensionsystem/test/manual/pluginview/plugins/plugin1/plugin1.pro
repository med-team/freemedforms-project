#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

TEMPLATE = lib
TARGET = plugin1

SOURCES += plugin1.cpp
HEADERS += plugin1.h

RELATIVEPATH = ../../../..
include(../../../../extensionsystem_test.pri)

LIBS += -L$${OUT_PWD}/../plugin2 -L$${OUT_PWD}/../plugin3 -lplugin2 -lplugin3

macx {
} else:unix {
    QMAKE_RPATHDIR += $${OUT_PWD}/../plugin2
    QMAKE_RPATHDIR += $${OUT_PWD}/../plugin3
}
