/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATAPACK_PACKMANAGER_H
#define DATAPACK_PACKMANAGER_H

#include <datapackutils/ipackmanager.h>

#include <QVector>
#include <QList>

/**
 * \file ./libs/datapackutils/packmanager.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DataPack {
class IServerEngine;

namespace Internal {

class PackManager : public IPackManager
{
    Q_OBJECT
public:
    explicit PackManager(QObject *parent = 0);
    ~PackManager();

    bool init(const QVector<DataPack::IServerEngine*> &engines);
    void checkInstalledPacks();

    QList<Pack> installedPack(bool forceRefresh = false);
    bool isDataPackInstalled(const Pack &pack);
    bool isDataPackInstalled(const QString &packUid, const QString &packVersion = QString::null);

    bool isPackInPersistentCache(const Pack &pack);

    bool downloadPack(const Pack &pack, QProgressBar *bar);
    bool checkCachedPackFileIntegrity(const Pack &pack);
    bool installDownloadedPack(const Pack &pack, QProgressBar *bar = 0);
    bool removePack(const Pack &pack);

private Q_SLOTS:
    void packDownloadDone(const DataPack::Pack &pack, const DataPack::ServerEngineStatus &status);

private:
    QList<Pack> m_InstalledPacks;
    QVector<DataPack::IServerEngine*> m_Engines;
    QStringList m_Msg, m_Errors;
};

} // namespace Internal
} // namespace DataPack

#endif // DATAPACK_PACKMANAGER_H
