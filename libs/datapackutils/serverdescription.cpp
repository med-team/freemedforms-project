/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "serverdescription.h"

#include <QDebug>

using namespace DataPack;

ServerDescription::ServerDescription() :
    Utils::GenericDescription("ServerDescription")
{
    addNonTranslatableExtraData(RecommendedUpdateFrequency, "RecomUpFreq");
    addNonTranslatableExtraData(RequiereAuthentification, "RequiereAuth");
    setData(RequiereAuthentification, false);
}

ServerDescription::~ServerDescription()
{
}

QString ServerDescription::toXml() const
{
    QString xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            "<!DOCTYPE FreeMedForms>\n"
            "<DataPackServer>\n";
    xml += Utils::GenericDescription::toXml();
    xml += "</DataPackServer>\n";
    return xml;
}
