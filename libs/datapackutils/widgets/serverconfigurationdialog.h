/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATAPACK_SERVERCONFIGURATIONDIALOG_H
#define DATAPACK_SERVERCONFIGURATIONDIALOG_H

#include <datapackutils/datapack_exporter.h>

#include <QDialog>

namespace DataPack {
class Server;

namespace Internal {
namespace Ui {
    class ServerConfigurationDialog;
}
}  // End namespace Internal

class DATAPACK_EXPORT ServerConfigurationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ServerConfigurationDialog(QWidget *parent = 0);
    ~ServerConfigurationDialog();

    void setServer(const Server &server);

public Q_SLOTS:
    bool submitTo(Server *server);

private Q_SLOTS:
    void on_serverType_currentIndexChanged(int index);
    void on_selectPath_clicked();

private:
    Internal::Ui::ServerConfigurationDialog *ui;
};

} // namespace DataPack

#endif // DATAPACK_SERVERCONFIGURATIONDIALOG_H
