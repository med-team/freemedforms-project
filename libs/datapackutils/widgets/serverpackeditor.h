/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATAPACK_SERVERPACKEDITOR_H
#define DATAPACK_SERVERPACKEDITOR_H

#include <datapackutils/datapack_exporter.h>

#include <QWidget>
class QStandardItemModel;
class QModelIndex;
class QToolBar;
class QListWidgetItem;
class QDataWidgetMapper;

/**
 * \file ./libs/datapackutils/widgets/serverpackeditor.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DataPack {
class PackDescription;
class Pack;
class PackModel;
class ServerModel;
namespace Internal {
class ServerPackEditorPrivate;
namespace Ui {
    class ServerPackEditor;
} // Ui
} // Internal

class DATAPACK_EXPORT ServerPackEditor : public QWidget
{
    Q_OBJECT
public:
    explicit ServerPackEditor(QWidget *parent = 0);
    ~ServerPackEditor();

public Q_SLOTS:
    bool refreshServerContent();
    bool submitChanges();

private Q_SLOTS:
    void onRefreshServerDone();

private:
    void populatePackView(const int packId);
    void populateServerView(const int serverId);

private Q_SLOTS:
    void switchToPackView();
    void switchToServerView();
    void onPackCategoriesChanged(const QModelIndex &index, const QModelIndex &previous);
    void onPackIndexActivated(const QModelIndex &index, const QModelIndex &previous);
    void serverActionTriggered(QAction *a);
    void refreshPacks();
    void processPacks();
    void serverCurrentChanged(const QModelIndex &c, const QModelIndex &p);
    void selectFirstRow();

private:
    void retranslate();
    void changeEvent(QEvent *e);

private:
    Internal::ServerPackEditorPrivate *d;
};

}  // End namespace DataPack

#endif // DATAPACK_SERVERPACKEDITOR_H
