/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "servercontent.h"

#include <utils/log.h>
#include <translationutils/constants.h>
#include <translationutils/trans_msgerror.h>

#include <QDomDocument>
#include <QDomElement>

using namespace DataPack;
using namespace Trans::ConstantTranslations;


namespace {
const char * const TAG_SERVERCONTENT = "ServerContents";
const char * const TAG_PACK = "Pack";
const char * const ATTRIB_FILENAME = "serverFileName";
}

ServerContent::ServerContent()
{
}

void ServerContent::clear()
{
    m_PackFileNames.clear();
}

bool ServerContent::isEmpty() const
{
    return m_PackFileNames.isEmpty();
}


bool ServerContent::addPackRelativeFileName(const QString &fileName)
{
    if (m_PackFileNames.contains(fileName))
        return false;
    m_PackFileNames.append(fileName);
    return true;
}

bool ServerContent::fromXml(const QString &xml)
{
    m_PackFileNames.clear();
    QDomDocument doc;
    QString error;
    int line, col;
    if (!doc.setContent(xml, &error, &line, &col)) {
        LOG_ERROR_FOR("DataPack::Pack", tkTr(Trans::Constants::ERROR_1_LINE_2_COLUMN_3).arg(error).arg(line).arg(col));
        return false;
    }
    QDomElement root = doc.firstChildElement(::TAG_SERVERCONTENT);
    return fromDomElement(root);
}


bool ServerContent::fromDomElement(const QDomElement &root)
{
    if (root.tagName().compare(::TAG_SERVERCONTENT, Qt::CaseInsensitive)!=0) {
        LOG_ERROR_FOR("ServerContent", "Wrong XML. No root tag: " + QString(::TAG_SERVERCONTENT));
        return false;
    }

    QDomElement pack = root.firstChildElement(::TAG_PACK);

    while (!pack.isNull()) {
        m_PackFileNames << pack.attribute(::ATTRIB_FILENAME);
        pack = pack.nextSiblingElement(::TAG_PACK);
    }
    return true;
}

bool ServerContent::toXml(QDomElement *root, QDomDocument *doc) const
{
    QDomElement content = doc->createElement(::TAG_SERVERCONTENT);
    if (!root)
        doc->appendChild(content);
    else
        root->appendChild(content);

    foreach(const QString &pack, m_PackFileNames) {
        QDomElement p = doc->createElement(::TAG_PACK);
        p.setAttribute(::ATTRIB_FILENAME, pack);
        content.appendChild(p);
    }
    return true;
}


QString ServerContent::toXml() const
{
    QDomDocument doc;
    if (!toXml(0, &doc)) {
        LOG_ERROR_FOR("ServerContent", "Wrong XML");
        return QString::null;
    }
    return doc.toString(2);
}
