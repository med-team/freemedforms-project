/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef SERVERCONTENTS_H
#define SERVERCONTENTS_H

#include <datapackutils/datapack_exporter.h>

#include <QUrl>
#include <QStringList>

class QDomElement;
class QDomDocument;

/**
 * \file ./libs/datapackutils/servercontent.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DataPack {

class DATAPACK_EXPORT ServerContent
{
public:
    ServerContent();

    void clear();
    bool isEmpty() const;

    const QStringList &packDescriptionFileNames() const {return m_PackFileNames;}
    bool addPackRelativeFileName(const QString &fileName);

    bool fromXml(const QString &xml);
    bool fromDomElement(const QDomElement &root);
    bool toXml(QDomElement *root, QDomDocument *doc) const;
    QString toXml() const;

private:
    QStringList m_PackFileNames;
};

}  // End namespace DataPack

#endif // SERVERCONTENTS_H
