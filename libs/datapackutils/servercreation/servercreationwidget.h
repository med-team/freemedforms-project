/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATAPACK_LIB_SERVERCREATIONWIDGET_H
#define DATAPACK_LIB_SERVERCREATIONWIDGET_H

#include <QWidget>

/**
 * \file ./libs/datapackutils/servercreation/servercreationwidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DataPack {
namespace Internal {
class ServerCreationWidgetPrivate;
} // namespace Internal

class ServerCreationWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ServerCreationWidget(QWidget *parent = 0);
    ~ServerCreationWidget();

    void setDefaultScreeningPath(const QString &absPath);
    bool setDefaultPathForServerDescriptionFiles(const QString &absPath);

    bool addScreeningPath(const QString &absPath);
    int numberOfCheckedPacks() const;

    void setDefaultServerOutputPath(const QString &absPath);

public Q_SLOTS:
    bool onCreateServerRequested();

private Q_SLOTS:
    bool onAddScreeningPathButtonClicked();
    void updateTotalNumberOfPacks();

private:
    void retranslate();
    void changeEvent(QEvent *e);

private:
    Internal::ServerCreationWidgetPrivate *d;
};

} // namespace DataPack

#endif // DATAPACK_LIB_SERVERCREATIONWIDGET_H

