/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developers : Eric Maeker                                          *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "packcreationmodel.h"
#include "packcreationqueue.h"
#include <datapackutils/pack.h>
#include <datapackutils/constants.h>

#include <utils/log.h>
#include <utils/global.h>
#include <translationutils/constants.h>
#include <translationutils/trans_current.h>
#include <translationutils/trans_msgerror.h>
#include <translationutils/trans_filepathxml.h>

#include <QDir>

using namespace DataPack;
using namespace Internal;
using namespace Trans::ConstantTranslations;

namespace DataPack {
namespace Internal {
class PackCreationModelPrivate {
public:
    PackCreationModelPrivate(PackCreationModel *parent) :
        _format(PackCreationModel::ShowByQueue),
        q(parent)
    {}

    ~PackCreationModelPrivate()
    {}

    void initialize()
    {
        q->clear();
        QStandardItem *root = new QStandardItem(tkTr(Trans::Constants::NO_ITEM));
        q->invisibleRootItem()->appendRow(root);
        q->setColumnCount(1);
    }

    void clearModelAndCache()
    {
        q->clear();
        _serversUidToItem.clear();
        _insertedPackCreationQueueUids.clear();
        _packDescriptionFilesIncluded.clear();
        _packItems.clear();
        _queues.clear();
    }

    QStandardItem *serverItem(const QString &uid)
    {
        return _serversUidToItem.value(uid, 0);
    }

    void addServerItem(const QString &uid, QStandardItem *item)
    {
        _serversUidToItem.insert(uid, item);
    }



    bool isPackCreationQueueAlreadyInserted(const PackCreationQueue &queue)
    {
        return _insertedPackCreationQueueUids.keys().contains(queue.uid());
    }

    void insertPackCreationQueueInCache(const PackCreationQueue &queue, QStandardItem *item)
    {
        _insertedPackCreationQueueUids.insert(queue.uid(), item);
    }

    QStandardItem *packToItem(const QString &absPathToDescriptionFile, const PackCreationQueue &queue)
    {
        DataPack::Pack pack;
        pack.fromXmlFile(absPathToDescriptionFile);

        QStandardItem *packItem = new QStandardItem(pack.name());
        QFont bold;
        bold.setBold(true);
        packItem->setFont(bold);
        packItem->setCheckable(true);
        packItem->setCheckState(Qt::Checked);

        QStandardItem *item = 0;
        item = new QStandardItem(tkTr(Trans::Constants::_1_COLON_2)
                                 .arg(tkTr(Trans::Constants::TYPE))
                                 .arg(pack.dataTypeName()));
        item->setToolTip(item->text());
        packItem->appendRow(item);
        item = new QStandardItem(tkTr(Trans::Constants::_1_COLON_2)
                                 .arg(tkTr(Trans::Constants::LICENSE))
                                 .arg(pack.description().data(PackDescription::LicenseName).toString()));
        item->setToolTip(item->text());
        packItem->appendRow(item);
        item = new QStandardItem(tkTr(Trans::Constants::_1_COLON_2)
                                 .arg(tkTr(Trans::Constants::VERSION))
                                 .arg(pack.version()));
        item->setToolTip(item->text());
        packItem->appendRow(item);
        item = new QStandardItem(tkTr(Trans::Constants::_1_COLON_2)
                                 .arg(tkTr(Trans::Constants::VENDOR))
                                 .arg(pack.vendor()));
        item->setToolTip(item->text());
        packItem->appendRow(item);
        item = new QStandardItem(tkTr(Trans::Constants::_1_COLON_2)
                                 .arg(tkTr(Trans::Constants::CREATION_DATE_TIME))
                                 .arg(QLocale().toString(pack.description().data(PackDescription::CreationDate).toDate())));
        item->setToolTip(item->text());
        packItem->appendRow(item);
        item = new QStandardItem(tkTr(Trans::Constants::_1_COLON_2)
                                 .arg(tkTr(Trans::Constants::LAST_MODIFICATION))
                                 .arg(QLocale().toString(pack.description().data(PackDescription::LastModificationDate).toDate())));
        item->setToolTip(item->text());
        packItem->appendRow(item);

        if (_format == PackCreationModel::ShowByServer) {
            item = new QStandardItem(tkTr(Trans::Constants::_1_COLON_2)
                                     .arg(tkTr(Trans::Constants::QUEUE))
                                     .arg(queue.sourceAbsolutePathFile()));
            item->setToolTip(item->text());
            packItem->appendRow(item);
        }

        _packItems.insert(pack.originalXmlConfigFileName(), packItem);

        return packItem;
    }

    bool packCreationQueueToItem(const PackCreationQueue &queue)
    {
        if (isPackCreationQueueAlreadyInserted(queue))
            return true;

        QFont bold;
        bold.setBold(true);

        QStandardItem *rootItem = 0;
        if (_format == PackCreationModel::ShowByServer) {
        } else if (_format == PackCreationModel::ShowByQueue) {
            rootItem = new QStandardItem(tkTr(Trans::Constants::_1_COLON_2)
                                                .arg(tkTr(Trans::Constants::QUEUE))
                                                .arg(queue.sourceAbsolutePathFile()));
            rootItem->setToolTip(queue.sourceAbsolutePathFile());
            rootItem->setFont(bold);
            rootItem->setCheckable(true);
            rootItem->setCheckState(Qt::Checked);
            q->invisibleRootItem()->appendRow(rootItem);
            insertPackCreationQueueInCache(queue, rootItem);
        } else {
            LOG_ERROR_FOR(q, "Format not supported");
            return false;
        }

        QHash<QString, QStandardItem *> serversUidToItem;
        foreach(const RequestedPackCreation &request, queue.queue()) {
            if (_packDescriptionFilesIncluded.contains(request.descriptionFilePath))
                continue;
            _packDescriptionFilesIncluded.append(request.descriptionFilePath);

            QStandardItem *server = 0;
            if (_format == PackCreationModel::ShowByQueue) {
                server = serversUidToItem.value(request.serverUid, 0);
            } else if (_format == PackCreationModel::ShowByServer) {
                server = serverItem(request.serverUid);
            }

            if (!server) {
                server = new QStandardItem(tkTr(Trans::Constants::_1_COLON_2)
                                           .arg(tkTr(Trans::Constants::SERVER))
                                           .arg(request.serverUid));
                if (_format == PackCreationModel::ShowByQueue) {
                    serversUidToItem.insert(request.serverUid, server);
                    server->setCheckable(true);
                    server->setCheckState(Qt::Checked);
                    server->setFont(bold);
                    rootItem->appendRow(server);
                } else if (_format == PackCreationModel::ShowByServer) {
                    addServerItem(request.serverUid, server);
                    rootItem = server;
                    rootItem->setFont(bold);
                    rootItem->setCheckable(true);
                    rootItem->setCheckState(Qt::Checked);
                    q->invisibleRootItem()->appendRow(rootItem);
                }
            }

            server->appendRow(packToItem(request.descriptionFilePath, queue));
        }

        return true;
    }

    bool screenPath(const QString &absPath)
    {
        if (_screenedAbsPath.contains(absPath))
            return true;
        _screenedAbsPath.append(absPath);

        QFileInfoList files = Utils::getFiles(QDir(absPath), QString(Constants::PACKCREATIONQUEUE_DEFAULT_FILENAME));

        foreach(const QFileInfo &info, files) {
            PackCreationQueue queue;
            if (!queue.fromXmlFile(info.absoluteFilePath())) {
                LOG_ERROR_FOR(q, tkTr(Trans::Constants::FILE_1_ISNOT_READABLE).arg(info.absoluteFilePath()));
                continue;
            }

            if (!packCreationQueueToItem(queue)) {
                LOG_ERROR_FOR(q, QString("Unable to create the queue branch: %1").arg(info.absoluteFilePath()));
                continue;
            }
            _queues << queue;
        }
        return true;
    }

public:
    QHash<QString, QStandardItem *> _serversUidToItem; //, _screeningPathToItem;
    QHash<QString, QStandardItem *> _insertedPackCreationQueueUids;  // Key: queue.uid()
    QHash<QString, QStandardItem *> _packItems; // Key: absPathDescriptionFile
    QList<PackCreationQueue> _queues;

    QStringList _screenedAbsPath;
    QStringList _packDescriptionFilesIncluded;
    PackCreationModel::Format _format;

private:
    PackCreationModel *q;
};
} // namespace Internal
} // namespace DataPack

PackCreationModel::PackCreationModel(QObject *parent) :
    QStandardItemModel(parent),
    d(new Internal::PackCreationModelPrivate(this))
{
    d->initialize();

}

PackCreationModel::~PackCreationModel()
{
    if (d)
        delete d;
    d = 0;
}


void PackCreationModel::clearPackModel()
{
    d->clearModelAndCache();
}


void PackCreationModel::setFormat(Format format)
{
    d->_format = format;
    d->clearModelAndCache();
    foreach(const QString &path, d->_screenedAbsPath)
        d->screenPath(path);
}


QVariant PackCreationModel::data(const QModelIndex &index, int role) const
{
    return QStandardItemModel::data(index, role);
}


bool PackCreationModel::setCheckStateRoleToItemAndChildren(const QModelIndex &parent, const QVariant &value)
{
    if (!itemFromIndex(parent)->isCheckable())
        return false;

    if (!QStandardItemModel::setData(parent, value, Qt::CheckStateRole))
        return false;

    for(int i=0; i < rowCount(parent); ++i) {
        QModelIndex child = this->index(i, 0, parent);
        if (!setCheckStateRoleToItemAndChildren(child, value))
            continue;
    }
    return true;
}


bool PackCreationModel::setCheckedToAllParents(const QModelIndex &index)
{
    if (!index.parent().isValid())
        return false;

    if (!itemFromIndex(index)->isCheckable())
        return false;

    if (QStandardItemModel::setData(index.parent(), Qt::Checked, Qt::CheckStateRole))
        return setCheckedToAllParents(index.parent());

    return true;
}


bool PackCreationModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == Qt::CheckStateRole) {
        bool children = setCheckStateRoleToItemAndChildren(index, value);
        bool parents = true;
        if (value.toInt() == Qt::Checked) {
            parents = setCheckedToAllParents(index);
        }
        return children & parents;
    }
    return false;
}


bool PackCreationModel::addPackCreationQueue(const PackCreationQueue &queue)
{
    return d->packCreationQueueToItem(queue);
}

bool PackCreationModel::addScreeningPath(const QString &screeningAbsPath)
{
    return d->screenPath(screeningAbsPath);
}

int PackCreationModel::totalNumberOfPacksFound() const
{
    return d->_packItems.count();
}


QStringList PackCreationModel::getCheckedPacks() const
{
    QStringList list;
    QHashIterator<QString, QStandardItem *> i(d->_packItems);
    while (i.hasNext()) {
        i.next();
        if (i.value()->checkState() == Qt::Checked)
            list << i.key();
    }
    return list;
}


PackCreationQueue &PackCreationModel::generateQueueForServerCreation() const
{
    PackCreationQueue *internalQueue = new PackCreationQueue;
    foreach(const QString &packDescPath, getCheckedPacks()) {
        foreach(const PackCreationQueue &queue, d->_queues) {
            foreach(const RequestedPackCreation &request, queue.queue()) {
                if (request.descriptionFilePath == packDescPath) {
                    if (!internalQueue->addToQueue(request)) {
                        LOG_ERROR("unable to add request to queue");
                    }
                    break;
                }
            }
        }
    }
    return *internalQueue;
}
