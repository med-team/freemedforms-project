/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developers : Eric Maeker                                          *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "packcreationqueue.h"
#include <datapackutils/pack.h>
#include <datapackutils/servercontent.h>
#include <datapackutils/constants.h>

#include <utils/log.h>
#include <utils/global.h>
#include <translationutils/constants.h>
#include <translationutils/trans_msgerror.h>
#include <quazip/JlCompress.h>
#include <quazip/global.h>

#include <QString>
#include <QDir>
#include <QFile>
#include <QDomDocument>

enum {
    WarnRequestChecking = false,
    DebugEqualityOperator = false
};

using namespace DataPack;
using namespace Trans::ConstantTranslations;



namespace {
const char * const XML_ROOT_TAG = "PackCreationQueue";
const char * const XML_DATAPACK_TAG = "datapack";
const char * const XML_DATAPACK_CONTENT_TAG = "content";
const char * const XML_DATAPACK_DESCRIPTION_ATTRIB = "description";
const char * const XML_DATAPACK_SERVER_ATTRIB = "server";
const char * const XML_CONTENT_TYPE_ATTRIB = "type";

const char * const XML_TYPE_FILE_ZIPPED = "file_zipped";
const char * const XML_TYPE_FILE_UNZIPPED = "file_unzipped";
const char * const XML_TYPE_DIR = "dir";

static QString xmlContentType(const int type)
{
    switch (type)
    {
    case DataPack::RequestedPackCreation::ZippedFile: return ::XML_TYPE_FILE_ZIPPED;
    case DataPack::RequestedPackCreation::UnzippedFile: return ::XML_TYPE_FILE_UNZIPPED;
    case DataPack::RequestedPackCreation::DirContent: return ::XML_TYPE_DIR;
    }
    return QString::null;
}
} // anonymous namespace


PackCreationQueue::PackCreationQueue()
{
    _uid = Utils::createUid();
}

PackCreationQueue::~PackCreationQueue()
{}

bool PackCreationQueue::checkValidity(const RequestedPackCreation &request) const
{
    if (!QFileInfo(request.descriptionFilePath).exists()) {
        if (WarnRequestChecking)
            qDebug() << "File does not exists:" << request.descriptionFilePath;
        return false;
    }

    foreach(int key, request.content.uniqueKeys()) {
        foreach(const QString &path, request.content.values(key)) {
            if (!QFileInfo(path).exists()) {
                if (WarnRequestChecking)
                    qDebug() << "File does not exists:" << path;
                return false;
            }
        }
    }
    return true;
}

bool PackCreationQueue::isEmpty() const
{
    return _queue.isEmpty();
}

bool PackCreationQueue::addToQueue(const RequestedPackCreation &request)
{
    if (!checkValidity(request))
        return false;
    _queue << request;
    return true;
}

PackCreationQueue& PackCreationQueue::operator+=(const PackCreationQueue& add)
{
    foreach(const RequestedPackCreation &request, add.queue()) {
        addToQueue(request);
    }
    return *this;
}



bool PackCreationQueue::containsPackDescriptionFile(const QString &absPath) const
{
    foreach(const RequestedPackCreation &request, _queue) {
        if (request.descriptionFilePath == absPath)
            return true;
    }
    return false;
}


bool PackCreationQueue::createZippedContent(const RequestedPackCreation &request, const QString &absZipFileName) const
{
    if (absZipFileName.isEmpty()) {
        LOG_ERROR_FOR("PackCreationQueue", "Empty file name");
        return false;
    }
    if (QFileInfo(absZipFileName).exists()) {
        LOG_ERROR_FOR("PackCreationQueue", "Zip file already exists");
        return false;
    }
    if (!checkValidity(request)) {
        LOG_ERROR_FOR("PackCreationQueue", "Invalid request");
        return false;
    }


    QString tmpPath = QString("%1/%2").arg(QDir::tempPath()).arg(Utils::createUid());
    if (!QDir().mkpath(tmpPath)) {
        LOG_ERROR_FOR("PackCreationQueue", QString("Unable to create path: %1").arg(tmpPath));
        return false;
    }
    int n = 0;

    foreach(const QString &path, request.content.values(RequestedPackCreation::DirContent)) {
        if (!request.isRelativePathFromDescriptionPathValid(path)) {
            LOG_ERROR_FOR("PackCreationQueue", QString("Content file outside pack description dir tree: %1 / description: %2")
                          .arg(path)
                          .arg(request.descriptionFilePath));
            Utils::removeDirRecursively(tmpPath);
            return false;
        }

        QString dest = QString("%1/%2").arg(tmpPath).arg(request.relativePathFromDescriptionPath(path));
        if (!Utils::copyDir(path, dest)) {
            LOG_ERROR_FOR("PackCreationQueue", QString("Unable to copy file: %1 to %2").arg(path).arg(dest));
            Utils::removeDirRecursively(tmpPath);
            return false;
        }
        ++n;
    }

    foreach(const QString &path, request.content.values(RequestedPackCreation::UnzippedFile)) {
        if (!request.isRelativePathFromDescriptionPathValid(path)) {
            LOG_ERROR_FOR("PackCreationQueue", QString("Content file outside pack description dir tree: %1 / description: %2")
                          .arg(path)
                          .arg(request.descriptionFilePath));
            Utils::removeDirRecursively(tmpPath);
            return false;
        }

        QString dest = QString("%1/%2").arg(tmpPath).arg(request.relativePathFromDescriptionPath(path));
        if (!QDir().mkpath(QFileInfo(dest).absolutePath())) {
            LOG_ERROR_FOR("PackCreationQueue", QString("Unable to create path: %1").arg(path));
            Utils::removeDirRecursively(tmpPath);
            return false;
        }

        if (!QFile::copy(path, dest)) {
            LOG_ERROR_FOR("PackCreationQueue", QString("Unable to copy file: %1 to %2").arg(path).arg(dest));
            Utils::removeDirRecursively(tmpPath);
            return false;
        }
        ++n;
    }

    if (n == 0) {
        if (request.content.values(RequestedPackCreation::ZippedFile).count() == 1) {
            return QFile(request.content.value(RequestedPackCreation::ZippedFile)).copy(absZipFileName);
        }
    }

    foreach(const QString &path, request.content.values(RequestedPackCreation::ZippedFile)) {
        if (!QuaZipTools::unzipFile(path, tmpPath))
            LOG_ERROR_FOR("PackCreationQueue", "Unable to unzip file: " + path);
        ++n;
    }

    if (!JlCompress::compressDir(absZipFileName, tmpPath, true)) {
        LOG_ERROR_FOR("PackCreationQueue", "Unable to compress dir: "+tmpPath);
        Utils::removeDirRecursively(tmpPath);
        return false;
    }

    if (!Utils::removeDirRecursively(tmpPath))
        LOG_ERROR_FOR("PackCreationQueue", QString("Unable to clean temp path: %1").arg(tmpPath));

    return true;
}


QString PackCreationQueue::sourceAbsolutePathFile() const
{
    return _sourceAbsPath;
}


bool PackCreationQueue::fromXmlFile(const QString &absFile)
{
    if (absFile.isEmpty() || !QFile(absFile).exists())
        return false;

    QString content = Utils::readTextFile(absFile, Utils::DontWarnUser);
    if (content.isEmpty())
        return false;

    QDomDocument doc;
    int line = 0;
    int col = 0;
    QString error;
    if (!doc.setContent(content, &error, &line, &col)) {
        LOG_ERROR_FOR("PackCreationQueue", tkTr(Trans::Constants::ERROR_1_LINE_2_COLUMN_3).arg(error).arg(line).arg(col));
        return false;
    }

    QDomElement root = doc.documentElement();
    if (root.tagName().compare(::XML_ROOT_TAG, Qt::CaseInsensitive) != 0) {
        LOG_ERROR_FOR("PackCreationQueue", "Wrong root tag: " + root.tagName() + "; awaiting " + ::XML_ROOT_TAG);
        return false;
    }

    QDomElement packElement = root.firstChildElement(::XML_DATAPACK_TAG);
    while (!packElement.isNull()) {
        RequestedPackCreation request;

        QString descrFile = packElement.attribute(::XML_DATAPACK_DESCRIPTION_ATTRIB);
        QFileInfo packDescrFile(descrFile);
        if (packDescrFile.isRelative())
            packDescrFile.setFile(QString("%1/%2").arg(QFileInfo(absFile).absolutePath()).arg(descrFile));
        if (!packDescrFile.exists()) {
            LOG_ERROR_FOR("PackCreationQueue", "Pack does not exists: " + packDescrFile.absoluteFilePath());
            packElement = packElement.nextSiblingElement(::XML_DATAPACK_TAG);
            continue;
        }
        request.descriptionFilePath = packDescrFile.absoluteFilePath();

        request.serverUid = packElement.attribute(::XML_DATAPACK_SERVER_ATTRIB);

        QDomElement content = packElement.firstChildElement(::XML_DATAPACK_CONTENT_TAG);
        while (!content.isNull()) {
            const QString &type = content.attribute(::XML_CONTENT_TYPE_ATTRIB);
            QString path = content.text();
            if (QDir(path).isRelative()) {
                path = QDir::cleanPath(QString("%1/%2")
                        .arg(QFileInfo(absFile).absolutePath())
                        .arg(path)
                                       );
            }

            if (type.compare(::XML_TYPE_DIR, Qt::CaseInsensitive) == 0) {
                request.content.insertMulti(RequestedPackCreation::DirContent, path);
            } else if (type.compare(::XML_TYPE_FILE_ZIPPED, Qt::CaseInsensitive) == 0) {
                request.content.insertMulti(RequestedPackCreation::ZippedFile, path);
            } else if (type.compare(::XML_TYPE_FILE_UNZIPPED, Qt::CaseInsensitive) == 0) {
                request.content.insertMulti(RequestedPackCreation::UnzippedFile, path);
            }

            content = content.nextSiblingElement(::XML_DATAPACK_CONTENT_TAG);
        }

        addToQueue(request);
        packElement = packElement.nextSiblingElement(::XML_DATAPACK_TAG);
    }
    _sourceAbsPath = QDir::cleanPath(absFile);
    return true;
}


bool PackCreationQueue::saveToXmlFile(const QString &absFile, bool useRelativePath) const
{
    QDomDocument doc("FreeMedForms");
    QDomElement root = doc.createElement(::XML_ROOT_TAG);
    doc.appendChild(root);

    foreach(const RequestedPackCreation &request, _queue) {
        QDomElement requestElement = doc.createElement(::XML_DATAPACK_TAG);
        root.appendChild(requestElement);
        if (useRelativePath) {
            QString rpath = QDir(QFileInfo(absFile).absolutePath()).relativeFilePath(request.descriptionFilePath);
            requestElement.setAttribute(::XML_DATAPACK_DESCRIPTION_ATTRIB, rpath);
        } else {
            requestElement.setAttribute(::XML_DATAPACK_DESCRIPTION_ATTRIB, request.descriptionFilePath);
        }
        requestElement.setAttribute(::XML_DATAPACK_SERVER_ATTRIB, request.serverUid);
        foreach(int key, request.content.uniqueKeys()) {
            foreach(const QString &path, request.content.values(key)) {
                QDomElement contentElement = doc.createElement(::XML_DATAPACK_CONTENT_TAG);
                requestElement.appendChild(contentElement);
                contentElement.setAttribute(::XML_CONTENT_TYPE_ATTRIB, xmlContentType(key));
                if (useRelativePath) {
                    QString rpath = request.relativePathFromDescriptionPath(path);
                    QDomText text = doc.createTextNode(rpath);
                    contentElement.appendChild(text);
                } else {
                    QDomText text = doc.createTextNode(path);
                    contentElement.appendChild(text);
                }
            }
        }
    }

    QString xml = QString("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                          "%1").arg(doc.toString(2));

    if (Utils::saveStringToFile(xml, absFile, Utils::Overwrite, Utils::DontWarnUser)) {
        _sourceAbsPath = absFile;
        return true;
    }

    return false;
}

QString RequestedPackCreation::relativePathFromDescriptionPath(const QString &absPath) const
{
    return QDir(QFileInfo(this->descriptionFilePath).absolutePath()).relativeFilePath(absPath);

}


bool RequestedPackCreation::isRelativePathFromDescriptionPathValid(const QString &absPath) const
{
    return (!relativePathFromDescriptionPath(absPath).contains("../"));
}

bool RequestedPackCreation::operator==(const RequestedPackCreation &other) const
{
    if (this->serverUid != other.serverUid) {
        if (DebugEqualityOperator)
            LOG_FOR("RequestedPackCreation", "serverUid mismatch");
        return false;
    }
    if (this->descriptionFilePath != other.descriptionFilePath) {
        if (DebugEqualityOperator)
            LOG_FOR("RequestedPackCreation", "descriptionFilePath mismatch");
        return false;
    }
    if (this->content != other.content) {
        if (DebugEqualityOperator)
            LOG_FOR("RequestedPackCreation", "content mismatch");
        return false;
    }
    return true;
}


bool PackCreationQueue::operator==(const PackCreationQueue &other) const
{
    if (this->_queue.count() != other._queue.count()) {
        if (DebugEqualityOperator)
            LOG_FOR("PackCreationQueue", "Pack count mismatch");
        return false;
    }
    foreach(const RequestedPackCreation &request, _queue) {
        if (!other._queue.contains(request)) {
            if (DebugEqualityOperator)
                LOG_FOR("PackCreationQueue", "request mismatch");
            return false;
        }
    }
    if (DebugEqualityOperator)
        LOG_FOR("PackCreationQueue", "operator==() returns true");
    return true;
}
