/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developers : Eric Maeker                                          *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATAPACK_PACKCREATIONMODEL_H
#define DATAPACK_PACKCREATIONMODEL_H

#include <datapackutils/datapack_exporter.h>
#include <QStandardItemModel>

/**
 * \file ./libs/datapackutils/servercreation/packcreationmodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DataPack {
class PackCreationQueue;

namespace Internal {
class PackCreationModelPrivate;
} // namespace Internal

class DATAPACK_EXPORT PackCreationModel : public QStandardItemModel
{
public:
    enum Format {
        ShowByQueue = 0,
        ShowByServer
    };

    explicit PackCreationModel(QObject *parent = 0);
    ~PackCreationModel();

    void clearPackModel();
    void setFormat(Format format);

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

//    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    int totalNumberOfPacksFound() const;
    QStringList getCheckedPacks() const;
    PackCreationQueue &generateQueueForServerCreation() const;

public Q_SLOTS:
    bool addPackCreationQueue(const PackCreationQueue &queue);
    bool addScreeningPath(const QString &screeningAbsPath);


private:
    bool setCheckStateRoleToItemAndChildren(const QModelIndex &parent, const QVariant &value);
    bool setCheckedToAllParents(const QModelIndex &index);

private:
    Internal::PackCreationModelPrivate *d;
};

} // namespace DataPack

#endif // DATAPACK_PACKCREATIONMODEL_H

