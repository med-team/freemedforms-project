/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developers : Eric Maeker                                          *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATAPACK_PACKSERVERCREATOR_H
#define DATAPACK_PACKSERVERCREATOR_H

#include <datapackutils/datapack_exporter.h>
#include <datapackutils/servercreation/packcreationqueue.h>
#include <QHash>
#include <QString>

/**
 * \file ./libs/datapackutils/servercreation/packservercreator.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DataPack {

class DATAPACK_EXPORT PackServerCreator
{
public:
    explicit PackServerCreator();
    ~PackServerCreator();

    void useDefaultPathForServerDescriptionFiles(const QString &rootPath);
    bool addPackCreationQueue(const PackCreationQueue &queue);
    bool setServerDescriptionFilePath(const QString &serverUid, const QString &descrAbsFileName);

    void setAutoVersionning(const bool enable) {_autoVersionning = enable;}
    bool autoVersionning() const {return _autoVersionning;}

    bool createServer(const QString &serverAbsPath) const;

private:
    PackCreationQueue _queue;
    QHash<QString, QString> _serverUid_DescrFile;
    bool _autoVersionning;
};

} // namespace DataPack

#endif // DATAPACK_PACKSERVERCREATOR_H

