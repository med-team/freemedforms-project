/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developers : Eric Maeker                                          *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATAPACK_PACKCREATIONQUEUE_H
#define DATAPACK_PACKCREATIONQUEUE_H

#include <datapackutils/datapack_exporter.h>
#include <QString>
#include <QMultiHash>
#include <QList>

/**
 * \file ./libs/datapackutils/servercreation/packcreationqueue.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DataPack {
struct DATAPACK_EXPORT RequestedPackCreation {
    enum ContentType {
        ZippedFile = 0,
        UnzippedFile,
        DirContent
    };

    QString relativePathFromDescriptionPath(const QString &absPath) const;
    bool isRelativePathFromDescriptionPathValid(const QString &absPath) const;

    bool operator==(const RequestedPackCreation &other) const;

    QString serverUid, descriptionFilePath;
    QMultiHash<int, QString> content; // Key = ContentType ; Value = the content itself (path, filename...) absolutePath
};

class DATAPACK_EXPORT PackCreationQueue
{
public:
    enum {
        UseRelativePath = true,
        UseAbsolutePath = false
    };

    explicit PackCreationQueue();
    ~PackCreationQueue();
    QString uid() const {return _uid;}

    bool checkValidity(const RequestedPackCreation &request) const;

    // Queue management
    bool isEmpty() const;
    bool addToQueue(const RequestedPackCreation &request);
    const QList<RequestedPackCreation> &queue() const {return _queue;}
    PackCreationQueue& operator+=(const PackCreationQueue& add);

    // Datapack content creation
    bool containsPackDescriptionFile(const QString &absPath) const;
    bool createZippedContent(const RequestedPackCreation &request, const QString &absZipFileName) const;

    // XML import/export
    QString sourceAbsolutePathFile() const;
    bool fromXmlFile(const QString &absFile);
    bool saveToXmlFile(const QString &absFile, bool useRelativePath = false) const;

    // Equality checking
    bool operator==(const PackCreationQueue &other) const;
    bool operator!=(const PackCreationQueue &other) const {return (!operator==(other));}

private:
    QList<RequestedPackCreation> _queue;
    QString _uid;
    mutable QString _sourceAbsPath;
};

} // namespace DataPack

#endif // DATAPACK_PACKCREATIONQUEUE_H

