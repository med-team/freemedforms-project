/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developers : Eric Maeker                                          *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "packservercreator.h"
#include <datapackutils/pack.h>
#include <datapackutils/serverdescription.h>
#include <datapackutils/servercontent.h>
#include <datapackutils/constants.h>

#include <utils/log.h>
#include <utils/global.h>
#include <translationutils/constants.h>
#include <translationutils/trans_msgerror.h>
#include <quazip/JlCompress.h>

#include <QString>
#include <QDir>
#include <QFile>
#include <QDomDocument>

using namespace DataPack;
using namespace Trans::ConstantTranslations;

namespace {

static QString getVendor(const QString &serverUid)
{
    if (serverUid == Constants::SERVER_COMMUNITY_FREE)
        return "community";
    else if (serverUid == Constants::SERVER_COMMUNITY_NONFREE)
        return "community";
    else if (serverUid == Constants::SERVER_ASSO_FREE)
        return "asso";
    else if (serverUid == Constants::SERVER_ASSO_NONFREE)
        return "asso";
    return serverUid;
}
} // anonymous namespace

PackServerCreator::PackServerCreator() :
    _autoVersionning(true)
{
}

PackServerCreator::~PackServerCreator()
{}


void PackServerCreator::useDefaultPathForServerDescriptionFiles(const QString &rootPath)
{
    _serverUid_DescrFile.insert(Constants::SERVER_COMMUNITY_FREE, QDir::cleanPath(QString("%1/servers/free/community/server.conf.xml").arg(rootPath)));
    _serverUid_DescrFile.insert(Constants::SERVER_COMMUNITY_NONFREE, QDir::cleanPath(QString("%1/servers/nonfree/community/server.conf.xml").arg(rootPath)));
    _serverUid_DescrFile.insert(Constants::SERVER_ASSO_FREE, QDir::cleanPath(QString("%1/servers/free/asso/server.conf.xml").arg(rootPath)));
    _serverUid_DescrFile.insert(Constants::SERVER_ASSO_NONFREE, QDir::cleanPath(QString("%1/servers/nonfree/asso/server.conf.xml").arg(rootPath)));
}

bool PackServerCreator::addPackCreationQueue(const PackCreationQueue &queue)
{
    _queue += queue;
    return true;
}


bool PackServerCreator::setServerDescriptionFilePath(const QString &serverUid, const QString &descrAbsFileName)
{
    if (_serverUid_DescrFile.keys().contains(serverUid))
        return false;
   _serverUid_DescrFile.insert(serverUid, descrAbsFileName);
    return true;
}


bool PackServerCreator::createServer(const QString &serverAbsPath) const
{
    if (_queue.isEmpty()) {
        LOG_ERROR_FOR("PackServerCreator", "No Pack selected/found for server creation");
        return false;
    }


    QString queueXmlFile = serverAbsPath + "/queue.xml";
    if (!_queue.saveToXmlFile(queueXmlFile, false)) {
        LOG_ERROR_FOR("PackServerCreator", QString("Unable to save queue: %1").arg(queueXmlFile));
        return false;
    } else {
        LOG_FOR("PackServerCreator", QString("File Created: %1").arg(queueXmlFile));
    }

    QList<Pack*> packs;
    QHash<QString, ServerContent*> serverContent;

    foreach(const RequestedPackCreation &request, _queue.queue()) {
        Pack *pack = new Pack;
        if (!pack->fromXmlFile(request.descriptionFilePath)) {
            LOG_ERROR_FOR("PackServerCreator", QString("Pack description file can not be read: %1").arg(request.descriptionFilePath));
            return false;
        }
        PackDescription descr = pack->description();

        QString serverRootPath = QString("%1/%2/%3/%4")
                .arg(serverAbsPath)
                .arg(request.serverUid.contains("nonfree", Qt::CaseInsensitive)?"nonfree":"free")
                .arg(getVendor(request.serverUid))
                .arg(qApp->applicationVersion());
        QString packPath = QString("%1/%2")
                .arg(serverRootPath)
                .arg(descr.data(PackDescription::Uuid).toString());
        QString zipFile = QString("%1/%2.zip")
                .arg(packPath)
                .arg(descr.data(PackDescription::Uuid).toString());

        if (!_queue.createZippedContent(request, zipFile)) {
            LOG_ERROR_FOR("PackServerCreator", QString("Unable to create server zipped content"));
            return false;
        }

        descr.setData(PackDescription::FreeMedFormsCompatVersion, qApp->applicationVersion());
        descr.setData(PackDescription::FreeDiamsCompatVersion, qApp->applicationVersion());
        descr.setData(PackDescription::FreeAccountCompatVersion, qApp->applicationVersion());
        descr.setData(PackDescription::LastModificationDate, QDateTime::currentDateTime().toString(Qt::ISODate));
        descr.setData(PackDescription::Size, QFileInfo(zipFile).size());
        descr.setData(PackDescription::Md5, Utils::fileMd5(zipFile));
        descr.setData(PackDescription::Sha1, Utils::fileSha1(zipFile));
        pack->setPackDescription(descr);

        QString packDescriptionOutputFileName = QString("%1/%2").arg(packPath).arg(Constants::PACKDESCRIPTION_FILENAME);
        if (!Utils::saveStringToFile(pack->toXml(), packDescriptionOutputFileName, Utils::Overwrite, Utils::DontWarnUser)) {
            LOG_ERROR_FOR("PackServerCreator", QString("Unable to save Pack description file: %1").arg(packDescriptionOutputFileName));
            return false;
        }

        QDir serverdir(serverRootPath);
        ServerContent *content = serverContent.value(request.serverUid);
        if (!content) {
            content = new ServerContent;
            serverContent.insert(request.serverUid, content);
        }
        content->addPackRelativeFileName(serverdir.relativeFilePath(packDescriptionOutputFileName));

        packs << pack;
    }

    foreach(const QString &serverUid, serverContent.uniqueKeys()) {
        ServerDescription descr;
        if (!descr.fromXmlFile(_serverUid_DescrFile.value(serverUid))) {
            LOG_ERROR_FOR("PackServerCreator", QString("Unable to read XML file: %1").arg(_serverUid_DescrFile.value(serverUid)));
            return false;
        }
        descr.setData(DataPack::ServerDescription::LastModificationDate, QDate::currentDate());
        if (autoVersionning()) {
            descr.setData(DataPack::ServerDescription::Version, qApp->applicationVersion());
            descr.setData(DataPack::ServerDescription::FreeMedFormsCompatVersion, qApp->applicationVersion());
            descr.setData(DataPack::ServerDescription::FreeDiamsCompatVersion, qApp->applicationVersion());
            descr.setData(DataPack::ServerDescription::FreeAccountCompatVersion, qApp->applicationVersion());
        }

        QString xml = descr.toXml();
        int start = xml.indexOf(QString("</%1>").arg(descr.rootTag()));
        xml.insert(start, serverContent.value(serverUid)->toXml());

        QString serverRootPath = QString("%1/%2/%3/%4")
                .arg(serverAbsPath)
                .arg(serverUid.contains("nonfree", Qt::CaseInsensitive)?"nonfree":"free")
                .arg(getVendor(serverUid))
                .arg(qApp->applicationVersion());

        if (!Utils::saveStringToFile(xml,serverRootPath + "/server.conf.xml", Utils::Overwrite, Utils::DontWarnUser)) {
            LOG_ERROR_FOR("PackServerCreator", "Unable to save server configuration file");
            return false;
        }

        QString tmp = QString("%1/packserver_%2/").arg(QDir::tempPath()).arg(Utils::createUid());
        if (!Utils::checkDir(tmp, true, "PackServerCreator")) {
            LOG_ERROR_FOR("PackServerCreator", "Unable to create temp path");
            return false;
        }
        QDir serverdir(serverRootPath);
        QFileInfoList list = Utils::getFiles(serverRootPath, "*.xml");
        bool serverFileIncluded = false;
        foreach(const QFileInfo &info, list) {
            QString tmpFile = tmp + QDir::separator() + serverdir.relativeFilePath(info.absoluteFilePath());
            Utils::checkDir(QFileInfo(tmpFile).absolutePath(), true, "PackServerCreator");
            QFile(info.absoluteFilePath()).copy(tmpFile);
            if (info.baseName()=="server.conf.xml")
                serverFileIncluded = true;
        }
        if (!serverFileIncluded) {
            QString tmpFile = tmp + "/server.conf.xml";
            QFile(serverRootPath + "/server.conf.xml").copy(tmpFile);
        }
        if (!JlCompress::compressDir(serverRootPath + "/serverconf.zip", tmp))
            LOG_ERROR_FOR("PackServerCreator", "Unable to zip config files");
        Utils::removeDirRecursively(tmp, 0);
    }

    qDeleteAll(serverContent);
    serverContent.clear();
    qDeleteAll(packs);
    packs.clear();
    return true;
}
