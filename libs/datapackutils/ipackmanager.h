/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATAPACK_IPACKMANAGER_H
#define DATAPACK_IPACKMANAGER_H

#include <datapackutils/datapack_exporter.h>
#include <datapackutils/serverenginestatus.h>
#include <datapackutils/pack.h>
#include <QObject>
QT_BEGIN_NAMESPACE
class QProgressBar;
QT_END_NAMESPACE

/**
 * \file ./libs/datapackutils/ipackmanager.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DataPack {

class DATAPACK_EXPORT IPackManager : public QObject
{
    Q_OBJECT
public:
    IPackManager(QObject *parent = 0) : QObject(parent) {}
    virtual ~IPackManager() {}

    virtual QList<Pack> installedPack(bool forceRefresh = false) = 0;
    virtual bool isDataPackInstalled(const Pack &pack) = 0;
    virtual bool isDataPackInstalled(const QString &packUid, const QString &packVersion = QString::null) = 0;

    virtual bool isPackInPersistentCache(const Pack &pack) = 0;

    // TODO: remove QProgressBar direct access and use sig/slot instead?
    virtual bool downloadPack(const Pack &pack, QProgressBar *bar) = 0;
    virtual bool checkCachedPackFileIntegrity(const Pack &pack) = 0;
    virtual bool installDownloadedPack(const Pack &pack, QProgressBar *bar = 0) = 0;
    virtual bool removePack(const Pack &pack) = 0;

    // TODO: add signal including the ServerEngineStatus
Q_SIGNALS:
    void packDownloaded(const DataPack::Pack &pack, const DataPack::ServerEngineStatus &status);
    void packInstalled(const DataPack::Pack &pack);
    void packRemoved(const DataPack::Pack &pack);
};

}  // End namespace DataPack

#endif // DATAPACK_IPACKMANAGER_H
