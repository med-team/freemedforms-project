/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PACKDEPENDENCYCHECKER_H
#define PACKDEPENDENCYCHECKER_H

#include <datapackutils/pack.h>

#include <QObject>

/**
 * \file ./libs/datapackutils/packdependencychecker.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DataPack {

class PackDependencyChecker : public QObject
{
    Q_OBJECT
public:
    explicit PackDependencyChecker(QObject *parent = 0);

    QList<Pack> packDependencies(const Pack &pack, const PackDependencyData::TypeOfDependence &dependence);

    void testCombination(const QList<Pack> &installPacks, const QList<Pack> &updatePacks, const QList<Pack> &removePacks);
    bool isCombinationCorrect() const;

    QList<Pack> neededToInstall() const;
    QList<Pack> neededToUpdate() const;
    QList<Pack> neededToRemove() const;

private:
    QList<Pack> m_ToInstall, m_ToUpdate, m_ToRemove;
    bool m_IsCorrect;
};

}  // End namespace DataPack

#endif // PACKDEPENDENCYCHECKER_H
