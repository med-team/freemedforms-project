/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATAPACK_SERVERENGINESTATUS_H
#define DATAPACK_SERVERENGINESTATUS_H

#include <datapackutils/datapack_exporter.h>

#include <QStringList>

namespace DataPack {

struct DATAPACK_EXPORT ServerEngineStatus {
    ServerEngineStatus() :
        hasError(false),
        isSuccessful(false),
        isLicenseAccepted(true),
        downloadCorrectlyFinished(false),
        proxyIdentificationError(false),
        serverIdentificationError(false)
    {}

    bool hasError, isSuccessful, isLicenseAccepted;
    bool downloadCorrectlyFinished;
    bool proxyIdentificationError, serverIdentificationError;
    QStringList errorMessages, engineMessages;
};

} // End namespace DataPack

#endif // DATAPACK_SERVERENGINESTATUS_H
