/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "datapackcore.h"
#include "servermanager.h"
#include "packmanager.h"
#include "serverengines/localserverengine.h"
#include "serverengines/httpserverengine.h"

#include <utils/log.h>
#include <utils/global.h>

#include <QDir>
#include <QNetworkProxy>

using namespace DataPack;

namespace  {
static DataPack::DataPackCore *m_instance = 0;
} // namespace anonymous

DataPack::DataPackCore &DataPack::DataPackCore::instance(QObject *parent)
{
    if (!m_instance)
        m_instance = new DataPackCore(parent);
    return *m_instance;
}

namespace DataPack {
namespace Internal {
class DataPackCorePrivate
{
public:
    DataPackCorePrivate() :
        m_ServerManager(0),
        m_PackManager(0),
        m_LocalEngine(0),
        m_HttpEngine(0)
    {}

public:
    ServerManager *m_ServerManager;
    PackManager *m_PackManager;
    LocalServerEngine *m_LocalEngine;
    HttpServerEngine *m_HttpEngine;
    QVector<IServerEngine *> m_Engines;

    QHash<int, QString> m_ThemePath;
    QString m_InstallPath, m_TmpCachePath, m_PersistentCachePath;
    QNetworkProxy m_Proxy;

    QHash<QString, QString> m_Tag_Path;
};
}  // End namespace Internal
}  // End namespace DataPack

DataPackCore::DataPackCore(QObject *parent) :
    QObject(parent),
    d(new Internal::DataPackCorePrivate)
{
    d->m_ServerManager = new Internal::ServerManager(this);
    d->m_PackManager = new Internal::PackManager(this);
}

DataPackCore::~DataPackCore()
{
    if (d) {
        delete d;
        d = 0;
    }
}

void DataPackCore::init()
{
    d->m_LocalEngine = new Internal::LocalServerEngine(this);
    d->m_HttpEngine = new Internal::HttpServerEngine(this);
    d->m_Engines << d->m_LocalEngine << d->m_HttpEngine;
    d->m_ServerManager->init(d->m_Engines);
    d->m_PackManager->init(d->m_Engines);
}

bool DataPackCore::isInternetConnectionAvailable()
{
    return !Utils::testInternetConnection().isEmpty();
}

void DataPackCore::setNetworkProxy(const QNetworkProxy &proxy)
{
    d->m_Proxy = proxy;
}

const QNetworkProxy &DataPackCore::networkProxy() const
{
    return d->m_Proxy;
}

IServerManager *DataPackCore::serverManager() const
{
    return d->m_ServerManager;
}

IPackManager *DataPackCore::packManager() const
{
    return d->m_PackManager;
}

bool DataPackCore::stopJobsAndClearQueues() const
{
    bool ok = true;
    for(int i=0; i < d->m_Engines.count(); ++i) {
        IServerEngine *e = d->m_Engines[i];
        if (!e->stopJobsAndClearQueue()) {
            LOG_ERROR("Unable to stop job && clear queue for engine: "+ e->objectName());
            ok = false;
        }
    }
    return ok;
}

void DataPackCore::setInstallPath(const QString &absPath)
{
    d->m_InstallPath = QDir::cleanPath(absPath);
    QDir test(d->m_InstallPath);
    if (!test.exists()) {
        if (!test.mkpath(test.absolutePath()))
            LOG_ERROR(QString("Unable to create DataPack::InstallDir %1").arg(d->m_InstallPath));
    }
}

QString DataPackCore::installPath() const
{
    return d->m_InstallPath;
}

void DataPackCore::setPersistentCachePath(const QString &absPath)
{
    d->m_PersistentCachePath = QDir::cleanPath(absPath);
    QDir test(d->m_PersistentCachePath);
    if (!test.exists()) {
        if (!test.mkpath(test.absolutePath()))
            LOG_ERROR(QString("Unable to create DataPack::PersistentCache %1").arg(d->m_PersistentCachePath));
    }
}

QString DataPackCore::persistentCachePath() const
{
    return d->m_PersistentCachePath;
}

void DataPackCore::setTemporaryCachePath(const QString &absPath)
{
    d->m_TmpCachePath = QDir::cleanPath(absPath);
    QDir test(d->m_TmpCachePath);
    if (!test.exists()) {
        if (!test.mkpath(test.absolutePath()))
            LOG_ERROR(QString("Unable to create DataPack::TempCache %1").arg(d->m_TmpCachePath));
    }
}

QString DataPackCore::temporaryCachePath() const
{
    return d->m_TmpCachePath;
}

void DataPackCore::setThemePath(ThemePath path, const QString &absPath)
{
    QDir test(absPath);
    if (!test.exists())
        LOG_ERROR(QString("Theme path does not exist %1").arg(test.absolutePath()));
    d->m_ThemePath.insert(path, QDir::cleanPath(absPath));
}

QString DataPackCore::icon(const QString &name, ThemePath path)
{
    return QString("%1/%2").arg(d->m_ThemePath.value(path)).arg(name);
}

void DataPackCore::registerPathTag(const QString &tag, const QString &absPath)
{
    d->m_Tag_Path.insert(tag, QDir::cleanPath(absPath) + QDir::separator());
}

bool DataPackCore::containsPathTag(const QString &path)
{
    foreach(const QString &t, d->m_Tag_Path.keys()) {
        if (path.contains(t, Qt::CaseInsensitive))
            return true;
    }
    return false;
}

QString DataPackCore::replacePathTag(const QString &path)
{
    QString tmp = path;
    foreach(const QString &t, d->m_Tag_Path.keys()) {
        if (tmp.contains(t, Qt::CaseInsensitive))
            tmp = tmp.replace(t, d->m_Tag_Path.value(t));
    }
    return QDir::cleanPath(tmp);
}
