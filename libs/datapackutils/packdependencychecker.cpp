/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "packdependencychecker.h"

#include <datapackutils/datapackcore.h>
#include <datapackutils/iservermanager.h>
#include <datapackutils/ipackmanager.h>

#include <QVector>

using namespace DataPack;

static inline DataPack::DataPackCore &core() { return DataPack::DataPackCore::instance(); }
static inline DataPack::IServerManager *serverManager() { return core().serverManager(); }
static inline DataPack::IPackManager *packManager() { return core().packManager(); }

PackDependencyChecker::PackDependencyChecker(QObject *parent) :
    QObject(parent),
    m_IsCorrect(false)
{
}

QList<Pack> PackDependencyChecker::packDependencies(const Pack &pack, const PackDependencyData::TypeOfDependence &dependence)
{
    QList<Pack> toReturn;
    for(int i = 0; i < pack.dependencies().count(); ++i) {
        if (pack.dependencies().at(i).type()!=dependence)
            continue;
        const QString &uid = pack.dependencies().at(i).uuid();
        const QString &version = pack.dependencies().at(i).version();
            for(int j=0; j < serverManager()->serverCount(); ++j) {
                const QList<Pack> &packs = serverManager()->getPackForServer(serverManager()->getServerAt(i));
                for(int z=0; z < packs.count(); ++z) {
                    if (packs.at(z).uuid().compare(uid,Qt::CaseInsensitive)==0 &&
                            packs.at(z).version().compare(version,Qt::CaseInsensitive)==0) {
                        toReturn << packs.at(z);
                    }
                }
            }
    }
    return toReturn;
}

void PackDependencyChecker::testCombination(const QList<Pack> &installPacks, const QList<Pack> &updatePacks, const QList<Pack> &removePacks)
{
    const QList<Pack> &current = packManager()->installedPack();
    m_ToInstall = installPacks;
    m_ToUpdate = updatePacks;
    m_ToRemove = removePacks;

    foreach(const Pack &pack, installPacks) {
        Pack::DataType type = pack.dataType();
        if (!(type==Pack::DrugsWithInteractions ||
                type==Pack::DrugsWithoutInteractions ||
                type==Pack::ZipCodes ||
                type==Pack::ICD))
            continue;
        QVector<Pack::DataType> types;
        types << type;
        if (type==Pack::DrugsWithInteractions)
            types << Pack::DrugsWithoutInteractions;
        else if (type==Pack::DrugsWithoutInteractions)
            types << Pack::DrugsWithInteractions;
        for(int i=0; i<types.count(); ++i) {
            foreach(const Pack &inst, current) {
                if (inst.dataType()==types.at(i)) {
                    if (!m_ToRemove.contains(inst))
                        m_ToRemove << inst;
                }
            }
        }

    }

}

bool PackDependencyChecker::isCombinationCorrect() const
{
    return true;
}

QList<Pack> PackDependencyChecker::neededToInstall() const
{
    return m_ToInstall;
}

QList<Pack> PackDependencyChecker::neededToUpdate() const
{
    return m_ToUpdate;
}

QList<Pack> PackDependencyChecker::neededToRemove() const
{
    return m_ToRemove;
}
