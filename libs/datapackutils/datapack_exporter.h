/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATAPACKUTILSEXPORTER_H
#define DATAPACKUTILSEXPORTER_H

/**
  \namespace DataPack
  \brief Namespace for the DataPack management library
  Namespace for the DataPack management library
*/

#include <qglobal.h>

// DEFINE EXPORTER
#if defined(DATAPACKUTILS_LIBRARY)
#define DATAPACK_EXPORT Q_DECL_EXPORT
#else
#define DATAPACK_EXPORT Q_DECL_IMPORT
#endif

#endif // DATAPACKUTILSEXPORTER_H
