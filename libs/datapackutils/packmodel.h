/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATAPACK_PACKMODEL_H
#define DATAPACK_PACKMODEL_H

#include <datapackutils/pack.h>
#include <QAbstractTableModel>

/**
 * \file ./libs/datapackutils/packmodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DataPack {
class Pack;
namespace Internal {
class PackModelPrivate;
}

class PackModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    enum DataRepresentation {
        Label = 0,
        IsInstalled,
        IsAnUpdate,
        ColumnCount
    };

    explicit PackModel(QObject *parent = 0);
    ~PackModel();

    void setInstallChecker(const bool onOff);
    void setPackCheckable(const bool checkable);
    bool isDirty() const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex & = QModelIndex()) const {return ColumnCount;}

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

    Qt::ItemFlags flags(const QModelIndex &index) const;

    const Pack &packageAt(const int index) const;

    QList<Pack> packageToInstall() const;
    QList<Pack> packageToUpdate() const;
    QList<Pack> packageToRemove() const;

public Q_SLOTS:
    void updateModel();
    void filter(const QString &vendor, const QList<Pack::DataType> &type);

private Q_SLOTS:
    void onServerAdded(const int index);
    void onServerRemoved(const int index);
//    void onServerDescriptionAvailable();
    void onPackInstalled(const DataPack::Pack &pack);
    void onPackRemoved(const DataPack::Pack &pack);

private:
    Internal::PackModelPrivate *d;
};

}  // End namespace DataPack

#endif // DATAPACK_PACKMODEL_H
