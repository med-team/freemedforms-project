/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "httpserverengine.h"
#include <datapackutils/datapackcore.h>
#include <datapackutils/servermanager.h>

#include <utils/log.h>
#include <utils/global.h>
#include <utils/widgets/basiclogindialog.h>
#include <translationutils/constants.h>
#include <translationutils/trans_filepathxml.h>
#include <translationutils/trans_msgerror.h>
#include <quazip/global.h>

#include <QDir>
#include <QFile>
#include <QNetworkProxyQuery>
#include <QAuthenticator>
#include <QHash>

#include <QDebug>


using namespace DataPack;
using namespace Internal;
using namespace Trans::ConstantTranslations;

static inline DataPack::DataPackCore &core() { return DataPack::DataPackCore::instance(); }
static inline DataPack::Internal::ServerManager *serverManager() {return qobject_cast<ServerManager*>(core().serverManager());}

namespace {
    const int MAX_AUTHENTIFICATION_TRIES = 3;
    const char * const  ICONEYES = "eyes.png";

    static QString statusKey(const Pack &pack) {
        return pack.uuid()+pack.version();
    }

    static QString statusKey(const Server &server) {
        return server.uuid()+server.version();
    }

}

ReplyData::ReplyData(QNetworkReply *_reply, Server *_server, Server::FileRequested _fileType, const Pack &_pack, QProgressBar *_progBar) :
    reply(_reply),
    server(_server),
    bar(_progBar),
    pack(_pack),
    fileType(_fileType)
{
}

ReplyData::ReplyData(QNetworkReply *_reply, Server *_server, Server::FileRequested _fileType, QProgressBar *_progBar) :
    reply(_reply),
    server(_server),
    bar(_progBar),
    fileType(_fileType)
{
}


HttpServerEngine::HttpServerEngine(QObject *parent)  :
    IServerEngine(parent),
    m_DownloadCount_Server(0),
    m_DownloadCount_PackDescription(0)
{
    setObjectName("HttpServerEngine");
    m_NetworkAccessManager = new QNetworkAccessManager(this);

    connect(m_NetworkAccessManager, SIGNAL(authenticationRequired(QNetworkReply*,QAuthenticator*)), this, SLOT(authenticationRequired(QNetworkReply*,QAuthenticator*)));
    connect(m_NetworkAccessManager, SIGNAL(proxyAuthenticationRequired(QNetworkProxy,QAuthenticator*)), this, SLOT(proxyAuthenticationRequired(QNetworkProxy,QAuthenticator*)));
#if QT_VERSION >= 0x050000
    m_NetworkAccessManager->clearAccessCache();
#endif
}

HttpServerEngine::~HttpServerEngine()
{}

bool HttpServerEngine::managesServer(const Server &server)
{
    if (core().isInternetConnectionAvailable())
        return server.nativeUrl().startsWith("http://");
    return false;
}

void HttpServerEngine::addToDownloadQueue(const ServerEngineQuery &query)
{
    m_queue.append(query);
}

int HttpServerEngine::downloadQueueCount() const
{
    return m_queue.count();
}


bool HttpServerEngine::startDownloadQueue()
{
    if (!core().isInternetConnectionAvailable()) {
        LOG_ERROR("No internet connection available.");
        return false;
    }
    if (!QNetworkProxy::applicationProxy().hostName().isEmpty()) {
        m_NetworkAccessManager->setProxy(QNetworkProxy::applicationProxy());
        LOG("Using proxy: " + m_NetworkAccessManager->proxy().hostName());
    } else {
        m_NetworkAccessManager->setProxy(QNetworkProxy::NoProxy);
        LOG("Clearing proxy");
    }
    for(int i = 0; i < m_queue.count(); ++i) {
        const ServerEngineQuery &query = m_queue.at(i);
        Server *s = query.server;
        if (!managesServer(*s))
            continue;
        qWarning() << "HTTP:startDownloadQueue; server #" << i << s->nativeUrl()<<s->uuid() << s->version();

        QNetworkReply *reply = 0;

        if (query.downloadDescriptionFiles) {
            QNetworkRequest request = createRequest(s->url(Server::ServerConfigurationFile));
            ++m_DownloadCount_Server;
            qWarning() << "Downloading server config" << s->url(Server::ServerConfigurationFile);

            ServerEngineStatus status;
            m_ServerStatus.insert(statusKey(*s), status);

            reply = m_NetworkAccessManager->get(request);
            m_replyToData.insert(reply, ReplyData(reply, s, Server::ServerConfigurationFile, query.progressBar));
        } else if (query.downloadPackFile) {
            QNetworkRequest request = createRequest(s->url(Server::PackFile, query.pack->serverFileName()));
            qWarning() << "Downloading pack" << s->url(Server::PackFile, query.pack->serverFileName());

            ServerEngineStatus status;
            m_PackStatus.insert(statusKey(*s), status);

            reply = m_NetworkAccessManager->get(request);
            m_replyToData.insert(reply, ReplyData(reply, s, Server::PackFile, *query.pack, query.progressBar));

        }
        connect(reply, SIGNAL(readyRead()), this, SLOT(serverReadyRead()));
        connect(reply, SIGNAL(finished()), this, SLOT(serverFinished()));
        connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(serverError(QNetworkReply::NetworkError)));

        QProgressBar *bar = query.progressBar;
        if (bar) {
            bar->setRange(0, 100);
            bar->setValue(0);
            connect(reply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(downloadProgress(qint64,qint64)));
        }
    }
    return true;
}

int HttpServerEngine::runningJobs() const
{
    return m_replyToData.count();
}

bool HttpServerEngine::stopJobsAndClearQueue()
{
    m_queue.clear();
    for(int i=0; i < m_replyToData.count(); ++i) {
        ReplyData &data = m_replyToData[m_replyToData.keys().at(i)];
        qWarning() << i; // << data.reply;// << data.reply->url().toString();
        data.reply->abort();
        if (data.bar) {
            data.bar->setValue(100);
            data.bar->setToolTip(tr("Abort"));
        }
        data.reply->deleteLater();
    }
    m_replyToData.clear();
    return true;
}

void HttpServerEngine::downloadProgress(qint64 bytesRead, qint64 totalBytes)
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    QProgressBar *bar = m_replyToData[reply].bar;
    if (!bar) {
        disconnect(reply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(downloadProgress(qint64,qint64)));
        return;
    }
    if (totalBytes>0) {
        int v = bytesRead*100/totalBytes;
        bar->setValue(v);
    } else {
        bar->setValue(0);
    }
}

void HttpServerEngine::authenticationRequired(QNetworkReply *reply, QAuthenticator *authenticator)
{
    LOG("Server authentication required: " +  reply->url().toString());
    const QString &host = reply->url().toString();
    m_AuthTimes.insert(host, m_AuthTimes.value(host, 0) + 1);
    if (m_AuthTimes.value(host) > MAX_AUTHENTIFICATION_TRIES) {
        LOG_ERROR("Server authentication max tries achieved. " +  host);
        return;
    }
    const ReplyData &data = m_replyToData.value(reply);
    const Server *server = data.server;
    Q_ASSERT(server);
    if (!server)
        return;
    Utils::BasicLoginDialog dlg;
    dlg.setModal(true);
    QString serverName = server->url();
    if (!server->name().isEmpty())
        serverName += " - " + server->name();
    dlg.setTitle(tr("Server %1\nrequires an authentication").arg(serverName));
    QString html = QString("<p style=\"text-align: center\">Host: %1 <br /><span style=\"font-weight:bold; color:darkred\">%2</span></p>")
            .arg(reply->url().host())
            .arg(tr("If you don't have any login just cancel the dialog"));
    dlg.setHtmlExtraInformation(html);
    dlg.setToggleViewIcon(core().icon(ICONEYES));
    if (dlg.exec()==QDialog::Accepted) {
        authenticator->setUser(dlg.login());
        authenticator->setPassword(dlg.password());
    }
}

void HttpServerEngine::proxyAuthenticationRequired(const QNetworkProxy &proxy, QAuthenticator *authenticator)
{
    LOG("Proxy authentication required: " +  proxy.hostName());
    const QString &host = proxy.hostName();
    m_AuthTimes.insert(host, m_AuthTimes.value(host, 0) + 1);
    if (m_AuthTimes.value(host) > MAX_AUTHENTIFICATION_TRIES) {
        LOG_ERROR("Proxy authentication max tries achieved. " +  host);
        return;
    }
    if (!proxy.user().isEmpty() && !proxy.password().isEmpty()) {
        authenticator->setUser(proxy.user());
        authenticator->setPassword(proxy.password());
    } else {
        Utils::BasicLoginDialog dlg;
        dlg.setModal(true);
        dlg.setTitle(tr("Proxy authentication required"));
        dlg.setToggleViewIcon(core().icon(ICONEYES));
        if (dlg.exec()==QDialog::Accepted) {
            authenticator->setUser(dlg.login());
            authenticator->setPassword(dlg.password());
        }
    }
}

void HttpServerEngine::serverReadyRead()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    ReplyData &data = m_replyToData[reply];
    data.response.append(reply->readAll());
}

void HttpServerEngine::serverError(QNetworkReply::NetworkError error)
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    qWarning() << "serverError" << reply->url().toString() << error;
    ReplyData &data = m_replyToData[reply];
    reply->deleteLater(); // we don't need reply anymore
    ServerEngineStatus *status = getStatus(data);
    Q_ASSERT(status);
    status->hasError = true;
    status->isSuccessful = false;
    status->errorMessages << tr("Server error: %1").arg(reply->errorString());
    LOG_ERROR(tr("Server error: %1").arg(reply->errorString()));
    if (data.pack.isValid())
        Q_EMIT packDownloaded(data.pack, *status);
    --m_DownloadCount_Server;
}

void HttpServerEngine::serverFinished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    qWarning() << "HTTP: serverFinished" << reply->request().url() << reply->error();

    if (reply->error() != QNetworkReply::NoError) {
        reply->deleteLater(); // we don't need reply anymore
        m_replyToData.remove(reply);
        if (m_replyToData.isEmpty()) {
            m_queue.clear();
            Q_EMIT queueDowloaded();
        }
        return;
    }

    ReplyData &data = m_replyToData[reply];
    Q_ASSERT(data.server);
    data.server->setConnected(true);
    reply->deleteLater(); // we don't need reply anymore

    ServerEngineStatus *status = getStatus(data);
    Q_ASSERT(status);
    status->downloadCorrectlyFinished = true;
    status->serverIdentificationError = false;
    status->proxyIdentificationError = false;

    switch (data.fileType) {
    case Server::ServerConfigurationFile:
    {
        afterServerConfigurationDownload(data);
        --m_DownloadCount_Server;
        break;
    }
    case Server::PackDescriptionFile:
    {
        afterPackDescriptionFileDownload(data);
        --m_DownloadCount_PackDescription;
        break;
    }
    case Server::PackFile:
    {
        afterPackFileDownload(data);
        break;
    }
    default: break;
    }

    m_replyToData.remove(reply);

    if (m_DownloadCount_PackDescription==0 && m_DownloadCount_Server==0) {
        LOG("Queue downloaded");
        m_queue.clear();
        Q_EMIT queueDowloaded();
    }
}

ServerEngineStatus *HttpServerEngine::getStatus(const ReplyData &data)
{
    if (data.server)
        return &m_ServerStatus[statusKey(*data.server)];
    return &m_PackStatus[statusKey(data.pack)];
}

void HttpServerEngine::afterServerConfigurationDownload(const ReplyData &data)
{
    qWarning() << "afterServerConfigurationDownload" << data.server->uuid() << data.response.size();

    Server *server = data.server;
    ServerEngineStatus *status = getStatus(data);
    Q_ASSERT(status);

    if (data.response.size() == 0) {
        status->hasError = true;
        status->isSuccessful = false;
        status->downloadCorrectlyFinished = true;
        status->engineMessages << tkTr(Trans::Constants::FILE_1_ISEMPTY).arg(data.reply->url().toString());
        LOG_ERROR(tkTr(Trans::Constants::FILE_1_ISEMPTY).arg(data.reply->url().toString()));
        return;
    }

    QStringList packDescriptionToDownload;
    switch (server->urlStyle()) {
    case Server::Http:
    case Server::HttpPseudoSecuredNotZipped:
    {
        Utils::saveStringToFile(data.response, core().persistentCachePath() + QDir::separator() + server->uuid() + QDir::separator() + "server.conf.xml");
        server->fromXml(data.response);
        status->engineMessages << tr("Server description file successfully downloaded.");
        break;
    }
    case Server::HttpPseudoSecuredAndZipped:
    {
        QString unzipPath = core().persistentCachePath() + QDir::separator() + server->uuid();
        QString zipName = unzipPath + QDir::separator() + "serverconf.zip";
        QString error;
        if (!Utils::removeDirRecursively(unzipPath, &error))
            LOG_ERROR("Error while removing tmp dir: " + error);
        QDir().mkpath(unzipPath);
        QFile zip(zipName);
        if (!zip.open(QFile::WriteOnly)) {
            LOG_ERROR(tkTr(Trans::Constants::FILE_1_ISNOT_READABLE).arg(zip.fileName()));
            status->errorMessages << tr("Server description file is not readable.");
            status->hasError = true;
            status->isSuccessful = false;
            return;
        }
        zip.write(data.response);
        zip.close();

        if (!QuaZipTools::unzipFile(zipName)) {
            LOG_ERROR("Unable to unzip file: " + zipName);
            status->errorMessages << tr("Server description file can not be unzipped.");
            status->hasError = true;
            status->isSuccessful = false;
            return;
        }

        QString serverConfFile = QFileInfo(zipName).absolutePath() + QDir::separator() + Server::serverConfigurationFileName();
        server->fromXml(Utils::readTextFile(serverConfFile, Utils::DontWarnUser));

        foreach(const QString &file, server->content().packDescriptionFileNames()) {
            QFileInfo info(file);
            if (info.isRelative()) { // This must be always the case...
                info.setFile(QFileInfo(zipName).absolutePath() + QDir::separator() + file);
            }
            if (!info.exists()) {
                packDescriptionToDownload << file;
                continue;
            }
            createPackAndRegisterToServerManager(*server, info.absoluteFilePath());
        }
        break;
    }
    default: LOG_ERROR("Url type not managed.");
    }

    if (!packDescriptionToDownload.isEmpty()) {
        LOG(tr("Adding pack description file to the download queue."));
        status->engineMessages << tr("Adding pack description file to the download queue.");
        foreach(const QString &file, packDescriptionToDownload) {
            QNetworkRequest request = createRequest(server->url(Server::PackDescriptionFile, file));
            QNetworkReply *reply = m_NetworkAccessManager->get(request);
            m_replyToData.insert(reply, ReplyData(reply, server, Server::PackDescriptionFile));
            ++m_DownloadCount_PackDescription;
            connect(reply, SIGNAL(readyRead()), this, SLOT(serverReadyRead()));
            connect(reply, SIGNAL(finished()), this, SLOT(serverFinished()));
        }
    }
}

void HttpServerEngine::afterPackDescriptionFileDownload(const ReplyData &data)
{
    PackDescription desc;
    desc.fromXmlContent(data.response);
    ServerEngineStatus *status = getStatus(data);
    Q_ASSERT(status);
    status->engineMessages.append(tr("Pack description successfully downloaded."));
}

void HttpServerEngine::afterPackFileDownload(const ReplyData &data)
{
    ServerEngineStatus *status = getStatus(data);
    Q_ASSERT(status);
    status->engineMessages.append(tr("Pack successfully downloaded."));
    status->isSuccessful = true;
    status->hasError = false;

    const Pack &pack = data.pack;
    QFileInfo toPersistentCache(pack.persistentlyCachedZipFileName());
    if (toPersistentCache.exists()) {
        QFile::remove(pack.persistentlyCachedZipFileName());
        QFile::remove(pack.persistentlyCachedXmlConfigFileName());
    }

    QString newPath = toPersistentCache.absolutePath();
    QDir newDir(newPath);
    if (!newDir.exists()) {
        QDir().mkpath(newPath);
    }

    QFile out(toPersistentCache.absoluteFilePath());
    if (!out.open(QFile::WriteOnly)) {
        LOG_ERROR(tkTr(Trans::Constants::FILE_1_CAN_NOT_BE_CREATED).arg(toPersistentCache.absoluteFilePath()));
        status->engineMessages.append(tr("Pack file can not be created in the persistent cache."));
        status->hasError = true;
        status->isSuccessful = false;
        return;
    }
    LOG("Writing pack content to " + toPersistentCache.absoluteFilePath());
    out.write(data.response);
    out.close();

    QFile::copy(pack.originalXmlConfigFileName(), pack.persistentlyCachedXmlConfigFileName());

    Q_EMIT packDownloaded(pack, *status);
}


void HttpServerEngine::createPackAndRegisterToServerManager(const Server &server, const QString &pathToPackDescription)
{
    Pack p;
    p.fromXmlFile(pathToPackDescription);
    serverManager()->registerPack(server, p);
}

QNetworkRequest HttpServerEngine::createRequest(const QString &url)
{
    QNetworkRequest request(url);
    request.setRawHeader("User-Agent", QString("FreeMedForms:%1;%2")
                         .arg(qApp->applicationName())
                         .arg(qApp->applicationVersion()).toUtf8());
    return request;
}


const ServerEngineStatus &HttpServerEngine::lastStatus(const Pack &pack)
{
    const QString &key = statusKey(pack);
    return m_PackStatus[key];
}


const ServerEngineStatus &HttpServerEngine::lastStatus(const Server &server)
{
    const QString &key = statusKey(server);
    return m_ServerStatus[key];
}
