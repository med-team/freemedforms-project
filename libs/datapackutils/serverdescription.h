/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATAPACK_SERVERDESCRIPTION_H
#define DATAPACK_SERVERDESCRIPTION_H

#include <datapackutils/datapack_exporter.h>
#include <utils/genericdescription.h>

#include <QVariant>
#include <QList>
QT_BEGIN_NAMESPACE
class QTreeWidget;
QT_END_NAMESPACE

/**
 * \file ./libs/datapackutils/serverdescription.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DataPack {
namespace Internal {
}

class DATAPACK_EXPORT ServerDescription : public Utils::GenericDescription
{
public:
    enum ExtraNonTranslatedData {
        RecommendedUpdateFrequency = Utils::GenericDescription::NonTranslatableExtraData + 1,
        RequiereAuthentification
    };

    ServerDescription();
    ~ServerDescription();

    QString toXml() const;

private:
};

}  // End namespace DataPack


#endif // DATAPACK_SERVERDESCRIPTION_H
