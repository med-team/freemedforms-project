/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "pack.h"
#include "datapackcore.h"

#include <utils/log.h>
#include <utils/global.h>
#include <translationutils/constants.h>
#include <translationutils/trans_current.h>
#include <translationutils/trans_drugs.h>
#include <translationutils/trans_msgerror.h>

#include <QDomDocument>
#include <QDomElement>
#include <QDir>

enum {
    DebugEqualityOperator = false
};

using namespace DataPack;
using namespace Trans::ConstantTranslations;

static inline DataPack::DataPackCore &core() {return DataPack::DataPackCore::instance();}

namespace {
const char *const TAG_ROOT = "DataPack_Pack";
const char *const TAG_PACKDESCRIPTION = "PackDescription";
const char *const TAG_PACKDEPENDENCIES = "PackDependencies";
}

Pack::Pack() :
    m_type(-1)
{
}

Pack::~Pack()
{
}


bool Pack::isValid() const
{
    return (!uuid().isEmpty() &&
            !version().isEmpty() &&
            !m_descr.data(PackDescription::Label).toString().isEmpty());
}

QString Pack::uuid() const
{
    return m_descr.data(PackDescription::Uuid).toString();
}

QString Pack::version() const
{
    return m_descr.data(PackDescription::Version).toString();
}

QString Pack::name() const
{
    return m_descr.data(PackDescription::Label).toString();
}

QString Pack::vendor() const
{
    const QString &v = m_descr.data(PackDescription::Vendor).toString();
    if (v.isEmpty())
        return tkTr(Trans::Constants::THE_FREEMEDFORMS_COMMUNITY);
    return v;
}


QString Pack::serverFileName() const
{
    if (m_descr.data(PackDescription::AbsFileName).toString().isEmpty())
        LOG_ERROR_FOR("Pack", "Pack does not have a defined server filename. Xml tag 'file' missing");
    return m_descr.data(PackDescription::AbsFileName).toString();
}

QString Pack::serverLicenseFileName() const
{
    return QString();
}


QString Pack::md5ControlChecksum() const
{
    return m_descr.data(PackDescription::Md5).toString();
}


QString Pack::sha1ControlChecksum() const
{
    return m_descr.data(PackDescription::Sha1).toString();
}

bool Pack::isSha1Checked() const
{
    return false;
}

bool Pack::isMd5Checked() const
{
    return false;
}


QString Pack::originalXmlConfigFileName() const
{
    return m_OriginalFileName;
}


QString Pack::persistentlyCachedXmlConfigFileName() const
{
    return core().persistentCachePath() + QDir::separator() + uuid() + QDir::separator() + "packconfig.xml";
}


QString Pack::persistentlyCachedZipFileName() const
{
    return core().persistentCachePath() + QDir::separator() + uuid() + QDir::separator() + QFileInfo(serverFileName()).fileName();
}


QString Pack::unzipPackToPath() const
{
    QString zipPath = m_descr.data(PackDescription::UnzipToPath).toString();
    if (core().containsPathTag(zipPath))
        zipPath = core().replacePathTag(zipPath);
    else
        zipPath.prepend(core().installPath() + QDir::separator());
    return zipPath;
}


QString Pack::installedXmlConfigFileName() const
{
    return core().installPath() + QDir::separator() + "packconfig" + QDir::separator() + uuid() + QDir::separator() + "packconfig.xml";
}

void Pack::setInstalledFiles(const QStringList &list)
{
    m_descr.setData(PackDescription::InstalledFiles, list.join("@@"));
}

QStringList Pack::installedFiles() const
{
    const QString &inst = m_descr.data(PackDescription::InstalledFiles).toString();
    QStringList draft = inst.split("@@");
    draft.removeAll("");
    QStringList list;
    foreach(QString s, draft) {
        s.prepend(unzipPackToPath() + QDir::separator());
        list << s;
    }
    return list;
}

Pack::DataType Pack::dataType() const
{
    if (m_type!=-1)
        return Pack::DataType(m_type);
    const QString &type = m_descr.data(PackDescription::DataType).toString();
    if (type.compare("FormsFullSet", Qt::CaseInsensitive)==0)
        m_type = Pack::FormSubset;
    else if (type.compare("SubForms", Qt::CaseInsensitive)==0)
        m_type = Pack::SubForms;
    else if (type.compare("DrugsWithInteractions", Qt::CaseInsensitive)==0)
        m_type = Pack::DrugsWithInteractions;
    else if (type.compare("DrugsWithoutInteractions", Qt::CaseInsensitive)==0)
        m_type = Pack::DrugsWithoutInteractions;
    else if (type.compare("icd", Qt::CaseInsensitive)==0)
        m_type = Pack::ICD;
    else if (type.compare("ZipCodes", Qt::CaseInsensitive)==0)
        m_type = Pack::ZipCodes;
    else if (type.compare("UserDocuments", Qt::CaseInsensitive)==0)
        m_type = Pack::UserDocuments;
    else if (type.compare("Accountancy", Qt::CaseInsensitive)==0)
        m_type = Pack::Accountancy;
    else if (type.compare("Account", Qt::CaseInsensitive)==0)
        m_type = Pack::Accountancy;
    else if (type.compare("AlertPack", Qt::CaseInsensitive)==0
             || type.compare("AlertPacks", Qt::CaseInsensitive)==0)
        m_type = Pack::AlertPacks;
    else if (type.compare("Binaries", Qt::CaseInsensitive)==0)
        m_type = Pack::Binaries;
    else
        m_type = Pack::UnknownType;
    return Pack::DataType(m_type);
}

QString Pack::dataTypeName() const
{
    Pack::DataType type = dataType();
    switch (type) {
    case Pack::FormSubset: return tkTr(Trans::Constants::FORMS);
    case Pack::SubForms: return tkTr(Trans::Constants::SUBFORMS);
    case Pack::DrugsWithInteractions: return tkTr(Trans::Constants::DRUGS_WITH_INTERACTIONS);
    case Pack::DrugsWithoutInteractions: return tkTr(Trans::Constants::DRUGS_WITHOUT_INTERACTIONS);
    case Pack::ICD: return tkTr(Trans::Constants::ICD10);
    case Pack::ZipCodes: return tkTr(Trans::Constants::ZIP_CODES);
    case Pack::UserDocuments: return tkTr(Trans::Constants::USER_DOCUMENTS);
    case Pack::AlertPacks: return tkTr(Trans::Constants::ALERT_PACKS);
    case Pack::Binaries: return tkTr(Trans::Constants::BINARY_PACKS);
    default: return tkTr(Trans::Constants::UNKNOWN);
    }
    return tkTr(Trans::Constants::UNKNOWN);
}


bool Pack::fromXmlFile(const QString &absFileName)
{
    m_OriginalFileName = absFileName;
    return readXml(Utils::readTextFile(absFileName, Utils::DontWarnUser));
}


bool Pack::readXml(const QString &fullPackConfigXml)
{
    QDomDocument doc;
    QString error;
    int line, col;
    if (!doc.setContent(fullPackConfigXml, &error, &line, &col)) {
        LOG_ERROR_FOR("DataPack::Pack", tkTr(Trans::Constants::ERROR_1_LINE_2_COLUMN_3).arg(error).arg(line).arg(col));
        return false;
    }
    QDomElement root = doc.firstChildElement(::TAG_ROOT);
    QDomElement descr = root.firstChildElement(::TAG_PACKDESCRIPTION);
    QDomElement dep = root.firstChildElement(::TAG_PACKDEPENDENCIES);
    if (!m_descr.fromDomElement(descr))
        return false;
    if (!m_depends.fromDomElement(dep))
        return false;
    return true;
}

QString Pack::toXml() const
{
    QDomDocument doc("FreeMedForms");
    QDomElement element = doc.createElement(::TAG_ROOT);
    doc.appendChild(element);
    if (!m_descr.toDomElement(&element, &doc))
        LOG_ERROR_FOR("Pack", "Unable to write PackDescription XML content to QDomDocument");
    if (!m_depends.toDomElement(&element, &doc))
        LOG_ERROR_FOR("Pack", "Unable to write PackDependencies XML content to QDomDocument");
    return QString("<?xml version='1.0' encoding='UTF-8'?>\n" + doc.toString(2));
}


bool Pack::operator==(const Pack &other) const
{
    if (this->uuid() != other.uuid()) {
        if (DebugEqualityOperator)
            LOG_FOR("Pack", "Uuid mismatch");
        return false;
    }
    if (this->version() != other.version()) {
        if (DebugEqualityOperator)
            LOG_FOR("Pack", "version mismatch");
        return false;
    }
    if (this->vendor() != other.vendor()) {
        if (DebugEqualityOperator)
            LOG_FOR("Pack", "vendor mismatch");
        return false;
    }
    if (this->name() != other.name()) {
        if (DebugEqualityOperator)
            LOG_FOR("Pack", "name mismatch");
        return false;
    }
    if (this->description() != other.description()) {
        if (DebugEqualityOperator)
            LOG_FOR("Pack", "PackDescription mismatch");
        return false;
    }
    return true;
}

QDebug operator<<(QDebug dbg, const DataPack::Pack &p)
{
    dbg.nospace() << "Pack("
                  << p.uuid()
                  << ", "
                  << p.version()
                  << ", "
                  << p.serverFileName()
                  << ")";
    return dbg.space();
}


