/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATAPACK_PACKCATEGORIESMODEL_H
#define DATAPACK_PACKCATEGORIESMODEL_H

#include <datapackutils/pack.h>
#include <QStandardItemModel>

/**
 * \file ./libs/datapackutils/packcategoriesmodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DataPack {
namespace Internal {
class PackCategoriesModelPrivate;
}

class PackCategoriesModel : public QStandardItemModel
{
    Q_OBJECT
public:
    enum DataRepresentation {
        Label = 0,
        ColumnCount
    };

    explicit PackCategoriesModel(QObject *parent = 0);
    ~PackCategoriesModel();

    int columnCount(const QModelIndex & = QModelIndex()) const {return ColumnCount;}
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QList<Pack::DataType> datatype(const QModelIndex &index) const;
    QString vendor(const QModelIndex &index) const;

public Q_SLOTS:
    void updateModel();

private Q_SLOTS:
    void onServerAdded(const int index);
    void onServerRemoved(const int index);

private:
    Internal::PackCategoriesModelPrivate *d;
};

}  // End namespace DataPack

#endif // DATAPACK_PACKCATEGORIESMODEL_H
