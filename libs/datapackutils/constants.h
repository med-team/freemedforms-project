/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef LIBS_DATAPACKS_CONSTANTS_H
#define LIBS_DATAPACKS_CONSTANTS_H

/**
 * \file ./libs/datapackutils/constants.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DataPack {
namespace Constants {

const char * const PACKDESCRIPTION_FILENAME = "packdescription.xml";
const char * const PACKCREATIONQUEUE_DEFAULT_FILENAME = "packcreation.xml";

// Server internal UID
const char *const SERVER_COMMUNITY_FREE = "comm_free";
const char *const SERVER_ASSO_FREE      = "asso_free";
const char *const SERVER_COMMUNITY_NONFREE = "comm_nonfree";
const char *const SERVER_ASSO_NONFREE   = "asso_nonfree";

} // namespace Constants
} // namespace DataPack


#endif // LIBS_DATAPACKS_CONSTANTS_H
