/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef SERVERIDENTIFICATION_H
#define SERVERIDENTIFICATION_H

#include <datapackutils/datapack_exporter.h>

#include <QString>

/**
 * \file ./libs/datapackutils/serveridentification.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace DataPack {

class DATAPACK_EXPORT ServerIdentification
{
public:
    ServerIdentification();

    void setLogin(const QString &login) {m_login=login;}
    void setUserName(const QString &user) {m_userName=user;}
    void setPrivateKey(const QString &key) {m_privateKey=key;}

    QString login() const {return m_login;}
    QString userName() const {return m_userName;}
    QString privateKey() const {return m_privateKey;}

private:
    QString m_login;
    QString m_userName;
    QString m_privateKey;

};

}  // End namespace DataPack

#endif // SERVERIDENTIFICATION_H
