/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "packmodel.h"
#include <datapackutils/datapackcore.h>
#include <datapackutils/servermanager.h>
#include <datapackutils/packmanager.h>

#include <coreplugin/constants_tokensandsettings.h>

#include <utils/log.h>
#include <utils/versionnumber.h>

#include <translationutils/constants.h>
#include <translationutils/trans_current.h>
#include <translationutils/trans_msgerror.h>
#include <translationutils/trans_menu.h>
#include <translationutils/trans_spashandupdate.h>

#include <QApplication>
#include <QFont>
#include <QString>
#include <QIcon>

#include <QDebug>

using namespace DataPack;
using namespace Trans::ConstantTranslations;

static inline DataPack::DataPackCore &core() { return DataPack::DataPackCore::instance(); }
static inline Internal::ServerManager *serverManager() { return qobject_cast<Internal::ServerManager*>(core().serverManager()); }
static inline Internal::PackManager *packManager() { return qobject_cast<Internal::PackManager*>(core().packManager()); }
static inline QIcon icon(const QString &name, DataPack::DataPackCore::ThemePath path = DataPack::DataPackCore::MediumPixmaps) { return QIcon(core().icon(name, path)); }

namespace {

const char *const ICON_PACKAGE = "package.png";

struct PackItem {
    PackItem(const Pack &p) :
        pack(p),
        isInstalled(false), isAnUpdate(false),
        fromServerId(-1),
        userCheckState(Qt::Unchecked)
    {}

    bool isAvailableOnServer() const {return fromServerId>=0;}

    Pack pack;
    bool isInstalled, isAnUpdate;
    int fromServerId;
    Qt::CheckState userCheckState;
};

static QString packToHtml(const PackItem &item)
{
    QString inst, color;
    color = "gray";
    if (item.isInstalled) {
        if (item.userCheckState!=Qt::Checked) {
            inst = QCoreApplication::translate("Datapack::PackModel", "Deletion requested");
            color = "red";
        } else {
            inst = tkTr(Trans::Constants::CURRENTLY_INSTALLED);
        }
    } else {
        if (item.userCheckState==Qt::Checked) {
            inst = QCoreApplication::translate("Datapack::PackModel", "Installation requested");
            color = "red";
        } else if (item.isAnUpdate) {
            if (item.userCheckState==Qt::Unchecked) {
                inst = QCoreApplication::translate("Datapack::PackModel", "Deletion requested");
            } else {
                inst = tkTr(Trans::Constants::UPDATE_AVAILABLE);
            }
            color = "blue";
        }
    }
    if (!inst.isEmpty()) {
        inst.prepend(QString("<span style=\"color:%1; font-size:small\">&nbsp;").arg(color));
        inst.append("</span>");
    }
    return QString("<span style=\"color:black;font-weight:bold\">%1</span><br />"
                   "<span style=\"color:gray; font-size:small\">%2: %3</span>%4")
            .arg(item.pack.name())
            .arg(tkTr(Trans::Constants::VERSION))
            .arg(item.pack.description().data(PackDescription::Version).toString())
            .arg(inst);
}

static QString packTooltip(const PackItem &item)
{
    const PackDescription &descr = item.pack.description();
    return QString("<p style=\"font-weight:bold;font-size:large;\">%1</p>"
                   "<p style=\"font-size:small;margin-left:20px;color:gray\">"
                   "%2: %3<br />"
                   "%4: %5<br />"
                   "%6: %7<br />"
                   "%8: %9<br />"
                   "%10: %11"
                   "</p>"
                   )
            .arg(descr.data(PackDescription::Label).toString().replace(" ","&nbsp;"))
            .arg(tkTr(Trans::Constants::VERSION))
            .arg(descr.data(PackDescription::Version).toString())
            .arg(tkTr(Trans::Constants::LAST_MODIFICATION))
            .arg(QLocale().toString(descr.data(PackDescription::LastModificationDate).toDate(), QLocale::LongFormat).replace(" ","&nbsp;"))
            .arg(tkTr(Trans::Constants::AUTHOR))
            .arg(descr.data(PackDescription::Author).toString())
            .arg(tkTr(Trans::Constants::VENDOR))
            .arg(item.pack.vendor())
            .arg(tkTr(Trans::Constants::DATA_TYPE))
            .arg(item.pack.dataTypeName())
            ;
}


}  //  End namespace anonymous

namespace DataPack {
namespace Internal {
class PackModelPrivate
{
public:
    PackModelPrivate() :
        m_InstallChecking(false),
        m_PackCheckable(false)
    {}

    void scanServerPack(const int index)
    {
        foreach(const Pack &p, serverManager()->getPackForServer(serverManager()->getServerAt(index))) {
            if (!p.isValid())
                continue;
            if (m_AvailPacks.contains(p))
                continue;
            m_AvailPacks << p;
        }
    }

    int highestVersionPack(const QString &packUuid)
    {
        Utils::VersionNumber highest("0.0.0");
        int id = -1;
        for(int i=0; i < m_AvailPacks.count(); ++i) {
            const Pack &p = m_AvailPacks.at(i);
            if (p.uuid() == packUuid) {
                Utils::VersionNumber testing(p.version());
                if (testing > highest) {
                    highest = testing;
                    id = i;
                }
            }
        }
        return id;
    }

    void createModelContent()
    {
        const QList<Pack> &installedPacks = packManager()->installedPack();
        for(int i=0; i < serverManager()->serverCount(); ++i) {
            scanServerPack(i);
        }

        foreach(const Pack &p, installedPacks) {
            if (m_AvailPacks.contains(p))
                continue;
            m_AvailPacks << p;
        }

        int appId = PackDescription::FreeMedFormsCompatVersion;
        if (qApp->applicationName().contains("freediams", Qt::CaseInsensitive)) {
            appId = PackDescription::FreeDiamsCompatVersion;
        } else if (qApp->applicationName().contains("freeaccount", Qt::CaseInsensitive)) {
            appId = PackDescription::FreeAccountCompatVersion;
        }
        Utils::VersionNumber appVersion(qApp->applicationVersion());
        for(int i = m_AvailPacks.count()-1; i >= 0; --i) {
            const Pack &p = m_AvailPacks.at(i);
            Utils::VersionNumber packCompatVersion(p.description().data(appId).toString());
            if (appVersion < packCompatVersion) // appVersion >= packVersion -> keep it
                m_AvailPacks.removeAt(i);
        }

        QList<int> idInUse; // id of Pack to use (from m_AvailPacks)
        QStringList processed;
        for(int i=0; i < m_AvailPacks.count(); ++i) {
            const Pack &p = m_AvailPacks.at(i);
            if (processed.contains(p.uuid()))
                continue;
            idInUse << highestVersionPack(p.uuid());
            processed << p.uuid();
        }


        foreach(int id, idInUse) {
            const Pack &p = m_AvailPacks.at(id);
            PackItem item(p);
            item.isInstalled = packManager()->isDataPackInstalled(p);
            if (!item.isInstalled) {
                bool installedWithLowerVersion = packManager()->isDataPackInstalled(p.uuid());
                if (installedWithLowerVersion) {
                    item.isInstalled = false;
                    item.isAnUpdate = true;
                    item.userCheckState = Qt::PartiallyChecked;
                }
            } else {
                item.userCheckState = Qt::Checked;
            }
            m_Items << item;
        }
    }

    void serverAdded(const int index)
    {
        Q_UNUSED(index);
        m_Items.clear();
        m_AvailPacks.clear();
        createModelContent();
    }

    void serverRemoved(const int index)
    {
        Q_UNUSED(index);
        m_Items.clear();
        m_AvailPacks.clear();
        createModelContent();
    }

public:
    bool m_InstallChecking, m_PackCheckable;
    QList<PackItem> m_Items;    // represents the non filtered model (all packages are shown)
    QList<Pack> m_AvailPacks;
    Pack m_InvalidPack;
    QList<int> rowToItem;       // when filtering the model, the list is populated with the item to show. If empty == not filtered
    QString _filterVendor;
    QList<Pack::DataType> _filterDataType;
};
}
}


PackModel::PackModel(QObject *parent) :
    QAbstractTableModel(parent),
    d(new Internal::PackModelPrivate)
{
    setObjectName("DataPack::PackModel");
    d->createModelContent();
    connect(serverManager(), SIGNAL(serverAboutToBeRemoved(int)), this, SLOT(onServerRemoved(int)));
    connect(serverManager(), SIGNAL(allServerDescriptionAvailable()), this, SLOT(updateModel()));
    connect(packManager(), SIGNAL(packInstalled(DataPack::Pack)), this, SLOT(onPackInstalled(DataPack::Pack)));
    connect(packManager(), SIGNAL(packRemoved(DataPack::Pack)), this, SLOT(onPackRemoved(DataPack::Pack)));
}

PackModel::~PackModel()
{
    if (d) {
        delete d;
        d = 0;
    }
}

void PackModel::setInstallChecker(const bool onOff)
{
    beginResetModel();
    d->m_InstallChecking = onOff;
    endResetModel();
}

void PackModel::setPackCheckable(const bool checkable)
{
    beginResetModel();
    d->m_PackCheckable = checkable;
    endResetModel();
}

bool PackModel::isDirty() const
{
    foreach(const PackItem &item, d->m_Items) {
        if (item.isInstalled && item.userCheckState!=Qt::Checked)
            return true;
        if (item.isAnUpdate && item.userCheckState!=Qt::PartiallyChecked)
            return true;
        if (!item.isInstalled && item.userCheckState==Qt::Checked)
            return true;
    }
    return false;
}

int PackModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) return 0;

    if (!d->rowToItem.isEmpty())
        return d->rowToItem.count();
    return d->m_Items.count();
}

QVariant PackModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();
    if (!d->rowToItem.isEmpty())
        row = d->rowToItem.at(row);
    if (row < 0 || row >= d->m_Items.count())
        return QVariant();

    if (role == Qt::DisplayRole) {
        switch (index.column()) {
        case Label: return packToHtml(d->m_Items.at(row));
        case IsInstalled: return d->m_Items.at(row).isInstalled;
        case IsAnUpdate: return d->m_Items.at(row).isAnUpdate;
        }
    } else if (role == Qt::ToolTipRole && index.column()==Label) {
        return packTooltip(d->m_Items.at(row));
    } else if (d->m_PackCheckable && role==Qt::CheckStateRole) {
        return d->m_Items.at(row).userCheckState;
    } else if (role == Qt::DecorationRole) {
        QString iconFileName = d->m_Items.at(row).pack.description().data(PackDescription::GeneralIcon).toString();
        if (iconFileName.startsWith(Core::Constants::TAG_APPLICATION_THEME_PATH))
            iconFileName = iconFileName.remove(Core::Constants::TAG_APPLICATION_THEME_PATH);
        if (!iconFileName.isEmpty())
            return icon(iconFileName);
    }
    return QVariant();
}

bool PackModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    int row = index.row();
    if (!d->rowToItem.isEmpty())
        row = d->rowToItem.at(row);
    if (row < 0 || row >= d->m_Items.count())
        return false;

    if (d->m_PackCheckable && role==Qt::CheckStateRole && index.column()==Label) {
        if (flags(index) & Qt::ItemIsTristate) {
            int v = (d->m_Items[row].userCheckState + 1) % 3;
            d->m_Items[row].userCheckState = Qt::CheckState(v);
        } else {
            d->m_Items[row].userCheckState = Qt::CheckState(value.toInt());
        }

        Q_EMIT dataChanged(index, index);
        return true;
    }
    return false;
}

Qt::ItemFlags PackModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags f = QAbstractTableModel::flags(index);
    if (d->m_PackCheckable && index.column()==Label) {
        f |= Qt::ItemIsUserCheckable;

        int row = index.row();
        if (!d->rowToItem.isEmpty())
            row = d->rowToItem.at(row);
        if (row < 0 || row >= d->m_Items.count())
            return f;
        if (d->m_Items.at(row).isAnUpdate)
            f |= Qt::ItemIsTristate;
    }
    return f;
}

const Pack &PackModel::packageAt(const int index) const
{
    int row = index;
    if (!d->rowToItem.isEmpty())
        row = d->rowToItem.at(row);
    if (row < 0 || row >= d->m_Items.count())
        return d->m_InvalidPack;

    return d->m_Items.at(row).pack;
}

QList<Pack> PackModel::packageToInstall() const
{
    QList<Pack> toReturn;
    foreach(const PackItem &it, d->m_Items) {
        if (!it.isInstalled && !it.isAnUpdate && it.userCheckState==Qt::Checked)
            toReturn << it.pack;
    }
    return toReturn;
}

QList<Pack> PackModel::packageToUpdate() const
{
    QList<Pack> toReturn;
    foreach(const PackItem &it, d->m_Items) {
        if (it.isAnUpdate && it.userCheckState==Qt::Checked)
            toReturn << it.pack;
    }
    return toReturn;
}

QList<Pack> PackModel::packageToRemove() const
{
    QList<Pack> toReturn;
    foreach(const PackItem &it, d->m_Items) {
        if (it.isInstalled && it.userCheckState!=Qt::Checked)
            toReturn << it.pack;
    }
    return toReturn;
}

void PackModel::updateModel()
{
    beginResetModel();
    d->m_Items.clear();
    d->m_AvailPacks.clear();
    d->createModelContent();
    filter(d->_filterVendor, d->_filterDataType);
    endResetModel();
}

void PackModel::filter(const QString &vendor, const QList<Pack::DataType> &types)
{
    beginResetModel();
    d->rowToItem.clear();
    if (types.isEmpty() && vendor.isEmpty()) {
        d->_filterVendor.clear();
        d->_filterDataType = types;
        endResetModel();
        return;
    }

    for(int i=0; i < d->m_Items.count(); ++i) {
        const PackItem &item = d->m_Items.at(i);
        if (item.pack.vendor() == vendor && (types.contains(item.pack.dataType())))
            d->rowToItem << i;
    }
    d->_filterVendor = vendor;
    d->_filterDataType = types;

    endResetModel();
}

void PackModel::onServerAdded(const int index)
{
    d->serverAdded(index);
    filter(d->_filterVendor, d->_filterDataType);
}

void PackModel::onServerRemoved(const int index)
{
    Q_UNUSED(index);
    d->serverRemoved(index);
    filter(d->_filterVendor, d->_filterDataType);
}

void PackModel::onPackInstalled(const DataPack::Pack &pack)
{
    for(int i=0; i < d->m_Items.count(); ++i) {
        PackItem &item = d->m_Items[i];
        if (item.pack != pack)
            continue;
        item.isInstalled = true;
        item.isAnUpdate = false;
        Q_EMIT dataChanged(index(i, Label), index(i, IsInstalled));
        break;
    }
}

void PackModel::onPackRemoved(const DataPack::Pack &pack)
{
    for(int i=0; i < d->m_Items.count(); ++i) {
        PackItem &item = d->m_Items[i];
        if (item.pack != pack)
            continue;
    }
}
