/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric MAEKER, MD <eric.maeker@gmail.com>             *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef GOOGLETRANSLATOR_H
#define GOOGLETRANSLATOR_H

#include <translationutils/translationutils_exporter.h>
#include <QObject>
#include <QHash>
class QNetworkReply;
class QNetworkAccessManager;
#include <QUrl>

namespace Utils {

class TRUTILS_EXPORT GoogleTranslator : public QObject
{
    Q_OBJECT
public:
    explicit GoogleTranslator(QObject *parent = 0);
    int setProxy(const QString &host, int port, const QString &username, const QString &password);

public Q_SLOTS:
    void replyFinished(QNetworkReply *reply);
    void textTranslated();
    void startTranslation(const QString &from,const QString &to, const QString &text, const QString &uid = QString::null);

Q_SIGNALS:
    void translationComplete(const QString &text);
    void translationCompleteWithUid(const QString &text, const QString &uid);

private:
    QNetworkAccessManager *manager;
    QHash<QString, QUrl> uidToUrl;
};

}  // End namespace Utils

#endif // GOOGLETRANSLATOR_H
