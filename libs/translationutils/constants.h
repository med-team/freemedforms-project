/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CONSTANTTRANSLATIONS_GLOBAL_H
#define CONSTANTTRANSLATIONS_GLOBAL_H

#include <translationutils/translationutils_exporter.h>

#include <QString>
#include <QStringList>
#include <QCoreApplication>

namespace Trans {
namespace Constants {

const char* const CONSTANTS_TR_CONTEXT = "tkConstants";
const char* const CONSTANTS_TRANSLATOR_NAME = "lib_translations";

// For multilingual class template
const char* const ALL_LANGUAGE = "xx";
const char* const ALL_LANGUAGE_TEXT = QT_TRANSLATE_NOOP("tkConstants", "All languages");
const char* const ALL_OTHER_LANGUAGES_TEXT = QT_TRANSLATE_NOOP("tkConstants", "All other languages");

// App dateFormat for editors

//TODO: Why is this hardcoded as "MM dd yyyy" which is Canadian, and not taken from the system settings?
//: This date format will be used in every QDateEdit only
const char* const DATEFORMAT_FOR_EDITOR = QT_TRANSLATE_NOOP("tkConstants", "MM dd yyyy");
const char* const DATEFORMAT_FOR_MODEL = QT_TRANSLATE_NOOP("tkConstants", "MM-dd-yyyy");

//: This date time format will be used in every QDateTimeEdit only
const char* const DATETIMEFORMAT_FOR_EDITOR = QT_TRANSLATE_NOOP("tkConstants", "MM dd yyyy hh:mm");

//: This time format will be used in every QTimeEdit only
const char* const TIMEFORMAT_FOR_EDITOR = QT_TRANSLATE_NOOP("tkConstants", "hh:mm");

} // end Constants


namespace ConstantTranslations {

TRUTILS_EXPORT QString tkTr(const char* toTr, const int plurials=1);

TRUTILS_EXPORT QString checkUpdateLabel(const int index);
TRUTILS_EXPORT QStringList checkUpdateLabels();

TRUTILS_EXPORT QStringList periods();
TRUTILS_EXPORT QString period(int id);
TRUTILS_EXPORT QString periodPlurialForm(int id, int nb = 1, const QString &defaultValue = QString::null);
TRUTILS_EXPORT QStringList dailySchemeList();
TRUTILS_EXPORT QStringList dailySchemeXmlTagList();
TRUTILS_EXPORT QString dailyScheme(const int dailySchemeFlag);
TRUTILS_EXPORT QStringList preDeterminedAges();
TRUTILS_EXPORT QStringList mealTime();
TRUTILS_EXPORT QString mealTime(const int index);

TRUTILS_EXPORT QStringList genders();
TRUTILS_EXPORT QStringList titles();

} // end  ConstantTranslations

} // end TranslationUtils

#endif // CONSTANTTRANSLATIONS_GLOBAL_H
