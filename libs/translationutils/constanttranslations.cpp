/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "constanttranslations.h"

#include <QLocale>
#include <QDebug>

using namespace Trans;
using namespace Trans::ConstantTranslations;

namespace Trans {
namespace ConstantTranslations {


QString tkTr(const char* toTr, const int plurials)
{
    Q_UNUSED(plurials);
    return QCoreApplication::translate(Constants::CONSTANTS_TR_CONTEXT, toTr);
}

QString checkUpdateLabel(const int index)
{
    switch (index) {
    case Constants::CheckUpdate_AtStartup: return tkTr(Constants::AT_STARTUP);
    case Constants::CheckUpdate_EachWeeks:return tkTr(Constants::EACH_WEEK);
    case Constants::CheckUpdate_EachMonth:return tkTr(Constants::EACH_MONTH);
    case Constants::CheckUpdate_EachQuarters:return tkTr(Constants::EACH_QUARTER);
    case Constants::CheckUpdate_Never:return tkTr(Constants::NEVER_AUTOMATICALLY);
    }
    return QString();
}

QStringList checkUpdateLabels()
{
    return QStringList() << tkTr(Constants::AT_STARTUP)
                         << tkTr(Constants::EACH_WEEK)
                         << tkTr(Constants::EACH_MONTH)
                         << tkTr(Constants::EACH_QUARTER)
                         << tkTr(Constants::NEVER_AUTOMATICALLY);
}

QStringList periods()
{
    return  QStringList()
            << tkTr(Constants::SECOND_S)
            << tkTr(Constants::MINUTE_S)
            << tkTr(Constants::HOUR_S)
            << tkTr(Constants::DAY_S)
            << tkTr(Constants::WEEK_S)
            << tkTr(Constants::MONTH_S)
            << tkTr(Constants::QUARTER_S)
            << tkTr(Constants::YEAR_S);
}

QString period(int id)
{
    switch (id)
    {
        case Constants::Time::Seconds : return tkTr(Constants::SECOND_S);
        case Constants::Time::Minutes : return tkTr(Constants::MINUTE_S);
        case Constants::Time::Hours :   return tkTr(Constants::HOUR_S);
        case Constants::Time::Days :    return tkTr(Constants::DAY_S);
        case Constants::Time::Weeks :   return tkTr(Constants::WEEK_S);
        case Constants::Time::Months :  return tkTr(Constants::MONTH_S);
        case Constants::Time::Quarter : return tkTr(Constants::QUARTER_S);
        case Constants::Time::Year :    return tkTr(Constants::YEAR_S);
        case Constants::Time::Decade :  return tkTr(Constants::DECADE_S);
    }
    return QString();
}

QString periodPlurialForm(int id, int nb, const QString &defaultValue)
{
    switch (id)
    {
        case Constants::Time::Seconds : if (nb > 1) return tkTr(Constants::SECONDS); else return tkTr(Constants::SECOND);
        case Constants::Time::Minutes : if (nb > 1) return tkTr(Constants::MINUTES); else return tkTr(Constants::MINUTE);
        case Constants::Time::Hours :   if (nb > 1) return tkTr(Constants::HOURS); else return tkTr(Constants::HOUR);
        case Constants::Time::Days :    if (nb > 1) return tkTr(Constants::DAYS); else return tkTr(Constants::DAY);
        case Constants::Time::Weeks :   if (nb > 1) return tkTr(Constants::WEEKS); else return tkTr(Constants::WEEK);
        case Constants::Time::Months :  if (nb > 1) return tkTr(Constants::MONTHS); else return tkTr(Constants::MONTH);
        case Constants::Time::Quarter : if (nb > 1) return tkTr(Constants::QUARTERS); else return tkTr(Constants::QUARTER);
        case Constants::Time::Year :    if (nb > 1) return tkTr(Constants::YEARS); else return tkTr(Constants::YEAR);
        case Constants::Time::Decade :  if (nb > 1) return tkTr(Constants::DECADES); else return tkTr(Constants::DECADE);
    }
    return defaultValue;
}





QStringList dailySchemeList()
{
    return QStringList()
            << tkTr(Constants::WAKEUPTIME)
            << tkTr(Constants::BREAKFAST)
            << tkTr(Constants::MORNING)
            << tkTr(Constants::TENOCLOCKBRUNCH)
            << tkTr(Constants::MIDDAY)
            << tkTr(Constants::LUNCH)
            << tkTr(Constants::TEATIME)
            << tkTr(Constants::EVENING)
            << tkTr(Constants::DINER)
            << tkTr(Constants::BEDTIME);
}

QStringList dailySchemeXmlTagList()
{
    static QStringList tags;
    if (tags.count())
        return tags;
    tags
            << QString(Constants::WAKEUPTIME).remove(" ")
            << QString(Constants::BREAKFAST).remove(" ")
            << QString(Constants::MORNING).remove(" ")
            << QString(Constants::TENOCLOCKBRUNCH).remove(" ").remove("'")
            << QString(Constants::MIDDAY).remove(" ")
            << QString(Constants::LUNCH).remove(" ")
            << QString(Constants::TEATIME).remove(" ")
            << QString(Constants::EVENING).remove(" ")
            << QString(Constants::DINER).remove(" ")
            << QString(Constants::BEDTIME).remove(" ")
            ;
    return tags;
}


QString dailyScheme(const int scheme)
{
    return dailySchemeList().at(scheme);
}


QStringList preDeterminedAges()
{
    return QStringList()
            << tkTr(Constants::DAYS)
            << tkTr(Constants::WEEKS)
            << tkTr(Constants::MONTHS)
            << tkTr(Constants::QUARTERS)
            << tkTr(Constants::YEARS)
            ;
}


QStringList mealTime()
{
    return QStringList()
           << ""
           << tkTr(Constants::NOMEALRELATION)
           << tkTr(Constants::DURINGMEAL)
           << tkTr(Constants::BEFOREMEAL)
           << tkTr(Constants::AFTERMEAL)
           << tkTr(Constants::NOTDURINGMEAL)
           << tkTr(Constants::ONLYIFMEAL)
           << tkTr(Constants::SEPARATEDFROMFOOD)
           << tkTr(Constants::SUGGESTEDWITHFOOD)
           << tkTr(Constants::WITHORWITHOUTFOOD)
           ;
}


QString mealTime(const int index)
{
    switch (index) {
    case Constants::Time::Undefined : return QString();
    case Constants::Time::NoRelationWithMeal : return tkTr(Constants::NOMEALRELATION);
    case Constants::Time::DuringMeal: return tkTr(Constants::DURINGMEAL);
    case Constants::Time::BeforeMeal: return tkTr(Constants::BEFOREMEAL);
    case Constants::Time::AfterMeal: return tkTr(Constants::AFTERMEAL);
    case Constants::Time::OutsideMeal: return tkTr(Constants::NOTDURINGMEAL);
    case Constants::Time::OnlyIfMeal: return tkTr(Constants::NOTDURINGMEAL);
    case Constants::Time::SeparatedFromFood: return tkTr(Constants::SEPARATEDFROMFOOD);
    case Constants::Time::SuggestedWithFood: return tkTr(Constants::SUGGESTEDWITHFOOD);
    case Constants::Time::WithOrWithoutFood: return tkTr(Constants::WITHORWITHOUTFOOD);
    }
    return QString();
}

QStringList genders()
{
    return QStringList()
            << tkTr(Trans::Constants::MALE)
            << tkTr(Trans::Constants::FEMALE)
            << tkTr(Trans::Constants::OTHER)
            << tkTr(Trans::Constants::UNKNOWN)
            ;
}

QStringList titles()
{
    return QStringList()
            << ""
            << tkTr(Trans::Constants::MISTER)
            << tkTr(Trans::Constants::MISS)
            << tkTr(Trans::Constants::MADAM)
            << tkTr(Trans::Constants::DOCTOR)
            << tkTr(Trans::Constants::PROFESSOR)
            << tkTr(Trans::Constants::CAPTAIN)
            ;
}

} // End ConstantTranslations
} // End Trans
