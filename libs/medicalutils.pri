#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

# include path for toolkit
include( medicalutils_dependencies.pri )
CONFIG( debug, debug|release ) {
    unix:LIBS    *= -lMedicalUtils_debug
    win32:LIBS   *= -lMedicalUtils_d
} else {
    LIBS  *= -lMedicalUtils
}
