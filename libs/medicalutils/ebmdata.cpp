/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "ebmdata.h"

#include <QString>
#include <QDomDocument>
#include <QStringList>

#include <QDebug>

using namespace MedicalUtils;

namespace {

    const char * const  ROOT_TAG = "PubmedArticle";

    const char * const  MEDLINE_CITATION = "MedlineCitation";

    const char * const      ARTICLE_TAG = "Article";
    const char * const          JOURNAL_TAG = "Journal";
    const char * const              JOURNALISSUE_TAG = "JournalIssue";
    const char * const                  VOLUME_TAG = "Volume";
    const char * const                  ISSUE_TAG = "Issue";
    const char * const                  PUBDATE_TAG = "PubDate";
    const char * const                      PUBDATE_YEAR_TAG = "Year";
    const char * const                      PUBDATE_MONTH_TAG = "Month";
    const char * const                  TITLE_TAG = "Title";
    const char * const                  ISO_ABBREV_TITLE_TAG = "ISOAbbreviation";

    const char * const          ARTICLETITLE_TAG = "ArticleTitle";
    const char * const          PAGIN_TAG = "Pagination";
    const char * const              MEDLINEPAGIN_TAG = "MedlinePgn";
    const char * const          ABSTRACT_TAG = "Abstract";
    const char * const              ABSTRACT_TEXT_TAG = "AbstractText";

    const char * const          AUTHORLIST_TAG = "AuthorList";
    const char * const              AUTHOR_TAG = "Author";
    const char * const              AUTHOR_VALID_ATTRIB = "ValidYN";
    const char * const                  AUTHORLASTNAME_TAG = "LastName";
    const char * const                  AUTHORFORENAME_TAG = "ForeName";

}






EbmData::EbmData()
{
}

EbmData::~EbmData()
{
}

bool EbmData::setPubMedXml(const QString &xml)
{
    m_PubMedXml=xml;
    m_Doc.clear();
    QString error;
    int line, col;
    if (!m_Doc.setContent(xml, &error, &line, &col)) {
        qWarning() << "ERROR" << error << line << col;
        return false;
    }



    QDomElement art = m_Doc.firstChildElement(::ROOT_TAG);
    art = art.firstChildElement(::MEDLINE_CITATION);
    art = art.firstChildElement(::ARTICLE_TAG);
    QDomElement w = art.firstChildElement(::JOURNAL_TAG);
    QString journalFull = w.firstChildElement(::TITLE_TAG).text();
    QString journalIso = w.firstChildElement(::ISO_ABBREV_TITLE_TAG).text();
    w = w.firstChildElement(::JOURNALISSUE_TAG);
    QString vol = w.firstChildElement(::VOLUME_TAG).text();
    QString issue = w.firstChildElement(::ISSUE_TAG).text();
    w = w.firstChildElement(::PUBDATE_TAG);
    QString year = w.firstChildElement(::PUBDATE_YEAR_TAG).text();
    QString month = w.firstChildElement(::PUBDATE_MONTH_TAG).text();
    w = art.firstChildElement(::PAGIN_TAG);
    QString pagin = w.firstChildElement(::MEDLINEPAGIN_TAG ).text();
    m_ShortRef = QString("%1 %2 %3;%4(%5):%6")
            .arg(journalFull)
            .arg(year)
            .arg(month)
            .arg(vol)
            .arg(issue)
            .arg(pagin);

    QDomElement authors = art.firstChildElement(::AUTHORLIST_TAG);
    w = authors.firstChildElement(::AUTHOR_TAG); // if ValidYN == Y go on
    QStringList authorsList;
    while (!w.isNull()) {
        if (w.attribute(::AUTHOR_VALID_ATTRIB)=="Y") {
            QString name = w.firstChildElement(::AUTHORLASTNAME_TAG).text();
            QString foreName = w.firstChildElement(::AUTHORFORENAME_TAG).text();
            authorsList.append(name + " " + foreName);
        }
        w = w.nextSiblingElement(::AUTHOR_TAG);
    }
    m_Authors = authorsList.join(". ");

    m_Title = art.firstChildElement(::ARTICLETITLE_TAG).text();

    m_Ref = QString("%1. %2\n   %3 %4 %5;%6(%7):%8")
            .arg(authorsList.join("; "))
            .arg(m_Title)
            .arg(journalIso)
            .arg(year)
            .arg(month)
            .arg(vol)
            .arg(issue)
            .arg(pagin);

    m_Abstract = art.firstChildElement(::ABSTRACT_TAG).firstChildElement(::ABSTRACT_TEXT_TAG).text();

    return true;
}

QString EbmData::data(const int reference) const
{
    if (reference >= ReferencesCount)
        return QString();
    switch (reference) {
    case Link: return m_Link;
    case PMID: return QString();
    case ShortReferences:
    {
        return m_ShortRef;
    }
    case AbstractPlainText:
    {
        if (!m_Abstract.isEmpty()) {
            return m_Abstract;
        }
        QDomElement art = m_Doc.firstChildElement(::ROOT_TAG);
        art = art.firstChildElement(::MEDLINE_CITATION);
        art = art.firstChildElement(::ARTICLE_TAG);
        art = art.firstChildElement(::ABSTRACT_TAG);
        return art.firstChildElement(ABSTRACT_TEXT_TAG).text();
    }
    case CompleteReferences:
    {
        return m_Ref;
    }
    case Title: return m_Title;
    case Authors: return m_Authors;
    }
    return QString();
}

QDebug operator<<(QDebug dbg, const MedicalUtils::EbmData &c)
{
    dbg.nospace() << QString("EbmData(%1; Ref: %2chars; Abstract: %3chars; Xml: %4chars\n")
                     .arg(c.data(MedicalUtils::EbmData::ShortReferences)).arg(c.references().size()).arg(c.abstract().size()).arg(c.xmlEncoded().size())
                  << ")";
    return dbg.space();
}

QDebug operator<<(QDebug dbg, const MedicalUtils::EbmData *c)
{
    if (!c) {
        dbg.nospace() << "EbmData(0x0)";
        return dbg.space();
    }
    return operator<<(dbg, *c);
}
