/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef MEDICALUTILS_GLOBAL_H
#define MEDICALUTILS_GLOBAL_H

#include <medicalutils/medical_exporter.h>
#include <medicalutils/aggir/girscore.h>

QT_BEGIN_NAMESPACE
class QDateTime;
class QDate;
QT_END_NAMESPACE

/**
 * \file ./libs/medicalutils/global.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace MedicalUtils {

MEDICALUTILS_EXPORT QString readableAge(const QDate &DOB);
MEDICALUTILS_EXPORT int ageYears(const QDate &DOB);

MEDICALUTILS_EXPORT double clearanceCreatinin(const int ageYears, const int weightKg, const double creatMlMin, const bool isMale);

} // MedicalUtils

#endif // MEDICALUTILS_GLOBAL_H
