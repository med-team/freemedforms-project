/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef MEDICALUTILSEXPORTER_H
#define MEDICALUTILSEXPORTER_H

#include <qglobal.h>

// DEFINE EXPORTER
#if defined(MEDICALUTILS_LIBRARY)
#define MEDICALUTILS_EXPORT Q_DECL_EXPORT
#else
#define MEDICALUTILS_EXPORT Q_DECL_IMPORT
#endif

#endif // MEDICALUTILSEXPORTER_H
