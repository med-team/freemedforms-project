/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "global.h"

#include <translationutils/constants.h>
#include <translationutils/trans_datetime.h>

#include <QDateTime>
#include <QDate>
#include <QString>
#include <QStringList>
#include <QVariant>

using namespace MedicalUtils;
using namespace Trans::Constants;
using namespace Trans::ConstantTranslations;

namespace MedicalUtils {

QString readableAge(const QDate &DOB)
{
    QDate current = QDate::currentDate();
    if (current.month() == DOB.month()
            && current.day() == DOB.day()) {
        int years = current.year() - DOB.year();
        return QString("%1 %2").arg(years).arg(tkTr(YEAR_S, years));
    }
    int daysTo = DOB.daysTo(current);
    double age = daysTo / 365.242199;
    QStringList readableAge;
    int years = (int)age;
    if (years>0) {
        QString tmp = QString::number(years) + " ";
        tmp.append(tkTr(YEAR_S, years));
        readableAge << tmp;
        age -= years;
    }
    int months = age * 12;
    if (months > 0) {
        QString tmp = QString::number(months) + " ";
        age -= months / 12.0;
        tmp.append(tkTr(MONTH_S, months));
        readableAge << tmp;
    }
    int days = daysTo - (years*365.25) - (months*12);
    if (age > 0) {
        QString tmp = QString::number((int)(age*365.242199)) + " ";
        tmp.append(tkTr(DAYS, days));
        readableAge << tmp;
    }
    return readableAge.join(" ");
}

int ageYears(const QDate &DOB)
{
    int daysTo = DOB.daysTo(QDate::currentDate());
    double age = daysTo / 365.242199;
    return (int)age;
}

double clearanceCreatinin(const int ageYears, const int weightKg, const double creatMlMin, const bool isMale)
{
    double cockroft = ((140 - ageYears) * weightKg) / (creatMlMin * 7.2);
    if (!isMale)
        cockroft *= 0.85;
    return cockroft;
}

}  // End MedicalUtils
