#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

TARGET = text
TEMPLATE = app
QT += core \
    gui
DEFINES += AGGREGATION_LIBRARY
INCLUDEPATH += ../../
SOURCES += main.cpp \
    ../../aggregate.cpp
HEADERS += main.h \
    myinterfaces.h \
    ../../aggregate.h \
    ../../aggregation_global.h
FORMS += main.ui

