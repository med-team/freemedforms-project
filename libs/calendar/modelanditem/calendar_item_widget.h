/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef LIBCALENDAR_ITEM_WIDGET_H
#define LIBCALENDAR_ITEM_WIDGET_H

#include <QWidget>
#include <QDateTime>

/**
 * \file ./libs/calendar/modelanditem/calendar_item_widget.h
 * \author Guillaume Denry, Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Calendar {
class AbstractCalendarModel;
namespace Internal {

class CalendarItemWidget : public QWidget
{
    Q_OBJECT
public:
    CalendarItemWidget(QWidget *parent = 0, const QString &uid = QString::null, AbstractCalendarModel *model = 0);

    const QString &uid() const { return m_uid; }

    /*! Returns the beginning of the item */
    const QDateTime &beginDateTime() const { return m_beginDateTime; }

    void setBeginDateTime(const QDateTime &dateTime);

    /*! Returns the ending of the item */
    const QDateTime &endDateTime() const { return m_endDateTime; }

    void setEndDateTime(const QDateTime &dateTime);

    /*! Returns the seconds of the item from beginning to it's end */
    int durationInSeconds() const {return m_beginDateTime.secsTo(m_endDateTime);}

    /*! Returns the internally used AbstractCalendarModel */
    AbstractCalendarModel *model() const { return m_model; }

private:
    bool event(QEvent *event);

private:
    // TMP : all date will probably be moved into a pure data class for events/tasks, etc
    QDateTime m_beginDateTime;
    QDateTime m_endDateTime;
    QString m_uid;
    AbstractCalendarModel *m_model;
};

}  // namespace Internal
}  // namespace Calendar

QDebug operator<<(QDebug dbg, const Calendar::Internal::CalendarItemWidget &c);
QDebug operator<<(QDebug dbg, const Calendar::Internal::CalendarItemWidget *c);


#endif  // LIBCALENDAR_ITEM_WIDGET_H
