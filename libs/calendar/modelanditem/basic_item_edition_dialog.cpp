/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "basic_item_edition_dialog.h"
#include "calendar_item.h"
#include <calendar/icalendaritemdatawidget.h>
#include "abstract_calendar_model.h"

#include <extensionsystem/pluginmanager.h>
#include <translationutils/constants.h>
#include <translationutils/trans_current.h>
#include <utils/global.h>

#include "ui_basic_item_edition_dialog.h"

#include <QDebug>

using namespace Calendar;
using namespace Trans::ConstantTranslations;

static inline ExtensionSystem::PluginManager *pluginManager() { return ExtensionSystem::PluginManager::instance(); }

BasicItemEditorDialog::BasicItemEditorDialog(AbstractCalendarModel *model, QWidget *parent) :
    QDialog(parent),
    m_Model(model),
    ui(new Internal::Ui::BasicItemEditionDialog)
{
    Q_ASSERT(model);
    ui->setupUi(this);
    m_moreInfo = ui->buttonBox->addButton(tkTr(Trans::Constants::MORE_INFORMATION), QDialogButtonBox::HelpRole);
    connect(m_moreInfo, SIGNAL(clicked()), this, SLOT(onShowMoreTriggered()));

    ui->viewer->setModel(model);

    QList<ICalendarItemDataWidget*> extended = pluginManager()->getObjects<ICalendarItemDataWidget>();

    for(int i = 0; i < extended.count(); ++i)
        addCalendarDataWidget(extended.at(i));

    adjustSize();
    Utils::centerWidget(this);
}

BasicItemEditorDialog::~BasicItemEditorDialog()
{
    delete ui;
}

void BasicItemEditorDialog::done(int r)
{
    if (r==QDialog::Accepted) {
        ui->viewer->submit();
        m_Model->submit(m_Item);
    } else {
        m_Model->revert(m_Item);
    }
    QDialog::done(r);
}


void BasicItemEditorDialog::addCalendarDataWidget(Calendar::ICalendarItemDataWidget *dataWidget)
{
    ui->viewer->addCalendarDataWidget(dataWidget);
    ui->viewer->adjustSize();
}

void BasicItemEditorDialog::init(const CalendarItem &item)
{
    m_Item = item;
    ui->viewer->setCalendarItem(item);
}

void BasicItemEditorDialog::onShowMoreTriggered()
{
    ui->viewer->toogleExtraInformation();
    if (ui->viewer->isShowingExtraInformation())
        m_moreInfo->setText(tkTr(Trans::Constants::HIDE_EXTRA_INFORMATION));
    else
        m_moreInfo->setText(tkTr(Trans::Constants::MORE_INFORMATION));

    adjustSize();
    Utils::centerWidget(this);
}
