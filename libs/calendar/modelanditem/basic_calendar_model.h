/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef BASIC_CALENDAR_MODEL_H
#define BASIC_CALENDAR_MODEL_H

#include <QPair>

#include <calendar/modelanditem/calendar_item.h>
#include <calendar/modelanditem/abstract_calendar_model.h>

/**
 * \file ./libs/calendar/modelanditem/basic_calendar_model.h
 * \author Guillaume Denry, Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Calendar {
/**
 * This model offers an optimized version of <getItemsBetween()> based on dichotomy method and double lists.
*/
class BasicCalendarModel : public AbstractCalendarModel
{
    Q_OBJECT
public:
    BasicCalendarModel(QObject *parent = 0);
    virtual ~BasicCalendarModel();

    QList<CalendarItem> getItemsBetween(const QDate &from, const QDate &to);

    int count() const { return m_sortedByBeginList.count(); }

    CalendarItem insertItem(const QDateTime &beginning, const QDateTime &ending);
    Calendar::CalendarItem addCalendarItem(const Calendar::CalendarItem &item);
    void removeItem(const QString &uid);
    bool moveItem(const Calendar::CalendarItem &from, Calendar::CalendarItem &to){Q_UNUSED(from); Q_UNUSED(to); return false;}

    CalendarItem getItemByUid(const QString &uid) const;

    void clearAll();

private:
    QList<CalendarItem*> m_sortedByBeginList;
    QList<CalendarItem*> m_sortedByEndList;

    void setItemByUid(const QString &uid, const CalendarItem &item);

    // returns an insertion index for a datetime in <list> from <first> to <last> (dichotomy method)
    int getInsertionIndex(bool begin, const QDateTime &dateTime, const QList<CalendarItem*> &list, int first, int last) const;

    // search for an intersected item, the first found item is enough
    int searchForIntersectedItem(const QList<CalendarItem*> &list, const QDate &from, const QDate &to, int first, int last) const;

    // create a uniq uid
    QString createUid() const;

    CalendarItem *getItemPointerByUid(const QString &uid) const;
};
}

#endif
