/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CALENDAR_ITEMEDITORWIDGET_H
#define CALENDAR_ITEMEDITORWIDGET_H

#include <calendar/calendar_exporter.h>

#include <QWidget>
#include <QDateTime>

/**
 * \file ./libs/calendar/modelanditem/item_editor_widget.h
 * \author Guillaume Denry, Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Calendar {
class CalendarItem;
class UserCalendar;
class AbstractCalendarModel;
class ICalendarItemDataWidget;

namespace Internal {
class ItemEditorWidgetPrivate;
}

class CALENDAR_EXPORT ItemEditorWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ItemEditorWidget(QWidget *parent = 0);
    ~ItemEditorWidget();

    void clear();
    void setModel(AbstractCalendarModel *model);

    void setCalendarItem(const Calendar::CalendarItem &item);
    Calendar::CalendarItem calendarEvent() const;

    void toogleExtraInformation();
    bool isShowingExtraInformation() const;

//    void setAvailableUserCalendar(const QList<Calendar::UserCalendar *> &userCals);

    void addCalendarDataWidget(Calendar::ICalendarItemDataWidget *dataWidget);

public Q_SLOTS:
    void submit();

private Q_SLOTS:
    void on_selectIconButton_clicked();
    void on_durationCombo_currentIndexChanged(int index);
    void onDateTimeChanged(const QDateTime &);
    void changeDuration(const int comboIndex);

protected:
    void changeEvent(QEvent *e);

private:
    Internal::ItemEditorWidgetPrivate *d;
};

}  // End namespace Calendar

#endif // CALENDAR_ITEMEDITORWIDGET_H
