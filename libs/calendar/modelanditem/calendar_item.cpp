/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "calendar_item.h"
#include "abstract_calendar_model.h"
#include <calendar/common.h>

#include <QDebug>

using namespace Calendar;

CalendarItem::CalendarItem() :
        m_Model(0)
{}

CalendarItem::CalendarItem(const QDateTime &beginning, const QDateTime &ending) :
        m_Model(0)
{
    m_beginning = beginning;
    m_ending = ending;
    m_created = QDateTime::currentDateTime();
    m_beginningType = Date_DateTime;
    m_endingType = Date_DateTime;
}

CalendarItem::CalendarItem(const QString &uid, const QDateTime &beginning, const QDateTime &ending) :
        m_Model(0)
{
    m_uid = uid;
    m_beginning = beginning;
    m_ending = ending;
    m_created = QDateTime::currentDateTime();
    m_beginningType = Date_DateTime;
    m_endingType = Date_DateTime;
}

void CalendarItem::setDaily(bool value) {
	DateType dateType = value ? Date_Date : Date_DateTime;

	if (m_beginningType == dateType && m_endingType == dateType)
		return;

	m_beginningType = dateType;
	m_endingType = dateType;
}

bool CalendarItem::isValid() const
{
    return m_beginning.isValid() && (m_Model);
}

bool CalendarItem::isNull() const
{
    return false;
}

QVariant CalendarItem::data(const int ref) const
{
    if (m_Model) {
        int refModel = -1;
        switch (ref) {
        case Uid : return m_uid;
        case Label: refModel = AbstractCalendarModel::Label; break;
        case Description: refModel = AbstractCalendarModel::Description; break;
        case Type: refModel = AbstractCalendarModel::Type; break;
        case Status: refModel = AbstractCalendarModel::Status; break;
        case IsPrivate: refModel = AbstractCalendarModel::IsPrivate; break;
        case Password: refModel = AbstractCalendarModel::Password; break;
        case IsBusy: refModel = AbstractCalendarModel::IsBusy; break;
        case IsAGroupEvent: refModel = AbstractCalendarModel::IsAGroupEvent; break;
        case DateStart: return m_beginning;
        case DateEnd: return m_ending;
        case Location: refModel = AbstractCalendarModel::Location; break;
        case LocationUid: refModel = AbstractCalendarModel::LocationUid; break;
        case IconPath: refModel = AbstractCalendarModel::IconPath; break;
        case CreatedDate: return m_created;
        }
        if (refModel!=-1)
            return m_Model->data(*this, refModel);
    }
    return QVariant();
}

bool CalendarItem::setData(const int ref, const QVariant &value)
{
    if (m_Model) {
        switch (ref) {
        case DateStart: setBeginning(value.toDateTime()); break;
        case DateEnd:  setEnding(value.toDateTime()); break;
        case CreatedDate: m_created = value.toDateTime(); break;
        }
        return m_Model->setData(*this, ref, value);
    } else {
        qWarning() << "CalendarItem does not have model";
    }
    return false;
}

void CalendarItem::setBeginningType(DateType value)
{
    if (m_beginningType==value)
        return;
    m_beginningType = value;
}

void CalendarItem::setEndingType(DateType value)
{
    if (m_endingType==value)
        return;
    m_endingType = value;
}

/** compute an intersection value with a day range
             * returns:
             * -1 if item is entirely before first day
             * 0 if item intersects [firstDay, lastDay]
             * 1 if item is entirely after lastDay
*/
int CalendarItem::intersects(const QDate &firstDay, const QDate &lastDay) const
{
    return intersectsDays(beginning(), ending(), firstDay, lastDay);
}

/** compute an overlap value with another item
             * returns:
             * false if items do not overlap
             * true if items overlap
*/
bool CalendarItem::overlap(const CalendarItem &item) const
{
    return ending() > item.beginning() && beginning() < item.ending();
}

void CalendarItem::setBeginning(const QDateTime &value)
{
    if (!value.isValid())
        return;
    if (m_beginning==value)
        return;
    m_beginning=value;
    if (m_Model) {
        m_Model->setData(*this, AbstractCalendarModel::DateStart, value);
    }
}

void CalendarItem::setEnding(const QDateTime &value)
{
    if (!value.isValid())
        return;
    if (m_ending==value)
        return;
    m_ending=value;
    if (m_Model) {
        m_Model->setData(*this, AbstractCalendarModel::DateEnd, value);
    }
}

bool CalendarItem::operator==(const CalendarItem &other) const
{
    return (other.uid()==uid() &&
            other.beginning()==m_beginning &&
            other.ending() == m_ending);
}

bool Calendar::calendarItemLessThan(const CalendarItem &item1, const CalendarItem &item2)
{
    if (item1.beginning() < item2.beginning())
        return true;
    else if (item1.beginning() > item2.beginning())
        return false;
    else if (item1.ending() > item2.ending())
        return true;
    else if (item1.ending() < item2.ending())
        return false;
    else if (item1.created() < item2.created())
        return true;
    else if (item1.created() > item2.created())
        return false;
    else
        return item1.uid() > item2.uid();
}

QDebug operator<<(QDebug dbg, const Calendar::CalendarItem &c)
{
    dbg.nospace() << "CalendarItem("
                  << c.beginning().toString(QLocale().dateTimeFormat(QLocale::ShortFormat))
                  << ", "
                  << c.ending().toString(QLocale().dateTimeFormat(QLocale::ShortFormat))
                  << ", " << c.model() << ")";
    return dbg.space();
}

QDebug operator<<(QDebug dbg, const Calendar::CalendarItem *c)
{
    if (!c) {
        dbg.nospace() << "CalendarItem(0x0)";
        return dbg.space();
    }
    return operator<<(dbg, *c);
}

