/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/









































#include "abstract_calendar_model.h"

using namespace Calendar;

AbstractCalendarModel::AbstractCalendarModel(QObject *parent) :
    QObject(parent), m_propagateEvents(true) {
}


QVariant AbstractCalendarModel::data(const Calendar::CalendarItem &item, const int dataRef, int role) const
{
    Q_UNUSED(item);
    Q_UNUSED(dataRef);
    Q_UNUSED(role);
    return QVariant();
}

bool AbstractCalendarModel::setData(const Calendar::CalendarItem &item, const int dataRef, const QVariant &value, int role)
{
    Q_UNUSED(item);
    Q_UNUSED(dataRef);
    Q_UNUSED(value);
    Q_UNUSED(role);
    return true;
}

bool AbstractCalendarModel::setPeopleList(const Calendar::CalendarItem &item, const QList<Calendar::People> &peoples)
{
    bool ok = true;
    for(int i=0; i < peoples.count(); ++i) {
        if (!addPeople(item, peoples.at(i)))
            ok=false;
    }
    return ok;
}

bool AbstractCalendarModel::addPeople(const Calendar::CalendarItem &item, const People &people)
{
    Q_UNUSED(item);
    Q_UNUSED(people);
    return false;
}

bool AbstractCalendarModel::removePeople(const Calendar::CalendarItem &item, const int peopleType, const QString &uid)
{
    Q_UNUSED(item);
    Q_UNUSED(peopleType);
    Q_UNUSED(uid);
    return false;
}

QList<Calendar::People> AbstractCalendarModel::peopleList(const Calendar::CalendarItem &item)
{
    Q_UNUSED(item);
    return QList<Calendar::People>();
}


void AbstractCalendarModel::beginInsertItem() {
}

void AbstractCalendarModel::endInsertItem(const CalendarItem &newItem) {
    if (m_propagateEvents)
        Q_EMIT itemInserted(newItem);
}

void AbstractCalendarModel::beginModifyItem() {
}

void AbstractCalendarModel::endModifyItem(const CalendarItem &oldItem, const CalendarItem &newItem) {
    if (m_propagateEvents)
        Q_EMIT itemModified(oldItem, newItem);
}

void AbstractCalendarModel::beginRemoveItem() {
}

void AbstractCalendarModel::endRemoveItem(const CalendarItem &removedItem) {
    if (m_propagateEvents)
        Q_EMIT itemRemoved(removedItem);
}

void AbstractCalendarModel::setItemIsMine(Calendar::CalendarItem *item) const {
    item->setModel(const_cast<AbstractCalendarModel*>(this));
}



void AbstractCalendarModel::stopEvents() {
    m_propagateEvents = false;
}

void AbstractCalendarModel::resumeEvents() {
    m_propagateEvents = true;
    Q_EMIT reset();
}

