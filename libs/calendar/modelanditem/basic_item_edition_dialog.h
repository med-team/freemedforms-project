/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef BASIC_ITEM_EDITION_DIALOG_H
#define BASIC_ITEM_EDITION_DIALOG_H

#include <calendar/calendar_exporter.h>
#include <calendar/modelanditem/calendar_item.h>

#include <QDialog>
#include <QPushButton>

/**
 * \file ./libs/calendar/modelanditem/basic_item_edition_dialog.h
 * \author Guillaume Denry, Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Calendar {
class AbstractCalendarModel;
class ICalendarItemDataWidget;
class CalendarItem;
namespace Internal {
namespace Ui {
class BasicItemEditionDialog;
} // End namespace Ui
}  // End namespace Internal


class CALENDAR_EXPORT BasicItemEditorDialog : public QDialog
{
    Q_OBJECT
public:
    BasicItemEditorDialog(AbstractCalendarModel *model, QWidget *parent = 0);
    ~BasicItemEditorDialog();

    void addCalendarDataWidget(Calendar::ICalendarItemDataWidget *dataWidget);

    void init(const CalendarItem &item);

protected:
    void done(int r);

private Q_SLOTS:
    void onShowMoreTriggered();

private:
    AbstractCalendarModel *m_Model;
    Internal::Ui::BasicItemEditionDialog *ui;
    QPushButton *m_moreInfo;
    CalendarItem m_Item;
};

}  // End namespace Calendar

#endif
