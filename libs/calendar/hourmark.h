/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CALENDAR_INTERNAL_HOURMARK_H
#define CALENDAR_INTERNAL_HOURMARK_H

#include <QWidget>
#include <QTime>

/**
 * \file ./libs/calendar/hourmark.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Calendar {
namespace Internal {

class HourMark : public QWidget
{
    Q_OBJECT
public:
    explicit HourMark(QWidget *parent = 0);

    void setDayOfWeek(int dayOfWeek);
    void setTime(const QTime &time);

protected:
    void paintEvent(QPaintEvent *);

private:
    int _day;
    QTime _time;
};

} // namespace Internal
} // namespace Calendar

#endif // CALENDAR_INTERNAL_HOURMARK_H
