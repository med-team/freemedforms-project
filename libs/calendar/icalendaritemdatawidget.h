/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ICALENDARITEMDATAWIDGET_H
#define ICALENDARITEMDATAWIDGET_H

#include <calendar/calendar_exporter.h>

#include <QObject>

/**
 * \file ./libs/calendar/icalendaritemdatawidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/


namespace Calendar {
class CalendarItem;
class AbstractCalendarModel;

class CALENDAR_EXPORT ICalendarItemDataWidget : public QObject
{
    Q_OBJECT
public:
    enum InsertionPlaces {
        Beginning = 0,              //!< top of dialog
        Ending,                     //!< bottom of dialog
        BeforeGeneralInformation,   //!< in the "extra section", before "general Information"
        AfterGeneralInformation,    //!< in the "extra section", after "general Information"
        BeforeDateTime,             //!< before date/time section
        AfterDateTime,              //!< after the date/time section
        BeforeDescription,          //!< before description
        AfterDescription            //!< after description
    };

    ICalendarItemDataWidget(QObject *parent = 0) : QObject(parent) { }
    virtual ~ICalendarItemDataWidget() {}

    virtual int insertionPlace() const = 0;
    virtual QWidget *createWidget(QWidget *parent = 0) = 0;

    virtual bool setCalendarItemModel(Calendar::AbstractCalendarModel *model) = 0;
    virtual bool setCalendarItem(const Calendar::CalendarItem &item) = 0;

    virtual bool clear() = 0;
    virtual bool submitChangesToCalendarItem(const Calendar::CalendarItem &item) = 0;
};

}  // End namespace Calendar


#endif // ICALENDARITEMDATAWIDGET_H
