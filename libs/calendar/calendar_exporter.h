/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CALENDARUTILSEXPORTER_H
#define CALENDARUTILSEXPORTER_H

/**
  \namespace Calendar
  \brief Namespace for the Calendar library
  Namespace for the Calendar library
*/

#include <qglobal.h>

// DEFINE EXPORTER
#if defined(CALENDAR_LIBRARY)
#define CALENDAR_EXPORT Q_DECL_EXPORT
#else
#define CALENDAR_EXPORT Q_DECL_IMPORT
#endif

#endif // CALENDARUTILSEXPORTER_H
