/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CALENDAR_WIDGET_H
#define CALENDAR_WIDGET_H

#include <calendar/calendar_exporter.h>
#include <calendar/common.h>

#include <QScrollArea>
#include <QTimer>

QT_BEGIN_NAMESPACE
class QVBoxLayout;
class QMenu;
QT_END_NAMESPACE

/**
 * \file ./libs/calendar/calendar_widget.h
 * \author Guillaume Denry, Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Calendar {
class AbstractCalendarModel;
class CalendarItem;
struct CalendarWidgetPrivate;

class CALENDAR_EXPORT CalendarWidget : public QWidget
{
    Q_OBJECT
    /** The number of minutes used when we want to move of size items. Worth 15 by default. */
    Q_PROPERTY(int dayGranularity READ dayGranularity WRITE setDayGranularity)
    /** default item duration. 30 minutes by default. */
    Q_PROPERTY(int dayItemDefaultDuration READ dayItemDefaultDuration WRITE setDayItemDefaultDuration)
    /** Divider for the vertical day scale in views.
     * For instance, to have a line every half-hour, use 2. For a line every fifteen minutes, use 4.
     * Worth 2 by default.
     */
    Q_PROPERTY(int dayScaleHourDivider READ dayScaleHourDivider WRITE setDayScaleHourDivider)
    /** Height (in pixels) of an hour. 40 by default. */
    Q_PROPERTY(int hourHeight READ hourHeight WRITE setHourHeight)
public:
    CalendarWidget(QWidget *parent = 0);

    AbstractCalendarModel *model() const { return m_model; }
    void setModel(AbstractCalendarModel *model);

    ViewType viewType() const;
    void setViewType(Calendar::ViewType viewType);

    void setDate(const QDate &date);

    int dayGranularity() const;
    int dayItemDefaultDuration() const;
    int dayScaleHourDivider() const;
    int hourHeight() const;

    Calendar::CalendarItem getContextualCalendarItem() const;

public Q_SLOTS:
    void setDayGranularity(int minutes);
    void setDayItemDefaultDuration(int minutes);
    void setDayScaleHourDivider(int divider);
    void setHourHeight(int pixels);
    void setContextMenuForItems(QMenu *menu);

    void scrollToTime(const QTime &time);

private Q_SLOTS:
    // navigation bar slots
    void firstDateChanged();
    void viewTypeChanged();

    // timer
    void timeout();

private:
    CalendarWidgetPrivate *m_d;
    ViewType m_viewType;
    AbstractCalendarModel *m_model;
};

}  // End namespace Calendar

#endif
