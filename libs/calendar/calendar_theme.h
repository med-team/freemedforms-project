/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef CALENDAR_THEME_H
#define CALENDAR_THEME_H

#include <calendar/calendar_exporter.h>

#include <QHash>
#include <QString>
#include <QColor>

/**
 * \file ./libs/calendar/calendar_theme.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Calendar {

class CALENDAR_EXPORT CalendarTheme
{
public:
    enum PathReference {
        SmallIconPath = 0,  // 16x16
        MediumIconPath,     // 32x32
        BigIconPath          // 64x64
    };

    enum IconReference {
        NavigationNext = 0,
        NavigationPrevious,
        NavigationCurrentDateView,
        NavigationBookmarks,
        NavigationViewMode,
        NavigationDayViewMode,
        NavigationWeekViewMode,
        NavigationMonthViewMode,
        NavigationForceModelRefreshing
    };

    enum IconSize {
        SmallSize = 0,
        MediumSize,
        BigSize
    };

    enum ColorInUse {
        ColorStatusWaiting = 0,
        ColorStatusApproved,
        ColorStatusArrived,
        ColorStatusChanged,
        ColorStatusCancelled,
        ColorStatusMissed
    };

    static CalendarTheme *instance();
    ~CalendarTheme() {}

    void setPath(const PathReference ref, const QString &absPath);
    void setIconFileName(const IconReference ref, const QString &fileName);

    QString iconFileName(const IconReference ref, const IconSize size = SmallSize) const;

    void setColor(const ColorInUse colorRef, const QColor &color);
    QColor color(const ColorInUse colorRef) const;

private:
    CalendarTheme() {}
    void populateWithDefault();

    QHash<PathReference, QString> m_path;
    QHash<IconReference, QString> m_icons;
    QHash<ColorInUse, QColor> m_colors;
    static CalendarTheme *m_Instance;
};

}  // End namespace Calendar

#endif // CALENDAR_THEME_H
