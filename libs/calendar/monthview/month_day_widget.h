/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef MONTH_DAY_WIDGET_H
#define MONTH_DAY_WIDGET_H

#include <QWidget>
#include <QDate>
#include <QMap>

#include <calendar/modelanditem/abstract_calendar_model.h>
#include <calendar/modelanditem/calendar_item.h>

namespace Calendar {
class MonthDayWidget : public QWidget {
    Q_OBJECT
public:
    MonthDayWidget(AbstractCalendarModel *model, const QDate &day, QWidget *parent = 0);

    const QDate &day() const { return m_day; }

protected:
    virtual void resizeEvent(QResizeEvent *event);
    bool eventFilter(QObject *obj, QEvent *event);

private:
    AbstractCalendarModel *m_model;
    QDate m_day;
    QList<CalendarItem> m_items;
    QMap<QWidget *, QString> m_uidByWidget;

    CalendarItem *getItemByUid(const QString &uid);

private Q_SLOTS:
    void refreshItems();
};
}

#endif
