/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef LIBCALENDAR_MONTH_VIEW_H
#define LIBCALENDAR_MONTH_VIEW_H

#include <calendar/view.h>

/**
 * \file ./libs/calendar/monthview/month_view.h
 * \author Guillaume Denry, Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Calendar {
namespace Internal {

class MonthHeader : public ViewWidget
{
    Q_OBJECT
public:
    MonthHeader(QWidget *parent = 0);

    QSize sizeHint() const;

protected:
    void paintEvent(QPaintEvent *event);
};

class MonthBody : public ViewWidget
{
    Q_OBJECT
public:
    MonthBody(QWidget *parent = 0);

    virtual int topHeaderHeight() const;
    virtual int leftHeaderWidth() const;

protected:
    virtual void paintBody(QPainter *painter, const QRect &visibleRect);
    virtual void resetItemWidgets();
    virtual void refreshItemsSizesAndPositions();
    QRect getDayRect(const QDate &day) const;

private:
    int m_weekCount;
    QPair<QDate,QDate> m_monthBoundingDays;
    //		void paintEvents(QPainter &painter, const QDate &day, const QRect &dayRect);

private Q_SLOTS:
    void firstDateChanged();
};

}  // namespace Internal
}  // namespace Calendar

#endif  // LIBCALENDAR_MONTH_VIEW_H
