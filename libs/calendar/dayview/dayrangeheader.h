/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef LIBCALENDAR_DAYRANGEHEADER_H
#define LIBCALENDAR_DAYRANGEHEADER_H

#include <calendar/view.h>

/**
 * \file ./libs/calendar/dayview/dayrangeheader.h
 * \author Guillaume Denry, Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Calendar {
namespace Internal {
class DayRangeHeaderPrivate;

class HourRangeWidget;
class DayWidget;

class DayRangeHeader : public ViewWidget
{
    Q_OBJECT
public:
    DayRangeHeader(QWidget *parent = 0, int rangeWidth = 7);

    QSize sizeHint() const;

    int rangeWidth() const;
    void setRangeWidth(int width);

protected:
    void paintEvent(QPaintEvent *event);
    void resetItemWidgets();
    void refreshItemsSizesAndPositions();
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

private Q_SLOTS:
    void modifyPressItem();
    void removePressItem();

private:
    Internal::DayRangeHeaderPrivate *d_header;
};

}  // namespace Internal
}  // namespace Calendar

#endif  // LIBCALENDAR_DAYRANGEHEADER_H
