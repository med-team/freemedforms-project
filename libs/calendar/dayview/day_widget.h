/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DAY_WIDGET_H
#define DAY_WIDGET_H

#include <calendar/modelanditem/calendar_item_widget.h>

/**
 * \file ./libs/calendar/dayview/day_widget.h
 * \author Guillaume Denry, Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Calendar {
class AbstractCalendarModel;
namespace Internal {

/**
 * This class represents a day event without hour range specification
*/
class DayWidget : public CalendarItemWidget
{
    Q_OBJECT
public:
    /** if uid is empty, this calendar item is considered as temporary and will be drawn with transparence
         */
    DayWidget(QWidget *parent = 0, const QString &uid = "", AbstractCalendarModel *model = 0);

    bool inMotion() const { return m_inMotion; }
    void setInMotion(bool value);

    QSize sizeHint() const;
    static QSize staticSizeHint();

private:
    bool m_inMotion;
    QWidget *m_aboveWidget;
    QFont m_titleFont; // TODO: choose a better font than the default one

    static QFont getTitleFont();

protected:
    virtual void paintEvent(QPaintEvent *event);
};

}  // namespace Internal
}  // namespace Calendar


#endif
