/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef LIBCALENDAR_DAYRANGEHEADER_H
#define LIBCALENDAR_DAYRANGEHEADER_H

#include <calendar/view.h>

/**
 * \file ./libs/calendar/dayview/hourwidget.h
 * \author Guillaume Denry, Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Calendar {
namespace Internal {

class HourWidget : public QWidget
{
    Q_OBJECT
public:
    HourWidget(QWidget *parent = 0);

    virtual QSize sizeHint() const { return QSize(0, 2); }

protected:
    void paintEvent(QPaintEvent *event);
};

}  // namespace Internal
}  // namespace Calendar

#endif  // LIBCALENDAR_DAYRANGEHEADER_H
