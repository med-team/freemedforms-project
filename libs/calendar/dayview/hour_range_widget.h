/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef HOUR_RANGE_WIDGET_H
#define HOUR_RANGE_WIDGET_H

#include <calendar/modelanditem/calendar_item_widget.h>

/**
 * \file ./libs/calendar/dayview/hour_range_widget.h
 * \author Guillaume Denry, Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Calendar {
class AbstractCalendarModel;
namespace Internal {

class HourRangeWidget : public CalendarItemWidget
{
    Q_OBJECT
public:
    HourRangeWidget(QWidget *parent = 0, const QString &uid = "", AbstractCalendarModel *model = 0);

    bool inMotion() const { return m_inMotion; }
    void setInMotion(bool value);

private:
    bool m_inMotion;
    QWidget *m_aboveWidget;

protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    //		virtual void mouseReleaseEvent(QMouseEvent *event);
};

}  // namespace Internal
}  // namespace Calendar

#endif  // HOUR_RANGE_WIDGET_H
