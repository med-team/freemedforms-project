/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef LIBCALENDAR_DAY_NODE_H
#define LIBCALENDAR_DAY_NODE_H

#include <QList>

namespace Calendar {
class CalendarItem;
namespace Internal {

class DayStore
{
public:
    /** store an item at the right place with a recursive method depending on date ranges
         * \return {int} the depth of the node (0 -> N)
         */
    int store(const CalendarItem &item);

private:
    QList< QList<CalendarItem> > m_items; // contains all layers of items m_items[0] is the first layer, m_items[1] is the second, etc
};

}  // namespace Internal
}  // namespace Calendar

#endif  // LIBCALENDAR_DAY_NODE_H
