/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "icalendaritemdatawidget.h"

namespace Calendar {
/*!
 * \class Calendar::ICalendarItemDataWidget
 * \brief Generic interface to describe a calendar data widget.
 *
 * Widgets implementing this interface can be plugged into the calendar item editor dialog
 * and provide additional data to any calendar item.
 */


/*!
 * \fn virtual int Calendar::ICalendarItemDataWidget::insertionPlace() const = 0;
 * \returns a InsertionPlaces which describes where the
 * widget was inserted into the ui of the calendar item editor dialog.
 * \sa InsertionPlaces
*/

/*!
 * \fn virtual QWidget *Calendar::ICalendarItemDataWidget::createWidget(QWidget *parent = 0) = 0;
 * \brief Factory method to create the widget.
 */

/*!
 * \fn virtual bool setCalendarItem(const Calendar::CalendarItem &item) = 0;
 */

/*!
 * \fn virtual bool submitChangesToCalendarItem(Calendar::CalendarItem &item) = 0;
 */

}
