/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "hourmark.h"

#include <QPixmap>
#include <QPainter>

using namespace Calendar;
using namespace Internal;

HourMark::HourMark(QWidget *parent) :
    QWidget(parent)
{
}

void HourMark::setDayOfWeek(int dayOfWeek)
{
    _day = dayOfWeek;
}

void HourMark::setTime(const QTime &time)
{
    _time=time;
}

void HourMark::paintEvent(QPaintEvent *)
{
    QPixmap linepixmap(size().width(), 22);
    linepixmap.fill(Qt::transparent);

    QPainter linePainter(&linepixmap);
    linePainter.setRenderHint(QPainter::Antialiasing);
    QPen linePen;
    linePen.setColor(Qt::lightGray);
    linePainter.setPen(linePen);
    linePainter.drawLine(0, 1, width(), 1);
    linePen.setColor(Qt::black);
    linePainter.setPen(linePen);
    linePainter.drawLine(0, 0, width(), 0);

    QRect r = rect();
    r.setHeight(15);
    r.adjust(2, 0, 0, 0);
    QPen fontPen;
    QFont font = linePainter.font();
    font.setPixelSize(10);
    linePainter.setFont(font);
    fontPen.setColor(Qt::lightGray);
    linePainter.setPen(fontPen);
    r.adjust(2, 0, 0, 0);
    linePainter.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, _time.toString("hh:mm"));
    fontPen.setColor(Qt::black);
    linePainter.setPen(fontPen);
    r.adjust(1, 0, 0, 0);
    linePainter.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, _time.toString("hh:mm"));

    QPainter painter(this);
    painter.drawPixmap(QPoint(0, 0), linepixmap, QRect(0, 0, width(), 22));

}
