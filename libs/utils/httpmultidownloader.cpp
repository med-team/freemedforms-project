/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers: Eric MAEKER, <eric.maeker@gmail.com>                 *
 *   Contributors:                                                         *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
/*!
 * \class Utils::HttpMultiDownloader
 * Multi Downloader allow you to download a set of URLs. You can define :
 * - the list of URL with setUrls(), addUrl()
 * - the output path, where downloaded files will be stored with setOutputPath()
 * - the file renaming to avoid file name collition. Files will be named using a
 * uuid. Use setUseUidAsFileNames()
 *
 * Linking file name with downloaded URL:
 * An XML file stored in the same path will store the book of the links between
 * files and URL. When a dowload is done, the file is stored in the output path
 * and the link URL<->fileName is stored in this XML file. You can read this file,
 * eg if you want to resume a past incomplete download of a queue, with
 * readXmlUrlFileLinks(), you can clear it with clearXmlUrlFileLinks() and force
 * the saving of its content (after downloads are done) with saveXmlUrlFileLinks().
 * This file is automatically updated after each 10 downloads.
 *
 * \note Unit-test available (see: tests/auto/auto.pro)
 */

#include "httpmultidownloader.h"
#include "httpdownloader.h"

#include <utils/log.h>
#include <utils/global.h>
#include <translationutils/constants.h>
#include <translationutils/trans_msgerror.h>

#include <QUrl>
#include <QDir>
#include <QDomDocument>
#include <QDebug>


using namespace Utils;
using namespace Internal;
using namespace Trans::ConstantTranslations;

namespace {
const char *const TAG_ROOT = "MultiDownloader";
const char *const TAG_URLROOT = "Url";
const char *const ATTRIB_URL = "u";
const char *const ATTRIB_FILENAME = "f";
const char *const ATTRIB_ERRORMSG = "m";
const char *const ATTRIB_ERROR = "e";
const char *const DEFAULT_XML_FILENAME = "multidownloader.xml";
}

namespace Utils {
namespace Internal {
struct DownloadedUrl {
    QUrl url;
    QNetworkReply::NetworkError networkError;
    QString errorMessage;
    QString outputFile;
};

class HttpMultiDownloaderPrivate
{
public:
    HttpMultiDownloaderPrivate(HttpMultiDownloader *parent) :
        _downloader(0),
        _downloadingIndex(-1),
        _useUidAsFileName(false),
        _onError(HttpMultiDownloader::OnErrorPursue),
        q(parent)
    {
        Q_UNUSED(q);
    }
    
    ~HttpMultiDownloaderPrivate()
    {
    }

    const DownloadedUrl &downloadedUrl(const QUrl &url)
    {
        foreach(const DownloadedUrl &dld, _downloadedUrl) {
            if (dld.url == url)
                return dld;
        }
        return _emptyDownloaded;
    }

    QString uuidFileName(const QUrl &url)
    {
        return QString("%1.%2")
                .arg(Utils::createUid())
                .arg(QFileInfo(url.toString()).completeSuffix());
    }
    
public:
    QList<QUrl> _urls;
    QString _outputPath;
    Utils::HttpDownloader *_downloader;
    QList<DownloadedUrl> _downloadedUrl;
    DownloadedUrl _emptyDownloaded;
    int _downloadingIndex;
    bool _useUidAsFileName;
    HttpMultiDownloader::OnError _onError;
    
private:
    HttpMultiDownloader *q;
};
} // namespace Internal
} // end namespace UserPlugin


/*! Constructor of the UserPlugin::HttpMultiDownloader class */
HttpMultiDownloader::HttpMultiDownloader(QObject *parent) :
    QObject(parent),
    d(new HttpMultiDownloaderPrivate(this))
{
    d->_downloader = new HttpDownloader(this);
    connect(d->_downloader, SIGNAL(downloadFinished()), this, SLOT(onCurrentDownloadFinished()));
}

/*! Destructor of the UserPlugin::HttpMultiDownloader class */
HttpMultiDownloader::~HttpMultiDownloader()
{
    if (d)
        delete d;
    d = 0;
}

void HttpMultiDownloader::setOutputPath(const QString &absolutePath)
{
    d->_outputPath = absolutePath;
    d->_downloader->setOutputPath(absolutePath);
}

QString HttpMultiDownloader::outputPath() const
{
    return d->_outputPath;
}


void HttpMultiDownloader::setUseUidAsFileNames(bool useUidInsteadOfUrlFileName)
{
    d->_useUidAsFileName = useUidInsteadOfUrlFileName;
}

bool HttpMultiDownloader::useUidAsFileNames() const
{
    return d->_useUidAsFileName;
}

void HttpMultiDownloader::setDownloadErrorManagement(OnError onError)
{
    d->_onError = onError;
}

HttpMultiDownloader::OnError HttpMultiDownloader::downloadErrorManagement() const
{
    return d->_onError;
}


bool HttpMultiDownloader::clearXmlUrlFileLinks()
{
    QFile xmlFile(QString("%1/%2").arg(outputPath()).arg(::DEFAULT_XML_FILENAME));
    if (xmlFile.exists())
        xmlFile.remove();
    return true;
}


bool HttpMultiDownloader::saveXmlUrlFileLinks()
{
    if (!d->_useUidAsFileName)
        return false;
    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    Q_EMIT progressMessageChanged(tr("Saving cache"));
    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    QDomDocument doc("FreeMedForms");
    QDomElement element = doc.createElement(::TAG_ROOT);
    doc.appendChild(element);
    QDir output(outputPath());
    int count = d->_downloadedUrl.count();
    int i = 0;
    foreach(const DownloadedUrl &url, d->_downloadedUrl) {
        QDomElement urlElement = doc.createElement(::TAG_URLROOT);
        urlElement.setAttribute(::ATTRIB_URL, url.url.toString());
        urlElement.setAttribute(::ATTRIB_FILENAME, output.relativeFilePath(url.outputFile));
        urlElement.setAttribute(::ATTRIB_ERRORMSG, url.errorMessage);
        urlElement.setAttribute(::ATTRIB_ERROR, url.networkError);
        ++i;
        if (i % 10 == 0) {
            int permille = i/count*1000;
            qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
            Q_EMIT downloadProgressPermille(permille);
            qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
        }
        element.appendChild(urlElement);
    }
    bool ok = Utils::saveStringToFile(QString("<?xml version='1.0' encoding='UTF-8'?>\n" + doc.toString(2)),
                                   QString("%1/%2").arg(outputPath()).arg(::DEFAULT_XML_FILENAME),
                                   Utils::Overwrite, Utils::DontWarnUser);
    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    Q_EMIT progressMessageChanged(tr("Cache saved"));
    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    return ok;
}


bool HttpMultiDownloader::readXmlUrlFileLinks()
{
    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    Q_EMIT progressMessageChanged(tr("Processing cache"));
    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    QDomDocument doc;
    QString error;
    int line, col;
    QString xml = Utils::readTextFile(QString("%1/%2").arg(outputPath()).arg(::DEFAULT_XML_FILENAME), Utils::DontWarnUser);
    if (!doc.setContent(xml, &error, &line, &col)) {
        LOG_ERROR_FOR("HttpMultiDownloader", tkTr(Trans::Constants::ERROR_1_LINE_2_COLUMN_3).arg(error).arg(line).arg(col));
        return false;
    }
    QDomElement element = doc.firstChildElement(::TAG_ROOT);
    QDir output(outputPath());
    int count = element.childNodes().count();
    int i = 0;
    element = element.firstChildElement(::TAG_URLROOT);
    while (!element.isNull()) {
        DownloadedUrl url;
        url.url = QUrl(element.attribute(::ATTRIB_URL));
        url.outputFile = output.absoluteFilePath(element.attribute(::ATTRIB_FILENAME));
        url.errorMessage = element.attribute(::ATTRIB_ERRORMSG);
        url.networkError = QNetworkReply::NetworkError(element.attribute(::ATTRIB_ERROR).toInt());
        d->_urls << url.url;
        d->_downloadedUrl << url;
        ++i;
        if (i % 10 == 0) {
            int permille = i/count*1000;
            qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
            Q_EMIT downloadProgressPermille(permille);
            qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
        }
        element = element.nextSiblingElement(::TAG_URLROOT);
    }
    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    Q_EMIT progressMessageChanged(tr("Cache processed"));
    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    return true;
}


void HttpMultiDownloader::setUrls(const QList<QUrl> &urls)
{
    d->_urls = urls;
}


void HttpMultiDownloader::setUrls(const QStringList &urls)
{
    d->_urls.clear();
    foreach(const QString &url, urls) {
        d->_urls << QUrl(url);
    }
}

void HttpMultiDownloader::addUrls(const QList<QUrl> &urls)
{
    d->_urls << urls;
}

const QList<QUrl> &HttpMultiDownloader::urls() const
{
    return d->_urls;
}

bool HttpMultiDownloader::startDownload()
{
    if (d->_urls.isEmpty()) {
        LOG_ERROR("Nothing to download");
        Q_EMIT allDownloadFinished();
        return false;
    }
    d->_downloadingIndex = 0;
    d->_downloader->setUrl(d->_urls.at(d->_downloadingIndex));
    if (d->_useUidAsFileName) {
        d->_downloader->setOutputFileName(d->uuidFileName(d->_urls.at(d->_downloadingIndex)));
    }
    if (!d->_downloader->startDownload()) {
        LOG_ERROR("Download not started");
        return false;
    }
    return true;
}


bool HttpMultiDownloader::onCurrentDownloadFinished()
{
    DownloadedUrl dld;
    dld.url = d->_downloader->url();
    dld.errorMessage = d->_downloader->lastErrorString();
    dld.networkError = d->_downloader->networkError();
    dld.outputFile = d->_downloader->outputAbsoluteFileName();
    d->_downloadedUrl.append(dld);

    Q_EMIT downloadFinished(dld.url);

    if (d->_downloadingIndex == (d->_urls.count()-1)) {
        saveXmlUrlFileLinks();
        Q_EMIT allDownloadFinished();
        return true;
    }

    if (d->_downloadingIndex % 10 == 0)
        saveXmlUrlFileLinks();

    ++d->_downloadingIndex;
    d->_downloader->setUrl(d->_urls.at(d->_downloadingIndex));
    if (d->_useUidAsFileName) {
        d->_downloader->setOutputFileName(d->uuidFileName(d->_urls.at(d->_downloadingIndex)));
    }
    if (!d->_downloader->startDownload()) {
        LOG_ERROR("Download not started");
        return false;
    }
    return true;
}

QList<QUrl> HttpMultiDownloader::downloadedUrls() const
{
    QList<QUrl> urls;
    foreach(const DownloadedUrl &dld, d->_downloadedUrl)
        urls << dld.url;
    return urls;
}

QString HttpMultiDownloader::lastErrorString(const QUrl &url) const
{
    const DownloadedUrl &dld = d->downloadedUrl(url);
    return dld.errorMessage;
}

QNetworkReply::NetworkError HttpMultiDownloader::networkError(const QUrl &url) const
{
    const DownloadedUrl &dld = d->downloadedUrl(url);
    return dld.networkError;
}

QString HttpMultiDownloader::outputAbsoluteFileName(const QUrl &url) const
{
    const DownloadedUrl &dld = d->downloadedUrl(url);
    return dld.outputFile;
}
