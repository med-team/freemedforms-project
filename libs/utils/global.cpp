/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#include "global.h"

#include <translationutils/constants.h>
#include <translationutils/trans_filepathxml.h>
#include <translationutils/trans_msgerror.h>
#include <utils/log.h>

#include <QApplication>
#include <QWidget>
#include <QDesktopWidget>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QMessageBox>
#include <QLineEdit>
#include <QTextCodec>
#include <QByteArray>
#include <QFont>
#include <QFileDialog>
#include <QDomNode>
#include <QDomDocument>
#include <QInputDialog>
#include <QProcess>
#include <QTextBrowser>
#include <QDialogButtonBox>
#include <QTextDocument>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QAbstractButton>
#include <QTextDocument>
#include <QCryptographicHash>
#include <QMainWindow>
#include <QModelIndex>
#include <QNetworkConfigurationManager>
#include <QBuffer>
#include <QUuid>



using namespace Utils;
using namespace Trans::ConstantTranslations;

namespace {
static const unsigned char two_letter_country_code_list[] =
"  " // Default
"AF" // Afghanistan
"AL" // Albania
"DZ" // Algeria
"AS" // AmericanSamoa
"AD" // Andorra
"AO" // Angola
"AI" // Anguilla
"AQ" // Antarctica
"AG" // AntiguaAndBarbuda
"AR" // Argentina
"AM" // Armenia
"AW" // Aruba
"AU" // Australia
"AT" // Austria
"AZ" // Azerbaijan
"BS" // Bahamas
"BH" // Bahrain
"BD" // Bangladesh
"BB" // Barbados
"BY" // Belarus
"BE" // Belgium
"BZ" // Belize
"BJ" // Benin
"BM" // Bermuda
"BT" // Bhutan
"BO" // Bolivia
"BA" // BosniaAndHerzegowina
"BW" // Botswana
"BV" // BouvetIsland
"BR" // Brazil
"IO" // BritishIndianOceanTerritory
"BN" // BruneiDarussalam
"BG" // Bulgaria
"BF" // BurkinaFaso
"BI" // Burundi
"KH" // Cambodia
"CM" // Cameroon
"CA" // Canada
"CV" // CapeVerde
"KY" // CaymanIslands
"CF" // CentralAfricanRepublic
"TD" // Chad
"CL" // Chile
"CN" // China
"CX" // ChristmasIsland
"CC" // CocosIslands
"CO" // Colombia
"KM" // Comoros
"CD" // DemocraticRepublicOfCongo
"CG" // PeoplesRepublicOfCongo
"CK" // CookIslands
"CR" // CostaRica
"CI" // IvoryCoast
"HR" // Croatia
"CU" // Cuba
"CY" // Cyprus
"CZ" // CzechRepublic
"DK" // Denmark
"DJ" // Djibouti
"DM" // Dominica
"DO" // DominicanRepublic
"TL" // EastTimor
"EC" // Ecuador
"EG" // Egypt
"SV" // ElSalvador
"GQ" // EquatorialGuinea
"ER" // Eritrea
"EE" // Estonia
"ET" // Ethiopia
"FK" // FalklandIslands
"FO" // FaroeIslands
"FJ" // Fiji
"FI" // Finland
"FR" // France
"FX" // MetropolitanFrance
"GF" // FrenchGuiana
"PF" // FrenchPolynesia
"TF" // FrenchSouthernTerritories
"GA" // Gabon
"GM" // Gambia
"GE" // Georgia
"DE" // Germany
"GH" // Ghana
"GI" // Gibraltar
"GR" // Greece
"GL" // Greenland
"GD" // Grenada
"GP" // Guadeloupe
"GU" // Guam
"GT" // Guatemala
"GN" // Guinea
"GW" // GuineaBissau
"GY" // Guyana
"HT" // Haiti
"HM" // HeardAndMcDonaldIslands
"HN" // Honduras
"HK" // HongKong
"HU" // Hungary
"IS" // Iceland
"IN" // India
"ID" // Indonesia
"IR" // Iran
"IQ" // Iraq
"IE" // Ireland
"IL" // Israel
"IT" // Italy
"JM" // Jamaica
"JP" // Japan
"JO" // Jordan
"KZ" // Kazakhstan
"KE" // Kenya
"KI" // Kiribati
"KP" // DemocraticRepublicOfKorea
"KR" // RepublicOfKorea
"KW" // Kuwait
"KG" // Kyrgyzstan
"LA" // Lao
"LV" // Latvia
"LB" // Lebanon
"LS" // Lesotho
"LR" // Liberia
"LY" // LibyanArabJamahiriya
"LI" // Liechtenstein
"LT" // Lithuania
"LU" // Luxembourg
"MO" // Macau
"MK" // Macedonia
"MG" // Madagascar
"MW" // Malawi
"MY" // Malaysia
"MV" // Maldives
"ML" // Mali
"MT" // Malta
"MH" // MarshallIslands
"MQ" // Martinique
"MR" // Mauritania
"MU" // Mauritius
"YT" // Mayotte
"MX" // Mexico
"FM" // Micronesia
"MD" // Moldova
"MC" // Monaco
"MN" // Mongolia
"MS" // Montserrat
"MA" // Morocco
"MZ" // Mozambique
"MM" // Myanmar
"NA" // Namibia
"NR" // Nauru
"NP" // Nepal
"NL" // Netherlands
"AN" // NetherlandsAntilles
"NC" // NewCaledonia
"NZ" // NewZealand
"NI" // Nicaragua
"NE" // Niger
"NG" // Nigeria
"NU" // Niue
"NF" // NorfolkIsland
"MP" // NorthernMarianaIslands
"NO" // Norway
"OM" // Oman
"PK" // Pakistan
"PW" // Palau
"PS" // PalestinianTerritory
"PA" // Panama
"PG" // PapuaNewGuinea
"PY" // Paraguay
"PE" // Peru
"PH" // Philippines
"PN" // Pitcairn
"PL" // Poland
"PT" // Portugal
"PR" // PuertoRico
"QA" // Qatar
"RE" // Reunion
"RO" // Romania
"RU" // RussianFederation
"RW" // Rwanda
"KN" // SaintKittsAndNevis
"LC" // StLucia
"VC" // StVincentAndTheGrenadines
"WS" // Samoa
"SM" // SanMarino
"ST" // SaoTomeAndPrincipe
"SA" // SaudiArabia
"SN" // Senegal
"SC" // Seychelles
"SL" // SierraLeone
"SG" // Singapore
"SK" // Slovakia
"SI" // Slovenia
"SB" // SolomonIslands
"SO" // Somalia
"ZA" // SouthAfrica
"GS" // SouthGeorgiaAndTheSouthSandwichIslands
"ES" // Spain
"LK" // SriLanka
"SH" // StHelena
"PM" // StPierreAndMiquelon
"SD" // Sudan
"SR" // Suriname
"SJ" // SvalbardAndJanMayenIslands
"SZ" // Swaziland
"SE" // Sweden
"CH" // Switzerland
"SY" // SyrianArabRepublic
"TW" // Taiwan
"TJ" // Tajikistan
"TZ" // Tanzania
"TH" // Thailand
"TG" // Togo
"TK" // Tokelau
"TO" // Tonga
"TT" // TrinidadAndTobago
"TN" // Tunisia
"TR" // Turkey
"TM" // Turkmenistan
"TC" // TurksAndCaicosIslands
"TV" // Tuvalu
"UG" // Uganda
"UA" // Ukraine
"AE" // UnitedArabEmirates
"GB" // UnitedKingdom
"US" // UnitedStates
"UM" // UnitedStatesMinorOutlyingIslands
"UY" // Uruguay
"UZ" // Uzbekistan
"VU" // Vanuatu
"VA" // VaticanCityState
"VE" // Venezuela
"VN" // VietNam
"VG" // BritishVirginIslands
"VI" // USVirginIslands
"WF" // WallisAndFutunaIslands
"EH" // WesternSahara
"YE" // Yemen
"YU" // Yugoslavia
"ZM" // Zambia
"ZW" // Zimbabwe
"CS" // SerbiaAndMontenegro
;

} // End anonymous namespace


namespace Utils {

bool isDebugWithoutInstallCompilation()
{
#ifdef DEBUG_WITHOUT_INSTALL
    return true;
#else
    return false;
#endif
}
bool isReleaseCompilation()
{
#ifdef RELEASE
    return true;
#else
    return false;
#endif
}
bool isGitBuild()
{
#ifdef FULLAPPLICATION_BUILD
    return false;
#else
    return true;
#endif
}
bool isFullApplication()
{
#ifdef FULLAPPLICATION_BUILD
    return true;
#else
    return false;
#endif
}
bool isAlpha()
{
    return qApp->applicationVersion().contains("alpha", Qt::CaseInsensitive);
}
bool isBeta()
{
    return qApp->applicationVersion().contains("beta", Qt::CaseInsensitive);
}
bool isReleaseCandidate()
{
    return qApp->applicationVersion().contains("rc", Qt::CaseInsensitive);
}
bool isRunningOnMac()
{
#ifdef Q_OS_MAC
    return true;
#else
    return false;
#endif
}
bool isRunningOnWin()
{
#ifdef Q_OS_WIN
    return true;
#else
    return false;
#endif
}
bool isRunningOnLinux()
{
#ifdef Q_OS_LINUX
    return true;
#else
    return false;
#endif
}
bool isRunningOnFreebsd()
{
#ifdef Q_OS_FREEBSD
    return true;
#else
    return false;
#endif
}
bool isLinuxIntegratedCompilation()
{
#ifdef LINUX_INTEGRATED
    return true;
#else
    return false;
#endif
}
QString uname()
{
    QString system;
    if (isRunningOnMac())
        system = "MacOs";
    else if (isRunningOnLinux())
        system = "Linux";
    else if (isRunningOnFreebsd())
        system = "FreeBSD";
    else if (isRunningOnWin())
        system = "Windows";
    if (system.isEmpty())
        return QString();
    system += ": ";
    QProcess uname;
    uname.start("uname", QStringList() << "-a");
    if (!uname.waitForStarted())
        LOG_ERROR_FOR("Utils", QApplication::translate("Utils", "Error while retrieve information of uname under %1").arg(system));
    if (!uname.waitForFinished())
        LOG_ERROR_FOR("Utils", QApplication::translate("Utils", "Error while retrieve information of uname under %1").arg(system));
    system += uname.readAll();
    return system.remove("\n");
}
QString osName()
{
    if (isRunningOnLinux())
        return "Linux";
    else if (isRunningOnMac())
        return "MacOs";
    else if (isRunningOnWin())
        return "Windows";
    else if (isRunningOnFreebsd())
        return "FreeBSD";
    return QString();
}

QStringList applicationPluginsPath(const QString &binaryName, const QString &libraryBaseName)
{
    QString app = qApp->applicationDirPath();

    if (isDebugWithoutInstallCompilation()) {
        if (isRunningOnMac()) {
            app.append("/../../../");
        }
        app += "/plugins/";
        qApp->addLibraryPath(QDir::cleanPath(app));
        return QStringList() << QDir::cleanPath(app);
    }

    if (isLinuxIntegratedCompilation()) {
        app = QString("/usr/%1/%2")
                .arg(libraryBaseName)
                .arg(QString(binaryName).remove("_debug").toLower());
        qApp->addLibraryPath(app);
        return QStringList() << app;
    }

    if (isRunningOnMac()) {
        app += "/../plugins/";
    } else if (isRunningOnWin()) {
        app += "/plugins/";
    } else {
        app += "/plugins/";
    }


    QStringList lpath;
    lpath << QDir::cleanPath(app) << QDir::cleanPath(app + "/qt");
    qApp->setLibraryPaths(lpath);
    return lpath;
}


bool copyDir(const QString &absSourcePath, const QString &absDestPath)
{
    if (!QDir(absSourcePath).exists())
        return false;

    if (!QDir(absDestPath).exists()) {
        if (!QDir().mkpath(absDestPath))
            return false;
    }

    QDir srcDir(absSourcePath);
    QFileInfoList files = Utils::getFiles(srcDir);
    foreach(const QFileInfo &info, files) {
        QString srcItem = info.absoluteFilePath();
        QString srcRel = QDir(absSourcePath).relativeFilePath(srcItem);
        if (srcRel.startsWith("..")) {
            LOG_ERROR_FOR("Tools", "Relative path outside source path tree");
            continue;
        }
        QString dstItem = QDir::cleanPath(QString("%1/%2")
                .arg(absDestPath)
                .arg(srcRel));
        if (info.isFile()) {
            if (!QDir().mkpath(QFileInfo(dstItem).absolutePath()))
                return false;
            if (!QFile::copy(srcItem, dstItem)) {
                return false;
            }
        } else {
            qDebug() << "Unhandled item" << info.filePath() << "in Utils::copyDir()";
        }
    }
    return true;
}


bool removeDir(const QString &name, QString *error)
{
    error->clear();
    QDir dir(name);
    if (!dir.exists()) {
        return true;
    }
    QStringList list = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    if (list.count()) {
        error->append(Trans::ConstantTranslations::tkTr(Trans::Constants::PATH_1_CONTAINS_DIRS).arg(name));
        return false;
    }
    list = dir.entryList(QDir::Files | QDir::NoDotAndDotDot);
    foreach(const QString &f, list) {
        if (!dir.remove(f)) {
            error->append(Trans::ConstantTranslations::tkTr(Trans::Constants::FILE_1_CANNOT_BE_REMOVED).arg(f));
            return false;
        }
    }
    if (!dir.rmpath(dir.absolutePath())) {
        error->append(Trans::ConstantTranslations::tkTr(Trans::Constants::PATH_1_CANNOT_BE_REMOVED).arg(dir.absolutePath()));
        return false;
    }
    return true;
}


bool removeDirRecursively(const QString &absPath, QString *error)
{
    if (error)
        error->clear();
    QDir dir(absPath);
    if (!dir.exists())
        return true;

    foreach(const QString &dirPath, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        QString er;
        if (!removeDirRecursively(dir.absolutePath() + QDir::separator() + dirPath, &er)) {
            if (error)
                error->append(er);
            return false;
        }
    }

    QString er;
    if (!removeDir(dir.absolutePath(), &er)) {
        if (error)
            error->append(er);
        return false;
    }

    return true;
}


QFileInfoList getFiles(QDir fromDir, const QStringList &filters, DirSearchType recursive)
{
    QFileInfoList files;
    if (!fromDir.exists())
        return files;
    if (fromDir.path() == ".")
        return files;
    foreach (const QFileInfo & file, fromDir.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot, QDir::DirsFirst | QDir::Name)) {
        if (file.isFile() && (filters.isEmpty() || QDir::match(filters, file.fileName())))
            files << file;
        else if (file.isDir() && recursive==Recursively) {
            fromDir.cd(file.filePath());
            files << getFiles(fromDir, filters, recursive);
            fromDir.cdUp();
        }
    }
    return files;
}


QFileInfoList getFiles(QDir fromDir, const QString &filter, DirSearchType recursive)
{
    return getFiles(fromDir, filter.isEmpty() ? QStringList() : QStringList(filter), recursive);
}


QFileInfoList getDirs(QDir fromDir, const QStringList &filters, DirSearchType recursive)
{
    QFileInfoList dirs;
    foreach (const QFileInfo &file, fromDir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::DirsFirst | QDir::IgnoreCase)) {
        if (file.isFile() && (filters.isEmpty() || QDir::match(filters, file.fileName())))
            dirs << file;
        else if (file.isDir() && recursive==Recursively) {
            fromDir.cd(file.filePath());
            dirs << getFiles(fromDir, filters, recursive);
            fromDir.cdUp();
        }
    }
    return dirs;
}


bool checkDir(const QString &absPath, bool createIfNotExist, const QString &logDirName)
{
    if (!QFile::exists(absPath)) {
        if (createIfNotExist) {
            LOG_FOR("Utils", QCoreApplication::translate("Utils", "%1: %2 does not exist. Trying to create it.")
                               .arg(logDirName, absPath));
            if (!QDir().mkpath(absPath)) {
                LOG_ERROR_FOR("Utils", QCoreApplication::translate("Utils", "Unable to create the %1: %2.")
                              .arg(logDirName, absPath));
                return false;
            }
        } else {
            LOG_FOR("Utils", QCoreApplication::translate("Utils", "%1: %2 does not exist.")
                               .arg(logDirName, absPath));
            return false;
        }
    }
    return true;
}


bool saveStringToFile(const QString &toSave, const QString &toFile, IOMode iomode, const Warn warnUser, QWidget *parent)
{
    return saveStringToEncodedFile(toSave, toFile, "UTF-8", iomode, warnUser, parent);
}


bool saveStringToEncodedFile(const QString &toSave, const QString &toFile, const QString &forceEncoding, IOMode iomode, const Warn warnUser, QWidget *parent)
{
    if (toFile.isEmpty()) {
        LOG_ERROR_FOR("Utils", "saveStringToEncodedFile(): fileName is empty");
        return false;
    }
    QWidget *wgt = parent;
    if (!parent) {
        wgt = qApp->activeWindow();
    }

    QString correctFileName = toFile;
    QFileInfo info(toFile);
    if (info.isRelative())
        correctFileName = qApp->applicationDirPath() + QDir::separator() + toFile;
    info.setFile(correctFileName);

    QFile file(info.absoluteFilePath());
    if (info.exists() && (info.isWritable() && warnUser == WarnUser)) {
        if (QMessageBox::warning(wgt, qApp->applicationName(),
                                 QCoreApplication::translate("Utils" ,
                                                             "File %1 already exists. Do you want de replace it?").arg(info.fileName()),
                                 QMessageBox::Cancel | QMessageBox::Ok) == QMessageBox::Ok) {
            if (iomode == Overwrite) {
                if (!file.open(QFile::WriteOnly | QIODevice::Text)) {
                    LOG_ERROR_FOR("Utils", QCoreApplication::translate("Utils", "Error %1 while trying to save file %2").arg(file.fileName(), file.errorString()));
                    return false;
                }
            } else if (iomode == AppendToFile) {
                if (!file.open(QFile::Append | QIODevice::Text)) {
                    LOG_ERROR_FOR("Utils", QCoreApplication::translate("Utils", "Error %1 while trying to save file %2").arg(file.fileName(), file.errorString()));
                    return false;
                }
            } else {
                return false;
            }

            QTextCodec *codec = QTextCodec::codecForName(forceEncoding.toUtf8());
            if (!codec) {
                LOG_ERROR_FOR("Utils", "Codec not found: " + forceEncoding);
                codec = QTextCodec::codecForName("UTF-8");
            }
            file.write(codec->fromUnicode(toSave));
            file.close();
            if (Log::debugFileInOutProcess())
                LOG_FOR("Utils", QCoreApplication::translate("Utils", "%1 successfully saved (%2)").arg(file.fileName()).arg(forceEncoding));
        } else {
            if (Log::debugFileInOutProcess())
                LOG_FOR("Utils", QCoreApplication::translate("Utils", "Save file aborted by user (file already exists): ") + file.fileName());
            return false;
        }
    } else {
        if (!file.open(QFile::WriteOnly | QIODevice::Text)) {
            LOG_ERROR_FOR("Utils", QCoreApplication::translate("Utils", "Error %1 while trying to save file %2").arg(file.fileName(),file.errorString()));
            return false;
        }

        QTextCodec *codec = QTextCodec::codecForName(forceEncoding.toUtf8());
        if (!codec) {
            LOG_ERROR_FOR("Utils", "Codec not found: " + forceEncoding);
            codec = QTextCodec::codecForName("UTF-8");
        }
        file.write(codec->fromUnicode(toSave));
        file.close();
        if (Log::debugFileInOutProcess())
            LOG_FOR("Utils", QCoreApplication::translate("Utils", "%1 successfully saved (%2)").arg(file.fileName()).arg(forceEncoding));
    }
    return true;
}

bool saveStringToFile(const QString &toSave, const QString &dirPath, const QString &filters, const QString &message, QWidget *parent)
{
    QWidget *wgt = parent;
    if (!parent) {
        wgt = qApp->activeWindow();
    }
    QString fileName;
    if (message.isEmpty())
        fileName = QFileDialog::getSaveFileName(wgt,
                                                QCoreApplication::translate("Utils", "Save to file"),
                                                dirPath,
                                                filters);
    else
        fileName = QFileDialog::getSaveFileName(wgt,
                                                message,
                                                dirPath,
                                                filters);

    if (fileName.isEmpty())
        return false;
    return Utils::saveStringToFile(toSave, fileName, Overwrite, WarnUser, wgt);
}

QString readTextFile(const QString &toRead, const Warn warnUser)
{return readTextFile(toRead, QString("UTF-8"), warnUser);}


QString readTextFile(const QString &toRead, const QString &encoder, const Warn warnUser)
{
    if (toRead.isEmpty())
        return QString();
    QString correctFileName = toRead;
    QFileInfo info(toRead);
    if (info.isRelative())
        correctFileName = qApp->applicationDirPath() + QDir::separator() + toRead;
    info.setFile(correctFileName);

    if (((!info.exists()) || (!info.isReadable())) && (warnUser == WarnUser)) {
        Utils::warningMessageBox(QCoreApplication::translate("Utils" , "File %1 does not exist or is not readable.").arg(correctFileName),
                                 "","", qApp->applicationName());
        return QString::null;
    } else {
        QFile file(correctFileName);
        if (!file.open(QFile::ReadOnly)) {
            LOG_ERROR_FOR("Utils", QCoreApplication::translate("Utils", "Error %1 while trying to open file %2")
                          .arg(correctFileName, file.errorString()));
            return QString::null;
        }
        QTextCodec *codec = QTextCodec::codecForName(encoder.toUtf8());
        if (!codec) {
            LOG_ERROR_FOR("Utils", "No codec for " + encoder);
            return QString();
        }
        QString str = codec->toUnicode(file.readAll());
        if (Log::debugFileInOutProcess())
            LOG_FOR("Utils", tkTr(Trans::Constants::FILE_1_LOADED).arg(toRead));
        return str;
    }
    return QString::null;
}

QString isDirExists(const QString &absPath)
{
    if (QDir(absPath).exists())
        return QDir::cleanPath(absPath);
    return QString();
}

QString isFileExists(const QString &absPath)
{
    if (QFile(absPath).exists())
        return QDir::cleanPath(absPath);
    return QString();
}

QByteArray fileMd5(const QString &fileName)
{
    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly)) {
        QByteArray fileData = file.readAll();
        QByteArray hashData = QCryptographicHash::hash(fileData, QCryptographicHash::Md5);
        return hashData.toHex();
    }
    return QByteArray();
}

QByteArray fileSha1(const QString &fileName)
{
    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly)) {
        QByteArray fileData = file.readAll();
        QByteArray hashData = QCryptographicHash::hash(fileData, QCryptographicHash::Sha1);
        return hashData.toHex();
    }
    return QByteArray();
}

#if QT_VERSION >= 0x050000
QByteArray fileSha256(const QString &fileName)
{
    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly)) {
        QByteArray fileData = file.readAll();
        QByteArray hashData = QCryptographicHash::hash(fileData, QCryptographicHash::Sha256);
        return hashData.toHex();
    }
    return QByteArray();
}
#endif

QString humanReadableFileSize(qint64 size)
{
     float num = size;
     QStringList list;
     list << "KB" << "MB" << "GB" << "TB";
     QStringListIterator i(list);
     QString unit("bytes");
     while(num >= 1024.0 && i.hasNext()) {
         unit = i.next();
         num /= 1024.0;
     }
     return QString("%1 %2").arg(QString().setNum(num,'f',2)).arg(unit);
}

/** \brief Creates an informative messagebox.
  \code
#include <utils/global.h>
[...]
    Utils::informativeMessageBox(...);
\endcode
**/
void informativeMessageBox(const QString &text, const QString &infoText, const QString &detail, const QString &title)
{
    QWidget *parent = qApp->activeWindow();
    QMessageBox mb(parent);
    mb.setWindowModality(Qt::WindowModal);
    mb.setIcon(QMessageBox::Information);
    if (title.isEmpty())
        mb.setWindowTitle(qApp->applicationName());
    else
        mb.setWindowTitle(title);
    mb.setText(text);
    mb.setInformativeText(infoText);
    if (!detail.isEmpty()) {
        if (Qt::mightBeRichText(detail)) {
            QTextDocument doc;
            doc.setHtml(detail);
            mb.setDetailedText(doc.toPlainText());
        } else {
            mb.setDetailedText(detail);
        }
    }
    mb.setStandardButtons(QMessageBox::Ok);
    mb.setDefaultButton(QMessageBox::Ok);
    mb.exec();
    qApp->setActiveWindow(parent);
}

/** \brief Creates a warning messagebox.
  \code
#include <utils/global.h>
[...]
    Utils::warningMessageBox(...);
\endcode
**/
void warningMessageBox(const QString &text, const QString &infoText, const QString &detail, const QString &title)
{
    LOG_FOR("Warning Dialog", infoText);
#if QT_VERSION < 0x050000
    if (qApp->type() == QApplication::Tty) {
#else
    if (!qobject_cast<QApplication*>(qApp)) {  // Equals to not GUI app
#endif
        qWarning() << "  * Text:" << text << "\n  * detail" << detail << "\n  * title:" << title;
        return;
    }
    QWidget *parent = qApp->activeWindow();
    QMessageBox mb(parent);
    mb.setWindowModality(Qt::WindowModal);
    mb.setIcon(QMessageBox::Warning);
    if (title.isEmpty())
        mb.setWindowTitle(qApp->applicationName());
    else
        mb.setWindowTitle(title);
    mb.setText(text);
    mb.setInformativeText(infoText);
    if (!detail.isEmpty()) {
        if (Qt::mightBeRichText(detail)) {
            QTextDocument doc;
            doc.setHtml(detail);
            mb.setDetailedText(doc.toPlainText());
        } else {
            mb.setDetailedText(detail);
        }
    }
    mb.setStandardButtons(QMessageBox::Ok);
    mb.setDefaultButton(QMessageBox::Ok);
    mb.exec();
    qApp->setActiveWindow(parent);
}

/** \brief Creates a messagebox with yes / no. Return true if user clicked yes.
  \code
#include <utils/global.h>
[...]
    bool yes = Utils::yesnoMessageBox(...);
    if (yes) {
    } else {
    }
\endcode
**/
bool yesNoMessageBox(const QString &text, const QString&infoText, const QString&detail, const QString &title, const QPixmap &icon)
{
    QWidget *parent = qApp->activeWindow();
    QMessageBox mb(parent);
    mb.setWindowModality(Qt::WindowModal);
    mb.setWindowFlags(mb.windowFlags() & ~Qt::WindowCloseButtonHint);
    if (icon.isNull()) {
        mb.setIcon(QMessageBox::Question);
    } else {
        mb.setIconPixmap(icon);
    }
    if (title.isEmpty())
        mb.setWindowTitle(qApp->applicationName());
    else
        mb.setWindowTitle(title);
    mb.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    mb.setDefaultButton(QMessageBox::Yes);
    mb.setText(text);
    mb.setInformativeText(infoText);
    if (!detail.isEmpty()) {
        if (Qt::mightBeRichText(detail)) {
            QTextDocument doc;
            doc.setHtml(detail);
            mb.setDetailedText(doc.toPlainText());
        } else {
            mb.setDetailedText(detail);
        }
    }
    int r = mb.exec();
    qApp->setActiveWindow(parent);
    if (r==QMessageBox::Yes)
        return true;
    return false;
}


int withButtonsMessageBox(const QString &text, const QString&infoText, const QString&detail, const QStringList &buttonsText, const QString &title, bool withCancelButton)
{
    QWidget *parent = qApp->activeWindow();
    QMessageBox mb(parent);
    mb.setWindowModality(Qt::WindowModal);
    mb.setIcon(QMessageBox::Question);
    if (title.isEmpty())
        mb.setWindowTitle(qApp->applicationName());
    else
        mb.setWindowTitle(title);
    mb.setText(text);
    mb.setInformativeText(infoText);
    if (!detail.isEmpty()) {
        if (Qt::mightBeRichText(detail)) {
            QTextDocument doc;
            doc.setHtml(detail);
            mb.setDetailedText(doc.toPlainText());
        } else {
            mb.setDetailedText(detail);
        }
    }
    QList<QPushButton *> buttons;
    foreach(const QString &s, buttonsText) {
        buttons << mb.addButton(s, QMessageBox::YesRole);
    }
    if (withCancelButton) {
        buttons << mb.addButton(QApplication::translate("Utils", "Cancel"), QMessageBox::RejectRole);
    }
    mb.setDefaultButton(buttons.at(0));
    int r = mb.exec();
    qApp->setActiveWindow(parent);
    if (r==buttonsText.count())
        return -1;
    return buttons.indexOf(static_cast<QPushButton*>(mb.clickedButton()));
}


int withButtonsMessageBox(const QString &text, const QString&infoText, const QString&detail, QMessageBox::StandardButtons buts, QMessageBox::StandardButton defaultButton, const QString &title)
{
    QWidget *parent = qApp->activeWindow();
    QMessageBox mb(parent);
    mb.setWindowModality(Qt::WindowModal);
    mb.setIcon(QMessageBox::Question);
    if (title.isEmpty())
        mb.setWindowTitle(qApp->applicationName());
    else
        mb.setWindowTitle(title);
    mb.setText(text);
    mb.setInformativeText(infoText);
    if (!detail.isEmpty()) {
        if (Qt::mightBeRichText(detail)) {
            QTextDocument doc;
            doc.setHtml(detail);
            mb.setDetailedText(doc.toPlainText());
        } else {
            mb.setDetailedText(detail);
        }
    }
    mb.setStandardButtons(buts);
    mb.setDefaultButton(defaultButton);
    int r = mb.exec();
    qApp->setActiveWindow(parent);
    return r;
}

bool okCancelMessageBox(const QString &text, const QString&infoText, const QString&detail, const QString &title)
{
    QWidget *parent = qApp->activeWindow();
    QMessageBox mb(parent);
    mb.setWindowModality(Qt::WindowModal);
    mb.setIcon(QMessageBox::Question);
    if (title.isEmpty())
        mb.setWindowTitle(qApp->applicationName());
    else
        mb.setWindowTitle(title);
    mb.setText(text);
    mb.setInformativeText(infoText);
    if (!detail.isEmpty()) {
        if (Qt::mightBeRichText(detail)) {
            QTextDocument doc;
            doc.setHtml(detail);
            mb.setDetailedText(doc.toPlainText());
        } else {
            mb.setDetailedText(detail);
        }
    }
    mb.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    mb.setDefaultButton(QMessageBox::Ok);
    int r = mb.exec();
    qApp->setActiveWindow(parent);
    return (r==QMessageBox::Ok);
}

bool functionNotAvailableMessageBox(const QString &functionText)
{
    informativeMessageBox(functionText,
                           QCoreApplication::translate("Utils", "This function is not available in this version."),
                           QCoreApplication::translate("Utils", "You can send an email to developers and explain your difficulties: freemedforms@googlegroups.com.")
                        );
    return true;
}

void quickDebugDialog(const QStringList &texts)
{
    QDialog *dlg = new QDialog();
    QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok);
    QGridLayout *grid = new QGridLayout(dlg);

    foreach(const QString &s, texts) {
        QTextBrowser *f = new QTextBrowser(dlg);
        if (Qt::mightBeRichText(s))
            f->setHtml(s);
        else
            f->setPlainText(s);
        grid->addWidget(f);
    }
    grid->addWidget(buttonBox);
    dlg->connect(buttonBox, SIGNAL(accepted()), dlg, SLOT(accept()));
    setFullScreen(dlg,true);
    dlg->exec();
    delete buttonBox;
    delete dlg;
}

bool defaultLicenseAgreementDialog(const QString &message, Utils::LicenseTerms::AvailableLicense license)
{
    QDialog dlg;
    QGridLayout layout(&dlg);
    QDialogButtonBox buttonBox(QDialogButtonBox::Yes | QDialogButtonBox::No);
    QTextBrowser tbrowse(&dlg);
    tbrowse.setReadOnly(true);
    QLabel appname(&dlg);
    if (qApp->applicationName().isEmpty()) {
        dlg.setWindowTitle(QCoreApplication::translate("Utils", "License agreement acceptation"));
        appname.setText(QString("<b>%1</b>").arg(QCoreApplication::translate("Utils", "License agreement acceptation")));
    } else {
        dlg.setWindowTitle(qApp->applicationName());
        appname.setText(QString("<b>%1</b>").arg(qApp->applicationName()));
    }
    appname.setAlignment(Qt::AlignCenter);
    QLabel centered;
    if (!message.isEmpty()) {
        centered.setText(message);
    } else {
        centered.setText(QCoreApplication::translate("Utils", "<b>Before you can use this software, you must agree its license terms</b>"));
    }
    QFont bold;
    bold.setBold(true);
    centered.setFont(bold);
    centered.setAlignment(Qt::AlignCenter);
    tbrowse.setText(Utils::LicenseTerms::getTranslatedLicenseTerms(license));
    QLabel question(QCoreApplication::translate("Utils", "Do you agree these terms?"));
    layout.addWidget(&appname);
    layout.addWidget(&centered);
    layout.addWidget(&tbrowse);
    layout.addWidget(&question);
    layout.addWidget(&buttonBox);
    dlg.connect(&buttonBox, SIGNAL(accepted()), &dlg, SLOT(accept()));
    dlg.connect(&buttonBox, SIGNAL(rejected()), &dlg, SLOT(reject()));
    dlg.show();
    qApp->setActiveWindow(&dlg);
    dlg.activateWindow();
    dlg.raise();
    if (dlg.exec()==QDialog::Accepted)
        return true;
    return false;
}

QString askUser(const QString &title, const QString &question)
{
    bool ok;
    QString text = QInputDialog::getText(qApp->activeWindow(), title, question, QLineEdit::Normal, "", &ok);
    if (ok)
        return text;
    return QString();
}

void centerWidget(QWidget *win, QWidget *reference)
{
    if (!win)
        return;
    QPoint center;
    if (!reference) {
        QMainWindow *win = 0;
        QWidgetList list = qApp->topLevelWidgets();
        for(int i = 0; i < list.count(); ++i) {
            win = qobject_cast<QMainWindow*>(list.at(i));
            if (win && win->isVisible()) {
                break;
            }
        }
        if (win)
            reference = win;
        else
            reference = qApp->desktop();
    }
    center = reference->rect().center() + reference->pos();
    QRect rect = win->rect();
    rect.moveCenter(center);
    win->move(rect.topLeft());
}

void setFullScreen(QWidget* win, bool on)
{
    if (win->isFullScreen()==on)
        return;

    if (on) {
        win->setWindowState(win->windowState() | Qt::WindowMaximized);
        LOG_FOR("Utils", QCoreApplication::translate("Utils", "%1 is now in fullScreen Mode.").arg(win->objectName()));
    } else {
        win->setWindowState(win->windowState() & ~Qt::WindowMaximized);
        LOG_FOR("Utils", QCoreApplication::translate("Utils", "%1 is now in non fullScreen Mode.").arg(win->objectName()));
    }
}

void resizeAndCenter(QWidget *widget, QWidget *reference)
{
    QWidget *ref = reference;
    if (!reference)
        ref = qApp->activeWindow();
    if (!ref) {
        widget->adjustSize();
        return;
    }
    QSize size = ref->size();
    size = QSize(size.width()*0.9, size.height()*0.9);
    widget->resize(size);
    centerWidget(widget, ref);
}

void switchEchoMode(QLineEdit * l)
{
    if (l->echoMode() == QLineEdit::Normal)
        l->setEchoMode(QLineEdit::Password);
    else
        l->setEchoMode(QLineEdit::Normal);
}

QDateTime roundDateTime(const QDateTime &date, const int minutesRound)
{
    Q_ASSERT(minutesRound > 0);
    if (date.isNull())
        return QDateTime();
    if (date.time().minute() % minutesRound == 0
            && date.time().second() == 0
            && date.time().msec() == 0)
        return QDateTime(date.date(), QTime(date.time().hour(), date.time().minute(), 0));
    QDateTime dt = QDateTime(date.date(), QTime(date.time().hour(), date.time().minute(), 0));
    int minToRound = dt.time().minute() % minutesRound;
    dt = dt.addSecs((minutesRound - minToRound)*60);
    return dt;
}

bool inRange(const int min, const int max, const int value)
{
    return ((value >= min) && (value <= max));
}

bool inRange(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QModelIndex &value)
{
    if (!inRange(topLeft.row(), bottomRight.row(), value.row()))
        return false;
    int min = (topLeft.row() * 1000) + topLeft.column();
    int max = (bottomRight.row() * 1000) + bottomRight.column();
    int val = (value.row() * 1000) + value.column();
    return inRange(min, max, val);
}

QString countryToIso(QLocale::Country country)
{
    if (country == QLocale::AnyCountry)
        return QString();

    QString code;
    code.resize(2);
    const unsigned char *c = two_letter_country_code_list + 2 * (uint(country));
    code[0] = ushort(c[0]);
    code[1] = ushort(c[1]);
    return code;
}

/*!
 * \brief Takes  \e country and returns a localized string using QLocale::countryToString()
 *
 * \param country QString an ISO-3166-1 2-char code of the country
 * \returns a localized string for the given code using QLocale::countryToString()
 */
QString countryIsoToName(const QString &country)
{
    if (country.size() != 2)
        return QString();
    QString t;
    t.resize(2);
    int c = 2;
    int max = sizeof(two_letter_country_code_list)/sizeof (*two_letter_country_code_list);
    int i = c;
    while (i < max) {
        const unsigned char *c = two_letter_country_code_list + i;
        t[0] = ushort(c[0]);
        t[1] = ushort(c[1]);
        if (t.compare(country, Qt::CaseInsensitive)==0) {
            return QLocale::countryToString(QLocale::Country(i/2));
        }
        ++i;
        ++i;
    }
    return QString();
}

/*!
 * \brief Maps an ISO string to Qt's internal Country code.
 * \param country an ISO-3166-1 2-char string
 * \returns a QLocale::Country code for the given \e country
 */
QLocale::Country countryIsoToCountry(const QString &country) {
    if (country.size() != 2)
        return QLocale::AnyCountry;
    QString t;
    t.resize(2);
    int c = 2;
    int max = sizeof(two_letter_country_code_list)/sizeof (*two_letter_country_code_list);
    int i = c;
    while (i < max) {
        const unsigned char *c = two_letter_country_code_list + i;
        t[0] = ushort(c[0]);
        t[1] = ushort(c[1]);
        if (t.compare(country, Qt::CaseInsensitive) == 0) {
            return QLocale::Country(i/2);
        }
        ++i;
        ++i;
    }
    return QLocale::AnyCountry;
}


QString htmlBodyContent(const QString &fullHtml, bool replaceBodyTagByParagraphTag)
{
    if (fullHtml.isEmpty())
        return QString::null;
    int beg = fullHtml.indexOf("<body");
    if (beg == -1)
        return fullHtml;
    beg += 5;
    int end = fullHtml.indexOf(">", beg);
    QString style = fullHtml.mid(beg, end-beg).trimmed();
    beg = end + 1;
    end = fullHtml.indexOf("</body>", beg);

    if (end < beg)
        end = fullHtml.length();

    if (replaceBodyTagByParagraphTag)
        return QString("<%1%2>%3</%1>")
                .arg("p")
                .arg(style.isEmpty()? "" : QString(" %1").arg(style))
                .arg(fullHtml.mid(beg, end-beg));
    return fullHtml.mid(beg, end-beg);
}


QString htmlTakeAllCssContent(QString &fullHtml)
{
    if (fullHtml.isEmpty())
        return QString::null;
    QString css;
    typedef QPair<int,int> pairs;
    QList<pairs> removalIndexes; // begin, end
    int begin = 0;
    while (begin >= 0) {
        begin = fullHtml.indexOf("<style", begin);
        if (begin != -1) {
            int end = fullHtml.indexOf("</style>", begin);
            if (end != -1) {
                end += 8;
                removalIndexes << pairs(begin, end);
                css += fullHtml.mid(begin, end-begin);
            } else {
                end = fullHtml.indexOf("/>", begin);
                Q_ASSERT(false); // "CSS extraction from file declaration not yet implemented"
            }
            begin += end;
        }
    }

    for (int i = removalIndexes.count() - 1; i > -1; --i) {
        const pairs &indexes = removalIndexes.at(i);
        fullHtml = fullHtml.remove(indexes.first, indexes.second - indexes.first);
    }

    return css;
}

QString htmlReplaceParagraphWithDiv(const QString &fullHtml)
{
    if (fullHtml.isEmpty())
        return QString::null;
    QString r = fullHtml;
    r = r.replace("<p>", "<div>");
    r = r.replace("<p ", "<div ");
    r = r.replace("</p>", "</div>");
    return r;
}


QString htmlRemoveLinkTags(const QString &fullHtml)
{
    QString html = fullHtml;
    int begin = html.indexOf("<a ", 0, Qt::CaseInsensitive);
    while (begin != -1) {
        int end = html.indexOf(">", begin);
        if (end != -1) {
            html = html.remove(begin, end-begin+1);
        }
        begin = html.indexOf("<a ", begin, Qt::CaseInsensitive);
    }
    html = html.remove("</a>", Qt::CaseInsensitive);
    return html;
}

QString fontToHtml(const QFont &font, const QColor &color)
{
    QString style;
    style = QString("font-family:%1;").arg(font.family());
    style += QString("font-size:%1pt;").arg(font.pointSize());
    if (font.bold())
        style += "font-weight:bold;";
    else
        style += "font-weight:normal;";
    if (font.italic())
        style += "font-style:italic;";
    else
        style += "font-style:normal;";
    if (font.underline())
        style += "text-decoration:underline;";
    else
        style += "text-decoration:none;";
    if (color.isValid()) {
        style += QString("color:%1;").arg(color.name());
    }
    return style;
}

QString textAlignmentToHtml(const Qt::Alignment &align)
{
    QString toReturn;
    if (align & Qt::AlignCenter)
        toReturn = "center";
    else if (align & Qt::AlignJustify)
        toReturn = "justify";
    else if (align & Qt::AlignRight)
        toReturn = "right";
    else
        toReturn = "left";
    if (!toReturn.isEmpty()) {
        toReturn.prepend("align=\"");
        toReturn += "\" ";
    }
    return toReturn;
}


QString htmlReplaceAccents(const QString &html)
{
    if (html.isEmpty())
        return html;
    QString toReturn = html;
    QHash< QString, QString > accents;
    accents.insert(QString::fromUtf8("é"), "&eacute;");
    accents.insert(QString(QChar(QChar::Nbsp)), "&nbsp;");

    accents.insert(QString::fromUtf8("è"), "&egrave;");
    accents.insert(QString::fromUtf8("à"), "&agrave;");
    accents.insert(QString::fromUtf8("ù"), "&ugrave;");

    accents.insert(QString::fromUtf8("ê"), "&ecirc;");
    accents.insert(QString::fromUtf8("â"), "&acirc;");
    accents.insert(QString::fromUtf8("î"), "&icirc;");
    accents.insert(QString::fromUtf8("ô"), "&ocirc;");
    accents.insert(QString::fromUtf8("û"), "&ucirc;");

    accents.insert(QString::fromUtf8("ä"), "&auml;");
    accents.insert(QString::fromUtf8("ë"), "&euml;");
    accents.insert(QString::fromUtf8("ï"), "&iuml;");
    accents.insert(QString::fromUtf8("ö"), "&ouml;");
    accents.insert(QString::fromUtf8("ü"), "&uuml;");

    accents.insert(QString::fromUtf8("œ"), "&oelig;");
    accents.insert(QString::fromUtf8("æ"), "&aelig;");
    accents.insert(QString::fromUtf8("ç"), "&ccedil;");

    accents.insert(QString::fromUtf8("ø"), "&oslash;");
    accents.insert(QString::fromUtf8("Ø"), "&Oslash;");

    accents.insert(QString::fromUtf8("É"), "&Eacute;");

    accents.insert(QString::fromUtf8("È"), "&Egrave;");
    accents.insert(QString::fromUtf8("À"), "&Agrave;");
    accents.insert(QString::fromUtf8("Ù"), "&Ugrave;");

    accents.insert(QString::fromUtf8("Ê"), "&Ecirc;");
    accents.insert(QString::fromUtf8("Â"), "&Acirc;");
    accents.insert(QString::fromUtf8("Î"), "&Icirc;");
    accents.insert(QString::fromUtf8("Ô"), "&Ocirc;");
    accents.insert(QString::fromUtf8("Û"), "&Ucirc;");

    accents.insert(QString::fromUtf8("Ä"), "&Auml;");
    accents.insert(QString::fromUtf8("Ë"), "&Euml;");
    accents.insert(QString::fromUtf8("Ï"), "&Iuml;");
    accents.insert(QString::fromUtf8("Ö"), "&Ouml;");
    accents.insert(QString::fromUtf8("Ü"), "&Uuml;");

    accents.insert(QString::fromUtf8("ã"), "&atilde;");
    accents.insert(QString::fromUtf8("õ"), "&otilde;");
    accents.insert(QString::fromUtf8("Ã"), "&Atilde;");
    accents.insert(QString::fromUtf8("Õ"), "&Otilde;");

    accents.insert(QString::fromUtf8("Œ"), "&OElig;");
    accents.insert(QString::fromUtf8("Æ"), "&AElig;");
    accents.insert(QString::fromUtf8("Ç"), "&Ccedil;");

    accents.insert(QString::fromUtf8("ø"), "&oslash;");
    accents.insert(QString::fromUtf8("Ø"), "&Oslash;");
    accents.insert(QString::fromUtf8("€"), "&#128;");
    accents.insert(QString::fromUtf8("¡"), "&iexcl;");

    accents.insert(QString::fromUtf8("¢"), "&cent;");
    accents.insert(QString::fromUtf8("£"), "&pound;");
    accents.insert(QString::fromUtf8("¤"), "&curren;");
    accents.insert(QString::fromUtf8("¥"), "&yen;");
    accents.insert(QString::fromUtf8("¦"), "&brvbar;");
    accents.insert(QString::fromUtf8("µ"), "&micro;");
    accents.insert(QString::fromUtf8("·"), "&middot;");
    accents.insert(QString::fromUtf8("»"), "&raquo;");
    accents.insert(QString::fromUtf8("«"), "&laquo;");
    accents.insert(QString::fromUtf8("¼"), "&frac14;");
    accents.insert(QString::fromUtf8("½"), "&frac12;");
    accents.insert(QString::fromUtf8("¾"), "&frac34;");
    accents.insert(QString::fromUtf8("¿"), "&iquest;");
    accents.insert(QString::fromUtf8("÷"), "&divide;");
    accents.insert(QString::fromUtf8("•"), "&#149;");
    accents.insert(QString::fromUtf8("©"), "&copy;");
    accents.insert(QString::fromUtf8("®"), "&reg;");
    accents.insert(QString::fromUtf8("™"), "&#153;");
    accents.insert(QString::fromUtf8("§"), "&sect;");
    accents.insert(QString::fromUtf8("…"), "&#133;");
    accents.insert(QString::fromUtf8("ˆ"), "&#136;");
    accents.insert(QString::fromUtf8("—"), "&mdash;");

    foreach(const QString &k, accents.keys()) {
        toReturn.replace(k, accents.value(k));
    }
    return toReturn;
}

QStringList htmlGetLinksToCssContent(const QString &html)
{
    QStringList list;
    if (html.isEmpty())
        return list;

    int begin = 0;
    do {
        begin = html.indexOf("<link ", begin);
        if (begin == -1)
            break;
        int endTag = html.indexOf(">", begin+6);
        if (endTag == -1)
            break;
        QString content = html.mid(begin, endTag-begin);
        if (content.contains("css", Qt::CaseInsensitive)
                && content.contains("href", Qt::CaseInsensitive) ) {
            int b = content.indexOf("href");
            b = content.indexOf("\"", b+4) + 1;
            int e = content.indexOf("\"", b);
            list << content.mid(b, e-b);
        }
        begin = endTag;
    } while (begin > 0);
    list.removeAll("");
    return list;
}

QString firstLetterUpperCase(const QString &s)
{
    if (s.isEmpty())
        return QString::null;
    QString tmp = s;
    tmp[0] = tmp[0].toUpper();
    return tmp;
}

QString correctTextAccentEncoding(const QString &text)
{
    QByteArray ba = text.toUtf8();
    QHash<QByteArray, QByteArray> conv;
    conv.insert(QByteArray("Å¡"), QByteArray("š"));
    conv.insert(QByteArray("Å'"), QByteArray("Œ"));
    conv.insert(QByteArray("Å\""), QByteArray("œ"));
    conv.insert(QByteArray("Å¸"), QByteArray("Ÿ"));
    conv.insert(QByteArray("Ã¿"), QByteArray("ÿ"));
    conv.insert(QByteArray("Ã€"), QByteArray("À"));
    QString s;
    s.append(QChar(195));
    s.append(QChar(160));
    conv.insert(s.toUtf8(), QByteArray("à"));
    conv.insert(QByteArray("Ã¡"), QByteArray("á"));
    conv.insert(QByteArray("Ã‚"), QByteArray("Â"));
    conv.insert(QByteArray("Ã¢"), QByteArray("â"));
    conv.insert(QByteArray("Ãƒ"), QByteArray("Ã"));
    conv.insert(QByteArray("Ã£"), QByteArray("ã"));
    conv.insert(QByteArray("Ã„"), QByteArray("Ä"));
    conv.insert(QByteArray("Ã¤"), QByteArray("ä"));
    conv.insert(QByteArray("Ã…"), QByteArray("Å"));
    conv.insert(QByteArray("Ã¥"), QByteArray("å"));
    conv.insert(QByteArray("Ã†"), QByteArray("Æ"));
    conv.insert(QByteArray("Ã¦"), QByteArray("æ"));
    conv.insert(QByteArray("Ã‡"), QByteArray("Ç"));
    conv.insert(QByteArray("Ã§"), QByteArray("ç"));
    conv.insert(QByteArray("Ãˆ"), QByteArray("È"));
    conv.insert(QByteArray("Ã¨"), QByteArray("è"));
    conv.insert(QByteArray("Ã‰"), QByteArray("É"));
    conv.insert(QByteArray("Ã©"), QByteArray("é"));
    conv.insert(QByteArray("ÃŠ"), QByteArray("Ê"));
    conv.insert(QByteArray("Ãª"), QByteArray("ê"));
    conv.insert(QByteArray("Ã‹"), QByteArray("Ë"));
    conv.insert(QByteArray("Ã«"), QByteArray("ë"));
    conv.insert(QByteArray("ÃŒ"), QByteArray("Ì"));
    conv.insert(QByteArray("Ã¬"), QByteArray("ì"));
    conv.insert(QByteArray("Ã"), QByteArray("Í"));
    conv.insert(QByteArray("Ã­"), QByteArray("í"));
    conv.insert(QByteArray("ÃŽ"), QByteArray("Î"));
    conv.insert(QByteArray("Ã®"), QByteArray("î"));
    conv.insert(QByteArray("Ã¯"), QByteArray("ï"));
    conv.insert(QByteArray("Ã"), QByteArray("Ð"));
    conv.insert(QByteArray("Ã°"), QByteArray("ð"));
    conv.insert(QByteArray("Ã'"), QByteArray("Ñ"));
    conv.insert(QByteArray("Ã±"), QByteArray("ñ"));
    conv.insert(QByteArray("Ã'"), QByteArray("Ò"));
    conv.insert(QByteArray("Ã²"), QByteArray("ò"));
    conv.insert(QByteArray("Ã\""), QByteArray("Ó"));
    conv.insert(QByteArray("Ã³"), QByteArray("ó"));
    conv.insert(QByteArray("Ã\""), QByteArray("Ô"));
    conv.insert(QByteArray("Ã´"), QByteArray("ô"));
    conv.insert(QByteArray("Ã•"), QByteArray("Õ"));
    conv.insert(QByteArray("Ãµ"), QByteArray("õ"));
    conv.insert(QByteArray("Ã–"), QByteArray("Ö"));
    conv.insert(QByteArray("Ã˜"), QByteArray("Ø"));
    conv.insert(QByteArray("Ã¸"), QByteArray("ø"));
    conv.insert(QByteArray("Ã™"), QByteArray("Ù"));
    conv.insert(QByteArray("Ã¹"), QByteArray("ù"));
    conv.insert(QByteArray("Ãš"), QByteArray("Ú"));
    conv.insert(QByteArray("Ãº"), QByteArray("ú"));
    conv.insert(QByteArray("Ã›"), QByteArray("Û"));
    conv.insert(QByteArray("Ã»"), QByteArray("û"));
    conv.insert(QByteArray("Ãœ"), QByteArray("Ü"));
    conv.insert(QByteArray("Ã¼"), QByteArray("ü"));
    conv.insert(QByteArray("Ã½"), QByteArray("ý"));
    conv.insert(QByteArray("Ãž"), QByteArray("Þ"));
    conv.insert(QByteArray("Ã¾"), QByteArray("þ"));
    conv.insert(QByteArray("ÃŸ"), QByteArray("ß"));
    conv.insert(QByteArray("Ã¶"), QByteArray("ö"));
    QHash<QByteArray, QByteArray>::const_iterator it = conv.constBegin();
    while (it != conv.constEnd()) {
        ba = ba.replace(it.key(), it.value());
        ++it;
    }
    return QString::fromUtf8(ba);
}

QString removeAccents(const QString &text)
{
    if (text.isEmpty())
        return QString::null;
    QHash< QString, QString > accents;
    accents.insert(QString::fromUtf8("é"), "e");
    accents.insert(QString::fromUtf8("è"), "e");
    accents.insert(QString::fromUtf8("à"), "a");
    accents.insert(QString::fromUtf8("ù"), "u");

    accents.insert(QString::fromUtf8("ê"), "e");
    accents.insert(QString::fromUtf8("â"), "a");
    accents.insert(QString::fromUtf8("î"), "i");
    accents.insert(QString::fromUtf8("ô"), "o");
    accents.insert(QString::fromUtf8("û"), "u");

    accents.insert(QString::fromUtf8("ä"), "a");
    accents.insert(QString::fromUtf8("ë"), "e");
    accents.insert(QString::fromUtf8("ï"), "i");
    accents.insert(QString::fromUtf8("ö"), "o");
    accents.insert(QString::fromUtf8("ü"), "u");

    accents.insert(QString::fromUtf8("œ"), "oe");
    accents.insert(QString::fromUtf8("æ"), "ae");
    accents.insert(QString::fromUtf8("ç"), "c");

    accents.insert(QString::fromUtf8("ø"), "o");
    accents.insert(QString::fromUtf8("Ø"), "O");

    accents.insert(QString::fromUtf8("É"), "E");
    accents.insert(QString::fromUtf8("È"), "E");
    accents.insert(QString::fromUtf8("À"), "A");
    accents.insert(QString::fromUtf8("Ù"), "U");

    accents.insert(QString::fromUtf8("Â"), "A");
    accents.insert(QString::fromUtf8("Ê"), "E");
    accents.insert(QString::fromUtf8("Î"), "I");
    accents.insert(QString::fromUtf8("Ô"), "O");
    accents.insert(QString::fromUtf8("Û"), "U");

    accents.insert(QString::fromUtf8("Ä"), "A");
    accents.insert(QString::fromUtf8("Ë"), "E");
    accents.insert(QString::fromUtf8("Ï"), "I");
    accents.insert(QString::fromUtf8("Ö"), "O");
    accents.insert(QString::fromUtf8("Ü"), "U");

    accents.insert(QString::fromUtf8("Œ"), "OE");
    accents.insert(QString::fromUtf8("Æ"), "AE");
    accents.insert(QString::fromUtf8("Ç"), "C");

    accents.insert(QString::fromUtf8("ø"), "o");
    accents.insert(QString::fromUtf8("Ø"), "O");

    accents.insert(QString::fromUtf8("ã"), "a");
    accents.insert(QString::fromUtf8("õ"), "o");
    accents.insert(QString::fromUtf8("Ã"), "A");
    accents.insert(QString::fromUtf8("Õ"), "O");

    QString toReturn = text;
    foreach(const QString &k, accents.keys())
        toReturn = toReturn.replace(k, accents.value(k));

    return toReturn;
}

static inline bool isLineSplitter(const QChar &c)
{
    return (c == ' '
            || c =='/'
            || c ==','
            || c ==';'
            || c =='.'
            || c =='?'
            || c ==':'
            || c =='-');
}

QString lineWrapString(const QString &in, int lineLength)
{
    if (in.isEmpty())
        return QString::null;
    QString tempStr = in;
    int len = in.length();
    int pos = lineLength;

    while (pos < len-1) {
        int tempPos = pos;

        while (!isLineSplitter(tempStr.at(tempPos)) && tempPos > 0) {
            tempPos--;
        }

        if (tempPos > 0)
            pos=tempPos;

        if (tempStr.at(tempPos)==' ') {
            tempStr.replace(tempPos, 1, '\n');
        } else {
            tempStr.insert(tempPos, '\n');
            ++len;
        }

        pos+=lineLength;
    }

    return tempStr;
}

QString centerString(const QString &in, const QChar &fill, int lineSize)
{
    QString out;
    out.fill(fill, lineSize);
    int begin = lineSize/2 - in.size()/2;
    if (begin <= 0)
        return in;
    out = out.replace(begin, in.size(), in);
    return out;
}

QString indentString(const QString &in, int lineIndent)
{
    if (in.isEmpty())
        return QString::null;
    QString indent;
    indent = indent.fill(' ', lineIndent);
    QString correctedIn = in;
    correctedIn = correctedIn.replace("\n", QString("\n"+indent));
    return QString("%1%2").arg(indent).arg(correctedIn);
}


QString createXml(const QString &mainTag, const QHash<QString,QString> &data, const int indent,const bool valueToBase64 )
{
    QDomDocument doc;
    QDomElement main = doc.createElement(mainTag);
    doc.appendChild(main);
    if (valueToBase64) {
        foreach(const QString &k, data.keys()) {
            QDomElement dataElement  = doc.createElement(k);
            main.appendChild(dataElement);
            if (!data.value(k).isEmpty()) {
                QDomText dataText = doc.createTextNode(data.value(k).toUtf8().toBase64());
                dataElement.appendChild(dataText);
            }
        }
    } else {
        foreach(const QString &k, data.keys()) {
            QDomElement dataElement  = doc.createElement(k);
            main.appendChild(dataElement);
            if (!data.value(k).isEmpty()) {
                QDomText dataText = doc.createTextNode(data.value(k));
                dataElement.appendChild(dataText);
            }
        }
    }
    return doc.toString(indent);
}


bool readXml(const QString &xmlContent, const QString &generalTag, QHash<QString,QString> &readData, const bool valueFromBase64)
{
    if (!xmlContent.contains(generalTag)) {
        Utils::Log::addError("Utils::readXml",QString("Error while reading Xml: tag %1 not found").arg(generalTag),
                             __FILE__, __LINE__);
        return false;
    }
    readData.clear();

    QDomDocument doc;
    doc.setContent(xmlContent);
    QDomElement root = doc.documentElement();
    QDomElement paramElem = root.firstChildElement();

    if (valueFromBase64) {
        while (!paramElem.isNull()) {
            if (!paramElem.tagName().compare(generalTag, Qt::CaseInsensitive)) {
                paramElem = paramElem.nextSiblingElement();
                continue;
            }
            readData.insert(paramElem.tagName(), QByteArray::fromBase64(paramElem.text().trimmed().toUtf8()));
            paramElem = paramElem.nextSiblingElement();
        }
    } else {
        while (!paramElem.isNull()) {
            if (!paramElem.tagName().compare(generalTag, Qt::CaseInsensitive)) {
                paramElem = paramElem.nextSiblingElement();
                continue;
            }
            readData.insert(paramElem.tagName(), paramElem.text().trimmed().toUtf8());
            paramElem = paramElem.nextSiblingElement();
        }
    }

    return true;
}

QString xmlRead(const QDomElement &father, const QString &name, const QString &defaultValue)
{
    QDomElement elem = father.firstChildElement(name);

    if (elem.isNull())
        return defaultValue;

    return elem.text();
}

QString xmlRead(const QDomElement &father, const QString &name, const char *defaultValue)
{
    QString defaultStr(defaultValue);
    return Utils::xmlRead(father, name, defaultStr);
}

int xmlRead(const QDomElement &father, const QString &name, const int defaultValue)
{
    QString defaultStr = QString::number(defaultValue);
    QString strValue = Utils::xmlRead(father, name, defaultStr);
    bool ok;
    int val = strValue.toInt(&ok);
    if (ok)
        return val;
    else
        return defaultValue;
}

int xmlRead(const QDomElement &father, const QString &name, const long int defaultValue)
{
    QString defaultStr = QString::number(defaultValue);
    QString strValue = Utils::xmlRead(father, name, defaultStr);
    bool ok;
    long int val = strValue.toLong(&ok);
    if (ok)
        return val;
    else
        return defaultValue;
}

bool xmlRead(const QDomElement &father, const QString &name, const bool defaultValue)
{
    QString defaultStr = QString::number((int) defaultValue);
    QString strValue = Utils::xmlRead(father, name, defaultStr);
    bool ok;
    int val = strValue.toInt(&ok);
    if (ok)
        return val;
    else
        return defaultValue;
}

void xmlWrite(QDomElement &father, const QString &name, const QString &value)
{
    QDomDocument document = father.ownerDocument();

    QDomElement elem = document.createElement(name);
    father.appendChild(elem);

    QDomText t = document.createTextNode(value);
    elem.appendChild(t);
}

void xmlWrite(QDomElement &father, const QString &name, char *value)
{
    QString strValue(value);
    Utils::xmlWrite(father, name, strValue);
}

void xmlWrite(QDomElement &father, const QString &name, int value)
{
    QString valueStr = QString::number(value);
    Utils::xmlWrite(father, name, valueStr);
}

void xmlWrite(QDomElement &father, const QString &name, long int value)
{
    QString valueStr = QString::number(value);
    Utils::xmlWrite(father, name, valueStr);
}

void xmlWrite(QDomElement &father, const QString &name, bool value)
{
    QString valueStr = QString::number((int) value);
    Utils::xmlWrite(father, name, valueStr);
}


QByteArray pixmapToBase64(const QPixmap &pixmap)
{
    QByteArray byteArray;
    if (pixmap.isNull())
        return byteArray;
    QBuffer buffer(&byteArray);
    if (!pixmap.save(&buffer, "PNG")) {
        LOG_ERROR_FOR("Global",
                      QString("Unable to transform QPixmap to base64 QByteArray. "
                              "Pixmap size: (%1,%2)")
                      .arg(pixmap.size().width())
                      .arg(pixmap.size().height()));
        return QByteArray();
    }
    return byteArray.toBase64();
}


QPixmap pixmapFromBase64(const QByteArray &base64)
{
    QPixmap pix;
    if (base64.isEmpty())
        return pix;
    if (!pix.loadFromData(QByteArray::fromBase64(base64))) {
        LOG_ERROR_FOR("Global", "Unable to transform base64 QByteArray to QPixmap");
        return QPixmap();
    }
    return pix;
}

int replaceToken(QString &textToAnalyse, const QString &token, const QString &value)
{
    if (!textToAnalyse.contains(token))
        return 0;
    QString t = token;
    t.remove(Constants::TOKEN_OPEN);
    t.remove(Constants::TOKEN_CLOSE);
    int begin, end;
    begin = 0;
    end = 0;
    int tokenLength = token.length() + QString(Constants::TOKEN_OPEN).length() + QString(Constants::TOKEN_CLOSE).length();
    int toReturn = 0;
    while (true) {
        begin = textToAnalyse.indexOf(Constants::TOKEN_OPEN + token + Constants::TOKEN_CLOSE, begin);
        if (begin==-1)
            break;
        end = begin + tokenLength;
        int beforeBegin;
        beforeBegin = textToAnalyse.lastIndexOf(Constants::TOKEN_OPEN, begin - 1);
        int afterEnd;
        afterEnd = textToAnalyse.indexOf(Constants::TOKEN_CLOSE, end);
        if ((beforeBegin==-1) || (afterEnd==-1)) {
            Utils::Log::addError("Utils", QApplication::translate("Utils", "Token replacement error (%1). Wrong number of parentheses.")
                                                                .arg(token + QString::number(beforeBegin)),
                                                                __FILE__, __LINE__);
            begin = end;
            continue;
        }
        if (value.isEmpty()) {
            textToAnalyse.remove(beforeBegin, afterEnd-beforeBegin+1);
            ++toReturn;
        } else {
            QString before = textToAnalyse.mid(beforeBegin, begin-beforeBegin);
            QString after = textToAnalyse.mid(end, afterEnd-end);
            textToAnalyse.remove(afterEnd,1);
            textToAnalyse.replace(begin, end-begin, value);
            textToAnalyse.remove(beforeBegin,1);
            ++toReturn;
        }
    }
    return toReturn;
}

int replaceTokens(QString &textToAnalyse, const QHash<QString, QString> &tokens_values)
{
    if (tokens_values.isEmpty())
        return 0;
    int i = 0;
    foreach(const QString &tok, tokens_values.keys()) {
        i += replaceToken(textToAnalyse, tok, tokens_values.value(tok));
    }
    return i;
}

int replaceTokens(QString &textToAnalyse, const QHash<QString, QVariant> &tokens_values)
{
    if (tokens_values.isEmpty())
        return 0;
    int i = 0;
    foreach(const QString &tok, tokens_values.keys()) {
        i += replaceToken(textToAnalyse, tok, tokens_values.value(tok).toString());
    }
    return i;
}


QString testInternetConnection()
{
    return QString("yes");
}


QString createUid()
{
    return QUuid::createUuid().toString().remove("-").remove("{").remove("}");
}


void linkSignalsFromFirstModelToSecondModel(QAbstractItemModel *model1, QAbstractItemModel *model2, bool connectDataChanged)
{
    QObject::connect(model1, SIGNAL(columnsAboutToBeInserted(QModelIndex, int , int )), model2, SIGNAL(columnsAboutToBeInserted(QModelIndex, int , int )));
    QObject::connect(model1, SIGNAL(columnsAboutToBeMoved(QModelIndex, int , int , QModelIndex, int )), model2, SIGNAL(columnsAboutToBeMoved(QModelIndex, int , int , QModelIndex, int )));
    QObject::connect(model1, SIGNAL(columnsAboutToBeRemoved(QModelIndex, int , int )), model2, SIGNAL( columnsAboutToBeRemoved(QModelIndex, int , int )));
    QObject::connect(model1, SIGNAL(columnsInserted(QModelIndex, int , int )), model2, SIGNAL(columnsInserted(QModelIndex, int , int )));
    QObject::connect(model1, SIGNAL(columnsMoved(QModelIndex, int , int , QModelIndex, int )), model2, SIGNAL(columnsMoved(QModelIndex, int , int , QModelIndex, int )));
    QObject::connect(model1, SIGNAL(columnsRemoved(QModelIndex, int , int )), model2, SIGNAL(columnsRemoved(QModelIndex, int , int )));
    if (connectDataChanged)
        QObject::connect(model1, SIGNAL(dataChanged(QModelIndex,QModelIndex)), model2, SIGNAL(dataChanged(QModelIndex,QModelIndex)));
    QObject::connect(model1, SIGNAL(headerDataChanged(Qt::Orientation, int, int)), model2, SIGNAL(headerDataChanged(Qt::Orientation, int, int)));
    QObject::connect(model1, SIGNAL(layoutAboutToBeChanged()), model2, SIGNAL(layoutAboutToBeChanged()));
    QObject::connect(model1, SIGNAL(layoutChanged()), model2, SIGNAL(layoutChanged()));
    QObject::connect(model1, SIGNAL(modelAboutToBeReset()), model2, SIGNAL(modelAboutToBeReset()));
    QObject::connect(model1, SIGNAL(modelReset()), model2, SIGNAL(modelReset()));
    QObject::connect(model1, SIGNAL(rowsAboutToBeInserted(QModelIndex, int , int )), model2, SIGNAL(rowsAboutToBeInserted(QModelIndex, int , int )));
    QObject::connect(model1, SIGNAL(rowsAboutToBeMoved(QModelIndex, int, int , QModelIndex, int)), model2, SIGNAL(rowsAboutToBeMoved(QModelIndex, int, int , QModelIndex, int)));
    QObject::connect(model1, SIGNAL(rowsAboutToBeRemoved(QModelIndex, int , int )), model2, SIGNAL(rowsAboutToBeRemoved(QModelIndex, int , int )));
    QObject::connect(model1, SIGNAL(rowsInserted(QModelIndex, int , int )), model2, SIGNAL(rowsInserted(QModelIndex, int , int )));
    QObject::connect(model1, SIGNAL(rowsMoved(QModelIndex, int , int , QModelIndex, int )), model2, SIGNAL(rowsMoved(QModelIndex, int , int , QModelIndex, int )));
    QObject::connect(model1, SIGNAL(rowsRemoved(QModelIndex, int , int )), model2, SIGNAL(rowsRemoved(QModelIndex, int , int )));
}

QVector<int> removeDuplicates(const QVector<int> &vector)
{
    QList<int> noDuplicates;
    foreach(int i, vector) {
        if (noDuplicates.contains(i))
            continue;
        noDuplicates << i;
    }
    return noDuplicates.toVector();
}

QList<int> removeDuplicates(const QList<int> &list)
{
    QList<int> noDuplicates;
    foreach(int i, list) {
        if (noDuplicates.contains(i))
            continue;
        noDuplicates << i;
    }
    return noDuplicates;
}

} // End Utils
