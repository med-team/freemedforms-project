/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers: Eric MAEKER, <eric.maeker@gmail.com>                 *
 *   Contributors:                                                         *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
/*!
 * \class Utils::DatabaseCsvImportator
 * Import a CSV content into a database (SQLite/MySQL are both supported).
 * \note Unit-test available (see: tests/auto/auto.pro)
 */

#include "databasecsvimportator.h"

#include <utils/global.h>
#include <utils/database.h>
#include <translationutils/constants.h>
#include <translationutils/trans_msgerror.h>
#include <translationutils/trans_filepathxml.h>
#include <translationutils/trans_database.h>

#include <QFileInfo>
#include <QSqlDatabase>
#include <QSqlError>

#include <QDebug>

using namespace Utils;
using namespace Internal;
using namespace Trans::ConstantTranslations;

namespace Utils {
namespace Internal {
class DatabaseCsvImportatorPrivate
{
public:
    DatabaseCsvImportatorPrivate(DatabaseCsvImportator *parent) :
        _currentPos(-1),
        _currentFieldStartPos(-1),
        _currentLineStartPos(-1),
        q(parent)
    {
        Q_UNUSED(q);
    }
    
    ~DatabaseCsvImportatorPrivate()
    {
    }

    bool isCurrentPositionEndOfSqlLine(const ImportationJob &job)
    {
        if (!IN_RANGE_STRICT_MAX(_currentPos, 0, _currentContent.size()))
            return true;

        if (_currentContent.at(_currentPos) != '\n')
            return false;

        if (isCurrentFieldIsEscaped(job)) {
            if (!isPreviousPositionEscapeChar(job))
                return false;
            return (_currentPos-1 != _currentFieldStartPos);
        }
        return true;
    }

    bool isCurrentPositionEndOfFile()
    {
        return (_currentPos >= _currentContent.size());
    }

    bool isCurrenPositionFieldSeparator(const ImportationJob &job)
    {
        if (!IN_RANGE_STRICT_MAX(_currentPos, 0, _currentContent.size()))
            return true;

        if (isCurrentFieldIsEscaped(job)) {
            if (!isPreviousPositionEscapeChar(job))
                return false;
            return (_currentPos-1 != _currentFieldStartPos);
        }

        if (_currentContent.at(_currentPos) == job.fieldSeparator)
            return true;

        if (_currentContent.at(_currentPos) == '\n')
            return true;

        return false;
    }

    bool isCurrentPositionEscapeChar(const ImportationJob &job)
    {
        if (!IN_RANGE_STRICT_MAX(_currentPos, 0, _currentContent.size()))
            return false;
        return (_currentContent.at(_currentPos)==job.textEscapingChar);
    }

    bool isPreviousPositionEscapeChar(const ImportationJob &job)
    {
        if (!IN_RANGE_STRICT_MAX(_currentPos-1, 0, _currentContent.size()))
            return false;
        return (_currentContent.at(_currentPos-1)==job.textEscapingChar);
    }

    bool isCurrentFieldIsEscaped(const ImportationJob &job)
    {
        if (!IN_RANGE_STRICT_MAX(_currentFieldStartPos, 0, _currentContent.size()))
            return false;
        return (_currentContent.at(_currentFieldStartPos)==job.textEscapingChar);
    }

    bool parseContent(const ImportationJob &job, Utils::ImportationResult *result)
    {
        Q_UNUSED(result);
        _currentPos = 0;
        _currentFieldStartPos = 0;
        _currentLineStartPos = 0;

        if (job.omitFirstLine) {
            _currentPos = _currentContent.indexOf("\n") + 1;
        }

        QString sqllines;
        while (!isCurrentPositionEndOfFile()) {
            _currentFieldStartPos = _currentPos;
            _currentLineStartPos = _currentPos;
            QStringList fields;
            while (!isCurrentPositionEndOfSqlLine(job)) {

                while (!isCurrenPositionFieldSeparator(job)) {
                    ++_currentPos;
                }

                if (isCurrentFieldIsEscaped(job)) {
                    int b = _currentFieldStartPos + 1;
                    int e = _currentPos - 1;
                    fields << _currentContent.mid(b, e-b).replace("'", "''");
                } else {
                    fields << _currentContent.mid(_currentFieldStartPos, _currentPos-_currentFieldStartPos).replace("'", "''");
                }

                if (_currentContent.at(_currentPos) == job.fieldSeparator
                        && _currentContent.at(_currentPos+1) == '\n' ) {
                    fields << QString();
                }

                if (!isCurrentPositionEndOfSqlLine(job))
                    ++_currentPos;
                _currentFieldStartPos = _currentPos;
            }

            _sqlCommands << QString("INSERT INTO %1 VALUES\n('%2');\n").arg(job.tableName).arg(fields.join("', '"));

            fields.clear();
            ++_currentPos;
        }
        sqllines.chop(2);
        return true;
    }

    Utils::ImportationResult &import(const ImportationJob &job)
    {
        _currentContent.clear();
        _currentPos = 0;
        _sqlCommands.clear();

        Utils::ImportationResult *result = new Utils::ImportationResult;
        if (!QFileInfo(job.absFilePath).exists()) {
            result->hasError = true;
            result->errors << tkTr(Trans::Constants::FILE_1_DOESNOT_EXISTS).arg(job.absFilePath);
            return *result;
        }

        QSqlDatabase db = QSqlDatabase::database(job.databaseConnectionName);
        if (!db.isOpen()) {
            if (!db.open()) {
                result->hasError = true;
                result->errors << tkTr(Trans::Constants::UNABLE_TO_OPEN_DATABASE_1_ERROR_2)
                                  .arg(db.connectionName())
                                  .arg(db.lastError().text());
                return *result;
            }
        }

        _currentContent = Utils::readTextFile(job.absFilePath, job.fileEncoding, Utils::DontWarnUser);
        if (_currentContent.isEmpty()) {
            result->hasError = true;
            result->errors << tkTr(Trans::Constants::FILE_1_ISEMPTY).arg(job.absFilePath);
            return *result;
        }

        parseContent(job, result);

        if (!Utils::Database::executeSQL(_sqlCommands, db)) {
            result->hasError = true;
            result->errors << "Unable to send data to database";
        }
        return *result;
    }

public:
    QString _currentContent;
    int _currentPos, _currentFieldStartPos, _currentLineStartPos;
    
    QList<Utils::ImportationJob> _jobs;
    QStringList _sqlCommands;

private:
    DatabaseCsvImportator *q;
};
} // namespace Internal
} // end namespace Utils

/*! Constructor of the Utils::DatabaseCsvImportator class */
DatabaseCsvImportator::DatabaseCsvImportator() :
    d(new DatabaseCsvImportatorPrivate(this))
{
}

/*! Destructor of the Utils::DatabaseCsvImportator class */
DatabaseCsvImportator::~DatabaseCsvImportator()
{
    if (d)
        delete d;
    d = 0;
}

/*! Initializes the object with the default values. Return true if initialization was completed. */
bool DatabaseCsvImportator::initialize()
{
    return true;
}

void DatabaseCsvImportator::addJob(const ImportationJob &job)
{
    d->_jobs << job;
}

ImportationResult &DatabaseCsvImportator::startJobs()
{
    ImportationResult *result = new ImportationResult;
    foreach(const ImportationJob &job, d->_jobs) {
        ImportationResult jobresult = d->import(job);
        if (jobresult.hasError) {
            result->hasError = true;
            result->errors << jobresult.errors;
        }
        result->messages << jobresult.messages;
    }
    return *result;
}
