/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef UTILS_QABSTRACTXMLTREEMODEL_H
#define UTILS_QABSTRACTXMLTREEMODEL_H

#include <utils/global_exporter.h>
#include <QAbstractItemModel>
QT_BEGIN_NAMESPACE
class QDomNode;
QT_END_NAMESPACE

/**
 * \file ./libs/utils/qabstractxmltreemodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
namespace Internal {
class QAbstractXmlTreeModelPrivate;
}

class UTILS_EXPORT QAbstractXmlTreeModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit QAbstractXmlTreeModel(const QString &fileName, const QString &mainTag, QObject *parent = 0);
    ~QAbstractXmlTreeModel();

    bool setSubMainTag(const QString &childToMainTag);

    virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
    virtual QModelIndex parent(const QModelIndex &child) const;

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;

//    virtual bool canFetchMore(const QModelIndex &parent = QModelIndex()) const;
//    virtual void fetchMore(const QModelIndex &parent = QModelIndex());

    virtual QDomNode node(const QModelIndex &index) const;

public Q_SLOTS:
    virtual bool saveModel();

private:
    Internal::QAbstractXmlTreeModelPrivate *dqaxtm;
};

}  // End namespace Utils

#endif // UTILS_QABSTRACTXMLTREEMODEL_H
