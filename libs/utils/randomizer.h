/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef RANDOMIZER_H
#define RANDOMIZER_H

#include <utils/global_exporter.h>

#include <QString>
#include <QStringList>
#include <QFileInfo>
#include <QPair>
#include <QDate>

/**
 * \file ./libs/utils/randomizer.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
namespace Internal {
class RandomizerPrivate;
}

class UTILS_EXPORT Randomizer
{
public:
    Randomizer();
    ~Randomizer();

    void setPathToFiles(const QString &path);

    QString randomString(int length) const;

    QString randomName() const;
    QString randomFirstName(bool male) const;

    QPair<int, QString> randomFrenchCity() const;

    int randomInt(int max) const;
    int randomInt(int min, int max) const;
//    qlonglong randomLongLongInt(qlonglong min, qlonglong max) const;
    double randomDouble(double min, double max) const;
    bool randomBool() const;

    QString randomWords(int nbOfWords) const;

    QDate randomDate(const int minYear, const int minMonth = 1, const int minDay = 1) const;
    QDateTime randomDateTime(const QDateTime &minDateTime) const;
    QTime randomTime(const int minHour, const int maxHour) const;

    QFileInfo randomFile(const QDir &inDir, const QStringList &filters) const;

    QString randomVersionNumber() const;

private:
    Internal::RandomizerPrivate *d;
};


} // End namespace Utils

#endif // RANDOMIZER_H
