/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef UTILS_GENERICUPDATEINFORMATION_H
#define UTILS_GENERICUPDATEINFORMATION_H

#include <utils/global_exporter.h>

#include <QDate>
#include <QHash>
#include <QString>
QT_BEGIN_NAMESPACE
class QDomElement;
class QDomDocument;
QT_END_NAMESPACE

/**
 * \file ./libs/utils/genericupdateinformation.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
class VersionNumber;

class UTILS_EXPORT GenericUpdateInformation
{
public:
    GenericUpdateInformation();
    virtual ~GenericUpdateInformation();

    void setFromVersion(const QString &version) {m_From = version;}
    void setToVersion(const QString &version) {m_To = version;}
    void setIsoDate(const QString &date) {m_Date = date;}
    void setDateTime(const QDateTime &dt) {m_Date = dt.toString(Qt::ISODate);}
    void setDate(const QDate &dt) {m_Date = dt.toString(Qt::ISODate);}
    void setAuthor(const QString &a) {m_Author = a;}
    void setText(const QString &text, const QString &lang = QString::null);

    QString fromVersion() const {return m_From;}
    QString toVersion() const {return m_To;}
    QString dateIso() const {return m_Date;}
    QString author() const {return m_Author;}
    QDate date() const {return QDate::fromString(m_Date, Qt::ISODate);}
    QString text(const QString lang = QString::null) const;

//    static bool lessThan(const GenericUpdateInformation *one, const GenericUpdateInformation *two);
    static bool lessThan(const GenericUpdateInformation &one, const GenericUpdateInformation &two);
    static bool greaterThan(const GenericUpdateInformation &one, const GenericUpdateInformation &two);

    static QList<GenericUpdateInformation> updateInformationForVersion(const QList<GenericUpdateInformation> &list, const QString &version);
    static QList<GenericUpdateInformation> updateInformationForVersion(const QList<GenericUpdateInformation> &list, const Utils::VersionNumber &version);

    static QList<GenericUpdateInformation> fromXml(const QDomElement &xml);
    static QString xmlTagName();
//    QString toXml() const;
    bool toDomElement(QDomElement *root, QDomDocument *document) const;

    QString toHtml() const;

    bool operator==(const GenericUpdateInformation &other) const;
    bool operator!=(const GenericUpdateInformation &other) const {return !operator==(other);}

private:
    QString m_From, m_To, m_Date, m_Author;
    QHash<QString, QString> m_TrText;
};

}  // End namespace Utils

UTILS_EXPORT QDebug operator<<(QDebug dbg, const Utils::GenericUpdateInformation &u);
UTILS_EXPORT QDebug operator<<(QDebug dbg, const Utils::GenericUpdateInformation *u);

#endif // UTILS_GENERICUPDATEINFORMATION_H
