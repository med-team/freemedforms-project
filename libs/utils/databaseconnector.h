/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef UTILS_DATABASECONNECTOR_H
#define UTILS_DATABASECONNECTOR_H

#include <utils/global_exporter.h>
#include <utils/database.h>

/**
 * \file ./libs/utils/databaseconnector.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
namespace Internal {
class DatabaseConnectorPrivate;
}

class UTILS_EXPORT DatabaseConnector
{
public:
    enum AccessMode {
        ReadOnly = 0,
        ReadWrite
    };

    DatabaseConnector();
    DatabaseConnector(const QString &clearLog, const QString &clearPass, const QString &hostName, const int port);
    DatabaseConnector(const QString &clearLog, const QString &clearPass);
    ~DatabaseConnector();

    void clear();
    bool isValid() const;

    void setDriver(const ::Utils::Database::AvailableDrivers driver);
    void setClearLog(const QString &log);
    void setClearPass(const QString &pass);
    void setHost(const QString &hostName);
    void setPort(const int port);
    void setAbsPathToReadOnlySqliteDatabase(const QString &absPath);
    void setAbsPathToReadWriteSqliteDatabase(const QString &absPath);
    void setAccessMode(const AccessMode mode);
    void setSqliteUsesExactFile(bool exactFile);
    void setGlobalDatabasePrefix(const QString &prefix);

    Database::AvailableDrivers driver() const;
    QString clearLog() const;
    QString clearPass() const;
    QString cryptedLog() const;
    QString cryptedPass() const;
    QString host() const;
    int port() const;
    QString absPathToSqliteReadOnlyDatabase() const;
    QString absPathToSqliteReadWriteDatabase() const;
    AccessMode accessMode() const;
    bool isDriverValid() const;
    bool useExactFile() const;
    QString globalDatabasePrefix() const;

    QString forSettings() const;
    void fromSettings(const QString &value);

    DatabaseConnector &operator=(const DatabaseConnector &in);
    bool operator==(const DatabaseConnector &other) const;

    QString toString() const;

private:
    Internal::DatabaseConnectorPrivate *d;
};

}  // End namespace Utils

UTILS_EXPORT QDebug operator<<(QDebug dbg, const Utils::DatabaseConnector &c);

#endif // UTILS_DATABASECONNECTOR_H
