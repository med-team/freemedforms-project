/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Christian A. Reiter, <christian.a.reiter@gmail.com> *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "datevalidator.h"

#include <translationutils/constants.h>
#include "translationutils/constanttranslations.h"

#include <QDate>
#include <QDebug>

using namespace Utils;
using namespace Trans::ConstantTranslations;

namespace {
const char * const SEPARATORS = "-./,;: ";
}

DateValidator::DateValidator(QObject *parent) :
    QValidator(parent)
{
    m_dateFormatList << tr("ddMMyy");
    m_dateFormatList << tr("ddMMyyyy");
    m_lastValidFormat = QString();

    addDateFormat(QLocale().dateFormat(QLocale::ShortFormat));

    QRegExp withSep = QRegExp(QString("[%1]*").arg(::SEPARATORS));
    addDateFormat(tkTr(Trans::Constants::DATEFORMAT_FOR_EDITOR).remove(withSep));

    addDateFormat(tkTr(Trans::Constants::DATEFORMAT_FOR_EDITOR));
}

/** \brief Validates the input string with custom date formats
 *
 * The function checks if the input string matches a string
 * in the format list. This list that is set up with FMF
 * defaults, system locale defaults and user defined settings.
 */
QValidator::State DateValidator::validate(QString &input, int &pos) const
{
    Q_UNUSED(pos);

    if (!QRegExp(QString("[0-9%1]*").arg(::SEPARATORS)).exactMatch(input)) {
        return QValidator::Invalid;
    }

    foreach(const QString &format, m_dateFormatList) {
        _currentDate = QDate::fromString(input, format);
        if (_currentDate.isValid()) {

            if (_currentDate.year() < (QDate::currentDate().year() - 80) && !format.contains("yyyy")) {
                _currentDate = _currentDate.addYears(100);
            }
            return QValidator::Acceptable;
        }
    }

    if(QRegExp(QString("[0-9%1]*").arg(::SEPARATORS)).exactMatch(input)) {
        return QValidator::Intermediate;
    }

    return QValidator::Invalid;
}

/*! \brief Sets the internal date
 *
 * The date is internally saved as QDate.
 */
void DateValidator::setDate(const QDate &date)
{
    _currentDate = date;
}

QDate DateValidator::date() const
{
    return _currentDate;
}

/*! \brief Adds a date format string to the internal list */
void DateValidator::addDateFormat(const QString &format)
{
    if (!m_dateFormatList.contains(format, Qt::CaseSensitive))
        m_dateFormatList.append(format);
}

/*! \brief Translates date formats on the fly
 *
 * This function is called e.g. when changing language of the program.
 */
void DateValidator::translateFormats()
{
    m_dateFormatList.takeFirst();
    m_dateFormatList.takeFirst();
    m_dateFormatList.prepend(tr("ddMMyy"));
    m_dateFormatList.prepend(tr("ddMMyyyy"));
}

void DateValidator::fixup(QString &input) const
{
    QRegExp withSep = QRegExp(QString("[%1]*").arg(::SEPARATORS));
    if (input.contains(withSep)) {
        input = input.remove(withSep);
        foreach(const QString &format, m_dateFormatList) {
            _currentDate = QDate::fromString(input, format);
            if (_currentDate.isValid()) {
                break;
            }
        }
    }
}
