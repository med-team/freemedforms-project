/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef UTILS_HTTPDOWNLOADER_PRIVATE_H
#define UTILS_HTTPDOWNLOADER_PRIVATE_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QHash>
#include <QNetworkReply>
#include <QPointer>

QT_BEGIN_NAMESPACE
class QFile;
class QProgressDialog;
class QMainWindow;
class QProgressBar;
QT_END_NAMESPACE

/**
 * \file ./libs/utils/httpdownloader_p.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
class HttpDownloader;
namespace Internal {

//FIXME: private part should be no QObject, but derive from QObjectPrivate
class HttpDownloaderPrivate : public QObject
{
    Q_OBJECT
public:
    explicit HttpDownloaderPrivate(HttpDownloader *parent);
    ~HttpDownloaderPrivate();

    bool startDownload();

public Q_SLOTS:
    bool startRequest(const QUrl &url);
    bool downloadFile();
    void cancelDownload();
    void httpFinished();
    void httpReadyRead();
    void updateDownloadProgress(qint64 bytesRead, qint64 totalBytes);

    void authenticationRequired(QNetworkReply *reply, QAuthenticator *authenticator);
    void proxyAuthenticationRequired(const QNetworkProxy &proxy, QAuthenticator *authenticator);

//    void slotAuthenticationRequired(QNetworkReply*,QAuthenticator *);
//#ifndef QT_NO_OPENSSL
//    void sslErrors(QNetworkReply*,const QList<QSslError> &errors);
//#endif

public:
    QString m_Path, m_OutputFileName, m_LabelText;
    QUrl m_Url;
    QNetworkAccessManager qnam;
    QPointer<QNetworkReply> reply;
    QFile *file;
    QProgressDialog *progressDialog;
    QProgressBar *progressBar;
    int httpGetId;
    bool httpRequestAborted;
    QHash<QString, int> m_AuthTimes;
    QString lastError;
    QNetworkReply::NetworkError networkError;
    bool _useBuffer;
    QByteArray _stringBuffer;

private:
    HttpDownloader *q;
};

}  // namespace Internal
}  // namespace Utils

#endif // UTILS_HTTPDOWNLOADER_PRIVATE_H
