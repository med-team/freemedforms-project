/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#ifndef UTILS_EMAILVALIDATOR_H
#define UTILS_EMAILVALIDATOR_H

#include <utils/global_exporter.h>
#include <QValidator>

QT_BEGIN_NAMESPACE
class QRegExp;
QT_END_NAMESPACE

namespace Utils {

class UTILS_EXPORT EmailValidator : public QValidator
{
    Q_OBJECT
public:
    explicit EmailValidator(QObject *parent = 0);
    State validate(QString &text, int &pos) const;
    void fixup(QString &text) const;

private:
    const QRegExp m_validMailRegExp;
    const QRegExp m_intermediateMailRegExp;
};

}

#endif // EMAILVALIDATOR_H
