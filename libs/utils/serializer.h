/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#ifndef UTILS_SERIALIZER_H
#define UTILS_SERIALIZER_H

#include <utils/global_exporter.h>

#include <QHash>
#include <QString>
#include <QStringList>
#include <QVariant>
QT_BEGIN_NAMESPACE
class QNetworkProxy;
QT_END_NAMESPACE

/**
 * \namespace Utils::Serializer
 * \brief Serializer/deserializer for various Qt objects.
 * \obsolete
*/

namespace Utils {
namespace Serializer {

UTILS_EXPORT const QString separator();
UTILS_EXPORT const QString openParenthese();
UTILS_EXPORT const QString closeParenthese();

// Serialize QStringList
UTILS_EXPORT const QString toString( const QStringList & list, bool base64Protection = false );
UTILS_EXPORT const QStringList toStringList( const QString & serialized, bool base64Protection = false );

// Serialize simples QHash
UTILS_EXPORT const QString toString( const QHash<int,QString> &hash, bool base64Protection = false );
UTILS_EXPORT const QString toString( const QHash<int,QVariant> &hash, bool base64Protection = false );
UTILS_EXPORT const QHash<int,QString> toHash( const QString &serialized, bool base64Protection = false );
UTILS_EXPORT const QHash<int,QVariant> toVariantHash( const QString &serialized, bool base64Protection = false );

// Serialize particular QHash
UTILS_EXPORT const QString threeCharKeyHashToString( const QHash<QString,QString> & hash, bool base64Protection = false );
UTILS_EXPORT const QHash<QString,QString> threeCharKeyHashToHash( const QString & serialized, bool base64Protection = false );

UTILS_EXPORT QString serializeProxy(const QNetworkProxy &proxy);
UTILS_EXPORT bool deserializeProxy(const QString &serializedString, QNetworkProxy &proxy);

}  // End Serializer
}  // End Utils

#endif // UTILS_SERIALIZER_H
