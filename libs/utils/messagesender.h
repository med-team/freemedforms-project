/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef MESSAGESENDER_H
#define MESSAGESENDER_H

#include <utils/global_exporter.h>

#include <QObject>
QT_BEGIN_NAMESPACE
class QNetworkReply;
QT_END_NAMESPACE

/**
 * \file ./libs/utils/messagesender.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
namespace Internal {
class MessageSenderPrivate;
}

class UTILS_EXPORT MessageSender : public QObject
{
    Q_OBJECT
public:
    enum TypeOfMessage {
        CorrectDrugsCoding,
        UncorrectDrugsCoding,
        InformationToDeveloper,
        DosageTransmission
    };

    MessageSender(QObject *parent = 0);
    ~MessageSender();

    // setters
    bool setTypeOfMessage(const TypeOfMessage &t);
    void setParent(QWidget *parent);
    void setUser(const QString &usr);
    void setMessage(const QString &msg);
    void showResultingMessageBox(bool state);

    // getters
    QString resultMessage() const;
    QString usedUrl() const;
    bool isSending() const;

    // starters
    bool postMessage();

Q_SIGNALS:
    /** \brief signal emitted when sending is completed. */
    void sent();

private Q_SLOTS:
    void httpFinished(QNetworkReply*reply);

private:
    Internal::MessageSenderPrivate *d;
};

}  // End Utils

#endif // MESSAGESENDER_H
