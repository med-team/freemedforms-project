/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef UPDATECHECKERPRIVATE_H
#define UPDATECHECKERPRIVATE_H

#include <QObject>
#include <QProgressBar>
#include <QPointer>
#include <QUrl>

namespace Utils {
class HttpDownloader;
namespace Internal {

class UpdateCheckerPrivate : public QObject
{
     Q_OBJECT
public:
    UpdateCheckerPrivate(QObject *parent = 0);
    ~UpdateCheckerPrivate();

    void cancelDownload();
    bool getFile(const QUrl &url);
    void updateFound(const QString &);

private Q_SLOTS:
    void onDownloadFinished();

public:
    HttpDownloader *_downloader;
    QUrl m_Url;
    QString m_UpdateText;
    QString m_LastVersion;
    bool m_FileRetrieved;
    QPointer<QProgressBar> m_ProgressBar;
};

}  // End Internal
}  // End Utils

#endif  // UPDATECHECKERPRIVATE_H
