/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/








#include "hprimparser.h"

#include <utils/log.h>
#include <utils/global.h>
#include <translationutils/constants.h>
#include <translationutils/trans_current.h>
#include <translationutils/trans_patient.h>

#include <QTextStream>
#include <QStringList>

#include <QDebug>

using namespace Trans::ConstantTranslations;

namespace {
const char *const EOF_TAG1 = "****FIN****";
const char *const EOF_TAG2 = "****FINFICHIER****";
const char *const LAB_TAG = "****LAB****";
const char *const ENCODED_LINE_SEPARATOR = "|";
}

using namespace Utils;
using namespace HPRIM;

HprimHeader::HprimHeader()
{}

HprimHeader::~HprimHeader()
{}


bool HprimHeader::isValid() const
{
    return !_data.value(PatientName).isEmpty()
            && !_data.value(PatientFirstName).isEmpty()
            && !_data.value(PatientDateOfBirth).isEmpty();
}

bool HprimHeader::isNull() const
{
    return _data.isEmpty() && _fullContent.isEmpty();
}

bool HprimHeader::setData(const int ref, const QString &value)
{
    if (ref==PatientDateOfBirth) {
        QDate date = QDate::fromString(value, "dd/MM/yyyy");
        if (!date.isValid()) {
            date = QDate::fromString(value, "dd/MM/yy");
            if (!date.isValid())
                return false;
        }
    }
    _data.insert(ref, value);
    return true;
}

QString HprimHeader::data(const int ref) const
{
    return _data.value(ref).trimmed();
}


void HprimHeader::setRawSource(const QString &fullContent)
{
    _fullContent = fullContent;
}


QString HprimHeader::rawSource() const
{
    if (!_fullContent.isEmpty())
        return _fullContent;

    _fullContent += _data.value(PatientId) + "\n";
    _fullContent += _data.value(PatientName) + "\n";
    _fullContent += _data.value(PatientFirstName) + "\n";
    if (!_data.value(PatientAddressFirstLine).isEmpty())
        _fullContent += _data.value(PatientAddressFirstLine).leftJustified(30, ' ') + "\n";
    else
        _fullContent += " \n";
    if (!_data.value(PatientAddressSecondLine).isEmpty())
        _fullContent += _data.value(PatientAddressSecondLine).leftJustified(30, ' ') + "\n";
    else
        _fullContent += " \n";
    _fullContent += _data.value(PatientAddressZipCode) + " " + _data.value(PatientAddressCity) + "\n";
    _fullContent += _data.value(PatientDateOfBirth) + "\n";
    _fullContent += _data.value(PatientSocialNumber) + "\n";
    _fullContent += _data.value(ExtraCode) + "\n";
    _fullContent += _data.value(DateOfExamination) + "\n";
    _fullContent += _data.value(SenderIdentity) + "\n";
    _fullContent += _data.value(ReceiverIdentity) + "\n";
    _fullContent += " \n";

    return _fullContent;
}

HprimRawContent::HprimRawContent()
{}

HprimRawContent::~HprimRawContent()
{}

bool HprimRawContent::isValid() const
{
    if (_fullContent.isEmpty())
        return false;

    int beginFirst = _fullContent.indexOf(::EOF_TAG1);
    if (beginFirst == -1)
        return false;
    beginFirst += QString(::EOF_TAG1).size();

    int beginSecond = _fullContent.indexOf(::EOF_TAG2, beginFirst);
    if (beginSecond == -1)
        return false;

    if (!_fullContent.mid(beginFirst, beginSecond-beginFirst).simplified().isEmpty())
        return false;
    beginSecond += QString(::EOF_TAG2).size();

    return _fullContent.mid(beginSecond).simplified().isEmpty();
}

bool HprimRawContent::isNull() const
{
    return _fullContent.isEmpty();
}

void HprimRawContent::setRawSource(const QString &fullContent)
{
    _fullContent = fullContent;
}

QString HprimRawContent::rawSource() const
{
    return _fullContent;
}


HprimMessage::HprimMessage()
{}

HprimMessage::~HprimMessage()
{}

bool HprimMessage::isValid() const
{
    return _header.isValid() && _rawContent.isValid();
}

bool HprimMessage::isNull() const
{
    return _header.isNull();
}

void HprimMessage::setHeader(const HprimHeader &header)
{
    _header = header;
}

const HprimHeader &HprimMessage::header() const
{
    return _header;
}

void HprimMessage::setRawContent(const HprimRawContent &rawContent)
{
    _rawContent = rawContent;
}

const HprimRawContent &HprimMessage::rawContent() const
{
    return _rawContent;
}

QString HprimMessage::toRawSource() const
{
    return QString("%1%2").arg(_header.rawSource()).arg(_rawContent.rawSource());
}

QString HprimMessage::toBasicHtml() const
{
    int justify = 30;
    QString html;
    html += "<span style=\"font-weight:600; color: darkblue\"><pre>";
    html += QString("%1\n*%2*\n%1\n\n")
            .arg(QString().fill('*', 90))
            .arg(Utils::centerString(tkTr(Trans::Constants::MESSAGE_HEADER), ' ', 88));
    html += QString("%1 %2\n")
            .arg(tkTr(Trans::Constants::_1_COLON_2)
                 .arg(tkTr(Trans::Constants::PATIENT).rightJustified(justify, ' '))
                 .arg(header().patientName()))
            .arg(header().patientFirstName());
    html += tkTr(Trans::Constants::_1_COLON_2)
            .arg(tkTr(Trans::Constants::DATE_OF_BIRTH).rightJustified(justify, ' '))
            .arg(QLocale().toString(header().patientDateOfBirth(), QLocale::LongFormat));
    html += tkTr(Trans::Constants::_1_COLON_2)
            .arg(tkTr(Trans::Constants::SOCIAL_NUMBER).rightJustified(justify, ' '))
            .arg(header().data(Utils::HPRIM::HprimHeader::PatientSocialNumber));
    html += QString("%1 %2 (%3 %4)\n")
            .arg(tkTr(Trans::Constants::_1_COLON_2)
                 .arg(tkTr(Trans::Constants::FULLADDRESS).rightJustified(justify, ' '))
                 .arg(header().data((Utils::HPRIM::HprimHeader::PatientAddressFirstLine))))
            .arg(header().data((Utils::HPRIM::HprimHeader::PatientAddressSecondLine)))
            .arg(header().data((Utils::HPRIM::HprimHeader::PatientAddressZipCode)))
            .arg(header().data((Utils::HPRIM::HprimHeader::PatientAddressCity)));
    html += "\n";
    html += tkTr(Trans::Constants::_1_COLON_2)
            .arg(tkTr(Trans::Constants::FROM).rightJustified(justify, ' '))
            .arg(header().data(Utils::HPRIM::HprimHeader::SenderIdentity));
    html += tkTr(Trans::Constants::_1_COLON_2)
            .arg(tkTr(Trans::Constants::TO).rightJustified(justify, ' '))
            .arg(header().data(Utils::HPRIM::HprimHeader::ReceiverIdentity));
    html += tkTr(Trans::Constants::_1_COLON_2)
            .arg(tkTr(Trans::Constants::DATE).rightJustified(justify, ' '))
            .arg(QLocale().toString(header().dateOfExamination(), QLocale::LongFormat));
    html += "\n";
    html += "</span></pre>\n";
    html += "<pre>";
    html += QString("%1\n*%2*\n%1\n\n")
            .arg(QString().fill('*', 90))
            .arg(Utils::centerString(tkTr(Trans::Constants::MESSAGE_CONTENT), ' ', 88));
    html += rawContent().rawSource().replace("<", "&lt;");
    html += "</pre>";
    return html;
}

Hprim2Content::Hprim2Content() :
    _parseError(false)
{}

Hprim2Content::Hprim2Content(const HprimRawContent &rawContent) :
    _parseError(false)
{
    parseContent(rawContent);
}

Hprim2Content::~Hprim2Content()
{}

bool Hprim2Content::parseContent(const HprimRawContent &rawContent)
{
    _parseError = false;
    QString source = rawContent.rawSource();

    if (!source.contains(::LAB_TAG))
        return !_parseError;

    QTextStream flux(&source, QIODevice::ReadOnly);
    if (!flux.seek(source.indexOf(::LAB_TAG))) {
        LOG_ERROR_FOR("Hprim2Content", "Unable to seek position");
        _parseError = true;
        return !_parseError;
    }

    int lineNb = -1;
    while (!flux.atEnd()) {
        QString line = flux.readLine();

        if (!line.startsWith("RES|"))
            continue;

        QStringList values = line.split(::ENCODED_LINE_SEPARATOR);

        ++lineNb;
        _lines.insert(lineNb, values);
    }

    return !_parseError;
}


bool Hprim2Content::isValid() const
{
    return !isNull() && !_parseError;
}

bool Hprim2Content::isNull() const
{
    return _lines.isEmpty();
}

int Hprim2Content::numberOfLines() const
{
    return _lines.count();
}

QString Hprim2Content::rawData(int lineNumber, int rawDataIndex) const
{
    if (!IN_RANGE_STRICT_MAX(lineNumber, 0, _lines.count()))
        return QString::null;

    const QStringList &rawData = _lines.value(lineNumber);
    if (!IN_RANGE_STRICT_MAX(rawDataIndex, 0, rawData.count()))
        return QString::null;

    return rawData.value(rawDataIndex);
}


namespace Utils {
namespace HPRIM {



HprimMessage &parseHprimRawSource(const QString &fullMessage)
{
    HprimMessage *msg = new HprimMessage;
    HprimHeader hdr;
    HprimRawContent rawContent;

    int line = 0;
    QString full = fullMessage;

    if (full.contains("\r") && !full.contains("\n"))
        full = full.replace("\r", "\n");

    full = Utils::correctTextAccentEncoding(full);

    QTextStream flux(&full, QIODevice::ReadOnly);
    QStringList lines;
    while (!flux.atEnd() && line<12) {
        lines << flux.readLine();
        ++line;
    }
    if (line != 12) {
        msg->setHeader(hdr);
        msg->setRawContent(rawContent);
        return *msg;
    }
    hdr.setRawSource(full.left(flux.pos()));

    int i = -1;
    hdr.setData(HprimHeader::PatientId, lines.at(++i));
    hdr.setData(HprimHeader::PatientName, lines.at(++i));
    hdr.setData(HprimHeader::PatientFirstName, lines.at(++i));
    hdr.setData(HprimHeader::PatientAddressFirstLine, lines.at(++i));
    hdr.setData(HprimHeader::PatientAddressSecondLine, lines.at(++i));
    ++i;
    const QString &zipCity = lines.at(i);
    int begin = zipCity.indexOf(" ");
    hdr.setData(HprimHeader::PatientAddressZipCode, zipCity.left(begin));
    hdr.setData(HprimHeader::PatientAddressCity, zipCity.mid(begin));

    hdr.setData(HprimHeader::PatientDateOfBirth, lines.at(++i));
    hdr.setData(HprimHeader::PatientSocialNumber, lines.at(++i));
    hdr.setData(HprimHeader::ExtraCode, lines.at(++i));
    hdr.setData(HprimHeader::DateOfExamination, lines.at(++i));
    hdr.setData(HprimHeader::SenderIdentity, lines.at(++i));
    hdr.setData(HprimHeader::ReceiverIdentity, lines.at(++i));

    rawContent.setRawSource(full.mid(flux.pos()));

    msg->setHeader(hdr);
    msg->setRawContent(rawContent);

    return *msg;
}


HprimRawContent &createMessageRawContent(const QString &plainTextMessage)
{
    HprimRawContent *content = new HprimRawContent;
    QString corrected = plainTextMessage;

    if (corrected.contains(::EOF_TAG1, Qt::CaseInsensitive)
            || corrected.contains(::EOF_TAG2, Qt::CaseInsensitive)) {
        corrected = corrected.remove(::EOF_TAG1, Qt::CaseInsensitive);
        corrected = corrected.remove(::EOF_TAG2, Qt::CaseInsensitive);
    }

    if (corrected.contains("\r")) {
        corrected = corrected.replace("\r", "<#@cr@#>");
        corrected = corrected.replace("<#@cr@#>\n", "\n");
        corrected = corrected.replace("<#@cr@#>", "\n");
    }

    if (!corrected.endsWith("\n"))
        corrected.append("\n");
    content->setRawSource(QString("%1%2\n%3\n")
                          .arg(corrected)
                          .arg(::EOF_TAG1)
                          .arg(::EOF_TAG2));
    return *content;
}

} // namespace HPRIM
} // namespace Utils


