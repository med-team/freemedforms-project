/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "passwordandlogin.h"

#include <utils/log.h>

#include <QCryptographicHash>
#include <QCoreApplication>

#include <QDebug>

enum {
    DebugCheckPassword = false
};



namespace Utils {

QString loginForSQL(const QString &log)
{
    return log.toUtf8().toBase64();
}

QString loginFromSQL(const QVariant &sql)
{
    return QByteArray::fromBase64(sql.toByteArray());
}

QString loginFromSQL(const QString &sql)
{
    return QByteArray::fromBase64(sql.toUtf8());
}

PasswordCrypter::PasswordCrypter()
{}

PasswordCrypter::~PasswordCrypter()
{}


QString PasswordCrypter::cryptPassword(const QString &toCrypt, PasswordCrypter::Algorithm algo)
{
    if (algo == ERROR)
        return QString::null;
    QCryptographicHash::Algorithm qch_algo = QCryptographicHash::Sha1;
    QString prefix;
    switch (algo) {
    case SHA1:
        break;
#if (QT_VERSION >= 0x050000)
    case SHA256:
        qch_algo = QCryptographicHash::Sha256;
        prefix = "sha256";
        break;
    case SHA512:
        qch_algo = QCryptographicHash::Sha512;
        prefix = "sha512";
        break;
#endif
#if (QT_VERSION >= 0x050100)
    case SHA3_256:
        qch_algo = QCryptographicHash::Sha3_256;
        prefix = "sha3-256";
        break;
    case SHA3_512:
        qch_algo = QCryptographicHash::Sha3_512;
        prefix = "sha3-512";
        break;
#endif
    default: return QString::null;
    }
    QByteArray crypt = QCryptographicHash::hash(toCrypt.toUtf8(), qch_algo);
    if (!prefix.isEmpty()) {
        QString r = QString("%1:%2")
            .arg(prefix)
            .arg(QString(crypt.toBase64()));
        return r;
    }
    QString r = QString(crypt.toBase64());
    return r;
}


PasswordCrypter::Algorithm PasswordCrypter::extractHashAlgorithm(const QString &cryptedBase64)
{
    if (!cryptedBase64.contains(":"))
        return SHA1;
    QString prefix = cryptedBase64.left(cryptedBase64.indexOf(":"));
    if (prefix == "sha1")
        return SHA1;
#if (QT_VERSION >= 0x050000)
    else if (prefix == "sha256")
        return SHA256;
    else if (prefix == "sha512")
        return SHA512;
#endif
#if (QT_VERSION >= 0x050100)
    else if (prefix == "sha3-256")
        return SHA3_256;
    else if (prefix == "sha3-512")
        return SHA3_512;
#endif
    return ERROR;
}


bool PasswordCrypter::checkPrefix(const QString &cryptedBase64, Algorithm algo)
{
    if (algo == ERROR)
        return false;
    return (extractHashAlgorithm(cryptedBase64) == algo);
}

/** Checks equality between a clear password \e clear and
 * a encrypted password (in base64 encoding) \e cryptedBase64.
 * The encrypted password must have been created using the
 * cryptPassword().
 */
bool PasswordCrypter::checkPassword(const QString &clear, const QString &cryptedBase64)
{
    if (DebugCheckPassword)
        WARN_FUNC;
    Algorithm algo = SHA1;
    if (cryptedBase64.contains(":")) {
        algo = extractHashAlgorithm(cryptedBase64);
    }
    QString crypted = cryptPassword(clear, algo);
    if (DebugCheckPassword) {
        qDebug() << QString("clear: %1; crypted: %2\nhash: %3; comparison: %4")
                    .arg(clear).arg(cryptedBase64).arg(algo).arg(crypted);
    }
    return (crypted.compare(cryptedBase64) == 0);
}


QString cryptPassword(const QString &toCrypt)
{
    QCryptographicHash crypter(QCryptographicHash::Sha1);
    crypter.addData(toCrypt.toUtf8());
    return crypter.result().toBase64();
}


QByteArray nonDestructiveEncryption(const QString &text, const QString &key)
{
    QByteArray texteEnBytes = text.toUtf8();
    QString k = key;
    if (key.isEmpty())
        k = QCryptographicHash::hash(qApp->applicationName().left(qApp->applicationName().indexOf("_d")).toUtf8(), QCryptographicHash::Sha1);
    QByteArray cle = k.toUtf8().toBase64();
    QByteArray codeFinal;
    int tailleCle = cle.length();
    for (int i = 0; i < texteEnBytes.length(); ++i) {
        codeFinal += char(texteEnBytes[i] ^ cle[i % tailleCle]);
    }
    return codeFinal.toHex().toBase64();
}



QString decrypt(const QByteArray &texte, const QString &key)
{
    QByteArray texteEnBytes = QByteArray::fromHex(QByteArray::fromBase64(texte));
    QString k = key;
    if (key.isEmpty())
        k = QCryptographicHash::hash(qApp->applicationName().left(qApp->applicationName().indexOf("_d")).toUtf8(), QCryptographicHash::Sha1);
    QByteArray cle = k.toUtf8().toBase64();
    QByteArray codeFinal;
    int tailleCle = cle.length();
    for (int i = 0; i < texteEnBytes.length(); ++i) {
        codeFinal += char(texteEnBytes[i] ^ cle[i % tailleCle]);
    }
    return codeFinal;
}

} // namespace Utils
