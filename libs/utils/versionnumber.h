/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef VERSIONNUMBER_H
#define VERSIONNUMBER_H

#include <utils/global_exporter.h>

#include <QString>

/**
 * \file ./libs/utils/versionnumber.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {

class UTILS_EXPORT VersionNumber
{
public:
    VersionNumber();
    VersionNumber(const QString &versionNumber);

    QString versionString() const {return m_Version;}

    int majorNumber() const {return m_Major;}
    int minorNumber() const {return m_Minor;}
    int debugNumber() const {return m_Debug;}

    bool isAlpha() const {return m_IsAlpha;}
    int alphaNumber() const {return m_Alpha;}

    bool isBeta() const {return m_IsBeta;}
    int betaNumber() const {return m_Beta;}

    bool isRC() const {return m_IsRC;}
    int rcNumber() const {return m_RC;}

    bool operator>(const VersionNumber &b) const;
    bool operator<(const VersionNumber &b) const;

    bool operator>=(const VersionNumber &b) const;
    bool operator<=(const VersionNumber &b) const;

    bool operator==(const VersionNumber &b) const;
    bool operator!=(const VersionNumber &b) const;

private:
    QString m_Version;
    int m_Major, m_Minor, m_Debug, m_Alpha, m_Beta, m_RC;
    bool m_IsAlpha, m_IsBeta, m_IsRC;
};

}  // End namespace Utils

UTILS_EXPORT QDebug operator<<(QDebug dbg, const Utils::VersionNumber &c);
UTILS_EXPORT QDebug operator<<(QDebug dbg, const Utils::VersionNumber *c);


#endif // VERSIONNUMBER_H
