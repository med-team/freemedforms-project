/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef UTILS_NUMBERTOSTRING_H
#define UTILS_NUMBERTOSTRING_H

#include <QString>
#include <utils/global_exporter.h>

/**
 * \file ./libs/utils/numbertostring.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils
{
UTILS_EXPORT QString integerToHumanReadableString(int n);
UTILS_EXPORT QStringList doubleToHumanReadableString(int integer, int decimal);
UTILS_EXPORT QStringList doubleToHumanReadableString(double decimal);
}

#endif // UTILS_NUMBERTOSTRING_H
