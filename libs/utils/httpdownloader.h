/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef UTILS_HTTPDOWNLOADER_H
#define UTILS_HTTPDOWNLOADER_H

#include <utils/global_exporter.h>
#include <QObject>
#include <QNetworkReply>

QT_BEGIN_NAMESPACE
class QProgressBar;
class QUrl;
class QMainWindow;
QT_END_NAMESPACE

/**
 * \file ./libs/utils/httpdownloader.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
namespace Internal {
class HttpDownloaderPrivate;
}  // namespace Internal

class UTILS_EXPORT HttpDownloader : public QObject
{
    Q_OBJECT
    friend class Utils::Internal::HttpDownloaderPrivate;

public:
    explicit HttpDownloader(QObject *parent = 0);
    ~HttpDownloader();

    void setMainWindow(QMainWindow *win);

    void setUrl(const QUrl &url);
    const QUrl &url() const;

    void setStoreInBuffer(bool store);
    QByteArray getBufferContent() const;

    void setOutputPath(const QString &absolutePath);
    void setOutputFileName(const QString &fileName);
    QString outputFileName() const;
    QString outputAbsoluteFileName() const;

    void setLabelText(const QString &text);

    QString lastErrorString() const;
    QNetworkReply::NetworkError networkError() const;

public Q_SLOTS:
    bool startDownload();
    bool cancelDownload();

Q_SIGNALS:
    void downloadFinished();
    void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);
    void downloadProgressPermille(int);

private:
    Internal::HttpDownloaderPrivate *d;
};

}  // namespace Utils

#endif // UTILS_HTTPDOWNLOADER_H
