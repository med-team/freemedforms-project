/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker                                         *
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef UTILS_DATABASECSVIMPORTATOR_H
#define UTILS_DATABASECSVIMPORTATOR_H

#include <utils/global_exporter.h>
#include <QString>
#include <QStringList>

/**
 * \file ./libs/utils/databasecsvimportator.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
namespace Internal {
class DatabaseCsvImportatorPrivate;
} // namespace Internal

struct UTILS_EXPORT ImportationJob
{
    ImportationJob() :
        fileEncoding("UTF-8"),
        fieldSeparator(','),
        textEscapingChar('"'),
        omitFirstLine(false)
    {}

    QString absFilePath;
    QString fileEncoding;

    QString databaseConnectionName;
    QString tableName;
    QChar fieldSeparator;
    QChar textEscapingChar;
    bool omitFirstLine;
};

struct UTILS_EXPORT ImportationResult
{
    ImportationResult() : hasError(false) {}

    bool hasError;
    QStringList errors;
    QStringList messages;
};

class UTILS_EXPORT DatabaseCsvImportator
{    
public:
    explicit DatabaseCsvImportator();
    ~DatabaseCsvImportator();
    bool initialize();

    void addJob(const ImportationJob &job);

    ImportationResult &startJobs();
    
private:
    Internal::DatabaseCsvImportatorPrivate *d;
};

} // namespace Utils

#endif  // UTILS_DATABASECSVIMPORTATOR_H

