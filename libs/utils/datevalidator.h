/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Christian A. Reiter, <christian.a.reiter@gmail.com> *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATEVALIDATOR_H
#define DATEVALIDATOR_H

#include "utils/global_exporter.h"
#include <QValidator>
#include <QStringList>
#include <QDate>

/**
 * \file ./libs/utils/datevalidator.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {

class UTILS_EXPORT DateValidator : public QValidator
{
    Q_OBJECT
public:
    explicit DateValidator(QObject *parent = 0);
    State validate(QString &input, int &pos) const;
    void fixup(QString &input) const;

    void setDate(const QDate &date);
    QDate date() const;

    void addDateFormat(const QString &format);
    QStringList acceptedDateFormats() const {return m_dateFormatList;}

    void translateFormats();

private:
    QStringList m_dateFormatList;
    QString m_lastValidFormat;
    mutable QDate _currentDate;
};
} // end Utils
#endif // DATEVALIDATOR_H
