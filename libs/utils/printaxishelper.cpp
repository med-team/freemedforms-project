/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "printaxishelper.h"

#include <QDebug>

namespace Utils {

PrintAxisHelper::PrintAxisHelper() :
    _pixToMmCoefX(0.),
    _pixToMmCoefY(0.),
    _left(0), _right(0), _top(0), _bottom(0),
    _transXPixels(0.),
    _transYPixels(0.)
{
}

void PrintAxisHelper::setPaperSize(const QRect &pageRectPixels, const QSizeF &pageSizeInMillimeters, const QPrinter::Unit = QPrinter::Millimeter)
{
    _pixToMmCoefX = (qreal)pageRectPixels.width() / pageSizeInMillimeters.width();
    _pixToMmCoefY = (qreal)pageRectPixels.height() / pageSizeInMillimeters.height();
    _pageRect = pageRectPixels;
}

void PrintAxisHelper::setMargins(qreal left, qreal top, qreal right, qreal bottom)
{
    _left = left;
    _right = right;
    _top = top;
    _bottom = bottom;
}


QPointF PrintAxisHelper::pointToPixels(const QPointF &pointInMilliters)
{
    return pointToPixels(pointInMilliters.x(), pointInMilliters.y());
}


QPointF PrintAxisHelper::pointToPixels(double x_millimeter, double y_millimeter)
{
    return QPointF(x_millimeter * _pixToMmCoefX + _transXPixels, y_millimeter * _pixToMmCoefY + _transYPixels);
}


QSizeF PrintAxisHelper::sizeToPixels(const QSizeF &sizeMilliters)
{
    return sizeToPixels(sizeMilliters.width(), sizeMilliters.height());
}


QSizeF PrintAxisHelper::sizeToPixels(double width_millimeter, double height_millimeter)
{
    return QSize(width_millimeter * _pixToMmCoefX, height_millimeter * _pixToMmCoefY);
}

void PrintAxisHelper::translatePixels(int x, int y)
{
    _transXPixels = x;
    _transYPixels = y;
}

void PrintAxisHelper::translateMillimeters(double x, double y)
{
    _transXPixels = x * _pixToMmCoefX;
    _transYPixels = y * _pixToMmCoefY;
}

void PrintAxisHelper::printString(QPainter *painter, const PrintString &printString)
{
    painter->save();

    QRectF content(pointToPixels(printString.topMillimeters),  sizeToPixels(printString.contentSizeMillimeters));

    if (_left != 0.
            || _top != 0.
            || _right != 0.
            || _bottom != 0.) {
        if (content.left() < _left)
            content.setLeft(_left);

        if (content.right() > (_pageRect.right() - _right))
            content.setRight(_pageRect.right() - _right);

        if (content.top() < _top)
            content.setTop(_top);

        if (content.bottom() > (_pageRect.bottom() - _bottom))
            content.setBottom(_pageRect.bottom() - _bottom);
    }


    if (printString.drawBoundingRect)
        painter->drawRect(content);

    if (printString.content.isEmpty()) {
        painter->restore();
        return;
    }

    QFont font = painter->font();
    QFontMetrics metrics(font);
    if (printString.autoFontResize) {
        int heightPixels = content.size().height() * 0.9; // printString.contentSizeMillimeters.height() * _pixToMmCoefY * 0.85;
        double heightScaleFactor = ((double)metrics.height()/(double)heightPixels);

        if (!printString.splitChars) {
            int widthPixels = content.size().width(); //printString.contentSizeMillimeters.width() * _pixToMmCoefX;
            double widthScaleFactor = ((double)metrics.width(printString.content)/(double)widthPixels);

            double scaleFactor = qMax(heightScaleFactor, widthScaleFactor);

            font.setPointSizeF(((double)font.pointSize() / scaleFactor));

            metrics = QFontMetrics(font);
            painter->setFont(font);
            painter->drawText(content, printString.alignment, printString.content);
        } else {
            int widthPixels = content.size().width();
            double widthScaleFactorPerChar = ((double)metrics.width(printString.content)/(double)widthPixels) / (double)(printString.content.size());

            for(int i=0; i < printString.content.size(); ++i) {
                widthScaleFactorPerChar = qMax(widthScaleFactorPerChar, ((double)metrics.width(printString.content, i)/(double)widthPixels));
            }

            double scaleFactor = qMax(heightScaleFactor, widthScaleFactorPerChar);

            font.setPointSizeF(((double)font.pointSize() / scaleFactor));

            metrics = QFontMetrics(font);
            painter->setFont(font);

            double pixelPerChar = (double)widthPixels / (double)(printString.content.size());

            for(int i=0; i < printString.content.size(); ++i) {
                QRectF charRect = QRectF(content.topLeft() + QPointF(i*pixelPerChar, 0.1), QSizeF(pixelPerChar, content.height()));
                painter->drawText(charRect, Qt::AlignCenter, printString.content.at(i));
            }
        }
    } else {
        painter->drawText(content, printString.alignment, printString.content);
    }



    painter->restore();
}

} // namespace Utils
