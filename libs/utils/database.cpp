/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       Jerome PINGUET <jerome@jerome.cc>                                 *
 ***************************************************************************/


#include "database.h"
#include "log.h"
#include "global.h"
#include "databaseconnector.h"

#include <translationutils/constants.h>
#include <translationutils/trans_database.h>
#include <translationutils/trans_filepathxml.h>

#include <QDir>
#include <QSqlRecord>
#include <QSqlField>
#include <QCoreApplication>
#include <QHash>
#include <QMultiHash>
#include <QMap>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QProgressDialog>
#include <QUuid>

#include <QDebug>

enum { WarnSqlCommands = false, WarnCreateTableSqlCommand = false, WarnLogMessages = false };

using namespace Utils;
using namespace Utils::Internal;
using namespace Trans::ConstantTranslations;

static inline bool connectedDatabase(QSqlDatabase &db, int line)
{
    if (!db.isOpen()) {
        if (!db.open()) {
            Utils::Log::addError("Utils::Database", tkTr(Trans::Constants::UNABLE_TO_OPEN_DATABASE_1_ERROR_2).arg(db.connectionName()).arg(db.lastError().text()), __FILE__, line);
            return false;
        }
    }
    return true;
}

namespace Utils {
namespace Internal {

struct DbIndex {
    Utils::Field field;
    QString name;
};

static QString mySqlHost()
{
    return QString("%");
}

class DatabasePrivate
{
public:
    enum ExtraCommand {
        NoExtra = 0,
        Count,
        Distinct,
        CountDistinct,
        Max,
        Total,
        Min
    };

    DatabasePrivate(Database *parent) :
        m_initialized(false),
        _transaction(false),
        m_LastCorrectLogin(-1),
        m_Driver(Database::SQLite),
        q(parent)
    {
    }
    ~DatabasePrivate() {}

    QStringList getSQLCreateTable(const int & tableref);
    QString getTypeOfField(const int & fieldref) const;

    int index(const int tableRef, const int fieldRef)
    {
        return fieldRef + (tableRef * 1000);
    }

    int fieldFromIndex(const int index)
    {
        return index % 1000;
    }

    int tableFromIndex(const int index)
    {
        return index / 1000;
    }

    static Database::Grants getGrants(const QString &connection, const QStringList &grants)
    {
        Q_UNUSED(connection);
        QHash<QString, int> ref;
        ref.insert("ALL PRIVILEGES", Database::Grant_All);
        ref.insert("ALTER", Database::Grant_Alter);
        ref.insert("ALTER ROUTINE", Database::Grant_AlterRoutine);
        ref.insert("CREATE", Database::Grant_Create);
        ref.insert("CREATE ROUTINE", Database::Grant_CreateRoutine);
        ref.insert("CREATE TEMPORARY TABLES", Database::Grant_CreateTmpTables);
        ref.insert("CREATE USER", Database::Grant_CreateUser);
        ref.insert("CREATE VIEW", Database::Grant_CreateView);
        ref.insert("DELETE", Database::Grant_Delete);
        ref.insert("DROP", Database::Grant_Drop);
        ref.insert("EXECUTE", Database::Grant_Execute);
        ref.insert("GRANT OPTION", Database::Grant_Options);
        ref.insert("INDEX", Database::Grant_Index);
        ref.insert("INSERT", Database::Grant_Insert);
        ref.insert("LOCK TABLES", Database::Grant_LockTables);
        ref.insert("PROCESS", Database::Grant_Process);
        ref.insert("SELECT", Database::Grant_Select);
        ref.insert("SHOW DATABASES", Database::Grant_ShowDatabases);
        ref.insert("SHOW VIEW", Database::Grant_ShowView);
        ref.insert("TRIGGER", Database::Grant_Trigger);
        ref.insert("UPDATE", Database::Grant_Update);
        ref.insert("RELOAD", Database::Grant_Reload);

        Database::Grants g = 0;
        foreach(const QString &s, grants) {
            foreach(const QString &k, ref.keys()) {
                if (s.contains(k + ",") || s.contains(k + " ON")) {
                    g |= QFlags<Database::Grant>(ref.value(k));
                }
            }
        }
        return g;
    }

    QString getSelectCommand(const FieldList &select, const JoinList &joins, const FieldList &conditions, ExtraCommand command = NoExtra) const
    {
        FieldList get, cond;
        JoinList jns;
        QString fields, from;
        QStringList tables;
        for(int i=0; i < select.count(); ++i) {
            get << select.at(i);
            if (get.at(i).tableName.isEmpty() || get.at(i).fieldName.isEmpty()) {
                get[i].tableName = q->table(select.at(i).table);
                get[i].fieldName = q->fieldName(select.at(i).table, select.at(i).field);
            }
        }
        for(int i=0; i < conditions.count(); ++i) {
            cond << conditions.at(i);
            if (cond.at(i).tableName.isEmpty() || cond.at(i).fieldName.isEmpty()) {
                cond[i].tableName = q->table(cond.at(i).table);
                cond[i].fieldName = q->fieldName(cond.at(i).table, cond.at(i).field);
            }
        }
        for(int i=0; i < joins.count(); ++i) {
            Field f1 = q->field(joins.at(i).field1.table, joins.at(i).field1.field);
            Field f2 = q->field(joins.at(i).field2.table, joins.at(i).field2.field);
            jns << Join(f1, f2);
        }

        for(int i=0; i < get.count(); ++i) {
            fields += QString("`%1`.`%2`, ").arg(get.at(i).tableName).arg(get.at(i).fieldName);
            tables << get.at(i).tableName;
        }
        tables.removeDuplicates();

        if (fields.isEmpty())
            return QString();
        fields.chop(2);

        QString w;
        if (cond.count() > 0) {
            w = "\nWHERE " + q->getWhereClause(cond);
            for(int i=0; i < cond.count(); ++i) {
                tables << cond.at(i).tableName;
            }
            tables.removeDuplicates();
        }

        QString j;
        for(int i=0; i < jns.count(); ++i) {
            j += q->joinToSql(jns.at(i)) + "\n";
            tables.removeAll(jns.at(i).field1.tableName);
        }
        tables.removeDuplicates();

        foreach(const QString &tab, tables) {
            from += QString("`%1`, ").arg(tab);
        }
        from.chop(2);

        switch (command) {
        case Count: fields = QString("count(%1)").arg(fields); break;
        case CountDistinct: fields = QString("count(distinct %1)").arg(fields); break;
        case Distinct: fields = QString("distinct(%1)").arg(fields); break;
        case Max: fields = QString("max(%1)").arg(fields); break;
        case Min: fields = QString("min(%1)").arg(fields); break;
        case Total: fields = QString("total(%1)").arg(fields); break;
        default: break;
        }

        return QString("SELECT %1 FROM %2\n%3%4").arg(fields, from, j, w);
    }

public:
    QHash<int, QString>        m_Tables;         // tables are not sorted
    QMultiHash<int, int>       m_Tables_Fields;  // links are not sorted
    QMap<int, QString>         m_Fields;         // fields should be sorted from first to last one using ref
    QHash<int, int>            m_TypeOfField;
    QHash<int, QString>        m_DefaultFieldValue;
    bool                       m_initialized, _transaction;
    int                        m_LastCorrectLogin;
    QString                    m_ConnectionName;
    QHash<QString, Database::Grants> m_Grants;
    Database::AvailableDrivers m_Driver;
    QMultiHash<int,int> m_PrimKeys; // K=table, V=field
    QVector<DbIndex> m_DbIndexes;

private:
    Database *q;
};
} // namespace Internal
} // namespace Utils

QString Utils::Database::_prefix;

void Database::logAvailableDrivers()
{
    QString tmp;
    foreach(const QString &drv, QSqlDatabase::drivers()) {
        if (QSqlDatabase::isDriverAvailable(drv)) {
            tmp += drv + " ; ";
        }
    }
    tmp.chop(3);
    LOG_FOR("Database", QString("Available drivers: %1").arg(QSqlDatabase::drivers().join(" ; ")));
}

Database::Database() :
    d_database(new DatabasePrivate(this))
{
}

Database::~Database()
{
    if (d_database)
        delete d_database;
    d_database=0;
}


QString Database::prefixedDatabaseName(AvailableDrivers driver, const QString &dbName)
{
    QString toReturn = dbName;
    if (!_prefix.isEmpty()) {
        if (!toReturn.startsWith(_prefix)) {
            if ((driver==MySQL || driver==PostSQL)
                    && !toReturn.startsWith("fmf_"))
                toReturn.prepend("fmf_");
            toReturn.prepend(_prefix);
        }
    } else {
        if ((driver==MySQL || driver==PostSQL)
                && !toReturn.startsWith("fmf_"))
            toReturn.prepend("fmf_");
    }
    return toReturn;
}


bool Database::createMySQLDatabase(const QString &dbName)
{
    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return false;

    LOG_FOR("Database", QString("Trying to create database: %1\n"
                                "       on host: %2(%3)\n"
                                "       with user: %4")
            .arg(dbName).arg(database().hostName()).arg(database().port()).arg(database().userName()));

    DB.transaction();
    QSqlQuery query(DB);
    if (!query.exec(QString("CREATE DATABASE `%1`;").arg(dbName))) {
        LOG_QUERY_ERROR_FOR("Database", query);
        DB.rollback();
        return false;
    }
    LOG_FOR("Database", tkTr(Trans::Constants::DATABASE_1_CORRECTLY_CREATED).arg(dbName));
    query.finish();
    DB.commit();
    return true;
}


bool Database::createMySQLUser(const QString &log,
                               const QString &password,
                               const Grants grants,
                               const QString &databasePrefix)
{
    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return false;

    QString g;
        if (grants & Grant_Select) {
            g += "SELECT, ";
        }
        if (grants & Grant_Update) {
            g += "UPDATE, ";
        }
        if (grants & Grant_Insert) {
            g += "INSERT, ";
        }
        if (grants & Grant_Delete) {
            g += "DELETE, ";
        }
        if (grants & Grant_Create) {
            g += "CREATE, ";
        }
        if (grants & Grant_Drop) {
            g += "DROP, ";
        }
        if (grants & Grant_Alter) {
            g += "ALTER, ";
        }
        g.chop(2);
    
    if (g.isEmpty()) {
        LOG_ERROR_FOR("Database","No grants when creating user");
        return false;
    }

    QString prefix = databasePrefix + "fmf_%";
    prefix = prefix.replace("_", "\\_");
    LOG_FOR("Database", QString("Trying to create MySQL user: %1\n"
                                "       on host: %2(%3)\n"
                                "       with user: %4\n"
                                "       db prefix: %5")
            .arg(log)
            .arg(database().hostName())
            .arg(database().port())
            .arg(database().userName())
            .arg(prefix));

    DB.transaction();
    QSqlQuery query(DB);
    QString req = QString("CREATE USER '%1'@'%2' IDENTIFIED BY '%3';")
            .arg(log).arg(mySqlHost()).arg(password);
    if (!query.exec(req)) {
        LOG_QUERY_ERROR_FOR("Database", query);
        LOG_DATABASE_FOR("Database", database());
        DB.rollback();
        return false;
    }
    query.finish();

    req = QString("GRANT %1 ON `%2`.* TO '%3'@'%4';")
            .arg(g).arg(prefix).arg(log).arg(mySqlHost());
    if (!query.exec(req)) {
        LOG_QUERY_ERROR_FOR("Database", query);
        LOG_DATABASE_FOR("Database", database());
        query.finish();
        DB.rollback();
        req = QString("DROP USER '%1'@'%2'").arg(log).arg(mySqlHost());
        if (!query.exec(req)) {
            LOG_QUERY_ERROR_FOR("Database", query);
            LOG_DATABASE_FOR("Database", database());
        } else {
            LOG_ERROR_FOR("Database", QString("User %1 removed").arg(log));
        }
        return false;
    }
    query.finish();
    
    if (grants & Grant_CreateUser) {
        req = QString("GRANT CREATE USER ON *.* TO '%1'@'%2' WITH GRANT OPTION;")
                .arg(log).arg(mySqlHost());
        if (!query.exec(req)) {
            LOG_QUERY_ERROR_FOR("Database", query);
            LOG_DATABASE_FOR("Database", database());
            query.finish();
            DB.rollback();
            req = QString("DROP USER '%1'@'%2'").arg(log).arg(mySqlHost());
            if (!query.exec(req)) {
                LOG_QUERY_ERROR_FOR("Database", query);
                LOG_DATABASE_FOR("Database", database());
            } else {
                LOG_ERROR_FOR("Database", QString("User %1 removed").arg(log));
            }
            return false;
        }
        query.finish();
    }

    DB.commit();
    LOG_FOR("Database", tkTr(Trans::Constants::DATABASE_USER_1_CORRECTLY_CREATED).arg(log));
    return true;
}


bool Database::dropMySQLUser(const QString &log, const QString &userHost)
{
    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return false;

    LOG_FOR("Database", QString("Trying to drop MySQL user: %1\n"
                                "       on host: %2(%3)\n"
                                "       with user: %4")
            .arg(log).arg(database().hostName()).arg(database().port()).arg(database().userName()));

    QString req;
    if (userHost.isEmpty()) {
        req = QString("DROP USER '%1';").arg(log);
    } else {
        req = QString("DROP USER '%1'@'%2';").arg(log).arg(userHost);
    }
    DB.transaction();
    QSqlQuery query(DB);
    if (!query.exec(req)) {
        LOG_QUERY_ERROR_FOR("Database", query);
        LOG_DATABASE_FOR("Database", database());
        DB.rollback();
        return false;
    } else {
        LOG_FOR("Database", QString("User %1 removed").arg(log));
    }
    DB.commit();
    return true;
}


bool Database::changeMySQLUserOwnPassword(const QString &login, const QString &newPassword)
{
    if (login.isEmpty())
        return false;

    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return false;

    LOG_FOR("Database", QString("Trying to change MySQL OWN user password:\n"
                                ".user: %1\n"
                                "@%2(%3)\n"
                                ".pass: %4")
            .arg(login)
            .arg(database().hostName())
            .arg(database().port())
            .arg(QString().fill('*', newPassword.size())));

    DB.transaction();
    QSqlQuery query(DB);
    QString req;
    req = QString("SET PASSWORD = PASSWORD('%1');")
                 .arg(newPassword);
    
    if (!query.exec(req)) {
        LOG_QUERY_ERROR_FOR("Database", query);
        LOG_DATABASE_FOR("Database", database());
        DB.rollback();
        return false;
    }
    
    query.finish();
    
    DB.commit();
    LOG_FOR("Database", QString("User %1 password modified").arg(login));
    return true;
}


bool Database::changeMySQLOtherUserPassword(const QString &login, const QString &newPassword)
{
    if (login.isEmpty())
        return false;

    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return false;

    LOG_FOR("Database", QString("Trying to change MySQL OTHER user password:\n"
                                "       user: %1\n"
                                "       host: %2(%3)\n"
                                "       new password: %4")
            .arg(login)
            .arg(database().hostName())
            .arg(database().port())
            .arg(newPassword));

    DB.transaction();
    QSqlQuery query(DB);
    QString req;
    QStringList hostNames = mySQLUserHostNames(login);

    foreach(const QString &hostName, hostNames ) {
        req = QString("SET PASSWORD FOR '%1'@'%2' = PASSWORD('%3');")
                .arg(login).arg(hostName).arg(newPassword);
        if (!query.exec(req)) {
            LOG_QUERY_ERROR_FOR("Database", query);
            LOG_DATABASE_FOR("Database", database());
            DB.rollback();
            return false;
        }
        query.finish();
    }
    if (!query.exec("FLUSH PRIVILEGES;")) {
        LOG_QUERY_ERROR_FOR("Database", query);
        LOG_DATABASE_FOR("Database", database());
        DB.rollback();
        return false;
    }
    DB.commit();
    LOG_FOR("Database", QString("User %1 password modified").arg(login));
    return true;
}

QSqlDatabase Database::database() const
{
    return QSqlDatabase::database(d_database->m_ConnectionName);
}


QString Database::sqliteFileName(const QString &connectionName,
                                 const QString &nonPrefixedDbName,
                                 const Utils::DatabaseConnector &connector,
                                 bool addGlobalPrefix)
{
    QString fileName;
    if (connector.accessMode()==DatabaseConnector::ReadOnly) {
        if (connector.useExactFile())
            fileName = QString("%1/%2")
                    .arg(connector.absPathToSqliteReadOnlyDatabase())
                    .arg(nonPrefixedDbName);
        else
            fileName = QString("%1/%2/%3")
                    .arg(connector.absPathToSqliteReadOnlyDatabase())
                    .arg(connectionName)
                    .arg(nonPrefixedDbName);
    } else if (connector.accessMode()==DatabaseConnector::ReadWrite) {
        if (connector.useExactFile()) {
            fileName = QString("%1/%2")
                    .arg(connector.absPathToSqliteReadWriteDatabase())
                    .arg(nonPrefixedDbName);
        } else {
            if (addGlobalPrefix) {
                fileName = QString("%1/%2/%3")
                        .arg(connector.absPathToSqliteReadWriteDatabase())
                        .arg(connectionName)
                        .arg(prefixedDatabaseName(connector.driver(), nonPrefixedDbName));
            } else {
                fileName = QString("%1/%2/%3")
                        .arg(connector.absPathToSqliteReadWriteDatabase())
                        .arg(connectionName)
                        .arg(nonPrefixedDbName);
            }
        }
    }
    if (!fileName.endsWith(".db"))
        fileName += ".db";
    return QDir::cleanPath(fileName);
}


bool Database::createConnection(const QString &connectionName, const QString &nonPrefixedDbName,
                                const Utils::DatabaseConnector &connector,
                                CreationOption createOption
                                )
{
    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    bool toReturn = true;
    d_database->m_Driver = connector.driver();
    QString prefixedDbName = prefixedDatabaseName(d_database->m_Driver, nonPrefixedDbName);

    if (WarnLogMessages) {
        LOG_FOR("Database", connectionName + "  //  " + prefixedDbName);
        qWarning() << connector;
    }

    if (!connector.isDriverValid()) {
        LOG_ERROR_FOR("Database", "Driver is not valid");
        return false;
    }

    if (QSqlDatabase::contains(connectionName)) {
        if (WarnLogMessages)
            LOG_FOR("Database", QCoreApplication::translate("Database",
                                                            "WARNING: %1 database already in use")
                    .arg(connectionName));
        d_database->m_ConnectionName = connectionName;
        return true;
    }

    QSqlDatabase DB;

    QString fileName = sqliteFileName(connectionName, nonPrefixedDbName, connector);
    QFileInfo sqliteFileInfo(fileName);

    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    if (createOption == DeleteAndRecreateDatabase
            && connector.accessMode() == DatabaseConnector::ReadWrite) {
        LOG_FOR("Database", "Delete database before re-creating it. Connection: " + connectionName + "; PathOrHost: " + prefixedDbName);
        switch (connector.driver()) {
        case SQLite:
        {
            if (sqliteFileInfo.exists()) {
                QFile f(sqliteFileInfo.absoluteFilePath());
                QString newName = QString("%1/%2-bkup-%3.%4")
                        .arg(sqliteFileInfo.absolutePath())
                        .arg(sqliteFileInfo.baseName())
                        .arg(QDateTime::currentDateTime().toString("yyyyMMddhhMMss"))
                        .arg(sqliteFileInfo.completeSuffix());
                if (!f.rename(newName))
                    LOG_ERROR_FOR("Database", "Unable to rename file.");
            }
            break;
        }
        case MySQL:
        {
            DB = QSqlDatabase::addDatabase("QMYSQL" , "__DB_DELETOR" + connectionName);
            DB.setHostName(connector.host());
            DB.setUserName(connector.clearLog());
            DB.setPassword(connector.clearPass());
            DB.setPort(connector.port());
            if (!DB.open()) {
                LOG_ERROR_FOR("Database", QString("Unable to connect to the server %1 - %2")
                                     .arg(connector.host()).arg(DB.lastError().text()));
                return false;
            }
            QSqlQuery query(DB);
            if (!query.exec("DROP DATABASE " + prefixedDbName)) {
                LOG_QUERY_ERROR_FOR("Database", query);
                LOG_ERROR_FOR("Database", "Unable to drop database");
            }
            if (WarnLogMessages)
                LOG_FOR("Database", QString("Connected to host %1").arg(connector.host()));
            break;
        }
        case PostSQL: break;
        }
        createOption = CreateDatabase;
    }
    if (QSqlDatabase::contains("__DB_DELETOR" + connectionName))
        QSqlDatabase::removeDatabase("__DB_DELETOR" + connectionName);

    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    switch (connector.driver()) {
    case SQLite: break;
    case MySQL:
    {
        DB = QSqlDatabase::addDatabase("QMYSQL" , connectionName);
        DB.setHostName(connector.host());
        DB.setUserName(connector.clearLog());
        DB.setPassword(connector.clearPass());
        DB.setPort(connector.port());
        if (!DB.open()) {
            LOG_ERROR_FOR("Database", QString("Unable to connect to the server %1 - %2")
                          .arg(connector.host()).arg(DB.lastError().text()));
            return false;
        }
        if (WarnLogMessages)
            LOG_FOR("Database", QString("Connected to host %1").arg(connector.host()));
        break;
    }
    case PostSQL:
    {
        break;
    }
    } // switch

    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    switch (connector.driver()) {
    case SQLite:
    {
        if ((!sqliteFileInfo.exists()) || (sqliteFileInfo.size() == 0)) {
            if (createOption == CreateDatabase) {
                if (!createDatabase(connectionName, sqliteFileInfo.fileName(), connector, createOption)) {
                    LOG_ERROR_FOR("Database", tkTr(Trans::Constants::DATABASE_1_CANNOT_BE_CREATED_ERROR_2).arg(prefixedDbName + "@" + fileName).arg("createDatabase returned false"));
                    return false;
                }
            } else { // Warn Only
                LOG_ERROR_FOR("Database", tkTr(Trans::Constants::DATABASE_1_CANNOT_BE_CREATED_ERROR_2).arg(prefixedDbName + "@" + fileName).arg("createDatabase not called"));
                return false;
            }
            qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
        }
        sqliteFileInfo.setFile(sqliteFileInfo.absoluteFilePath());
        break;
    }
    case MySQL:
    {
        DB.setDatabaseName(prefixedDbName);
        if (!DB.open()) {
            if (createOption == CreateDatabase) {
                if (!createDatabase(connectionName, prefixedDbName, connector, createOption)) {
                    LOG_ERROR_FOR("Database", tkTr(Trans::Constants::DATABASE_1_CANNOT_BE_CREATED_ERROR_2).arg(prefixedDbName + "@" + connector.host()).arg(""));
                    return false;
                }
            } else { // Warn Only
                if (WarnLogMessages)
                    LOG_ERROR_FOR("Database", tkTr(Trans::Constants::DATABASE_1_CANNOT_BE_CREATED_ERROR_2).arg(prefixedDbName + "@" + connector.host()).arg(""));
                return false;
            }
            qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
        }
        if (WarnLogMessages)
            LOG_FOR("Database", tkTr(Trans::Constants::CONNECTED_TO_DATABASE_1_DRIVER_2).arg(prefixedDbName).arg(DB.driverName()));
        break;
    }
    case PostSQL:
    {
        break;
    }
    }

    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    switch (connector.driver()) {
    case SQLite:
    {
        if (!sqliteFileInfo.isReadable()) {
            LOG_ERROR_FOR("Database", QCoreApplication::translate("Database", "ERROR: Database `%1` is not readable. Path: %2")
                          .arg(prefixedDbName, sqliteFileInfo.absoluteFilePath()));
            toReturn = false;
        }
        break;
    }
    case MySQL:
    {
        if (!DB.isOpen()) {
            qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
            if (!DB.open()) {
                LOG_ERROR_FOR("Database", QCoreApplication::translate("Database",
                                                                      "ERROR: Database %1 is not readable. Host: %2")
                              .arg(prefixedDbName, connector.host()));
                return false;
            }
            qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
        }
        QSqlQuery query("SHOW GRANTS FOR CURRENT_USER;", DB);
        if (!query.isActive()) {
            LOG_ERROR_FOR("Database", QCoreApplication::translate("Database",
                                                                  "ERROR: Database %1 is not readable. Path: %2")
                          .arg(prefixedDbName, connector.host()));
            LOG_QUERY_ERROR_FOR("Database", query);
            return false;
        } else {
            QStringList grants;
            while (query.next()) {
                grants << query.value(0).toString();
            }
            qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
            d_database->m_Grants.insert(connectionName, d_database->getGrants(connectionName, grants));
        }
        break;
    }
    case PostSQL:
    {
        break;
    }
    }

    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    if (connector.accessMode() == DatabaseConnector::ReadWrite) {
        switch (connector.driver()) {
        case SQLite:
        {
            if (!sqliteFileInfo.isWritable()) {
                LOG_ERROR_FOR("Database", QCoreApplication::translate("Database",
                                                                      "ERROR: Database %1 is not writable. Path: %2.")
                              .arg(prefixedDbName, fileName));
                toReturn = false;
            }
            break;
        }
        case MySQL:
        {
            break;
        }
        case PostSQL:
        {
            break;
        }
        }
    }

    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    switch (connector.driver())
    {
    case SQLite :
    {
        if (QSqlDatabase::connectionNames().contains(connectionName)) {
            DB = QSqlDatabase::database(connectionName);
        } else {
            DB = QSqlDatabase::addDatabase("QSQLITE", connectionName);
            DB.setDatabaseName(sqliteFileInfo.absoluteFilePath());
        }
        if (!DB.isOpen()) {
            if (!DB.open()) {
                LOG_ERROR_FOR("Database", "Unable to open database");
                return false;
            }
        }
        break;
    }
    case MySQL :
    {
        break;
    }
    case PostSQL :
        return false;
    }

    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    if (WarnLogMessages)
        LOG_FOR("Database", QCoreApplication::translate("Database",  "INFO: database %1 connection = %2")
                .arg(prefixedDbName).arg(DB.isOpen()));

    if (!DB.isOpen()) {
        if (!DB.open()) {
            LOG_ERROR_FOR("Database", tkTr(Trans::Constants::UNABLE_TO_OPEN_DATABASE_1_ERROR_2).arg(prefixedDbName, DB.lastError().text()));
            toReturn = false;
        }
    } else {
        if (WarnLogMessages)
            LOG_FOR("Database", tkTr(Trans::Constants::CONNECTED_TO_DATABASE_1_DRIVER_2).arg(prefixedDbName).arg(DB.driverName()));
    }

    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    if (toReturn)
        d_database->m_ConnectionName = connectionName;

    return toReturn;
}


bool Database::createDatabase(const QString &connectionName , const QString &prefixedDbName,
                            const Utils::DatabaseConnector &connector,
                            CreationOption createOption
                           )
{
    if (connector.driver()==SQLite) {
        return createDatabase(connectionName, prefixedDbName,
                              connector.absPathToSqliteReadWriteDatabase() + QDir::separator() + connectionName + QDir::separator(),
                              Database::TypeOfAccess(connector.accessMode()),
                              connector.driver(),
                              connector.clearLog(), connector.clearPass(),
                              connector.port(),
                              createOption);
    } else {
        return createDatabase(connectionName, prefixedDbName,
                              connector.host(),
                              Database::TypeOfAccess(connector.accessMode()),
                              connector.driver(),
                              connector.clearLog(), connector.clearPass(),
                              connector.port(),
                              createOption);
    }
}


QString Database::connectionName() const
{
    return d_database->m_ConnectionName;
}


QString Database::createUid()
{
    return Utils::createUid();
}

Database::Grants Database::grants(const QString &connectionName) const
{
    return d_database->m_Grants.value(connectionName, 0);
}


Database::Grants Database::getConnectionGrants(const QString &connectionName) // static
{
    QSqlDatabase DB = QSqlDatabase::database(connectionName);
    if (!connectedDatabase(DB, __LINE__))
        return Database::Grant_NoGrant;
    DB.transaction();

    if (DB.driverName()=="QSQLITE") {
        return Grant_All;
    }
    if (DB.driverName()=="QMYSQL") {
        QStringList grants;
        QSqlQuery query("SHOW GRANTS FOR CURRENT_USER;", DB);
        if (!query.isActive()) {
            LOG_ERROR_FOR("Database", "No grants for user on database?");
            LOG_QUERY_ERROR_FOR("Database", query);
            DB.rollback();
            return Grant_NoGrant;
        } else {
            while (query.next()) {
                grants << query.value(0).toString();
            }
        }
        query.finish();
        DB.commit();
        return DatabasePrivate::getGrants(connectionName, grants);
    }
    return Grant_NoGrant;
}



void Database::setConnectionName(const QString &c)
{ d_database->m_ConnectionName = c; }


void Database::setDriver(const Database::AvailableDrivers drv)
{ d_database->m_Driver = drv; }


Database::AvailableDrivers Database::driver() const
{ return d_database->m_Driver; }


int Database::addTable(const int & ref, const QString & name)
{
    d_database->m_Tables.insert(ref, name);
    return d_database->m_Tables.key(name);
}


int Database::addField(const int & tableref, const int & fieldref, const QString & name, TypeOfField type, const QString & defaultValue)
{
    Q_ASSERT_X(name.length() < 64, "Database", "Name of field can not exceed 50 chars");
    int ref = d_database->index(tableref, fieldref);
    d_database->m_Tables_Fields.insertMulti(tableref, ref);
    d_database->m_Fields.insert(ref , name);
    d_database->m_TypeOfField.insert(ref , type);
    d_database->m_DefaultFieldValue.insert(ref, defaultValue);
    return d_database->fieldFromIndex(ref);
}


void Database::addPrimaryKey(const int &tableref, const int &fieldref)
{
    d_database->m_PrimKeys.insertMulti(tableref, fieldref);
}


void Database::addIndex(const int &tableref, const int &fieldref, const QString &name)
{
    Utils::Field f = this->field(tableref, fieldref);
    addIndex(f, name);
}


void Database::addIndex(const Utils::Field &field, const QString &name)
{
    Internal::DbIndex index;
    index.field = this->field(field.table, field.field);
    if (name.isEmpty()) {
        index.name = index.field.tableName + "__" + index.field.fieldName;
    } else {
        index.name = name;
    }
    d_database->m_DbIndexes.append(index);
}


bool Database::checkDatabaseScheme()
{
    if (d_database->m_ConnectionName.isEmpty())
        return false;
    if (d_database->m_Tables.keys().count() == 0)
        return false;
    if (d_database->m_Tables_Fields.keys().count() == 0)
        return false;

    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    QSqlDatabase DB = QSqlDatabase::database(d_database->m_ConnectionName);
    if (!connectedDatabase(DB, __LINE__))
        return false;
    DB.transaction();

    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    QList<int> list = d_database->m_Tables.keys();
    qSort(list);
    foreach(int i, list) {
        qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
        QSqlRecord rec = DB.record(d_database->m_Tables.value(i));
        int expected = d_database->m_Tables_Fields.values(i).count();
        int current = rec.count();
        if (current != expected) {
            LOG_ERROR_FOR("Database", QCoreApplication::translate("Database", "Database Scheme Error: wrong number of fields for table %1 (expected: %2; current: %3)")
                                   .arg(d_database->m_Tables.value(i))
                          .arg(expected)
                          .arg(current)
                          );
            return false;
        }
        QList<int> fields = d_database->m_Tables_Fields.values(i);
        qSort(fields);
        int id = 0;
        foreach(int f, fields) {
            if (d_database->m_Fields.value(f)!= rec.field(id).name()) {
                LOG_ERROR_FOR("Database", QCoreApplication::translate("Database", "Database Scheme Error: field number %1 differs: %2 instead of %3 in table %4")
                                   .arg(id).arg(d_database->m_Fields.value(f), rec.field(id).name(), d_database->m_Tables.value(i)));
                return false;
            }
            id++;
        }
    }
    DB.commit();
    return true;
}


bool Database::checkVersion(const Field &field, const QString &expectedVersion)
{
    return (getVersion(field).compare(expectedVersion)==0);
}


QString Database::getVersion(const Field &field) const
{
    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return QString::null;
    DB.transaction();
    QString value;
    QSqlQuery query(DB);
    if (query.exec(select(field.table, field.field))) {
        if (query.next())
            value = query.value(0).toString();
    }
    query.finish();
    DB.commit();
    return value;
}


bool Database::setVersion(const Field &field, const QString &version)
{
    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__)) {
        return false;
    }
    DB.transaction();
    QSqlQuery query(DB);

    query.prepare(prepareDeleteQuery(field.table));
    if (!query.exec()) {
        LOG_QUERY_ERROR_FOR("Database", query);
        query.finish();
        DB.rollback();
        return false;
    }

    query.prepare(prepareInsertQuery(field.table));
    FieldList fields = this->fields(field.table);
    foreach(const Field f, fields)
        query.bindValue(f.field, QVariant());
    query.bindValue(field.field, version);
    if (!query.exec()) {
        LOG_QUERY_ERROR_FOR("Database", query);
        query.finish();
        DB.rollback();
        return false;
    }
    query.finish();
    DB.commit();
    return true;
}


QString Database::fieldName(const int &tableref, const int &fieldref) const
{
    if (!d_database->m_Tables.contains(tableref))
        return QString::null;
    if (!d_database->m_Tables_Fields.keys().contains(tableref))
        return QString::null;
    if (!d_database->m_Fields.keys().contains(fieldref + (tableref * 1000)))
        return QString::null;

    return d_database->m_Fields.value(d_database->index(tableref, fieldref));
}


Field Database::field(const int &tableref, const int &fieldref) const
{
    Field ret;
    ret.table = tableref;
    ret.field = fieldref;
    ret.tableName = table(tableref);
    ret.fieldName = fieldName(tableref, fieldref);
    ret.type = typeOfField(tableref, fieldref);
    return ret;
}


FieldList Database::fields(const int tableref) const
{
    FieldList fields;
    for(int i = 0; i < (d_database->m_Tables_Fields.values(tableref).count()); ++i) {
        fields << field(tableref, i);
    }
    return fields;
}


QStringList Database::fieldNames(const int &tableref) const
{
    if (!d_database->m_Tables.contains(tableref))
        return QStringList();
    if (!d_database->m_Tables_Fields.keys().contains(tableref))
        return QStringList();

    QList<int> list = d_database->m_Tables_Fields.values(tableref);
    qSort(list);
    QStringList toReturn;
    foreach(int i, list)
       toReturn << d_database->m_Fields.value(i);
    return toReturn;
}


QStringList Database::fieldNamesSql(const int &tableref) const
{
    if (!d_database->m_Tables.contains(tableref))
        return QStringList();
    if (!d_database->m_Tables_Fields.keys().contains(tableref))
        return QStringList();
    QStringList fieldNamesList;
    QString tableString = table(tableref);

    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return QStringList();
    DB.transaction();

    QSqlQuery query(DB);
    QString req;
    if (database().driverName().contains("MYSQL")) {
        req = QString("SHOW COLUMNS FROM %1").arg(tableString);
    }
    if (database().driverName().contains("SQLITE")) {
        req = QString("PRAGMA table_info('%1');")
             .arg(tableString);
    }
    if (query.exec(req)) {
        while (query.next()) {
            fieldNamesList << query.value(Name_PragmaValue).toString();
        }
    } else {
        LOG_QUERY_ERROR_FOR("Database", query);
        Utils::warningMessageBox("Warning",QString("Unable to get the fields of %1").arg(tableString));
        DB.rollback();
        return QStringList();
    }
    DB.commit();
    return fieldNamesList;
}


Database::TypeOfField Database::typeOfField(const int tableref, const int fieldref) const
{
    int ref = d_database->index(tableref, fieldref);
    return Database::TypeOfField(d_database->m_TypeOfField.value(ref , FieldUndefined));
}


QString Database::table(const int &tableref) const
{
    return d_database->m_Tables.value(tableref, QString());
}


QStringList Database::tables() const
{
    return d_database->m_Tables.values();
}


QString Database::getWhereClause(const int &tableref, const QHash<int, QString> &conditions) const
{
    QString where = "";
    QHashIterator<int, QString> i(conditions);
    while (i.hasNext()) {
        i.next();
        if (i.key() == -1) {
            where.append(QString(" (%1) AND ")
                         .arg(i.value()));
        } else {
            int index = d_database->index(tableref, i.key());
            if (!d_database->m_Fields.keys().contains(index))
                continue;
            where.append(QString(" (`%1`.`%2` %3) AND ")
                         .arg(d_database->m_Tables[tableref])
                         .arg(d_database->m_Fields.value(index))
                         .arg(i.value()));
        }
    }
    where.chop(5);
    if (conditions.count() > 1)
        where = QString("(%1)").arg(where);
    if (WarnSqlCommands)
        qWarning() << where;
    return where;
}


QString Database::getWhereClause(const FieldList &fields, WhereClauseType type) const
{
    QString where = "";
    QString clause;
    if (type==AND)
        clause = "AND";
    else if (type==OR)
        clause = "OR ";

    for(int i = 0; i < fields.count(); ++i) {
        QString tab, f;
        if (fields.at(i).tableName.isEmpty()) {
            tab = table(fields.at(i).table);
            f = fieldName(fields.at(i).table, fields.at(i).field);
        } else {
            tab = fields.at(i).tableName;
            f = fields.at(i).fieldName;
        }

        if (fields.at(i).orCondition) {
            where.chop(4);
            where += "OR ";
        }
        where += QString("(`%1`.`%2` %3) %4 ")
                .arg(tab)
                .arg(f)
                .arg(fields.at(i).whereCondition)
                .arg(clause);
    }
    where.chop(5);
    if (fields.count() > 1)
        where = QString("(%1)").arg(where);
    if (WarnSqlCommands)
        qWarning() << where;
    return where;
}

QString Database::getWhereClause(const Field &field) const
{
    return getWhereClause(Utils::FieldList() << field);
}


QString Database::joinToSql(const Join &join) const
{
    QString s;
    switch (join.type) {
    case SimpleJoin: s = "JOIN "; break;
    case OuterJoin: s = "OUTER JOIN "; break;
    case LeftJoin: s = "LEFT JOIN "; break;
    case InnerJoin: s = "INNER JOIN "; break;
    case NaturalJoin: s = "NATURAL JOIN "; break;
    case CrossJoin: s = "CROSS JOIN "; break;
    }
    if (s.isEmpty())
        return s;
    s += "`" + join.field1.tableName + "` ON " ;
    s += QString("`%1`.`%2`=`%3`.`%4` ")
         .arg(join.field1.tableName, join.field1.fieldName)
         .arg(join.field2.tableName, join.field2.fieldName);
    return s;
}


QString Database::select(const int &tableref, const int &fieldref, const QHash<int, QString> &conditions) const
{
    QString toReturn;
    toReturn = QString("SELECT `%2`.`%1` FROM `%2` WHERE %3")
            .arg(fieldName(tableref, fieldref))
            .arg(table(tableref))
            .arg(getWhereClause(tableref, conditions));
    if (WarnSqlCommands)
        qWarning() << toReturn;
    return toReturn;
}


QString Database::select(const int &tableref, const int &fieldref) const
{
    QString toReturn;
    toReturn = QString("SELECT `%2`.`%1` FROM `%2`")
            .arg(fieldName(tableref, fieldref))
            .arg(table(tableref));
    if (WarnSqlCommands)
        qWarning() << toReturn;
    return toReturn;
}


QString Database::select(const int &tableref, const QList<int> &fieldsref, const QHash<int, QString> & conditions) const
{
    QString toReturn;
    QString tmp;
    foreach(const int & i, fieldsref)
        tmp += "`" + table(tableref) + "`.`" + fieldName(tableref, i)+ "`, ";
    if (tmp.isEmpty())
        return QString::null;
    tmp.chop(2);
    if (!conditions.isEmpty()) {
        toReturn = QString("SELECT %1 FROM `%2` WHERE %3")
                .arg(tmp)
                .arg(table(tableref))
                .arg(getWhereClause(tableref, conditions));
    } else {
        toReturn = QString("SELECT %1 FROM `%2`")
                .arg(tmp)
                .arg(table(tableref));
    }
    if (WarnSqlCommands)
        qWarning() << toReturn;
    return toReturn;
}


QString Database::select(const int & tableref,const  QList<int> &fieldsref) const
{
    QString toReturn;
    QString tmp;
    foreach(const int &i, fieldsref)
        tmp += "`" + table(tableref) + "`.`" + fieldName(tableref, i)+ "`, ";
    if (tmp.isEmpty())
        return QString::null;
    tmp.chop(2);
    toReturn = QString("SELECT %1 FROM `%2`")
            .arg(tmp)
            .arg(table(tableref));
    if (WarnSqlCommands)
        qWarning() << toReturn;
    return toReturn;
}


QString Database::select(const int &tableref, const QHash<int, QString> &conditions) const
{
    QString toReturn;
    QString tmp;
    QList<int> list = d_database->m_Tables_Fields.values(tableref);
    qSort(list);
    foreach(const int &i, list)
        tmp += "`" + table(tableref) + "`.`" + d_database->m_Fields.value(i) + "`, ";
    if (tmp.isEmpty())
        return QString::null;
    tmp.chop(2);
    toReturn = QString("SELECT %1 FROM `%2` WHERE %3")
            .arg(tmp)
            .arg(table(tableref))
            .arg(getWhereClause(tableref, conditions));
    if (WarnSqlCommands)
        qWarning() << toReturn;
    return toReturn;
}


QString Database::select(const int &tableref) const
{
    QString toReturn;
    QString tmp;
    QList<int> list = d_database->m_Tables_Fields.values(tableref);
    qSort(list);
    foreach(const int & i, list)
        tmp += "`" + table(tableref) + "`.`" + d_database->m_Fields.value(i) + "`, ";
    if (tmp.isEmpty())
        return QString::null;
    tmp.chop(2);
    toReturn = QString("SELECT %1 FROM `%2`")
            .arg(tmp)
            .arg(table(tableref));
    if (WarnSqlCommands)
        qWarning() << toReturn;
    return toReturn;
}


QString Database::selectDistinct(const int &tableref, const QList<int> &fields, const QHash<int, QString> &conditions) const
{
    QString toReturn = select(tableref, fields, conditions);
    toReturn = toReturn.replace("SELECT", "SELECT DISTINCT").replace("SELECT DISTINCT DISTINCT", "SELECT DISTINCT");
    return toReturn;
}


QString Database::selectDistinct(const int &tableref, const int &fieldref, const QHash<int, QString> &conditions) const
{
    return select(tableref, fieldref, conditions).replace("SELECT", "SELECT DISTINCT").replace("SELECT DISTINCT DISTINCT", "SELECT DISTINCT");
}


QString Database::selectDistinct(const int & tableref, const int & fieldref) const
{
    return select(tableref, fieldref).replace("SELECT", "SELECT DISTINCT").replace("SELECT DISTINCT DISTINCT", "SELECT DISTINCT");
}

QString Database::select(const FieldList &select, const JoinList &joins) const
{
    FieldList get;
    JoinList jns;
    QString fields, from;
    QStringList tables;
    for(int i=0; i < select.count(); ++i) {
        get << select.at(i);
        if (get.at(i).tableName.isEmpty() || get.at(i).fieldName.isEmpty()) {
            get[i].tableName = table(select.at(i).table);
            get[i].fieldName = fieldName(select.at(i).table, select.at(i).field);
        }
    }
    for(int i=0; i < joins.count(); ++i) {
        Field f1 = field(joins.at(i).field1.table, joins.at(i).field1.field);
        Field f2 = field(joins.at(i).field2.table, joins.at(i).field2.field);
        jns << Join(f1, f2);
    }

    for(int i=0; i < get.count(); ++i) {
        fields += QString("`%1`.`%2`, ").arg(get.at(i).tableName).arg(get.at(i).fieldName);
        tables << get.at(i).tableName;
    }
    tables.removeDuplicates();

    if (fields.isEmpty())
        return QString();
    fields.chop(2);

    QString j;
    for(int i=0; i < jns.count(); ++i) {
        j += joinToSql(jns.at(i)) + "\n";
        tables.removeAll(jns.at(i).field1.tableName);
    }
    tables.removeDuplicates();

    foreach(const QString &tab, tables) {
        from += QString("`%1`, ").arg(tab);
    }
    from.chop(2);

    return QString("SELECT %1 FROM %2 \n %3").arg(fields, from, j);
}


QString Database::select(const FieldList &select, const JoinList &joins, const FieldList &conditions) const
{
    return d_database->getSelectCommand(select, joins, conditions);
}

QString Database::select(const FieldList &select, const JoinList &joins, const Field &condition) const
{
    FieldList cond;
    cond << condition;
    return this->select(select, joins, cond);
}


QString Database::select(const int tableref, const JoinList &joins, const FieldList &conditions) const
{
    FieldList fields;
    for(int i = 0; i < (d_database->m_Tables_Fields.values(tableref).count()); ++i) {
        fields << Field(tableref, i);
    }
    return this->select(fields, joins, conditions);
}

QString Database::select(const int tableref, const Join &join, const Field &condition) const
{
    JoinList joins;
    joins << join;
    FieldList cond;
    cond << condition;
    return this->select(tableref, joins, cond);
}

QString Database::select(const int tableref, const Join &join, const FieldList &conditions) const
{
    JoinList joins;
    joins << join;
    return this->select(tableref, joins, conditions);
}

QString Database::select(const Field &select, const JoinList &joins, const Field &conditions) const
{
    FieldList get;
    get << select;
    FieldList cond;
    cond << conditions;
    return this->select(get, joins, cond);
}

QString Database::select(const FieldList &select, const Join &join, const Field &condition) const
{
    JoinList joins;
    joins << join;
    FieldList cond;
    cond << condition;
    return this->select(select, joins, cond);
}

QString Database::select(const Field &select, const Join &join, const FieldList &conditions) const
{
    JoinList joins;
    joins << join;
    FieldList get;
    get << select;
    return this->select(get, joins, conditions);
}

QString Database::select(const Field &select, const JoinList &joins, const FieldList &conditions) const
{
    FieldList get;
    get << select;
    return this->select(get, joins, conditions);
}

QString Database::select(const Field &select, const Join &join, const Field &condition) const
{
    JoinList joins;
    joins << join;
    FieldList get;
    get << select;
    FieldList conds;
    conds << condition;
    return this->select(get, joins, conds);
}


QString Database::fieldEquality(const int tableRef1, const int fieldRef1, const int tableRef2, const int fieldRef2) const
{
    return QString("`%1`.`%2`=`%3`.`%4`")
            .arg(table(tableRef1), fieldName(tableRef1, fieldRef1))
            .arg(table(tableRef2), fieldName(tableRef2, fieldRef2));
}


int Database::count(const int & tableref, const int & fieldref, const QString &filter) const
{
    int count = -1;
    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return count;
    DB.transaction();

    QString req = QString("SELECT count(`%1`) FROM `%2`").arg(d_database->m_Fields.value(d_database->index(tableref, fieldref))).arg(d_database->m_Tables[tableref]);
    if (!filter.isEmpty())
        req += " WHERE " + filter;
    if (WarnSqlCommands)
        qWarning() << req;
    QSqlQuery query(DB);
    if (query.exec(req)) {
        if (query.next()) {
            count = query.value(0).toInt();
        } else {
            LOG_QUERY_ERROR_FOR("Database", query);
        }
    } else {
        LOG_QUERY_ERROR_FOR("Database", query);
    }
    query.finish();
    (count==-1) ? DB.rollback() : DB.commit();
    return count;
}


int Database::count(const FieldList &select, const JoinList &joins, const FieldList &conditions) const
{
    int count = -1;
    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return count;
    DB.transaction();

    QString req = d_database->getSelectCommand(select, joins, conditions, DatabasePrivate::Count);
    if (WarnSqlCommands)
        qWarning() << req;
    QSqlQuery query(DB);
    if (query.exec(req)) {
        if (query.next()) {
            count = query.value(0).toInt();
        } else {
            LOG_QUERY_ERROR_FOR("Database", query);
        }
    } else {
        LOG_QUERY_ERROR_FOR("Database", query);
    }
    query.finish();
    (count==-1) ? DB.rollback() : DB.commit();
    return count;
}


int Database::countDistinct(const FieldList &select, const JoinList &joins, const FieldList &conditions) const
{
    int count = -1;
    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return count;
    DB.transaction();

    QString req = d_database->getSelectCommand(select, joins, conditions, DatabasePrivate::CountDistinct);
    if (WarnSqlCommands)
        qWarning() << req;
    QSqlQuery query(DB);
    if (query.exec(req)) {
        if (query.next()) {
            count = query.value(0).toInt();
        } else {
            LOG_QUERY_ERROR_FOR("Database", query);
        }
    } else {
        LOG_QUERY_ERROR_FOR("Database", query);
    }
    query.finish();
    (count==-1) ? DB.rollback() : DB.commit();
    return count;
}


QVariant Database::max(const int &tableref, const int &fieldref, const QString &filter) const
{
    QVariant toReturn;
    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return toReturn;
    DB.transaction();

    QString req = QString("SELECT max(%1) FROM %2")
                  .arg(d_database->m_Fields.value(d_database->index(tableref, fieldref)))
                  .arg(d_database->m_Tables[tableref]);
    if (!filter.isEmpty())
        req += " WHERE " + filter;
    if (WarnSqlCommands)
        qWarning() << req;
    QSqlQuery query(DB);
    if (query.exec(req)) {
        if (query.next()) {
            toReturn = query.value(0);
        } else {
            LOG_QUERY_ERROR_FOR("Database", query);
            query.finish();
            DB.rollback();
        }
    } else {
        LOG_QUERY_ERROR_FOR("Database", query);
        query.finish();
        DB.rollback();
    }
    query.finish();
    DB.commit();
    return toReturn;
}


QVariant Database::max(const int &tableref, const int &fieldref, const int &groupBy, const QString &filter) const
{
    QVariant toReturn;
    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return toReturn;
    DB.transaction();
    QString req = QString("SELECT max(%1) FROM %2 GROUP BY %3")
                  .arg(d_database->m_Fields.value(d_database->index(tableref, fieldref)))
                  .arg(d_database->m_Tables[tableref])
                  .arg(d_database->m_Fields.value(d_database->index(tableref, groupBy)));
    if (!filter.isEmpty())
        req += " WHERE " + filter;
    if (WarnSqlCommands)
        qWarning() << req;
    QSqlQuery query(DB);
    if (query.exec(req)) {
        if (query.next()) {
            toReturn = query.value(0);
            DB.commit();
        } else {
            LOG_QUERY_ERROR_FOR("Database", query);
            DB.rollback();
        }
    } else {
        LOG_QUERY_ERROR_FOR("Database", query);
        DB.rollback();
    }
    return toReturn;
}


QVariant Database::min(const int &tableref, const int &fieldref, const QString &filter) const
{
    QVariant toReturn;
    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return toReturn;
    DB.transaction();

    QString req = QString("SELECT MIN(%1) FROM %2")
                  .arg(d_database->m_Fields.value(d_database->index(tableref, fieldref)))
                  .arg(d_database->m_Tables[tableref]);
    if (!filter.isEmpty())
        req += " WHERE " + filter;
    if (WarnSqlCommands)
        qWarning() << req;
    QSqlQuery query(DB);
    if (query.exec(req)) {
        if (query.next()) {
            toReturn = query.value(0);
            DB.commit();
        } else {
            LOG_QUERY_ERROR_FOR("Database", query);
            DB.rollback();
        }
    } else {
        LOG_QUERY_ERROR_FOR("Database", query);
        DB.rollback();
    }
    return toReturn;
}


QString Database::maxSqlCommand(const int &tableref, const int &fieldref, const QString &filter) const
{
    QString req = QString("SELECT max(%1) FROM %2")
                  .arg(d_database->m_Fields.value(d_database->index(tableref, fieldref)))
                  .arg(d_database->m_Tables[tableref]);
    if (!filter.isEmpty())
        req += " WHERE " + filter;
    if (WarnSqlCommands)
        qWarning() << req;
    return req;
}


QString Database::totalSqlCommand(const int tableRef, const int fieldRef, const QHash<int, QString> &where) const
{
    QString toReturn;
    if (where.count()) {
        toReturn = QString("SELECT SUM(`%1`) FROM `%2` WHERE %3")
                   .arg(d_database->m_Fields.value(d_database->index(tableRef, fieldRef)))
                   .arg(d_database->m_Tables.value(tableRef))
                   .arg(getWhereClause(tableRef, where));
    } else  {
        toReturn = QString("SELECT SUM(`%1`) FROM `%2`")
                   .arg(d_database->m_Fields.value(d_database->index(tableRef, fieldRef)))
                   .arg(d_database->m_Tables.value(tableRef));
    }
    return toReturn;
}

QString Database::totalSqlCommand(const int tableref, const int fieldref) const
{
    QString toReturn;
    toReturn = QString("SELECT SUM(`%1`) FROM `%2`")
               .arg(d_database->m_Fields.value(d_database->index(tableref, fieldref)))
               .arg(d_database->m_Tables.value(tableref));
    return toReturn;
}


double Database::sum(const int tableRef, const int fieldRef, const QHash<int, QString> &where) const
{
    double toReturn = 0.0;
    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return toReturn;
    DB.transaction();

    QSqlQuery query(DB);
    QString req;
    if (where.isEmpty())
        req = totalSqlCommand(tableRef, fieldRef);
    else
        req = totalSqlCommand(tableRef, fieldRef, where);

    if (query.exec(req)) {
        if (query.next()) {
            toReturn = query.value(0).toDouble();
            DB.commit();
        } else {
            LOG_QUERY_ERROR_FOR("Database", query);
            DB.rollback();
        }
    } else {
        LOG_QUERY_ERROR_FOR("Database", query);
        DB.rollback();
    }
    return toReturn;
}


double Database::sum(const int tableRef, const int fieldRef) const
{
    QHash<int, QString> where;
    return sum(tableRef, fieldRef, where);
}

QString Database::prepareInsertQuery(const int tableref) const
{
    QString toReturn;
    QString fields;
    QString values;
    QList<int> list = d_database->m_Tables_Fields.values(tableref);
    qSort(list);
    foreach(const int & i, list)
    {
        fields.append("`"+ d_database->m_Fields.value(i) + "`, ");
        values.append("? , ");
    }
    fields.chop(2);
    values.chop(2);
    toReturn = QString("INSERT INTO `%1` \n(%2) \nVALUES (%3);")
            .arg(table(tableref))
            .arg(fields)
            .arg(values);
    if (WarnSqlCommands)
        qWarning() << toReturn;
    return toReturn;
}

QString Database::prepareUpdateQuery(const int tableref, const int fieldref, const QHash<int, QString> &conditions)
{
    QString toReturn;
    toReturn = QString("UPDATE `%1` SET `%2` = ? WHERE %4")
               .arg(table(tableref))
               .arg(fieldName(tableref, fieldref))
               .arg(getWhereClause(tableref, conditions));
    if (WarnSqlCommands)
        qWarning() << toReturn;
    return toReturn;
}

QString Database::prepareUpdateQuery(const int tableref, const QList<int> &fieldref, const QHash<int, QString> &conditions)
{
    QString toReturn;
    QString tmp;
    foreach(const int &i, fieldref) {
        tmp += "`" + fieldName(tableref, i) + "`= ?, ";
    }
    tmp.chop(2);
    toReturn = QString("UPDATE `%1` SET %2 WHERE %4")
               .arg(table(tableref))
               .arg(tmp)
               .arg(getWhereClause(tableref, conditions));
    if (WarnSqlCommands)
        qWarning() << toReturn;
    return toReturn;
}

QString Database::prepareUpdateQuery(const int tableref, const int fieldref)
{
    QString toReturn;
    toReturn = QString("UPDATE `%1` SET `%2` =?")
               .arg(table(tableref))
               .arg(fieldName(tableref, fieldref));
    if (WarnSqlCommands)
        qWarning() << toReturn;
    return toReturn;
}

QString Database::prepareUpdateQuery(const int tableref, const QHash<int, QString> &conditions)
{
    QString toReturn;
    QString tmp;
    foreach(const QString &f, fieldNames(tableref))
        tmp += QString ("`%1`=? , ").arg(f);
    tmp.chop(2);
    toReturn = QString("UPDATE `%1` SET \n%2 \nWHERE %3")
               .arg(table(tableref))
               .arg(tmp)
               .arg(getWhereClause(tableref, conditions));
    if (WarnSqlCommands)
        qWarning() << toReturn;
    return toReturn;
}

QString Database::prepareUpdateQuery(const int tableref)
{
    QString toReturn;
    QString tmp;
    foreach(const QString &f, fieldNames(tableref))
        tmp += QString ("`%1`=? , ").arg(f);
    tmp.chop(2);
    toReturn = QString("UPDATE `%1` SET \n%2 ")
               .arg(table(tableref))
               .arg(tmp);
    if (WarnSqlCommands)
        qWarning() << toReturn;
    return toReturn;
}

QString Database::prepareDeleteQuery(const int tableref)
{
    return prepareDeleteQuery(tableref, QHash<int,QString>());
}

QString Database::prepareDeleteQuery(const int tableref, const QHash<int,QString> &conditions)
{
    QString toReturn;
    if (!conditions.isEmpty()) {
        toReturn = QString("DELETE FROM `%1` \n WHERE %2")
                .arg(table(tableref))
                .arg(getWhereClause(tableref, conditions));
    } else {
        toReturn = QString("DELETE FROM `%1`")
                .arg(table(tableref));
    }
    if (WarnSqlCommands)
        qWarning() << toReturn;
    return toReturn;
}


bool Database::createTable(const int &tableref) const
{
    if (!d_database->m_Tables.contains(tableref))
        return false;
    if (! d_database->m_Tables_Fields.keys().contains(tableref))
        return false;
    if (d_database->m_ConnectionName.isEmpty())
        return false;

    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return false;
    bool insideTransaction = true;
    if (!d_database->_transaction) {
        DB.transaction();
        d_database->_transaction = true;
        insideTransaction = false;
    }

    QStringList req;
    req = d_database->getSQLCreateTable(tableref);

    if (!executeSQL(req, DB)) {
        if (!insideTransaction) {
            d_database->_transaction = false;
            DB.rollback();
        }
        return false;
    } else {
        if (!insideTransaction) {
            d_database->_transaction = false;
            DB.commit();
        }
    }
    return true;
}


bool Database::createTables() const
{
    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return false;

    bool insideTransaction = true;
    if (!d_database->_transaction) {
        DB.transaction();
        d_database->_transaction = true;
        insideTransaction = false;
    }

    QList<int> list = d_database->m_Tables.keys();
    qSort(list);
    foreach(int i, list) {
        qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
        if(!createTable(i)) {
            LOG_ERROR_FOR("Database", QCoreApplication::translate("Database", "Can not create table %1").arg(table(i)));
            if (!insideTransaction) {
                DB.rollback();
                d_database->_transaction = false;
            }
            return false;
        }
    }
    if (!insideTransaction) {
        DB.commit();
        d_database->_transaction = false;
    }
    return true;
}


bool Database::alterTableForNewField(const int tableRef, const int newFieldRef, const int TypeOfField, const QString &nullOption)
{
    Q_UNUSED(TypeOfField);
    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return false;
    QString defaultSql;
    if (!nullOption.isEmpty()) {
        if (driver()==MySQL)
            defaultSql = QString("DEFAULT %1").arg(nullOption);
        else if (driver()==SQLite)
            defaultSql = QString("DEFAULT %1").arg(nullOption);
    } else {
        const QString &defaultValue = d_database->m_DefaultFieldValue.value(d_database->index(tableRef, newFieldRef));
        if (!defaultValue.isEmpty()) {
            if (driver()==MySQL)
                defaultSql = QString("DEFAULT %1").arg(defaultValue);
            else if (driver()==SQLite)
                defaultSql = QString("DEFAULT %1").arg(defaultValue);
        }
    }

    QString type = d_database->getTypeOfField(d_database->index(tableRef, newFieldRef));
    QString req;
    req = QString("ALTER TABLE `%1`"
                  "  ADD `%2` %3 %4;")
            .arg(table(tableRef), fieldName(tableRef, newFieldRef), type, defaultSql);

    if (WarnSqlCommands)
        qWarning() << req;

    DB.transaction();
    QSqlQuery query(DB);
    if (!query.exec(req)) {
          LOG_QUERY_ERROR_FOR("Database", query);
          LOG_FOR("Database",QString("Unable to add the fields %1").arg(fieldName(tableRef, newFieldRef)));
          query.finish();
          DB.rollback();
          return false;
    }
    query.finish();
    DB.commit();
    return true;
}


bool Database::modifyMySQLColumnType(const int & tableref, const int & fieldref)
{
    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))                                       
        return false;
    QString req;
    QString newType = d_database->getTypeOfField(d_database->index(tableref, fieldref));
    req = QString("ALTER TABLE `%1` MODIFY COLUMN `%2` %3;")
            .arg(table(tableref), fieldName(tableref, fieldref), newType);
    DB.transaction();
    QSqlQuery query(DB);
    if (!query.exec(req)) {                                                     
          LOG_QUERY_ERROR_FOR("Database", query);                               
          query.finish();                                                       
          DB.rollback();                                                        
          return false;                                                         
    }
    query.finish();
    DB.commit();      
    return true;
}

/**                                                                             
 * Modify the type of an existing column of a table in a MySQL database
 * Default value is set by QString & defaultValue                             
 */
bool Database::modifyMySQLColumnType(const int & tableref, const int & fieldref,
                                     const QString & defaultValue)
{                                                                               
    QSqlDatabase DB = database();                                               
    if (!connectedDatabase(DB, __LINE__))                                       
        return false;
    QString newType = d_database->getTypeOfField(d_database->index(tableref, fieldref));     
    QString req;
    req = QString("ALTER TABLE `%1` MODIFY COLUMN `%2` %3 DEFAULT %4;")                  
            .arg(table(tableref)).arg(fieldName(tableref, fieldref)).arg(newType).arg(defaultValue);         
    DB.transaction();                                                           
    QSqlQuery query(DB);                                                        
    if (!query.exec(req)) {                                                     
          LOG_QUERY_ERROR_FOR("Database", query);                               
          query.finish();                                                       
          DB.rollback();                                                        
          return false;                                                         
    }                                                                           
    query.finish();                                                             
    DB.commit();                                                                
    return true;                                                                
}


QStringList Database::mySQLUserHostNames(const QString & userName)

{
    QSqlDatabase DB = database();
    if (!connectedDatabase(DB, __LINE__))
        return QStringList();
    QString req;
    req = QString("SELECT Host FROM mysql.user where user= '%1';").arg(userName);
    DB.transaction();

    QStringList hostNames;
    QSqlQuery query(req, DB);
    if (!query.isActive()) {
        LOG_ERROR_FOR("Database", "No host name for user?");
        LOG_QUERY_ERROR_FOR("Database", query);
        DB.rollback();
        return QStringList();
    } else {
        while (query.next()) {
            hostNames << query.value(0).toString();
        }
    }
    query.finish();
    DB.commit();
    qWarning() << "mySQLUserHostNames() -> hostNames:" << hostNames;
    return hostNames;
}


bool Database::vacuum(const QString &connectionName)
{
    QSqlDatabase DB = QSqlDatabase::database(connectionName);
    if (!connectedDatabase(DB, __LINE__))
        return false;
    QSqlQuery query(DB);
    if (!query.exec("VACUUM")) {
          LOG_QUERY_ERROR_FOR("Database", query);
          return false;
    }
    return true;
}


bool Database::executeSQL(const QStringList &list, QSqlDatabase &DB)
{
    if (!connectedDatabase(DB, __LINE__))
        return false;
    DB.transaction();
    QSqlQuery query(DB);
    foreach(QString r, list) {
        r = r.trimmed();
        if (r.isEmpty())
            continue;

        if (r.startsWith("--")) {
            continue;
        }

        if (r.startsWith("."))
            continue;

        if (r.startsWith("BEGIN", Qt::CaseInsensitive)
                || r.startsWith("COMMIT", Qt::CaseInsensitive))
            continue;

        if (!query.exec(r)) {
            LOG_QUERY_ERROR_FOR("Database", query);
            query.finish();
            DB.rollback();
            return false;
        }
        query.finish();
    }
    DB.commit();
    return true;
}


bool Database::executeSQL(const QString &req, QSqlDatabase &DB)
{
    if (req.isEmpty())
        return false;
    if (!connectedDatabase(DB, __LINE__))
        return false;
    QStringList list = req.trimmed().split(";\n", QString::SkipEmptyParts);
    return executeSQL(list, DB);
}


bool Database::executeSqlFile(const QString &connectionName, const QString &fileName, QString *error)
{
    if (error)
        error->clear();

    QSqlDatabase DB = QSqlDatabase::database(connectionName);
    if (!connectedDatabase(DB, __LINE__)) {
        if (error)
            error->append(tkTr(Trans::Constants::UNABLE_TO_OPEN_DATABASE_1_ERROR_2)
                          .arg(DB.connectionName()).arg(DB.lastError().text()));
        return false;
    }

    if (!QFile::exists(fileName)) {
        LOG_ERROR_FOR("Database", tkTr(Trans::Constants::FILE_1_DOESNOT_EXISTS).arg(fileName));
        if (error)
            error->append(tkTr(Trans::Constants::FILE_1_DOESNOT_EXISTS).arg(fileName));
        return false;
    }

    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        LOG_ERROR_FOR("Database", tkTr(Trans::Constants::FILE_1_ISNOT_READABLE).arg(fileName));
        if (error)
            error->append(tkTr(Trans::Constants::FILE_1_ISNOT_READABLE).arg(fileName));
        return false;
    }

    QString req = QString::fromUtf8(file.readAll());
    return executeSQL(req, DB);
}


bool Database::importCsvToDatabase(const QString &connectionName, const QString &fileName, const QString &table, const QString &separator, bool ignoreFirstLine)
{
    QSqlDatabase DB = QSqlDatabase::database(connectionName);
    if (!connectedDatabase(DB, __LINE__))
        return false;
    DB.transaction();

    if (!DB.tables().contains(table)) {
        LOG_ERROR_FOR("Database", "No table found");
        DB.rollback();
        return false;
    }

    QString content = Utils::readTextFile(fileName, Utils::DontWarnUser);
    if (content.isEmpty())
        return false;
    QStringList lines = content.split("\n", QString::SkipEmptyParts);
    content.clear();
    int start = 0;
    if (ignoreFirstLine)
        start = 1;

    QSqlRecord record = DB.record(table);
    QString req = "INSERT INTO " + table + " (\n";
    for(int i = 0; i < record.count(); ++i) {
        req += "`" + record.fieldName(i) + "`, ";
    }
    req.chop(2);
    req += ")\n VALUES (";

    QSqlQuery query(DB);
    for(int i = start; i < lines.count(); ++i) {
        QStringList values = lines.at(i).split(separator, QString::KeepEmptyParts);
        QString reqValues;
        int counter = 0;
        foreach(const QString &val, values) {
            ++counter;
            if (val.isEmpty()) {
                reqValues += "NULL, ";
            } else {
                if (val.startsWith("'") && val.endsWith("'")) {
                    reqValues += val + ", ";
                } else if (val.startsWith("\"") && val.endsWith("\"")) {
                    reqValues += val + ", ";
                } else if (val.contains(QRegExp("\\D", Qt::CaseInsensitive))) {
                    QString tmp = val;
                    reqValues += "\"" + tmp.replace("\"", "“") + "\", ";
                } else {
                    reqValues += val + ", ";
                }
            }
        }
        reqValues.chop(2);
        reqValues += ");\n";
        if (!query.exec(req + reqValues)) {
            LOG_QUERY_ERROR_FOR("Database", query);
            query.finish();
            DB.rollback();
            return false;
        }
        query.finish();
    }
    DB.commit();
    return true;
}

QStringList DatabasePrivate::getSQLCreateTable(const int &tableref)
{
    QString toReturn;
    toReturn = QString("CREATE TABLE IF NOT EXISTS `%1` (\n  ").arg(m_Tables.value(tableref));
    QList<int> list = m_Tables_Fields.values(tableref);
    qSort(list);
    QStringList fieldLine;

    int maxLength = 0;
    for(int i=0; i < list.count(); ++i) {
        const QString &f = m_Fields.value(list.at(i));
        maxLength = qMax(maxLength, f.size());
    }
    maxLength += 3;

    foreach(int i, list) {
        QString fieldName = QString("`%1`").arg(m_Fields.value(i)).leftJustified(maxLength, ' ');
        if (m_DefaultFieldValue.value(i) == "NULL") {
            if (Database::TypeOfField(m_TypeOfField.value(i)) != Database::FieldIsUniquePrimaryKey) {
                fieldLine.append(QString("%1 %2 DEFAULT NULL")
                                .arg(fieldName)
                                .arg(getTypeOfField(i)));// .leftJustified(20, ' '))
            } else {
                fieldLine.append(QString("%1 %2")
                                .arg(fieldName)
                                .arg(getTypeOfField(i)));// .leftJustified(20, ' '))
            }
        } else {
            switch (Database::TypeOfField(m_TypeOfField.value(i)))
            {
            case Database::FieldIsUUID :
            case Database::FieldIsLongText :
            case Database::FieldIsShortText :
            case Database::FieldIsTwoChars :
            case Database::FieldIsBlob :
                fieldLine.append(QString("%1 %2 DEFAULT '%3'")
                                .arg(fieldName)
                                .arg(getTypeOfField(i))// .leftJustified(20, ' '))
                                .arg(m_DefaultFieldValue.value(i)));
                break;
            case Database::FieldIsDate :
                {
                    QString defVal = m_DefaultFieldValue.value(i).simplified();
                    if (defVal.startsWith("CUR")) {
                        if (m_Driver==Database::MySQL) {
                            defVal = "NULL";
                        } else if (defVal.endsWith("()")) {
                            defVal = defVal.remove("()");
                        }
                        fieldLine.append(QString("%1 %2 DEFAULT %3")
                                         .arg(fieldName)
                                         .arg(getTypeOfField(i))// .leftJustified(20, ' '))
                                         .arg(defVal));
                    }
                    else
                        fieldLine.append(QString("%1 %2 DEFAULT '%3'")
                                        .arg(fieldName)
                                        .arg(getTypeOfField(i))// .leftJustified(20, ' '))
                                        .arg(m_DefaultFieldValue.value(i)));
                    break;
                }
            case Database::FieldIsBoolean :
            case Database::FieldIsInteger :
            case Database::FieldIsLongInteger :
            case Database::FieldIsUnsignedInteger:
            case Database::FieldIsUnsignedLongInteger:
            case Database::FieldIsReal :
            case Database::FieldIsTimeStamp :
                fieldLine.append(QString("%1 %2 DEFAULT %3")
                                .arg(fieldName)
                                .arg(getTypeOfField(i))// .leftJustified(20, ' '))
                                .arg(m_DefaultFieldValue.value(i)));
                break;
            case Database::FieldIsIsoUtcDateTime :
                fieldLine.append(QString("%1 %2")
                                 .arg(fieldName)
                                 .arg(getTypeOfField(i)));
                break;
            default :
                fieldLine.append(QString("%1 %2 DEFAULT '%3'")
                                .arg(fieldName)
                                .arg(getTypeOfField(i))// .leftJustified(20, ' '))
                                .arg(m_DefaultFieldValue.value(i)));
                break;

        }
        }
    }
    toReturn.append(fieldLine.join(",\n  "));

    foreach(int field, m_PrimKeys.values(tableref)) {
        int ref = index(tableref, field);
        if (m_TypeOfField.value(ref) != Database::FieldIsUniquePrimaryKey) {
            toReturn.append(QString(",\nPRIMARY KEY(%1)").arg(m_Fields.value(ref)));
        }
    }
    toReturn.append("\n);\n");

    QStringList indexes;
    for(int i = 0; i < m_DbIndexes.count(); ++i) {
        const DbIndex &idx = m_DbIndexes.at(i);
        if (idx.field.table==tableref) {
            indexes << QString("CREATE INDEX %1 ON %2 (%3);\n")
                    .arg(idx.name)
                    .arg(idx.field.tableName)
                    .arg(idx.field.fieldName);
        }
    }

    if (WarnCreateTableSqlCommand)
        qWarning() << toReturn << "\nIndexes: \n" << indexes;

    return QStringList() << toReturn << indexes;
}

QString DatabasePrivate::getTypeOfField(const int &fieldref) const
{
    QString toReturn;
    switch (Database::TypeOfField(m_TypeOfField.value(fieldref)))
    {
        case Database::FieldIsUUID :
            toReturn = "varchar(32)";
            break;
        case Database::FieldIsBoolean :
            toReturn = "int(1)";
            break;
        case Database::FieldIsLongText :
            toReturn = "varchar(2000)";
            break;
        case Database::FieldIsShortText :
            toReturn = "varchar(200)";
            break;
        case Database::FieldIsTwoChars :
            toReturn = "varchar(2)";
            break;
        case Database::FieldIsBlob :
        if (m_Driver==Database::SQLite) {
            toReturn = "blob"; // 1,000,000,000 max size
        } else if (m_Driver==Database::MySQL) {
            toReturn = "longblob"; // 4Go max size
        }
            break;
        case Database::FieldIsDate :
            toReturn = "date";
            break;
        case Database::FieldIsTime:
            toReturn = "time";
            break;
        case Database::FieldIsDateTime:
            toReturn = "datetime";
            break;
        case Database::FieldIsTimeStamp:
            toReturn = "timestamp";
            break;
        case Database::FieldIsIsoUtcDateTime:
            toReturn = "varchar(20)";
            break;
        case Database::FieldIsOneChar :
            toReturn = "varchar(1)";
            break;
        case Database::FieldIsInteger :
            toReturn = "integer";
            break;
        case Database::FieldIsUniquePrimaryKey :
            if (m_Driver==Database::SQLite) {
                toReturn = "integer not null primary key";
            } else if (m_Driver==Database::MySQL) {
                toReturn = "integer unsigned not null primary key auto_increment";
            }
            break;
        case Database::FieldIsLongInteger :
            toReturn = "int(11)";
            break;
        case Database::FieldIsUnsignedInteger:
            toReturn = "integer unsigned";
            break;
        case Database::FieldIsUnsignedLongInteger:
        if (m_Driver==Database::SQLite) {
            toReturn = "unsigned bigint";
        } else if (m_Driver==Database::MySQL) {
            toReturn = "bigint unsigned";
        }
            break;
        case Database::FieldIsReal :
            toReturn = "double";
            break;
        default : toReturn = QString::null; break;
    }
    return toReturn;
}

void Database::toTreeWidget(QTreeWidget *tree) const
{
    QFont bold;
    bold.setBold(true);
    tree->clear();

    QSqlDatabase DB = QSqlDatabase::database(d_database->m_ConnectionName);

    QTreeWidgetItem *db = new QTreeWidgetItem(tree, QStringList() << "General information");
    db->setFont(0, bold);
    db->setFirstColumnSpanned(true);
    new QTreeWidgetItem(db, QStringList() << "Connection Name" << d_database->m_ConnectionName);
    new QTreeWidgetItem(db, QStringList() << "Database Name" << DB.databaseName());
    if (DB.isOpenError()) {
        QTreeWidgetItem *e = new QTreeWidgetItem(db, QStringList() << "Error" << DB.lastError().text());
        e->setFont(0, bold);
    } else {
        new QTreeWidgetItem(db, QStringList() << "Connected" << "Without error");
    }

    QTreeWidgetItem *drv = new QTreeWidgetItem(tree, QStringList() << "Driver information");
    drv->setFont(0, bold);
    drv->setFirstColumnSpanned(true);
    new QTreeWidgetItem(drv, QStringList() << "Qt Driver" << DB.driverName());
    if (DB.driverName()=="QSQLITE") {
        new QTreeWidgetItem(drv, QStringList() << "Driver" << "SQLite");
        QString path = QFileInfo(DB.databaseName()).absolutePath();
        path = QDir(qApp->applicationDirPath()).relativeFilePath(path);
        new QTreeWidgetItem(drv, QStringList() << "Path" << path);
        new QTreeWidgetItem(drv, QStringList() << "FileName" << QFileInfo(DB.databaseName()).baseName());
    }
    else if (DB.driverName()=="QMYSQL") {
        new QTreeWidgetItem(drv, QStringList() << "Driver" << "MySQL");
        new QTreeWidgetItem(drv, QStringList() << "Host" << DB.hostName());
        new QTreeWidgetItem(drv, QStringList() << "Port" << QString::number(DB.port()));
        new QTreeWidgetItem(drv, QStringList() << "Login" << "****");
        new QTreeWidgetItem(drv, QStringList() << "Password" << "****");
    }
    else if (DB.driverName()=="QPSQL") {
        new QTreeWidgetItem(drv, QStringList() << "Driver" << "PostGreSQL");
        new QTreeWidgetItem(drv, QStringList() << "Host" << DB.hostName());
        new QTreeWidgetItem(drv, QStringList() << "Port" << QString::number(DB.port()));
        new QTreeWidgetItem(drv, QStringList() << "Login" << "****");
        new QTreeWidgetItem(drv, QStringList() << "Password" << "****");
    }

    QTreeWidgetItem *grants = new QTreeWidgetItem(tree, QStringList() << "Grants");
    grants->setFont(0, bold);
    grants->setFirstColumnSpanned(true);
    Database::Grants g = d_database->m_Grants.value(d_database->m_ConnectionName);
    if (g & Database::Grant_All) {
        new QTreeWidgetItem(grants, QStringList() << "ALL PRIVILEGES");
    } else {
        QHash<QString, int> ref;
        ref.insert("ALL PRIVILEGES", Database::Grant_All);
        ref.insert("ALTER", Database::Grant_Alter);
        ref.insert("ALTER ROUTINE", Database::Grant_AlterRoutine);
        ref.insert("CREATE", Database::Grant_Create);
        ref.insert("CREATE ROUTINE", Database::Grant_CreateRoutine);
        ref.insert("CREATE TEMPORARY TABLES", Database::Grant_CreateTmpTables);
        ref.insert("CREATE USER", Database::Grant_CreateUser);
        ref.insert("CREATE VIEW", Database::Grant_CreateView);
        ref.insert("DELETE", Database::Grant_Delete);
        ref.insert("DROP", Database::Grant_Drop);
        ref.insert("EXECUTE", Database::Grant_Execute);
        ref.insert("GRANT OPTION", Database::Grant_Options);
        ref.insert("INDEX", Database::Grant_Index);
        ref.insert("INSERT", Database::Grant_Insert);
        ref.insert("LOCK TABLES", Database::Grant_LockTables);
        ref.insert("PROCESS", Database::Grant_Process);
        ref.insert("SELECT", Database::Grant_Select);
        ref.insert("SHOW DATABASES", Database::Grant_ShowDatabases);
        ref.insert("SHOW VIEW", Database::Grant_ShowView);
        ref.insert("TRIGGER", Database::Grant_Trigger);
        ref.insert("UPDATE", Database::Grant_Update);
        foreach(const int grant, ref.values()) {
            if (g & grant)
                new QTreeWidgetItem(grants, QStringList() << ref.key(grant));
        }
    }
    tree->expandAll();
    tree->resizeColumnToContents(0);
    tree->resizeColumnToContents(1);
}

QDebug operator<<(QDebug dbg, const Utils::Database &database)
{
    QSqlDatabase DB = database.database();
    QString msg = "Database(";
    msg += QString("connection: %1, name: %2, driver: %3, open: %4, canOpen: %5")
            .arg(DB.connectionName())
            .arg(DB.databaseName())
            .arg(DB.driverName())
            .arg(DB.isOpen())
            .arg(DB.open());
    for(int i = 0; i>=0; ++i) {
        const QString &table = database.table(i);
        if (table.isNull())
            break;
        msg += QString("\n          table: %1").arg(table);
        for(int j=0; j>=0; ++j) {
            const QString &field = database.field(i, j).fieldName;
            if (field.isNull())
                break;
            msg += QString("\n            field: %1").arg(field);
        }
    }
    dbg.nospace() << msg;
    return dbg.space();
}

QDebug operator<<(QDebug dbg, const Utils::Database *database)
{
    return operator<<(dbg, *database);
}
