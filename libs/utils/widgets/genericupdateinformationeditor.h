/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef GENERICUPDATEINFORMATIONEDITOR_H
#define GENERICUPDATEINFORMATIONEDITOR_H

#include <utils/global_exporter.h>
#include <utils/genericupdateinformation.h>

#include <QWidget>

/**
 * \file ./libs/utils/widgets/genericupdateinformationeditor.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
namespace Ui {
    class GenericUpdateInformationEditor;
}

class UTILS_EXPORT GenericUpdateInformationEditor : public QWidget
{
    Q_OBJECT
public:
    explicit GenericUpdateInformationEditor(QWidget *parent = 0);
    ~GenericUpdateInformationEditor();

    void setUpdateInformation(const Utils::GenericUpdateInformation &info);

public Q_SLOTS:
    GenericUpdateInformation submit();

private Q_SLOTS:
    void on_langSelector_activated(const QString &text);

private:
    Ui::GenericUpdateInformationEditor *ui;
    GenericUpdateInformation m_info;
    QString m_PreviousLang;
};

}  // End namespace Utils

#endif // GENERICUPDATEINFORMATIONEDITOR_H
