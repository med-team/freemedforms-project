/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef QBUTTONLINEEDIT_H
#define QBUTTONLINEEDIT_H

/**
 * \file ./libs/utils/widgets/qbuttonlineedit.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

#include <utils/global_exporter.h>

#include <QString>
#include <QIcon>
#include <QLineEdit>

QT_BEGIN_NAMESPACE
class QToolButton;
class QTimer;
QT_END_NAMESPACE

namespace Utils {
namespace Internal {
class QButtonLineEditPrivate;
}

class UTILS_EXPORT QButtonLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    QButtonLineEdit(QWidget *parent = 0);
    ~QButtonLineEdit();

    void setDelayedSignals(bool state);

    void setLeftButton(QToolButton *button);
    void setRightButton(QToolButton *button);

    void setRightIcon(const QIcon &icon);
    void setLeftIcon(const QIcon &icon);

    void clearLeftButton();
    void clearRightButton();

    void setRoundedCorners();

    void setEditorPlaceholderText(const QString &placeholder);

    void setTranslatableExtraToolTip(const QString &trContext, const QString &translatable);
    void setExtraToolTip(const QString &nonTranslatable);
    void setExtraStyleSheet(const QString &extraCss);
    void clearExtraStyleSheet();

protected:
    void keyPressEvent(QKeyEvent *event);
    void resizeEvent(QResizeEvent *);
    void changeEvent(QEvent *e);

private Q_SLOTS:
    void emitTextChangedSignal();
    void leftTrig(QAction *action);

private:
    Internal::QButtonLineEditPrivate *d_qble;
};

}  // End namespace Utils

#endif
