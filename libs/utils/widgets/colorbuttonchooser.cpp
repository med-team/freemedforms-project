/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "colorbuttonchooser.h"

#include <QColorDialog>

using namespace Utils;

ColorButtonChooser::ColorButtonChooser(QWidget *parent)
        : QPushButton(parent)
{
    connect(this, SIGNAL(clicked()), this, SLOT(onClicked()));
    setMinimumSize(22,22);
    setMaximumSize(22,22);
    setColor(QColor(Qt::black));
}

ColorButtonChooser::ColorButtonChooser(const QColor& color, QWidget* parent)
        : QPushButton(parent)
{
    connect(this, SIGNAL(clicked()), this, SLOT(onClicked()));
    setColor(color);
}

ColorButtonChooser::~ColorButtonChooser()
{
}


const QColor &ColorButtonChooser::color() const
{
    return m_Color;
}

void ColorButtonChooser::setColor(const QColor &color)
{
    m_Color = color;
    QPixmap pixmap(iconSize());
    pixmap.fill(m_Color);
    setIcon(QIcon(pixmap));
}

void ColorButtonChooser::onClicked()
{
    bool ok;
    const QRgb rgb = QColorDialog::getRgba(m_Color.rgba(), &ok, window());
    if (ok) {
        QColor color = QColor::fromRgba(rgb);
        setColor(color);
    }
}
