/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef COUNTRYCOMBOBOX_H
#define COUNTRYCOMBOBOX_H

#include <utils/global_exporter.h>
#include <QComboBox>

/**
 * \file ./libs/utils/widgets/countrycombobox.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {

class UTILS_EXPORT CountryComboBox : public QComboBox
{
    Q_OBJECT
    Q_PROPERTY(QLocale::Country currentCountry READ currentCountry WRITE setCurrentCountry NOTIFY currentCountryChanged)
    Q_PROPERTY(QString currentIsoCountry READ currentIsoCountry WRITE setCurrentIsoCountry NOTIFY currentIsoCountryChanged)

public:
    explicit CountryComboBox(QWidget *parent = 0);
    void setFlagPath(const QString &absPath);
    virtual void initialize();
    virtual void addCountry(QLocale::Country country);
    virtual void removeCountry(QLocale::Country country);

    QLocale::Country currentCountry() const;
    QString currentIsoCountry() const;
    QString currentCountryName() const;

public Q_SLOTS:
    void setCurrentIsoCountry(const QString &isoCode);
    void setCurrentCountry(QLocale::Country country);

Q_SIGNALS:
    void currentCountryChanged(QLocale::Country country);
    void currentIsoCountryChanged(const QString &name);

private:
    QString m_FlagPath;
private Q_SLOTS:
    void on_currentIndexChanged(int index);
};

}  // End namespace Utils

#endif // COUNTRYCOMBOBOX_H
