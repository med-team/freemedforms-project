/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef COMBOWITHFANCYBUTTON_H
#define COMBOWITHFANCYBUTTON_H

#include <utils/global_exporter.h>
#include <QComboBox>

QT_BEGIN_NAMESPACE
class QModelIndex;
class QTreeView;
class QSettings;
class QString;
class QStringListModel;
QT_END_NAMESPACE

#include <QDebug>

/**
 * \file ./libs/utils/widgets/combowithfancybutton.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
namespace Internal {
class ItemDelegate;
class StringModel;
}

class UTILS_EXPORT ComboWithFancyButton : public QComboBox
{
    Q_OBJECT

public:
    ComboWithFancyButton(QWidget *parent = 0);

    void setRemoveItems(bool state);
    void setMoveItems(bool state);

    void fancyAddItems(const QStringList &list, const QVariant &userData = QVariant());
    void fancyAddItem(const QString &text, const QVariant &userData = QVariant());
    QStringList fancyItems(const QVariant &userData = QVariant()) const;

    void setRemoveLightIcon(const QIcon &icon);
    void setMoveUpLightIcon(const QIcon &icon);
    void setMoveDownLightIcon(const QIcon &icon);

public Q_SLOTS:
    void fancyClear();

protected Q_SLOTS:
    void handlePressed(const QModelIndex &index);

protected:
    // From virtual members of QComboBox
    void showPopup();
    void hidePopup();

private:
    Internal::ItemDelegate *delegate;
    QTreeView *view;
    QSettings *m_Settings;
    Internal::StringModel *stringModel;
    QString m_Key;
    bool m_ignoreHide, m_editableState;
    int m_Index;
    QString m_Text;
};

}  // End namespace Utils

#endif // COMBOWITHFANCYBUTTON_H
