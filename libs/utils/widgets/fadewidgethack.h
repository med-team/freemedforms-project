/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef UTILS_FADERWIDGET_H
#define UTILS_FADERWIDGET_H

#include <utils/global_exporter.h>

#include <QWidget>
#include <QColor>

/**
 * \file ./libs/utils/widgets/fadewidgethack.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {

class UTILS_EXPORT FaderWidget : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QColor fadeColor READ fadeColor WRITE setFadeColor)
    Q_PROPERTY(int fadeDuration READ fadeDuration WRITE setFadeDuration)

public:
    enum FadeType {
        FadeOutParentWidget,
        FadeInParentWidget
    };

    FaderWidget(QWidget *parent);

    void setFadeType(FadeType type) {_type = type;}

    QColor fadeColor() const { return startColor; }
    void setFadeColor(const QColor &newColor) { startColor = newColor; }

    int fadeDuration() const { return duration; }
    void setFadeDuration(int milliseconds) { duration = milliseconds; }

    void start();

protected:
    void paintEvent(QPaintEvent *event);

Q_SIGNALS:
    void fadeDone();

private:
    QTimer *timer;
    QColor startColor;
    int currentAlpha;
    int duration;
    FadeType _type;
};

}  // namespace Utils

#endif // UTILS_FADERWIDGET_H
