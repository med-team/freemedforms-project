/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#ifndef SCROLLINGWIDGET_H
#define SCROLLINGWIDGET_H

#include <utils/global_exporter.h>

#include <QWidget>
#include <QTimerEvent>
#include <QPaintEvent>
#include <QShowEvent>
#include <QHideEvent>
/**
 * \file ./libs/utils/widgets/scrollingwidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
namespace Internal {
class ScrollingWidgetPrivate;
}

class UTILS_EXPORT ScrollingWidget : public QWidget
{
    Q_OBJECT
    Q_PROPERTY( QString text READ text WRITE setText)
    Q_PROPERTY( int timerDelay READ timerDelay WRITE setTimerDelay )

public:
    enum Direction {
        LeftToRight = 0,
        RightToLeft,
        TopToBottom,
        BottomToTop
    };

    ScrollingWidget( QWidget *parent = 0 );
    ~ScrollingWidget();

    void setText( const QString &text);
    QString text();
    QSize sizeHint() const;

    void setDirection( int direction );

    void setTimerDelay( const int delay ); // ms
    int timerDelay(); // ms

private:
    void paintEvent( QPaintEvent *event);
    void timerEvent( QTimerEvent *event);
    void showEvent( QShowEvent *event);
    void hideEvent( QHideEvent *event);

private:
    Internal::ScrollingWidgetPrivate *d;
};

}  // End Utils

#endif // SCROLLINGWIDGET_H
