/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "loginwidget.h"

#include <translationutils/constants.h>
#include <translationutils/trans_database.h>

#include "ui_loginwidget.h"

using namespace Utils;
using namespace Trans::ConstantTranslations;

LoginWidget::LoginWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoginWidget)
{
    ui->setupUi(this);
    layout()->setMargin(2);
    ui->loginLabel->setText(tkTr(Trans::Constants::LOGIN));
    ui->passwordLabel->setText(tkTr(Trans::Constants::PASSWORD));
}

LoginWidget::~LoginWidget()
{
    delete ui;
}

void LoginWidget::setToggleViewIcon(const QString &fullAbsPath)
{
    ui->login->setIcon(QIcon(fullAbsPath));
    ui->password->setIcon(QIcon(fullAbsPath));
}

void LoginWidget::focusLogin()
{
    this->setFocus();
    ui->login->setFocus();
}

void LoginWidget::toggleLoginEcho(bool visible)
{
    if (visible)
        ui->login->setEchoMode(QLineEdit::Normal);
    else
        ui->login->setEchoMode(QLineEdit::Password);
}

void LoginWidget::togglePasswordEcho(bool visible)
{
    if (visible)
        ui->password->setEchoMode(QLineEdit::Normal);
    else
        ui->password->setEchoMode(QLineEdit::Password);
}

QString LoginWidget::login() const
{
    return ui->login->text();
}

QString LoginWidget::password() const
{
    return ui->password->text();
}

void LoginWidget::changeEvent(QEvent *e)
{
    if (e->type()==QEvent::LanguageChange) {
        ui->loginLabel->setText(tkTr(Trans::Constants::LOGIN));
        ui->passwordLabel->setText(tkTr(Trans::Constants::PASSWORD));
    }
}
