/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "lineeditechoswitcher.h"

#include <QPushButton>
#include <QHBoxLayout>
#include <QApplication>
#include <QToolButton>

using namespace Utils;
using namespace Utils::Internal;

namespace Utils {
namespace Internal {
class LineEditEchoSwitcherPrivate
{
public:
    LineEditEchoSwitcherPrivate() :
        m_ToolButton(0)
    {
    }

    void switchEchoMode(QLineEdit *l)
    {
        if (l->echoMode() == QLineEdit::Normal)
            l->setEchoMode(QLineEdit::Password);
        else
            l->setEchoMode(QLineEdit::Normal);
    }

    QToolButton *m_ToolButton;
};
}  // End Internal
}  // End Utils

LineEditEchoSwitcher::LineEditEchoSwitcher(QWidget *parent) :
    QButtonLineEdit(parent),
    d(0)
{
    setObjectName("LineEditEchoSwitcher");
    d = new LineEditEchoSwitcherPrivate;
    d->m_ToolButton = new QToolButton(this);
    d->m_ToolButton->setToolTip(QCoreApplication::translate("LineEditEchoSwitcher","Display/Hide text"));
    d->m_ToolButton->setFocusPolicy(Qt::ClickFocus);
    setRightButton(d->m_ToolButton);
    connect(d->m_ToolButton, SIGNAL(clicked()), this, SLOT(toogleEchoMode()));
}

LineEditEchoSwitcher::~LineEditEchoSwitcher()
{
    if (d) {
        delete d;
        d = 0;
    }
}


void LineEditEchoSwitcher::toogleEchoMode()
{
    d->switchEchoMode(this);
}

void LineEditEchoSwitcher::setIcon(const QIcon &icon)
{
    d->m_ToolButton->setIcon(icon);
}

void LineEditEchoSwitcher::changeEvent(QEvent *e)
{
    if (e->type()==QEvent::LanguageChange) {
        d->m_ToolButton->setToolTip(QCoreApplication::translate("LineEditEchoSwitcher","Display/Hide text"));
    }
}
