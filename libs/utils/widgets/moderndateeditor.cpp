/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Christian A. Reiter, <christian.a.reiter@gmail.com> *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/



#include "moderndateeditor.h"

#include <utils/datevalidator.h>
#include <translationutils/constants.h>
#include <translationutils/trans_datetime.h>
#include <translationutils/trans_agenda.h>

#include <QAction>
#include <QToolButton>
#include <QTimer>
#include <QDebug>

using namespace Utils;
using namespace Internal;
using namespace Trans::ConstantTranslations;

namespace Utils {
namespace Internal {
class ModernDateEditorPrivate
{
public:
    ModernDateEditorPrivate(ModernDateEditor *parent):
        _rightButton(0),
        _leftButton(0),
        aShortDisplay(0),
        aLongDisplay(0),
        aNumericDisplay(0),
        aToday(0),
        _validator(0),
        q(parent)
    {}

    ~ModernDateEditorPrivate()
    {}

    void createRightButton(const QString &fullAbsPath)
    {
        if (!_rightButton) {
            _rightButton = new QToolButton(q);
            _rightButton->setFocusPolicy(Qt::ClickFocus);
            _rightButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
            _rightButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
            q->retranslate();
            _rightButton->resize(20, 20);
            q->setRightButton(_rightButton);
            QObject::connect(_rightButton, SIGNAL(clicked()), q, SLOT(clear()));
        }
        _rightButton->setIcon(QIcon(fullAbsPath));
    }

    void createLeftButton(const QString &fullAbsPath)
    {
        if (!_leftButton) {
            _leftButton = new QToolButton(q);
            _leftButton->setPopupMode(QToolButton::InstantPopup);
            QAction *sep = new QAction(q);
            sep->setSeparator(true);
            aShortDisplay = new QAction(q);
            aLongDisplay = new QAction(q);
            aNumericDisplay = new QAction(q);
            aToday = new QAction(q);
            _leftButton->addAction(aToday);
            _leftButton->addAction(sep);
            _leftButton->addAction(aLongDisplay);
            _leftButton->addAction(aShortDisplay);
            _leftButton->addAction(aNumericDisplay);
            _leftButton->setDefaultAction(aLongDisplay);
            q->retranslate();
            _leftButton->setIcon(QIcon(fullAbsPath));
            q->setLeftButton(_leftButton);
            QObject::connect(_leftButton, SIGNAL(triggered(QAction*)), q, SLOT(onLeftButtonActionTriggered(QAction*)));
        }
        _leftButton->setIcon(QIcon(fullAbsPath));
        aToday->setIcon(QIcon(fullAbsPath));
        aShortDisplay->setIcon(QIcon(fullAbsPath));
        aLongDisplay->setIcon(QIcon(fullAbsPath));
        aNumericDisplay->setIcon(QIcon(fullAbsPath));
    }

public:
    QDate m_date;
    QDate m_maximumDate;
    QDate m_minimumDate;
    QToolButton *_rightButton, *_leftButton;
    QAction *aShortDisplay, *aLongDisplay, *aNumericDisplay, *aToday;
    DateValidator *_validator;
    QString _defaultEditingFormat;

private:
    ModernDateEditor *q;
};
} // namespace Internal
} // namespace Utils

ModernDateEditor::ModernDateEditor(QWidget *parent) :
    QButtonLineEdit(parent),
    d_de(new Internal::ModernDateEditorPrivate(this))
{
    init();
}

ModernDateEditor::ModernDateEditor(const QDate &date, QWidget *parent) :
    QButtonLineEdit(parent),
    d_de(new Internal::ModernDateEditorPrivate(this))
{
    init(date);
}

ModernDateEditor::~ModernDateEditor()
{
    if (d_de) {
        delete d_de;
    }
    d_de = 0;
}

void ModernDateEditor::init(const QDate& date, const QDate& maximumDate, const QDate& minimumDate)
{
    d_de->m_date = date;
    d_de->m_minimumDate = minimumDate;
    d_de->m_maximumDate = maximumDate;

    d_de->_validator = new DateValidator(this);
    setValidator(d_de->_validator);

    d_de->_defaultEditingFormat = tkTr(Trans::Constants::DATEFORMAT_FOR_EDITOR);
}

void ModernDateEditor::setClearIcon(const QString &fullAbsPath)
{
    d_de->createRightButton(fullAbsPath);
}

void ModernDateEditor::setDateIcon(const QString &fullAbsPath)
{
    d_de->createLeftButton(fullAbsPath);
}

void ModernDateEditor::setDate(const QDate &date)
{
    if (date.isNull()) {
        clear();
        return;
    }
    QDate oldDate = d_de->m_date;
    d_de->m_date = date;
    d_de->_validator->setDate(date);
    if (oldDate != date) {
        Q_EMIT dateChanged(d_de->m_date);
        Q_EMIT dateChanged();
    }
    updateDisplayText();
}

QDate ModernDateEditor::date() const
{
    return d_de->m_date;
}

/** \brief sets the internal date of the widget to NULL
 *
 *  If internal date is not already NULL, the widget emits the dateChanged(QDate &) signal. */
void ModernDateEditor::clear()
{
    if(!d_de->m_date.isNull()) {
        d_de->m_date = QDate();
        Q_EMIT dateChanged(d_de->m_date);
        Q_EMIT dateChanged();
    }
    d_de->_validator->setDate(d_de->m_date);
    setText("");
    updatePlaceHolder();
}


void ModernDateEditor::focusOutEvent(QFocusEvent *event)
{
    setValidator(0);
    QString val = text();
    int pos = 0;
    if (d_de->_validator->validate(val, pos) == QValidator::Intermediate)
        d_de->_validator->fixup(val);
    setText(val);
    d_de->m_date = d_de->_validator->date();
    if (d_de->m_date.isValid()) {
        clearExtraStyleSheet();
    } else {
        if (text().isEmpty()) {
            clearExtraStyleSheet();
        } else {
            setExtraStyleSheet(QString("background: %1").arg("#f66"));
        }
    }
    updateDisplayText();
    Q_EMIT dateChanged(d_de->m_date);
    Q_EMIT dateChanged();
    QButtonLineEdit::focusOutEvent(event);
}

void ModernDateEditor::focusInEvent(QFocusEvent *event)
{
    if (d_de->m_date.isValid()) {
        setText(d_de->m_date.toString(d_de->_defaultEditingFormat));
    } else {
    }
    setValidator(d_de->_validator);
    d_de->_validator->setDate(d_de->m_date);
    QButtonLineEdit::focusInEvent(event);
}

/** \brief sets the internal date of the widget to the given string
 *
 * Tries to parse the string using QDate::fromString. If it is an invalid date string,
 * the date field is set to NULL. This method is called when a valid date string was
 * entered and the user presses Enter or the widget looses focus.
 */
void ModernDateEditor::setDateString(QString dateString)
{
    int pos = 0;
    d_de->_validator->validate(dateString, pos);
    QDate previousDate = d_de->m_date;
    d_de->m_date = d_de->_validator->date();
    if (d_de->m_date.isValid()) {
        if (d_de->m_date != previousDate) {
            Q_EMIT dateChanged(d_de->m_date);
            Q_EMIT dateChanged();
        }
    }
    updateDisplayText();
}

void ModernDateEditor::updateDisplayText()
{
    if (hasFocus()) {
        return;
    }

    if (d_de->m_date.isValid()) {
        if (d_de->_leftButton) {
            setText(d_de->m_date.toString(d_de->_leftButton->defaultAction()->data().toString()));
        } else {
            setText(d_de->m_date.toString(tkTr(Trans::Constants::DATEFORMAT_FOR_EDITOR)));
        }
    } else {
    }
}

void ModernDateEditor::onLeftButtonActionTriggered(QAction *a)
{
    if (a==d_de->aToday) {
        setDate(QDate::currentDate());
        QTimer::singleShot(10, d_de->aLongDisplay, SLOT(trigger()));
    }
    updateDisplayText();
}

void ModernDateEditor::updatePlaceHolder()
{
}

void ModernDateEditor::retranslate()
{
    updatePlaceHolder();
    if (d_de->aLongDisplay) {
        d_de->aLongDisplay->setText(tkTr(Trans::Constants::SHOW_LONG_FORMAT));
        d_de->aLongDisplay->setToolTip(d_de->aLongDisplay->text());
        d_de->aLongDisplay->setData(QLocale().dateFormat(QLocale::LongFormat));
    }
    if (d_de->aShortDisplay) {
        d_de->aShortDisplay->setText(tkTr(Trans::Constants::SHOW_SHORT_FORMAT));
        d_de->aShortDisplay->setToolTip(d_de->aShortDisplay->text());
        d_de->aShortDisplay->setData(QLocale().dateFormat(QLocale::ShortFormat));
    }
    if (d_de->aNumericDisplay) {
        d_de->aNumericDisplay->setText(tkTr(Trans::Constants::SHOW_NUMERIC_FORMAT));
        d_de->aNumericDisplay->setToolTip(d_de->aNumericDisplay->text());
        d_de->aNumericDisplay->setData(tkTr(Trans::Constants::DATEFORMAT_FOR_EDITOR));
    }
    if (d_de->aToday) {
        d_de->aToday->setText(tkTr(Trans::Constants::TODAY));
        d_de->aToday->setToolTip(d_de->aToday->text());
    }
    d_de->_validator->translateFormats();
}

void ModernDateEditor::changeEvent(QEvent *e)
{
    if (e->type()==QEvent::LanguageChange) {
        retranslate();
    }
    QButtonLineEdit::changeEvent(e);
}
