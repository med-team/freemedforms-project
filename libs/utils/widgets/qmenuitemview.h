/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef UTILS_QMENUITEMVIEW_H
#define UTILS_QMENUITEMVIEW_H

#include "../global_exporter.h"
#include <QMenu>

QT_BEGIN_NAMESPACE
class QModelIndex;
class QAbstractItemModel;
QT_END_NAMESPACE

/**
 * \file ./libs/utils/widgets/qmenuitemview.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
namespace Internal {
class QMenuItemViewPrivate;
} // namespace Internal

class UTILS_EXPORT QMenuItemView : public QMenu
{
    Q_OBJECT
    
public:
    explicit QMenuItemView(QWidget *parent = 0);
    ~QMenuItemView();

    void setModel(QAbstractItemModel *model);
    QAbstractItemModel *model() const;

    void setRootIndex(const QModelIndex & index);
    QModelIndex rootIndex() const;

Q_SIGNALS:
    void hovered(const QString &text) const;
    void triggered(const QModelIndex &index) const;

private Q_SLOTS:
    void aboutToShow();
    void triggered(QAction *action);
    void hovered(QAction *action);

private:
    Internal::QMenuItemViewPrivate *d;
};

} // namespace Utils

#endif  // UTILS_QMENUITEMVIEW_H

