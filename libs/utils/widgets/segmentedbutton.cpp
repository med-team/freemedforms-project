/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "segmentedbutton.h"

#include <utils/global.h>

#include <QHBoxLayout>
#include <QPushButton>
#include <QFrame>

#include <QDebug>

using namespace Utils;

namespace {

const char *const FIRST_BUTTON =
        "border-top-left-radius: 8px;"
        "border-bottom-left-radius: 8px;"
        ;

const char *const MIDDLE_BUTTON = ""
        ;

const char *const LAST_BUTTON =
        "border-top-right-radius: 8px;"
        "border-bottom-right-radius: 8px;"
        ;

const char *const BUTTON_CSS =
        "QPushButton {"
        "border: 1px outset #777;"
        "background: qradialgradient(cx: 0.3, cy: -0.4,"
        "fx: 0.3, fy: -0.4,"
        "radius: 1.35, stop: 0 #fff, stop: 1 #eee);"
        "color: #333;"
        "%1"
        "padding: 3px;"
        "}"

        "QPushButton:hover {"
        "background: qradialgradient(cx: 0.4, cy: -0.1,"
        "fx: 0.4, fy: -0.1,"
        "radius: 1.35, stop: 0 #fff, stop: 1 #ededed);"
        "}"

        "QPushButton:pressed {"
        "border: 1px inset #666;"
        "background: qradialgradient(cx: 0.3, cy: -0.4,"
        "fx: 0.3, fy: -0.4,"
        "radius: 1.35, stop: 0 #fff, stop: 1 #aaa);"
        "}"

        "QPushButton:checked {"
        "border: 1px inset #666;"
        "background: qradialgradient(cx: 0.3, cy: -0.4,"
        "fx: 0.3, fy: -0.4,"
        "radius: 1.35, stop: 0 #fff, stop: 1 #bbb);"
        "color: darkBlue;"
        "}"

        ;

}

SegmentedButton::SegmentedButton(QWidget *parent) :
    QWidget(parent),
    _first(0),
    _last(0)
{
    QHBoxLayout *lay = _buttonLayout = new QHBoxLayout(this);
    lay->setMargin(0);
    if (Utils::isRunningOnMac()) {
        lay->setSpacing(11);
    } else if (Utils::isRunningOnLinux() || Utils::isRunningOnFreebsd()) {
        lay->setSpacing(0);
    } else {
        lay->setSpacing(0);
    }
    setLayout(lay);
}

void SegmentedButton::setFirstButton(QPushButton *but)
{
    but->setFocusPolicy(Qt::NoFocus);
    but->setStyleSheet(QString(::BUTTON_CSS).arg(::FIRST_BUTTON));
    _buttonLayout->addWidget(but);
    _first = but;
}
void SegmentedButton::addMiddleButton(QPushButton *but)
{
    but->setFocusPolicy(Qt::NoFocus);
    but->setStyleSheet(QString(::BUTTON_CSS).arg(::MIDDLE_BUTTON));
    _buttonLayout->addWidget(but);
    _buttons << but;
}
void SegmentedButton::setLastButton(QPushButton *but)
{
    but->setFocusPolicy(Qt::NoFocus);
    but->setStyleSheet(QString(::BUTTON_CSS).arg(::LAST_BUTTON));
    _buttonLayout->addWidget(but);
    _last = but;
}

void SegmentedButton::setAutoExclusive(bool state)
{
    if (_first)
        _first->setAutoExclusive(state);
    if (_last)
        _last->setAutoExclusive(state);
    for(int i=0; i < _buttons.count(); ++i)
        _buttons.at(i)->setAutoExclusive(state);
}

void SegmentedButton::computeSizes()
{
    int width = 0;
    if (_first)
        width = _first->width();
    if (_last)
        width = qMax(width, _last->width());
    for(int i=0; i < _buttons.count(); ++i) {
        width = qMax(width, _buttons.at(i)->width());
    }

    if (_first)
        _first->setMinimumWidth(width);
    if (_last)
        _last->setMinimumWidth(width);
    for(int i=0; i < _buttons.count(); ++i)
        _buttons.at(i)->setMinimumWidth(width);
}
