/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef UPPERCASEVALIDATOR_H
#define UPPERCASEVALIDATOR_H

#include <utils/global_exporter.h>
#include <QValidator>
QT_BEGIN_NAMESPACE
class QString;
QT_END_NAMESPACE

/**
 * \file ./libs/utils/widgets/uppercasevalidator.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

/**
  \class Utils::UpperCaseValidator
  Use this validator in QLineEdit when you want the user input to always be uppercase.
*/

namespace Utils {

class UTILS_EXPORT UpperCaseValidator : public QValidator
{
public:
    UpperCaseValidator(QObject *parent);
    ~UpperCaseValidator();

    QValidator::State validate(QString &text, int &pos) const;
};

class UTILS_EXPORT CapitalizationValidator : public QValidator
{
public:
    CapitalizationValidator(QObject *parent);
    ~CapitalizationValidator();

    QValidator::State validate(QString &text, int &pos) const;
};

}

#endif // UPPERCASEVALIDATOR_H
