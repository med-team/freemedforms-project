/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, <eric.maeker@gmail.com>,                             *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PERIODSELECTORTOOLBUTTON_H
#define PERIODSELECTORTOOLBUTTON_H

#include <utils/global_exporter.h>
#include <QToolButton>

namespace Utils {
namespace Internal {
class PeriodSelectorToolButtonPrivate;
}  // namespace Internal

class UTILS_EXPORT PeriodSelectorToolButton : public QToolButton
{
    Q_OBJECT
public:
    explicit PeriodSelectorToolButton(QWidget *parent = 0);
    ~PeriodSelectorToolButton();

    void setMainMenuTitle(const QString &translatableTitle, const QString &translationContext);
    void setStartPeriodsAt(const int transConstantsTimeEnumValue);

Q_SIGNALS:
    void periodSelected(int period, int value);

private Q_SLOTS:
    void _actionTriggered(QAction *a);

private:
    void changeEvent(QEvent *e);

private:
    Internal::PeriodSelectorToolButtonPrivate *d;
};

}  // namespace Utils

#endif // PERIODSELECTORTOOLBUTTON_H
