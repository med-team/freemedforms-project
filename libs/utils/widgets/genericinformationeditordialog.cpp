/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "genericinformationeditordialog.h"
#include "ui_genericinformationeditordialog.h"

using namespace Utils;

GenericInformationEditorDialog::GenericInformationEditorDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GenericInformationEditorDialog)
{
    ui->setupUi(this);
}

GenericInformationEditorDialog::~GenericInformationEditorDialog()
{
    delete ui;
}

void GenericInformationEditorDialog::setDescription(const Utils::GenericDescription &desc)
{
    m_desc = desc;
    ui->widget->setDescription(desc);
}

Utils::GenericDescription GenericInformationEditorDialog::submit()
{
    return Utils::GenericDescription();
}

void GenericInformationEditorDialog::done(int r)
{
    QDialog::done(r);
}
