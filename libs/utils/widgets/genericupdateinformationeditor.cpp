/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "genericupdateinformationeditor.h"

#include <translationutils/constants.h>

#include "ui_genericupdateinformationeditor.h"

using namespace Utils;
using namespace Trans::ConstantTranslations;

GenericUpdateInformationEditor::GenericUpdateInformationEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GenericUpdateInformationEditor)
{
    ui->setupUi(this);
    ui->date->setDisplayFormat(tkTr(Trans::Constants::DATEFORMAT_FOR_EDITOR));
    ui->langSelector->addItems(QStringList() << "xx" << "en" << "fr" << "de" << "es");
}

GenericUpdateInformationEditor::~GenericUpdateInformationEditor()
{
    delete ui;
}

void GenericUpdateInformationEditor::setUpdateInformation(const Utils::GenericUpdateInformation &info)
{
    m_info = info;

    on_langSelector_activated(ui->langSelector->currentText());
}

void GenericUpdateInformationEditor::on_langSelector_activated(const QString &text)
{
    if (m_PreviousLang.isEmpty()) {
        m_PreviousLang = ui->langSelector->currentText();
    } else {
        m_info.setText(ui->updateText->toHtml(), m_PreviousLang);
        m_PreviousLang = text;
    }
    ui->updateText->setHtml(m_info.text(text));
}

GenericUpdateInformation GenericUpdateInformationEditor::submit()
{
    m_info.setFromVersion(ui->from->text());
    m_info.setToVersion(ui->to->text());
    m_info.setIsoDate(ui->date->date().toString(Qt::ISODate));
    m_info.setText(ui->updateText->toHtml(), ui->langSelector->currentText());
    return m_info;
}
