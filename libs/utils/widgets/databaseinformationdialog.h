/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef UTILS_DATABASEINFORMATIONDIALOG_H
#define UTILS_DATABASEINFORMATIONDIALOG_H

#include <utils/global_exporter.h>
#include <QDialog>
QT_BEGIN_NAMESPACE
class QTreeWidget;
QT_END_NAMESPACE

/**
 * \file ./libs/utils/widgets/databaseinformationdialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
class Database;
namespace Internal {
class DatabaseInformationDialogPrivate;
} // namespace Internal

class UTILS_EXPORT DatabaseInformationDialog : public QDialog
{
    Q_OBJECT
public:
    explicit DatabaseInformationDialog(QWidget *parent = 0);
    ~DatabaseInformationDialog();

    void setTitle(const QString &title);
    bool setDatabase(const Utils::Database &database);

    QTreeWidget *getHeaderTreeWidget();
    QTreeWidget *getDescriptionTreeWidget();

public Q_SLOTS:
    bool saveContent();

private:
    Internal::DatabaseInformationDialogPrivate *d;
};

} // namespace Utils

#endif  // UTILS_DATABASEINFORMATIONDIALOG_H

