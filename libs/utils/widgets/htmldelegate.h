/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef UTILS_HTMLDELEGATE_H
#define UTILS_HTMLDELEGATE_H

#include <utils/global_exporter.h>
#include <QStyledItemDelegate>

/**
 * \file ./libs/utils/widgets/htmldelegate.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
namespace Internal {
class HtmlDelegatePrivate;
} // namespace Internal

class UTILS_EXPORT HtmlDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit HtmlDelegate(QObject *parent = 0);
    ~HtmlDelegate();

protected:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;

private Q_SLOTS:
    void treeView_indexDataChanged();

private:
    Internal::HtmlDelegatePrivate *d_html;
};

} // namespace Utils

#endif // UTILS_HTMLDELEGATE_H
