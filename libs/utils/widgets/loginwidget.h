/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef UTILS_LOGINWIDGET_H
#define UTILS_LOGINWIDGET_H

#include <utils/global_exporter.h>
#include <QWidget>

/**
 * \file ./libs/utils/widgets/loginwidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {

namespace Ui {
    class LoginWidget;
}

class UTILS_EXPORT LoginWidget : public QWidget
{
    Q_OBJECT
public:
    explicit LoginWidget(QWidget *parent = 0);
    ~LoginWidget();

    void setToggleViewIcon(const QString &fullAbsPath);
    void focusLogin();

    void toggleLoginEcho(bool visible);
    void togglePasswordEcho(bool visible);

    QString login() const;
    QString password() const;


private:
    void changeEvent(QEvent *e);

private:
    Ui::LoginWidget *ui;
};

} // namespace Utils

#endif // UTILS_LOGINWIDGET_H
