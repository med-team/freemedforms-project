/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef SEGMENTEDBUTTON_H
#define SEGMENTEDBUTTON_H

#include <utils/global_exporter.h>
#include <QWidget>

QT_BEGIN_NAMESPACE
class QPushButton;
class QHBoxLayout;
QT_END_NAMESPACE

namespace Utils {

class UTILS_EXPORT SegmentedButton : public QWidget
{
    Q_OBJECT
public:
    explicit SegmentedButton(QWidget *parent = 0);

    void setFirstButton(QPushButton *but);
    void addMiddleButton(QPushButton *but);
    void setLastButton(QPushButton *but);

    void setAutoExclusive(bool state);
    void computeSizes();

private:
    QHBoxLayout *_buttonLayout;
    QPushButton *_first, *_last;
    QList<QPushButton*> _buttons;
};

}

#endif // SEGMENTEDBUTTON_H
