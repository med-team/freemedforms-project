/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef UTILS_BASICLOGINDIALOG_H
#define UTILS_BASICLOGINDIALOG_H

#include <utils/global_exporter.h>
#include <QDialog>
QT_BEGIN_NAMESPACE
class QPushButton;
class QTextBrowser;
QT_END_NAMESPACE

/**
 * \file ./libs/utils/widgets/basiclogindialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {
namespace Ui {
class BasicLoginDialog;
}

class UTILS_EXPORT BasicLoginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit BasicLoginDialog(QWidget *parent = 0);
    ~BasicLoginDialog();

    void setTitle(const QString &title);
    void setToggleViewIcon(const QString &fullAbsPath);
    void setHtmlExtraInformation(const QString &html);
    void focusLogin();
    QString login() const;
    QString password() const;

private Q_SLOTS:
    void onMoreClicked();

private:
    Ui::BasicLoginDialog *ui;
    QPushButton *_more;
    QTextBrowser *_moreBrowser;
};

} // namespace Utils
#endif // UTILS_BASICLOGINDIALOG_H
