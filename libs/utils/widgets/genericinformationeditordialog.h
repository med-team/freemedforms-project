/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef UTILS_GENERICINFORMATIONEDITORDIALOG_H
#define UTILS_GENERICINFORMATIONEDITORDIALOG_H

#include <utils/global_exporter.h>
#include <utils/genericdescription.h>

#include <QDialog>

/**
 * \file ./libs/utils/widgets/genericinformationeditordialog.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {

namespace Ui {
    class GenericInformationEditorDialog;
}

class UTILS_EXPORT GenericInformationEditorDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GenericInformationEditorDialog(QWidget *parent = 0);
    ~GenericInformationEditorDialog();

    void setDescription(const Utils::GenericDescription &desc);

private:
    Utils::GenericDescription submit();
    void done(int r);

private:
    Ui::GenericInformationEditorDialog *ui;
    Utils::GenericDescription m_desc;
};


} // namespace Utils


#endif // UTILS_GENERICINFORMATIONEDITORDIALOG_H
