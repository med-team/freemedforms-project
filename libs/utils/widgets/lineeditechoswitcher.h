/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef LINEEDITECHOSWITCHER_H
#define LINEEDITECHOSWITCHER_H

#include <utils/global_exporter.h>
#include <utils/widgets/qbuttonlineedit.h>

/**
 * \file ./libs/utils/widgets/lineeditechoswitcher.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

#include <QWidget>
#include <QLineEdit>
#include <QEvent>
#include <QIcon>

namespace Utils {
namespace Internal {
class LineEditEchoSwitcherPrivate;
}

class UTILS_EXPORT LineEditEchoSwitcher : public QButtonLineEdit
{
    Q_OBJECT
    Q_PROPERTY( QString text READ text WRITE setText USER true)

public:
    LineEditEchoSwitcher(QWidget *parent = 0);
    ~LineEditEchoSwitcher();

    void setIcon(const QIcon &icon);

public Q_SLOTS:
    void toogleEchoMode();

protected:
    void changeEvent(QEvent *e);

private:
    Internal::LineEditEchoSwitcherPrivate *d;
};

}  // End Utils

#endif // LINEEDITECHOSWITCHER_H
