/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Code inspired of fresh/pColorButton : MonkeyStudio                    *
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef COLORBUTTONCHOOSER_H
#define COLORBUTTONCHOOSER_H

#include <utils/global_exporter.h>

#include <QPushButton>
#include <QColor>

/**
 * \file ./libs/utils/widgets/colorbuttonchooser.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {

class UTILS_EXPORT ColorButtonChooser : public QPushButton
{
    Q_OBJECT
    Q_PROPERTY(QColor color READ color WRITE setColor)

public:
    ColorButtonChooser(QWidget *parent = 0);
    ColorButtonChooser(const QColor &color, QWidget *parent = 0);
    virtual ~ColorButtonChooser();

    const QColor &color() const;

public Q_SLOTS:
    void setColor(const QColor &color);

protected Q_SLOTS:
    void onClicked();

private:
    QColor m_Color;
};

}  // End namespace Utils

#endif // COLORBUTTONCHOOSER_H
