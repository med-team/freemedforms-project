/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include <utils/widgets/uppercasevalidator.h>

#include <QString>
#include <QDebug>

using namespace Utils;

UpperCaseValidator::UpperCaseValidator(QObject *parent) :
    QValidator(parent)
{}  // nothing to do in constructor

UpperCaseValidator::~UpperCaseValidator()
{}

QValidator::State UpperCaseValidator::validate(QString &text, int &pos) const  // PS: no UpperCase for the first letter of variables, only for class names
{
    Q_UNUSED(pos); // pos is not needed
    text = text.toUpper();  // Uppercase the text
    return QValidator::Acceptable;  // return "ok text is like we want it to be"
}

CapitalizationValidator::CapitalizationValidator(QObject *parent) :
    QValidator(parent)
{}  // nothing to do in constructor

CapitalizationValidator::~CapitalizationValidator()
{}

QValidator::State CapitalizationValidator::validate(QString &text, int &pos) const  // PS: no UpperCase for the first letter of variables, only for class names
{
    int previous = pos-2;
    if (text.isEmpty())
        return QValidator::Acceptable;

    if (previous < 0) {
        text = text.replace(0, 1, text.at(pos-1).toUpper());
    } else {
        QChar prev = text.at(previous);
        if (prev==' ' || prev=='-' || prev==',' || prev=='.' || prev==';') {
            text = text.replace(previous+1, 1, text.at(pos-1).toUpper());
        }
    }
    return QValidator::Acceptable;  // return "ok text is like we want it to be"
}

