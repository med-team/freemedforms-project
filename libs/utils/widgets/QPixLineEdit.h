/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef QPIXLINEEDIT_H
#define QPIXLINEEDIT_H

#include <utils/global_exporter.h>

#include <QLineEdit>
#include <QPainter>
#include <QPaintEvent>
#include <QStyle>

/**
 * \file ./libs/utils/widgets/QPixLineEdit.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {

class UTILS_EXPORT QPixLineEdit :  public QLineEdit
{
    Q_OBJECT
public:

    // Pixmap should be 16x16
    /** */
    QPixLineEdit( QWidget * parent = 0 ) : QLineEdit( parent ){}

    /** */
    ~QPixLineEdit() {}

    /** */
    void setInsidePixmap( const QPixmap & pix )
    {
        p = pix; // pix is supposed to be a square
        int marg = ( this->height() - p.height() ) / 2;
        setStyleSheet( QString( "QLineEdit { padding-left: %1px; } " ).arg( p.width() + marg ) );
    }


private:
    void paintEvent( QPaintEvent * event )
    {
        QLineEdit::paintEvent( event );
        QPainter pix( this );
        int marg = ( this->height() - p.height() ) / 2;
        pix.drawPixmap( QPoint( 5, marg ) , p, QRect( 0, 0, this->width() , this->height() ) );
    }

    QPixmap p;
};

}

#endif
