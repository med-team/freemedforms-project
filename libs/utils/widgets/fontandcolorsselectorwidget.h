/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FONTANDCOLORSSELECTORWIDGET_H
#define FONTANDCOLORSSELECTORWIDGET_H

#include <utils/global_exporter.h>
#include <QWidget>
#include <QLabel>

namespace Utils {
class FontSelectorButton;
class ColorButtonChooser;

class UTILS_EXPORT FontAndColorsSelectorWidget : public QWidget
{
    Q_OBJECT
public:
    explicit FontAndColorsSelectorWidget(QWidget *parent = 0);

    void setLabelText(const QString &unTranslatedText, const QString &translationContext);
    void setDefaultFont(const QFont &font);
    void setCurrentFont(const QFont &font);
    void setDefaultColor(const QColor &color);
    void setCurrentColor(const QColor &color);

    QFont currentFont() const;
    QColor currentColor() const;

private:
    void retranslate();
    void changeEvent(QEvent *event);

private:
    FontSelectorButton *_fontButton;
    QLabel *_label;
    ColorButtonChooser *_colorButton;
    QString _unTrLabel, _trContext;
};

}  // namespace Utils

#endif // FONTANDCOLORSSELECTORWIDGET_H
