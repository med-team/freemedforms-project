/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATETIMEDELEGATE_H
#define DATETIMEDELEGATE_H

#include <utils/global_exporter.h>
#include <QStyledItemDelegate>
#include <QDate>
#include <QTime>

/**
 * \file ./libs/utils/widgets/datetimedelegate.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {

class UTILS_EXPORT DateTimeDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    DateTimeDelegate(QObject *parent = 0, bool dateOnly = false);

    void setDateRange(const QDateTime &min, const QDateTime &max);
    void setDateRange(const QDate &min, const QDate &max);
    void setDateOnly(bool state);

    QString displayText(const QVariant &value, const QLocale &locale) const;

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const;

    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const;

    void updateEditorGeometry(QWidget *editor,
                              const QStyleOptionViewItem &option, const QModelIndex &index) const;

private:
    bool m_IsDateOnly;
    QTime m_MaxTime, m_MinTime;
    QDate m_MaxDate, m_MinDate;
};


} // end namespace Utils


#endif // DATETIMEDELEGATE_H
