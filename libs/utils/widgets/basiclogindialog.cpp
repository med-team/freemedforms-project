/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "basiclogindialog.h"
#include "ui_basiclogindialog.h"

#include <translationutils/constants.h>
#include <translationutils/trans_current.h>

#include <QPushButton>
#include <QTextBrowser>

using namespace Utils;
using namespace Trans::ConstantTranslations;

BasicLoginDialog::BasicLoginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BasicLoginDialog),
    _more(0),
    _moreBrowser(0)
{
    ui->setupUi(this);
    ui->loginWidget->togglePasswordEcho(false);
    adjustSize();
}

BasicLoginDialog::~BasicLoginDialog()
{
    delete ui;
}

void BasicLoginDialog::setTitle(const QString &title)
{
    ui->title->setText(title);
    this->setWindowTitle(title);
}

void BasicLoginDialog::setToggleViewIcon(const QString &fullAbsPath)
{
    ui->loginWidget->setToggleViewIcon(fullAbsPath);
}

void BasicLoginDialog::setHtmlExtraInformation(const QString &html)
{
    _more = ui->buttonBox->addButton(tkTr(Trans::Constants::MORE_INFORMATION), QDialogButtonBox::ActionRole);
    _moreBrowser = new QTextBrowser(this);
    _moreBrowser->setHtml(html);
    _moreBrowser->setHidden(true);
    layout()->addWidget(_moreBrowser);
    connect(_more, SIGNAL(clicked()), this, SLOT(onMoreClicked()));
}

void BasicLoginDialog::focusLogin()
{
    ui->loginWidget->focusLogin();
}

QString BasicLoginDialog::login() const
{
    return ui->loginWidget->login();
}

QString BasicLoginDialog::password() const
{
    return ui->loginWidget->password();
}

void BasicLoginDialog::onMoreClicked()
{
    if (!_moreBrowser)
        return;
    _moreBrowser->setVisible(!_moreBrowser->isVisible());
    adjustSize();
}
