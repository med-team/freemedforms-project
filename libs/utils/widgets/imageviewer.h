/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <utils/global_exporter.h>
#include <QDialog>
#include <QPixmap>
#include <QString>
#include <QList>

QT_BEGIN_NAMESPACE
class QLabel;
class QScrollArea;
class QScrollBar;
class QDialogButtonBox;
QT_END_NAMESPACE

namespace Utils {

class UTILS_EXPORT ImageViewer : public QDialog
{
    Q_OBJECT
public:
    explicit ImageViewer(QWidget *parent = 0);

    void setPixmap(const QPixmap &pixmap);
    void setPixmaps(const QList<QPixmap> &pixmaps);
    void showPixmapFile(const QString &absFilePath);

private Q_SLOTS:
    void zoomIn();
    void zoomOut();
    void normalSize();
    void fitToWindow();
    void next();
    void previous();
    void toggleFullScreen();
    void updateButtons();

private:
    void scaleImage(double factor);
    void adjustScrollBar(QScrollBar *scrollBar, double factor);

    QLabel *imageLabel;
    QScrollArea *scrollArea;
    int scaleFactor;
    QDialogButtonBox *m_ButBox;
    QPushButton *mPreviousButton, *mNextButton;
    QList<QPixmap> m_pixmaps;
    int m_CurrentIndex;
};

}  // End namespace Utils

#endif // IMAGEVIEWER_H
