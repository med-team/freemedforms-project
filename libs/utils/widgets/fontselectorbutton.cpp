/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "fontselectorbutton.h"

#include <QApplication>
#include <QFontDialog>
#include <QFontMetrics>
#include <QAction>
#include <QEvent>
#include <QHelpEvent>
#include <QTextDocument>
#include <QCursor>

#include <QDebug>

using namespace Utils;

FontSelectorButton::FontSelectorButton(QWidget *parent) :
    QToolButton(parent),
    _fontChanged(false),
    _currentDefined(false)
{
    aEditFont = new QAction(this);
    aResetToDefault = new QAction(this);
    retranslate();
    addAction(aEditFont);
    addAction(aResetToDefault);
    setDefaultAction(aEditFont);
    connect(aResetToDefault, SIGNAL(triggered()), this, SLOT(resetToDefaultFont()));
    connect(aEditFont, SIGNAL(triggered()), this, SLOT(editFont()));
}

void FontSelectorButton::editFont()
{
    QWidget *focused = QApplication::activeWindow();
    QFontDialog dlg;
    QFont useMe;
    if (_currentDefined)
        useMe = _current;
    else
        useMe = _default;
    dlg.setCurrentFont(useMe);
    if (dlg.exec()==QDialog::Accepted) {
        if (useMe!=dlg.currentFont()) {
            _fontChanged = true;
            _current = dlg.currentFont();
            applyFont(_current);
        }
    }
    QApplication::setActiveWindow(focused);
}

void FontSelectorButton::setDefaultFont(const QFont &font)
{
    _default = font;
    applyFont(font);
}

void FontSelectorButton::setCurrentFont(const QFont &font)
{
    _currentDefined = true;
    _current = font;
    applyFont(font);
}

QFont FontSelectorButton::currentFont()
{
    if (_currentDefined)
        return _current;
    else
        return _default;
}

void FontSelectorButton::resetToDefaultFont()
{
    _currentDefined = true;
    _current = _default;
    applyFont(_current);
}

void FontSelectorButton::applyFont(const QFont &font)
{
    QTextDocument doc(this);
    doc.setDefaultFont(font);
    doc.setPlainText(this->text());
    setToolTip(doc.toHtml());
}

void FontSelectorButton::retranslate()
{
    aEditFont->setText(tr("Edit font"));
    aResetToDefault->setText(tr("Reset to default"));
    aEditFont->setToolTip(aEditFont->text());
    aResetToDefault->setToolTip(aResetToDefault->text());
}

void FontSelectorButton::changeEvent(QEvent *event)
{
    if (event->type()==QEvent::LanguageChange) {
        retranslate();
    }
}
