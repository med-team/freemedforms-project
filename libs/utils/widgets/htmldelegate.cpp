/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main Developers:                                                     *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/


#include "htmldelegate.h"

#include <QPainter>
#include <QStyleOptionViewItemV4>
#include <QVariant>
#include <QModelIndex>
#include <QString>
#include <QIcon>
#include <QTextDocument>
#include <QApplication>
#include <QAbstractTextDocumentLayout>
#include <QTreeView>
#include <QMap>
#include <QTimer>
#include <QListView>
#include <QTableView>

#include <QDebug>


enum { DrawDelegateRect = false };

using namespace Utils;
using namespace Internal;

namespace Utils {
namespace Internal {
class HtmlDelegatePrivate
{
public:
    HtmlDelegatePrivate(HtmlDelegate *parent) :
        q(parent)
    {}

    ~HtmlDelegatePrivate()
    {
    }

    QString changeColors(const QStyleOptionViewItemV4 &optionV4, QString text)
    {
        if (optionV4.state & QStyle::State_Selected) {
            text.replace(QRegExp("color\\s*:\\s*gray", Qt::CaseInsensitive), "color:lightgray");
            text.replace(QRegExp("color\\s*:\\s*black", Qt::CaseInsensitive), "color:white");
            text.replace(QRegExp("color\\s*:\\s*blue", Qt::CaseInsensitive), "color:lightcyan");
            text.replace(QRegExp("color\\s*:\\s*red", Qt::CaseInsensitive), "color:bisque");
            text.replace(QRegExp("color\\s*:\\s*marron", Qt::CaseInsensitive), "color:#F2E6E6");
        }
        return text;
    }

    void setHtml(const QModelIndex &index, const QStyleOptionViewItemV4 &optionV4)
    {
        QTextDocument *doc;
        if (_documents.contains(index)) {
            doc = _documents.value(index);
        } else {
            doc = new QTextDocument(q);
            _documents.insert(index, doc);
        }
        doc->setHtml(changeColors(optionV4, optionV4.text));
    }

    void setDocumentWidth(const QModelIndex &index, const QStyleOptionViewItemV4 &optionV4)
    {
        QTextDocument *doc = _documents.value(index);
        if (!doc) {
            qWarning() << "No Doc?" << index;
            return;
        }
        doc->setTextWidth(getMaxWidth(optionV4));
    }

    QSize documentSize(const QModelIndex &index)
    {
        QTextDocument *doc = _documents.value(index);
        if (!doc) {
            qWarning() << "No Doc?" << index;
            return QSize(1,1);
        }
        return QSize(doc->textWidth(), doc->size().height());
    }

    void drawDocument(QPainter *painter, const QModelIndex &index, const QRect &rect)
    {
        QTextDocument *doc = _documents.value(index, 0);
        if (!doc) {
            qWarning() << "  **** HtmlDelegate: No doc registered for index:" << index;
            return;
        }
        doc->drawContents(painter, rect);
    }

    int getMaxWidth(const QStyleOptionViewItemV4 &optionV4)
    {
        int max = optionV4.rect.width();

        QListView *view = qobject_cast<QListView*>(const_cast<QWidget*>(optionV4.widget));
        if (view)
            max = qMin(max, view->viewport()->width());

        QTreeView *treeview = qobject_cast<QTreeView*>(const_cast<QWidget*>(optionV4.widget));
        if (treeview) {
            QModelIndex idx = optionV4.index;
            max = treeview->columnWidth(idx.column());
            int indent = treeview->indentation();
            while (idx.parent().isValid()) {
                idx = idx.parent();
                indent += treeview->indentation();
            }
            max -= indent;
        }

        if (!optionV4.decorationSize.isNull() && !optionV4.icon.isNull()) {
            max -= optionV4.decorationSize.width();
        }

        return max;
    }

public:
    QMap<QPersistentModelIndex, QTextDocument*> _documents;
    QTimer timer;
    QMultiMap<QTreeView *, QPersistentModelIndex> _treeViewDataChanged;

private:
    HtmlDelegate *q;
};
} // namespace Internal
} // namespace Utils


HtmlDelegate::HtmlDelegate(QObject *parent) :
    QStyledItemDelegate(parent),
    d_html(new HtmlDelegatePrivate(this))
{
    d_html->timer.setInterval(75);
    d_html->timer.setSingleShot(true);
    this->connect(&d_html->timer, SIGNAL(timeout()), this, SLOT(treeView_indexDataChanged()));
}

HtmlDelegate::~HtmlDelegate()
{
    if (d_html)
        delete d_html;
    d_html = 0;
}

void HtmlDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyleOptionViewItemV4 optionV4 = option;
    initStyleOption(&optionV4, index);
    QStyle *style = optionV4.widget? optionV4.widget->style() : QApplication::style();


    QTreeView *treeview = qobject_cast<QTreeView*>(const_cast<QWidget*>(optionV4.widget));
    if (treeview) {
        if (d_html->getMaxWidth(optionV4) != d_html->documentSize(index).width()) {
            if (!d_html->_treeViewDataChanged.values(treeview).contains(index)) {
                d_html->_treeViewDataChanged.insertMulti(treeview, index);
                d_html->timer.start();
            }
        }
    }

    QTableView *tableview = qobject_cast<QTableView*>(const_cast<QWidget*>(optionV4.widget));
    if (tableview) {
        d_html->setHtml(index, optionV4);
        d_html->setDocumentWidth(index, optionV4);
    }

    QString backupText = optionV4.text;
    optionV4.text = QString(); // inhibe text displaying
    style->drawControl(QStyle::CE_ItemViewItem, &optionV4, painter);
    optionV4.text = backupText;

    QAbstractTextDocumentLayout::PaintContext ctx;

    if (optionV4.state & QStyle::State_Selected)
        ctx.palette.setColor(QPalette::Text, optionV4.palette.color(QPalette::Active, QPalette::HighlightedText));

    QRect plainTextRect = style->subElementRect(QStyle::SE_ItemViewItemText, &optionV4);
    plainTextRect = plainTextRect.adjusted(1,1,-1,-1);

    QRect textRect = optionV4.rect;
    textRect.setTop(plainTextRect.top());
    textRect = textRect.adjusted(1,1,-1,-1);

    QPen pen;
    if (DrawDelegateRect) {
        pen.setColor(QColor("red"));
        painter->setPen(pen);
        painter->drawRect(plainTextRect);
        pen.setColor(QColor("blue"));
        painter->setPen(pen);
        painter->drawRoundedRect(textRect, 5, 5);
    }

    painter->save();
    painter->translate(textRect.topLeft());
    painter->setClipRect(textRect.translated(-textRect.topLeft()));
    QRect htmlRect = textRect.translated(-textRect.topLeft());
    painter->translate(plainTextRect.left(), 0);

    if (treeview) {
        QModelIndex idx = index;
        int indent = treeview->indentation();
        while (idx.parent().isValid()) {
            idx = idx.parent();
            indent += treeview->indentation();
        }
        painter->translate(-QPointF(indent, 0));
    }

    if (DrawDelegateRect) {
        pen.setColor(QColor("green"));
        painter->setPen(pen);
        painter->drawRoundedRect(htmlRect.adjusted(2,2,-2,-2), 5, 5);
    }

    htmlRect.setWidth(d_html->getMaxWidth(optionV4));

    d_html->setHtml(index, optionV4);
    d_html->setDocumentWidth(index, optionV4);

    d_html->drawDocument(painter, index, htmlRect);
    painter->translate(-plainTextRect.left(), 0);
    painter->restore();
}

QSize HtmlDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyleOptionViewItemV4 optionV4 = option;
    initStyleOption(&optionV4, index);

    d_html->setHtml(index, optionV4);
    d_html->setDocumentWidth(index, optionV4);



    return d_html->documentSize(index);

}

void HtmlDelegate::treeView_indexDataChanged()
{
    foreach(QTreeView *tree, d_html->_treeViewDataChanged.keys()) {
        foreach(const QPersistentModelIndex &index, d_html->_treeViewDataChanged.values(tree)) {
            tree->dataChanged(index, index);
        }
    }
    d_html->_treeViewDataChanged.clear();
}
