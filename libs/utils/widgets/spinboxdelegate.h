/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef SPINBOXDELEGATE_H
#define SPINBOXDELEGATE_H

#include <utils/global_exporter.h>

#include <QItemDelegate>
#include <QWidget>
#include <QObject>
#include <QModelIndex>
#include <QStyleOptionViewItem>
#include <QSize>


namespace Utils {

class UTILS_EXPORT SpinBoxDelegate : public QItemDelegate
 {
     Q_OBJECT
 public:
     SpinBoxDelegate(QObject *parent = 0, double min = 0.0, double max = 100.0, bool isDouble = false);

     void setMaximum(double max);
     void setDouble(bool isDouble);

     QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                           const QModelIndex &index) const;

     void setEditorData(QWidget *editor, const QModelIndex &index) const;
     void setModelData(QWidget *editor, QAbstractItemModel *model,
                       const QModelIndex &index) const;

     void updateEditorGeometry(QWidget *editor,
         const QStyleOptionViewItem &option, const QModelIndex &index) const;

 private:
     bool m_IsDouble;
     double m_Min, m_Max;
 };


} // end namespace Utils


#endif // SPINBOXDELEGATE_H
