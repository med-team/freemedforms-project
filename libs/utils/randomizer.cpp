/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 ***************************************************************************/




#include "randomizer.h"
#include "global.h"
#include "log.h"

#include <time.h>

#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QVector>
#include <QVariant>


#include <time.h>

using namespace Utils;
using namespace std;

#define makeRandDouble(max) ((double)rand() / ((double)RAND_MAX + 1) * (double)max)
#define makeRand(max)       (int)((double)rand() / ((double)RAND_MAX + 1) * (double)max)

namespace Utils {
namespace Internal {
class RandomizerPrivate
{
public:
    RandomizerPrivate() {}

    ~RandomizerPrivate()
    {
        boysFirstnames.clear();
        girlsFirstnames.clear();
    }



    void readZipCodes()
    {
        if (!zipCodes.isEmpty())
            return;
        QString c = Utils::readTextFile(m_Path + "/zipcodes.csv");
        if (c.isEmpty())
            Utils::Log::addError("Randomizer", "Can not read zip codes.",
                                 __FILE__, __LINE__);
        foreach(const QString &s, c.split("\n", QString::SkipEmptyParts)) {
            QStringList z = s.split("\t");
            if (z.count() != 2)
                continue;
            zipCodes.insert(z.at(1).toInt(), z.at(0).toUpper());
        }
    }



public:
    QVector<QString> boysFirstnames;
    QVector<QString> girlsFirstnames;
    QVector<QString> words;
    QMap<int, QString> zipCodes;
    QString m_Path;
};
}
}


Randomizer::Randomizer() :
        d(0)
{
    d = new Internal::RandomizerPrivate;
    srand(time(NULL));
}

Randomizer::~Randomizer()
{
    if (d) {
        delete d;
        d = 0;
    }
}

void Randomizer::setPathToFiles(const QString &p)
{
    d->m_Path = QDir::cleanPath(p);
}

QString Randomizer::randomFirstName(bool male) const
{
    QSqlDatabase db;
    if (!QSqlDatabase::connectionNames().contains("__RANDOM__FIRSTNAMES__")) {
        db = QSqlDatabase::addDatabase("QSQLITE", "__RANDOM__FIRSTNAMES__");
        db.setDatabaseName(d->m_Path + "/firstnames.db");
    } else {
        db = QSqlDatabase::database("__RANDOM__FIRSTNAMES__");
    }
    if (!db.isOpen()) {
        if (!db.open()) {
            LOG_ERROR_FOR("Randomizer", "Unable to connect to database: " + db.databaseName());
            return QString::null;
        }
    }
    int sex;
    (male) ? sex=0 : sex=1;  // female

    int max = 255;
    db.transaction();
    QSqlQuery query(db);
    QString req = QString("SELECT count(ID) FROM FIRSTNAMES WHERE LANG='%1' AND FEMALE=%2;")
            .arg("fr") // TODO: manage multiple languages
            .arg(sex);
    if (query.exec(req)) {
        if (query.next()) {
            max = query.value(0).toInt() - 1;
        }
    } else {
        LOG_QUERY_ERROR_FOR("Randomizer", query);
    }
    query.finish();

    req = QString("SELECT FIRSTNAME FROM FIRSTNAMES WHERE (LANG='%1' AND FEMALE=%2) LIMIT %3,1;")
            .arg("fr") // TODO: manage multiple languages
            .arg(sex)
            .arg(randomInt(0, max));
    if (query.exec(req)) {
        if (query.next()) {
            return query.value(0).toString().toUpper();
        }
    } else {
        LOG_QUERY_ERROR_FOR("Randomizer", query);
    }
    return QString::null;
}

QString Randomizer::randomString(int length) const
{
    static char consonnes[]="BCDFGHJKLMNPQRSTVWXZ";
    static char voyelles[]="AEIOUY";

    QString s;
    int l = length;
    if (length == 0) {
        l = 5;
    }
    for (int i=0; i<l; i++)
    {
        if ((l > 3) && (makeRand(5) == 2))
            s.append(" ");
        else if ((i % 2) == 0)
            s.append(QChar(consonnes[makeRand(20)]));
        else
            s.append(QChar(voyelles[makeRand(6)]));
    }
    return s;
}

QPair<int, QString> Randomizer::randomFrenchCity() const
{
    if (d->zipCodes.isEmpty())
        d->readZipCodes();
    QPair<int, QString> p;
    int r = makeRand(d->zipCodes.count() - 1);
    p.first = d->zipCodes.keys().at(r);
    p.second = d->zipCodes.value(p.first);
    return p;
}

QString Randomizer::randomName() const
{
    QString randName;
    do
    {
        randName = randomWords(randomInt(1, 2));
    }while ( randName.size()>7 );
    
    return randName;
}

QString Randomizer::randomWords(int nbOfWords) const
{
    QSqlDatabase db;
    if (!QSqlDatabase::connectionNames().contains("__RANDOM__WORDS__")) {
        db = QSqlDatabase::addDatabase("QSQLITE", "__RANDOM__WORDS__");
        db.setDatabaseName(d->m_Path + "/words.db");
    } else {
        db = QSqlDatabase::database("__RANDOM__WORDS__");
    }
    if (!db.isOpen()) {
        if (!db.open()) {
            LOG_ERROR_FOR("Randomizer", "Unable to connect to database: " + db.databaseName());
            return QString::null;
        }
    }
    int max = 255;
    db.transaction();
    QSqlQuery query(db);
    QString req = QString("SELECT max(ID) FROM WORDS WHERE LANG='%1';")
            .arg("fr"); // TODO: manage multiple languages
    if (query.exec(req)) {
        if (query.next()) {
            max = query.value(0).toInt();
        }
    } else {
        LOG_QUERY_ERROR_FOR("Randomizer", query);
    }
    query.finish();

    QStringList t;
    for(int i=0; i < nbOfWords; ++i) {
        QString req = QString("SELECT WORD FROM WORDS WHERE LANG='%1' AND ID='%2';")
                .arg("fr") // TODO: manage multiple languages
                .arg(randomInt(0, max));
        if (query.exec(req)) {
            if (query.next()) {
                QString val = query.value(0).toString().toUpper();
                t << val.replace(QRegExp("\\W"), "_");
            }
        } else {
            LOG_QUERY_ERROR_FOR("Randomizer", query);
        }
    }
    db.commit();
    return t.join(" ");
}

int Randomizer::randomInt(int max) const
{
    return makeRand(max);
}

bool Randomizer::randomBool() const
{
    int z = 0;
    for(int i=0; i < randomInt(1, 50); ++i) {
        z += randomInt(0,1);
    }
    return (z%1==0);
}

int Randomizer::randomInt(int min, int max) const
{
    Q_ASSERT_X(min <= max, "Utils::Randomizer", "min > max");
    if (min == max)
        return min;
    int i = min - 10;
    int z = 0;
    while (i < min) {
        i = makeRand(max);
        if (++z == 20) {
            i = max;
            break;
        }
    }
    return i;
}

double Randomizer::randomDouble(double min, double max) const
{
    Q_ASSERT(min < max);
    double i = min - 10.;
    int z = 0;
    while (i < min) {


        i = makeRandDouble(max);
        if (++z == 20) {
            i = max;
            break;
        }
    }
    return i;
}


QDate Randomizer::randomDate(const int minYear, const int minMonth, const int minDay) const
{
    int r = -1;
    QDate toReturn(minYear, minMonth, minDay);
    int days = toReturn.daysTo(QDate::currentDate());
    if (randomInt(1, 7) > 5)
        days = days / 2;
    if (days > 1) {
        int i = 0;
        while (r < 1) {
            r = randomInt(days);
            if (++i == 20)
                break;
        }
    } else {
        return toReturn.addDays(1);
    }
    return toReturn.addDays(r);
}


QDateTime Randomizer::randomDateTime(const QDateTime &minDateTime) const
{
    QDateTime toReturn(randomDate(minDateTime.date().year(), minDateTime.date().month(), minDateTime.date().day()));
    if (toReturn.date() == minDateTime.date()) {
        int i = 0;
        while (toReturn < minDateTime) {
            int r = 0;
            int j = 0;
            while (r < 1) {
                r = randomInt(23452634);
                if (++j == 20) {
                    r = i + 1;
                    break;
                }
            }
            toReturn.addMSecs(r);
            if (++i == 20)
                break;
        }
    } else {
        toReturn.setTime(QTime(randomInt(23), randomInt(59), randomInt(59)));
    }
    return toReturn;
}

QTime Randomizer::randomTime(const int minHour, const int maxHour) const
{
    int h = randomInt(minHour, maxHour);
    int m = randomInt(0, 59);
    int s = randomInt(0, 59);
    return QTime(h,m,s);
}

QFileInfo Randomizer::randomFile(const QDir &inDir, const QStringList &filters) const
{
    QFileInfoList list = inDir.entryInfoList(filters);
    return list.at(randomInt(0, list.count()));
}

QString Randomizer::randomVersionNumber() const
{
    QString v = QString("%1.%2.%3").arg(randomInt(0, 100)).arg(randomInt(0, 100)).arg(randomInt(0, 100));
    if (randomBool()) {
        switch (randomInt(0, 3)) {
        case 0: v += QString("~alpha%1").arg(randomInt(0, 100)); break;
        case 1: v += QString("~beta%1").arg(randomInt(0, 100)); break;
        case 2: v += QString("~RC%1").arg(randomInt(0, 100)); break;
        default: break;
        }
    }
    return v;
}

