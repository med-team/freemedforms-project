/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef IDATABASEDUMPER_H
#define IDATABASEDUMPER_H

#include <QObject>

/**
 * \file ./libs/utils/idatabasedumper.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {

class IDatabaseDumper : public QObject
{
    Q_OBJECT
public:
    IDatabaseDumper(QObject *parent = 0) : QObject(parent) {}
    virtual ~IDatabaseDumper() {}

    virtual bool dumpDatabaseToFile(const QString &file, bool zip = true) = 0;
    virtual bool restoreDatabaseFromFile(const QString &file) = 0;

    virtual bool errorEncountered() const = 0;

Q_SIGNALS:
    void processStarted();
    void processDone();
    void error(const QString &error);
    void message(const QString &message);
};

}  // End namespace Utils

#endif // IDATABASEDUMPER_H
