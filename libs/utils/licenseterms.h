/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef LICENSETERMS_H
#define LICENSETERMS_H

#include <utils/global_exporter.h>

#include <QString>

/**
 * \file ./libs/utils/licenseterms.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/


namespace Utils {

class UTILS_EXPORT LicenseTerms
{
public:
    enum AvailableLicense {
        LGPL = 0,
        GPLv3,
        BSD,
        BSDModified,
        NonFree
    };

    static QString getTranslatedLicenseTerms(int availableLicense);
};

}  // end Core

#endif // LICENSETERMS_H
