/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef UTILS_WAITFORSIGNAL_H
#define UTILS_WAITFORSIGNAL_H

#include <utils/global_exporter.h>
#include <QObject>

/**
 * \file ./libs/utils/waitforsignal.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Utils {

UTILS_EXPORT bool waitForSignal(QObject *sender, const char *signal, int timeoutMs = 10000);

} // namespace Utils

#endif  // ACCOUNT_WAITFORSIGNAL_H

