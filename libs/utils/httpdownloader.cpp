/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 ***************************************************************************/


#include "httpdownloader.h"
#include "httpdownloader_p.h"

#include <utils/log.h>
#include <utils/global.h>
#include <utils/widgets/basiclogindialog.h>

#include <QProgressDialog>
#include <QFileInfo>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QDir>
#include <QMainWindow>
#include <QProgressBar>
#include <QNetworkProxy>
#include <QAuthenticator>
#include <QNetworkProxy>

using namespace Utils;
using namespace Internal;

namespace {
const int MAX_AUTHENTIFICATION_TRIES = 3;
}

HttpDownloader::HttpDownloader(QObject *parent) :
    QObject(parent),
    d(new HttpDownloaderPrivate(this))
{
    setObjectName("HttpDownloader");
}

HttpDownloader::~HttpDownloader()
{
    if (d)
        delete d;
    d = 0;
}

/*! \deprecated should not have any direct contact to GUI!
 */
void HttpDownloader::setMainWindow(QMainWindow *win)
{
    if (d->progressDialog) {
        delete d->progressDialog;
        d->progressDialog = new QProgressDialog(win);
        d->progressDialog->setWindowModality(Qt::WindowModal);
        connect(d->progressDialog, SIGNAL(canceled()), d, SLOT(cancelDownload()));
    }
}


void HttpDownloader::setUrl(const QUrl &url)
{
    d->m_Url = url;
}

const QUrl &HttpDownloader::url() const
{
    return d->m_Url;
}


void HttpDownloader::setStoreInBuffer(bool store)
{
   d->_useBuffer = store;
}

QByteArray HttpDownloader::getBufferContent() const
{
    return d->_stringBuffer;
}

void HttpDownloader::setOutputPath(const QString &absolutePath)
{
    if (QDir(absolutePath).exists())
        d->m_Path = absolutePath;
    else
        d->m_Path.clear();
}

/*!
  * \brief Sets the output file name.
  *
  * By default, the output filename is the filename of the URL. This method sets a custom out put filename.
  * If you want to clear the output file name just pass an empty QString to this method.
  * \sa setUrl(), setOutputPath()
  */
void HttpDownloader::setOutputFileName(const QString &fileName)
{
    d->m_OutputFileName = fileName;
}

/*!
  * \brief Returns the output file name.
  *
  * By default, the output filename is the filename of the URL,
  * you can customize it with setOutputFileName().
  * \sa setUrl(), setOutputFileName()
  */
QString HttpDownloader::outputFileName() const
{
    if (d->m_OutputFileName.isEmpty()) {
        QFileInfo fileInfo(d->m_Url.path());
        QString fileName = fileInfo.fileName();
        if (fileName.isEmpty())
            fileName = "index.html";
        return fileName;
    }
    return d->m_OutputFileName;
}

/*!
  * \brief Returns the output absolute path and file name.
  *
  * By default, the output filename is the filename of the URL, you can customize it with setOutputFileName().
  * \sa setUrl(), setOutputFileName(), setOutputPath()
  */
QString HttpDownloader::outputAbsoluteFileName() const
{
    if (d->m_Path.isEmpty())
        return outputFileName();
    return QDir::cleanPath(d->m_Path + QDir::separator() + outputFileName());
}

void HttpDownloader::setLabelText(const QString &text)
{
    d->m_LabelText = text;
}

QString HttpDownloader::lastErrorString() const
{
    return d->lastError;
}

QNetworkReply::NetworkError HttpDownloader::networkError() const
{
    return d->networkError;
}


bool HttpDownloader::startDownload()
{
    return d->startDownload();
}


bool HttpDownloader::cancelDownload()
{
    d->cancelDownload();
    Q_EMIT downloadFinished();
    return true;
}

HttpDownloaderPrivate::HttpDownloaderPrivate(HttpDownloader *parent) :
    reply(0),
    file(0),
    progressDialog(0),
    progressBar(0),
    httpGetId(-1),
    httpRequestAborted(false),
    networkError(QNetworkReply::NoError),
    _useBuffer(false),
    q(parent)
{
    setObjectName("HttpDownloaderPrivate");

    if (!QNetworkProxy::applicationProxy().hostName().isEmpty()) {
        qnam.setProxy(QNetworkProxy::applicationProxy());
        LOG("Using proxy: " + qnam.proxy().hostName());
    } else {
        qnam.setProxy(QNetworkProxy::NoProxy);
        LOG("Clearing proxy");
    }

    connect(&qnam, SIGNAL(authenticationRequired(QNetworkReply*,QAuthenticator*)), this, SLOT(authenticationRequired(QNetworkReply*,QAuthenticator*)));
    connect(&qnam, SIGNAL(proxyAuthenticationRequired(QNetworkProxy,QAuthenticator*)), this, SLOT(proxyAuthenticationRequired(QNetworkProxy,QAuthenticator*)));
}

HttpDownloaderPrivate::~HttpDownloaderPrivate()
{}

bool HttpDownloaderPrivate::startDownload()
{
    if (progressBar) {
        progressBar->setRange(0, 100);
        progressBar->setValue(0);
        progressBar->setToolTip(tr("Initialization of the download"));
    }
    if (!m_Url.isValid())
        return false;
    if (m_Url.isEmpty())
        return false;
    return downloadFile();
}

bool HttpDownloaderPrivate::downloadFile()
{
    if (!_useBuffer) {
        QString fileName = q->outputAbsoluteFileName();

        if (QFile::exists(fileName)) {
            if (!Utils::yesNoMessageBox(tr("There already exists a file called %1 in "
                                           "the current directory. Overwrite?").arg(fileName), ""))
                return false;
            QFile::remove(fileName);
        }

        file = new QFile(fileName);
        if (!file->open(QIODevice::WriteOnly)) {
            lastError = tr("Unable to save the file %1: %2.")
                    .arg(fileName).arg(file->errorString());
            LOG_ERROR(lastError);
            delete file;
            file = 0;
            return false;
        }
    } else {
        _stringBuffer.clear();
    }

    if (progressBar) {
        progressBar->setToolTip(m_LabelText);
    }

    httpRequestAborted = false;
    return startRequest(m_Url);
}

bool HttpDownloaderPrivate::startRequest(const QUrl &url)
{
    if (!url.isValid())
        return false;
    if (m_LabelText.isEmpty()) {
        LOG(tr("Start downloading: %1 to %2").arg(m_Url.toString()).arg(m_Path));
    } else {
        LOG(m_LabelText);
    }
    reply = qnam.get(QNetworkRequest(url));
    connect(reply, SIGNAL(finished()), this, SLOT(httpFinished()));
    connect(reply, SIGNAL(readyRead()), this, SLOT(httpReadyRead()));
    connect(reply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(updateDownloadProgress(qint64,qint64)));

    return true;
}

void HttpDownloaderPrivate::cancelDownload()
{
    httpRequestAborted = true;
    if (reply) {
        reply->abort();
        reply->deleteLater();
    }
    networkError = QNetworkReply::OperationCanceledError;
    lastError = tr("Download canceled.");
    _stringBuffer.clear();
}

void HttpDownloaderPrivate::httpFinished()
{
    if (reply->error() == QNetworkReply::NoError) {
        qWarning() << "httpFinished No error";
	}
    else qWarning() << "httpFinished" << " QNetworkReply::NetworkError value: " << reply->error() << reply->errorString();

    networkError = reply->error();

    if (httpRequestAborted || networkError != QNetworkReply::NoError) {
        if (file) {
            file->close();
            file->remove();
            delete file;
            file = 0;
        }
        reply->deleteLater();
        if (progressDialog)
            progressDialog->hide();
        Q_EMIT q->downloadFinished();
        return;
    }

    if (progressBar) {
        if (networkError != QNetworkReply::NoError) {
            progressBar->setValue(0);
            lastError = tr("Download finished with an error: %1.").arg(reply->errorString());
            progressBar->setToolTip(lastError);
        } else  {
            progressBar->setValue(100);
            progressBar->setToolTip(tr("Download finished."));
        }
    }

    if (!_useBuffer) {
        file->flush();
        file->close();
    }

    QVariant redirectionTarget = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);
    if (networkError) {
        if (!_useBuffer)
            file->remove();
        Utils::informativeMessageBox(tr("Download failed: %1.")
                                     .arg(reply->errorString()), "", "", tr("HTTP"));
    } else if (!redirectionTarget.isNull()) {
        QUrl newUrl = m_Url.resolved(redirectionTarget.toUrl());
        if (Utils::yesNoMessageBox(tr("Redirect to %1?").arg(newUrl.toString()),
                                   tr("Redirect to %1?").arg(newUrl.toString()),
                                   "", tr("HTTP"))) {
            m_Url = newUrl;
            reply->deleteLater();
            if (!_useBuffer) {
                file->open(QIODevice::WriteOnly);
                file->resize(0);
            }
            startRequest(m_Url);
            return;
        }
    } else {
        LOG(tr("Downloaded %1 to current directory.").arg("file"));//.arg(fileName));
    }

    reply->deleteLater();
    reply = 0;
    if (file)
        delete file;
    file = 0;

    Q_EMIT q->downloadFinished();
}

void HttpDownloaderPrivate::httpReadyRead()
{
    if (!_useBuffer && file)
        file->write(reply->readAll());
    else
        _stringBuffer += reply->readAll();
}


void HttpDownloaderPrivate::updateDownloadProgress(qint64 bytesReceived, qint64 totalBytes)
{
    if (httpRequestAborted)
        return;

    Q_EMIT q->downloadProgress(bytesReceived, totalBytes);

    int permille = 0;
    if (totalBytes>0)
        permille = bytesReceived*1000/totalBytes;

    Q_EMIT q->downloadProgressPermille(permille);
}

void HttpDownloaderPrivate::authenticationRequired(QNetworkReply *reply, QAuthenticator *authenticator)
{
    LOG("Server authentication required: " +  reply->url().toString());
    const QString &host = reply->url().toString();
    m_AuthTimes.insert(host, m_AuthTimes.value(host, 0) + 1);
    if (m_AuthTimes.value(host) > MAX_AUTHENTIFICATION_TRIES) {
        LOG_ERROR("Server authentication max tries achieved. " +  host);
        return;
    }
    Utils::BasicLoginDialog dlg;
    dlg.setModal(true);
    dlg.setTitle(tr("Server authentication required"));
    if (dlg.exec()==QDialog::Accepted) {
        authenticator->setUser(dlg.login());
        authenticator->setPassword(dlg.password());
    }
}

void HttpDownloaderPrivate::proxyAuthenticationRequired(const QNetworkProxy &proxy, QAuthenticator *authenticator)
{
    LOG("Proxy authentication required: " +  proxy.hostName());
    const QString &host = proxy.hostName();
    m_AuthTimes.insert(host, m_AuthTimes.value(host, 0) + 1);
    if (m_AuthTimes.value(host) > MAX_AUTHENTIFICATION_TRIES) {
        LOG_ERROR("Proxy authentication max tries achieved. " +  host);
        return;
    }
    if (!proxy.user().isEmpty() && !proxy.password().isEmpty()) {
        authenticator->setUser(proxy.user());
        authenticator->setPassword(proxy.password());
    } else {
        Utils::BasicLoginDialog dlg;
        dlg.setModal(true);
        dlg.setTitle(tr("Proxy authentication required"));
        if (dlg.exec()==QDialog::Accepted) {
            authenticator->setUser(dlg.login());
            authenticator->setPassword(dlg.password());
        }
    }
}


