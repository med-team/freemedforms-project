/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#ifndef MEDINTUXEXPORTER_H
#define MEDINTUXEXPORTER_H

#include <qglobal.h>

// DEFINE EXPORTER
#if defined(MEDINTUX_LIBRARY)
#define MEDINTUX_EXPORT Q_DECL_EXPORT
#else
#define MEDINTUX_EXPORT Q_DECL_IMPORT
#endif

#endif  // TKMEDINTUXEXPORTER_H
