#!/bin/sh
# ****************************************************************************
#  *  The FreeMedForms project is a set of free, open source medical         *
#  *  applications.                                                          *
#  *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
#  *  All rights reserved.                                                   *
#  *                                                                         *
# *  License: BSD 3-clause (https://opensource.org/licenses/BSD-3-Clause)   *
#  ***************************************************************************
# /***************************************************************************
#  *  Main developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
#  *  Contributors:                                                          *
#  *       Christian A. Reiter <christian.a.reiter@gmail.com                 *
#  *       NAME <MAIL@ADDRESS.COM>                                           *
#  ***************************************************************************
# this script redirects the output from the error console to stdout
# so that the whole output can be filtered via e.g. grep

# define colors for highlighting
GREEN="\033[32;40m"
NO_COLOUR="\033[0m"

#for i in $( ls plugins ); do
#    if [ -f plugins/$i/*.pro ]; then
#        echo
#        echo "$WHITE********* Updating translation for FreeMedForms plugin: $i$NO_COLOUR"
#        echo
#        lupdate plugins/$i/*plugin.pro -no-obsolete  2>&1
#    fi
#done

APPS=". freediams freedrc freetoolbox freeddimanager"
for a in $APPS; do
    for i in $( ls $a/plugins ); do
        if [ -f $a/plugins/$i/*.pro ]; then
            printf "$GREEN********* Updating translation for $a plugin: $i$NO_COLOUR\n"
            lupdate $a/plugins/$i/*.pro -no-obsolete  2>&1
        fi
    done
done

for i in $( ls libs ); do
    if [ -f libs/$i/*.pro ]; then
        printf "$GREEN********* Updating translation for libs: $i$NO_COLOUR\n"
        lupdate libs/$i/*.pro -no-obsolete  2>&1
    fi
done

exit 0;
