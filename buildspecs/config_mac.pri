#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

macx{
    QMAKE_MACOSX_DEPLOYMENT_TARGET=10.11
    # define rpath
    CONFIG(debug_without_install) {
        RPATH_LIBS_BIN   = ../../../plugins/
    } else {
        RPATH_LIBS_BIN   = ../plugins/
    }
}
