#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

# This file is included in the fmf-plugins.pri and the libsworkbench
TRANSLATIONS += \
    $${SOURCES_TRANSLATIONS}/$$lower($${PRE_TRANSLATION})_$$lower($${TRANSLATION_NAME})_fr.ts \
    $${SOURCES_TRANSLATIONS}/$$lower($${PRE_TRANSLATION})_$$lower($${TRANSLATION_NAME})_de.ts \
    $${SOURCES_TRANSLATIONS}/$$lower($${PRE_TRANSLATION})_$$lower($${TRANSLATION_NAME})_es.ts \
    $${SOURCES_TRANSLATIONS}/$$lower($${PRE_TRANSLATION})_$$lower($${TRANSLATION_NAME})_pt.ts \
    $${SOURCES_TRANSLATIONS}/$$lower($${PRE_TRANSLATION})_$$lower($${TRANSLATION_NAME})_cs.ts \
    $${SOURCES_TRANSLATIONS}/$$lower($${PRE_TRANSLATION})_$$lower($${TRANSLATION_NAME})_ja.ts \
    $${SOURCES_TRANSLATIONS}/$$lower($${PRE_TRANSLATION})_$$lower($${TRANSLATION_NAME})_pl.ts \
    $${SOURCES_TRANSLATIONS}/$$lower($${PRE_TRANSLATION})_$$lower($${TRANSLATION_NAME})_ru.ts \
    $${SOURCES_TRANSLATIONS}/$$lower($${PRE_TRANSLATION})_$$lower($${TRANSLATION_NAME})_sl.ts \
    $${SOURCES_TRANSLATIONS}/$$lower($${PRE_TRANSLATION})_$$lower($${TRANSLATION_NAME})_it.ts

