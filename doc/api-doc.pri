#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

# this inclusion is only here to simplify the access to the api documentation files

OTHER_FILES +=  \
               $${PWD}/api/index.qdoc \
               $${PWD}/api/groups.qdoc \
               $${PWD}/api/coderules.qdoc \
               $${PWD}/api/licenseterms.qdoc \
               $${PWD}/api/databasestruct.qdoc \
               $${PWD}/api/freemedforms-api.qdoc \
               $${PWD}/api/whatsnew.qdoc \
               $${PWD}/api/buildinstructions.qdoc \
               $${PWD}/api/fmf_datamanagement.qdoc \
               $${PWD}/Doxyfile
