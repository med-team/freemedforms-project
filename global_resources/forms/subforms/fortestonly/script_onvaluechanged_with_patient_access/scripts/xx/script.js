function getClearance()
{
    freemedforms.forms.namespaceInUse = "Test::Script::ScriptCalculator::WithPatient::FakeClCrGroup";
    var w = freemedforms.forms.item("Weight").currentValue;
    var wUnit = freemedforms.forms.item("WeightUnit").currentText;
    var cr = freemedforms.forms.item("Creatinine").currentValue;
    var crUnit = freemedforms.forms.item("CreatinineUnit").currentText;
    var ageYears = freemedforms.patient.yearsOld;


    var genderMod = 1.04;
    if (freemedforms.patient.isFemale)
        genderMod = 0.85;

    if (wUnit=="lbl") {
        w *= 0.45359237;
    }

    if (crUnit != "mg/L") {
        cr /= 8.85;
    }

    var clcr = (((140-ageYears)*w) / (7.2*cr)) * genderMod;
    clcr += " ml/min";

    var html = "<p>Test using the Cockroft&Gault formula<br />";
    html += "Used formula: <b>(((140-ageYears)*w) / (7.2*cr)) * genderMod</b><br />";
    html += "Used values are:<br />";
    html += "&nbsp;&nbsp;- age in years: "+ageYears + "<br />";
    html += "&nbsp;&nbsp;- weight in Kg: "+w + "<br />";
    html += "&nbsp;&nbsp;- creatinine in mg/L: "+cr + "<br />";
    html += "&nbsp;&nbsp;- gender modulator: "+genderMod + "<br />";
    html += "Result is:<b>" + clcr + " ml/min</b><br />";
    html += "</p>";
    return html;
}

