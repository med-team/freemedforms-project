#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

#-------------------------------------------------
#
# Project created by QtCreator 2013-03-08T18:45:16
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = numbertostring
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += ../../libs

SOURCES += main.cpp \
    ../../libs/utils/numbertostring.cpp \
    ../../libs/translationutils/constanttranslations.cpp

HEADERS += ../../libs/utils/numbertostring.h \
    ../../libs/translationutils/constants.h \
    ../../libs/translationutils/trans_numbers.h
