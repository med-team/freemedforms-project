#include "tkPrinterTester.h"

#include <tkPrinter.h>

#include <QDir>
#include <tkGlobal.h>
#include <QDebug>
#include <QApplication>


tkPrinterTester::tkPrinterTester( QMainWindow *win ) :
        w(win)
{
    if (!w)
        w = new QMainWindow();
     test1();
}


void tkPrinterTester::test1()
{
    QDir dir(qApp->applicationDirPath());
    if (tkGlobal::isRunningOnMac())
        dir.cd("../../../");
    QString header = tkGlobal::readTextFile(":/header.html");
    QString header2 = tkGlobal::readTextFile(":/header_2.html");
    QString footer = tkGlobal::readTextFile(":/footer.html");
    QString footer2 = tkGlobal::readTextFile(":/footer_2.html");
    QString watermark = tkGlobal::readTextFile(":/watermark.html");
    QPixmap pixWatermark;
    pixWatermark.load(":/pixmapWatermark.png");
    QString document = tkGlobal::readTextFile(":/document.html");


    tkPrinter pe;
    pe.askForPrinter();
    pe.printer()->setFromTo(3,3);
    pe.addTextWatermark( "Adding a plain text\nWATERMARK", tkPrinter::EvenPages, Qt::AlignCenter, Qt::AlignCenter, QFont(), QColor("lightgrey") );
    pe.addTextWatermark( "Printed with DrugsInteractions", tkPrinter::EvenPages, Qt::AlignBottom, Qt::AlignCenter, QFont(), QColor("lightgrey") );
    pe.addPixmapWatermark( pixWatermark, tkPrinter::EachPages, Qt::AlignTop );
    pe.setHeader( header2 );
    pe.setFooter( footer );
    pe.setContent( document );
    pe.setOrientation(QPrinter::Portrait);
    pe.setPaperSize(QPrinter::A4);
    pe.previewDialog();
}

void tkPrinterTester::previewerTest()
{
    tkPrinter printer;
    previewer = printer.previewer(w);
    qWarning() << previewer;
    previewer->initialize();
    previewer->setHeader( "" );
    previewer->setFooter( "" );
    previewer->setWatermark("");
    w->setCentralWidget(previewer);
    w->show();
}
