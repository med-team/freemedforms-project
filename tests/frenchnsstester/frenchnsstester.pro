#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

#-------------------------------------------------
#
# Project created by QtCreator 2012-12-31T00:46:08
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = frenchnsstester
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        ../../plugins/basewidgetsplugin/frenchsocialnumberwidget.cpp

HEADERS  += mainwindow.h \
        ../../plugins/basewidgetsplugin/frenchsocialnumberwidget.h

FORMS    += mainwindow.ui \
    ../../plugins/basewidgetsplugin/frenchsocialnumber.ui
