/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "servercreationdialog.h"
#include "ui_servercreationdialog.h"

#include <datapackutils/servercreation/packcreationmodel.h>

#include <QPushButton>
#include <QDebug>

ServerCreationDialog::ServerCreationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ServerCreationDialog)
{
    ui->setupUi(this);
    _packCreationModel = new DataPack::PackCreationModel(this);
    ui->treeView->setModel(_packCreationModel);

    ui->pathChooser->setPath("/home/eric/freedddimanager/Documents/DataPacks");
    QPushButton *apply = ui->buttonBox->button(QDialogButtonBox::Apply);
    connect(apply, SIGNAL(clicked()), this, SLOT(screenNewPath()));

    QPushButton *checkedToConsole = ui->buttonBox->addButton(tr("checked to console"), QDialogButtonBox::ActionRole);
    connect(checkedToConsole, SIGNAL(clicked()), this, SLOT(onCheckedToConsole()));

}

ServerCreationDialog::~ServerCreationDialog()
{
    delete ui;
}

void ServerCreationDialog::screenNewPath()
{
    _packCreationModel->addScreeningPath(ui->pathChooser->path());
}

void ServerCreationDialog::onCheckedToConsole()
{
    qDebug() << _packCreationModel->getCheckedPacks();
}
