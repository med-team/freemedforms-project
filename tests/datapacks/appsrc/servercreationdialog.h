/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef DATAPACK_TESTINGS_APP_SERVERCREATIONDIALOG_H
#define DATAPACK_TESTINGS_APP_SERVERCREATIONDIALOG_H

#include <QDialog>

namespace Ui {
class ServerCreationDialog;
}

namespace DataPack {
class PackCreationModel;
}

class ServerCreationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ServerCreationDialog(QWidget *parent = 0);
    ~ServerCreationDialog();

private Q_SLOTS:
    void screenNewPath();
    void onCheckedToConsole();

private:
    Ui::ServerCreationDialog *ui;
    DataPack::PackCreationModel *_packCreationModel;
};

#endif // DATAPACK_TESTINGS_APP_SERVERCREATIONDIALOG_H
