/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef USERMANAGER_COREPLUGIN_H
#define USERMANAGER_COREPLUGIN_H

#include <extensionsystem/iplugin.h>

/**
 * \file ./tests/usermanager/plugins/coreplugin/coreplugin.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
class ApplicationGeneralPreferencesPage;
namespace Internal {
class CoreImpl;
}
}

namespace Core {
namespace Internal {

class CorePlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.freemedforms.UserManagerTester.CorePlugin" FILE "Core.json")

public:
    CorePlugin();
    ~CorePlugin();

    bool initialize(const QStringList &arguments, QString *errorMessage = 0);
    void extensionsInitialized();

public Q_SLOTS:
    void remoteArgument(const QString&);

private:
    CoreImpl *m_CoreImpl;
    ApplicationGeneralPreferencesPage *prefPage;
};

} // namespace Internal
} // namespace Core

#endif // USERMANAGER_COREPLUGIN_H
