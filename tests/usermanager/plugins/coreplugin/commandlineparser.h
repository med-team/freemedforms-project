/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef USERMANAGER_COMMANDLINEPARSER_H
#define USERMANAGER_COMMANDLINEPARSER_H

#include <coreplugin/icommandline.h>
#include <QObject>
#include <QHash>

/**
 * \file ./tests/usermanager/plugins/coreplugin/commandlineparser.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
namespace Internal {

class CommandLine  : public Core::ICommandLine
{
    Q_OBJECT

public:
    CommandLine(QObject *parent = 0);
    ~CommandLine();

    QVariant value(int param, const QVariant &def = QVariant()) const;
    void setValue(int ref, const QVariant &value);
    QString paramName(int param) const;

private:
    QHash<int,QVariant> params;
    QHash<int, QString> ref;
};

}  // End namespace Internal
}  // End namespace Core

#endif // USERMANAGER_COMMANDLINEPARSER_H
