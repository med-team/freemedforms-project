/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <utils/global.h>

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    Core::IMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qWarning();

    resize(900,600);
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::initialize(const QStringList &arguments, QString *errorString)
{
    return true;
}

void MainWindow::extensionsInitialized()
{
    raise();
    show();
}
