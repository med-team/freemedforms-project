<plugin name="ZipCodes" version="0.0.1" compatVersion="0.0.1">
    <vendor>FreeMedForms</vendor>
    <copyright>(C) 2008-2017 Eric Maeker, MD</copyright>
    <license>See application's license</license>
    <category>Patient data</category>
    <description>Zip codes analyzer and auto-completer plugin.</description>
    <url>http://www.freemedforms.com</url>
    <dependencyList>
        <dependency name="Core" version="0.0.1"/>
    </dependencyList>
</plugin>
