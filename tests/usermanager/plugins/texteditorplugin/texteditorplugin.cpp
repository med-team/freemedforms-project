/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#include "texteditorplugin.h"

#include <utils/log.h>

#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/icore.h>
#include <coreplugin/translators.h>

#include <QtPlugin>

#include <QDebug>

using namespace Editor;
using namespace Internal;



TextEditorPlugin::TextEditorPlugin()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "creating TextEditorPlugin";
}

TextEditorPlugin::~TextEditorPlugin()
{
}

bool TextEditorPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "TextEditorPlugin::initialize";
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);

    Core::ICore::instance()->translators()->addNewTranslator("plugin_texteditor");

    return true;
}

void TextEditorPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "TextEditorPlugin::extensionsInitialized";

    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));
}

ExtensionSystem::IPlugin::ShutdownFlag TextEditorPlugin::aboutToShutdown()
{
    if (Utils::Log::debugPluginsCreation())
        WARN_FUNC;
    return SynchronousShutdown;
}

Q_EXPORT_PLUGIN(TextEditorPlugin)
