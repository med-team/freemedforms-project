<plugin name="Alert" version="0.0.1" compatVersion="0.0.1">
    <vendor>tests/prevention</vendor>
    <copyright>(C) 2008-2017 by Eric MAEKER, MD</copyright>
    <license>See application's license</license>
    <description>The alert plugin manages all type of alerts in the project.</description>
    <url>http://www.freemedforms.com/</url>
    <dependencyList>
        <dependency name="Core" version="0.0.1"/>
    </dependencyList>
</plugin>
