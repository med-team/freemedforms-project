<plugin name="Prevention" version="0.0.1" compatVersion="0.0.1">
    <vendor>Prevention</vendor>
    <copyright>(C) 2008-2017 by Eric MAEKER, MD</copyright>
    <license>See application's license</license>
    <description>The Core plugin of Prevention test app.</description>
    <url>http://www.freemedforms.com/</url>
</plugin>
