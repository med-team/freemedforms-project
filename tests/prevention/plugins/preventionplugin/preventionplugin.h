/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef PREVENTION_COREPLUGIN_H
#define PREVENTION_COREPLUGIN_H

#include <extensionsystem/iplugin.h>

/**
 * \file ./tests/prevention/plugins/preventionplugin/preventionplugin.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Prevention {
    class ApplicationGeneralPreferencesPage;
}


namespace Prevention {
namespace Internal {

class PreventionPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
public:
    PreventionPlugin();
    ~PreventionPlugin();

    bool initialize(const QStringList &arguments, QString *errorMessage = 0);
    void extensionsInitialized();

public Q_SLOTS:
    void postCoreOpened();

private:
    ApplicationGeneralPreferencesPage *prefPage;
};

} // namespace Internal
} // namespace Prevention

#endif // Prevention_COREPLUGIN_H
