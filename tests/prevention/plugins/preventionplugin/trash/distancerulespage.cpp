
#include "distancerulespage.h"
#include <accountplugin/constants.h>

#include <accountbaseplugin/constants.h>

#include <utils/log.h>
#include <utils/global.h>
#include <utils/database.h>
#include <translationutils/constanttranslations.h>

#include <coreplugin/icore.h>
#include <coreplugin/isettings.h>
#include <coreplugin/itheme.h>
#include <coreplugin/iuser.h>

#include <coreplugin/constants_icons.h>

#include <QFile>
#include <QTextStream>
#include <QIODevice>
#include <QRegExp>
#include <QLocale>
#include <QDir>
#include <QFile>

enum { WarnDebugMessage = false };

using namespace Account;
using namespace Account::Internal;
using namespace Trans::ConstantTranslations;

static inline Core::ISettings *settings() { return Core::ICore::instance()->settings(); }
static inline Core::ITheme *theme()  { return Core::ICore::instance()->theme(); }
static inline Core::IUser *user() { return Core::ICore::instance()->user(); }


DistanceRulesPage::DistanceRulesPage(QObject *parent) :
        IOptionsPage(parent), m_Widget(0)
{
    setObjectName("DistanceRulesPage");
}

DistanceRulesPage::~DistanceRulesPage()
{
    if (m_Widget) delete m_Widget;
    m_Widget = 0;
}

QString DistanceRulesPage::id() const { return objectName(); }
QString DistanceRulesPage::name() const { return tkTr(Trans::Constants::DISTRULES); }
QString DistanceRulesPage::category() const { return tkTr(Trans::Constants::ACCOUNTANCY); }

void DistanceRulesPage::resetToDefaults()
{
    m_Widget->writeDefaultSettings(settings());
    m_Widget->setDatasToUi();
}

void DistanceRulesPage::applyChanges()
{
    if (WarnDebugMessage)
        LOG("applyChanges");
    if (!m_Widget) {
        return;
    }
    m_Widget->saveToSettings(settings());
}

void DistanceRulesPage::finish() { delete m_Widget; }

void DistanceRulesPage::checkSettingsValidity()
{
    QHash<QString, QVariant> defaultvalues;

    foreach(const QString &k, defaultvalues.keys()) {
        if (settings()->value(k) == QVariant())
            settings()->setValue(k, defaultvalues.value(k));
    }
    settings()->sync();
}

QWidget *DistanceRulesPage::createPage(QWidget *parent)
{
    if (m_Widget)
        delete m_Widget;
    m_Widget = new DistanceRulesWidget(parent);
    return m_Widget;
}

DistanceRulesWidget::DistanceRulesWidget(QWidget *parent) :
        QWidget(parent), m_Model(0), m_Mapper(0)
{
    setObjectName("DistanceRulesWidget");
    setupUi(this);
    addButton->setIcon(theme()->icon(Core::Constants::ICONADD));
    deleteButton->setIcon(theme()->icon(Core::Constants::ICONREMOVE));
    preferredSpinBox->setRange(0,1);
    
    m_Model = new AccountDB::DistanceRulesModel(this);
    distanceRulesUidLabel->setText("");
    m_Mapper = new QDataWidgetMapper(this);
    m_Mapper->setSubmitPolicy(QDataWidgetMapper::AutoSubmit);
    m_Mapper->setModel(m_Model);
    m_Mapper->setCurrentModelIndex(QModelIndex());
    m_Mapper->addMapping(distanceRulesUidLabel,AccountDB::Constants::DISTRULES_UID);
    m_Mapper->addMapping(typeEdit, AccountDB::Constants::DISTRULES_TYPE);
    m_Mapper->addMapping(valueDoubleSpinBox, AccountDB::Constants::DISTRULES_VALUES);
    m_Mapper->addMapping(preferredSpinBox, AccountDB::Constants::DISTRULES_PREF);
    m_Mapper->addMapping(minKmDoubleSpinBox, AccountDB::Constants::DISTRULES_MIN_KM);
    distanceRulesComboBox->setModel(m_Model);
    distanceRulesComboBox->setModelColumn(AccountDB::Constants::DISTRULES_TYPE);
    setDatasToUi();
}

DistanceRulesWidget::~DistanceRulesWidget()
{
}

void DistanceRulesWidget::setDatasToUi()
{
    if (WarnDebugMessage)
        LOG("index row  =" + QString::number(distanceRulesComboBox->currentIndex()));
    m_Mapper->setCurrentIndex(distanceRulesComboBox->currentIndex());
}

void DistanceRulesWidget::saveModel()
{
    if (WarnDebugMessage)
        LOG("currentIndex =" + QString::number(m_Mapper->currentIndex()));
    if (m_Model->isDirty()) {
        bool yes = Utils::yesNoMessageBox(tr("Save changes?"),
                                          tr("You make changes into the distancerules table.\n"
                                             "Do you want to save them?"));
        if (yes) {
           if (!m_Model->submit()) {if (WarnDebugMessage)
    	      qDebug() << __FILE__ << QString::number(__LINE__) << " distancerules no submit ";
                LOG_ERROR(tkTr(Trans::Constants::UNABLE_TO_SAVE_DATA_IN_DATABASE_1).
                                                   arg(tr("distancerules")));
            }
        } 
        else {
            m_Model->revert();
        }
    }
    if (WarnDebugMessage)
        LOG("distanceRules error =" + m_Model->lastError().text());
}

void DistanceRulesWidget::on_distanceRulesComboBox_currentIndexChanged(int index)
{
    Q_UNUSED(index);
    m_Mapper->setCurrentIndex(distanceRulesComboBox->currentIndex());
}

void DistanceRulesWidget::on_addButton_clicked()
{
    if (WarnDebugMessage)
        LOG("rowCount1 =" + QString::number(m_Model->rowCount()));
    if (!m_Model->insertRow(m_Model->rowCount()))
        LOG_ERROR("Unable to add row");
    if (WarnDebugMessage)
        LOG("rowCount2 =" + QString::number(m_Model->rowCount()));
    distanceRulesComboBox->setCurrentIndex(m_Model->rowCount()-1);
    distanceRulesUidLabel->setText(calcDistanceRulesUid());
    distanceRulesUidLabel->setFocus();
    typeEdit->setFocus();
}

void DistanceRulesWidget::on_deleteButton_clicked()
{
    if (!m_Model->removeRow(distanceRulesComboBox->currentIndex())) {
        LOG_ERROR("Unable to remove row");
    }
    distanceRulesComboBox->setCurrentIndex(m_Model->rowCount() - 1);
}

void DistanceRulesWidget::saveToSettings(Core::ISettings *sets)
{
    Q_UNUSED(sets);
    if (!m_Model->submit()) {
        LOG_ERROR(tkTr(Trans::Constants::UNABLE_TO_SAVE_DATA_IN_DATABASE_1).arg(tr("distancerules")));
        Utils::warningMessageBox(tr("Can not submit distancerules to your personnal database."),
                                 tr("An error occured during distancerules saving. Datas are corrupted."));
    }
    connect(typeEdit,SIGNAL(textEdited(const QString &)),distanceRulesComboBox,SLOT(setEditText(const QString &)));
    update();
}

void DistanceRulesWidget::writeDefaultSettings(Core::ISettings *s)
{
    Q_UNUSED(s);
}

void DistanceRulesWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        retranslateUi(this);
        addButton->setToolTip(tkTr(Trans::Constants::FILENEW_TEXT));
        deleteButton->setToolTip(tkTr(Trans::Constants::REMOVE_TEXT));
        break;
    default:
        break;
    }
}

QString DistanceRulesWidget::calcDistanceRulesUid(){
    QString uuidStr;
    uuidStr = Utils::Database::createUid();
    return uuidStr;
}






