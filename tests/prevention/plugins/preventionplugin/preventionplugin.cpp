/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "preventionplugin.h"
#include "preventCore.h"
#include "connexion.h"

#include <extensionsystem/pluginmanager.h>
#include <utils/log.h>

#include <coreplugin/icore.h>
#include <coreplugin/appaboutpage.h>
#include <coreplugin/dialogs/commonaboutpages.h>
#include <coreplugin/dialogs/commondebugpages.h>
#include <coreplugin/dialogs/pluginaboutpage.h>
#include <coreplugin/translators.h>
#include <coreplugin/dialogs/applicationgeneralpreferences.h>

#include <QtCore/QtPlugin>
#include <QDebug>

using namespace Prevention::Internal;

PreventionPlugin::PreventionPlugin() :
    prefPage(0)
{
    qWarning() << "PreventionPlugin::PreventionPlugin()";
    Core::ICore::instance()->translators()->addNewTranslator("lib_utils");
    Core::ICore::instance()->translators()->addNewTranslator("lib_translations");

    connect(Core::ICore::instance(), SIGNAL(coreOpened()), this, SLOT(postCoreOpened()));
}

PreventionPlugin::~PreventionPlugin()
{
    qWarning() << "PreventionPlugin::~CorePlugin()";
}

bool PreventionPlugin::initialize(const QStringList &arguments, QString *errorMessage)
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "PreventionPlugin::initialize";
    return true;
}

void PreventionPlugin::extensionsInitialized()
{
    if (Utils::Log::debugPluginsCreation())
        qWarning() << "PreventionPlugin::extensionsInitialized";


    addAutoReleasedObject(new Core::PluginAboutPage(pluginSpec(), this));

}

void PreventionPlugin::postCoreOpened()
{
    Common::Connexion * c = new Common::Connexion;
    if (!c->connect())
    {
          qWarning() << __FILE__ << QString::number(__LINE__) << "cannot connect to database" ;
        }
    PreventCore *p = new PreventCore;
    p->run();
}

Q_EXPORT_PLUGIN(PreventionPlugin)
