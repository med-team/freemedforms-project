/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef USER_H
#define USER_H

#include <coreplugin/iuser.h>

class User : public Core::IUser
{
public:
    User(QObject *parent = 0);
    ~User();

    virtual void clear();
    virtual bool has(const int ref) const;

    virtual QVariant value(const int ref) const;
    virtual bool setValue(const int ref, const QVariant &value);

    virtual QString toXml() const;
    virtual bool fromXml(const QString &xml);

};

#endif // USER_H
