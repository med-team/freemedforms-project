/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef FREEICD_COMMANDLINEPARSER_H
#define FREEICD_COMMANDLINEPARSER_H

#include <coreplugin/icommandline.h>

#include <QString>
#include <QVariant>

/**
 * \file ./tests/prevention/plugins/coreplugin/commandlineparser.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Core {
    class IPatient;
    class Patient;

namespace Internal {
class CommandLinePrivate;

class CommandLine  : public Core::ICommandLine
{
public:
    CommandLine();
    ~CommandLine();

    QVariant value(int param, const QVariant &def = QVariant()) const;
    QString paramName(int param) const;

    void feedPatientDatas(Core::IPatient *patient);

private:
    CommandLinePrivate *d;
};

}  // End namespace Internal
}  // End namespace Core

#endif // FREEICD_COMMANDLINEPARSER_H
