/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
#include "commandlineparser.h"

#include <coreplugin/ipatient.h>
#include <coreplugin/icommandline.h>

#include <utils/global.h>
#include <utils/log.h>
#include <translationutils/constants.h>
#include <translationutils/trans_filepathxml.h>
#include <translationutils/trans_msgerror.h>

#include <QApplication>
#include <QStringList>
#include <QFile>
#include <QDir>
#include <QDomElement>
#include <QDomDocument>

#include <QDebug>

using namespace Core;
using namespace Internal;

CommandLine::CommandLine() :
        Core::ICommandLine()
{
}

CommandLine::~CommandLine()
{
}

void CommandLine::feedPatientDatas(Core::IPatient *patient)
{
    Q_UNUSED(patient);
}

QVariant CommandLine::value(int param, const QVariant &def) const
{
    Q_UNUSED(param);
    Q_UNUSED(def);
    return QVariant();
}

QString CommandLine::paramName(int param) const
{
    Q_UNUSED(param);
    return QString::null;
}
