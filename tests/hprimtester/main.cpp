#include "../../libs/utils/hprimparser.h"
#include "../../libs/utils/global.h"

#include <QCoreApplication>
#include <QString>
#include <QRegExp>
#include <QHash>

#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

                         qWarning() << "Testing wrong encoding correction";

            QString file = app.applicationDirPath();
    if (Utils::isRunningOnMac())
        file += "/../../..";
    file += "/tests/wrong_encoding.hpr";


    QString content = Utils::readTextFile(file, "MacRoman");


    qWarning() << Utils::correctTextAccentEncoding(content);






    return 0;
}
