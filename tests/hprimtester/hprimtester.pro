#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

# first definitions
TEMPLATE = app
TARGET = hprimtester

UTILS=../../libs/utils/
TRANS=../../libs/translationutils/

INCLUDEPATH=../../libs

QT += sql xml network

HEADERS += \
    $${UTILS}/log.h \
    $${UTILS}/global.h \
    $${UTILS}/licenseterms.h \
    $${TRANS}/constants.h \
    $${UTILS}/hprimparser.h

SOURCES += main.cpp \
    $${UTILS}/log.cpp \
    $${UTILS}/global.cpp \
    $${UTILS}/licenseterms.cpp \
    $${TRANS}/constanttranslations.cpp \
    $${UTILS}/hprimparser.cpp



