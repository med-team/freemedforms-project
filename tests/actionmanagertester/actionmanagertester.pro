#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

TARGET = actionmanagertester
CONFIG += console
TEMPLATE = app

# include general configuration
include( ../../config.pri )
include( $${PACKAGE_LIBS_SOURCES}/sdk_toolkit.pri)
QT *= sql \
    network \
    xml \
    gui

# sources
SOURCES += main.cpp \
           TestWidget.cpp \
           MainWindow.cpp

HEADERS += MainWindow.h \
           TestWidget.h
