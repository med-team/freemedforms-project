#include "mainwindow.h"
#include "zipcountrycompleters.h"
#include "frenchsocialnumber.h"

#include "ui_mainwindow.h"


#include <QDir>
#include <QDebug>



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ZipCountryCompleters *completer = new ZipCountryCompleters(this);
    ui->zipLineEdit->setText("62000");
    ui->cityLineEdit->setText("ARRAES");
    completer->setCountryComboBox(ui->comboBox);
    completer->setZipLineEdit(ui->zipLineEdit);
    completer->setCityLineEdit(ui->cityLineEdit);
    completer->checkData();

    ui->gridLayout->addWidget(new FrenchSocialNumber(this), 10, 0, 1, 2);

}

MainWindow::~MainWindow()
{
    delete ui;
}
