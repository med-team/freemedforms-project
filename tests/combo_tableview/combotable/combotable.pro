#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

#-------------------------------------------------
#
# Project created by QtCreator 2010-08-20T11:42:43
#
#-------------------------------------------------

QT       += core gui

TARGET = combotable
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ../../../libs/utils/widgets/combowithfancybutton.cpp

HEADERS  += mainwindow.h \
    ../../../libs/utils/widgets/combowithfancybutton.h

FORMS    += mainwindow.ui
