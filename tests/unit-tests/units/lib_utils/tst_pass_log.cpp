/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include <utils/global.h>

#include "../../autotest.h"

#include <QDebug>
#include <QTest>


class tst_UtilsLoginAndPass : public QObject
{
    Q_OBJECT
public:

private slots:
    void initTestCase()
    {
    }

    void testLoginFunctions()
    {
        QString clear = "jhqsd_sdfkjh132234.45125kkjhoanlcvok";
        QString control = "amhxc2Rfc2Rma2poMTMyMjM0LjQ1MTI1a2tqaG9hbmxjdm9r";
        QCOMPARE(Utils::loginForSQL(clear), control);
        QCOMPARE(Utils::loginFromSQL(control), clear);
    }

    void testPasswordFunctions()
    {
        QString clear = "jhqsd_sdfkjh132234.45125kkjhoanlcvok";
        QString key = "ijbkdsf2452kqjsnfaAERAz";
                         ;
        QCOMPARE(Utils::cryptPassword(clear), QString("7mR6sSYyStkB0yD6KkGcAHtE2m4="));
        QCOMPARE(Utils::crypt(clear, key), QByteArray("MGIzZjAxMWEwNTZkMjExZTNjMDEyMzU4N2Y2Nzc4NDA1MDYzNWU0ZTU3NWM2ODVkM2EzZTNjM2IzZTM5MDE1MTAyMjExZjAy"));

        QCOMPARE(Utils::decrypt(QByteArray("NWQ1ZjVhNGEyNzM1MWQxMjEwNWM1OTJiNWEwMzQ0NDQ0NTAzMWU0Mjc5NTQxOTFhMWQzMzA4MWUxOTU2NWUwOTU0NDE0NDUy")), clear);
        QCOMPARE(Utils::decrypt(QByteArray("MGIzZjAxMWEwNTZkMjExZTNjMDEyMzU4N2Y2Nzc4NDA1MDYzNWU0ZTU3NWM2ODVkM2EzZTNjM2IzZTM5MDE1MTAyMjExZjAy"), QString("ijbkdsf2452kqjsnfaAERAz")), clear);

    }

    void cleanupTestCase()
    {}
};

DECLARE_TEST(tst_UtilsLoginAndPass)
#include "tst_pass_log.moc"



