/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include <utils/global.h>

#include "../../autotest.h"

#include <QDebug>
#include <QTest>

class tst_Html : public QObject
{
    Q_OBJECT
public:
    QString css, in, out;

private slots:
    void initTestCase()
    {
        css = "<style type=\"text/css\">"
                ".__ident__formContent {"
                "  font-size: 12pt;"
                "  color: black;"
                "  border: 2px solid gray;"
                "}"
                " "
                ".__ident__formContent .formHeader {"
                "}"
                "</style>";
    }

    void extractHtmlBody()
    {
        in = "<body>Whoooa!</body>";
        QCOMPARE(Utils::htmlBodyContent(in), QString("<p>Whoooa!</p>"));


        out = "<div class=\"__ident__formContent\"> <!-- comment -->"
                "<div class=\"formHeader\">"
                "<div class=\"formLabel\">[[EpisodeFormLabel]]</div>"
                "</div> <!-- blah -->";

        const QString body =
                "<html><!-- comment -->"
                "<body%1>%2</body>"
                "<!-- another comment --></html>";

        in = body.arg("", out);

        QCOMPARE(Utils::htmlBodyContent(in), QString("<p>%1</p>").arg(out));


        in = body.arg(" style=\"background: white;\"", out);
        QCOMPARE(Utils::htmlBodyContent(in), QString("<p style=\"background: white;\">%1</p>").arg(out));
    }

    void extractMisformedBody()
    {
        in = "<body>Whoooa!";
        QCOMPARE(Utils::htmlBodyContent(in), QString("<p>Whoooa!</p>"));
    }

    void extractCssFromBody()
    {
        const QString html =
                "<html>"
                "<body>\n"
                "%1\n"
                "<div class=\"__ident__formContent\">"
                "<div class=\"formHeader\">"
                "<div class=\"formLabel\">[[EpisodeFormLabel]]</div>"
                "</div> <!-- formHeader -->"
                "</body>"
                "</html>";

        in = html.arg(css);
        out = html.arg("");

        QCOMPARE(Utils::htmlTakeAllCssContent(in), css);
        QVERIFY(in == out);
    }

    void extractCssFromHeader()
    {
        const QString html =
                "<html>"
                "<header>"
                "%1\n"
                "</header>"
                "<body>\n"
                "<div class=\"__ident__formContent\">"
                "<div class=\"formHeader\">"
                "<div class=\"formLabel\">[[EpisodeFormLabel]]</div>"
                "</div> <!-- formHeader -->"
                "</body>"
                "</html>";

        in = html.arg(css);
        out = html.arg("");

        QCOMPARE(Utils::htmlTakeAllCssContent(in), css);
        QVERIFY(in == out);
    }

    void extractDoubleCss()
    {
        QString doubleCss = css + css;
        const QString html =
                "<html>"
                "<header>"
                "%1\n"
                "</header>"
                "<body>\n"
                "%1\n"
                "<div class=\"__ident__formContent\">"
                "<div class=\"formHeader\">"
                "<div class=\"formLabel\">[[EpisodeFormLabel]]</div>"
                "</div> <!-- formHeader -->"
                "</body>"
                "</html>";

        in = html.arg(css);
        out = html.arg("");

        QCOMPARE(Utils::htmlTakeAllCssContent(in), doubleCss);
        QVERIFY(in == out);
    }

    void removeLinkTags()
    {
        const QString html =
                "<html>"
                "<head>"
                "</head>"
                "<body>\n"
                "%1\n"
                "<div class=\"__ident__formContent\">"
                "<div class=\"formHeader\">"
                "<div class=\"formLabel\">[[EpisodeFormLabel]]</div>"
                "</div> <!-- formHeader -->"
                "%2\n"
                "</body>"
                "</html>";

        in = html.arg("<a href=\"kjlkdjflqkjdflqksdfqdsfq\">").arg("</a>");
        out = html.arg("").arg("");
        QVERIFY(Utils::htmlRemoveLinkTags(in) == out);
    }

    void extractCssFileLinks()
    {
        const QString html =
                "<html>\n"
                "<head>\n"
                "<meta>\n"
                "<link rel=\"stylesheet\" href=\"../style/test1.css\" type=\"text/css\">\n"
                "<link rel=\"stylesheet\" href=\"../style/test2.css\" type=\"text/css\">\n"
                "</meta>\n"
                "</head>\n"
                "<body>\n"
                "<div class=\"__ident__formContent\">\n"
                "<div class=\"formHeader\">\n"
                "<div class=\"formLabel\">[[EpisodeFormLabel]]</div>\n"
                "</div> <!-- formHeader -->\n"
                "</div>\n"
                "</body>\n"
                "</html>\n";
        QStringList correct;
        correct << "../style/test1.css"
                << "../style/test2.css";
        QStringList result = Utils::htmlGetLinksToCssContent(html);
        QVERIFY(correct == result);
    }


};

DECLARE_TEST(tst_Html)
#include "tst_html.moc"
