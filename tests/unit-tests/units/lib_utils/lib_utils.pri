#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

SOURCES += \
#    $${PWD}/testhtmlcontent.cpp \
    \
#    $${PWD}/tst_html.cpp \
#    $${PWD}/os_config.cpp \
#    $${PWD}/tst_fileaccess.cpp \
#    $${PWD}/tst_datetime.cpp \
#    $${PWD}/tst_xml.cpp \
#    $${PWD}/tst_pass_log.cpp \
#    $${PWD}/tst_versionnumber.cpp \
#    $${PWD}/tst_printaxishelper.cpp \
#    $${PWD}/tst_httpdownloader.cpp \
#    $${PWD}/tst_hprim.cpp \
    $${PWD}/tst_database.cpp \
#    $${PWD}/tst_databasecsvimport.cpp \
#    $${PWD}/tst_genericdescription.cpp \
#    $${PWD}/tst_databaseconnector.cpp \
#    $${PWD}/tst_dircopy.cpp \
#    $${PWD}/tst_passwordhashing.cpp

#HEADERS += \
#    $${PWD}/testhtmlcontent.h

#FORMS += \
#    $${PWD}/testhtmlcontent.ui
