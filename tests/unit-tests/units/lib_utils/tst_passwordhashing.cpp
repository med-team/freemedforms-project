/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include <utils/passwordandlogin.h>
#include <utils/randomizer.h>
#include "../../autotest.h"

#include <QCryptographicHash>
#include <QTest>
#include <QDebug>
#include <QTest>



class tst_UtilsPasswordHashing : public QObject
{
    Q_OBJECT

    const QString clearPass = "CryptMe_CryptMe_CryptMe_CryptMe_CryptMe";
    Utils::PasswordCrypter crypter;

private slots:
    void initTestCase()
    {
    }

    void test_passwordHashing_defaultValue()
    {
#if (QT_VERSION < 0x050000)
        QVERIFY(Utils::PasswordCrypter::Default == Utils::PasswordCrypter::SHA1);
#elif (QT_VERSION >= 0x050000 && QT_VERSION < 0x050100)
        QVERIFY(Utils::PasswordCrypter::Default == Utils::PasswordCrypter::SHA512);
#else
        QVERIFY(Utils::PasswordCrypter::Default == Utils::PasswordCrypter::SHA3_512);
#endif
    }

    void test_passwordHashing_Qt48x()
    {
        QString cryptedPass = crypter.cryptPassword(clearPass, Utils::PasswordCrypter::SHA1);

        QVERIFY(crypter.checkPrefix(cryptedPass, Utils::PasswordCrypter::SHA1) == true);

        QVERIFY(crypter.checkPassword("Clear", cryptedPass) == false);
        QVERIFY(crypter.checkPassword("CryptMe_CryptMe", cryptedPass) == false);
        QVERIFY(crypter.checkPassword("CryptMe_CryptMe_CryptMe", cryptedPass) == false);
        QVERIFY(crypter.checkPassword(clearPass, cryptedPass) == true);
    }

#if (QT_VERSION >= 0x050000)
        void test_passwordHashing_Qt50x()
        {
            QString cryptedPass = crypter.cryptPassword(clearPass, Utils::PasswordCrypter::SHA256);

            QVERIFY(crypter.checkPrefix(cryptedPass, Utils::PasswordCrypter::SHA1) == false);
            QVERIFY(crypter.checkPrefix(cryptedPass, Utils::PasswordCrypter::SHA256) == true);
            QVERIFY(crypter.checkPrefix(cryptedPass, Utils::PasswordCrypter::SHA512) == false);

            QVERIFY(crypter.checkPassword("Clear", cryptedPass) == false);
            QVERIFY(crypter.checkPassword("CryptMe_CryptMe", cryptedPass) == false);
            QVERIFY(crypter.checkPassword("CryptMe_CryptMe_CryptMe", cryptedPass) == false);
            QVERIFY(crypter.checkPassword(clearPass, cryptedPass) == true);

            cryptedPass = crypter.cryptPassword(clearPass, Utils::PasswordCrypter::SHA512);
            QVERIFY(crypter.checkPrefix(cryptedPass, Utils::PasswordCrypter::SHA1) == false);
            QVERIFY(crypter.checkPrefix(cryptedPass, Utils::PasswordCrypter::SHA256) == false);
            QVERIFY(crypter.checkPrefix(cryptedPass, Utils::PasswordCrypter::SHA512) == true);

            QVERIFY(crypter.checkPassword("Clear", cryptedPass) == false);
            QVERIFY(crypter.checkPassword("CryptMe_CryptMe", cryptedPass) == false);
            QVERIFY(crypter.checkPassword("CryptMe_CryptMe_CryptMe", cryptedPass) == false);
            QVERIFY(crypter.checkPassword(clearPass, cryptedPass) == true);
        }
#endif

#if (QT_VERSION >= 0x050100)
        void test_passwordHashing_Qt51x()
        {
            QString cryptedPass = crypter.cryptPassword(clearPass, Utils::PasswordCrypter::SHA3_256);

            QVERIFY(crypter.checkPrefix(cryptedPass, Utils::PasswordCrypter::SHA1) == false);
            QVERIFY(crypter.checkPrefix(cryptedPass, Utils::PasswordCrypter::SHA256) == false);
            QVERIFY(crypter.checkPrefix(cryptedPass, Utils::PasswordCrypter::SHA512) == false);
            QVERIFY(crypter.checkPrefix(cryptedPass, Utils::PasswordCrypter::SHA3_256) == true);
            QVERIFY(crypter.checkPrefix(cryptedPass, Utils::PasswordCrypter::SHA3_512) == false);

            QVERIFY(crypter.checkPassword("Clear", cryptedPass) == false);
            QVERIFY(crypter.checkPassword("CryptMe_CryptMe", cryptedPass) == false);
            QVERIFY(crypter.checkPassword("CryptMe_CryptMe_CryptMe", cryptedPass) == false);
            QVERIFY(crypter.checkPassword(clearPass, cryptedPass) == true);

            cryptedPass = crypter.cryptPassword(clearPass, Utils::PasswordCrypter::SHA3_512);
            QVERIFY(crypter.checkPrefix(cryptedPass, Utils::PasswordCrypter::SHA1) == false);
            QVERIFY(crypter.checkPrefix(cryptedPass, Utils::PasswordCrypter::SHA256) == false);
            QVERIFY(crypter.checkPrefix(cryptedPass, Utils::PasswordCrypter::SHA512) == false);
            QVERIFY(crypter.checkPrefix(cryptedPass, Utils::PasswordCrypter::SHA3_256) == false);
            QVERIFY(crypter.checkPrefix(cryptedPass, Utils::PasswordCrypter::SHA3_512) == true);

            QVERIFY(crypter.checkPassword("Clear", cryptedPass) == false);
            QVERIFY(crypter.checkPassword("CryptMe_CryptMe", cryptedPass) == false);
            QVERIFY(crypter.checkPassword("CryptMe_CryptMe_CryptMe", cryptedPass) == false);
            QVERIFY(crypter.checkPassword(clearPass, cryptedPass) == true);
        }
#endif

    void test_passwordHashLength()
    {
        QString clearPass;
        Utils::Randomizer random;
        for(int i = 0; i < 100; ++i) {
            clearPass += random.randomString(1);

            QVERIFY(crypter.cryptPassword(clearPass).length() < 200);

#if (QT_VERSION >= 0x050000)
            QVERIFY(crypter.cryptPassword(clearPass, Utils::PasswordCrypter::SHA256).length() < 200);
            QVERIFY(crypter.cryptPassword(clearPass, Utils::PasswordCrypter::SHA512).length() < 200);
#endif

#if (QT_VERSION >= 0x050100)
            QVERIFY(crypter.cryptPassword(clearPass, Utils::PasswordCrypter::SHA3_256).length() < 200);
            QVERIFY(crypter.cryptPassword(clearPass, Utils::PasswordCrypter::SHA3_512).length() < 200);
#endif
        }

    }

    void test_passwordUpdate()
    {


        QString clearPass;
        Utils::Randomizer random;
        for(int i = 0; i < 100; ++i) {
            clearPass += random.randomString(1);
            QString oldPass = Utils::cryptPassword(clearPass);

            QVERIFY(crypter.checkPassword(clearPass, oldPass));

#if (QT_VERSION >= 0x050000)
            QVERIFY(crypter.checkPassword(clearPass, oldPass));
#endif

#if (QT_VERSION >= 0x050100)
            QVERIFY(crypter.checkPassword(clearPass, oldPass));
#endif
        }
    }

    void cleanupTestCase()
    {
    }
};

DECLARE_TEST(tst_UtilsPasswordHashing)
#include "tst_passwordhashing.moc"


