/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include <utils/printaxishelper.h>

#include "../../autotest.h"

#include <QDebug>
#include <QTest>

class tst_PrintAxisHelper : public QObject
{
    Q_OBJECT
public:

private slots:
    void initTestCase()
    {
    }

};

DECLARE_TEST(tst_PrintAxisHelper)
#include "tst_printaxishelper.moc"

