/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include <utils/global.h>

#include "../../autotest.h"

#include <QDebug>
#include <QTest>
#include <QCryptographicHash>


class tst_DateTime : public QObject
{
    Q_OBJECT
public:

private slots:
    void initTestCase()
    {
    }

    void testRoundDateTime()
    {
        QDateTime dt = QDateTime(QDate::currentDate(), QTime(12,12,12));
        QDateTime rd = QDateTime(QDate::currentDate(), QTime(12,15,00));
        QCOMPARE(Utils::roundDateTime(dt, 15), rd);

        dt = QDateTime(QDate::currentDate(), QTime(12,15,12));
        rd = QDateTime(QDate::currentDate(), QTime(12,30,00));
        QCOMPARE(Utils::roundDateTime(dt, 15), rd);

        dt = QDateTime(QDate::currentDate(), QTime(12,45,00,01));
        rd = QDateTime(QDate::currentDate(), QTime(13,00,00));
        QCOMPARE(Utils::roundDateTime(dt, 15), rd);

        dt = QDateTime(QDate::currentDate(), QTime(12,44,59,99));
        rd = QDateTime(QDate::currentDate(), QTime(12,45,00));
        QCOMPARE(Utils::roundDateTime(dt, 15), rd);

        dt = QDateTime(QDate::currentDate(), QTime(12,01,10,01));
        rd = QDateTime(QDate::currentDate(), QTime(12,15,00));
        QCOMPARE(Utils::roundDateTime(dt, 15), rd);

        int div = 15;
        int max = 60*div - 2;
        dt = QDateTime(QDate::currentDate(), QTime(12,0,1));
        for(int i = 0; i < max; ++i) {
            dt = dt.addMSecs(1000);
            rd = QDateTime(QDate::currentDate(), QTime(12,15,00));
            QCOMPARE(Utils::roundDateTime(dt, div), rd);
        }
        dt = dt.addMSecs(2000);
        rd = QDateTime(QDate::currentDate(), QTime(12,30,00));
        QCOMPARE(Utils::roundDateTime(dt, div), rd);
    }

    void testInRange()
    {
    }

    void cleanupTestCase()
    {}
};

DECLARE_TEST(tst_DateTime)
#include "tst_datetime.moc"

