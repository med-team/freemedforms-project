/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include <utils/global.h>

#include "../../autotest.h"

#include <QDebug>
#include <QTest>
#include <QDir>


class tst_OsConfig : public QObject
{
    Q_OBJECT
public:

private slots:
    void initTestCase()
    {
    }

    void testOsConfig()
    {
#if defined(Q_OS_MAC) || defined(Q_OS_MAC64) || defined(Q_OS_MACX)
        QVERIFY(Utils::isRunningOnMac() == true);
        QVERIFY(Utils::isRunningOnWin() == false);
        QVERIFY(Utils::isRunningOnLinux() == false);
        QVERIFY(Utils::isRunningOnFreebsd() == false);
#endif

#if defined(Q_OS_WIN32)
        QVERIFY(Utils::isRunningOnMac() == false);
        QVERIFY(Utils::isRunningOnWin() == true);
        QVERIFY(Utils::isRunningOnLinux() == false);
        QVERIFY(Utils::isRunningOnFreebsd() == false);
#endif

#if defined(Q_OS_LINUX)
        QVERIFY(Utils::isRunningOnMac() == false);
        QVERIFY(Utils::isRunningOnWin() == false);
        QVERIFY(Utils::isRunningOnLinux() == true);
        QVERIFY(Utils::isRunningOnFreebsd() == false);
#endif

#if defined(Q_OS_FREEBSD)
        QVERIFY(Utils::isRunningOnMac() == false);
        QVERIFY(Utils::isRunningOnWin() == false);
        QVERIFY(Utils::isRunningOnLinux() == false);
        QVERIFY(Utils::isRunningOnFreebsd() == true);
#endif
    }

    void testCompilationFlags()
    {
#ifdef DEBUG_WITHOUT_INSTALL
        QVERIFY(Utils::isDebugWithoutInstallCompilation() == true);
        QVERIFY(Utils::isReleaseCompilation() == false);
#endif

#ifndef DEBUG_WITHOUT_INSTALL
        QVERIFY(Utils::isDebugWithoutInstallCompilation() == false);
#endif

#ifdef LINUX_INTEGRATED
        QVERIFY(Utils::isLinuxIntegratedCompilation() == true);
        QVERIFY(Utils::isRunningOnMac() == false);
        QVERIFY(Utils::isRunningOnWin() == false);
#endif

#ifdef DEBUG
        QVERIFY(Utils::isReleaseCompilation() == false);
#endif
#ifdef RELEASE
        QVERIFY(Utils::isReleaseCompilation() == true);
        QVERIFY(Utils::isDebugWithoutInstallCompilation() == false);
#endif
    }

    void testApplicationLibPath()
    {
        QStringList libs = Utils::applicationPluginsPath("freemedforms", "lib64");
#ifdef DEBUG_WITHOUT_INSTALL
        QString test = qApp->applicationDirPath();
        if (Utils::isRunningOnMac())
            test += "/../../../plugins";
        else if (Utils::isRunningOnLinux())
            test += "/plugins";
        else if (Utils::isRunningOnWin())
            test += "/plugins";
        test = QDir::cleanPath(test);
        QVERIFY(libs.contains(test, Qt::CaseInsensitive) == true);
#endif
#ifdef RELEASE
        if (Utils::isRunningOnMac())
            test += "/../plugins";
        else if (Utils::isRunningOnWin())
            test += "/plugins";
        test = QDir::cleanPath(test);
        QVERIFY(libs.contains(test, Qt::CaseInsensitive) == true);
#endif
    }

    void cleanupTestCase()
    {}
};

DECLARE_TEST(tst_OsConfig)
#include "os_config.moc"
