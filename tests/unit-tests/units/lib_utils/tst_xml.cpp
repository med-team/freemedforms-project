/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include <utils/global.h>

#include "../../autotest.h"

#include <QDebug>
#include <QTest>
#include <QCryptographicHash>


class tst_UtilsXml : public QObject
{
    Q_OBJECT
public:

private slots:
    void initTestCase()
    {
    }

    void testHashFromToXml()
    {
        QHash<QString,QString> toXml;
        toXml.insert("TAG1", "VALUE IS A <SCRIPT>");
        toXml.insert("TAG2", "VALUE IS A 23423.2342");
        toXml.insert("TAG3", ";qsdf;&amp;&lt;&gt;");
        QString xml = Utils::createXml("MAIN", toXml, 2, false);
        QString control = "<MAIN>\n"
                "  <TAG1>VALUE IS A &lt;SCRIPT></TAG1>\n"
                "  <TAG2>VALUE IS A 23423.2342</TAG2>\n"
                "  <TAG3>;qsdf;&amp;amp;&amp;lt;&amp;gt;</TAG3>\n"
              "</MAIN>\n";

        QHash<QString,QString> fromXml;
        Utils::readXml(xml, "MAIN", fromXml, false);
        QCOMPARE(toXml, fromXml);

        control = "<MAIN>\n"
                "  <TAG1>VkFMVUUgSVMgQSA8U0NSSVBUPg==</TAG1>\n"
                "  <TAG2>VkFMVUUgSVMgQSAyMzQyMy4yMzQy</TAG2>\n"
                "  <TAG3>O3FzZGY7JmFtcDsmbHQ7Jmd0Ow==</TAG3>\n"
                "</MAIN>\n";
        xml = Utils::createXml("MAIN", toXml, 2, true);

        Utils::readXml(xml, "MAIN", fromXml, true);
        QCOMPARE(toXml, fromXml);
    }



    void cleanupTestCase()
    {}
};

DECLARE_TEST(tst_UtilsXml)
#include "tst_xml.moc"


