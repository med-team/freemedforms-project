/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include <utils/global.h>

#include "../../autotest.h"

#include <QDebug>
#include <QTest>
#include <QCryptographicHash>


class tst_FileAccess : public QObject
{
    Q_OBJECT
public:
    QString fileName;
    QString content;

private slots:
    void initTestCase()
    {
        fileName = qApp->applicationDirPath() + "/test.txt";
        content = "abcdefghijklmnopqrstuvwxyz\nABCDEFGHIJHKLMNOPQRSTUVWXYZ\n";
        content += "<ACCENTS>\n";
        content += "  ÀàÄäâÂãÃ\n";
        content += "  éÉèÈëËêÊ\n";
        content += "  ïÏîÎìÌ\n";
        content += "  òÒöÖôÔ\n";
        content += "  ùÙüÜûÛ\n";
        content += "  ÿŸ\n";
        content += "  ñÑ\n";
        content += "</ACCENTS>\n";
    }

    void testBasicFileAccess()
    {
        QFile(fileName).remove();

        QStringList enc;
        enc << "UTF-8"
            << "UTF-16"
               ;
        foreach(const QString &e, enc) {
            QVERIFY(Utils::saveStringToEncodedFile(content, fileName, e, Utils::Overwrite, Utils::DontWarnUser) == true);
            QVERIFY(QFile(fileName).exists() == true);
            QCOMPARE(Utils::readTextFile(fileName, e, Utils::DontWarnUser), content);
            QFile(fileName).remove();
        }
        QVERIFY(Utils::saveStringToFile(content, fileName, Utils::Overwrite, Utils::DontWarnUser) == true);
        QVERIFY(QFile(fileName).exists() == true);
        QCOMPARE(Utils::readTextFile(fileName, Utils::DontWarnUser), content);

        QCOMPARE(Utils::isFileExists(fileName), fileName);
        QCOMPARE(Utils::isDirExists(QFileInfo(fileName).absolutePath()), QFileInfo(fileName).absolutePath());
        fileName += "_";
        QCOMPARE(Utils::isFileExists(fileName), QString());
        QCOMPARE(Utils::isDirExists(QFileInfo(fileName).absolutePath() + "_"), QString());
        fileName.chop(1);

        QVERIFY(QFile(fileName).exists() == true);
        QByteArray s = QCryptographicHash::hash(content.toUtf8(), QCryptographicHash::Md5);
        QCOMPARE(Utils::fileMd5(fileName), s.toHex());
        s = QCryptographicHash::hash(content.toUtf8(), QCryptographicHash::Sha1);
        QCOMPARE(Utils::fileSha1(fileName), s.toHex());
#if QT_VERSION >= 0x050000
        s = QCryptographicHash::hash(content.toUtf8(), QCryptographicHash::Sha256);
        QCOMPARE(Utils::fileSha256(fileName), s.toHex());
#endif

        QFile(fileName).remove();
    }

    void cleanupTestCase()
    {}
};

DECLARE_TEST(tst_FileAccess)
#include "tst_fileaccess.moc"
