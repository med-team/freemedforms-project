/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors:                                                         *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef HTMLCONTENTTESTER_H
#define HTMLCONTENTTESTER_H

#include <QDialog>

/**
 * \file ./tests/unit-tests/units/lib_utils/testhtmlcontent.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Tests {
namespace Internal {
class HtmlContentTesterPrivate;
} // namespace Internal

class HtmlContentTester : public QDialog
{
    Q_OBJECT

public:
    explicit HtmlContentTester(QWidget *parent = 0);
    ~HtmlContentTester();
    bool initialize();

private Q_SLOTS:
    void refreshBrowserContent();

private:
    Internal::HtmlContentTesterPrivate *d;
};

} // namespace Tests

#endif  // HTMLCONTENTTESTER_H

