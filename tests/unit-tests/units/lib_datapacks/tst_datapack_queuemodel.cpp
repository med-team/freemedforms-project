/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include <utils/global.h>

#include "../../autotest.h"

#include <utils/log.h>
#include <utils/global.h>
#include <utils/randomizer.h>
#include <datapackutils/pack.h>
#include <datapackutils/packdescription.h>
#include <datapackutils/server.h>
#include <datapackutils/servercreation/packcreationqueue.h>
#include <datapackutils/servercreation/packcreationmodel.h>
#include <datapackutils/constants.h>

#include <quazip/JlCompress.h>

#include <QDebug>
#include <QTest>
#include <QDir>
#include <QFileInfo>
#include <QString>



namespace {

static void populatePackDescription(DataPack::PackDescription &desc, QHash<int, QString> &values)
{
    QString uid;
    for(int i = 0; i < DataPack::PackDescription::MaxParam; ++i) {
        uid = Utils::createUid();
        values.insert(i, uid);
        desc.setData(i, uid);
    }

    values.insert(DataPack::PackDescription::Size, "2349234");
    desc.setData(DataPack::PackDescription::Size, "2349234");
    uid = Utils::createUid();
    values.insert(DataPack::PackDescription::UnzipToPath, uid);
    desc.setData(DataPack::PackDescription::UnzipToPath, uid);
    uid = Utils::createUid();
    values.insert(DataPack::PackDescription::Md5, uid);
    desc.setData(DataPack::PackDescription::Md5, uid);
    uid = Utils::createUid();
    values.insert(DataPack::PackDescription::Sha1, uid);
    desc.setData(DataPack::PackDescription::Sha1, uid);
    uid = Utils::createUid();
    values.insert(DataPack::PackDescription::DataType, uid);
    desc.setData(DataPack::PackDescription::DataType, uid);
    uid = Utils::createUid();
    values.insert(DataPack::PackDescription::InstalledFiles, uid);
    desc.setData(DataPack::PackDescription::InstalledFiles, uid);
}

QString createFakeContent(int size)
{
    QString content;
    while (content.size() < size)
        content += Utils::createUid();
    return content.left(size);
}

} // Anonymous namespace

class tst_DataPack_QueueModel : public QObject
{
    Q_OBJECT

    const int loop = 10;
    QString _tempPath;
    Utils::Randomizer random;
    QHash<QString, QString> _tempPathToDescriptionPath; // Key= tmppath for the request equivalent path for pack description file ; Value = pack description file path
    QList<DataPack::PackCreationQueue *> queues;

    void createFakePackDescriptionAndContent(QString path, DataPack::RequestedPackCreation &request, DataPack::RequestedPackCreation::ContentType type = DataPack::RequestedPackCreation::UnzippedFile, uint numberOfContents = 1)
    {
        using namespace DataPack;
        path = QString("%1/%2").arg(path).arg(Utils::createUid());
        QVERIFY(QDir().mkpath(path) == true);

        QString descrFile = QString("%1/%2").arg(path).arg(Constants::PACKDESCRIPTION_FILENAME);
        PackDescription descr;
        QHash<int, QString> values;
        populatePackDescription(descr, values);
        QVERIFY(Utils::saveStringToFile(descr.toXml(), descrFile) == true);

        request.serverUid = Constants::SERVER_COMMUNITY_FREE;
        request.descriptionFilePath = descrFile;
        _tempPathToDescriptionPath.insert(path, request.descriptionFilePath);

        for(int i = 0; i < numberOfContents; ++i) {
            if (type == RequestedPackCreation::UnzippedFile) {
                QString content;
                if (random.randomBool()) {
                    content = QString("%1/%2/context_%3.txt").arg(path).arg(Utils::createUid()).arg(i);
                    QVERIFY(QDir().mkpath(QFileInfo(content).absolutePath()) == true);
                } else {
                    content = QString("%1/context_%2.txt").arg(path).arg(i);
                }
                QVERIFY(Utils::saveStringToFile(createFakeContent(1024 * (i+1)), content) == true);
                request.content.insert(RequestedPackCreation::UnzippedFile, content);
            } else if (type == RequestedPackCreation::ZippedFile) {
                QString content;
                if (random.randomBool()) {
                    content = QString("%1/%2/context_%3.txt").arg(path).arg(Utils::createUid()).arg(i);
                    QVERIFY(QDir().mkpath(QFileInfo(content).absolutePath()) == true);
                } else {
                    content = QString("%1/context_%2.txt").arg(path).arg(i);
                }
                QString zipFileName = QString("%1/zipped_context_%2.zip").arg(path).arg(i);
                QVERIFY(Utils::saveStringToFile(createFakeContent(1024 * (i+1)), content) == true);
                QVERIFY(JlCompress::compressFiles(zipFileName, QStringList() << content) == true);
                QVERIFY(QFile(content).remove() == true);
                request.content.insert(RequestedPackCreation::ZippedFile, zipFileName);
            } else if (type == RequestedPackCreation::DirContent) {
                QString dir;
                if (random.randomBool()) {
                    dir = QString("%1/%2/%3").arg(path).arg(Utils::createUid()).arg(Utils::createUid());
                    QVERIFY(QDir().mkpath(QFileInfo(dir).absolutePath()) == true);
                } else {
                    dir = QString("%1/%2").arg(path).arg(Utils::createUid());
                }
                QVERIFY(QDir().mkpath(dir));
                QString content = QString("%1/%2").arg(dir).arg("dir_content.txt");
                QVERIFY(Utils::saveStringToFile(createFakeContent(1024 * (i+1)), content) == true);
                request.content.insert(RequestedPackCreation::DirContent, dir);
            }
        }
    }

    void createFakeQueue(const QString &path, DataPack::PackCreationQueue &queue)
    {
        using namespace DataPack;

        for(int i=0; i < loop; ++i) {
            RequestedPackCreation request;
            createFakePackDescriptionAndContent(path, request);
            QVERIFY(queue.addToQueue(request) == true);
        }

        QString queueFileName = QString("%1/%2")
                .arg(path)
                .arg(Constants::PACKCREATIONQUEUE_DEFAULT_FILENAME);
        QVERIFY(queue.saveToXmlFile(queueFileName) == true);
    }

private Q_SLOTS:

    void initTestCase()
    {
        Utils::Log::setDebugFileInOutProcess(false);

        _tempPath = QString("%1/packcreationmodel_%2/").arg(QDir::tempPath()).arg(Utils::createUid());
        QVERIFY(QDir().mkpath(_tempPath) == true);
        qDebug() << "Queue test path" << _tempPath;

        using namespace DataPack;

        for(int i=0; i < loop; ++i) {
            PackCreationQueue *queue = new PackCreationQueue;
            QString path = QString("%1/Q_%2").arg(_tempPath).arg(Utils::createUid());
            createFakeQueue(path, *queue);
            queues.append(queue);
        }
    }


    void test_createFakeSetOfQueue()
    {
        using namespace DataPack;

        QList<PackCreationQueue *> queues;
        for(int i=0; i < loop; ++i) {
            PackCreationQueue *queue = new PackCreationQueue;
            QString path = QString("%1/Q_%2").arg(_tempPath).arg(Utils::createUid());
            createFakeQueue(path, *queue);
            queues.append(queue);
        }

        PackCreationModel globalModel(this);
        for(int i=0; i < queues.count(); ++i) {
            PackCreationQueue *queue = queues.at(i);

            PackCreationModel model(this);
            QVERIFY(model.addPackCreationQueue(*queue) == true);
            QVERIFY(model.getCheckedPacks().count() == loop);

            QVERIFY(globalModel.addPackCreationQueue(*queue) == true);
            QVERIFY(globalModel.getCheckedPacks().count() == (loop * (i+1)));
        }

        for(int i=0; i < queues.count(); ++i) {
            globalModel.clearPackModel();
            QVERIFY(globalModel.getCheckedPacks().isEmpty() == true);
            PackCreationQueue *queue = queues.at(i);
            QVERIFY(globalModel.addPackCreationQueue(*queue) == true);
            QVERIFY(globalModel.getCheckedPacks().count() == loop);
        }

        globalModel.clearPackModel();
        for(int i=0; i < queues.count(); ++i) {
            PackCreationQueue *queue = queues.at(i);
            PackCreationModel model(this);
            QVERIFY(model.addScreeningPath(QFileInfo(queue->sourceAbsolutePathFile()).absolutePath()) == true);
            QVERIFY(model.getCheckedPacks().count() == loop);

            QVERIFY(globalModel.addScreeningPath(QFileInfo(queue->sourceAbsolutePathFile()).absolutePath()) == true);
            QVERIFY(globalModel.getCheckedPacks().count() == (loop * (i+1)));
        }



    }


    void cleanupTestCase()
    {
        qDeleteAll(queues);
        QVERIFY(Utils::removeDirRecursively(_tempPath, 0) == true);
    }
};

DECLARE_TEST(tst_DataPack_QueueModel)
#include "tst_datapack_queuemodel.moc"


