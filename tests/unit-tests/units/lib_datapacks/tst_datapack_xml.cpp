/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developer: Eric MAEKER, <eric.maeker@gmail.com>                   *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include <utils/global.h>

#include "../../autotest.h"

#include <utils/global.h>
#include <datapackutils/pack.h>
#include <datapackutils/packdescription.h>
#include <datapackutils/server.h>
#include <datapackutils/servercreation/packcreationqueue.h>
#include <datapackutils/constants.h>
#include <datapackutils/servercontent.h>

#include <QDebug>
#include <QTest>
#include <QDir>
#include <QFileInfo>



namespace {

static void populatePackDescription(DataPack::PackDescription &desc, QHash<int, QString> &values)
{
    QString uid;
    for(int i = 0; i < DataPack::PackDescription::MaxParam; ++i) {
        uid = Utils::createUid();
        values.insert(i, uid);
        desc.setData(i, uid);
    }

    values.insert(DataPack::PackDescription::Size, "2349234");
    desc.setData(DataPack::PackDescription::Size, "2349234");
    uid = Utils::createUid();
    values.insert(DataPack::PackDescription::UnzipToPath, uid);
    desc.setData(DataPack::PackDescription::UnzipToPath, uid);
    uid = Utils::createUid();
    values.insert(DataPack::PackDescription::Md5, uid);
    desc.setData(DataPack::PackDescription::Md5, uid);
    uid = Utils::createUid();
    values.insert(DataPack::PackDescription::Sha1, uid);
    desc.setData(DataPack::PackDescription::Sha1, uid);
    uid = Utils::createUid();
    values.insert(DataPack::PackDescription::DataType, uid);
    desc.setData(DataPack::PackDescription::DataType, uid);
    uid = Utils::createUid();
    values.insert(DataPack::PackDescription::InstalledFiles, uid);
    desc.setData(DataPack::PackDescription::InstalledFiles, uid);
}

static void populateQueueWithRealDescriptionFiles(DataPack::PackCreationQueue &queue)
{
    QString grPath = QString("%1/global_resources/datapack_description").arg(SOURCES_ROOT_PATH);
    QFileInfoList list = Utils::getFiles(QDir(grPath), "packdescription.xml");
    foreach(const QFileInfo &info, list) {
        DataPack::RequestedPackCreation request;
        request.serverUid = DataPack::Constants::SERVER_COMMUNITY_FREE;
        request.descriptionFilePath = info.absoluteFilePath();
        request.content.insert(DataPack::RequestedPackCreation::UnzippedFile, info.absoluteFilePath()) ;
        QVERIFY(queue.addToQueue(request) == true);  // files exists!
    }
}

}

class tst_DataPack_Xml : public QObject
{
    Q_OBJECT
public:
    tst_DataPack_Xml() {}

private Q_SLOTS:
    void initTestCase()
    {
    }

    void testPackDescriptionFromToXml()
    {
        using namespace DataPack;

        DataPack::PackDescription desc;
        QHash<int, QString> values;
        populatePackDescription(desc, values);

        QString xml = desc.toXml();
        DataPack::PackDescription desc2;
        desc2.fromXmlContent(xml);
        QCOMPARE(desc.toXml(), desc2.toXml());
        foreach(const int key, values.uniqueKeys()) {
            QCOMPARE(desc2.data(key).toString(), values.value(key));
        }

        QVERIFY(desc == desc2);
    }

    void testPackDependenciesFromToXml()
    {
    }

    void testPackFromToXml()
    {
        DataPack::PackDescription desc;
        QHash<int, QString> values;
        populatePackDescription(desc, values);

        DataPack::Pack pack;
        pack.setPackDescription(desc);
        QVERIFY(pack.isValid() == true);

        QString xml = pack.toXml();
        QString fileName = QDir::tempPath() + "/ut_"+Utils::createUid();
        QVERIFY(Utils::saveStringToFile(xml, fileName) == true);
        DataPack::Pack pack2;
        pack2.fromXmlFile(fileName);
        QVERIFY(pack == pack2);
        QVERIFY(QFile(fileName).remove() == true);
    }

    void testPackCreationQueueFromToXml()
    {
        DataPack::PackCreationQueue queue;

        DataPack::RequestedPackCreation request;
        request.serverUid = DataPack::Constants::SERVER_COMMUNITY_FREE;
        request.descriptionFilePath = QString("%1/%2").arg(qApp->applicationDirPath()).arg(DataPack::Constants::PACKDESCRIPTION_FILENAME);
        request.content.insert(DataPack::RequestedPackCreation::DirContent, qApp->applicationDirPath()) ;
        request.content.insert(DataPack::RequestedPackCreation::ZippedFile, qApp->applicationDirPath() + "/zipped.zip") ;
        request.content.insert(DataPack::RequestedPackCreation::UnzippedFile, qApp->applicationDirPath() + "/test.xml") ;
        QVERIFY(queue.addToQueue(request) == false);  // files does not exist!
        request.content.clear();

        populateQueueWithRealDescriptionFiles(queue);

        QString fileName = QDir::tempPath() + "/ut_"+Utils::createUid();
        QVERIFY(queue.sourceAbsolutePathFile() != fileName);
        QVERIFY(queue.sourceAbsolutePathFile().isEmpty());
        QVERIFY(queue.saveToXmlFile(fileName, false) == true);
        QVERIFY(queue.sourceAbsolutePathFile() == fileName);

        DataPack::PackCreationQueue queue2;
        queue2.fromXmlFile(fileName);
        QVERIFY(queue == queue2);
        QVERIFY(QFile(fileName).remove() == true);
    }

    void testServerContentXml()
    {
        DataPack::ServerContent content;
        QVERIFY(content.packDescriptionFileNames().isEmpty() == true);
        QVERIFY(content.addPackRelativeFileName("Content1") == true);
        QVERIFY(content.addPackRelativeFileName("Content2") == true);
        QVERIFY(content.addPackRelativeFileName("Content3") == true);
        QVERIFY(content.addPackRelativeFileName("Content3") == false);
        QStringList test;
        test << "Content1" << "Content2" << "Content3";
        QVERIFY(content.packDescriptionFileNames().count() == 3);
        QVERIFY(content.packDescriptionFileNames() == test);

        DataPack::ServerContent content2;
        content2.fromXml(content.toXml());
        QVERIFY(content2.packDescriptionFileNames().count() == 3);
        QVERIFY(content2.packDescriptionFileNames() == test);

        content.clear();
        QVERIFY(content.packDescriptionFileNames().isEmpty() == true);
        content2.clear();
        QVERIFY(content2.packDescriptionFileNames().isEmpty() == true);
    }

    void cleanupTestCase()
    {}
};

DECLARE_TEST(tst_DataPack_Xml)
#include "tst_datapack_xml.moc"


