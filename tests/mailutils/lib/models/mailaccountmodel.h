/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef MAILUTILS_MAILACCOUNTMODEL_H
#define MAILUTILS_MAILACCOUNTMODEL_H

#include "../mail_exporter.h"
#include <QAbstractTableModel>

/**
 * \file ./tests/mailutils/lib/models/mailaccountmodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Mail {
class MailAccount;
namespace Internal {
class MailAccountModelPrivate;

class MAIL_EXPORT MailAccountModel : public QAbstractTableModel
{
    Q_OBJECT
    
public:
    enum ColumnDataRepresentation {
        Label = 0,
        Uid,
        UserFullName,
        UserAddress,
        UserLog,
        UserPass,
        HostName,
        HostPort,
        HostUseSsl,
        HostUseStartTsl,
        LastSucceededConnection,
        ColumnCount

    };
    explicit MailAccountModel(QObject *parent = 0);
    ~MailAccountModel();
    bool initialize();

    int columnCount(const QModelIndex &) const {return ColumnCount;}
    int rowCount(const QModelIndex &) const;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;

    void addMailAccount(const MailAccount &account);
    void addMailAccounts(const QList<MailAccount> &accounts);

    const QList<MailAccount> &mailAccounts() const;

Q_SIGNALS:
    
public Q_SLOTS:
    
private:
    Internal::MailAccountModelPrivate *d;
};

} // namespace Internal
} // namespace Mail

#endif  // MAILUTILS_MAILACCOUNTMODEL_H

