/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Guillaume Denry <guillaume.denry@gmail.com>                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef MAILUTILS_EXPORTER_H
#define MAILUTILS_EXPORTER_H

/**
  \namespace Mail
  \brief Namespace for the Mail manager library
*/

#include <qglobal.h>

// DEFINE EXPORTER
#if defined(MAIL_LIBRARY)
#define MAIL_EXPORT Q_DECL_EXPORT
#else
#define MAIL_EXPORT Q_DECL_IMPORT
#endif

#endif // MAILUTILS_EXPORTER_H
