/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef MAILUTILS_MAILACCOUNT_H
#define MAILUTILS_MAILACCOUNT_H

#include "mail_exporter.h"
#include <QString>
#include <QDateTime>

/**
 * \file ./tests/mailutils/lib/mailaccount.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Mail {
namespace Internal {
class MailAccountPrivate;
} // namespace Internal

class MAIL_EXPORT MailAccount
{    
public:
    explicit MailAccount();
    MailAccount(const MailAccount &copy);

    ~MailAccount();
    
    void setAccountDescription(const QString &descr);
    void setUserName(const QString &name);
    void setUserMailAddress(const QString &mailAddress);

    void setHostName(const QString &hostName);
    void setHostPort(int port);
    void setHostUsesSsl(bool useSsl);
    void setHostUsesStartTls(bool useTls);
    void setHostLogin(const QString &hostLogin);
    void setHostPassword(const QString &password);
    void setLastSucceededConnection(const QDateTime &dt);

    QString accountDescription() const;
    QString userName() const;
    QString userMailAddress() const;

    QString hostName() const;
    int hostPort() const;
    bool hostUsesSsl() const;
    bool hostUsesStartTls() const;
    QString hostLogin() const;
    QString hostPassword() const;
    QDateTime lastSucceededConnection() const;

    QString toXml() const;
    bool fromXml(const QString &xmlContent);

    MailAccount &operator=(const MailAccount &other);
    bool operator==(const MailAccount &other);

private:
    Internal::MailAccountPrivate *d;
};

} // namespace Mail

#endif  // MAILUTILS_MAILACCOUNT_H

