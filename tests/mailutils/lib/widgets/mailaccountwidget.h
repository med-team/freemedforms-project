/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef MAILUTILS_INTERNAL_MAILACCOUNTWIDGET_H
#define MAILUTILS_INTERNAL_MAILACCOUNTWIDGET_H

#include <QObject>

/**
 * \file ./tests/mailutils/lib/widgets/mailaccountwidget.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Mail {
namespace Internal {
class MailAccountModel;
class MailAccount;
class MailAccountWidgetPrivate;

class MailAccountWidget : public QObject
{
    Q_OBJECT
    
public:
    explicit MailAccountWidget(QObject *parent = 0);
    ~MailAccountWidget();
    bool initialize();

    void setMailAccount(const MailAccount &account);

    void setMailAccountModel(MailAccountModel *model);
    void setMailAccountModelIndex(MailAccountModel *model);

Q_SIGNALS:
    
public Q_SLOTS:
    
private:
    MailAccountWidgetPrivate *d;
};

} // namespace Internal
} // namespace Mail

#endif // MAILUTILS_INTERNAL_MAILACCOUNTWIDGET_H

