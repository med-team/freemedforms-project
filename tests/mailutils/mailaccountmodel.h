/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *   Main developers : Eric Maeker
 *   Contributors :                                                        *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#ifndef ACCOUNT_MAILACCOUNTMODEL_H
#define ACCOUNT_MAILACCOUNTMODEL_H

#include "mail_exporter.h"
#include <QObject>

/**
 * \file ./tests/mailutils/mailaccountmodel.h
 * \author Eric Maeker
 * \version 1.0.0
 * \date 05 March 2017
*/

namespace Account {
namespace Internal {
class MailAccountModelPrivate;
} // namespace Internal

class ACCOUNT_EXPORT MailAccountModel : public QObject
{
    Q_OBJECT
    
public:
    explicit MailAccountModel(QObject *parent = 0);
    ~MailAccountModel();
    
    bool initialize();
    
Q_SIGNALS:
    
public Q_SLOTS:
    
private:
    Internal::MailAccountModelPrivate *d;
};

} // namespace Account

#endif  // ACCOUNT_MAILACCOUNTMODEL_H

