/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "mainwindow.h"

#include <datapackutils/datapackcore.h>
#include <datapackutils/iservermanager.h>

#include <utils/global.h>

#include <QApplication>
#include <QDesktopWidget>
#include <QDebug>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("datapack");
    a.setApplicationVersion("0.7.0");

    MainWindow w;
    w.show();
    Utils::centerWidget(&w, qApp->desktop());

    return a.exec();
}
