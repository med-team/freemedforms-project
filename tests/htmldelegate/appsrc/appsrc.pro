#/***************************************************************************
# *  The FreeMedForms project is a set of free, open source medical         *
# *  applications.                                                          *
# *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
# *  All rights reserved.                                                   *
# *                                                                         *
# *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
# ***************************************************************************/
#/***************************************************************************
# *  Main developers : Eric MAEKER, <eric.maeker@gmail.com>                 *
# *  Contributors:                                                          *
# *       NAME <MAIL@ADDRESS.COM>                                           *
# ***************************************************************************/

TEMPLATE = app
TARGET = htmldelegate

QT += xml network core gui

include(../../../buildspecs/config.pri)
include(../../../libs/utils.pri)
include(../../../libs/translationutils.pri)
include(../../../libs/rpath.pri)

HEADERS = mainwindow.h \
    ../../../libs/utils/widgets/htmldelegate.h \
    fakehtmlmodel.h \
    AnnouncementItemDelegate.h

SOURCES = main.cpp \
    ../../../libs/utils/widgets/htmldelegate.cpp \
    mainwindow.cpp \
    fakehtmlmodel.cpp \
    AnnouncementItemDelegate.cpp

FORMS = mainwindow.ui

