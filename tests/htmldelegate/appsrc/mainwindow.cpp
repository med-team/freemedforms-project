/***************************************************************************
 *  The FreeMedForms project is a set of free, open source medical         *
 *  applications.                                                          *
 *  (C) 2008-2017 by Eric MAEKER, MD (France) <eric.maeker@gmail.com>      *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  License: GPLv3 (see http://www.gnu.org/licenses/licenses.html)         *
 ***************************************************************************/
/***************************************************************************
 *  Main Developers:                                                       *
 *       Eric MAEKER, MD <eric.maeker@gmail.com>                           *
 *  Contributors:                                                          *
 *       NAME <MAIL@ADDRESS.COM>                                           *
 ***************************************************************************/
#include "mainwindow.h"
#include "fakehtmlmodel.h"

#include "ui_mainwindow.h"

#include <utils/global.h>
#include <utils/widgets/htmldelegate.h>

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QStandardItemModel *model = new FakeHtmlModel(this);

    ui->listView->setModel(model);
    ui->listView->setItemDelegate(new Utils::HtmlDelegate(this));
    ui->listView->setUniformItemSizes(false);
    ui->listView->setResizeMode(QListView::Adjust);

    ui->treeView->setModel(model);
    ui->treeView->setItemDelegate(new Utils::HtmlDelegate(this));
    ui->treeView->setUniformRowHeights(false);
    ui->treeView->setIndentation(10);

    ui->tableView->setModel(model);
    ui->tableView->setItemDelegate(new Utils::HtmlDelegate(this));

    resize(900,600);
}

MainWindow::~MainWindow()
{
    delete ui;
}

